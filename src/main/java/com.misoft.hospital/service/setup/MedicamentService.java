package com.misoft.hospital.service.setup;

import com.misoft.hospital.model.setup.Medicament;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

/**
 * Created by kodero on 5/17/16.
 */
public class MedicamentService {
    @Inject
    EntityManager em;

    public Medicament getLabTestTypeByCode(Long productCode){
        List<Medicament> pList = em.createQuery("select m from Medicament m where m.product.productCode =:productCode and m.deleted <> 1")
                .setParameter("productCode", productCode)
                .getResultList();
        if(pList.size() > 0) return pList.get(0);
        return null;
    }
}
