package com.misoft.hospital.service.setup;

import com.misoft.hospital.model.setup.Staff;

import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import java.util.List;

/**
 * Created by kodero on 5/17/16.
 */
@Named
public class StaffService {
    @Inject
    EntityManager em;

    public Staff getStaffByCode(String staffNo){
        List<Staff> pList = em.createQuery("select new Staff(s.id, s.surname, s.otherNames) from Staff s where s.staffNo =:staffNo and s.deleted <> 1")
                .setParameter("staffNo", staffNo)
                .getResultList();
        if(pList.size() > 0) return pList.get(0);
        return null;
    }
}
