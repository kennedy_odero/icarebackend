package com.misoft.hospital.service.setup;

import com.misoft.hospital.model.setup.LabTestType;

import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import java.util.List;

/**
 * Created by kodero on 5/16/16.
 */
@Named
public class LabTestTypeService {

    @Inject
    EntityManager em;

    public LabTestType getLabTestTypeByCode(String lttCode){
        List<LabTestType> pList = em.createQuery("select ltt from LabTestType ltt where ltt.code =:lttNo and ltt.deleted <> 1")
                .setParameter("lttNo", lttCode)
                .getResultList();
        if(pList.size() > 0) return pList.get(0);
        return null;
    }
}
