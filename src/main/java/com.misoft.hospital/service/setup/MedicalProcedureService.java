package com.misoft.hospital.service.setup;

import com.misoft.hospital.model.setup.MedicalProcedure;

import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import java.util.List;

/**
 * Created by kodero on 6/2/16.
 */
@Named
public class MedicalProcedureService {
    @Inject
    EntityManager em;

    public MedicalProcedure getMedicalProcedureByCode(String mpCode){
        List<MedicalProcedure> pList = em.createQuery("select mp from MedicalProcedure mp where mp.product.productCode =:mpCode and mp.deleted <> 1")
                .setParameter("mpCode", Long.parseLong(mpCode))
                .getResultList();
        if(pList.size() > 0) return pList.get(0);
        return null;
    }
}
