package com.misoft.hospital.service.healthcare;

import com.misoft.hospital.model.healthcare.*;
import com.misoft.hospital.rest.BaseEntityService;
import com.misoft.hospital.rest.util.SequenceManager;
import com.misoft.hospital.service.grant.GService;
import com.misoft.hospital.service.setup.LabTestTypeService;
import com.misoft.hospital.service.setup.StaffService;
import corvid.model.common.ServiceRequestIn;

import javax.enterprise.event.Event;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;
import java.util.logging.Logger;

/**
 * Created by kodero on 5/16/16.
 */
@Named
public class LabRequestService {
    @Inject
    SequenceManager seq;

    @Inject
    PatientService patientService;

    @Inject
    AppointmentService appointmentService;

    @Inject
    LabTestTypeService labTestTypeService;

    @Inject
    StaffService staffService;

    @Inject
    GService gService;

    @Inject
    protected Logger log;

    @Inject
    Event<BaseEntityService.EntityCreatedEvent<LabRequest>> event;

    //process incoming lab request
    public void processIncomingLabRequest(ServiceRequestIn serviceRequestIn){
        //create a lab request based on the @labRequestIn received
        LabRequest labRequest = new LabRequest();
        labRequest.setPatient(patientService.getPatientByPatientNumber(serviceRequestIn.getPatientNo()));
        labRequest.setAppointment(appointmentService.getAppointmentByPatientNumber(serviceRequestIn.getAppointmentNo()));
        labRequest.setAutoSubmit(true);
        labRequest.setRequestSource(RequestSource.Internal);
        labRequest.setRequestStatus(RequestStatus.SUBMITTED);
        labRequest.setDateRequested(serviceRequestIn.getDateOfRequest());
        //labRequest.setServiceCenter(gService.find("89dbdea9-b78e-4cf2-a68d-10049bbe15da", ServiceCenter.class));
        labRequest.setConsultationRequest(gService.find("15a476a3-d604-4688-b9f7-ef68aef5c1d1", ConsultationRequest.class));
        labRequest.setRequestNo(seq.nextSequence("SERVICE_REQUEST"));
        labRequest.addLabRequestItem(new LabRequestItem(labTestTypeService.getLabTestTypeByCode(serviceRequestIn.getProductCode()), labRequest.getRequestNo() + "-1", serviceRequestIn.getImpression()));
        labRequest.setRequester(staffService.getStaffByCode(serviceRequestIn.getStaffNo()));
        gService.persist(labRequest);
        event.fire(new BaseEntityService.EntityCreatedEvent<>(labRequest));
    }
}
