package com.misoft.hospital.service.healthcare;

import com.misoft.hospital.model.healthcare.Appointment;
import com.misoft.hospital.model.healthcare.Patient;
import com.misoft.hospital.service.grant.GService;
import com.misoft.hospital.util.callbacks.During;
import com.misoft.hospital.util.callbacks.EntityCallbackMethod;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.logging.Logger;

/**
 * Created by kodero on 6/22/16.
 */
@Named
public class AppointmentCallback {
    @Inject
    private GService gService;

    @Inject
    Logger log;

    @EntityCallbackMethod(when = During.CREATE)
    public void checkAndCreatePatient(Appointment appointment){
        if(appointment.getNewPatient()){
            try {
                log.info("=================Creating patient=================");
                Patient patient = appointment.getPatient();
                String[] names = patient.getPatientName().split(" ");
                patient.setSurname(names[0].trim());
                patient.setOtherNames((patient.getPatientName().replace(names[0].trim(), "")).trim());
                appointment.setPatient(gService.makePersistent(patient));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
