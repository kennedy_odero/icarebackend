package com.misoft.hospital.service.healthcare;

import com.misoft.hospital.model.healthcare.*;
import com.misoft.hospital.model.setup.Medicament;
import com.misoft.hospital.model.setup.MedicamentFrequency;
import com.misoft.hospital.rest.BaseEntityService;
import com.misoft.hospital.rest.util.SequenceManager;
import com.misoft.hospital.service.grant.GService;
import com.misoft.hospital.service.setup.StaffService;
import corvid.model.common.PrescriptionRequestIn;

import javax.enterprise.event.Event;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.inject.Named;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.transaction.Transactional;
import javax.validation.ValidationException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.logging.Logger;

/**
 * Created by kodero on 6/1/16.
 */
@Named
public class PrescriptionRequestService {
    @Inject
    SequenceManager seq;

    @Inject
    PatientService patientService;

    @Inject
    AppointmentService appointmentService;

    @Inject
    StaffService staffService;

    @Inject
    GService gService;

    @Inject
    Event<BaseEntityService.EntityCreatedEvent<PrescriptionRequest>> event;

    @Inject
    MedicamentService medicamentService;

    @Inject
    Logger log;

    //process incoming lab request
    @Transactional
    public void processIncomingLabRequest(@Observes PrescriptionRequestIn prescriptionRequestIn){
        //create a lab request based on the @labRequestIn received
        PrescriptionRequest prescriptionRequest = new PrescriptionRequest();
        prescriptionRequest.setPatient(patientService.getPatientByPatientNumber(prescriptionRequestIn.getPatientNo()));
        prescriptionRequest.setAppointment(appointmentService.getAppointmentByPatientNumber(prescriptionRequestIn.getAppointmentNo()));
        prescriptionRequest.setAutoSubmit(true);
        prescriptionRequest.setRequestSource(RequestSource.Internal);
        prescriptionRequest.setRequestStatus(RequestStatus.SUBMITTED);
        prescriptionRequest.setDateRequested(prescriptionRequestIn.getDateOfRequest());
        prescriptionRequest.setRequestNo(seq.nextSequence("SERVICE_REQUEST"));
        Medicament medicament = medicamentService.getMedicamentByProductCode(prescriptionRequestIn.getDrugCode());
        MedicamentFrequency medicamentFrequency = medicamentService.getFrequencyByCode(prescriptionRequestIn.getSchedule());
        Integer dispenseQuantity = calculateDosage(medicament, medicamentFrequency, prescriptionRequestIn.getDosage(), prescriptionRequestIn.getDuration());
        prescriptionRequest.addPrescriptionRequestItem(new PrescriptionRequestItem(medicamentService.getMedicamentByProductCode(prescriptionRequestIn.getDrugCode()), new BigDecimal(prescriptionRequestIn.getDosage()), medicamentService.getFrequencyByCode(prescriptionRequestIn.getSchedule()), dispenseQuantity, prescriptionRequestIn.getDuration()));
        prescriptionRequest.setRequester(staffService.getStaffByCode(prescriptionRequestIn.getStaffNo()));
        gService.persist(prescriptionRequest);
        event.fire(new BaseEntityService.EntityCreatedEvent<>(prescriptionRequest));
    }

    private Integer calculateDosage(Medicament medicament, MedicamentFrequency medicamentFrequency, Integer perDoseQty, Integer duration){
        ScriptEngineManager mgr = new ScriptEngineManager();
        ScriptEngine engine = mgr.getEngineByName("JavaScript");
        try {
            //lets execute the expression
            log.info("Per dosage quantity : " + perDoseQty);
            log.info("Duration : " + duration);
            String toEval = medicamentFrequency.getDosageFormula().replaceAll("p", perDoseQty + "").replaceAll("d", duration + "");
            Integer dosageQuantity = (Integer) engine.eval(toEval);
            BigDecimal dispenseQty = new BigDecimal(dosageQuantity / medicament.getUnitQuantity().doubleValue()).setScale(0, RoundingMode.HALF_UP);
            return dispenseQty.intValue();
        }catch (Exception e){
            e.printStackTrace();
            return 0;
        }
    }
}
