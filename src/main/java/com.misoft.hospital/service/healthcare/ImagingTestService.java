package com.misoft.hospital.service.healthcare;

import com.misoft.hospital.model.setup.ImagingTest;

import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import java.util.List;

/**
 * Created by kodero on 6/1/16.
 */
@Named
public class ImagingTestService {
    @Inject
    EntityManager em;

    public ImagingTest getImagingTestByProductCode(String productCode){
        List<ImagingTest> pList = em.createQuery("select it from ImagingTest it where it.product.productCode =:productCode and it.deleted <> 1")
                .setParameter("productCode", Long.parseLong(productCode))
                .getResultList();
        if(pList.size() > 0) return pList.get(0);
        return null;
    }
}
