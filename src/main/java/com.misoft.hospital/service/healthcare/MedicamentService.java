package com.misoft.hospital.service.healthcare;

import com.misoft.hospital.model.setup.Medicament;
import com.misoft.hospital.model.setup.MedicamentFrequency;

import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import java.util.List;

/**
 * Created by kodero on 6/1/16.
 */
@Named
public class MedicamentService {

    @Inject
    EntityManager em;

    public Medicament getMedicamentByProductCode(String productCode){
        List<Medicament> pList = em.createQuery("select m from Medicament m where m.product.productCode =:productCode and m.deleted <> 1")
                .setParameter("productCode", Long.parseLong(productCode))
                .getResultList();
        if(pList.size() > 0) return pList.get(0);
        return null;
    }

    public MedicamentFrequency getFrequencyByCode(String freqCode){
        List<MedicamentFrequency> pList = em.createQuery("select mf from MedicamentFrequency mf where mf.abbreviation like '%" + freqCode + "%' and mf.deleted <> 1")
                .getResultList();
        if(pList.size() > 0) return pList.get(0);
        return null;
    }
}
