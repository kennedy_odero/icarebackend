package com.misoft.hospital.service.healthcare;

import com.misoft.hospital.model.events.BatchBillingEvent;
import com.misoft.hospital.model.healthcare.*;
import com.misoft.hospital.rest.BaseEntityService;
import com.misoft.hospital.rest.util.SequenceManager;
import com.misoft.hospital.service.admin.UserService;
import com.misoft.hospital.service.grant.GService;
import com.misoft.hospital.util.callbacks.During;
import com.misoft.hospital.util.callbacks.EntityCallbackMethod;
import org.joda.time.DateTime;
import org.joda.time.Years;

import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by kodero on 6/26/16.
 */
@Named
public class ConsultationRequestCallback {

    @Inject
    SequenceManager sequenceManager;

    @Inject
    UserService userService;

    @Inject
    GService gService;

    @Inject
    Logger log;

    @Inject
    Event<BaseEntityService.EntityCreatedEvent<LabRequest>> labRequestCreated;

    @Inject
    Event<BaseEntityService.EntityCreatedEvent<ImagingRequest>> imagingRequestCreated;

    @Inject
    Event<BaseEntityService.EntityCreatedEvent<OperationRequest>> operationRequestCreated;

    @Inject
    Event<BaseEntityService.EntityCreatedEvent<PrescriptionRequest>> prescriptionRequestCreated;

    @Inject
    Event<BatchBillingEvent> batchBillingEvent;

    @EntityCallbackMethod(when = During.UPDATE)
    public void createLabRequest(ConsultationRequest consultationRequest) throws Exception {
        //batch requests
        BatchBillingEvent batchEvent = new BatchBillingEvent();

        if(consultationRequest.getLabRequestItems().size() > 0){
            //create lab request
            Long requestNo = sequenceManager.nextSequence("SERVICE_REQUEST");
            List<LabRequestItem> newItems = new ArrayList<>();
            int i = 1;
            for (LabRequestItem lri : consultationRequest.getLabRequestItems()){
                if(lri.getId() == null){
                    lri.setItemRequestNo(requestNo + "-" + i );
                    lri.setComments(consultationRequest.getDiagnosis());
                    newItems.add(lri);
                }
                i++;
            }
            if(newItems.size() > 0){//only create a lab request if there are new items
                LabRequest labRequest = new LabRequest(requestNo, RequestSource.Internal, consultationRequest.getPatient(), new Date(), userService.currentUser().getStaff(), RequestStatus.SUBMITTED, consultationRequest.getAppointment());
                labRequest = gService.makePersistent(labRequest);
                for(LabRequestItem lri : newItems){ //hack!!!
                    labRequest.addLabRequestItem(gService.makePersistent(lri));//for this hack should go away!!
                }
                gService.makePersistent(labRequest);
                //labRequestCreated.fire(new BaseEntityService.EntityCreatedEvent<>(labRequest));
                batchEvent.addRequest(labRequest);
            }
        }

        //prescription request
        if(consultationRequest.getPrescriptionRequestItems().size() > 0){
            //create prescription request
            Long requestNo = sequenceManager.nextSequence("SERVICE_REQUEST");
            List<PrescriptionRequestItem> newPrescriptionItems = new ArrayList<>();
            for (PrescriptionRequestItem pri : consultationRequest.getPrescriptionRequestItems()){
                if(pri.getId() == null){
                    newPrescriptionItems.add(pri);
                }
            }
            if(newPrescriptionItems.size() > 0){//only create a lab request if there are new items
                PrescriptionRequest prescriptionRequest = new PrescriptionRequest(requestNo, RequestSource.Internal, consultationRequest.getPatient(), new Date(), userService.currentUser().getStaff(), RequestStatus.SUBMITTED, consultationRequest.getAppointment());
                prescriptionRequest = gService.makePersistent(prescriptionRequest);
                log.info("Prescription items" + prescriptionRequest.getPrescriptionRequestItems().size());
                for(PrescriptionRequestItem pri : newPrescriptionItems){ //hack!!!
                    prescriptionRequest.addPrescriptionRequestItem(gService.makePersistent(pri));//for this hack should go away!!
                }
                //prescriptionRequest.addPrescriptionRequestItems(newItems);
                log.info("Prescription items" + prescriptionRequest.getPrescriptionRequestItems().size());
                gService.makePersistent(prescriptionRequest);
                //prescriptionRequestCreated.fire(new BaseEntityService.EntityCreatedEvent<>(prescriptionRequest));
                batchEvent.addRequest(prescriptionRequest);
            }
        }

        //imaging request
        if(consultationRequest.getImagingRequestItems().size() > 0){
            //create lab request
            Long requestNo = sequenceManager.nextSequence("SERVICE_REQUEST");
            List<ImagingRequestItem> newItems = new ArrayList<>();
            for (ImagingRequestItem iri : consultationRequest.getImagingRequestItems()){
                if(iri.getId() == null){
                    newItems.add(iri);
                }
            }
            if(newItems.size() > 0){//only create a lab request if there are new items
                ImagingRequest imagingRequest = new ImagingRequest(requestNo, RequestSource.Internal, consultationRequest.getPatient(), new Date(), userService.currentUser().getStaff(), RequestStatus.SUBMITTED, consultationRequest.getAppointment());
                imagingRequest = gService.makePersistent(imagingRequest);
                for(ImagingRequestItem iri : newItems){ //hack!!!
                    imagingRequest.addImagingRequestItem(gService.makePersistent(iri));//for this hack should go away!!
                }
                gService.makePersistent(imagingRequest);
                //imagingRequestCreated.fire(new BaseEntityService.EntityCreatedEvent<>(imagingRequest));
                batchEvent.addRequest(imagingRequest);
            }
        }

        //medical procedures request
        if(consultationRequest.getRequestMedicalProcedures().size() > 0){
            //create lab request
            Long requestNo = sequenceManager.nextSequence("SERVICE_REQUEST");
            List<RequestMedicalProcedure> newItems = new ArrayList<>();
            for (RequestMedicalProcedure iri : consultationRequest.getRequestMedicalProcedures()){
                if(iri.getId() == null){
                    newItems.add(iri);
                }
            }
            if(newItems.size() > 0){//only create a lab request if there are new items
                OperationRequest operationRequest = new OperationRequest(requestNo, RequestSource.Internal, consultationRequest.getPatient(), new Date(), userService.currentUser().getStaff(), RequestStatus.SUBMITTED, consultationRequest.getAppointment());
                operationRequest = gService.makePersistent(operationRequest);
                for(RequestMedicalProcedure rmp : newItems){ //hack!!!
                    operationRequest.addOperationRequestItem(gService.makePersistent(rmp));//for this hack should go away!!
                }
                gService.makePersistent(operationRequest);
                //operationRequestCreated.fire(new BaseEntityService.EntityCreatedEvent<>(operationRequest));
                batchEvent.addRequest(operationRequest);
            }
        }

        //update consultation diagnoses
        for(ConsultationDiagnosis cd : consultationRequest.getConsultationDiagnosises()){
            setDiagnosisAge(cd);
        }

        //fire batch billing
        batchBillingEvent.fire(batchEvent);
    }


    public void setDiagnosisAge(ConsultationDiagnosis consultationDiagnosis){
        int age = Years.yearsBetween(new DateTime(consultationDiagnosis.getConsultationRequest().getPatient().getDob()), new DateTime()).getYears();
        consultationDiagnosis.setAge(age);
        if(age <= 5) consultationDiagnosis.setAgeGroup("UNDER5");
        else consultationDiagnosis.setAgeGroup("OVER5");
        //appointment
        consultationDiagnosis.setAppointment(consultationDiagnosis.getConsultationRequest().getAppointment());
        consultationDiagnosis.setNewPatient(consultationDiagnosis.getAppointment().getNewPatient());
    }
}
