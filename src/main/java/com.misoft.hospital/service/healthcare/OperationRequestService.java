package com.misoft.hospital.service.healthcare;

import com.misoft.hospital.model.healthcare.*;
import com.misoft.hospital.rest.BaseEntityService;
import com.misoft.hospital.rest.util.SequenceManager;
import com.misoft.hospital.service.grant.GService;
import com.misoft.hospital.service.setup.MedicalProcedureService;
import com.misoft.hospital.service.setup.StaffService;
import corvid.model.common.ServiceRequestIn;

import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by kodero on 6/2/16.
 */
@Named
public class OperationRequestService {
    @Inject
    SequenceManager seq;

    @Inject
    PatientService patientService;

    @Inject
    AppointmentService appointmentService;

    @Inject
    StaffService staffService;

    @Inject
    GService gService;

    @Inject
    Event<BaseEntityService.EntityCreatedEvent<OperationRequest>> event;

    @Inject
    MedicalProcedureService medicalProcedureService;

    //process incoming lab request
    public void processIncomingOperationRequest(ServiceRequestIn serviceRequestIn){
        //create an operation request based on the @serviceRequestIn received
        OperationRequest operationRequest = new OperationRequest();
        operationRequest.setPatient(patientService.getPatientByPatientNumber(serviceRequestIn.getPatientNo()));
        operationRequest.setAppointment(appointmentService.getAppointmentByPatientNumber(serviceRequestIn.getAppointmentNo()));
        operationRequest.setAutoSubmit(true);
        operationRequest.setRequestSource(RequestSource.Internal);
        operationRequest.setRequestStatus(RequestStatus.SUBMITTED);
        operationRequest.setDateRequested(serviceRequestIn.getDateOfRequest());
        operationRequest.setRequestNo(seq.nextSequence("SERVICE_REQUEST"));
        operationRequest.addOperationRequestItem(new RequestMedicalProcedure(medicalProcedureService.getMedicalProcedureByCode(serviceRequestIn.getProductCode())));
        operationRequest.setRequester(staffService.getStaffByCode(serviceRequestIn.getStaffNo()));
        gService.persist(operationRequest);
        event.fire(new BaseEntityService.EntityCreatedEvent<>(operationRequest));
    }
}
