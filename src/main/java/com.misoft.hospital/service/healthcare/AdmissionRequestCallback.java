package com.misoft.hospital.service.healthcare;

import com.misoft.hospital.model.healthcare.*;
import com.misoft.hospital.rest.BaseEntityService;
import com.misoft.hospital.rest.util.SequenceManager;
import com.misoft.hospital.service.grant.GService;
import com.misoft.hospital.util.callbacks.During;
import com.misoft.hospital.util.callbacks.EntityCallbackMethod;

import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.Date;
import java.util.logging.Logger;

/**
 * Created by kodero on 6/23/16.
 */
@Named
public class AdmissionRequestCallback {
    @Inject
    private GService gService;

    @Inject
    Logger log;

    @Inject
    SequenceManager sequenceManager;

    @Inject
    Event<BaseEntityService.EntityCreatedEvent<Appointment>> appointmentCreatedEvent;

    @EntityCallbackMethod(when = During.CREATE)
    public void checkAndCreatePatient(AdmissionRequest admissionRequest){
        if(admissionRequest.getNewPatient()){
            try {
                log.info("=================Creating patient=================");
                Patient patient = admissionRequest.getPatient();
                String[] names = patient.getPatientName().split(" ");
                patient.setSurname(names[0].trim());
                patient.setOtherNames(names[1].trim());
                patient.setIdentificationNo(patient.getIdentificationNo());
                admissionRequest.setPatient(gService.makePersistent(patient));
                //now create appointment
                Appointment appointment = new Appointment();
                appointment.setPatient(patient);
                appointment.setDate(new Date());
                appointment.setAppointmentNo(sequenceManager.nextSequence("APPOINTMENTS"));
                appointment.setAutoCreateTriage(true);
                appointment.setBillPaymentType(BillPaymentType.SELF);
                appointment.setUrgency(Urgency.NORMAL);
                appointment.setStatus(Status.OPEN);
                appointment = gService.makePersistent(appointment);
                //fire appointment created event
                appointmentCreatedEvent.fire(new BaseEntityService.EntityCreatedEvent<>(appointment));
                admissionRequest.setAppointment(appointment);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else{
            log.info("=================Updating patient details=================");
            Patient patient = admissionRequest.getPatient();
            patient.setIdentificationNo(admissionRequest.getIdentificationNo());
            patient.setResidentialAddress(admissionRequest.getResidentialAddress());
            patient.setNationalityString(admissionRequest.getNationalityString());
            patient.setEmployer(admissionRequest.getEmployer());
            patient.setInpatientNo(admissionRequest.getInpatientNo() + "");
            admissionRequest.setPatient(gService.edit(patient));
        }
    }
}
