package com.misoft.hospital.service.healthcare;

import com.misoft.hospital.model.healthcare.Appointment;
import com.misoft.hospital.model.healthcare.ConsultationRequest;
import com.misoft.hospital.model.healthcare.RequestStatus;
import com.misoft.hospital.model.healthcare.Status;
import com.misoft.hospital.rest.BaseEntityService;

import javax.enterprise.event.Observes;
import javax.enterprise.event.TransactionPhase;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import java.util.List;

/**
 * Created by kodero on 1/13/16.
 */
@Named
public class ConsultationRequestService {
    @Inject
    EntityManager entityManager;

    public void onConsultationRequestComplete(@Observes(during = TransactionPhase.IN_PROGRESS) BaseEntityService.EntityUpdatedEvent<ConsultationRequest> event){
        //check if consultation has been closed
        if(event.getEntity().getRequestStatus() == RequestStatus.COMPLETE){
            //close the hospital visit as well
            Appointment app = event.getEntity().getAppointment();
            app.setStatus(Status.CLOSED);
        }
    }

    public ConsultationRequest getConsultationRequestByAppointment(Appointment appointment){
        List<ConsultationRequest> requests = entityManager.createQuery("select cr from ConsultationRequest cr where cr.appointment.id =:appointmentId")
                .setParameter("appointmentId", appointment.getId())
                .getResultList();
        if(requests.size() > 0) return requests.get(0);
        return null;
    }
}
