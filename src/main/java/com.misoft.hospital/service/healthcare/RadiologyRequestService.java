package com.misoft.hospital.service.healthcare;

import com.misoft.hospital.model.healthcare.*;
import com.misoft.hospital.rest.BaseEntityService;
import com.misoft.hospital.rest.util.SequenceManager;
import com.misoft.hospital.service.grant.GService;
import com.misoft.hospital.service.setup.StaffService;
import corvid.model.common.ServiceRequestIn;

import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by kodero on 6/1/16.
 */
@Named
public class RadiologyRequestService {
    @Inject
    SequenceManager seq;

    @Inject
    PatientService patientService;

    @Inject
    AppointmentService appointmentService;

    @Inject
    StaffService staffService;

    @Inject
    GService gService;

    @Inject
    Event<BaseEntityService.EntityCreatedEvent<ImagingRequest>> event;

    @Inject
    ImagingTestService imagingTestService;

    //process incoming lab request
    public void processIncomingImagingRequest(ServiceRequestIn serviceRequestIn){
        //create a lab request based on the @labRequestIn received
        ImagingRequest imagingRequest = new ImagingRequest();
        imagingRequest.setPatient(patientService.getPatientByPatientNumber(serviceRequestIn.getPatientNo()));
        imagingRequest.setAppointment(appointmentService.getAppointmentByPatientNumber(serviceRequestIn.getAppointmentNo()));
        imagingRequest.setAutoSubmit(true);
        imagingRequest.setRequestSource(RequestSource.Internal);
        imagingRequest.setRequestStatus(RequestStatus.SUBMITTED);
        imagingRequest.setDateRequested(serviceRequestIn.getDateOfRequest());
        //labRequest.setServiceCenter(gService.find("89dbdea9-b78e-4cf2-a68d-10049bbe15da", ServiceCenter.class));
        imagingRequest.setConsultationRequest(gService.find("15a476a3-d604-4688-b9f7-ef68aef5c1d1", ConsultationRequest.class));
        imagingRequest.setRequestNo(seq.nextSequence("SERVICE_REQUEST"));
        imagingRequest.addImagingRequestItem(new ImagingRequestItem(imagingTestService.getImagingTestByProductCode(serviceRequestIn.getProductCode())));
        imagingRequest.setRequester(staffService.getStaffByCode(serviceRequestIn.getStaffNo()));
        gService.persist(imagingRequest);
        event.fire(new BaseEntityService.EntityCreatedEvent<>(imagingRequest));
    }
}
