package com.misoft.hospital.service.healthcare;

import com.misoft.hospital.model.healthcare.*;
import com.misoft.hospital.model.mortuary.MortuaryRequest;
import com.misoft.hospital.model.setup.Bed;
import com.misoft.hospital.model.setup.BedStatus;
import com.misoft.hospital.rest.BaseEntityService;
import com.misoft.hospital.rest.util.SequenceManager;
import com.misoft.hospital.service.admin.UserService;
import com.misoft.hospital.service.grant.GService;
import org.joda.time.DateTime;

import javax.enterprise.event.Event;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by kodero on 12/12/15.
 */
@Named
public class AdmissionService {

    static String inpatientNoSeqName = "IP/";

    @Inject
    GService gService;

    @Inject
    EntityManager em;

    @Inject
    SequenceManager sequenceManager;

    @Inject
    UserService userService;

    @Inject
    ConsultationRequestService consultationRequestService;

    @Inject
    Logger log;

    @Inject
    Event<BaseEntityService.EntityUpdatedEvent<ConsultationRequest>> consultationRequestEvent;

    public void onDischarge(@Observes BaseEntityService.EntityUpdatedEvent<AdmissionRequest> event){
        AdmissionRequest admissionRequest = event.getEntity();
        if(admissionRequest.getDischargeReason() == null) return;
        if(admissionRequest.getDischargeReason().getDischargeType() == DischargeType.DECEASED){
            //our patient died, update the alive status of the patient record
            Patient p = event.getEntity().getPatient();
            p.setPatientStatus(PatientStatus.DECEASED);
            gService.edit(p);
        }else{
            Patient p = event.getEntity().getPatient();
            p.setPatientStatus(PatientStatus.DISCHARGED);
            gService.edit(p);
        }

        //check if we need to stop billing
        if (admissionRequest.isCeaseBilling()) {
            //cease billing of inpatient
            for(AdmissionBed admissionBed : event.getEntity().getAdmissionBeds()){
                if(admissionBed.getDateTo() == null){//only end the accomodation for anything that has not ended already
                    if(admissionRequest.getDischargeReason().getDischargeType() == DischargeType.DECEASED)
                        admissionBed.setDateTo(event.getEntity().getDateDeceased());
                    else admissionBed.setDateTo(event.getEntity().getDateTo());
                    gService.edit(admissionBed);
                }
            }
        }

        //check if we need to create mortuary request
        if (admissionRequest.isCreateMortuaryRequest()) {
            //create a mortuary request
            log.info("============================== Creating mortuary request ==================");
            MortuaryRequest mortuaryRequest = getMortuaryRequestForAppointment(admissionRequest.getAppointment());
            if(mortuaryRequest == null) mortuaryRequest = new MortuaryRequest();
            mortuaryRequest.setPatient(admissionRequest.getPatient());
            mortuaryRequest.setRequestStatus(RequestStatus.SUBMITTED);
            mortuaryRequest.setRequester(userService.currentUser().getStaff());
            mortuaryRequest.setAutoSubmit(true);
            mortuaryRequest.setDateRequested(new Date());
            mortuaryRequest.setDeceasedOn(admissionRequest.getDateDeceased());
            mortuaryRequest.setRequestNo(sequenceManager.nextSequence("SERVICE_REQUEST"));
            mortuaryRequest.setAppointment(admissionRequest.getAppointment());
            gService.persist(mortuaryRequest);
        }

        //now close the consultation request
        if(admissionRequest.getDischargeReason() != null){
            ConsultationRequest consultationRequest = consultationRequestService.getConsultationRequestByAppointment(admissionRequest.getAppointment());
            consultationRequest.setRequestStatus(RequestStatus.COMPLETE);
            if(admissionRequest.getDischargeReason().getDischargeType() != DischargeType.DECEASED)
                consultationRequestEvent.fire(new BaseEntityService.EntityUpdatedEvent<>(consultationRequest));//only close appointment if the discharge is not death
        }
    }

    public void onAdmissionReceived(@Observes BaseEntityService.EntityUpdatedEvent<AdmissionRequest> event){
        AdmissionRequest admissionRequest = event.getEntity();
        //check the top event, if its RECEIVED, create an inpatient #
        if(event.getEntity().getRequestStatus() == RequestStatus.RECEIVED){
            //check if we already have an inpatient #
            if(event.getEntity().getPatient().getInpatientNo() == null){
                //generate a new inpatient no
                String seqPart = new DateTime().getYear() + "/" + new DateTime().getMonthOfYear();
                Long seq = sequenceManager.nextSequence(inpatientNoSeqName + seqPart);
                event.getEntity().getPatient().setInpatientNo(inpatientNoSeqName + seqPart + "/" + seq);
            }
        }

        //check if a bed is already assigned
        if(admissionRequest.getDischargeReason() == null
                && event.getEntity().getRequestStatus() == RequestStatus.PROCESSING
                && admissionRequest.getCurrentBed() != null){
            //toon has just been admitted
            Patient patient = admissionRequest.getPatient();
            patient.setPatientStatus(PatientStatus.ADMITTED);
            gService.edit(patient);
        }
    }

    public MortuaryRequest getMortuaryRequestForAppointment(Appointment appointment){
        List<MortuaryRequest> mrs = em.createQuery("select mr from MortuaryRequest mr where mr.appointment.id =:appointmentId")
                .setParameter("appointmentId", appointment.getId())
                .getResultList();
        if (mrs.size() > 1) return mrs.get(0);
        return null;
    }
}
