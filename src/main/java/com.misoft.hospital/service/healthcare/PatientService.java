package com.misoft.hospital.service.healthcare;

import com.misoft.hospital.model.healthcare.Patient;
import com.misoft.hospital.rest.BaseEntityService;
import org.krysalis.barcode4j.impl.code39.Code39Bean;
import org.krysalis.barcode4j.output.bitmap.BitmapCanvasProvider;
import org.krysalis.barcode4j.tools.UnitConv;

import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

/**
 * Created by kodero on 4/7/16.
 */
@Named
public class PatientService {

    @Inject
    EntityManager em;

    public void onCreatePatient(@Observes BaseEntityService.EntityCreatedEvent<Patient> event){
        createBarcode(event.getEntity());
    }

    public void onUpdatePatient(@Observes BaseEntityService.EntityUpdatedEvent<Patient> event){
        createBarcode(event.getEntity());
    }

    private void createBarcode(Patient patient) {
        if(patient.getPatientNoBarcode() == null){
            //create barcode image
            Code39Bean bean = new Code39Bean();
            final int dpi = 150;
            bean.setModuleWidth(UnitConv.in2mm(1.0f / dpi)); //makes the narrow bar
            bean.setWideFactor(3);
            bean.doQuietZone(false);
            //Open output file
            ByteArrayOutputStream bytesOut = new ByteArrayOutputStream();

            //OutputStream out = new FileOutputStream(outputFile);
            try {
                //Set up the canvas provider for monochrome PNG output
                BitmapCanvasProvider canvas = new BitmapCanvasProvider(
                        bytesOut, "image/x-png", dpi, BufferedImage.TYPE_BYTE_BINARY, false, 0);
                //Generate the barcode
                bean.generateBarcode(canvas, patient.getPatientNo());

                //Signal end of generation
                canvas.finish();
                patient.setPatientNoBarcode(bytesOut.toByteArray());
            }catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public Patient getPatientByPatientNumber(String patientNumber){
        List<Patient> pList = em.createQuery("select new Patient(p.id, p.patientNo, p.surname, p.otherNames, p.gender, p.dob) from Patient p where p.patientNo =:patientNo and deleted <> 1")
                .setParameter("patientNo", patientNumber)
                .getResultList();
        if(pList.size() > 0) return pList.get(0);
        return null;
    }
}
