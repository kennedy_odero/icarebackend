package com.misoft.hospital.service.healthcare;

import com.misoft.hospital.model.admin.User;
import com.misoft.hospital.model.healthcare.RequestStatus;
import com.misoft.hospital.model.healthcare.RequestStatusHistory;
import com.misoft.hospital.model.healthcare.ServiceRequest;
import com.misoft.hospital.rest.BaseEntityService;
import com.misoft.hospital.service.admin.UserService;
import com.misoft.hospital.service.grant.GService;

import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.ValidationException;
import java.util.*;
import java.util.logging.Logger;

/**
 * Created by kodero on 12/10/15.
 */
@Named
public class ServiceRequestService {

    @Inject
    private UserService userService;

    @Inject
    GService gService;

    @Inject
    protected Logger log;

    private Set<RequestStatus> canSubmitFromStatus = new HashSet<>(Arrays.asList(RequestStatus.NEW, RequestStatus.RECALLED, RequestStatus.RETURNED));
    private Set<RequestStatus> canReceiveRecallOrReturnFromStatus = new HashSet<>(Arrays.asList(RequestStatus.SUBMITTED));
    private Set<RequestStatus> canHoldFromStatus = new HashSet<>(Arrays.asList(RequestStatus.NEW, RequestStatus.RECALLED, RequestStatus.RETURNED));
    //private Set<RequestStatus> canCancelFromStatus = new HashSet<>(Arrays.asList(RequestStatus.NEW, RequestStatus.RECALLED, RequestStatus.RETURNED));

    public void entityCreatedEvent(@Observes BaseEntityService.EntityCreatedEvent<? extends ServiceRequest> event){
        log.info("========================== an object that extends setup request has been created===========================" + event.getEntity());
        //fix the date requested
        event.getEntity().setDateRequested(new Date());
        gService.edit(event.getEntity());
        checkRequestHistory(event.getEntity());
    }

    public void entityUpdatedEvent(@Observes BaseEntityService.EntityUpdatedEvent<? extends ServiceRequest> event){
        log.info("========================== an object that extends setup request has been updated===========================" + event.getEntity());
        checkRequestHistory(event.getEntity());
    }

    public void checkRequestHistory(ServiceRequest request){
        User u = null;
        try{
            u = userService.currentUser();
        }catch (Exception e){
            log.info("Didn't get user, thread most likely not a servlet thread!");
        }
        //update request status history
        if(request.getRequestStatusHistories().size() == 0){
            //new request, insert the first one
            request.addRequestStatusHistory(new RequestStatusHistory(request.getRequestStatus(), new Date(), u));
        }else{
            //there are already revisions, so check if the current status is the same top revision status
            if(request.getRequestStatus() != (new ArrayList<>(request.getRequestStatusHistories()).get(0).getStatus())){
                request.addRequestStatusHistory(new RequestStatusHistory(request.getRequestStatus(), new Date(), u));
            }
        }
        //check auto submit
        if(RequestStatus.NEW == request.getRequestStatus() && request.getAutoSubmit()){
            request.setRequestStatus(RequestStatus.SUBMITTED);
            checkRequestHistory(request); //update request history again
        }
    }

    public ServiceRequest submitRequest(ServiceRequest request) throws ValidationException{
        if(canSubmitFromStatus.contains(request.getRequestStatus())){
            request.setRequestStatus(RequestStatus.SUBMITTED);
            return request;
        }
        //status not allowed to submit from
        throw new ValidationException("Cannot submit a request in {" + request.getRequestStatus() + "} status!");
    }

    public ServiceRequest receiveRequest(ServiceRequest request) {
        if(canReceiveRecallOrReturnFromStatus.contains(request.getRequestStatus())){
            request.setRequestStatus(RequestStatus.RECEIVED);
            return request;
        }
        //status not allowed to submit from
        throw new ValidationException("Cannot receive a request in {" + request.getRequestStatus() + "} status!");
    }

    public ServiceRequest recallRequest(ServiceRequest request) {
        if(canReceiveRecallOrReturnFromStatus.contains(request.getRequestStatus())){
            request.setRequestStatus(RequestStatus.RECALLED);
            return request;
        }
        //status not allowed to submit from
        throw new ValidationException("Cannot recall a request in {" + request.getRequestStatus() + "} status!");
    }

    public ServiceRequest returnRequest(ServiceRequest request) {
        if(canReceiveRecallOrReturnFromStatus.contains(request.getRequestStatus())){
            request.setRequestStatus(RequestStatus.RETURNED);
            return request;
        }
        //status not allowed to submit from
        throw new ValidationException("Cannot return a request in {" + request.getRequestStatus() + "} status!");
    }

    public ServiceRequest cancelRequest(ServiceRequest request) {
        //check if request is already billed
        if(request.getBilled() != null && request.getBilled() == true){
            throw new ValidationException("Cannot cancel a request that has been billed!");
        }
        //now check the status
        if(canSubmitFromStatus.contains(request.getRequestStatus())){
            request.setRequestStatus(RequestStatus.CANCELLED);
            return request;
        }
        //status not allowed to submit from
        throw new ValidationException("Cannot cancel a request in {" + request.getRequestStatus() + "} status!");

    }

    public ServiceRequest holdRequest(ServiceRequest request) {
        request.setRequestStatus(RequestStatus.ON_HOLD);
        return request;
    }

    public ServiceRequest completeRequest(ServiceRequest request) {
        request.setRequestStatus(RequestStatus.COMPLETE);
        return request;
    }

    public ServiceRequest beginProcessingRequest(ServiceRequest request) {
        request.setRequestStatus(RequestStatus.PROCESSING);
        return request;
    }
}
