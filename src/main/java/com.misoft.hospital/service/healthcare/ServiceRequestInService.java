package com.misoft.hospital.service.healthcare;

import corvid.model.common.ServiceRequestIn;

import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;
import java.util.logging.Logger;

/**
 * Created by kodero on 6/1/16.
 */
@Named
public class ServiceRequestInService {
    @Inject
    LabRequestService labRequestService;

    @Inject
    RadiologyRequestService radiologyRequestService;

    @Inject
    OperationRequestService operationRequestService;

    @Inject
    Logger log;

    @Transactional
    public void processIncomingServiceRequest(@Observes ServiceRequestIn serviceRequestIn){
        if ("LABD".equalsIgnoreCase(serviceRequestIn.getDeptCode())){
            labRequestService.processIncomingLabRequest(serviceRequestIn);
        }else if("RDLG".equalsIgnoreCase(serviceRequestIn.getDeptCode())){
            radiologyRequestService.processIncomingImagingRequest(serviceRequestIn);
        }else if("OPT".equalsIgnoreCase(serviceRequestIn.getDeptCode())){
            operationRequestService.processIncomingOperationRequest(serviceRequestIn);
        }else {
            log.info("-------------------Unknown Request found------------------" + serviceRequestIn.getDeptCode());
        }
    }
}
