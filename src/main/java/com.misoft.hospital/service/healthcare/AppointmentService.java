package com.misoft.hospital.service.healthcare;

import com.misoft.hospital.model.admin.User;
import com.misoft.hospital.model.healthcare.*;
import com.misoft.hospital.rest.BaseEntityService;
import com.misoft.hospital.rest.patientCare.TriagingResource;
import com.misoft.hospital.rest.util.SequenceManager;
import com.misoft.hospital.service.annotations.CurrentUser;
import corvid.model.common.AppointmentOut;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Period;

import javax.enterprise.event.Event;
import javax.enterprise.event.Observes;
import javax.enterprise.event.TransactionPhase;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by kodero on 1/13/16.
 */
@Named
public class AppointmentService {

    @Inject
    EntityManager em;

    @Inject
    @CurrentUser
    User user;

    @Inject
    SequenceManager sequenceManager;

    @Inject
    Event<BaseEntityService.EntityCreatedEvent<TriagingRequest>> triageEvent;

    @Inject
    Event<BaseEntityService.EntityCreatedEvent<ConsultationRequest>> consultationEvent;

    @Inject
    Logger log;

    @Inject
    private Event<AppointmentOut> appointmentOutEvent;

    public void onCreateAppointment(@Observes(during = TransactionPhase.BEFORE_COMPLETION) BaseEntityService.EntityCreatedEvent<Appointment> event){
        Appointment app = event.getEntity();
        checkAndCreateTriage(app);
        checkAndCreateAppointmentOut(app);
        checkAndCreateConsultation(app);
    }

    public void onUpdateAppointment(@Observes(during = TransactionPhase.BEFORE_COMPLETION) BaseEntityService.EntityUpdatedEvent<Appointment> event){
        Appointment app = event.getEntity();
        checkAndCreateTriage(app);
        checkAndCreateAppointmentOut(app);
        checkAndCreateConsultation(app);
    }

    private void checkAndCreateAppointmentOut(Appointment app){
        log.info("-------------------------Sending to JMS : " + app.isSentToJms() + "----------------------");
        try{
            if(!app.isSentToJms() && app.getConsultationType().getUnit().getCode().equalsIgnoreCase("EYEU")){
                //raise the event iff we are dealing with eye unit and its not processed
                Patient p = app.getPatient();
                int ageInDays = Days.daysBetween(new DateTime(p.getDob()), new DateTime()).getDays();
                String billPaymentType = "SELF".equalsIgnoreCase(app.getBillPaymentType() + "")? "SELF" : app.getBillPaymentType() + " - " + app.getInsuranceCompany().getName() + "";
                appointmentOutEvent.fire(new AppointmentOut(p.getPatientNo(), p.getSurname() + " " + p.getOtherNames(), ageInDays, p.getGender() + "", app.getAppointmentNo(), app.getDate(), app.getConsultationType().getUnit().getCode(), app.getComments(), billPaymentType));
                app.setSentToJms(true);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void checkAndCreateTriage(Appointment app) {
        log.info("Received event");
        //check if a triage already exists for this appointment
        if(app.getAutoCreateTriage()){
            try{
                Query q = em.createQuery("select NEW com.misoft.hospital.model.healthcare.TriagingRequest(t.id) from TriagingRequest t where t.appointment.id =:appointmentId and t.deleted <> 1")
                        .setParameter("appointmentId", app.getId());
                List<TriagingRequest> trs = q.getResultList();
                if(trs.size() == 0){
                    //we haven't created any, so do the necessary
                    log.info("Creating triaging request");
                    TriagingRequest tr = new TriagingRequest(app.getPatient(), new Date(), user.getStaff(), sequenceManager.nextSequence("SERVICE_REQUEST"), RequestSource.Internal, app, RequestStatus.NEW, true);
                    //set the clinical indications
                    Query q2 = em.createQuery("select NEW com.misoft.hospital.model.healthcare.ClinicalIndication(ci.id) from ClinicalIndication ci where ci.deleted <> 1", ClinicalIndication.class);
                    List<ClinicalIndication> indications = q2.getResultList();
                    for(ClinicalIndication ci : indications){
                        tr.addPatientClinicalIndication(new PatientClinicalIndication(ci));
                    }
                    em.persist(tr);
                    triageEvent.fire(new BaseEntityService.EntityCreatedEvent<>(tr));
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    private void checkAndCreateConsultation(Appointment app) {
        log.info("Received event");
        //check if a triage already exists for this appointment
        if(app.getAutoCreateTriage()){
            try{
                Query q = em.createQuery("select NEW com.misoft.hospital.model.healthcare.ConsultationRequest(c.id) from ConsultationRequest c where c.patient.id =:patientId and c.deleted <> 1")
                        .setParameter("patientId", app.getPatient().getId());
                List<TriagingRequest> trs = q.getResultList();
                if(trs.size() == 0){
                    //we haven't created any, so do the necessary
                    log.info("Creating triaging request");
                    ConsultationRequest cr = new ConsultationRequest(app.getPatient(), new Date(), user.getStaff(), sequenceManager.nextSequence("SERVICE_REQUEST"), RequestSource.Internal, app, RequestStatus.NEW, true, app.getConsultationType());
                    Query q2 = em.createQuery("select NEW com.misoft.hospital.model.healthcare.ClinicalIndication(ci.id) from ClinicalIndication ci where ci.deleted <> 1", ClinicalIndication.class);
                    List<ClinicalIndication> indications = q2.getResultList();
                    for(ClinicalIndication ci : indications){
                        cr.addPatientClinicalIndication(new PatientClinicalIndication(ci));
                    }
                    em.persist(cr);
                    consultationEvent.fire(new BaseEntityService.EntityCreatedEvent<>(cr));
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    public Appointment getAppointmentByPatientNumber(Long appointmentNo){
        List<Appointment> pList = em.createQuery("select new Appointment(id, billPaymentType) from Appointment a where a.appointmentNo =:appointmentNo and deleted <> 1")
                .setParameter("appointmentNo", appointmentNo)
                .getResultList();
        if(pList.size() > 0) return pList.get(0);
        return null;
    }
}
