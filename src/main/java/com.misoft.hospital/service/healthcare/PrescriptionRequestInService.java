package com.misoft.hospital.service.healthcare;

import com.misoft.hospital.model.healthcare.*;
import com.misoft.hospital.rest.BaseEntityService;
import com.misoft.hospital.rest.util.SequenceManager;
import com.misoft.hospital.service.grant.GService;
import com.misoft.hospital.service.setup.MedicamentService;
import com.misoft.hospital.service.setup.StaffService;
import corvid.model.common.PrescriptionRequestIn;

import javax.enterprise.event.Event;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.logging.Logger;

/**
 * Created by kodero on 5/17/16.
 */
public class PrescriptionRequestInService {
    @Inject
    SequenceManager seq;

    @Inject
    PatientService patientService;

    @Inject
    AppointmentService appointmentService;

    @Inject
    MedicamentService medicamentService;

    @Inject
    StaffService staffService;

    @Inject
    GService gService;

    @Inject
    protected Logger log;

    @Inject
    Event<BaseEntityService.EntityCreatedEvent<PrescriptionRequest>> event;

    //process incoming lab request
    @Transactional
    public void processIncomingPrescriptionRequest(@Observes PrescriptionRequestIn prescriptionRequestIn){
        //create a lab request based on the @labRequestIn received
        PrescriptionRequest prescriptionRequest = new PrescriptionRequest();
        prescriptionRequest.setPatient(patientService.getPatientByPatientNumber(prescriptionRequestIn.getPatientNo()));
        prescriptionRequest.setAppointment(appointmentService.getAppointmentByPatientNumber(prescriptionRequestIn.getAppointmentNo()));
        prescriptionRequest.setAutoSubmit(true);
        prescriptionRequest.setRequestSource(RequestSource.Internal);
        prescriptionRequest.setRequestStatus(RequestStatus.SUBMITTED);
        prescriptionRequest.setDateRequested(prescriptionRequestIn.getDateOfRequest());
        //prescriptionRequest.setServiceCenter(gService.find("89dbdea9-b78e-4cf2-a68d-10049bbe15da", ServiceCenter.class));
        prescriptionRequest.setConsultationRequest(gService.find("15a476a3-d604-4688-b9f7-ef68aef5c1d1", ConsultationRequest.class));
        prescriptionRequest.setRequestNo(seq.nextSequence("SERVICE_REQUEST"));
        prescriptionRequest.addPrescriptionRequestItem(new PrescriptionRequestItem(medicamentService.getLabTestTypeByCode(Long.parseLong(prescriptionRequestIn.getDrugCode())), 0, prescriptionRequestIn.getDuration()));
        prescriptionRequest.setRequester(staffService.getStaffByCode(prescriptionRequestIn.getStaffNo()));
        gService.persist(prescriptionRequest);
        event.fire(new BaseEntityService.EntityCreatedEvent<>(prescriptionRequest));
    }
}
