package com.misoft.hospital.service.healthcare;

import com.misoft.hospital.model.healthcare.*;
import com.misoft.hospital.model.setup.TestCase;
import com.misoft.hospital.rest.BaseEntityService;
import com.misoft.hospital.rest.util.SequenceManager;
import com.misoft.hospital.service.admin.UserService;
import com.misoft.hospital.service.grant.GService;
import com.misoft.hospital.service.setup.LabTestTypeService;
import corvid.model.common.LabItemResult;

import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by kodero on 2/16/16.
 */
@Named
public class LabRequestItemService {

    @Inject
    UserService userService;

    @Inject
    SequenceManager seq;

    @Inject
    PatientService patientService;

    @Inject
    AppointmentService appointmentService;

    @Inject
    LabTestTypeService labTestTypeService;

    @Inject
    GService gService;

    @Inject
    EntityManager em;

    @Inject
    protected Logger log;

    public void entityCreatedEvent(@Observes BaseEntityService.EntityCreatedEvent<LabRequestItem> event){
        log.info("========================== Lab request item has been created===========================" + event.getEntity());
        //fix the date requested
        checkItemHistory(event.getEntity());
        gService.edit(event.getEntity());
    }

    public void entityUpdatedEvent(@Observes BaseEntityService.EntityUpdatedEvent<LabRequestItem> event){
        log.info("========================== Lab request item has been updated===========================" + event.getEntity());
        checkItemHistory(event.getEntity());
    }

    @Transactional
    public void labItemResultReceivedEvent(@Observes LabItemResult event){
        log.info("============= Lab result received has been received, updating the lab request========" + event.getId());
        //get the result name == analyte
        String analyte = event.getAnalyte();
        LabRequestItem lri = getRequestItemByRequestNo(event.getRequestId());
        if(lri == null) log.log(Level.SEVERE, "The request Item {" + event.getRequestId() + "} was not found!!");
        //now get the test case
        TestCase tc = getTestCaseByNameAndTestType(event.getAnalyte(), lri.getLabTestType().getId());
        if(tc == null) tc = createTesCase(event, lri);
        //now create the result
        LabTestResult ltr = new LabTestResult();
        ltr.setTestCase(tc);
        ltr.setResult(event.getResult());
        ltr.setWarn(event.isWarn());
        ltr.setLabRequestItem(lri);
        gService.persist(ltr);
        log.info("============= Done processing lab item result {" + event.getRequestId() + "} =================");
    }

    private TestCase createTesCase(LabItemResult event, LabRequestItem lri) {
        TestCase tc = new TestCase();
        tc.setAnalyte(event.getAnalyte());
        tc.setUpperLimit(event.getUpperLimit());
        tc.setLabTestType(lri.getLabTestType());
        return gService.persist(tc);
    }

    public void checkItemHistory(LabRequestItem item){
        //update request status history
        if(item.getItemStatusHistories().size() == 0){
            //new request, insert the first one
            item.addItemStatusHistory(new ItemStatusHistory(item.getRequestItemStatus(), new Date(), userService.currentUser()));
        }else{
            //there are already revisions, so check if the current status is the same top revision status
            if(item.getRequestItemStatus() != (new ArrayList<>(item.getItemStatusHistories()).get(0).getStatus())){
                item.addItemStatusHistory(new ItemStatusHistory(item.getRequestItemStatus(), new Date(), userService.currentUser()));
            }
        }
        //optionally start the request
        LabRequest labRequest = item.getLabRequest();
        if(item.getRequestItemStatus() == LabRequestItemStatus.Sample_Received && labRequest.getRequestStatus() == RequestStatus.RECEIVED){
            //request is received, so bump it to in progress
            labRequest.setRequestStatus(RequestStatus.PROCESSING);
        }
        //check if all the items are done
        checkIfRequestIsComplete(item);
    }

    private void checkIfRequestIsComplete(LabRequestItem item) {
        LabRequest labRequest = item.getLabRequest();
        Boolean allFinished = true;
        for(LabRequestItem lri : labRequest.getLabRequestItems()){
            if(lri.getRequestItemStatus() != LabRequestItemStatus.Released){
                allFinished = false;
            }
        }
        if(allFinished) labRequest.setRequestStatus(RequestStatus.COMPLETE);
    }

    public LabRequestItem getRequestItemByRequestNo(String requestNo){
        List<LabRequestItem> list = em.createQuery("select lri from LabRequestItem lri where lri.itemRequestNo =:requestNo")
                .setParameter("requestNo", requestNo)
                .getResultList();
        if(list.size() == 1) return list.get(0);
        return null;
    }

    public TestCase getTestCaseByNameAndTestType(String analyte, String labTestTypeId){
        List<TestCase> list = em.createQuery("select tc from TestCase tc where tc.analyte =:analyte and tc.labTestType.id =:labTestTypeId")
                .setParameter("analyte", analyte)
                .setParameter("labTestTypeId", labTestTypeId)
                .getResultList();
        if(list.size() == 1) return list.get(0);
        return null;
    }
}
