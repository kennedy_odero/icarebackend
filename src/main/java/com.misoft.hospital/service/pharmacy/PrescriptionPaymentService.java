package com.misoft.hospital.service.pharmacy;

import com.misoft.hospital.model.billing.BillItem;
import com.misoft.hospital.model.billing.BillPayment;
import com.misoft.hospital.model.billing.PatientPayment;
import com.misoft.hospital.model.healthcare.ServiceCenter;
import com.misoft.hospital.model.setup.Product;
import com.misoft.hospital.model.setup.ProductType;
import com.misoft.hospital.model.stores.InventoryUpdateEvent;
import com.misoft.hospital.rest.BaseEntityService;
import com.misoft.hospital.service.grant.GService;

import javax.enterprise.event.Event;
import javax.enterprise.event.Observes;
import javax.enterprise.event.TransactionPhase;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.logging.Logger;

/**
 * Created by kodero on 8/6/16.
 */
@Named
public class PrescriptionPaymentService {

    @Inject
    Logger log;

    @Inject
    GService gService;

    @Inject
    Event<InventoryUpdateEvent> inventoryUpdateEventEvent;

    public void onPatientBillCreated(@Observes(during = TransactionPhase.BEFORE_COMPLETION) BaseEntityService.EntityCreatedEvent<PatientPayment> event){
        log.info("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%Patient Payment Created!" + event.getEntity().getPatient().getPatientNo() + ", Checking inventory update event, size : " + event.getEntity().getBillPayments().size());
        try{
            for(BillPayment bp : event.getEntity().getBillPayments()){
                log.info("Processing #" + bp.getPatientBill().getBillNo() + ", balance : " + bp.getPatientBill().getBalance());
                if(bp.getPatientBill().getBalance().compareTo(event.getEntity().getAmountPaid()) <= 0){
                    log.info("#" + bp.getPatientBill().getBillNo() + " bill has been cleared, update inventory, balance : " + bp.getPatientBill().getBalance());
                    //bill cleared
                    Collection<Product> products = new ArrayList<>();
                    //PatientBill pb = gService.find(bp.getPatientBill().getId(), PatientBill.class);
                    for(BillItem bi : bp.getPatientBill().getBillItems()){
                        if(bi.getProduct().getProductType() == ProductType.GOODS){
                            //paid for a good, fire inventory update
                            products.add(bi.getProduct());
                        }
                        //update the status to paid
                        bi.setPaid(true);
                        gService.edit(bi);
                    }
                    //create inventory update event
                    ServiceCenter svc = gService.find("f6702a9c-d480-4b5f-a18d-eded29f36230", ServiceCenter.class);
                    InventoryUpdateEvent iue = new InventoryUpdateEvent(new Date(), Arrays.asList(svc), null, products);
                    //inventoryUpdateEventEvent.fire(iue);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
