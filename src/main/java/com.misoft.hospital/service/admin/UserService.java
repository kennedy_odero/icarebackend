
package com.misoft.hospital.service.admin;

import com.misoft.hospital.data.admin.UserProfileRepository;
import com.misoft.hospital.data.admin.UserRepository;
import com.misoft.hospital.model.admin.*;
import com.misoft.hospital.rest.admin.UserResource;
import com.misoft.hospital.service.grant.GService;
import com.misoft.hospital.service.annotations.CurrentUser;
import com.misoft.hospital.util.TokenGenerator;

import javax.ejb.Stateful;
import javax.enterprise.context.ConversationScoped;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.util.Date;
import java.util.logging.Logger;


@SuppressWarnings("CdiUnproxyableBeanTypesInspection")
@Stateful
@ConversationScoped
public class UserService implements Serializable {

    @Inject
    private Logger log;

    @SuppressWarnings("CdiUnproxyableBeanTypesInspection")
    @Inject
    private transient EntityManager em;

    @Inject
    private HttpServletRequest req;

    @Inject
    private UserRepository userRepository;

    @Inject
    private GService gService;

    @Inject
    private UserProfileRepository userProfileRepository;

    @Inject
    private UserResource userResource;

    public User registerNewUser(User user) throws Exception {
        log.info("Registering " + user.getEmail());
        final User createdUser = gService.makePersistent(user);
        return createdUser;
    }

    public User updateUserLogin(User user, String ip, String userAgent, String referer, LoginAttemptResult result) {
        if(user.getPersistenceToken() == null){
            //generate new token
            user.setPersistenceToken(generateToken());
        }
        UserLogin ul = new UserLogin(new Date(), user, ip, userAgent, referer, result);
        em.merge(ul);
        em.merge(user);
        return user;
    }

    public void logFailedLoginAttempt(User user, String ip, String userAgent, String referer, LoginAttemptResult result){
        UserLogin ul = new UserLogin(new Date(), user, ip, userAgent, referer, result);
        user.setFailedLoginCount(user.getFailedLoginCount() == null ? 1 : user.getFailedLoginCount() + 1);
        //check if we need to lock the user account
        if(user.getFailedLoginCount() == 3) user.setAccountStatus(AccountStatus.Locked);
        em.merge(user); em.merge(ul);
    }

    private String generateToken() {
        return new TokenGenerator().nextSessionId();
    }

    private void setRolesAndAccessLevels(User user) {
        //TODO
        log.info("setting the access levels for user ' " + user.getEmail() + " '");
    }


    @Produces@Named("currentUser")
    @CurrentUser
    @RequestScoped
    public User currentUser() {
        final String token = req.getHeader(User.TOKEN);
        log.info("the user token from the request '" + token + "'");
        User currentUser = userRepository.findUserByToken(token);
        return currentUser;
    }

    public boolean isLoggedIn(final String token) throws Exception {
        log.info("check if user is logged in ");
        User user = userRepository.findUserByToken(token);
        //TODO, to softRemove
        req.setAttribute("currentUser", user);
        return user != null;
    }


    public User addUserProfile(User user, String profileName) throws Exception {
        final UserProfile userProfile = userProfileRepository.findUserProfile(profileName);
        user.setUserProfile(userProfile);
        gService.makePersistent(userProfile);
        final User updatedUser = gService.makePersistent(user);
        return updatedUser;
    }
}
