package com.misoft.hospital.service.admin;

import com.misoft.hospital.data.admin.UserRepository;
import com.misoft.hospital.model.admin.User;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import java.util.logging.Logger;

/**
 * Created by kodero on 8/2/15.
 */
@RequestScoped
@Named("currentUserService")
public class CurrentUserService {

    @Inject
    private Logger log;

    @Inject
    private HttpServletRequest req;

    @Inject
    private UserRepository userRepository;

    private User currentUser;

    public User currentUser() {
        if(currentUser == null){
            final String token = req.getHeader(User.TOKEN);
            log.info("the user token from the request '" + token + "'");
            currentUser = userRepository.findUserByToken(token);
        }
        return currentUser;
    }
}
