package com.misoft.hospital.service.admin;

import com.misoft.hospital.data.admin.PermissionRepository;
import com.misoft.hospital.data.admin.UserRepository;
import com.misoft.hospital.model.admin.Permission;
import com.misoft.hospital.service.grant.GService;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.event.Reception;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.logging.Logger;


@RequestScoped
public class PermissionService {

    @Inject
    private Logger log;

    @Inject
    private EntityManager em;

    @Inject
    private PermissionRepository repository;

    @Inject
    private UserRepository userRepository;


    @Inject
    GService gService;

    public void onCreated(@Observes(notifyObserver = Reception.ALWAYS) final Permission createdPermission) {
        log.info("event received after creation.... " + createdPermission.getId());
        //create the user_permission(user_id,createdPermission,false) entries for all users
        gService.updateUserPermissions(createdPermission);
    }
}
