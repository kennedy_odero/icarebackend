package com.misoft.hospital.service.admin;

import com.misoft.hospital.model.admin.Config;

import javax.ejb.Stateful;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.List;

/**
 * Created by kodero on 12/30/14.
 */
@RequestScoped
@Stateful
public class ConfigService implements Serializable {

    @Inject
    private EntityManager entityManager;

    public Config getCurrentConfig(){
        List<Config> l = entityManager.createQuery("from Config c").getResultList();
        if(l.size() == 1) return l.get(0);
        return null;
    }
}
