package com.misoft.hospital.service.integration;

import com.misoft.hospital.data.gdto.types.BooleanType;
import com.misoft.hospital.data.gdto.types.StringType;
import com.misoft.hospital.model.healthcare.LabRequest;
import com.misoft.hospital.model.healthcare.LabRequestItem;
import com.misoft.hospital.rest.BaseEntityService;
import com.misoft.hospital.service.preferences.PreferenceService;
import corvid.model.common.LabRequestItemCreated;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Period;

import javax.enterprise.event.*;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.Date;
import java.util.logging.Logger;

/**
 * Created by kodero on 4/3/16.
 */
@Named
public class LimsService {

    @Inject
    private Logger log;

    @Inject
    private PreferenceService preferenceService;

    @Inject
    Event<LabRequestItemCreated> event;

    public void onNewLabRequest(@Observes BaseEntityService.EntityCreatedEvent<LabRequest> event){
        log.info("===new Lab request created on " + new Date());
        checkAndCreateLimsRequest(event.getEntity());
    }

    public void onNewLabRequest(@Observes BaseEntityService.EntityUpdatedEvent<LabRequest> event){
        log.info("===new Lab request updated on " + new Date());
        checkAndCreateLimsRequest(event.getEntity());
    }

    public void checkAndCreateLimsRequest(LabRequest labRequest) {
        Boolean limsEnabled = preferenceService.getPreferenceValue("integration.lims.enabled", new BooleanType());
        if(limsEnabled){
            log.info("========================Creating LIMS request on : " + new Date() + " ===============================");
            //create lims request
            String limsDriverClass = preferenceService.getPreferenceValue("integration.lims.driverClass", new StringType());
            String limsUsername = preferenceService.getPreferenceValue("integration.lims.username", new StringType());
            String limsPassword = preferenceService.getPreferenceValue("integration.lims.password", new StringType());
            String limsOutputTable = preferenceService.getPreferenceValue("integration.lims.outputTable", new StringType());

            log.info("=== Parameters for creating the request==========");
            log.info("=== integration.lims.driverClass  ==========" + limsDriverClass);
            log.info("=== integration.lims.username  ==========" + limsUsername);
            log.info("=== integration.lims.password  ==========" + limsPassword);
            log.info("=== integration.lims.outputTable  ==========" + limsOutputTable);
            log.info("=== item.getLabRequest().getPatient().getDob()  ==========" + labRequest.getPatient().getDob());

            log.info("========================Pushing created lab requests to JMS queue===============================");
            for (LabRequestItem item : labRequest.getLabRequestItems()){
                log.info("item.getLabTestType().getProduct() : " + item.getLabTestType().getProduct());
                String requestor = item.getLabRequest().getRequester() ==null? "" : (item.getLabRequest().getRequester().getSurname() + " " + item.getLabRequest().getRequester().getOtherNames());
                LabRequestItemCreated lric = new LabRequestItemCreated(item.getItemRequestNo(),
                        item.getLabRequest().getPatient().getPatientNo(),
                        item.getLabRequest().getPatient().getSurname() + " " + item.getLabRequest().getPatient().getOtherNames(),
                        item.getLabRequest().getPatient().getGender() + "",
                        Days.daysBetween(new DateTime(item.getLabRequest().getPatient().getDob()), new DateTime(new Date())).getDays(),
                        item.getLabRequest().getDateRequested(),
                        item.getLabTestType().getProduct().getProductCode() + "",
                        item.getLabTestType().getName(),
                        item.getLabTestType().getProduct().getListPrice(),
                        requestor,
                        item.getLabRequest().getAppointment().getBillPaymentType().name(),
                        item.getTestSample() == null? "-" : item.getTestSample().getName(),
                        item.getComments());
                event.fire(lric);
            }
        }
    }
}
