package com.misoft.hospital.service.integration;

import corvid.model.common.*;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.cdi.CdiEventEndpoint;
import org.apache.camel.component.jms.JmsComponent;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.jms.ConnectionFactory;

/**
 * Created by kodero on 5/13/16.
 */
public class DefaultRoutes extends RouteBuilder{
    @Inject
    CdiEventEndpoint<LabRequestItemCreated> labRequestItemCreatedEvent;

    @Inject
    CdiEventEndpoint<LabItemResult> labResultReceivedCdiEvent;

    @Inject
    CdiEventEndpoint<AppointmentOut> appointmentOutEvent;

    @Resource(mappedName = "java:jboss/DefaultJMSConnectionFactory")
    ConnectionFactory connectionFactory;

    @Inject
    CdiEventEndpoint<ServiceRequestIn> serviceRequestInEvent;

    @Inject
    CdiEventEndpoint<PrescriptionRequestIn> prescriptionRequestInEvent;

    @Override
    public void configure() throws Exception {
        JmsComponent component = new JmsComponent();
        component.setConnectionFactory(connectionFactory);
        getContext().addComponent("jms", component);

        //outgoing lab requests
        from(labRequestItemCreatedEvent)
                .log("$$$$$$$$$$$$$$$$$$$ CDI event received: ${body} $$$$$$$$$$$$$$$$$$$")
                .to("jms:LabRequestOutQ")
                .log("$$$$$$$$$$$$$$$$$$$ JMS Message sent $$$$$$$$$$$$$$$$$$$");

        //incoming lab results
        from("jms:LabResultInQ")
                .log("//////////////////////// Lab result received: ${body} /////////////////")
                .to(labResultReceivedCdiEvent)
                .log("//////////////////////// Raised CDI event for ${body} /////////////////////");

        //appointments out
        from(appointmentOutEvent)
                .log("$$$$$$$$$$$$$$$$$$$ Appointment Out CDI event received: ${body} $$$$$$$$$$$$$$$$$$$")
                .to("jms:AppointmentOutQ")
                .log("//////////////////////// Sent Appointment Out to JMS endpoint ${body} /////////////////////");

        //incoming lab requests
        from("jms:ServiceRequestInQ")
                .log("//////////////////////// Service request in received: ${body} /////////////////")
                .to(serviceRequestInEvent)
                .log("//////////////////////// Raised CDI event service request in ${body} /////////////////////");

        //incoming lab requests
        from("jms:PrescriptionRequestInQ")
                .log("//////////////////////// Prescription request in received: ${body} /////////////////")
                .to(prescriptionRequestInEvent)
                .log("//////////////////////// Raised CDI event prescription request in ${body} /////////////////////");
    }
}
