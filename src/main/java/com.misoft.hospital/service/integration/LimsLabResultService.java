package com.misoft.hospital.service.integration;

import corvid.model.common.LabItemResult;
import corvid.model.common.LabRequestItemCreated;
import org.apache.camel.Exchange;

import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.transaction.Transactional;

/**
 * Created by kodero on 5/15/16.
 */
@Named("limsLabResultService")
public class LimsLabResultService {

    @PersistenceContext(unitName = "primary", type = PersistenceContextType.EXTENDED)
    EntityManager em;

    @Inject
    Event<LabResultReceived> event;

    @Transactional
    public void processResult(Exchange oldExchange, Exchange newExchange) throws Exception {
        try {
            LabItemResult labRequestItem = oldExchange.getIn().getBody(LabItemResult.class);
            event.fire(new LabResultReceived(labRequestItem));
        } catch (Exception e) {
            throw new Exception("There was a problem saving the lab request : ", e);
        }
    }
}
