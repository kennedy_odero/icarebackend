package com.misoft.hospital.service.integration;

import com.misoft.hospital.model.healthcare.LabRequestItem;
import corvid.model.common.LabItemResult;

/**
 * Created by kodero on 5/15/16.
 */
public class LabResultReceived {
    private LabItemResult labItemResult;

    public LabResultReceived(LabItemResult labItemResult){
        setLabItemResult(labItemResult);
    }

    public LabItemResult getLabItemResult() {
        return labItemResult;
    }

    public void setLabItemResult(LabItemResult labItemResult) {
        this.labItemResult = labItemResult;
    }
}
