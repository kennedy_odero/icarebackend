package com.misoft.hospital.service.accounts;

import com.misoft.hospital.model.accounts.AccountingPeriod;
import com.misoft.hospital.model.accounts.PeriodType;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.Date;
import java.util.List;

/**
 * Created by kodero on 1/2/15.
 */
@Stateless
public class AccountingPeriodService {
    @Inject
    private EntityManager entityManager;

    public AccountingPeriod getPeriod(Date apDate, PeriodType type){
        List<AccountingPeriod> aps = entityManager.createQuery("from AccountingPeriod ap where :apDate between ap.fromDate and ap.toDate and ap.periodType =:apType")
                .setParameter("apDate", apDate)
                .setParameter("apType", type)
                .getResultList();
        if(aps.size() == 1) return aps.get(0);
        return null;
    }
}
