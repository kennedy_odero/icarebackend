package com.misoft.hospital.service.callbacks;

import com.misoft.hospital.model.healthcare.ConsultationDiagnosis;
import com.misoft.hospital.util.callbacks.EntityCallbackMethod;
import org.joda.time.DateTime;
import org.joda.time.Years;

import javax.inject.Named;

/**
 * Created by kodero on 8/1/16.
 */
@Named
public class ConsultationDiagnosisCallback {

    @EntityCallbackMethod
    public void setDiagnosisAge(ConsultationDiagnosis consultationDiagnosis){
        int age = Years.yearsBetween(new DateTime(consultationDiagnosis.getConsultationRequest().getPatient().getDob()), new DateTime()).getYears();
        consultationDiagnosis.setAge(age);
        if(age <= 5) consultationDiagnosis.setAgeGroup("UNDER5");
        else consultationDiagnosis.setAgeGroup("OVER5");
    }
}
