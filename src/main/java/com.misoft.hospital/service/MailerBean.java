package com.misoft.hospital.service;

import javax.annotation.Resource;
import javax.ejb.AsyncResult;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.text.DateFormat;
import java.util.Date;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;


@Stateless
public class MailerBean {


    @Resource(name = "java:/LemrSMTP")
    private Session mailSession;

    private final static Logger logger = Logger.getLogger(MailerBean.class.getName());

    @Asynchronous
    public Future<String> sendConfirmRegistrationEmail(String email, String emailAddress) {
        String status;
        try {
            Message message = new MimeMessage(mailSession);
            message.setFrom(InternetAddress.parse("mokua83ke@gmail.com", false)[0]);
            //set From name
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emailAddress, false));
            //set TO name
            message.setSubject("[Lemr] Registration Confirmation");
            message.setHeader("X-Mailer", "JavaMail");
            DateFormat dateFormatter = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.SHORT);
            Date timeStamp = new Date();
            String messageBody = email;
            message.setContent(messageBody, "text/html");
            message.setSentDate(timeStamp);
            Transport.send(message);
            status = "Sent";
            logger.log(Level.INFO, "Mail sent to {0}", emailAddress);
        } catch (MessagingException ex) {
            logger.severe("Error in sending message.");
            status = "Encountered an error";
            logger.severe(ex.getMessage() + ex.getNextException().getMessage());
            logger.severe(ex.getCause().getMessage());
        }
        return new AsyncResult<>(status);
    }

    @Asynchronous
    public Future<String> sendRequestNotification(String email, String emailAddress) {
        String status;
        try {
            Message message = new MimeMessage(mailSession);
            message.setFrom(InternetAddress.parse("mokua83ke@gmail.com", false)[0]);
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emailAddress, false));
            message.setSubject("Request Application from Lemr");
            message.setHeader("X-Mailer", "JavaMail");
            DateFormat dateFormatter = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.SHORT);
            Date timeStamp = new Date();
            String messageBody = email
                    + dateFormatter.format(timeStamp)
                    + ".";
            message.setText(messageBody);
            message.setSentDate(timeStamp);
            Transport.send(message);
            status = "Sent";
            logger.log(Level.INFO, "Mail sent to {0}", email);
        } catch (MessagingException ex) {
            logger.severe("Error in sending message.");
            status = "Encountered an error";
            logger.severe(ex.getMessage() + ex.getNextException().getMessage());
            logger.severe(ex.getCause().getMessage());
        }
        return new AsyncResult<>(status);
    }

    @Asynchronous
    public Future<String> sendLoiApplicationNotification(String email, String emailAddress) {
        String status;
        try {
            Message message = new MimeMessage(mailSession);
            message.setFrom(InternetAddress.parse("mokua83ke@gmail.com", false)[0]);
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emailAddress, false));
            message.setSubject("Test message from Lemr");
            message.setHeader("X-Mailer", "JavaMail");
            DateFormat dateFormatter = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.SHORT);
            Date timeStamp = new Date();
            String messageBody = email
                    + dateFormatter.format(timeStamp)
                    + ".";
            message.setText(messageBody);
            message.setSentDate(timeStamp);
            Transport.send(message);
            status = "Sent";
            logger.log(Level.INFO, "Mail sent to {0}", email);
        } catch (MessagingException ex) {
            logger.severe("Error in sending message.");
            status = "Encountered an error";
            logger.severe(ex.getMessage() + ex.getNextException().getMessage());
            logger.severe(ex.getCause().getMessage());
        }
        return new AsyncResult<>(status);
    }


    public Future<String> sendRegistrationCongratulationsEmail(String email, String emailAddress) {
        String status;
        try {
            Message message = new MimeMessage(mailSession);
            message.setFrom(InternetAddress.parse("mokua83ke@gmail.com", false)[0]);
            //set From name
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emailAddress, false));
            //set TO name
            message.setSubject("[Lemr] Your email was confirmed for application Lemr"); //TODO hard-coded
            message.setHeader("X-Mailer", "JavaMail");
            Date timeStamp = new Date();
            String messageBody = email;
            message.setContent(messageBody, "text/html");
            message.setSentDate(timeStamp);
            Transport.send(message);
            status = "Sent";
            logger.log(Level.INFO, "Mail sent to {0}", emailAddress);
        } catch (MessagingException ex) {
            logger.severe("Error in sending message.");
            status = "Encountered an error";
            logger.severe(ex.getMessage() + ex.getNextException().getMessage());
            logger.severe(ex.getCause().getMessage());
        }
        return new AsyncResult<>(status);
    }

    public Future<String> sendPasswordResetEmail(String email, String emailAddress) {
        String status;
        try {
            Message message = new MimeMessage(mailSession);
            message.setFrom(InternetAddress.parse("mokua83ke@gmail.com", false)[0]);
            //set From name
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emailAddress, false));
            //set TO name
            message.setSubject("[Lemr] Password Reset Request"); //TODO hard-coded
            message.setHeader("X-Mailer", "JavaMail");
            Date timeStamp = new Date();
            String messageBody = email;
            message.setContent(messageBody, "text/html");
            message.setSentDate(timeStamp);
            Transport.send(message);
            status = "Sent";
            logger.log(Level.INFO, "Mail sent to {0}", emailAddress);
        } catch (MessagingException ex) {
            logger.severe("Error in sending message.");
            status = "Encountered an error";
            logger.severe(ex.getMessage() + ex.getNextException().getMessage());
            logger.severe(ex.getCause().getMessage());
        }
        return new AsyncResult<>(status);
    }

    public Future<String> sendPasswordResetCongratulationsEmail(String email, String emailAddress) {
        String status;
        try {
            Message message = new MimeMessage(mailSession);
            message.setFrom(InternetAddress.parse("mokua83ke@gmail.com", false)[0]);
            //set From name
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emailAddress, false));
            //set TO name
            message.setSubject("[Lemr] Your Password was Reset for application Lemr"); //TODO hard-coded
            message.setHeader("X-Mailer", "JavaMail");
            Date timeStamp = new Date();
            String messageBody = email;
            message.setContent(messageBody, "text/html");
            message.setSentDate(timeStamp);
            Transport.send(message);
            status = "Sent";
            logger.log(Level.INFO, "Mail sent to {0}", emailAddress);
        } catch (MessagingException ex) {
            logger.severe("Error in sending message.");
            status = "Encountered an error";
            logger.severe(ex.getMessage() + ex.getNextException().getMessage());
            logger.severe(ex.getCause().getMessage());
        }
        return new AsyncResult<>(status);
    }
}