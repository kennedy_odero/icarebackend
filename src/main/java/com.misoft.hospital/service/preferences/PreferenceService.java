package com.misoft.hospital.service.preferences;

import com.misoft.hospital.data.gdto.types.AbstractType;
import com.misoft.hospital.model.preferences.PreferenceValue;
import com.misoft.hospital.rest.BaseEntityService;

import javax.annotation.PostConstruct;
import javax.ejb.Startup;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;

/**
 * Created by kodero on 4/3/16.
 */
@ApplicationScoped
@Startup
public class PreferenceService {

    @Inject
    private EntityManager em;

    @Inject
    private Logger log;

    private static final ConcurrentHashMap<String, PreferenceValue> preferences = new ConcurrentHashMap<>();

    @PostConstruct
    public void initPreferences() {
        log.info("===============Preloading preferences===========================");
        List<PreferenceValue> prefs = em.createQuery("select pv from PreferenceValue pv").getResultList();
        for(PreferenceValue pv : prefs){
            preferences.put(pv.getKey(), pv);
        }
    }

    public <S, T extends AbstractType<S>> S getPreferenceValue(String prefCode, T t){
        /*List<PreferenceValue> prefs = em.createNamedQuery("getPreferenceByCode").setParameter("prefCode", prefCode).getResultList();
        if (prefs.size() == 0) return null;*/
        if(preferences.containsKey(prefCode)) return preferences.get(prefCode).getTypedValue(t);
        return null;
    }

    public void onPreferenceSave(@Observes BaseEntityService.EntityUpdatedEvent<PreferenceValue> event){
        log.info("===============Preference value updated : ===========================" + event.getEntity().getPreferenceDefinition().getKey());
        preferences.put(event.getEntity().getPreferenceDefinition().getKey(), event.getEntity());
        /*try {
            String defaultDepartmentId = new ObjectMapper().readTree(new StringReader(getPreferenceValue("default.department", new StringType()))).get("attributes").get("id").textValue();
            log.info("===============Default department : ===========================" + defaultDepartmentId);
        } catch (IOException e) {
            e.printStackTrace();
        }*/
    }
}
