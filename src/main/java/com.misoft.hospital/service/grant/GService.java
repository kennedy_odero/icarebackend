
package com.misoft.hospital.service.grant;

import com.misoft.hospital.model.admin.Permission;
import com.misoft.hospital.model.admin.User;
import com.misoft.hospital.model.admin.UserPermission;
import com.misoft.hospital.model.base.ModelBase;
import com.misoft.hospital.rest.admin.UserPermissionRequest;
import org.hibernate.CacheMode;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;

import javax.ejb.AsyncResult;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Query;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Future;
import java.util.logging.Logger;


@Stateless
public class GService implements Serializable {

    private static final int HIBERNATE_BATCH_SIZE = 10;

    @Inject
    private Logger log;

    @Inject
    private EntityManager em;

    public <T> T makePersistent(T entity) throws Exception {
        final T merge = em.merge(entity);
        em.flush();
        return merge;
    }

    public <T> T persist(T entity) {
        em.persist(entity);
        em.flush();
        return entity;
    }

    public <T> T edit(T entity) {
        T t = getEntityManager().merge(entity);
        return t;
    }

    public <T> void remove(T entity) {
        getEntityManager().remove(getEntityManager().merge(entity));
    }

    public <T extends ModelBase> void softRemove(T entity) {
        entity.setDeleted(true);
        entity.setDeletedAt(new Date());
        getEntityManager().merge(getEntityManager().merge(entity));
    }

    public <T extends ModelBase> void softRemove(String id, Class<T> entityClass) {
        T entity = find(id, entityClass);
        if (entity == null) {
            throw new EntityNotFoundException(" Entity with id " + id + " does not exist in the database");
        }
        log.info(" the entity to be deleted , id " + entity);
        entity.setDeleted(true);
        entity.setDeletedAt(new Date());
        getEntityManager().merge(entity);
    }

    public <T> void remove(String id, Class<T> entityClass) {
        T entity = find(id, entityClass);
        if (entity == null) {
            throw new EntityNotFoundException(" Entity with id " + id + " does not exist in the database");
        }
        log.info(" the entity to be deleted , id " + entity);
        getEntityManager().remove(entity);
    }

    public <T> T find(Object id, Class<T> entityClass) {
        return getEntityManager().find(entityClass, id);
    }


    public <T> List<T> findAll(Class entityClass) {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        return getEntityManager().createQuery(cq).getResultList();
    }


    public void executeQuery(UserPermissionRequest request, String sql) {
        int result = em.createNativeQuery(sql)
                .setParameter(1, request.getPermissionId().longValue())
                .setParameter(2, request.getUserId().longValue())
                .setParameter(3, request.getStatus())
                .executeUpdate();
        em.flush();
    }

    /**
     * TODO testing
     * Asynchronously create user-permissions for all the users in the system.
     *
     * @param createdPermission
     * @return
     */
    @Asynchronous
    public Future<String> updateUserPermissions(Permission createdPermission) {
        String status = "";
        Session session = (Session) em.getDelegate();
        ScrollableResults users = session.createQuery("select u from User u")
                .setMaxResults(100)//BATCH_SIZE
                .setCacheMode(CacheMode.IGNORE)
                .setReadOnly(true)
                .scroll(ScrollMode.FORWARD_ONLY);

        int rowsToFlush = 0;
        int totalRows = 0;
        while (users.next()) {
            User currentUser = (User) users.get(0);
            final UserPermission userPermission = new UserPermission(Boolean.FALSE, currentUser, createdPermission);

            session.save(userPermission);
            session.flush();

            ++rowsToFlush;
            ++totalRows;
            if (rowsToFlush == HIBERNATE_BATCH_SIZE) {
                //flush a batch of updates and release memory:
                session.flush();
                session.clear();
                rowsToFlush = 0;
                log.info("Just flushed " + HIBERNATE_BATCH_SIZE + " rows to the database.");
                log.info("Total rows flushed is " + totalRows);
            }

        }

        return new AsyncResult<>(status);
    }

    public int updateUserPermissionStatus(UserPermission userPermission, Boolean status) {
        Query q = em.createQuery("UPDATE UserPermission u SET u.status = :status " +
                "WHERE u.id =:id");
        q.setParameter("status", status);
        q.setParameter("id", userPermission.getId());
        int updated = q.executeUpdate();
        return updated;
    }

    protected EntityManager getEntityManager() {
        return em;
    }
}
