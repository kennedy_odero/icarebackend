package com.misoft.hospital.service.billing;

import com.misoft.hospital.model.billing.*;
import com.misoft.hospital.model.events.AdmissionBedStart;
import com.misoft.hospital.model.events.AdmissionBedStop;
import com.misoft.hospital.model.events.BatchBillingEvent;
import com.misoft.hospital.model.healthcare.*;
import com.misoft.hospital.model.mortuary.MortuaryStorageUnit;
import com.misoft.hospital.model.setup.Bed;
import com.misoft.hospital.model.setup.BedStatus;
import com.misoft.hospital.model.setup.Product;
import com.misoft.hospital.rest.BaseEntityService;
import com.misoft.hospital.rest.mortuary.MortuaryStorageUnitStart;
import com.misoft.hospital.rest.mortuary.MortuaryStorageUnitStop;
import com.misoft.hospital.rest.util.SequenceManager;
import com.misoft.hospital.service.grant.GService;
import com.misoft.hospital.service.integration.LimsService;
import com.misoft.hospital.service.preferences.PreferenceService;
import org.joda.time.DateTime;

import javax.enterprise.event.Event;
import javax.enterprise.event.Observes;
import javax.enterprise.event.TransactionPhase;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by kodero on 1/17/16.
 */
@Named
public class BillingService<T extends ServiceRequest & BillableRequest> {

    private static final String BILL_ITEM_SEQUENCE_NAME = "BILL_ITEM";
    @Inject
    EntityManager em;

    @Inject
    private SequenceManager seq;

    @Inject
    Logger log;

    @Inject
    private PreferenceService preferenceService;

    @Inject
    GService gService;

    @Inject
    Event<BaseEntityService.EntityCreatedEvent<PatientBill>> billCreated;

    @Inject
    Event<BaseEntityService.EntityUpdatedEvent<PatientBill>> billUpdated;

    @Inject
    Event<AdmissionBedStart> admissionBedStartEvent;

    @Inject
    Event<AdmissionBedStop> admissionBedStopEvent;

    @Inject
    LimsService limsService;

    //listen to request submit
    public void onNewRequestSubmit(@Observes(during = TransactionPhase.BEFORE_COMPLETION) BaseEntityService.EntityCreatedEvent<T> event){
        processRequest(event.getEntity());
    }

    public void onUpdatedRequestSubmit(@Observes(during = TransactionPhase.BEFORE_COMPLETION) BaseEntityService.EntityUpdatedEvent<T> event){
        processRequest(event.getEntity());
    }

    public void onBatchBillingEvent(@Observes BatchBillingEvent event){
        log.info("--Batch billing event noted---");
        event.getRequests().forEach(sr -> processRequest((T)sr));
        //manually fire Lab request integration
        event.getRequests().forEach(sr -> {
            log.info("-------------------------Checking if we have a Lab request-------------------------");
            if(sr instanceof LabRequest){
                log.info("---------------------Found a Lab request---------------------------------------");
                limsService.checkAndCreateLimsRequest((LabRequest)sr);
            }
        });
    }

    public void onCreateAppointment(@Observes(during = TransactionPhase.BEFORE_COMPLETION) BaseEntityService.EntityCreatedEvent<Appointment> event){
        //bill the activation
        Appointment appointment = event.getEntity();
        //get the patient charge
        if(appointment.getConsultationType().getProduct() != null){
            Product product = appointment.getConsultationType().getProduct();
            if(BigDecimal.ZERO.compareTo(product.getListPrice()) != 0 ){ //only update bill if the product has a non-zero value;
                try{
                    PatientBill currentBill = getOrCreateBillingForAppointment(appointment);
                    //save the bill
                    log.info("=====================Updating patient bill #" + currentBill.getBillNo() + "============================Patient charge total :" + product.getListPrice());
                    BillItem billItem = new BillItem();
                    //if(charge.getBillItem() != null) billItem = charge.getBillItem();//just revise the amount billed if it was already billed
                    //charge.setBillItem(billItem);
                    billItem.setBillItemStatus(BillItemStatus.BILLED);
                    billItem.setProduct(product);
                    billItem.setQuantity(new BigDecimal(1));
                    billItem.setUnitPrice(product.getListPrice());
                    billItem.setLineTotal(product.getListPrice().multiply(billItem.getQuantity()));
                    billItem.setTax(BigDecimal.ZERO);
                    billItem.setBilledFrom(BilledFrom.PATIENT_CHARGE);
                    billItem.setBillNo(System.currentTimeMillis());
                    billItem.setBillItemType(BillItemType.Normal);
                    //billItem.setServiceCenter(appointment.getConsultationType().getServiceCenter());
                    if(billItem.getId() == null) currentBill.addBillItem(em.merge(billItem));//for new bill items
                    else billItem = em.merge(billItem);
                    currentBill.recomputeTotal();
                    gService.edit(currentBill);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
    }

    public void onCreatePatientCharge(@Observes(during = TransactionPhase.BEFORE_COMPLETION) BaseEntityService.EntityCreatedEvent<PatientCharge> event){
        //now bill the request
        PatientCharge charge = event.getEntity();
        createOrUpdateBillItemCharge(charge);
    }

    public void onUpdatePatientCharge(@Observes(during = TransactionPhase.BEFORE_COMPLETION) BaseEntityService.EntityUpdatedEvent<PatientCharge> event){
        //now bill the request
        PatientCharge charge = event.getEntity();
        createOrUpdateBillItemCharge(charge);
    }

    private void createOrUpdateBillItemCharge(PatientCharge charge) {
        if(BigDecimal.ZERO.compareTo(charge.getTotal()) != 0){
            try{
                PatientBill currentBill = getOrCreateBillingForAppointment(charge.getAppointment());
                //save the bill
                log.info("=====================Updating patient bill #" + currentBill.getBillNo() + "============================Patient charge total :" + charge.getTotal());
                BillItem bi = createChargeBillItem(charge);
                if(bi.getId() == null) currentBill.addBillItem(gService.persist(bi));//for new bill items
                else bi = gService.edit(bi);
                currentBill.recomputeTotal();
                em.merge(currentBill);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    private synchronized void processRequest(T request) {
        if(request.getRequestStatus() == RequestStatus.SUBMITTED || request instanceof PrescriptionRequest){//only process billing if we are in the submit status
            log.info("===========================Submitted a billable request..." + request.getServiceType() + ", status : " + request.getRequestStatus() + " =========================================");
            //now bill the request
            try{
                PatientBill currentBill = getOrCreateBillingForAppointment(request.getAppointment());
                currentBill.recomputeTotal();
                currentBill = request.billRequest(currentBill);
                synchronized (this){
                    for(BillItem bi : currentBill.getBillItems()){ //hack!!!
                        if(bi.getBillItemStatus() == null) bi.setBillItemStatus(BillItemStatus.BILLED);
                        if(bi.getId() == null) currentBill.addBillItem(gService.persist(bi));//for new bill items
                        else bi = gService.edit(bi);
                    }
                }
                if(request.getBilled() == null || !request.getBilled()) request.setBilled(true);
                em.merge(currentBill);
                if (currentBill.getId() == null) billCreated.fire(new BaseEntityService.EntityCreatedEvent(currentBill));
                else billUpdated.fire(new BaseEntityService.EntityUpdatedEvent(currentBill));
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    private PatientBill getOrCreateBillingForAppointment(Appointment appointment) {
        System.out.println("Appointment : " + appointment);
        List<PatientBill> bills = em.createQuery("select pb from PatientBill pb where pb.appointment.id =:appointmentId and pb.balance <> 0 and pb.deleted <> 1 order by pb.createdAt desc")
                .setParameter("appointmentId", appointment.getId()).getResultList();
        if(bills.size() > 0) return bills.get(0);
        System.out.println("");
        //no bill was found so create a new one;
        PatientBill pb = new PatientBill(appointment, appointment.getPatient(), new Date(), new Date(), seq.nextSequence("PATIENT_BILL"), appointment.getBillPaymentType());
        pb = em.merge(pb);
        return pb;
    }

    public void checkAdmissionBeds(@Observes(during = TransactionPhase.BEFORE_COMPLETION) BaseEntityService.EntityCreatedEvent<AdmissionRequest> event){
        AdmissionRequest admissionRequest = event.getEntity();
        Bed currentBed = admissionRequest.getCurrentBed();
        List<AdmissionBed> admissionBeds = admissionRequest.getAdmissionBeds();
        if(admissionBeds.size() == 0 ) {
            if(currentBed != null){
                AdmissionBed admissionBed = new AdmissionBed(currentBed, currentBed.getWard(), admissionRequest.getDateFrom());
                admissionRequest.addAdmissionBed(admissionBed);
                admissionBedStartEvent.fire(new AdmissionBedStart(admissionBed));
            }
        }else{
            //check if already added
            log.info("============Current Bed : " + currentBed.getBedNo());
            for(AdmissionBed b : admissionBeds){
                log.info("============Bed : " + b.getBed().getBedNo());
            }
            if(admissionBeds.get(admissionBeds.size() - 1).getBed().getId() != currentBed.getId()){
                //first update previous beds to end their tenancy
                for(AdmissionBed bed : admissionRequest.getAdmissionBeds()){
                    //update dateTo
                    if(bed.getDateTo() == null) {
                        bed.setDateTo(new DateTime().withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0).toDate());//only update the beds that have not been updated
                        admissionBedStopEvent.fire(new AdmissionBedStop(bed));
                    }
                }
                AdmissionBed admissionBed = new AdmissionBed(currentBed, currentBed.getWard(), new Date());
                admissionRequest.addAdmissionBed(admissionBed);
                admissionBedStartEvent.fire(new AdmissionBedStart(admissionBed));
            }
        }
        //update the bed status
        if(currentBed != null){
            currentBed.setBedStatus(BedStatus.OCCUPIED);
            gService.edit(currentBed);
        }
    }

    //@Asynchronous
    public void onAdmissionBedStart(@Observes(during = TransactionPhase.BEFORE_COMPLETION) AdmissionBedStart event){
        //get the current patient bill
        log.info("=========Admission start detected==========" + event.getAdmissionBed().getBed().getBedNo());
        AdmissionBed admissionBed = event.getAdmissionBed();
        try{
            PatientBill currentBill = getOrCreateBillingForAppointment(admissionBed.getAdmissionRequest().getAppointment());
            Product product = admissionBed.getBed().getProduct();
            BillItem billItem = new BillItem();
            admissionBed.setBillItem(billItem);
            billItem.setProduct(product);
            billItem.setBillItemStatus(BillItemStatus.BILLED);
            billItem.setBillNo(System.currentTimeMillis());
            billItem.setQuantity(BigDecimal.ONE);
            billItem.setUnitPrice(product.getListPrice());
            billItem.setLineTotal(product.getListPrice().multiply(billItem.getQuantity()));
            billItem.setTax(BigDecimal.ZERO);
            billItem.setSrType(admissionBed.getAdmissionRequest().getServiceType());
            billItem.setSrFqn(admissionBed.getAdmissionRequest().getClass().getName());
            billItem.setSrId(admissionBed.getAdmissionRequest().getId());
            billItem.setBilledFrom(BilledFrom.SERVICE_REQUEST);
            //billItem.setServiceCenter(admissionBed.getAdmissionRequest().getServiceCenter());
            billItem.setReference(admissionBed.getId());
            //bed specific attributes
            billItem.setBillItemType(BillItemType.Accumulation);
            billItem.setDateFrom(admissionBed.getDateFrom());
            currentBill.addBillItem(gService.persist(billItem));
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    //@Asynchronous
    public void onAdmissionBedStop(@Observes(during = TransactionPhase.BEFORE_COMPLETION) AdmissionBedStop event){
        log.info("=========Admission stop detected==========" + event.getAdmissionBed().getBed().getBedNo());
        //get the current patient bill
        AdmissionBed admissionBed = event.getAdmissionBed();
        closeBillItem(admissionBed.getBillItem(), admissionBed.getDateTo());
    }

    private void closeBillItem(BillItem billItem, Date dateTo) {
        try{
            billItem.setDateTo(dateTo);
            em.merge(billItem);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    //@Asynchronous
    public void onMortuaryAdmissionStart(@Observes(during = TransactionPhase.BEFORE_COMPLETION) MortuaryStorageUnitStart event){
        //get the current patient bill
        log.info("=========Mortuary admission start detected==========" + event.getMortuaryStorageUnit().getStorageUnit().getUnitCode());
        MortuaryStorageUnit mortuaryStorageUnit = event.getMortuaryStorageUnit();
        try{
            PatientBill currentBill = getOrCreateBillingForAppointment(mortuaryStorageUnit.getMortuaryRequest().getAppointment());
            Product product = mortuaryStorageUnit.getStorageUnit().getProduct();
            BillItem billItem = new BillItem();
            mortuaryStorageUnit.setBillItem(billItem);
            billItem.setBillItemStatus(BillItemStatus.BILLED);
            billItem.setProduct(product);
            billItem.setBillNo(System.currentTimeMillis());
            billItem.setQuantity(BigDecimal.ONE);
            billItem.setUnitPrice(product.getListPrice());
            billItem.setLineTotal(product.getListPrice().multiply(billItem.getQuantity()));
            billItem.setTax(BigDecimal.ZERO);
            billItem.setSrType(mortuaryStorageUnit.getMortuaryRequest().getServiceType());
            billItem.setSrFqn(mortuaryStorageUnit.getMortuaryRequest().getClass().getName());
            billItem.setSrId(mortuaryStorageUnit.getMortuaryRequest().getId());
            billItem.setBilledFrom(BilledFrom.SERVICE_REQUEST);
            //billItem.setServiceCenter(admissionBed.getAdmissionRequest().getServiceCenter());
            billItem.setReference(mortuaryStorageUnit.getId());
            //bed specific attributes
            billItem.setBillItemType(BillItemType.Accumulation);
            billItem.setDateFrom(mortuaryStorageUnit.getDateFrom());
            currentBill.addBillItem(gService.persist(billItem));
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    //@Asynchronous
    public void onMortuaryAdmissionStop(@Observes(during = TransactionPhase.BEFORE_COMPLETION) MortuaryStorageUnitStop event){
        log.info("=========Mortuary admission stop detected==========" + event.getMortuaryStorageUnit().getStorageUnit().getUnitCode());
        //get the current patient bill
        MortuaryStorageUnit mortuaryStorageUnit = event.getMortuaryStorageUnit();
        closeBillItem(mortuaryStorageUnit.getBillItem(), mortuaryStorageUnit.getDateTo());
    }

    private BillItem createChargeBillItem(PatientCharge charge){
        Product product = charge.getProduct();
        BillItem billItem = new BillItem();
        if(charge.getBillItem() != null) billItem = charge.getBillItem();//just revise the amount billed if it was already billed
        if(billItem.getBillNo() == null) System.currentTimeMillis();
        charge.setBillItem(billItem);
        billItem.setBillItemStatus(BillItemStatus.BILLED);
        billItem.setProduct(product);
        billItem.setQuantity(new BigDecimal(charge.getQuantity()));
        billItem.setUnitPrice(product.getListPrice());
        billItem.setLineTotal(product.getListPrice().multiply(billItem.getQuantity()));
        billItem.setTax(BigDecimal.ZERO);
        billItem.setBilledFrom(BilledFrom.PATIENT_CHARGE);
        billItem.setBillNo(System.currentTimeMillis());
        billItem.setBillItemType(BillItemType.Normal);
        //billItem.setServiceCenter(charge.getServiceRequest().getServiceCenter());
        return billItem;
    }
}