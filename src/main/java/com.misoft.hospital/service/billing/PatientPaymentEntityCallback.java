package com.misoft.hospital.service.billing;

import com.misoft.hospital.model.billing.*;
import com.misoft.hospital.service.grant.GService;
import com.misoft.hospital.util.callbacks.During;
import com.misoft.hospital.util.callbacks.EntityCallbackMethod;

import javax.inject.Inject;
import javax.inject.Named;
import java.math.BigDecimal;
import java.util.Date;
import java.util.logging.Logger;

/**
 * Created by kodero on 6/29/16.
 */
@Named
public class PatientPaymentEntityCallback {

    @Inject
    GService gService;

    @EntityCallbackMethod(when = During.DELETE)
    public void cascadeDelete(PatientPayment patientPayment){
        for (BillPayment billPayment : patientPayment.getBillPayments()){
            billPayment.setAmountSettled(BigDecimal.ZERO);
            billPayment.setDeleted(true);
            billPayment.setDeletedAt(new Date());
        }
    }

    @EntityCallbackMethod(when = {During.CREATE, During.UPDATE})
    public void versionRevisions(PatientPayment patientPayment){
        //create Payment revision
        PaymentRevision rev = new PaymentRevision(patientPayment, patientPayment.getAmountPaid());
        patientPayment.getPaymentRevisions().add(rev);
        //gService.persist(rev);
    }

    @EntityCallbackMethod(when = {During.CREATE})
    public void updateBillItemsToPaid(PatientPayment patientPayment){
        //update the bill items to paid
        for(BillItemPayment item : patientPayment.getBillItemPayments()){
            if(item.getBillItem().getBillItemType() == BillItemType.Accumulation){
                //stop the accumulation
                item.getBillItem().setDateTo(new Date());//this stops the item from automatic billing
            }
        }
    }
}
