package com.misoft.hospital.service.billing;

import com.misoft.hospital.model.billing.PatientBill;

/**
 * Created by kodero on 1/17/16.
 */
public interface BillableRequest {
    public abstract PatientBill billRequest(PatientBill patientBill);
}
