package com.misoft.hospital.validators;

import com.misoft.hospital.model.healthcare.ConsultationRequest;
import com.misoft.hospital.model.healthcare.ImagingRequestItem;
import com.misoft.hospital.model.healthcare.LabRequest;
import com.misoft.hospital.model.healthcare.LabRequestItem;
import com.misoft.hospital.util.validation.ValidateAt;
import com.misoft.hospital.util.validation.ValidationMethod;

import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.validation.Validation;
import javax.validation.ValidationException;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by kodero on 4/29/16.
 */
@Named
public class ConsultationValidator {

    @Inject
    EntityManager em;

    @Inject
    Logger log;

    @ValidationMethod
    public void validateSameConsultationRequests(ConsultationRequest consultationRequest){
        List<Object> requestNos = em.createQuery("select cr.requestNo from ConsultationRequest cr where cr.appointment.id =:appointmentId and cr.unit.id =:unitId and cr.id <> :currentId")
                .setParameter("unitId", consultationRequest.getConsultationType().getUnit().getId())
                .setParameter("currentId", consultationRequest.getId())
                .setParameter("appointmentId", consultationRequest.getAppointment().getId()).getResultList();
        if(requestNos.size() > 0) throw new ValidationException("A consultation request already exits for this patient in the same unit!");
    }

    @ValidationMethod(when = ValidateAt.UPDATE)
    public void validateBlockedRequests(ConsultationRequest consultationRequest){
        log.info("Validating entries in consultation request-----------------");
        for(LabRequestItem lri : consultationRequest.getLabRequestItems()){
            if(lri.getLabTestType().getBlocked() == true && lri.getId() == null) throw new ValidationException("Lab test {" + lri.getLabTestType().getName() + "} has been blocked from ordering!");
        }

        for(ImagingRequestItem iri : consultationRequest.getImagingRequestItems()){
            if(iri.getImagingTest().getBlocked() == true && iri.getId() == null) throw new ValidationException("Imaging test {" + iri.getImagingTest().getName() + "} has been blocked from ordering!");
        }
    }
}
