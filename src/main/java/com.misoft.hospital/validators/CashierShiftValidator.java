package com.misoft.hospital.validators;

import com.misoft.hospital.model.accounts.CashierShift;
import com.misoft.hospital.model.accounts.ShiftStatus;
import com.misoft.hospital.service.admin.UserService;
import com.misoft.hospital.util.validation.ValidateAt;
import com.misoft.hospital.util.validation.ValidationMethod;

import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.validation.ValidationException;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created by kodero on 4/27/16.
 */
@Named
public class CashierShiftValidator {

    @Inject
    EntityManager em;

    @Inject
    UserService userService;

    @ValidationMethod(when = ValidateAt.CREATE)
    public void validateDuplicateShift(CashierShift cashierShift){
        //new shift, so check if some other shift already exists
        List<Object> shiftNos = em.createQuery("select cs.shiftNo from CashierShift cs where cs.owner.id =:staffId and cs.status ='OPEN'")
                //.setParameter("currentId", cashierShift.getId())
                .setParameter("staffId", userService.currentUser().getStaff().getId()).getResultList();
        if(shiftNos.size() != 0){
            throw new ValidationException("You already have an open shift[" + shiftNos.get(0) + "]!");
        }
    }

    @ValidationMethod(when = ValidateAt.UPDATE)
    public void validateClosingValue(CashierShift cashierShift){
        if(cashierShift.getStatus() == ShiftStatus.CLOSED && cashierShift.getCashReceipts().compareTo(BigDecimal.ZERO) > 0){
            if(cashierShift.getClosingValue().compareTo(BigDecimal.ZERO) == 0)
            throw new ValidationException("Please enter the closing value denominations!");
        }
    }
}
