package com.misoft.hospital.validators;

import com.misoft.hospital.model.healthcare.Patient;
import com.misoft.hospital.util.validation.ValidateAt;
import com.misoft.hospital.util.validation.ValidationMethod;

import javax.inject.Named;
import javax.validation.ValidationException;

/**
 * Created by kodero on 6/2/16.
 */
@Named
public class PatientValidator {

    @ValidationMethod(when = {ValidateAt.CREATE, ValidateAt.UPDATE})
    public void validatePatientInfo(Patient patient){
        if(patient.getGender() == null)
            throw new ValidationException("Please enter the patient gender!");
        if(patient.getSurname() == null || patient.getOtherNames() == null)
            throw new ValidationException("Please enter the full patient name!");
        if(patient.getDob() == null)
            throw new ValidationException("Please enter the patient\'s age!");
    }
}
