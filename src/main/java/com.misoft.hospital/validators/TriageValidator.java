package com.misoft.hospital.validators;

import com.misoft.hospital.model.healthcare.TriagingRequest;
import com.misoft.hospital.util.validation.ValidationMethod;

import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.validation.ValidationException;
import java.util.List;

/**
 * Created by kodero on 3/23/16.
 */
@Named
public class TriageValidator {

    @Inject
    private EntityManager em;

    @ValidationMethod
    public void validateDuplicateTriage(TriagingRequest triagingRequest){
        //check if an appointment for the same user already exists
        List<Object> existingTriageRequsts = em.createQuery("select tr.requestNo " +
                "from TriagingRequest tr " +
                "where tr.patient.id =:patientId " +
                "   and tr.appointment.id =:appointmentId " +
                "   and tr.id <>:thisRequest")
                .setParameter("patientId", triagingRequest.getPatient().getId())
                .setParameter("appointmentId", triagingRequest.getAppointment().getId())
                .setParameter("thisRequest", triagingRequest.getId())
                .getResultList();
        if(existingTriageRequsts.size() != 0){
            throw new ValidationException("A triage request[" + existingTriageRequsts.get(0) + "] already exists for that patient!");
        }
    }
}
