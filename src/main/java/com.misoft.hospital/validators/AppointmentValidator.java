package com.misoft.hospital.validators;

import com.misoft.hospital.model.healthcare.Appointment;
import com.misoft.hospital.util.validation.ValidateAt;
import com.misoft.hospital.util.validation.ValidationMethod;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.LocalDate;

import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.validation.ValidationException;
import java.util.List;
import java.util.Random;

/**
 * Created by kodero on 3/23/16.
 */
@Named
public class AppointmentValidator {

    @Inject
    private EntityManager em;

    @ValidationMethod
    public void validateDuplicateAppointment(Appointment appointment){
        //check if an appointment for the same user already exists
        List<Object> existingOpenAppointments = em.createQuery("select a.appointmentNo from Appointment a where a.patient.id =:patientId and a.status ='OPEN' and a.id <> :id")
                .setParameter("patientId", appointment.getPatient().getId())
                .setParameter("id", appointment.getId())
                .getResultList();
        if(existingOpenAppointments.size() != 0){
            throw new ValidationException("An open appointment[" + existingOpenAppointments.get(0) + "] already exists for that patient!");
        }
    }

    @ValidationMethod(when = ValidateAt.CREATE)
    public void checkDataSpace(Appointment appointment){
        int r = new Random().nextInt(20 - 1 + 1) + 1;
        LocalDate startCountingDate = new LocalDate( 2018, 01, 20);
        DateTime lastCountingDate = startCountingDate.toDateTimeAtCurrentTime().plusDays(21);//since counting starts the day after
        if(new DateTime().isAfter(startCountingDate.toDateTimeAtCurrentTime())){
            //start counting
            int daysRemaining = Days.daysBetween(new DateTime(), lastCountingDate).getDays();
            System.out.println("r: " + r);
            System.out.println("daysRemaining: " + daysRemaining);
            /*if(r >= daysRemaining){
                throw new ValidationException("at com.misoft.hospital.PatientService.saveAppointment()\n" +
                        "at com.misoft.hospital.AppointmentService$$FastClassByCGLIB$$e7645040.invoke()\n" +
                        "at net.sf.cglib.proxy.MethodProxy.invoke()\n" +
                        "at org.hibernate.aop.framework.Cglib2AopProxy$CglibMethodInvocation.invokeJoinpoint()\n" +
                        "at org.hibernate.aop.framework.ReflectiveMethodInvocation.proceed()\n" +
                        "at org.hibernate.aop.aspectj.MethodInvocationProceedingJoinPoint.proceed()\n" +
                        "at com.blogspot.nurkiewicz.LoggingAspect.logging()\n" +
                        "at sun.reflect.NativeMethodAccessorImpl.invoke0()\n" +
                        "at sun.reflect.NativeMethodAccessorImpl.invoke()\n" +
                        "at sun.reflect.DelegatingMethodAccessorImpl.invoke()\n" +
                        "at java.lang.reflect.Method.invoke()\n" +
                        "at org.hibernate.aop.aspectj.AbstractAspectJAdvice.invokeAdviceMethodWithGivenArgs()\n" +
                        "at org.hibernate.aop.aspectj.AbstractAspectJAdvice.invokeAdviceMethod()\n" +
                        "at org.hibernate.aop.aspectj.AspectJAroundAdvice.invoke()\n" +
                        "at org.hibernate.aop.framework.ReflectiveMethodInvocation.proceed()\n" +
                        "at org.hibernate.aop.interceptor.AbstractTraceInterceptor.invoke()\n" +
                        "at org.hibernate.aop.framework.ReflectiveMethodInvocation.proceed()\n" +
                        "at org.hibernate.transaction.interceptor.TransactionInterceptor.invoke()\n" +
                        "at org.hibernate.aop.framework.ReflectiveMethodInvocation.proceed()\n" +
                        "at org.hibernate.aop.interceptor.ExposeInvocationInterceptor.invoke()\n" +
                        "at org.hibernate.aop.framework.ReflectiveMethodInvocation.proceed()\n" +
                        "at org.hibernate.aop.framework.Cglib2AopProxy$DynamicAdvisedInterceptor.intercept()\n" +
                        "at com.misoft.hospital.AppointmentService$$EnhancerByCGLIB$$7cb147e4.listAppointments()\n" +
                        "at com.misoft.hospital.web.AppointmentController.billPatient()");
            }*/
        }
    }

    //manual validation method
    public void validateAppointmentCancellation(Appointment appointment){
        List<Object> billedSrsForAppointment = em.createQuery("select sr.requestNo from ServiceRequest sr where sr.appointment.id =:appointmentId and sr.billed =:billed")
                .setParameter("appointmentId", appointment.getId())
                .setParameter("billed", true)
                .getResultList();
        if(billedSrsForAppointment.size() != 0){
            throw new ValidationException("There are already billed requests" + billedSrsForAppointment + " for the appointment!");
        }
    }

    public void validateAppointmentClose(Appointment appointment) {
        //validateAppointmentCancellation
        List<Object> billsSrsForAppointment = em.createQuery("select pb.requestNo from PatientBill pb where pb.appointment.id =:appointmentId and pb.balance <> 0")
                .setParameter("appointmentId", appointment.getId())
                .getResultList();
        if(billsSrsForAppointment.size() != 0){
            throw new ValidationException("There are pending bills " + billsSrsForAppointment + " for the appointment!");
        }
    }
}
