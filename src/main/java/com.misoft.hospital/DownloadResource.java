package com.misoft.hospital;


import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.File;

/**
 * Created by kodero on 1/30/15.
 */
@Path("/downloads")
public class DownloadResource {
    @GET
    @Path("/{canonicalFileName}/{realFileName}")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response downloadFile(@PathParam("realFileName")String realFileName, @PathParam("canonicalFileName") String canonicalFileName) {
        File file = new File(System.getProperty("java.io.tmpdir") + "/" + realFileName); // Initialize this to the File path you want to serve.
        if(file.exists()){
            return Response.ok(file, MediaType.APPLICATION_OCTET_STREAM)
                    .header("Content-Disposition", "attachment; filename=\"" + canonicalFileName + "\"" ) //optional
                    .build();
        }else{
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }
}
