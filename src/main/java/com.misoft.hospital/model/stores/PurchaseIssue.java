package com.misoft.hospital.model.stores;

import com.misoft.hospital.model.healthcare.ServiceCenter;
import com.misoft.hospital.util.callbacks.During;
import com.misoft.hospital.util.callbacks.EntityCallbackMethod;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created by kodero on 8/6/16.
 */
@Entity
@DiscriminatorValue("STOCK_ISSUE")
public class PurchaseIssue extends Order {

    @JoinColumn(name = "service_center", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private ServiceCenter serviceCenter;

    @Column(name = "issued_to")
    private String issuedTo;

    @NotNull
    @Column(name = "date_issued")
    private Date dateIssued;

    @EntityCallbackMethod(when = {During.CREATE, During.UPDATE})
    public void updateOrderLineDetails(){
        for(OrderLine ol : getOrderLines()){
            ol.setMultiplier(ol.getProductPackaging().getQuantityInPackaging());
            ol.setOrderDate(((PurchaseIssue)ol.getOrder()).getDateIssued());
            ol.setOrderType(PurchaseIssue.class.getAnnotation(DiscriminatorValue.class).value());
        }
    }

    public ServiceCenter getServiceCenter() {
        return serviceCenter;
    }

    public void setServiceCenter(ServiceCenter serviceCenter) {
        this.serviceCenter = serviceCenter;
    }

    public String getIssuedTo() {
        return issuedTo;
    }

    public void setIssuedTo(String issuedTo) {
        this.issuedTo = issuedTo;
    }

    public Date getDateIssued() {
        return dateIssued;
    }

    public void setDateIssued(Date dateIssued) {
        this.dateIssued = dateIssued;
    }
}
