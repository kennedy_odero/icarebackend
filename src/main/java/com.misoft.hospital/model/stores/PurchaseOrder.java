package com.misoft.hospital.model.stores;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Created by kodero on 3/27/15.
 */
@Entity
@DiscriminatorValue("PURCHASE_ORDER")
public class PurchaseOrder extends Order {

}
