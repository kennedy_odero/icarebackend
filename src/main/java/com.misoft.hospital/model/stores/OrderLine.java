package com.misoft.hospital.model.stores;

import com.misoft.hospital.model.base.ModelBase;
import com.misoft.hospital.model.bloodBank.CollectionBatch;
import com.misoft.hospital.model.setup.Product;
import com.misoft.hospital.model.setup.ProductPackaging;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by kodero on 3/27/15.
 */
@Entity
@Table(name  = "order_lines")
@Filter(name = "filterByDeleted")
public class OrderLine extends ModelBase {

    @JoinColumn(name = "product", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Product product;

    @JoinColumn(name = "product_packaging", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private ProductPackaging productPackaging;

    @Column(name = "quantity")
    private Integer quantity;

    @Column(name = "multiplier", columnDefinition = "mediumint default 1")
    private Integer multiplier = 1;

    @Column(name = "damaged")
    private Integer damaged;

    @Column(name = "unit_price")
    private BigDecimal unitPrice;

    @Column(name = "line_total")
    private BigDecimal lineTotal;

    @JoinColumn(name = "purchase_order", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Order order;

    @JoinColumn(name = "collection_batch", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private CollectionBatch collectionBatch;

    @Column(name = "order_date", columnDefinition = "datetime default CURRENT_TIMESTAMP")
    private Date orderDate;

    @Column(name = "order_type")
    private String orderType;

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    public BigDecimal getLineTotal() {
        return lineTotal;
    }

    public void setLineTotal(BigDecimal lineTotal) {
        this.lineTotal = lineTotal;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Integer getDamaged() {
        return damaged;
    }

    public void setDamaged(Integer damaged) {
        this.damaged = damaged;
    }

    public ProductPackaging getProductPackaging() {
        return productPackaging;
    }

    public void setProductPackaging(ProductPackaging productPackaging) {
        this.productPackaging = productPackaging;
    }

    public CollectionBatch getCollectionBatch() {
        return collectionBatch;
    }

    public void setCollectionBatch(CollectionBatch collectionBatch) {
        this.collectionBatch = collectionBatch;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public Integer getMultiplier() {
        return multiplier;
    }

    public void setMultiplier(Integer multiplier) {
        this.multiplier = multiplier;
    }
}
