package com.misoft.hospital.model.stores;

import com.misoft.hospital.model.accounts.Creditor;
import com.misoft.hospital.model.approval.ApprovalTemplate;
import com.misoft.hospital.model.approval.DocumentApproval;
import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

/**
 * Created by kodero on 4/10/15.
 */
@Entity
@Table(name = "orders")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "order_type")
@Filter(name = "filterByDeleted")
public class Order extends ModelBase {

    @Column(name = "po_no")
    private Long poNo;

    @Column(name = "date_ordered")
    private Date dateOrdered;

    @Column(name = "date_received")
    private Date dateReceived;

    @JoinColumn(name = "supplier", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Creditor supplier;

    @Column(name = "received")
    private Boolean received;

    @Column(name = "deliver_by")
    private Date deliverBy;

    @OneToMany(mappedBy = "order", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @Filter(name = "filterByDeleted")
    private Collection<OrderLine> orderLines;

    @Column(name = "po_total")
    private BigDecimal poTotal;

    @JoinColumn(name = "approval_template", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private ApprovalTemplate approvalTemplate;

    @JoinColumn(name = "document_approval", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private DocumentApproval documentApproval;

    public ApprovalTemplate getApprovalTemplate() {
        return approvalTemplate;
    }

    public void setApprovalTemplate(ApprovalTemplate approvalTemplate) {
        this.approvalTemplate = approvalTemplate;
    }

    public DocumentApproval getDocumentApproval() {
        return documentApproval;
    }

    public void setDocumentApproval(DocumentApproval documentApproval) {
        this.documentApproval = documentApproval;
    }

    public Long getPoNo() {
        return poNo;
    }

    public void setPoNo(Long poNo) {
        this.poNo = poNo;
    }

    public Date getDateOrdered() {
        return dateOrdered;
    }

    public void setDateOrdered(Date dateOrdered) {
        this.dateOrdered = dateOrdered;
    }

    public Date getDateReceived() {
        return dateReceived;
    }

    public void setDateReceived(Date dateReceived) {
        this.dateReceived = dateReceived;
    }

    public Creditor getSupplier() {
        return supplier;
    }

    public void setSupplier(Creditor supplier) {
        this.supplier = supplier;
    }

    public Boolean getReceived() {
        return received;
    }

    public void setReceived(Boolean received) {
        this.received = received;
    }

    public Date getDeliverBy() {
        return deliverBy;
    }

    public void setDeliverBy(Date deliverBy) {
        this.deliverBy = deliverBy;
    }

    public Collection<OrderLine> getOrderLines() {
        return orderLines;
    }

    public void setOrderLines(Collection<OrderLine> orderLines) {
        this.orderLines = orderLines;
    }

    public BigDecimal getPoTotal() {
        return poTotal;
    }

    public void setPoTotal(BigDecimal poTotal) {
        this.poTotal = poTotal;
    }
}
