package com.misoft.hospital.model.stores;

import com.misoft.hospital.model.admin.User;
import com.misoft.hospital.model.base.ModelBase;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by kodero on 8/14/15.
 */
@Entity
@Table(name = "stock_mvt_history")
public class MovementStatusHistory extends ModelBase{

    public MovementStatusHistory(){

    }

    public MovementStatusHistory(MovementStatus status, StockMovement movement, Date date, User user){
        setStatus(status);
        setStockMovement(movement);
        setDate(new Date());
        setUser(user);
    }

    @Column(name = "status")
    private MovementStatus status;

    @JoinColumn(name = "stock_movement", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private StockMovement stockMovement;

    @Column(name = "date")
    private Date date;

    @JoinColumn(name = "user", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private User user;

    public MovementStatus getStatus() {
        return status;
    }

    public void setStatus(MovementStatus status) {
        this.status = status;
    }

    public StockMovement getStockMovement() {
        return stockMovement;
    }

    public void setStockMovement(StockMovement stockMovement) {
        this.stockMovement = stockMovement;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
