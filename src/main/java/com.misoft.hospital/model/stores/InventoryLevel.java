package com.misoft.hospital.model.stores;

import com.misoft.hospital.model.accounts.AccountingPeriod;
import com.misoft.hospital.model.base.ModelBase;
import com.misoft.hospital.model.healthcare.ServiceCenter;
import com.misoft.hospital.model.setup.Product;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by kodero on 5/3/15.
 */
@Entity
@Table(name = "inventory_levels")
@Filter(name = "filterByDeleted")
public class InventoryLevel extends ModelBase {

    @Column(name = "run_date")
    private Date runDate;

    @JoinColumn(name = "product", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Product product;

    @Column(name = "delivered")
    private BigDecimal delivered;

    @Column(name = "returned")
    private BigDecimal returned;

    @Column(name = "moved_in")
    private BigDecimal movedIn;

    @Column(name = "moved_out")
    private BigDecimal movedOut;

    @Column(name = "dispensed")
    private BigDecimal dispensed;

    @Column(name = "calc_quantity")
    private BigDecimal calcQuantity;

    @Column(name = "physical_quantity")
    private BigDecimal physicalQuantity;

    @Column(name = "variance")
    private BigDecimal variance;

    @JoinColumn(name = "service_center", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private ServiceCenter serviceCenter;

    @JoinColumn(name = "accounting_period", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private AccountingPeriod accountingPeriod;

    public InventoryLevel(){

    }

    public InventoryLevel(Product product, BigDecimal calcQuantity, BigDecimal physicalQauntity, BigDecimal variance, ServiceCenter serviceCenter, AccountingPeriod ap){
        setProduct(product);
        setCalcQuantity(calcQuantity);
        setPhysicalQuantity(physicalQauntity);
        setVariance(variance);
        setServiceCenter(serviceCenter);
        setAccountingPeriod(ap);
    }

    public Date getRunDate() {
        return runDate;
    }

    public void setRunDate(Date runDate) {
        this.runDate = runDate;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public BigDecimal getCalcQuantity() {
        return calcQuantity;
    }

    public void setCalcQuantity(BigDecimal calcQuantity) {
        this.calcQuantity = calcQuantity;
    }

    public BigDecimal getPhysicalQuantity() {
        return physicalQuantity;
    }

    public void setPhysicalQuantity(BigDecimal physicalQuantity) {
        this.physicalQuantity = physicalQuantity;
    }

    public BigDecimal getVariance() {
        return variance;
    }

    public void setVariance(BigDecimal variance) {
        this.variance = variance;
    }

    public ServiceCenter getServiceCenter() {
        return serviceCenter;
    }

    public void setServiceCenter(ServiceCenter serviceCenter) {
        this.serviceCenter = serviceCenter;
    }

    public AccountingPeriod getAccountingPeriod() {
        return accountingPeriod;
    }

    public void setAccountingPeriod(AccountingPeriod accountingPeriod) {
        this.accountingPeriod = accountingPeriod;
    }

    public BigDecimal getDelivered() {
        return delivered;
    }

    public void setDelivered(BigDecimal delivered) {
        this.delivered = delivered;
    }

    public BigDecimal getReturned() {
        return returned;
    }

    public void setReturned(BigDecimal returned) {
        this.returned = returned;
    }

    public BigDecimal getMovedIn() {
        return movedIn;
    }

    public void setMovedIn(BigDecimal movedIn) {
        this.movedIn = movedIn;
    }

    public BigDecimal getMovedOut() {
        return movedOut;
    }

    public void setMovedOut(BigDecimal movedOut) {
        this.movedOut = movedOut;
    }

    public BigDecimal getDispensed() {
        return dispensed;
    }

    public void setDispensed(BigDecimal dispensed) {
        this.dispensed = dispensed;
    }
}
