package com.misoft.hospital.model.stores;

import java.math.BigDecimal;

/**
 * Created by kodero on 5/3/15.
 */
public class InventoryLevelInfo {
    private String pId;
    private String pName;
    private BigDecimal delivered;
    private BigDecimal returned;
    private BigDecimal movedIn;
    private BigDecimal movedOut;
    private BigDecimal dispensed;

    public InventoryLevelInfo(String pId, String pName, BigDecimal delivered, BigDecimal returned, BigDecimal movedIn, BigDecimal movedOut, BigDecimal dispensed){
        setpId(pId);
        setpName(pName);
        setDelivered(delivered);
        setReturned(returned);
        setMovedIn(movedIn);
        setMovedOut(movedOut);
        setDispensed(dispensed);
    }

    public String getpId() {
        return pId;
    }

    public void setpId(String pId) {
        this.pId = pId;
    }

    public String getpName() {
        return pName;
    }

    public void setpName(String pName) {
        this.pName = pName;
    }

    public BigDecimal getDelivered() {
        return delivered;
    }

    public void setDelivered(BigDecimal delivered) {
        this.delivered = delivered;
    }

    public BigDecimal getReturned() {
        return returned;
    }

    public void setReturned(BigDecimal returned) {
        this.returned = returned;
    }

    public BigDecimal getMovedIn() {
        return movedIn;
    }

    public void setMovedIn(BigDecimal movedIn) {
        this.movedIn = movedIn;
    }

    public BigDecimal getMovedOut() {
        return movedOut;
    }

    public void setMovedOut(BigDecimal movedOut) {
        this.movedOut = movedOut;
    }

    public BigDecimal getDispensed() {
        return dispensed;
    }

    public void setDispensed(BigDecimal dispensed) {
        this.dispensed = dispensed;
    }
}
