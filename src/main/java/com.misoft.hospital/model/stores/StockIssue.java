package com.misoft.hospital.model.stores;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by kodero on 4/15/15.
 */
@Entity
@DiscriminatorValue("ISSUE")
public class StockIssue extends StockMovement{

    @JoinColumn(name = "stock_request", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private  StockRequest stockRequest;

    @OneToMany(mappedBy = "stockIssue", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @org.hibernate.annotations.Filter(name = "filterByDeleted")
    private Collection<StockRequestIssue> stockRequestIssues;

    public Collection<StockRequestIssue> getStockRequestIssues() {
        return stockRequestIssues;
    }

    public void setStockRequestIssues(Collection<StockRequestIssue> stockRequestIssues) {
        this.stockRequestIssues = stockRequestIssues;
    }

    public StockRequest getStockRequest() {
        return stockRequest;
    }

    public void setStockRequest(StockRequest stockRequest) {
        this.stockRequest = stockRequest;
    }
}
