package com.misoft.hospital.model.stores;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.*;

/**
 * Created by kodero on 4/10/15.
 */
@Entity
@Table(name = "po_deliveries")
@Filter(name = "filterByDeleted")
public class PODelivery extends ModelBase {

    @JoinColumn(name = "purchase_order", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private PurchaseOrder purchaseOrder;

    @JoinColumn(name = "purchase_delivery", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private PurchaseDelivery purchaseDelivery;

    public PurchaseOrder getPurchaseOrder() {
        return purchaseOrder;
    }

    public void setPurchaseOrder(PurchaseOrder purchaseOrder) {
        this.purchaseOrder = purchaseOrder;
    }

    public PurchaseDelivery getPurchaseDelivery() {
        return purchaseDelivery;
    }

    public void setPurchaseDelivery(PurchaseDelivery purchaseDelivery) {
        this.purchaseDelivery = purchaseDelivery;
    }
}
