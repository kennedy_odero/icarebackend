package com.misoft.hospital.model.stores;

import com.misoft.hospital.model.healthcare.ServiceCenter;
import com.misoft.hospital.util.callbacks.During;
import com.misoft.hospital.util.callbacks.EntityCallbackMethod;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;

/**
 * Created by kodero on 4/10/15.
 */
@Entity
@DiscriminatorValue("PURCHASE_DELIVERY")
public class PurchaseDelivery extends Order {

    @Column(name = "delivered_on")
    private Date deliveredOn;

    @Column(name = "delivered_by")
    private String deliveredBy;

    @Column(name = "delivery_comments")
    private String deliveryComments;

    @JoinColumn(name = "service_center", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private ServiceCenter serviceCenter;

    @OneToMany(mappedBy = "purchaseDelivery", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @org.hibernate.annotations.Filter(name = "filterByDeleted")
    Collection<PODelivery> poDeliveries;

    @Column(name = "po_number")
    private String poNumber;

    @EntityCallbackMethod(when = {During.CREATE, During.UPDATE})
    public void updateOrderLineDetails(){
        for(OrderLine ol : getOrderLines()){
            ol.setMultiplier(ol.getProductPackaging().getQuantityInPackaging());
            ol.setOrderDate(((PurchaseDelivery)ol.getOrder()).getDeliveredOn());
            ol.setOrderType(PurchaseDelivery.class.getAnnotation(DiscriminatorValue.class).value());
        }
    }

    public String getDeliveredBy() {
        return deliveredBy;
    }

    public void setDeliveredBy(String deliveredBy) {
        this.deliveredBy = deliveredBy;
    }

    public String getDeliveryComments() {
        return deliveryComments;
    }

    public void setDeliveryComments(String deliveryComments) {
        this.deliveryComments = deliveryComments;
    }

    public Date getDeliveredOn() {
        return deliveredOn;
    }

    public void setDeliveredOn(Date deliveredOn) {
        this.deliveredOn = deliveredOn;
    }

    public Collection<PODelivery> getPoDeliveries() {
        return poDeliveries;
    }

    public void setPoDeliveries(Collection<PODelivery> poDeliveries) {
        this.poDeliveries = poDeliveries;
    }

    public ServiceCenter getServiceCenter() {
        return serviceCenter;
    }

    public void setServiceCenter(ServiceCenter serviceCenter) {
        this.serviceCenter = serviceCenter;
    }
}
