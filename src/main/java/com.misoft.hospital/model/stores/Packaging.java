package com.misoft.hospital.model.stores;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * Created by kodero on 8/5/16.
 */
@Entity
@Table(name = "packagings")
@Filter(name = "filterByDeleted")
public class Packaging extends ModelBase{

    @NotNull
    @Column(name = "name")
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
