package com.misoft.hospital.model.stores;

import com.misoft.hospital.util.callbacks.During;
import com.misoft.hospital.util.callbacks.EntityCallbackMethod;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Created by kodero on 8/20/16.
 */
@Entity
@DiscriminatorValue("ADJUSTMENT")
public class StockAdjustment extends Order{

    @Column(name = "reason")
    private String reason;

    @EntityCallbackMethod(when = {During.CREATE, During.UPDATE})
    public void updateOrderLineDetails(){
        for(OrderLine ol : getOrderLines()){
            ol.setOrderDate((ol.getOrder()).getDateOrdered());
            ol.setOrderType(StockAdjustment.class.getAnnotation(DiscriminatorValue.class).value());
            ol.setMultiplier(ol.getProductPackaging().getQuantityInPackaging());
        }
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
