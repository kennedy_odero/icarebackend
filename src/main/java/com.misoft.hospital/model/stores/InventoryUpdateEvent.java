package com.misoft.hospital.model.stores;

import com.misoft.hospital.model.accounts.AccountingPeriod;
import com.misoft.hospital.model.healthcare.ServiceCenter;
import com.misoft.hospital.model.setup.Product;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 * Created by kodero on 5/3/15.
 */
public class InventoryUpdateEvent {
    Date updateDate;
    Collection<ServiceCenter> serviceCenters = new ArrayList<>();
    AccountingPeriod accountingPeriod;
    Collection<Product> products = new ArrayList<>();

    public InventoryUpdateEvent(){

    }

    public InventoryUpdateEvent(Date updateDate, Collection<ServiceCenter> serviceCenters, AccountingPeriod accountingPeriod, Collection<Product> products){
        setUpdateDate(updateDate);
        setServiceCenters(serviceCenters);
        setAccountingPeriod(accountingPeriod);
        setProducts(products);
    }

    public Collection<Product> getProducts() {
        return products;
    }

    public void setProducts(Collection<Product> products) {
        this.products = products;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public AccountingPeriod getAccountingPeriod() {
        return accountingPeriod;
    }

    public void setAccountingPeriod(AccountingPeriod accountingPeriod) {
        this.accountingPeriod = accountingPeriod;
    }

    public Collection<ServiceCenter> getServiceCenters() {
        return serviceCenters;
    }

    public void setServiceCenters(Collection<ServiceCenter> serviceCenters) {
        this.serviceCenters = serviceCenters;
    }
}
