package com.misoft.hospital.model.stores;

import com.misoft.hospital.model.admin.User;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;

/**
 * Created by kodero on 4/15/15.
 */
@Entity
@DiscriminatorValue("REQUEST")
public class StockRequest extends StockMovement {
    @Column(name = "date_req")
    private Date dateRequested;

    @Column(name = "required_by")
    private Date requiredBy;

    @JoinColumn(name = "requested_by", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private User requestedBy;

    public Date getDateRequested() {
        return dateRequested;
    }

    public void setDateRequested(Date dateRequested) {
        this.dateRequested = dateRequested;
    }

    public User getRequestedBy() {
        return requestedBy;
    }

    public void setRequestedBy(User requestedBy) {
        this.requestedBy = requestedBy;
    }

    public Date getRequiredBy() {
        return requiredBy;
    }

    public void setRequiredBy(Date requiredBy) {
        this.requiredBy = requiredBy;
    }
}
