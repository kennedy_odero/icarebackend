package com.misoft.hospital.model.stores;

import com.misoft.hospital.model.healthcare.ServiceCenter;

import javax.persistence.*;

/**
 * Created by kodero on 4/10/15.
 */
@Entity
@DiscriminatorValue("PURCHASE_RETURN")
public class PurchaseReturn extends Order {

    @Column(name = "reason", columnDefinition = "text")
    private String reason;

    @JoinColumn(name = "service_center", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private ServiceCenter serviceCenter;

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public ServiceCenter getServiceCenter() {
        return serviceCenter;
    }

    public void setServiceCenter(ServiceCenter serviceCenter) {
        this.serviceCenter = serviceCenter;
    }
}
