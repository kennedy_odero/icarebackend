package com.misoft.hospital.model.stores;

import com.misoft.hospital.model.base.ModelBase;
import com.misoft.hospital.model.setup.Product;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by kodero on 3/29/15.
 */
@Entity
@Table(name = "mvt_line_items")
@Filter(name = "filterByDeleted")
public class MovementLineItem extends ModelBase {

    @JoinColumn(name = "product", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Product product;

    @Column(name = "qty")
    private BigDecimal qty;

    @Column(name = "qty_req")
    private BigDecimal qtyReq;

    @Column(name = "issued")
    private BigDecimal issued;

    @JoinColumn(name = "stock_mvt", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private StockMovement stockMovement;

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public StockMovement getStockMovement() {
        return stockMovement;
    }

    public void setStockMovement(StockMovement stockMovement) {
        this.stockMovement = stockMovement;
    }

    public BigDecimal getQty() {
        return qty;
    }

    public void setQty(BigDecimal qty) {
        this.qty = qty;
    }

    public BigDecimal getIssued() {
        return issued;
    }

    public void setIssued(BigDecimal issued) {
        this.issued = issued;
    }

    public BigDecimal getQtyReq() {
        return qtyReq;
    }

    public void setQtyReq(BigDecimal qtyReq) {
        this.qtyReq = qtyReq;
    }
}
