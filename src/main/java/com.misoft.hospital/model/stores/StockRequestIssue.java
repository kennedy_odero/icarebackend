package com.misoft.hospital.model.stores;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.*;

/**
 * Created by kodero on 4/15/15.
 */
@Entity
@Table(name = "request_issues")
@Filter(name = "filterByDeleted")
public class StockRequestIssue extends ModelBase {

    @JoinColumn(name = "request", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private StockRequest stockRequest;

    @JoinColumn(name = "issue", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private StockIssue stockIssue;

    public StockRequest getStockRequest() {
        return stockRequest;
    }

    public void setStockRequest(StockRequest stockRequest) {
        this.stockRequest = stockRequest;
    }

    public StockIssue getStockIssue() {
        return stockIssue;
    }

    public void setStockIssue(StockIssue stockIssue) {
        this.stockIssue = stockIssue;
    }
}
