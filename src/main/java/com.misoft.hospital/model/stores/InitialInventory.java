package com.misoft.hospital.model.stores;

import com.misoft.hospital.util.callbacks.During;
import com.misoft.hospital.util.callbacks.EntityCallbackMethod;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Created by kodero on 8/5/16.
 */
@Entity
@DiscriminatorValue("INITIAL_INVENTORY")
public class InitialInventory extends Order{

    @EntityCallbackMethod(when = {During.CREATE, During.UPDATE})
    public void updateOrderLineDetails(){
        for(OrderLine ol : getOrderLines()){
            ol.setOrderDate((ol.getOrder()).getDateOrdered());
            ol.setOrderType(InitialInventory.class.getAnnotation(DiscriminatorValue.class).value());
            ol.setMultiplier(ol.getProductPackaging().getQuantityInPackaging());
        }
    }
}
