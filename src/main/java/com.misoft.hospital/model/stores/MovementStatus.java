package com.misoft.hospital.model.stores;

/**
 * Created by kodero on 3/29/15.
 */
public enum MovementStatus {
    /* movement request */
    NEW,
    RECEIVED,
    REJECTED,
    CANCELLED,
    COMPLETED,
    /* movement issue */
    DISPATCHED,
    /* receipt */
    VERIFIED
}
