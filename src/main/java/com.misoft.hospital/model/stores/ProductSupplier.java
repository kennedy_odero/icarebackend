package com.misoft.hospital.model.stores;

import com.misoft.hospital.model.accounts.AccountingPeriod;
import com.misoft.hospital.model.accounts.Creditor;
import com.misoft.hospital.model.base.ModelBase;
import com.misoft.hospital.model.setup.Product;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by kodero on 8/14/15.
 */
@Entity
@Table(name = "product_suppliers")
public class ProductSupplier extends ModelBase{

    @JoinColumn(name = "supplier", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Creditor supplier;

    @JoinColumn(name = "product", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Product product;

    @JoinColumn(name = "accounting_period", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private AccountingPeriod period;

    @Column(name = "rate")
    private BigDecimal rate;

    public Creditor getSupplier() {
        return supplier;
    }

    public void setSupplier(Creditor supplier) {
        this.supplier = supplier;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public AccountingPeriod getPeriod() {
        return period;
    }

    public void setPeriod(AccountingPeriod period) {
        this.period = period;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }
}
