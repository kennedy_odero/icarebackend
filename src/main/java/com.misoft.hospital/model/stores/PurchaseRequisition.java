package com.misoft.hospital.model.stores;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Created by kodero on 8/17/15.
 */
@Entity
@DiscriminatorValue("PURCHASE_REQUISITION")
public class PurchaseRequisition extends Order{

}
