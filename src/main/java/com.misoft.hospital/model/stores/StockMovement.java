package com.misoft.hospital.model.stores;

import com.misoft.hospital.model.admin.User;
import com.misoft.hospital.model.base.ModelBase;
import com.misoft.hospital.model.healthcare.ServiceCenter;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Created by kodero on 3/29/15.
 */
@Entity
@Table(name = "stock_movements")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "mvt_type")
@Filter(name = "filterByDeleted")
public class StockMovement extends ModelBase {

    @Column(name = "mvt_no")
    private Long movementNo;

    @JoinColumn(name = "svc_center_from", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private ServiceCenter from;

    @JoinColumn(name = "svc_center_to", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private ServiceCenter to;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private MovementStatus status = MovementStatus.NEW;

    @OneToMany(mappedBy = "stockMovement", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    private List<MovementStatusHistory> movementStatusHistories = new ArrayList<>();

    @OneToMany(mappedBy = "stockMovement", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @Filter(name = "filterByDeleted")
    private Collection<MovementLineItem> movementLineItems;

    @Column(name = "type")
    @Enumerated(EnumType.STRING)
    private MovementType type;

    @Column(name = "date_issued")
    private Date dateOfMovement;

    @JoinColumn(name = "issued_by", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private User movementBy;

    @Column(name = "delivered_by")
    private String deliveredBy;

    public Long getMovementNo() {
        return movementNo;
    }

    public void setMovementNo(Long movementNo) {
        this.movementNo = movementNo;
    }

    public ServiceCenter getFrom() {
        return from;
    }

    public void setFrom(ServiceCenter from) {
        this.from = from;
    }

    public ServiceCenter getTo() {
        return to;
    }

    public void setTo(ServiceCenter to) {
        this.to = to;
    }

    public MovementStatus getStatus() {
        return status;
    }

    public void setStatus(MovementStatus status) {
        this.status = status;
    }

    public Collection<MovementLineItem> getMovementLineItems() {
        return movementLineItems;
    }

    public void setMovementLineItems(Collection<MovementLineItem> movementLineItems) {
        this.movementLineItems = movementLineItems;
    }

    public MovementType getType() {
        return type;
    }

    public void setType(MovementType type) {
        this.type = type;
    }

    public List<MovementStatusHistory> getMovementStatusHistories() {
        return movementStatusHistories;
    }

    public void setMovementStatusHistories(List<MovementStatusHistory> movementStatusHistories) {
        this.movementStatusHistories = movementStatusHistories;
    }

    public Date getDateOfMovement() {
        return dateOfMovement;
    }

    public void setDateOfMovement(Date dateOfMovement) {
        this.dateOfMovement = dateOfMovement;
    }

    public User getMovementBy() {
        return movementBy;
    }

    public void setMovementBy(User movementBy) {
        this.movementBy = movementBy;
    }

    public String getDeliveredBy() {
        return deliveredBy;
    }

    public void setDeliveredBy(String deliveredBy) {
        this.deliveredBy = deliveredBy;
    }
}
