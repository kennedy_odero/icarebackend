package com.misoft.hospital.model.approval;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by kodero on 12/16/15.
 */
@Entity
@Table(name = "document_approval")
public class DocumentApproval extends ModelBase{

    @OneToMany(mappedBy = "documentApproval", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @Filter(name = "filterByDeleted")
    private Collection<ApproverApproval> approverApprovals;

    public Collection<ApproverApproval> getApproverApprovals() {
        return approverApprovals;
    }

    public void setApproverApprovals(Collection<ApproverApproval> approverApprovals) {
        this.approverApprovals = approverApprovals;
    }
}
