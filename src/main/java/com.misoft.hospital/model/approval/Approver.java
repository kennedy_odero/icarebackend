package com.misoft.hospital.model.approval;

import com.misoft.hospital.model.base.ModelBase;
import com.misoft.hospital.model.setup.Staff;

import javax.persistence.*;

/**
 * Created by kodero on 12/16/15.
 */
@Entity
@Table(name = "approvers")
public class Approver extends ModelBase{

    @JoinColumn(name = "main_approver", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Staff mainApprover;

    @JoinColumn(name = "alt_approver", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Staff alternateApprover;

    @Column(name = "approval_required")
    private Boolean approvalRequired;

    @JoinColumn(name = "template", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private ApprovalTemplate approvalTemplate;

    public Staff getMainApprover() {
        return mainApprover;
    }

    public void setMainApprover(Staff mainApprover) {
        this.mainApprover = mainApprover;
    }

    public Staff getAlternateApprover() {
        return alternateApprover;
    }

    public void setAlternateApprover(Staff alternateApprover) {
        this.alternateApprover = alternateApprover;
    }

    public Boolean getApprovalRequired() {
        return approvalRequired;
    }

    public void setApprovalRequired(Boolean approvalRequired) {
        this.approvalRequired = approvalRequired;
    }

    public ApprovalTemplate getApprovalTemplate() {
        return approvalTemplate;
    }

    public void setApprovalTemplate(ApprovalTemplate approvalTemplate) {
        this.approvalTemplate = approvalTemplate;
    }
}
