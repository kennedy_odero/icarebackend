package com.misoft.hospital.model.approval;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by kodero on 12/16/15.
 */
@Entity
@Table(name = "document_approvals")
public class ApprovalTemplate extends ModelBase{

    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "approvalTemplate", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @Filter(name = "filterByDeleted")
    private Collection<Approver> approvers;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Collection<Approver> getApprovers() {
        return approvers;
    }

    public void setApprovers(Collection<Approver> approvers) {
        this.approvers = approvers;
    }
}
