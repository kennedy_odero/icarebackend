package com.misoft.hospital.model.approval;

import com.misoft.hospital.model.base.ModelBase;

import javax.persistence.*;

/**
 * Created by kodero on 12/16/15.
 */
@Entity
@Table(name = "approver_approvals")
public class ApproverApproval extends ModelBase{

    @JoinColumn(name = "approver", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Approver approver;

    @Column(name = "approved")
    private Boolean approved;

    @JoinColumn(name = "document_approval", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private DocumentApproval documentApproval;

    public Approver getApprover() {
        return approver;
    }

    public void setApprover(Approver approver) {
        this.approver = approver;
    }

    public Boolean getApproved() {
        return approved;
    }

    public void setApproved(Boolean approved) {
        this.approved = approved;
    }

    public DocumentApproval getDocumentApproval() {
        return documentApproval;
    }

    public void setDocumentApproval(DocumentApproval documentApproval) {
        this.documentApproval = documentApproval;
    }
}
