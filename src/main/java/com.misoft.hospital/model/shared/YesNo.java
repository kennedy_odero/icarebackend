package com.misoft.hospital.model.shared;

/**
 * Created by larry on 3/30/2015.
 */
public enum YesNo {
    YES,
    NO
}
