package com.misoft.hospital.model.shared;

/**
 * Created by larry on 3/21/2015.
 */
public enum Status {
    ACTIVE,
    INACTIVE
}
