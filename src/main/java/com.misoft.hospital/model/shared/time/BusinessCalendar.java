package com.misoft.hospital.model.shared.time;

import com.misoft.hospital.model.base.AttributeModelBase;
import com.misoft.hospital.model.shared.ImmutableIterator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;


@Entity
@Table(name = "business_calendar")
public class BusinessCalendar extends AttributeModelBase {


    @Column(name = "NAME", nullable = false)
    @NotNull
    private String name;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "business_calendar_holidays", joinColumns = @JoinColumn(name = "BUSINESS_CALENDAR_ID"))
    protected Set<CalendarDate> holidays;

    /**
     * Should be rewritten for each particular organization
     * TODO load this from external file
     */
    static Set<CalendarDate> defaultHolidays() {
        return new HashSet<>();
    }

    /**
     * @param name
     * @param holidays
     */
    public BusinessCalendar(String name, Set<CalendarDate> holidays) {
        super();
        this.name = name;
        this.holidays = holidays;
    }

    public BusinessCalendar() {
        holidays = defaultHolidays();
    }

    public void addHolidays(Set<CalendarDate> days) {
        holidays.addAll(days);
    }

    public int getElapsedBusinessDays(CalendarInterval interval) {
        int tally = 0;
        Iterator<CalendarDate> iterator = businessDaysOnly(interval.daysIterator());
        while (iterator.hasNext()) {
            iterator.next();
            tally += 1;
        }
        return tally;
    }

    /*
     * @deprecated
     */
    public CalendarDate nearestBusinessDay(CalendarDate day) {
        if (isBusinessDay(day))
            return day;
        else
            return nextBusinessDay(day);
    }

    public boolean isHoliday(CalendarDate day) {
        return holidays.contains(day);
    }

    public boolean isWeekend(CalendarDate day) {
        Calendar calday = day.asJavaCalendarUniversalZoneMidnight();
        return (calday.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY)
                || (calday.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY);
    }

    public boolean isBusinessDay(CalendarDate day) {
        return !isWeekend(day) && !isHoliday(day);
    }


    public Iterator<CalendarDate> businessDaysOnly(final Iterator<CalendarDate> calendarDays) {
        return new ImmutableIterator<CalendarDate>() {
            CalendarDate lookAhead = null;

            {
                next();
            }

            public boolean hasNext() {
                return lookAhead != null;
            }

            public CalendarDate next() {
                CalendarDate next = lookAhead;
                lookAhead = nextBusinessDate();
                return next;
            }

            private CalendarDate nextBusinessDate() {
                CalendarDate result = null;
                do {
                    result = calendarDays.hasNext() ? (CalendarDate) calendarDays.next() : null;
                } while (!(result == null || isBusinessDay(result)));
                return result;
            }
        };
    }

    public CalendarDate plusBusinessDays(CalendarDate startDate,
                                         int numberOfDays) {
        if (numberOfDays < 0)
            throw new IllegalArgumentException(
                    "Negative numberOfDays not supported");
        Iterator<CalendarDate> iterator = CalendarInterval.everFrom(startDate).daysIterator();
        return nextNumberOfBusinessDays(numberOfDays, iterator);
    }

    public CalendarDate minusBusinessDays(CalendarDate startDate,
                                          int numberOfDays) {
        if (numberOfDays < 0)
            throw new IllegalArgumentException(
                    "Negative numberOfDays not supported");
        Iterator<CalendarDate> iterator = CalendarInterval.everPreceding(startDate).daysInReverseIterator();
        return nextNumberOfBusinessDays(numberOfDays, iterator);
    }

    private CalendarDate nextNumberOfBusinessDays(int numberOfDays, Iterator<CalendarDate> calendarDays) {
        Iterator businessDays = businessDaysOnly(calendarDays);
        CalendarDate result = null;
        for (int i = 0; i <= numberOfDays; i++) {
            result = (CalendarDate) businessDays.next();
        }
        return result;
    }

    public CalendarDate nextBusinessDay(CalendarDate startDate) {
        if (isBusinessDay(startDate))
            return plusBusinessDays(startDate, 1);
        else
            return plusBusinessDays(startDate, 0);
    }


    // Only for use by persistence mapping frameworks
    // <rant>These methods break encapsulation and we put them in here
    // begrudgingly</rant>
  /*  @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "business_calendar_holidays", joinColumns = @JoinColumn(name = "BUSINESS_CALENDAR_ID"))*/
    public Set<CalendarDate> getForPersistentMapping_Holidays() {
        return holidays;
    }

    // Only for use by persistence mapping frameworks
    // <rant>These methods break encapsulation and we put them in here
    // begrudgingly</rant>
    public void setForPersistentMapping_Holidays(Set<CalendarDate> holidays) {
        this.holidays = holidays;
    }

    //@Column(name = "NAME", nullable = false)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}