

package com.misoft.hospital.model.shared;

import java.util.Iterator;

abstract public class ImmutableIterator<T> implements Iterator<T> {

    public void remove() {
        throw new UnsupportedOperationException("sorry, no can do :-(");
    }

}