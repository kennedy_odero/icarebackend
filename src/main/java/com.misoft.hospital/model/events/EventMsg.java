package com.misoft.hospital.model.events;

import com.misoft.hospital.model.base.ModelBase;
import com.misoft.hospital.model.healthcare.ServiceRequest;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.Formula;

import javax.persistence.*;

/**
 * Created by kodero on 12/26/15.
 */
@Entity
@Table(name = "event_messages")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "event_kind")
@Filter(name = "filterByDeleted")
public class EventMsg extends ModelBase{

    @Column(name = "event_type")
    @Enumerated(EnumType.STRING)
    private EventType eventType;

    @Column(name = "event_details", columnDefinition = "text")
    private String eventDetails;

    @Formula("event_kind")
    private String eventKind;

    //for lack of a better place!!
    @JoinColumn(name = "service_request", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private ServiceRequest serviceRequest;

    @Column(name = "sr_fqn")
    private String serviceRequestFqn;

    public EventType getEventType() {
        return eventType;
    }

    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }

    public String getEventDetails() {
        return eventDetails;
    }

    public void setEventDetails(String eventDetails) {
        this.eventDetails = eventDetails;
    }

    public String getEventKind() {
        return eventKind;
    }

    public void setEventKind(String eventKind) {
        this.eventKind = eventKind;
    }

    public ServiceRequest getServiceRequest() {
        return serviceRequest;
    }

    public void setServiceRequest(ServiceRequest serviceRequest) {
        this.serviceRequest = serviceRequest;
    }

    public String getServiceRequestFqn() {
        return serviceRequestFqn;
    }

    public void setServiceRequestFqn(String serviceRequestFqn) {
        this.serviceRequestFqn = serviceRequestFqn;
    }
}
