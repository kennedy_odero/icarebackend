package com.misoft.hospital.model.events;

import com.misoft.hospital.model.admin.User;
import com.misoft.hospital.model.healthcare.RequestStatus;
import com.misoft.hospital.model.healthcare.ServiceRequest;

import javax.persistence.*;

/**
 * Created by kodero on 12/26/15.
 */
@Entity
@DiscriminatorValue("SR_STATUS_EVENT")
public class ServiceRequestStatusEvent extends EventMsg{

    @JoinColumn(name = "triggered_by", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private User triggeredBy;

    @Column(name = "request_status")
    @Enumerated(EnumType.STRING)
    private RequestStatus requestStatus;

    public User getTriggeredBy() {
        return triggeredBy;
    }

    public void setTriggeredBy(User triggeredBy) {
        this.triggeredBy = triggeredBy;
    }

    public RequestStatus getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(RequestStatus requestStatus) {
        this.requestStatus = requestStatus;
    }
}
