package com.misoft.hospital.model.events;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Created by kodero on 12/26/15.
 */
@Entity
@DiscriminatorValue("SERVER_EVENT")
public class ServerEvent extends EventMsg{
}
