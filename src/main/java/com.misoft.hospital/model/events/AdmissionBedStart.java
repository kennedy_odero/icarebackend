package com.misoft.hospital.model.events;

import com.misoft.hospital.model.healthcare.AdmissionBed;

/**
 * Created by kodero on 5/3/16.
 */
public class AdmissionBedStart {
    AdmissionBed admissionBed;

    public AdmissionBedStart(AdmissionBed admissionBed){
        this.admissionBed = admissionBed;
    }

    public AdmissionBed getAdmissionBed() {
        return admissionBed;
    }

    public void setAdmissionBed(AdmissionBed admissionBed) {
        this.admissionBed = admissionBed;
    }
}
