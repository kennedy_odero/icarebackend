package com.misoft.hospital.model.events;

/**
 * Created by kodero on 12/26/15.
 */
public enum EventType {
    INFO,
    SUCCESS,
    WARNING,
    ERROR
}
