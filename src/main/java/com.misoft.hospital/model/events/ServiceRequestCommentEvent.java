package com.misoft.hospital.model.events;

import com.misoft.hospital.model.admin.User;

import javax.persistence.*;

/**
 * Created by kodero on 12/26/15.
 */
@Entity
@DiscriminatorValue("SR_COMMENT_EVENT")
public class ServiceRequestCommentEvent extends EventMsg{
    @JoinColumn(name = "triggered_by", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private User commentBy;

    @Column(name = "request_status")
    private String comment;

    public User getCommentBy() {
        return commentBy;
    }

    public void setCommentBy(User commentBy) {
        this.commentBy = commentBy;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
