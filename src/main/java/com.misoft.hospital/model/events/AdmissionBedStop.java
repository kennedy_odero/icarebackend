package com.misoft.hospital.model.events;

import com.misoft.hospital.model.healthcare.AdmissionBed;

/**
 * Created by kodero on 5/3/16.
 */
public class AdmissionBedStop {
    AdmissionBed admissionBed;

    public AdmissionBedStop(AdmissionBed admissionBed){
        this.admissionBed = admissionBed;
    }

    public AdmissionBed getAdmissionBed() {
        return admissionBed;
    }

    public void setAdmissionBed(AdmissionBed admissionBed) {
        this.admissionBed = admissionBed;
    }
}
