package com.misoft.hospital.model.events;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by kodero on 11/6/16.
 */
public class BatchBillingEvent {
    private Collection<Object> requests = new ArrayList<>();

    public BatchBillingEvent(Collection<Object> requests){
        this.requests = requests;
    }

    public BatchBillingEvent(){

    }

    public void addRequest(Object request){
        this.requests.add(request);
    }

    public Collection<Object> getRequests(){
        return this.requests;
    }
}
