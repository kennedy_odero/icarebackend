package com.misoft.hospital.model.mohregisters;

import com.misoft.hospital.model.base.ModelBase;
import com.misoft.hospital.model.setup.Gender;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by kodero on 10/18/15.
 */

@Entity
@Table(name = "admission_register")
public class AdmissionRegister extends ModelBase{

    @Column(name = "date_requested")
    private Date dateOfAdmission;

    @Column(name = "patient_no")
    private String patientNo;

    @Column(name = "request_no")
    private Long requestNo;

    @Column(name = "surname")
    private String surname;

    @Column(name = "other_names")
    private String otherNames;

    @Column(name = "age")
    private BigDecimal age;

    @Column(name = "gender")
    private String gender;

    @Column(name = "sub_location")
    private String subLocation;

    @Column(name = "village")
    private String village;

    @Column(name = "box_no")
    private String address;

    @Column(name = "hiv_test_status")
    private String hivTestStatus;

    @Column(name = "hiv_status")
    private String hivStatus;

    @Column(name = "nutrition")
    private String nutrition;

    @Column(name = "diagnosis")
    private String diagnosis;

    @Column(name = "treatment_no")
    private String prescriptionNo;

    @Column(name = "date_to")
    private Date dateOfDischarge;

    @Column(name = "adm_outcome")
    private String outcome;

    @Column(name = "notes")
    private String notes;

    public Date getDateOfAdmission() {
        return dateOfAdmission;
    }

    public void setDateOfAdmission(Date dateOfAdmission) {
        this.dateOfAdmission = dateOfAdmission;
    }

    public Long getRequestNo() {
        return requestNo;
    }

    public void setRequestNo(Long requestNo) {
        this.requestNo = requestNo;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getOtherNames() {
        return otherNames;
    }

    public void setOtherNames(String otherNames) {
        this.otherNames = otherNames;
    }

    public BigDecimal getAge() {
        return age;
    }

    public void setAge(BigDecimal age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getSubLocation() {
        return subLocation;
    }

    public void setSubLocation(String subLocation) {
        this.subLocation = subLocation;
    }

    public String getVillage() {
        return village;
    }

    public void setVillage(String village) {
        this.village = village;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getHivTestStatus() {
        return hivTestStatus;
    }

    public void setHivTestStatus(String hivTestStatus) {
        this.hivTestStatus = hivTestStatus;
    }

    public String getHivStatus() {
        return hivStatus;
    }

    public void setHivStatus(String hivStatus) {
        this.hivStatus = hivStatus;
    }

    public String getNutrition() {
        return nutrition;
    }

    public void setNutrition(String nutrition) {
        this.nutrition = nutrition;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public String getPrescriptionNo() {
        return prescriptionNo;
    }

    public void setPrescriptionNo(String prescriptionNo) {
        this.prescriptionNo = prescriptionNo;
    }

    public Date getDateOfDischarge() {
        return dateOfDischarge;
    }

    public void setDateOfDischarge(Date dateOfDischarge) {
        this.dateOfDischarge = dateOfDischarge;
    }

    public String getOutcome() {
        return outcome;
    }

    public void setOutcome(String outcome) {
        this.outcome = outcome;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getPatientNo() {
        return patientNo;
    }

    public void setPatientNo(String patientNo) {
        this.patientNo = patientNo;
    }
}
