package com.misoft.hospital.model.mohregisters;

import com.misoft.hospital.model.base.ModelBase;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by kodero on 10/18/15.
 */
@Entity
@Table(name = "laboratory_register")
public class LaboratoryRegister extends ModelBase{

    @Column(name = "date_requested")
    private Date dateRequested;

    @Column(name = "patient_no")
    private String patientNo;

    @Column(name = "request_no")
    private Long requestNo;

    @Column(name = "visit_type")
    private String visitType;

    @Column(name = "surname")
    private String surname;

    @Column(name = "other_names")
    private String otherNames;

    @Column(name = "age")
    private BigDecimal age;

    @Column(name = "gender")
    private String gender;

    @Column(name = "village")
    private String village;

    @Column(name = "box_no")
    private String address;

    @Column(name = "diagnosis")
    private String diagnosis;

    @Column(name = "prior_treatment")
    private String priorTreatment;

    @Column(name = "test_sample")
    private String testSample;

    @Column(name = "_condition")
    private String _condition;

    @Column(name = "investigation_required")
    private String investigationRequired;

    @Column(name = "date_sample_collected")
    private Date dateSampleCollected;

    @Column(name = "date_sample_received")
    private Date dateSampleReceived;

    @Column(name = "date_sample_analysed")
    private Date dateSampleAnalysed;

    @Column(name = "date_results_disp")
    private Date dateResultsDispatched;

    @Column(name = "results")
    private String results;

    @Column(name = "comments")
    private String comments;

    @Column(name = "s_surname")
    private String staffSurname;

    @Column(name = "s_other_names")
    private String staffOtherNames;

    public Date getDateRequested() {
        return dateRequested;
    }

    public void setDateRequested(Date dateRequested) {
        this.dateRequested = dateRequested;
    }

    public String getPatientNo() {
        return patientNo;
    }

    public void setPatientNo(String patientNo) {
        this.patientNo = patientNo;
    }

    public Long getRequestNo() {
        return requestNo;
    }

    public void setRequestNo(Long requestNo) {
        this.requestNo = requestNo;
    }

    public String getVisitType() {
        return visitType;
    }

    public void setVisitType(String visitType) {
        this.visitType = visitType;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getOtherNames() {
        return otherNames;
    }

    public void setOtherNames(String otherNames) {
        this.otherNames = otherNames;
    }

    public BigDecimal getAge() {
        return age;
    }

    public void setAge(BigDecimal age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getVillage() {
        return village;
    }

    public void setVillage(String village) {
        this.village = village;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public String getPriorTreatment() {
        return priorTreatment;
    }

    public void setPriorTreatment(String priorTreatment) {
        this.priorTreatment = priorTreatment;
    }

    public String getTestSample() {
        return testSample;
    }

    public void setTestSample(String testSample) {
        this.testSample = testSample;
    }

    public String get_condition() {
        return _condition;
    }

    public void set_condition(String _condition) {
        this._condition = _condition;
    }

    public String getInvestigationRequired() {
        return investigationRequired;
    }

    public void setInvestigationRequired(String investigationRequired) {
        this.investigationRequired = investigationRequired;
    }

    public Date getDateSampleCollected() {
        return dateSampleCollected;
    }

    public void setDateSampleCollected(Date dateSampleCollected) {
        this.dateSampleCollected = dateSampleCollected;
    }

    public Date getDateSampleReceived() {
        return dateSampleReceived;
    }

    public void setDateSampleReceived(Date dateSampleReceived) {
        this.dateSampleReceived = dateSampleReceived;
    }

    public Date getDateSampleAnalysed() {
        return dateSampleAnalysed;
    }

    public void setDateSampleAnalysed(Date dateSampleAnalysed) {
        this.dateSampleAnalysed = dateSampleAnalysed;
    }

    public Date getDateResultsDispatched() {
        return dateResultsDispatched;
    }

    public void setDateResultsDispatched(Date dateResultsDispatched) {
        this.dateResultsDispatched = dateResultsDispatched;
    }

    public String getResults() {
        return results;
    }

    public void setResults(String results) {
        this.results = results;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getStaffSurname() {
        return staffSurname;
    }

    public void setStaffSurname(String staffSurname) {
        this.staffSurname = staffSurname;
    }

    public String getStaffOtherNames() {
        return staffOtherNames;
    }

    public void setStaffOtherNames(String staffOtherNames) {
        this.staffOtherNames = staffOtherNames;
    }
}
