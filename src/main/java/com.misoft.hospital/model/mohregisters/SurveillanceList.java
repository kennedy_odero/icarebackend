package com.misoft.hospital.model.mohregisters;

/**
 * Created by kodero on 10/20/15.
 */
public enum SurveillanceList {
    UNDER_5,
    OVER_5
}
