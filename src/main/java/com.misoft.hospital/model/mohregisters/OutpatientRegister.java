package com.misoft.hospital.model.mohregisters;

import com.misoft.hospital.model.base.ModelBase;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by kodero on 10/18/15.
 */
@Entity
@Table(name = "outpatient_register")
public class OutpatientRegister extends ModelBase{

    @Column(name = "date_requested")
    private Date dateRequested;

    @Column(name = "patient_no")
    private String patientNo;

    @Column(name = "visit_type")
    private String visitType;

    @Column(name = "surname")
    private String surname;

    @Column(name = "other_names")
    private String otherNames;

    @Column(name = "age")
    private BigDecimal age;

    @Column(name = "gender")
    private String gender;

    @Column(name = "sub_location")
    private String subLocation;

    @Column(name = "village")
    private String village;

    @Column(name = "box_no")
    private String address;

    @Column(name = "danger_signs")
    private String dangerSigns;

    @Column(name = "visual_acuity")
    private String visualAcuity;

    @Column(name = "duration_of_illness")
    private String durationOfIllness;

    @Column(name = "diagnosis")
    private String diagnosis;

    @Column(name = "hiv_test_status")
    private String hivTestStatus;

    @Column(name = "hiv_status")
    private String hivStatus;

    @Column(name = "nutrition")
    private String nutrition;

    @Column(name = "treatment")
    private String treatment;

    @Column(name = "notes")
    private String notes;

    public Date getDateRequested() {
        return dateRequested;
    }

    public void setDateRequested(Date dateRequested) {
        this.dateRequested = dateRequested;
    }

    public String getPatientNo() {
        return patientNo;
    }

    public void setPatientNo(String patientNo) {
        this.patientNo = patientNo;
    }

    public String getVisitType() {
        return visitType;
    }

    public void setVisitType(String visitType) {
        this.visitType = visitType;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getOtherNames() {
        return otherNames;
    }

    public void setOtherNames(String otherNames) {
        this.otherNames = otherNames;
    }

    public BigDecimal getAge() {
        return age;
    }

    public void setAge(BigDecimal age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getSubLocation() {
        return subLocation;
    }

    public void setSubLocation(String subLocation) {
        this.subLocation = subLocation;
    }

    public String getVillage() {
        return village;
    }

    public void setVillage(String village) {
        this.village = village;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDangerSigns() {
        return dangerSigns;
    }

    public void setDangerSigns(String dangerSigns) {
        this.dangerSigns = dangerSigns;
    }

    public String getVisualAcuity() {
        return visualAcuity;
    }

    public void setVisualAcuity(String visualAcuity) {
        this.visualAcuity = visualAcuity;
    }

    public String getDurationOfIllness() {
        return durationOfIllness;
    }

    public void setDurationOfIllness(String durationOfIllness) {
        this.durationOfIllness = durationOfIllness;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public String getHivTestStatus() {
        return hivTestStatus;
    }

    public void setHivTestStatus(String hivTestStatus) {
        this.hivTestStatus = hivTestStatus;
    }

    public String getHivStatus() {
        return hivStatus;
    }

    public void setHivStatus(String hivStatus) {
        this.hivStatus = hivStatus;
    }

    public String getNutrition() {
        return nutrition;
    }

    public void setNutrition(String nutrition) {
        this.nutrition = nutrition;
    }

    public String getTreatment() {
        return treatment;
    }

    public void setTreatment(String treatment) {
        this.treatment = treatment;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
}
