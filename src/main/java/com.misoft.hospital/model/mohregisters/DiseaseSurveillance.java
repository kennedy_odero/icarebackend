package com.misoft.hospital.model.mohregisters;

import com.misoft.hospital.model.base.ModelBase;
import com.misoft.hospital.model.setup.Disease;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by kodero on 10/20/15.
 */
@Entity
@Table(name = "disease_surveillance")
public class DiseaseSurveillance extends ModelBase{

    @Column(name = "list")
    private SurveillanceList surveillanceList;

    @Column(name = "effective_date")
    private Date effectiveDate;

    @JoinColumn(name = "disease", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Disease disease;

    @Column(name = "_index")
    private Integer index;

    public SurveillanceList getSurveillanceList() {
        return surveillanceList;
    }

    public void setSurveillanceList(SurveillanceList surveillanceList) {
        this.surveillanceList = surveillanceList;
    }

    public Disease getDisease() {
        return disease;
    }

    public void setDisease(Disease disease) {
        this.disease = disease;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public Date getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }
}
