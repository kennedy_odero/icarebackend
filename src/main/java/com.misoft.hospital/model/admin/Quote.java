package com.misoft.hospital.model.admin;

import org.hibernate.annotations.Type;

import javax.persistence.*;

/**
 * Created by kodero on 12/30/14.
 */
@Entity
@Table(name = "quotes")
public class Quote {

    @Id
    @GeneratedValue(generator = "lemrUUIDGenerator")
    @Column(name = "id")
    private String id;

    @Column(name = "quote", length = 2000, columnDefinition="Text")
    @Type(type="text")
    private String quote;

    @Column(name = "author")
    private String author;

    @Column(name = "genre")
    private String genre;

    public Quote(){

    }

    public Quote(String quote, String author, String genre){
        setQuote(quote);
        setAuthor(author);
        setGenre(genre);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getQuote() {
        return quote;
    }

    public void setQuote(String quote) {
        this.quote = quote;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }
}
