package com.misoft.hospital.model.admin;

/**
 * Created by kodero on 12/24/14.
 */
public enum PeriodStatus {
    OPEN,
    CLOSED
}
