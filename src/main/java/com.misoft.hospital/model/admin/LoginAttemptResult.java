package com.misoft.hospital.model.admin;

/**
 * Created by kodero on 10/26/15.
 */
public enum LoginAttemptResult {
    SUCCESS,
    FAILURE
}
