package com.misoft.hospital.model.admin;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.*;

/**
 * Created by kodero on 7/1/15.
 */
@Entity
@Table(name = "resources")
@Filter(name = "filterByDeleted")
@NamedQuery(name = "findByUrlAndNamespace", query = "select r.content from FrontendResource r where r.resourceUrl =:url and r.folder =:namespace")
public class FrontendResource extends ModelBase{

    @Column(name = "name")
    private String name;

    @Column(name = "url")
    private String resourceUrl;

    @Column(name = "content", columnDefinition = "text")
    private String content;

    @Column(name = "resource_type")
    @Enumerated(EnumType.STRING)
    private ResourceType resourceType;

    @Column(name = "folder")
    private String folder;

    public FrontendResource(){

    }

    public FrontendResource(String id, String name, String folder, String resourceUrl, ResourceType resourceType){
        setId(id);
        setName(name);
        setFolder(folder);
        setResourceUrl(resourceUrl);
        setResourceType(resourceType);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getResourceUrl() {
        return resourceUrl;
    }

    public void setResourceUrl(String resourceUrl) {
        this.resourceUrl = resourceUrl;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public ResourceType getResourceType() {
        return resourceType;
    }

    public void setResourceType(ResourceType resourceType) {
        this.resourceType = resourceType;
    }

    public String getFolder() {
        return folder;
    }

    public void setFolder(String folder) {
        this.folder = folder;
    }
}
