package com.misoft.hospital.model.admin;

/**
 * Created by kodero on 10/27/15.
 */
public enum AccountStatus {
    Active,
    Inactive,
    Locked,
    Suspended
}
