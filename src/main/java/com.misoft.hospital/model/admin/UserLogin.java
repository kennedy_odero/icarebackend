package com.misoft.hospital.model.admin;

import com.misoft.hospital.model.base.ModelBase;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by kodero on 10/26/15.
 */
@Entity
@Table(name = "user_logins")
public class UserLogin extends ModelBase{

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date_loged_in")
    private Date dateLogedIn;

    @JoinColumn(name = "user", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private User user;

    @Column(name = "ip")
    private String ip;

    @Column(name = "user_agent")
    private String userAgent;

    @Column(name = "referer")
    private String referer;

    @Column(name = "result")
    @Enumerated(EnumType.STRING)
    private LoginAttemptResult loginAttemptResult;//success/failure

    public UserLogin(){

    }

    public UserLogin(Date dateLogedIn, User user, String ip, String userAgent, String referer, LoginAttemptResult result){
        setDateLogedIn(dateLogedIn);
        setUser(user);
        setIp(ip);
        setUserAgent(userAgent);
        setReferer(referer);
        setLoginAttemptResult(result);
    }

    public Date getDateLogedIn() {
        return dateLogedIn;
    }

    public void setDateLogedIn(Date dateLogedIn) {
        this.dateLogedIn = dateLogedIn;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public String getReferer() {
        return referer;
    }

    public void setReferer(String referer) {
        this.referer = referer;
    }

    public LoginAttemptResult getLoginAttemptResult() {
        return loginAttemptResult;
    }

    public void setLoginAttemptResult(LoginAttemptResult loginAttemptResult) {
        this.loginAttemptResult = loginAttemptResult;
    }
}
