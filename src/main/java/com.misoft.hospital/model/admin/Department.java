package com.misoft.hospital.model.admin;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by kodero on 7/24/15.
 */
@Entity
@Table(name = "departments")
@Filter(name = "filterByDeleted")
public class Department extends ModelBase{

    @Column(name = "name")
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
