package com.misoft.hospital.model.admin;

/**
 * Created by kodero on 7/1/15.
 */
public enum ResourceType {
    HTML,
    JAVASCRIPT,
    CSS
}
