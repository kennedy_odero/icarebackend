
package com.misoft.hospital.model.util;

import com.misoft.hospital.model.base.AttributeModelBase;

import javax.persistence.*;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author mokua
 */
@Entity
@Table(name = "group_members", schema = "")
@XmlRootElement
@org.hibernate.annotations.Filter(name = "filterByDeleted")
@NamedQueries({
        @NamedQuery(name = "GroupMember.findAll", query = "SELECT g FROM GroupMember g"),
        @NamedQuery(name = "GroupMember.findById", query = "SELECT g FROM GroupMember g WHERE g.id = :id"),
        @NamedQuery(name = "GroupMember.findByCreatedAt", query = "SELECT g FROM GroupMember g WHERE g.createdAt = :createdAt"),
        @NamedQuery(name = "GroupMember.findByUpdatedAt", query = "SELECT g FROM GroupMember g WHERE g.updatedAt = :updatedAt"),
        @NamedQuery(name = "GroupMember.findByCreatedById", query = "SELECT g FROM GroupMember g WHERE g.createdByUser = :createdById"),
        @NamedQuery(name = "GroupMember.findByUpdatedById", query = "SELECT g FROM GroupMember g WHERE g.createdByUser = :updatedById"),
        @NamedQuery(name = "GroupMember.findByGroupableId", query = "SELECT g FROM GroupMember g WHERE g.groupableId = :groupableId"),
        @NamedQuery(name = "GroupMember.findByGroupableType", query = "SELECT g FROM GroupMember g WHERE g.groupableType = :groupableType")})
public class GroupMember extends AttributeModelBase {


    @Column(name = "groupable_id")
    private Integer groupableId;

    @Size(max = 255)
    @Column(name = "groupable_type")
    private String groupableType;

    @JoinColumn(name = "group_", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Groups group;

    public GroupMember() {
    }


    public Integer getGroupableId() {
        return groupableId;
    }

    public void setGroupableId(Integer groupableId) {
        this.groupableId = groupableId;
    }

    public String getGroupableType() {
        return groupableType;
    }

    public void setGroupableType(String groupableType) {
        this.groupableType = groupableType;
    }

    public Groups getGroup() {
        return group;
    }

    public void setGroup(Groups groupId) {
        this.group = groupId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GroupMember)) {
            return false;
        }
        GroupMember other = (GroupMember) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "GroupMember[ id=" + id + " ]";
    }
}
