
package com.misoft.hospital.model.util;


import com.misoft.hospital.model.base.AttributeModelBase;

import javax.persistence.*;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

/**
 * @author mokua
 */
@Entity
@Table(name = "documents", schema = "")
@XmlRootElement
@org.hibernate.annotations.Filter(name = "filterByDeleted")
@NamedQueries({
        @NamedQuery(name = "Document.findAll", query = "SELECT d FROM Document d"),
        @NamedQuery(name = "Document.findById", query = "SELECT d FROM Document d WHERE d.id = :id"),
        @NamedQuery(name = "Document.findByCreatedAt", query = "SELECT d FROM Document d WHERE d.createdAt = :createdAt"),
        @NamedQuery(name = "Document.findByUpdatedAt", query = "SELECT d FROM Document d WHERE d.updatedAt = :updatedAt"),
        @NamedQuery(name = "Document.findByDocumentFileName", query = "SELECT d FROM Document d WHERE d.documentFileName = :documentFileName"),
        @NamedQuery(name = "Document.findByDocumentContentType", query = "SELECT d FROM Document d WHERE d.documentContentType = :documentContentType"),
        @NamedQuery(name = "Document.findByDocumentFileSize", query = "SELECT d FROM Document d WHERE d.documentFileSize = :documentFileSize"),
        @NamedQuery(name = "Document.findByDocumentUpdatedAt", query = "SELECT d FROM Document d WHERE d.documentUpdatedAt = :documentUpdatedAt")})
public class Document extends AttributeModelBase {


    @Size(max = 255)
    @Column(name = "document_file_name")
    private String documentFileName;

    @Size(max = 255)
    @Column(name = "document_content_type")
    private String documentContentType;

    @Column(name = "document_file_size")
    private Integer documentFileSize;

    @Column(name = "document_updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date documentUpdatedAt;


    public Document() {
    }


    public String getDocumentFileName() {
        return documentFileName;
    }

    public void setDocumentFileName(String documentFileName) {
        this.documentFileName = documentFileName;
    }

    public String getDocumentContentType() {
        return documentContentType;
    }

    public void setDocumentContentType(String documentContentType) {
        this.documentContentType = documentContentType;
    }

    public Integer getDocumentFileSize() {
        return documentFileSize;
    }

    public void setDocumentFileSize(Integer documentFileSize) {
        this.documentFileSize = documentFileSize;
    }

    public Date getDocumentUpdatedAt() {
        return documentUpdatedAt;
    }

    public void setDocumentUpdatedAt(Date documentUpdatedAt) {
        this.documentUpdatedAt = documentUpdatedAt;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Document)) {
            return false;
        }
        Document other = (Document) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Document[ id=" + id + " ]";
    }

}
