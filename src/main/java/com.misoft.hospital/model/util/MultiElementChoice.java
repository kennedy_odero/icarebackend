
package com.misoft.hospital.model.util;

import com.misoft.hospital.model.base.AttributeModelBase;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author mokua
 */
@Entity
@Table(name = "multi_element_choices", schema = "")
@XmlRootElement
@org.hibernate.annotations.Filter(name = "filterByDeleted")
@NamedQueries({
        @NamedQuery(name = "MultiElementChoice.findAll", query = "SELECT m FROM MultiElementChoice m"),
        @NamedQuery(name = "MultiElementChoice.findById", query = "SELECT m FROM MultiElementChoice m WHERE m.id = :id"),
        @NamedQuery(name = "MultiElementChoice.findByCreatedAt", query = "SELECT m FROM MultiElementChoice m WHERE m.createdAt = :createdAt"),
        @NamedQuery(name = "MultiElementChoice.findByUpdatedAt", query = "SELECT m FROM MultiElementChoice m WHERE m.updatedAt = :updatedAt"),
        @NamedQuery(name = "MultiElementChoice.findByTargetId", query = "SELECT m FROM MultiElementChoice m WHERE m.targetId = :targetId")})
public class MultiElementChoice extends AttributeModelBase {


    @Basic(optional = false)
    @NotNull
    @Column(name = "target_id")
    private int targetId;

    @JoinColumn(name = "multi_element_value", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private MultiElementValue multiElementValue;

    public MultiElementChoice() {
    }


    public int getTargetId() {
        return targetId;
    }

    public void setTargetId(int targetId) {
        this.targetId = targetId;
    }

    public MultiElementValue getMultiElementValue() {
        return multiElementValue;
    }

    public void setMultiElementValue(MultiElementValue multiElementValueId) {
        this.multiElementValue = multiElementValueId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MultiElementChoice)) {
            return false;
        }
        MultiElementChoice other = (MultiElementChoice) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "MultiElementChoice[ id=" + id + " ]";
    }

}
