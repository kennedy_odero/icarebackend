package com.misoft.hospital.model.util;

import com.misoft.hospital.model.base.ModelBase;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by kodero on 10/18/14.
 */
@Entity
@Table(name = "mapped_entities")
public class MappedEntity extends ModelBase {

    @Column(name = "name", unique = true)
    private String name;

    @Column(name = "fqn", unique = true)
    private String fqn;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFqn() {
        return fqn;
    }

    public void setFqn(String fqn) {
        this.fqn = fqn;
    }
}
