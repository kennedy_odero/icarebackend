
package com.misoft.hospital.model.util;

import com.misoft.hospital.model.base.AttributeModelBase;

import javax.persistence.*;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.Collection;

/**
 * @author mokua
 */
@Entity
@Table(schema = "")
@XmlRootElement
@org.hibernate.annotations.Filter(name = "filterByDeleted")
@NamedQueries({
        @NamedQuery(name = "Groups.findAll", query = "SELECT g FROM Groups g"),
        @NamedQuery(name = "Groups.findById", query = "SELECT g FROM Groups g WHERE g.id = :id"),
        @NamedQuery(name = "Groups.findByCreatedAt", query = "SELECT g FROM Groups g WHERE g.createdAt = :createdAt"),
        @NamedQuery(name = "Groups.findByUpdatedAt", query = "SELECT g FROM Groups g WHERE g.updatedAt = :updatedAt"),
        @NamedQuery(name = "Groups.findByName", query = "SELECT g FROM Groups g WHERE g.name = :name"),
        @NamedQuery(name = "Groups.findByDeprecated", query = "SELECT g FROM Groups g WHERE g.deprecated = :deprecated")})
public class Groups extends AttributeModelBase {


    @Size(max = 255)
    private String name;

    private Boolean deprecated;

    @OneToMany(mappedBy = "group", fetch = FetchType.LAZY, cascade = CascadeType.ALL /*, orphanRemoval = true*/)
    @org.hibernate.annotations.Filter(name = "filterByDeleted")
    private Collection<GroupMember> groupMembers;

    public Groups() {
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getDeprecated() {
        return deprecated;
    }

    public void setDeprecated(Boolean deprecated) {
        this.deprecated = deprecated;
    }

    @XmlTransient
    public Collection<GroupMember> getGroupMembers() {
        return groupMembers;
    }

    public void setGroupMembers(Collection<GroupMember> groupMemberCollection) {
        this.groupMembers = groupMemberCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Groups)) {
            return false;
        }
        Groups other = (Groups) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Groups[ id=" + id + " ]";
    }

}
