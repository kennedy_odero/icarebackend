package com.misoft.hospital.model.util;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author mokua
 */
@Entity
@Table(name = "lemr_seq_gen")
public class SeqGen implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;

    @Column(name = "SEQ_NAME", nullable = false, updatable = false, unique = true)
    private String sequenceName;

    @Column(name = "NEXT_VAL", nullable = false, updatable = true)
    private Long nextVal;

    /**
     *
     */
    public SeqGen() {
    }

    /**
     * @param sequenceName
     * @param nextVal
     */
    public SeqGen(String sequenceName, Long nextVal) {

        this.sequenceName = sequenceName;
        this.nextVal = nextVal;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the sequenceName
     */
    public String getSequenceName() {
        return sequenceName;
    }

    /**
     * @param sequenceName the sequenceName to set
     */
    public void setSequenceName(String sequenceName) {
        this.sequenceName = sequenceName;
    }


    public Long getNextVal() {
        return nextVal;
    }

    public void setNextVal(Long nextVal) {
        this.nextVal = nextVal;
    }
}
