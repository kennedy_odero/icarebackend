

package com.misoft.hospital.model.util;

import com.misoft.hospital.model.base.ModelBase;
import com.misoft.hospital.model.base.ModelBaseGroup;
import org.codehaus.jackson.annotate.JsonCreator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.ArrayList;
import java.util.Collection;

/**
 * @author mokua
 */
@Entity
@Table(name = "multi_element_groups", schema = "")
@XmlRootElement
@org.hibernate.annotations.Filter(name = "filterByDeleted")
@NamedQueries({
        @NamedQuery(name = "MultiElementGroup.findAll", query = "SELECT m FROM MultiElementGroup m"),
        @NamedQuery(name = "MultiElementGroup.findById", query = "SELECT m FROM MultiElementGroup m WHERE m.id = :id"),
        @NamedQuery(name = "MultiElementGroup.findByCreatedAt", query = "SELECT m FROM MultiElementGroup m WHERE m.createdAt = :createdAt"),
        @NamedQuery(name = "MultiElementGroup.findByUpdatedAt", query = "SELECT m FROM MultiElementGroup m WHERE m.updatedAt = :updatedAt"),
        @NamedQuery(name = "MultiElementGroup.findByTargetClassName", query = "SELECT m FROM MultiElementGroup m WHERE m.targetClassName = :targetClassName"),
        @NamedQuery(name = "MultiElementGroup.findByName", query = "SELECT m FROM MultiElementGroup m WHERE m.name = :name"),
        @NamedQuery(name = "MultiElementGroup.findByDescription", query = "SELECT m FROM MultiElementGroup m WHERE m.description = :description")})
public class MultiElementGroup extends ModelBase {

    public static enum EditorType {
        TextInput("TextInput"),  // text field
        AdaptiveTextInput("AdaptiveTextInput"), // text area
        RichTextInput("RichTextInput"),    // custom editor
        SelectOne("SelectOne"),        //drop down
        NumberRange("NumberRange");       // slider

        private String name;

        EditorType(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return name;
        }

        @JsonCreator
        public static EditorType create(String value) {
            if (value == null) {
                throw new IllegalArgumentException();
            }
            for (EditorType v : values()) {
                if (value.equals(v.getName())) {
                    return v;
                }
            }
            throw new IllegalArgumentException();
        }

        public String getName() {
            return name;
        }
    }


    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "target_class_name")
    private String targetClassName;

    @Size(max = 255)
    private String name;

    @Size(max = 255)
    private String label;

    @Size(max = 255)
    private String description;

    @Column(name = "editor_type")
    @Enumerated(EnumType.STRING)
    private EditorType editorType;

    @Column(name = "min_value")
    private Integer min;

    @Column(name = "max_value")
    private Integer max;


    @OneToMany(mappedBy = "multiElementGroup", fetch = FetchType.LAZY, cascade = CascadeType.ALL /*, orphanRemoval = true*/)
    @org.hibernate.annotations.Filter(name = "filterByDeleted")
    private Collection<MultiElementValue> multiElementValues;

    @OneToMany(mappedBy = "multiElementGroup", fetch = FetchType.LAZY)
    @org.hibernate.annotations.Filter(name = "filterByDeleted")
    private Collection<ModelBaseGroup> attributes = new ArrayList<>();

    public MultiElementGroup() {
    }

    public MultiElementGroup(String targetClassName, String name, String label) {
        this.targetClassName = targetClassName;
        this.name = name;
        this.label = label;
    }

    public MultiElementGroup(Integer max, Integer min, String name, String targetClassName, EditorType editorType, String label, String description) {
        this.max = max;
        this.min = min;
        this.name = name;
        this.targetClassName = targetClassName;
        this.editorType = editorType;
        this.label = label;
        this.description = description;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getTargetClassName() {
        return targetClassName;
    }

    public void setTargetClassName(String targetClassName) {
        this.targetClassName = targetClassName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setAttributes(Collection<ModelBaseGroup> attributes) {
        this.attributes = attributes;
    }

    public EditorType getEditorType() {
        return editorType;
    }

    public void setEditorType(EditorType editorType) {
        this.editorType = editorType;
    }

    public Integer getMax() {
        return max;
    }

    public void setMax(Integer max) {
        this.max = max;
    }

    public Integer getMin() {
        return min;
    }

    public void setMin(Integer min) {
        this.min = min;
    }

    @XmlTransient
    public Collection<MultiElementValue> getMultiElementValues() {
        return multiElementValues;
    }

    public void setMultiElementValues(Collection<MultiElementValue> multiElementValueCollection) {
        this.multiElementValues = multiElementValueCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MultiElementGroup)) {
            return false;
        }
        MultiElementGroup other = (MultiElementGroup) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "MultiElementGroup[ id=" + id + " ]";
    }


    public Collection<ModelBaseGroup> getAttributes() {
        return attributes;
    }
}
