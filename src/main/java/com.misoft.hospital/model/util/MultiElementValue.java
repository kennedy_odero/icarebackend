package com.misoft.hospital.model.util;

import com.misoft.hospital.model.base.ModelBase;

import javax.persistence.*;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.Collection;

/**
 * @author mokua
 */
@Entity
@Table(name = "multi_element_values", schema = "")
@XmlRootElement
@org.hibernate.annotations.Filter(name = "filterByDeleted")
@NamedQueries({
        @NamedQuery(name = "MultiElementValue.findAll", query = "SELECT m FROM MultiElementValue m"),
        @NamedQuery(name = "MultiElementValue.findById", query = "SELECT m FROM MultiElementValue m WHERE m.id = :id"),
        @NamedQuery(name = "MultiElementValue.findByCreatedAt", query = "SELECT m FROM MultiElementValue m WHERE m.createdAt = :createdAt"),
        @NamedQuery(name = "MultiElementValue.findByUpdatedAt", query = "SELECT m FROM MultiElementValue m WHERE m.updatedAt = :updatedAt"),
        @NamedQuery(name = "MultiElementValue.findByDescription", query = "SELECT m FROM MultiElementValue m WHERE m.description = :description"),
        @NamedQuery(name = "MultiElementValue.findByValue", query = "SELECT m FROM MultiElementValue m WHERE m.value = :value")})
public class MultiElementValue extends ModelBase {


    @Size(max = 255)
    private String description;

    @Size(max = 255)
    private String value;


    @OneToMany(cascade = CascadeType.ALL, mappedBy = "multiElementValue", fetch = FetchType.LAZY)
    @org.hibernate.annotations.Filter(name = "filterByDeleted")
    private Collection<MultiElementChoice> multiElementChoices;

    @JoinColumn(name = "multi_element_group", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private MultiElementGroup multiElementGroup;

    @OneToMany(mappedBy = "dependentMultiElementValue", fetch = FetchType.LAZY)
    @org.hibernate.annotations.Filter(name = "filterByDeleted")
    private Collection<MultiElementValue> multiElementValues;

    @JoinColumn(name = "dependent_multi_element_value", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private MultiElementValue dependentMultiElementValue;

    public MultiElementValue() {
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }


    @XmlTransient
    public Collection<MultiElementChoice> getMultiElementChoices() {
        return multiElementChoices;
    }

    public void setMultiElementChoices(Collection<MultiElementChoice> multiElementChoiceCollection) {
        this.multiElementChoices = multiElementChoiceCollection;
    }

    public MultiElementGroup getMultiElementGroup() {
        return multiElementGroup;
    }

    public void setMultiElementGroup(MultiElementGroup multiElementGroupId) {
        this.multiElementGroup = multiElementGroupId;
    }

    @XmlTransient
    public Collection<MultiElementValue> getMultiElementValues() {
        return multiElementValues;
    }

    public void setMultiElementValues(Collection<MultiElementValue> multiElementValueCollection) {
        this.multiElementValues = multiElementValueCollection;
    }

    public MultiElementValue getDependentMultiElementValue() {
        return dependentMultiElementValue;
    }

    public void setDependentMultiElementValue(MultiElementValue dependentMultiElementValueId) {
        this.dependentMultiElementValue = dependentMultiElementValueId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MultiElementValue)) {
            return false;
        }
        MultiElementValue other = (MultiElementValue) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "MultiElementValue[ id=" + id + " ]";
    }

}
