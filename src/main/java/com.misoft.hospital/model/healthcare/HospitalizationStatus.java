package com.misoft.hospital.model.healthcare;

/**
 * Created by kodero on 2/22/16.
 */
public enum HospitalizationStatus {
    HOSPITALIZED,
    DISCHARGED,
    DECEASED
}
