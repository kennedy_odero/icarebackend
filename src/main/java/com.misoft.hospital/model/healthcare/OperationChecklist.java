package com.misoft.hospital.model.healthcare;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.*;

/**
 * Created by kodero on 3/25/15.
 */
@Entity
@Table(name = "operation_checklist")
@Filter(name = "filterByDeleted")
public class OperationChecklist extends ModelBase {

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @JoinColumn(name = "stage", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private OperationStage stage;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public OperationStage getStage() {
        return stage;
    }

    public void setStage(OperationStage stage) {
        this.stage = stage;
    }
}
