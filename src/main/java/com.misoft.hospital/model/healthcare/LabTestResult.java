package com.misoft.hospital.model.healthcare;

import com.misoft.hospital.model.base.ModelBase;
import com.misoft.hospital.model.setup.TestCase;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by kodero on 2/8/16.
 */
@Entity
@Table(name = "lab_test_results")
public class LabTestResult extends ModelBase{

    @NotNull
    @JoinColumn(name = "request_item", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private LabRequestItem labRequestItem;

    @JoinColumn(name = "test_case", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private TestCase testCase;

    @Column(name = "result")
    private String result;

    @Column(name = "warn")
    private Boolean warn;

    @Column(name = "excluded")
    private Boolean excluded;

    @Column(name = "remarks", columnDefinition = "text")
    private String remarks;

    public LabRequestItem getLabRequestItem() {
        return labRequestItem;
    }

    public void setLabRequestItem(LabRequestItem labRequestItem) {
        this.labRequestItem = labRequestItem;
    }

    public TestCase getTestCase() {
        return testCase;
    }

    public void setTestCase(TestCase testCase) {
        this.testCase = testCase;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public Boolean getWarn() {
        return warn;
    }

    public void setWarn(Boolean warn) {
        this.warn = warn;
    }

    public Boolean getExcluded() {
        return excluded;
    }

    public void setExcluded(Boolean excluded) {
        this.excluded = excluded;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }
}
