package com.misoft.hospital.model.healthcare;

import com.misoft.hospital.model.billing.BillItem;
import com.misoft.hospital.model.billing.BilledFrom;
import com.misoft.hospital.model.billing.PatientBill;
import com.misoft.hospital.model.setup.Product;
import com.misoft.hospital.model.setup.Staff;
import com.misoft.hospital.model.setup.Unit;
import com.misoft.hospital.service.billing.BillableRequest;
import com.misoft.hospital.service.healthcare.ConsultationRequestCallback;
import com.misoft.hospital.util.callbacks.EntityCallbackClass;
import com.misoft.hospital.util.validation.ValidationMethod;
import com.misoft.hospital.util.validation.ValidatorClass;
import com.misoft.hospital.validators.ConsultationValidator;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import javax.validation.ValidationException;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 * Created by kodero on 3/17/15.
 */
@Entity
@DiscriminatorValue("CONSULTATION")
@Filter(name = "filterByDeleted")
@ValidatorClass(ConsultationValidator.class)
@EntityCallbackClass(ConsultationRequestCallback.class)
public class ConsultationRequest extends ServiceRequest implements BillableRequest{

    @NotNull
    @JoinColumn(name = "consultation_type", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private ConsultationType consultationType;

    @Column(name = "symptoms", columnDefinition = "text")
    private String symptoms;

    @Column(name = "diagnosis")
    private String diagnosis;

    @Column(name = "chief_complaint")
    private String chiefComplaint;

    @Column(name = "duration_of_illness")
    private Integer durationOfIllness;

    @Column(name = "diagnosis_comments")
    private String diagnosisComments;

    @Column(name = "prior_treatment")
    private String priorTreatment;

    /*under 5 attributes */

    @Column(name = "danger_signs")
    private String dangerSigns;

    @Column(name = "visual_acuity")
    private String visualAcuity;

    @Column(name = "hiv_test_status")
    private String HIVTestStatus;

    @Column(name = "hiv_status")
    private String HIVStatus;

    @Column(name = "nutrition")
    private String nutrition;

    /* structured details */
    @Column(name = "pmh", columnDefinition = "text")
    private String pastMedicalHistory;

    @Column(name = "drug_history", columnDefinition = "text")
    private String drugHistory;

    @Column(name = "family_history", columnDefinition = "text")
    private String familyHistory;

    @Column(name = "review_summary", columnDefinition = "text")
    private String reviewSummary;

    @Column(name = "treatment_plan", columnDefinition = "text")
    private String treatmentPlan;

    /* ANC checks */
    @OneToMany(mappedBy = "consultationRequest", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @Filter(name = "filterByDeleted")
    private Collection<PatientANCCheck> patientANCChecks;

    /* PNC checks */
    @OneToMany(mappedBy = "consultationRequest", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @Filter(name = "filterByDeleted")
    private Collection<PatientANCCheck> patientPNCChecks;

    @JoinColumn(name = "unit", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Unit unit;

    @OneToMany(mappedBy = "consultationRequest", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @Filter(name = "filterByDeleted")
    private Collection<ConsultationDiagnosis> consultationDiagnosises = new ArrayList<>();

    @OneToMany(mappedBy = "consultationRequest", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @Filter(name = "filterByDeleted")
    private Collection<RequestConsultation> requestConsultations = new ArrayList<>();

    @OneToMany(mappedBy = "consultationRequest", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @Filter(name = "filterByDeleted")
    private Collection<PatientProblem> patientProblems;

    @OneToMany(mappedBy = "consultationRequest", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @Filter(name = "filterByDeleted")
    private Collection<Hpc> hpcs;

    @OneToMany(mappedBy = "consultationRequest", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @Filter(name = "filterByDeleted")
    private Collection<ReviewedSystem> reviewedSystems;

    @OneToMany(mappedBy = "consultationRequest", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @Filter(name = "filterByDeleted")
    private Collection<PatientSystemProblem> systemProblems;

    @OneToMany(mappedBy = "consultationRequest", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @org.hibernate.annotations.Filter(name = "filterByDeleted")
    private Collection<PatientClinicalIndication> patientClinicalIndications = new ArrayList<>();

    @Transient
    private Collection<LabRequestItem> labRequestItems = new ArrayList<>();

    @Transient
    private Collection<ImagingRequestItem> imagingRequestItems = new ArrayList<>();

    @Transient
    private Collection<RequestMedicalProcedure> requestMedicalProcedures = new ArrayList<>();

    @Transient
    private Collection<PrescriptionRequestItem> prescriptionRequestItems = new ArrayList<>();

    public ConsultationRequest(){

    }

    public ConsultationRequest(String id){
        setId(id);
    }

    public ConsultationRequest(Patient patient, Date dateRequested, Staff requester, Long requestNo, RequestSource requestSource, Appointment appointment, RequestStatus requestStatus, Boolean autoSubmit, ConsultationType consultationType){
        setPatient(patient);
        setDateRequested(dateRequested);
        setRequester(requester);
        setRequestNo(requestNo);
        setRequestSource(requestSource);
        setAppointment(appointment);
        setRequestStatus(requestStatus);
        setAutoSubmit(autoSubmit);
        setConsultationType(consultationType);
    }

    @ValidationMethod
    public void validateConsultationProduct(){
        if(getConsultationType().getProduct() == null){
            throw new ValidationException("The consultation type selected has not been setup properly!");
        }
    }

    @Override
    public PatientBill billRequest(PatientBill patientBill){
        Long startTime = System.currentTimeMillis();
        for(RequestConsultation item : getRequestConsultations()){
            Product product = item.getConsultationType().getProduct();
            if(BigDecimal.ZERO.compareTo(product.getListPrice()) == 0){
                BillItem billItem = new BillItem();
                if(item.getBillItem() != null) billItem = item.getBillItem();//just revise the amount billed if it was already billed
                if(billItem.getBillNo() == null) billItem.setBillNo(startTime);
                item.setBillItem(billItem);
                billItem.setProduct(product);
                billItem.setQuantity(BigDecimal.ONE);
                billItem.setUnitPrice(product.getListPrice());
                billItem.setLineTotal(product.getListPrice().multiply(billItem.getQuantity()));
                billItem.setTax(BigDecimal.ZERO);
                billItem.setSrType(this.getServiceType());
                billItem.setSrFqn(this.getClass().getName());
                billItem.setSrId(this.getId());
                billItem.setBilledFrom(BilledFrom.SERVICE_REQUEST);
                //billItem.setServiceCenter(this.getServiceCenter());
                patientBill.addBillItem(billItem);
                startTime++;
            }
        }
        return patientBill;
    }

    public Collection<RequestConsultation> getRequestConsultations() {
        return requestConsultations;
    }

    public void setRequestConsultations(Collection<RequestConsultation> requestConsultations) {
        this.requestConsultations = requestConsultations;
    }

    public void addRequestConsultations(RequestConsultation requestConsultation) {
        if(requestConsultation == null) return;
        requestConsultation.setConsultationRequest(this);
        getRequestConsultations().add(requestConsultation);
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public String getSymptoms() {
        return symptoms;
    }

    public void setSymptoms(String symptoms) {
        this.symptoms = symptoms;
    }

    public Collection<ConsultationDiagnosis> getConsultationDiagnosises() {
        return consultationDiagnosises;
    }

    public void setConsultationDiagnosises(Collection<ConsultationDiagnosis> consultationDiagnosises) {
        this.consultationDiagnosises = consultationDiagnosises;
    }

    public String getDiagnosisComments() {
        return diagnosisComments;
    }

    public void setDiagnosisComments(String diagnosisComments) {
        this.diagnosisComments = diagnosisComments;
    }

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    public String getPriorTreatment() {
        return priorTreatment;
    }

    public void setPriorTreatment(String priorTreatment) {
        this.priorTreatment = priorTreatment;
    }

    public Integer getDurationOfIllness() {
        return durationOfIllness;
    }

    public void setDurationOfIllness(Integer durationOfIllness) {
        this.durationOfIllness = durationOfIllness;
    }

    public String getNutrition() {
        return nutrition;
    }

    public void setNutrition(String nutrition) {
        this.nutrition = nutrition;
    }

    public String getDangerSigns() {
        return dangerSigns;
    }

    public void setDangerSigns(String dangerSigns) {
        this.dangerSigns = dangerSigns;
    }

    public String getVisualAcuity() {
        return visualAcuity;
    }

    public void setVisualAcuity(String visualAcuity) {
        this.visualAcuity = visualAcuity;
    }

    public String getHIVTestStatus() {
        return HIVTestStatus;
    }

    public void setHIVTestStatus(String HIVTestStatus) {
        this.HIVTestStatus = HIVTestStatus;
    }

    public String getHIVStatus() {
        return HIVStatus;
    }

    public void setHIVStatus(String HIVStatus) {
        this.HIVStatus = HIVStatus;
    }

    public Collection<PatientProblem> getPatientProblems() {
        return patientProblems;
    }

    public void setPatientProblems(Collection<PatientProblem> patientProblems) {
        this.patientProblems = patientProblems;
    }

    public String getChiefComplaint() {
        return chiefComplaint;
    }

    public void setChiefComplaint(String chiefComplaint) {
        this.chiefComplaint = chiefComplaint;
    }

    public Collection<PatientANCCheck> getPatientANCChecks() {
        return patientANCChecks;
    }

    public void setPatientANCChecks(Collection<PatientANCCheck> patientANCChecks) {
        this.patientANCChecks = patientANCChecks;
    }

    public Collection<PatientANCCheck> getPatientPNCChecks() {
        return patientPNCChecks;
    }

    public void setPatientPNCChecks(Collection<PatientANCCheck> patientPNCChecks) {
        this.patientPNCChecks = patientPNCChecks;
    }

    public Collection<Hpc> getHpcs() {
        return hpcs;
    }

    public void setHpcs(Collection<Hpc> hpcs) {
        this.hpcs = hpcs;
    }

    public Collection<ReviewedSystem> getReviewedSystems() {
        return reviewedSystems;
    }

    public void setReviewedSystems(Collection<ReviewedSystem> reviewedSystems) {
        this.reviewedSystems = reviewedSystems;
    }

    public Collection<PatientSystemProblem> getSystemProblems() {
        return systemProblems;
    }

    public void setSystemProblems(Collection<PatientSystemProblem> systemProblems) {
        this.systemProblems = systemProblems;
    }

    public String getPastMedicalHistory() {
        return pastMedicalHistory;
    }

    public void setPastMedicalHistory(String pastMedicalHistory) {
        this.pastMedicalHistory = pastMedicalHistory;
    }

    public String getDrugHistory() {
        return drugHistory;
    }

    public void setDrugHistory(String drugHistory) {
        this.drugHistory = drugHistory;
    }

    public String getFamilyHistory() {
        return familyHistory;
    }

    public void setFamilyHistory(String familyHistory) {
        this.familyHistory = familyHistory;
    }

    public String getReviewSummary() {
        return reviewSummary;
    }

    public void setReviewSummary(String reviewSummary) {
        this.reviewSummary = reviewSummary;
    }

    public String getTreatmentPlan() {
        return treatmentPlan;
    }

    public void setTreatmentPlan(String treatmentPlan) {
        this.treatmentPlan = treatmentPlan;
    }

    public ConsultationType getConsultationType() {
        return consultationType;
    }

    public void setConsultationType(ConsultationType consultationType) {
        this.consultationType = consultationType;
    }

    public Collection<LabRequestItem> getLabRequestItems() {
        return labRequestItems;
    }

    public void setLabRequestItems(Collection<LabRequestItem> labRequestItems) {
        this.labRequestItems = labRequestItems;
    }

    public Collection<ImagingRequestItem> getImagingRequestItems() {
        return imagingRequestItems;
    }

    public void setImagingRequestItems(Collection<ImagingRequestItem> imagingRequestItems) {
        this.imagingRequestItems = imagingRequestItems;
    }

    public Collection<RequestMedicalProcedure> getRequestMedicalProcedures() {
        return requestMedicalProcedures;
    }

    public void setRequestMedicalProcedures(Collection<RequestMedicalProcedure> requestMedicalProcedures) {
        this.requestMedicalProcedures = requestMedicalProcedures;
    }

    public Collection<PrescriptionRequestItem> getPrescriptionRequestItems() {
        return prescriptionRequestItems;
    }

    public void setPrescriptionRequestItems(Collection<PrescriptionRequestItem> prescriptionRequestItems) {
        this.prescriptionRequestItems = prescriptionRequestItems;
    }

    public Collection<PatientClinicalIndication> getPatientClinicalIndications() {
        return patientClinicalIndications;
    }

    public void setPatientClinicalIndications(Collection<PatientClinicalIndication> patientClinicalIndications) {
        this.patientClinicalIndications = patientClinicalIndications;
    }

    public void addPatientClinicalIndication(PatientClinicalIndication pci) {
        if(pci == null) return;
        pci.setConsultationRequest(this);
        getPatientClinicalIndications().add(pci);
    }
}
