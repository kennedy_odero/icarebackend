package com.misoft.hospital.model.healthcare;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by kodero on 2/22/16.
 */
@Entity
@Table(name = "discharge_reasons")
@Filter(name = "filterByDeleted")
public class DischargeReason extends ModelBase{

    @NotNull
    @Column(name = "name")
    private String reason;

    @Column(name = "description")
    private String description;

    @Column(name = "discharge_type", columnDefinition = "varchar(100) default 'NORMAL'")
    @Enumerated(EnumType.STRING)
    private DischargeType dischargeType;

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public DischargeType getDischargeType() {
        return dischargeType;
    }

    public void setDischargeType(DischargeType dischargeType) {
        this.dischargeType = dischargeType;
    }
}
