package com.misoft.hospital.model.healthcare;

/**
 * Created by kodero on 3/20/15.
 */
public enum PrescriptionItemStatus {
    VALID,
    DISCONTINUED,
    EXTENDED,
    COMPLETED
}
