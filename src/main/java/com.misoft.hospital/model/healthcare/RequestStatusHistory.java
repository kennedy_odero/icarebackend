package com.misoft.hospital.model.healthcare;

import com.misoft.hospital.model.admin.User;
import com.misoft.hospital.model.base.ModelBase;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by kodero on 12/10/15.
 */
@Entity
@Table(name = "request_status_history")
public class RequestStatusHistory extends ModelBase{

    @Enumerated(EnumType.STRING)
    private RequestStatus status;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date_changed")
    private Date dateChanged;

    @JoinColumn(name = "user", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private User user;

    @JoinColumn(name = "service_request", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private ServiceRequest serviceRequest;

    public RequestStatusHistory(RequestStatus status, Date dateChanged, User user){
        setStatus(status);
        setDateChanged(dateChanged);
        setUser(user);
    }

    public RequestStatusHistory(){

    }

    public RequestStatus getStatus() {
        return status;
    }

    public void setStatus(RequestStatus status) {
        this.status = status;
    }

    public Date getDateChanged() {
        return dateChanged;
    }

    public void setDateChanged(Date dateChanged) {
        this.dateChanged = dateChanged;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public ServiceRequest getServiceRequest() {
        return serviceRequest;
    }

    public void setServiceRequest(ServiceRequest serviceRequest) {
        this.serviceRequest = serviceRequest;
    }
}
