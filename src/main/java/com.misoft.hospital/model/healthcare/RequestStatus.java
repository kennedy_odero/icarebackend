package com.misoft.hospital.model.healthcare;

/**
 * Created by kodero on 3/15/15.
 */
public enum RequestStatus {
    NEW,
    SUBMITTED,
    RECALLED,
    RETURNED,
    RECEIVED,
    PROCESSING,
    ON_HOLD,
    CANCELLED,
    COMPLETE
}
