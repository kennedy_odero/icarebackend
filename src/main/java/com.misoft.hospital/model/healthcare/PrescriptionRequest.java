package com.misoft.hospital.model.healthcare;

import com.misoft.hospital.model.billing.BillItem;
import com.misoft.hospital.model.billing.BillItemType;
import com.misoft.hospital.model.billing.BilledFrom;
import com.misoft.hospital.model.billing.PatientBill;
import com.misoft.hospital.model.setup.Product;
import com.misoft.hospital.model.setup.Staff;
import com.misoft.hospital.service.billing.BillableRequest;
import com.misoft.hospital.util.validation.ValidationMethod;

import javax.persistence.*;
import javax.validation.ValidationException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Created by kodero on 3/11/15.
 */
@Entity
@DiscriminatorValue("PRESCRIPTION")
public class PrescriptionRequest extends ServiceRequest implements BillableRequest {

    @OneToMany(mappedBy = "prescriptionRequest", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @org.hibernate.annotations.Filter(name = "filterByDeleted")
    private Collection<PrescriptionRequestItem> prescriptionRequestItems = new ArrayList<>();

    @Column(name = "dispensed", columnDefinition = "tinyint default 0")
    private Boolean dispensed = false;

    @Column(name = "confirmed", columnDefinition = "tinyint default 0")
    private Boolean confirmed;

    public PrescriptionRequest(){

    }

    public PrescriptionRequest(Long requestNo, RequestSource requestSource, Patient patient, Date dateRequested, Staff staff, RequestStatus requestStatus, Appointment appointment){
        super(requestNo, requestSource, patient, dateRequested, staff, requestStatus, appointment);
    }

    @Override
    public PatientBill billRequest(PatientBill patientBill){
        Long startTime = System.currentTimeMillis();
        for(PrescriptionRequestItem item : getPrescriptionRequestItems()){
            if(item.isAvailable()){
                //only bill if the item is available
                Product product = item.getMedicament().getProduct();
                BillItem billItem = new BillItem();
                if(item.getBillItem() != null) billItem = item.getBillItem();//just revise the amount billed if it was already billed
                if(billItem.getBillNo() == null) billItem.setBillNo(startTime);
                item.setBillItem(billItem);
                billItem.setProduct(product);
                billItem.setQuantity(item.getDispenseQty());
                billItem.setUnitPrice(product.getListPrice());
                billItem.setLineTotal(product.getListPrice().multiply(billItem.getQuantity()));
                billItem.setTax(BigDecimal.ZERO);
                billItem.setSrType(this.getServiceType());
                billItem.setSrFqn(this.getClass().getName());
                billItem.setSrId(this.getId());
                billItem.setBilledFrom(BilledFrom.SERVICE_REQUEST);
                //billItem.setServiceCenter(this.getServiceCenter());
                billItem.setBillItemType(BillItemType.Normal);
                patientBill.addBillItem(billItem);
                startTime++;
            }else if(!item.getDispensed()){
                if(item.getBillItem() != null){
                    item.getBillItem().setDeleted(true);//remove the bill item if it was already set
                }
            }
        }
        return patientBill;
    }

    @ValidationMethod
    public void validateEntries(){
        if (getPrescriptionRequestItems().size() == 0) throw new ValidationException("Please add at least 1 prescription item!");
        for (PrescriptionRequestItem pri : getPrescriptionRequestItems()) {
            if(pri.getMedicament() == null) throw new ValidationException("Please make sure all items have a medicament!");
            if(pri.getMedicament().getBlocked()) throw new ValidationException("Medicament {" + pri.getMedicament().getProduct().getName() +"} is blocked for requests!");
        }
    }

    public Boolean isConfirmed() {
        return confirmed;
    }

    public void setConfirmed(Boolean confirmed) {
        this.confirmed = confirmed;
    }

    public Collection<PrescriptionRequestItem> getPrescriptionRequestItems() {
        return prescriptionRequestItems;
    }

    public void setPrescriptionRequestItems(Collection<PrescriptionRequestItem> prescriptionRequestItems) {
        this.prescriptionRequestItems = prescriptionRequestItems;
    }

    public Boolean getDispensed() {
        return dispensed;
    }

    public void setDispensed(Boolean dispensed) {
        this.dispensed = dispensed;
    }

    public Boolean isDispensed() {
        return dispensed;
    }

    public void addPrescriptionRequestItem(PrescriptionRequestItem prescriptionRequestItem){
        if(prescriptionRequestItem != null){
            prescriptionRequestItem.setPrescriptionRequest(this);
            getPrescriptionRequestItems().add(prescriptionRequestItem);
        }
    }

    public void addPrescriptionRequestItems(List<PrescriptionRequestItem> prescriptionRequestItems) {
        for(PrescriptionRequestItem pri : prescriptionRequestItems){
            if(pri != null){
                pri.setPrescriptionRequest(this);
                getPrescriptionRequestItems().add(pri);
            }
        }
    }
}
