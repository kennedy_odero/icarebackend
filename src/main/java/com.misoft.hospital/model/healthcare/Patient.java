package com.misoft.hospital.model.healthcare;

import com.misoft.hospital.model.setup.*;
import com.misoft.hospital.util.callbacks.During;
import com.misoft.hospital.util.callbacks.EntityCallbackMethod;
import com.misoft.hospital.util.validation.ValidatorClass;
import com.misoft.hospital.validators.PatientValidator;
import org.hibernate.annotations.Formula;

import javax.persistence.*;
import javax.validation.ValidationException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 * Created by kodero on 3/10/15.
 */
@Entity
@DiscriminatorValue("PATIENT")
@ValidatorClass(PatientValidator.class)
public class Patient extends Person {

    @Formula("(concat(surname, ' ', other_names))")
    private String patientName;

    @Column(name = "patient_no")
    private String patientNo;

    @Column(name = "inpatient_no")
    private String inpatientNo;

    @Column(name = "bar_code", columnDefinition = "mediumblob")
    private byte[] patientNoBarcode;

    @Column(name = "educ_level")
    private String educationalLevel;

    @Column(name = "housing_conds")
    private String housingConditions;

    @JoinColumn(name = "occupation", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Occupation occupation;

    @Column(name = "place_of_birth")
    private String placeOfBirth;

    @Column(name = "dob_mode")
    private Boolean dobMode;

    @Column(name = "age_in_string")
    private String ageInString;

    @JoinColumn(name = "religion", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Religion religion;

    @JoinColumn(name = "location", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Location location;

    @JoinColumn(name = "sub_location", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Location subLocation;

    @JoinColumn(name = "village", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Location village;

    @Column(name = "socio_notes")
    private String socioEconomicNotes;

    @Column(name = "critical_info", columnDefinition = "text")
    private String criticalInfo;

    @Column(name = "patient_status")
    @Enumerated(EnumType.STRING)
    private PatientStatus patientStatus = PatientStatus._;

    @Column(name = "marital_status")
    @Enumerated(EnumType.STRING)
    private MaritalStatus maritalStatus;

    @OneToMany(mappedBy = "patient", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @org.hibernate.annotations.Filter(name = "filterByDeleted")
    private Collection<Admission> admissions = new ArrayList<>();

    @OneToMany(mappedBy = "patient", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @org.hibernate.annotations.Filter(name = "filterByDeleted")
    private Collection<Appointment> appointments;

    @OneToMany(mappedBy = "patient", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @org.hibernate.annotations.Filter(name = "filterByDeleted")
    private Collection<ServiceRequest> serviceRequests;

    //new born details
    @Column(name = "length")
    private BigDecimal length;

    @Column(name = "weight")
    private BigDecimal weight;

    @Column(name = "name_at_birth")
    private String nameAtBirth;

    @JoinColumn(name = "patient", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Patient patient; //when the patient is a mother who has has born

    @OneToMany(mappedBy = "patient", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @org.hibernate.annotations.Filter(name = "filterByDeleted")
    private Collection<Patient> patients; //newborns

    @OneToMany(mappedBy = "patient", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @org.hibernate.annotations.Filter(name = "filterByDeleted")
    private Collection<APGARScore> apgarScores;

    @OneToMany(mappedBy = "patient", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @org.hibernate.annotations.Filter(name = "filterByDeleted")
    private Collection<NeonatalSignCheck> neonatalSigns;

    @OneToMany(mappedBy = "patient", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @org.hibernate.annotations.Filter(name = "filterByDeleted")
    private Collection<CongenitalDisease> congenitalDiseases;

    @OneToMany(mappedBy = "patient", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @org.hibernate.annotations.Filter(name = "filterByDeleted")
    private Collection<NeonatalReflexCheck> neonatalReflexChecks;

    @OneToMany(mappedBy = "patient", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @org.hibernate.annotations.Filter(name = "filterByDeleted")
    private Collection<PSCScore> pscScores;

    @OneToMany(mappedBy = "patient", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @org.hibernate.annotations.Filter(name = "filterByDeleted")
    private Collection<PatientAllergy> patientAllergies;

    @OneToMany(mappedBy = "patient", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @org.hibernate.annotations.Filter(name = "filterByDeleted")
    private Collection<RiskFactor> riskFactors;

    @OneToMany(mappedBy = "patient", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @org.hibernate.annotations.Filter(name = "filterByDeleted")
    private Collection<ChronicDiseaseHistory> chronicDiseaseHistories;

    @Formula("coalesce((select 1 from appointments a where a.status = 'OPEN' and a.patient = id limit 1),0)")
    private Boolean hasAppointment;

    @EntityCallbackMethod(when = During.DELETE)
    public void updateClosure(){
        throw new ValidationException("Patient deletion not allowed!");
    }

    public Patient(){

    }

    public Patient(String id, byte[] patientNoBarcode){
        setId(id);
        setPatientNoBarcode(patientNoBarcode);
    }

    public Patient(String id, String patientNo, String surname, String otherNames, Gender gender, Date dob){
        setId(id);
        setPatientNo(patientNo);
        setSurname(surname);
        setOtherNames(otherNames);
        setGender(gender);
        setDob(dob);
    }

    public String getEducationalLevel() {
        return educationalLevel;
    }

    public void setEducationalLevel(String educationalLevel) {
        this.educationalLevel = educationalLevel;
    }

    public String getHousingConditions() {
        return housingConditions;
    }

    public void setHousingConditions(String housingConditions) {
        this.housingConditions = housingConditions;
    }

    public String getSocioEconomicNotes() {
        return socioEconomicNotes;
    }

    public void setSocioEconomicNotes(String socioEconomicNotes) {
        this.socioEconomicNotes = socioEconomicNotes;
    }

    public Collection<Admission> getAdmissions() {
        return admissions;
    }

    public void setAdmissions(Collection<Admission> admissions) {
        this.admissions = admissions;
    }

    public Collection<Appointment> getAppointments() {
        return appointments;
    }

    public void setAppointments(Collection<Appointment> appointment) {
        this.appointments = appointment;
    }

    public Collection<ServiceRequest> getServiceRequests() {
        return serviceRequests;
    }

    public void setServiceRequests(Collection<ServiceRequest> serviceRequests) {
        this.serviceRequests = serviceRequests;
    }

    public String getPlaceOfBirth() {
        return placeOfBirth;
    }

    public void setPlaceOfBirth(String placeOfBirth) {
        this.placeOfBirth = placeOfBirth;
    }

    public Collection<PatientAllergy> getPatientAllergies() {
        return patientAllergies;
    }

    public void setPatientAllergies(Collection<PatientAllergy> patientAllergies) {
        this.patientAllergies = patientAllergies;
    }

    public Collection<ChronicDiseaseHistory> getChronicDiseaseHistories() {
        return chronicDiseaseHistories;
    }

    public void setChronicDiseaseHistories(Collection<ChronicDiseaseHistory> chronicDiseaseHistories) {
        this.chronicDiseaseHistories = chronicDiseaseHistories;
    }

    public Collection<RiskFactor> getRiskFactors() {
        return riskFactors;
    }

    public void setRiskFactors(Collection<RiskFactor> riskFactors) {
        this.riskFactors = riskFactors;
    }

    public String getPatientNo() {
        return patientNo;
    }

    public void setPatientNo(String patientNo) {
        this.patientNo = patientNo;
    }

    public String getCriticalInfo() {
        return criticalInfo;
    }

    public void setCriticalInfo(String criticalInfo) {
        this.criticalInfo = criticalInfo;
    }

    public PatientStatus getPatientStatus() {
        return patientStatus;
    }

    public void setPatientStatus(PatientStatus patientStatus) {
        this.patientStatus = patientStatus;
    }

    public MaritalStatus getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(MaritalStatus maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public BigDecimal getLength() {
        return length;
    }

    public void setLength(BigDecimal length) {
        this.length = length;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    public String getNameAtBirth() {
        return nameAtBirth;
    }

    public void setNameAtBirth(String nameAtBirth) {
        this.nameAtBirth = nameAtBirth;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Collection<Patient> getPatients() {
        return patients;
    }

    public void setPatients(Collection<Patient> patients) {
        this.patients = patients;
    }

    public Collection<APGARScore> getApgarScores() {
        return apgarScores;
    }

    public void setApgarScores(Collection<APGARScore> apgarScores) {
        this.apgarScores = apgarScores;
    }

    public Collection<NeonatalSignCheck> getNeonatalSigns() {
        return neonatalSigns;
    }

    public void setNeonatalSigns(Collection<NeonatalSignCheck> neonatalSigns) {
        this.neonatalSigns = neonatalSigns;
    }

    public Collection<CongenitalDisease> getCongenitalDiseases() {
        return congenitalDiseases;
    }

    public void setCongenitalDiseases(Collection<CongenitalDisease> congenitalDiseases) {
        this.congenitalDiseases = congenitalDiseases;
    }

    public Collection<NeonatalReflexCheck> getNeonatalReflexChecks() {
        return neonatalReflexChecks;
    }

    public void setNeonatalReflexChecks(Collection<NeonatalReflexCheck> neonatalReflexChecks) {
        this.neonatalReflexChecks = neonatalReflexChecks;
    }

    public Collection<PSCScore> getPscScores() {
        return pscScores;
    }

    public void setPscScores(Collection<PSCScore> pscScores) {
        this.pscScores = pscScores;
    }

    public void setOccupation(Occupation occupation) {
        this.occupation = occupation;
    }

    public Religion getReligion() {
        return religion;
    }

    public void setReligion(Religion religion) {
        this.religion = religion;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Location getSubLocation() {
        return subLocation;
    }

    public void setSubLocation(Location subLocation) {
        this.subLocation = subLocation;
    }

    public Location getVillage() {
        return village;
    }

    public void setVillage(Location village) {
        this.village = village;
    }

    public Occupation getOccupation() {
        return occupation;
    }

    public String getInpatientNo() {
        return inpatientNo;
    }

    public void setInpatientNo(String inpatientNo) {
        this.inpatientNo = inpatientNo;
    }

    public byte[] getPatientNoBarcode() {
        return patientNoBarcode;
    }

    public void setPatientNoBarcode(byte[] patientNoBarcode) {
        this.patientNoBarcode = patientNoBarcode;
    }

    public Boolean getDobMode() {
        return dobMode;
    }

    public void setDobMode(Boolean dobMode) {
        this.dobMode = dobMode;
    }

    public String getAgeInString() {
        return ageInString;
    }

    public void setAgeInString(String ageInString) {
        this.ageInString = ageInString;
    }

    public Boolean isHasAppointment() {
        return hasAppointment;
    }

    public void setHasAppointment(Boolean hasAppointment) {
        this.hasAppointment = hasAppointment;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }
}
