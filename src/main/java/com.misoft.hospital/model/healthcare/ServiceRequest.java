package com.misoft.hospital.model.healthcare;

import com.misoft.hospital.model.base.ModelBase;
import com.misoft.hospital.model.billing.BillItem;
import com.misoft.hospital.model.billing.PatientBill;
import com.misoft.hospital.model.billing.PatientCharge;
import com.misoft.hospital.model.setup.Staff;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.Formula;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Created by kodero on 3/11/15.
 */
@Entity
@Table(name = "service_requests")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "request_tye")
@Filter(name = "filterByDeleted")
public class ServiceRequest extends ModelBase {

    @NotNull
    @Column(name = "request_no")
    private Long requestNo;

    @NotNull
    @Column(name = "request_source")
    @Enumerated(EnumType.STRING)
    private RequestSource requestSource  = RequestSource.Internal;

    @JoinColumn(name = "patient", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Patient patient;

    @Column(name = "date_requested")
    private Date dateRequested;

    @Column(name = "notes")
    private String notes;

    @Formula("request_tye")
    @Column(name = "request_type")
    //@Enumerated(EnumType.STRING)
    private String serviceType;

    @JoinColumn(name = "requester", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Staff requester;

    @JoinColumn(name = "assignee", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Staff assignee;

    @Column(name = "request_status")
    @Enumerated(EnumType.STRING)
    private RequestStatus requestStatus;

    @NotNull
    @JoinColumn(name = "appointment", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Appointment appointment;

    /*@NotNull
    @JoinColumn(name = "svc_center", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private ServiceCenter serviceCenter;*/

    @Column(name = "aggregate_amount")
    private BigDecimal aggregateAmount;

    @Column(name = "billing_status")
    private BillingStatus billingStatus;

    @Column(name = "billed", columnDefinition = "tinyint default '0'")
    private Boolean billed = false;

    /*applies to Lab, Radiology, Imaging, Admission, Operation, Prescription, Referral requests*/
    @JoinColumn(name = "consult_request", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private ConsultationRequest consultationRequest;

    @OneToMany(mappedBy = "serviceRequest", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @org.hibernate.annotations.Filter(name = "filterByDeleted")
    @OrderBy("date_changed desc")
    private Collection<RequestStatusHistory> requestStatusHistories = new ArrayList<>();

    @OneToMany(mappedBy = "serviceRequest", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @org.hibernate.annotations.Filter(name = "filterByDeleted")
    @OrderBy("charge_no asc")
    private Collection<PatientCharge> patientCharges = new ArrayList<>();

    @Column(name = "auto_submit", columnDefinition = "tinyint default '0'")
    private Boolean autoSubmit = false;

    public ServiceRequest(){

    }

    public ServiceRequest(Long requestNo, RequestSource requestSource, Patient patient, Date dateRequested, Staff staff, RequestStatus requestStatus, Appointment appointment){
        setRequestNo(requestNo);
        setRequestSource(requestSource);
        setPatient(patient);
        setDateRequested(dateRequested);
        setRequester(staff);
        setRequestStatus(requestStatus);
        setAppointment(appointment);
    }

    public Collection<RequestStatusHistory> getRequestStatusHistories() {
        return requestStatusHistories;
    }

    public void setRequestStatusHistories(Collection<RequestStatusHistory> requestStatusHistories) {
        this.requestStatusHistories = requestStatusHistories;
    }

    public Collection<PatientCharge> getPatientCharges() {
        return patientCharges;
    }

    public void setPatientCharges(Collection<PatientCharge> patientCharges) {
        this.patientCharges = patientCharges;
    }

    public void addRequestStatusHistory(RequestStatusHistory requestStatusHistory) {
        if(requestStatusHistory == null) return;
        requestStatusHistory.setServiceRequest(this);
        getRequestStatusHistories().add(requestStatusHistory);
    }

    public Date getDateRequested() {
        return dateRequested;
    }

    public void setDateRequested(Date dateRequested) {
        this.dateRequested = dateRequested;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Staff getRequester() {
        return requester;
    }

    public void setRequester(Staff requester) {
        this.requester = requester;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public RequestStatus getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(RequestStatus requestStatus) {
        this.requestStatus = requestStatus;
    }

    public Appointment getAppointment() {
        return appointment;
    }

    public void setAppointment(Appointment appointment) {
        this.appointment = appointment;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Staff getAssignee() {
        return assignee;
    }

    public void setAssignee(Staff assignee) {
        this.assignee = assignee;
    }

    public Long getRequestNo() {
        return requestNo;
    }

    public void setRequestNo(Long requestNo) {
        this.requestNo = requestNo;
    }

    public RequestSource getRequestSource() {
        return requestSource;
    }

    public void setRequestSource(RequestSource requestSource) {
        this.requestSource = requestSource;
    }

    public BigDecimal getAggregateAmount() {
        return aggregateAmount;
    }

    public void setAggregateAmount(BigDecimal aggregateAmount) {
        this.aggregateAmount = aggregateAmount;
    }

    public Boolean getBilled() {
        return billed;
    }

    public void setBilled(Boolean billed) {
        this.billed = billed;
    }

    public ConsultationRequest getConsultationRequest() {
        return consultationRequest;
    }

    public void setConsultationRequest(ConsultationRequest consultationRequest) {
        this.consultationRequest = consultationRequest;
    }

    public BillingStatus getBillingStatus() {
        return billingStatus;
    }

    public void setBillingStatus(BillingStatus billingStatus) {
        this.billingStatus = billingStatus;
    }

    public Boolean getAutoSubmit() {
        return autoSubmit;
    }

    public void setAutoSubmit(Boolean autoSubmit) {
        this.autoSubmit = autoSubmit;
    }
}
