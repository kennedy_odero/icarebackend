package com.misoft.hospital.model.healthcare;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Created by kodero on 3/11/15.
 */
@Entity
@DiscriminatorValue("PHYSIOTHERAPY")
public class PhysiotherapyRequest extends ServiceRequest {

}
