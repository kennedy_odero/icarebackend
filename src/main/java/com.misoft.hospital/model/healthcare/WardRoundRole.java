package com.misoft.hospital.model.healthcare;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * Created by kodero on 4/1/15.
 */
@Entity
@Table(name = "ward_round_roles")
@Filter(name = "filterByDeleted")
public class WardRoundRole extends ModelBase {

    @NotNull
    @Column(name = "name")
    private String name;

    @NotNull
    @Column(name = "responsibility")
    private String responsibility;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getResponsibility() {
        return responsibility;
    }

    public void setResponsibility(String responsibility) {
        this.responsibility = responsibility;
    }
}
