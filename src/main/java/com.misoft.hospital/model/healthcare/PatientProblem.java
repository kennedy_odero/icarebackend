package com.misoft.hospital.model.healthcare;

import com.misoft.hospital.model.base.ModelBase;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by kodero on 10/27/15.
 */
@Entity
@Table(name = "patient_problems")
public class PatientProblem extends ModelBase{

    @Column(name = "description", columnDefinition = "text")
    private String description;

    @Column(name = "onset_date")
    private Date onsetDate;

    @Column(name = "history", columnDefinition = "text")
    private String history;

    @Column(name = "recurrence")
    @Enumerated(EnumType.STRING)
    private ProblemRecurrence problemRecurrence;

    @Column(name = "date_resolved")
    @Temporal(TemporalType.DATE)
    private Date dateResolved;

    @JoinColumn(name = "consultation", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private ConsultationRequest consultationRequest;

    public Date getOnsetDate() {
        return onsetDate;
    }

    public void setOnsetDate(Date onsetDate) {
        this.onsetDate = onsetDate;
    }

    public String getHistory() {
        return history;
    }

    public void setHistory(String history) {
        this.history = history;
    }

    public ProblemRecurrence getProblemRecurrence() {
        return problemRecurrence;
    }

    public void setProblemRecurrence(ProblemRecurrence problemRecurrence) {
        this.problemRecurrence = problemRecurrence;
    }

    public Date getDateResolved() {
        return dateResolved;
    }

    public void setDateResolved(Date dateResolved) {
        this.dateResolved = dateResolved;
    }

    public ConsultationRequest getConsultationRequest() {
        return consultationRequest;
    }

    public void setConsultationRequest(ConsultationRequest consultationRequest) {
        this.consultationRequest = consultationRequest;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
