package com.misoft.hospital.model.healthcare;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by kodero on 3/25/15.
 */
@Entity
@Table(name = "operation_stages")
@Filter(name = "filterByDeleted")
public class OperationStage extends ModelBase {

    @Column(name = "name")
    private String name;

    @Column(name = "_index")
    private Integer index;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }
}
