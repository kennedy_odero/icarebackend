package com.misoft.hospital.model.healthcare;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by kodero on 4/18/15.
 */
@Entity
@Table(name = "referral_reasons")
@Filter(name = "filterByDeleted")
public class ReferralReason extends ModelBase {

    @Column(name = "reason")
    private String reason;

    @Column(name = "description", columnDefinition = "text")
    private String description;

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
