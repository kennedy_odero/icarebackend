package com.misoft.hospital.model.healthcare;

import com.drew.lang.annotations.NotNull;
import com.misoft.hospital.model.base.ModelBase;
import com.misoft.hospital.model.billing.BillItem;
import com.misoft.hospital.model.setup.LabTestType;
import com.misoft.hospital.model.setup.Staff;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 * Created by kodero on 3/20/15.
 */
@Entity
@Table(name ="lab_requests_items")
@Filter(name = "filterByDeleted")
public class LabRequestItem extends ServiceRequestItem {

    @NotNull
    @Column(name = "item_request_no")
    private String itemRequestNo;

    @JoinColumn(name = "lab_test_type", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private LabTestType labTestType;

    @JoinColumn(name = "lab_request", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private LabRequest labRequest;

    @JoinColumn(name = "test_sample", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private TestSample testSample;

    @Column(name = "specimen_cond")
    private String conditionOfSpecimen;

    @Column(name = "date_sample_collected")
    private Date dateSampleCollected;

    @Column(name = "date_sample_received")
    private Date dateSampleReceived;

    @Column(name = "date_sample_analysed")
    private Date dateSampleAnalysed;

    @Column(name = "date_results_disp")
    private Date dateResultsDispatched;

    @JoinColumn(name = "lab_unit", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private LabUnit labUnit;

    @Column(name = "results", columnDefinition = "text")
    private String results;

    @Column(name = "comments", columnDefinition = "text")
    private String comments;

    @Column(name = "lab_result")
    @Enumerated(EnumType.STRING)
    private LABResult labResult;

    @Column(name = "line_total")
    private BigDecimal lineTotal;

    @JoinColumn(name = "analysing_officer", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Staff analysingOfficer;

    @OneToMany(mappedBy = "labRequestItem", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @Filter(name = "filterByDeleted")
    private Collection<LabTestResult> labTestResults;

    @Enumerated(EnumType.STRING)
    @Column(name = "item_status")
    private LabRequestItemStatus requestItemStatus = LabRequestItemStatus.Incomplete;

    @OneToMany(mappedBy = "labRequestItem", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @org.hibernate.annotations.Filter(name = "filterByDeleted")
    @OrderBy("date_changed desc")
    private Collection<ItemStatusHistory> itemStatusHistories = new ArrayList<>();

    /*@JoinColumn(name = "bill_item", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private BillItem billItem;*/

    public LabRequestItem(){

    }

    public LabRequestItem(LabTestType labTestType, String itemRequestNo, String impression){
        setLabTestType(labTestType);
        setItemRequestNo(itemRequestNo);
        setComments(impression);
    }

    public LabTestType getLabTestType() {
        return labTestType;
    }

    public void setLabTestType(LabTestType labTestType) {
        this.labTestType = labTestType;
    }

    public LabRequest getLabRequest() {
        return labRequest;
    }

    public void setLabRequest(LabRequest labRequest) {
        this.labRequest = labRequest;
    }

    public String getResults() {
        return results;
    }

    public void setResults(String results) {
        this.results = results;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public LABResult getLabResult() {
        return labResult;
    }

    public void setLabResult(LABResult labResult) {
        this.labResult = labResult;
    }

    public TestSample getTestSample() {
        return testSample;
    }

    public void setTestSample(TestSample testSample) {
        this.testSample = testSample;
    }

    public LabUnit getLabUnit() {
        return labUnit;
    }

    public void setLabUnit(LabUnit labUnit) {
        this.labUnit = labUnit;
    }

    public BigDecimal getLineTotal() {
        return lineTotal;
    }

    public void setLineTotal(BigDecimal lineTotal) {
        this.lineTotal = lineTotal;
    }

    public Staff getAnalysingOfficer() {
        return analysingOfficer;
    }

    public void setAnalysingOfficer(Staff analysingOfficer) {
        this.analysingOfficer = analysingOfficer;
    }

    public String getConditionOfSpecimen() {
        return conditionOfSpecimen;
    }

    public void setConditionOfSpecimen(String conditionOfSpecimen) {
        this.conditionOfSpecimen = conditionOfSpecimen;
    }

    public Date getDateSampleCollected() {
        return dateSampleCollected;
    }

    public void setDateSampleCollected(Date dateSampleCollected) {
        this.dateSampleCollected = dateSampleCollected;
    }

    public Date getDateSampleReceived() {
        return dateSampleReceived;
    }

    public void setDateSampleReceived(Date dateSampleReceived) {
        this.dateSampleReceived = dateSampleReceived;
    }

    public Date getDateSampleAnalysed() {
        return dateSampleAnalysed;
    }

    public void setDateSampleAnalysed(Date dateSampleAnalysed) {
        this.dateSampleAnalysed = dateSampleAnalysed;
    }

    public Date getDateResultsDispatched() {
        return dateResultsDispatched;
    }

    public void setDateResultsDispatched(Date dateResultsDispatched) {
        this.dateResultsDispatched = dateResultsDispatched;
    }

    public Collection<LabTestResult> getLabTestResults() {
        return labTestResults;
    }

    public void setLabTestResults(Collection<LabTestResult> labTestResults) {
        this.labTestResults = labTestResults;
    }

    public String getItemRequestNo() {
        return itemRequestNo;
    }

    public void setItemRequestNo(String itemRequestNo) {
        this.itemRequestNo = itemRequestNo;
    }

    public LabRequestItemStatus getRequestItemStatus() {
        return requestItemStatus;
    }

    public void setRequestItemStatus(LabRequestItemStatus requestItemStatus) {
        this.requestItemStatus = requestItemStatus;
    }

    public Collection<ItemStatusHistory> getItemStatusHistories() {
        return itemStatusHistories;
    }

    public void addItemStatusHistory(ItemStatusHistory itemStatusHistory) {
        if(itemStatusHistory == null) return;
        itemStatusHistory.setLabRequestItem(this);
        getItemStatusHistories().add(itemStatusHistory);
    }

    public void setItemStatusHistories(Collection<ItemStatusHistory> itemStatusHistories) {
        this.itemStatusHistories = itemStatusHistories;
    }
}
