package com.misoft.hospital.model.healthcare;

/**
 * Created by kodero on 3/12/15.
 */
public enum VisitType {
    NEW_CASE,
    FOLLOW_UP,//re-visit
    FIRST_ANTENATAL,
    SUBS_ANTENATAL,
    POSTNATAL_VISIT,
    SPECIAL_CLINIC
}
