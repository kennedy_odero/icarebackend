package com.misoft.hospital.model.healthcare;

import com.misoft.hospital.model.base.ModelBase;
import com.misoft.hospital.model.setup.Institution;
import com.misoft.hospital.model.setup.Unit;
import com.misoft.hospital.service.healthcare.AppointmentCallback;
import com.misoft.hospital.util.callbacks.During;
import com.misoft.hospital.util.callbacks.EntityCallbackClass;
import com.misoft.hospital.util.callbacks.EntityCallbackMethod;
import com.misoft.hospital.util.validation.ValidationMethod;
import com.misoft.hospital.util.validation.ValidatorClass;
import com.misoft.hospital.validators.AppointmentValidator;
import org.hibernate.annotations.Filter;
import org.joda.time.DateTime;
import org.joda.time.Years;

import javax.persistence.*;
import javax.validation.ValidationException;
import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.Date;

/**
 * Created by kodero on 3/11/15.
 */
@Entity
@Table(name = "appointments")
@Filter(name = "filterByDeleted")
@ValidatorClass(AppointmentValidator.class)
@EntityCallbackClass(AppointmentCallback.class)
public class Appointment extends ModelBase {

    @NotNull
    @Column(name = "appointment_no")
    private Long appointmentNo;

    @NotNull
    @JoinColumn(name = "patient", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Patient patient;

    @NotNull
    @Column(name = "appt_date")
    private Date date;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private Status status = Status.OPEN;

    @JoinColumn(name = "institution", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Institution institution;

    @JoinColumn(name = "unit", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Unit unit;

    @NotNull
    @Column(name = "urgency")
    @Enumerated(EnumType.STRING)
    private Urgency urgency;

    @Column(name = "end_date")
    private Date end;

    @Column(name = "comments", columnDefinition = "text")
    private String comments;

    @Column(name = "bill_pmt_type")
    @Enumerated(EnumType.STRING)
    private BillPaymentType billPaymentType;

    @Column(name = "payment_type")
    @Enumerated(EnumType.STRING)
    private PaymentType paymentType;

    @OneToMany(mappedBy = "appointment", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @Filter(name = "filterByDeleted")
    private Collection<ServiceRequest> serviceRequests;

    @JoinColumn(name = "special_clinic", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private SpecialClinic specialClinic;

    @JoinColumn(name = "insurance_company", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private InsuranceCompany insuranceCompany;

    @Column(name = "insurance_approved")
    private Boolean insuranceApproved;

    @Column(name = "auto_create_triage")
    private Boolean autoCreateTriage;

    @JoinColumn(name = "consultation_type", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private ConsultationType consultationType;

    @Column(name = "sent_to_jms", columnDefinition = "tinyint default 0")
    private Boolean sentToJms = false;

    @Column(name = "new_patient", columnDefinition = "tinyint default 1")
    private Boolean newPatient = false;

    @Column(name = "patient_age")
    private Integer patientAge = 0;

    public Appointment(){

    }

    public Appointment(String id, BillPaymentType billPaymentType){
        setId(id);
        setBillPaymentType(billPaymentType);
    }

    @ValidationMethod
    public void extraValidations(){
        if(getConsultationType() == null) throw new ValidationException("Please specify the consultation type!");
        if((getBillPaymentType() == BillPaymentType.CORPORATE && getInsuranceCompany() == null)){
            throw new ValidationException("Please specify the corporate client!");
        }
    }

    @ValidationMethod
    public void quickAppointmentValidation(){
        if(getNewPatient()){
            //validate if details of new patient have been specified
            if(getPatient().getPatientNo() == null) throw new ValidationException("Patient# cannot be empty!");
            if(getPatient().getPatientName() == null) throw new ValidationException("Please provide the patient name!");
            if(!getPatient().getPatientName().matches("^[a-zA-Z]+\\s+[a-zA-Z0-9]+(\\s+[a-zA-Z0-9]+)?$")) throw new ValidationException("Please provide a valid name name! Two or three names");
            if(getPatient().getDob() == null) throw new ValidationException("Please provide the patient\'s age!");
            if(getPatient().getGender() == null) throw new ValidationException("Please provide the patient\'s age!");
            if(getPatient().getResidentialAddress() == null) throw new ValidationException("Please provide the patient\'s residential address!");
        }
    }

    @EntityCallbackMethod(when = During.CREATE)
    public void normalizePatientAge(){
        setPatientAge(Years.yearsBetween(new DateTime(getPatient().getDob()), new DateTime()).getYears());
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Institution getInstitution() {
        return institution;
    }

    public void setInstitution(Institution institution) {
        this.institution = institution;
    }

    public Urgency getUrgency() {
        return urgency;
    }

    public void setUrgency(Urgency urgency) {
        this.urgency = urgency;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public Collection<ServiceRequest> getServiceRequests() {
        return serviceRequests;
    }

    public void setServiceRequests(Collection<ServiceRequest> serviceRequests) {
        this.serviceRequests = serviceRequests;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public PaymentType getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(PaymentType paymentType) {
        this.paymentType = paymentType;
    }

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    public Long getAppointmentNo() {
        return appointmentNo;
    }

    public void setAppointmentNo(Long appointmentNo) {
        this.appointmentNo = appointmentNo;
    }

    public SpecialClinic getSpecialClinic() {
        return specialClinic;
    }

    public void setSpecialClinic(SpecialClinic specialClinic) {
        this.specialClinic = specialClinic;
    }

    public InsuranceCompany getInsuranceCompany() {
        return insuranceCompany;
    }

    public void setInsuranceCompany(InsuranceCompany insuranceCompany) {
        this.insuranceCompany = insuranceCompany;
    }

    public Boolean getInsuranceApproved() {
        return insuranceApproved;
    }

    public void setInsuranceApproved(Boolean insuranceApproved) {
        this.insuranceApproved = insuranceApproved;
    }

    public Boolean getAutoCreateTriage() {
        return autoCreateTriage;
    }

    public void setAutoCreateTriage(Boolean autoCreateTriage) {
        this.autoCreateTriage = autoCreateTriage;
    }

    public BillPaymentType getBillPaymentType() {
        return billPaymentType;
    }

    public void setBillPaymentType(BillPaymentType billPaymentType) {
        this.billPaymentType = billPaymentType;
    }

    public ConsultationType getConsultationType() {
        return consultationType;
    }

    public void setConsultationType(ConsultationType consultationType) {
        this.consultationType = consultationType;
    }

    public Boolean isSentToJms() {
        return sentToJms;
    }

    public void setSentToJms(Boolean sentToJms) {
        this.sentToJms = sentToJms;
    }

    public Boolean getNewPatient() {
        return newPatient;
    }

    public void setNewPatient(Boolean newPatient) {
        this.newPatient = newPatient;
    }

    public Integer getPatientAge() {
        return patientAge;
    }

    public void setPatientAge(Integer patientAge) {
        this.patientAge = patientAge;
    }
}
