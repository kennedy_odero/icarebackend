package com.misoft.hospital.model.healthcare;

import com.misoft.hospital.model.base.ModelBase;
import com.misoft.hospital.model.setup.Ward;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.Date;

/**
 * Created by kodero on 4/1/15.
 */
@Entity
@Table(name = "ward_round_teams")
@Filter(name = "filterByDeleted")
public class WardRoundTeam extends ModelBase {

    @NotNull
    @Column(name = "team_date")
    private Date teamDate;

    @NotNull
    @Column(name = "team_id")
    private Long teamId;

    @JoinColumn(name = "ward", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Ward ward;

    @OneToMany(mappedBy = "wardRoundTeam", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @Filter(name = "filterByDeleted")
    private Collection<WardAllocation> wardAllocations;

    @OneToMany(mappedBy = "wardRoundTeam", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @Filter(name = "filterByDeleted")
    private Collection<WardRoundStaff> wardRoundStaff;

    public Date getTeamDate() {
        return teamDate;
    }

    public void setTeamDate(Date teamDate) {
        this.teamDate = teamDate;
    }

    public Long getTeamId() {
        return teamId;
    }

    public void setTeamId(Long teamId) {
        this.teamId = teamId;
    }

    public Collection<WardAllocation> getWardAllocations() {
        return wardAllocations;
    }

    public void setWardAllocations(Collection<WardAllocation> wardAllocations) {
        this.wardAllocations = wardAllocations;
    }

    public Collection<WardRoundStaff> getWardRoundStaff() {
        return wardRoundStaff;
    }

    public void setWardRoundStaff(Collection<WardRoundStaff> wardRoundStaff) {
        this.wardRoundStaff = wardRoundStaff;
    }

    public Ward getWard() {
        return ward;
    }

    public void setWard(Ward ward) {
        this.ward = ward;
    }
}
