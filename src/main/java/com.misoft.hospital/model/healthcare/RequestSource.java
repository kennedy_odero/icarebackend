package com.misoft.hospital.model.healthcare;

/**
 * Created by kodero on 9/2/15.
 */
public enum RequestSource {
    Internal,
    External
}
