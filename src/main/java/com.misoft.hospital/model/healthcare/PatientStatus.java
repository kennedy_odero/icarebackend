package com.misoft.hospital.model.healthcare;

/**
 * Created by kodero on 6/24/15.
 */
public enum PatientStatus {
    _,
    ADMITTED,//@Deprecated
    DISCHARGED,//@Deprecated
    DECEASED,
    ALIVE
}
