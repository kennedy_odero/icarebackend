package com.misoft.hospital.model.healthcare;

import com.misoft.hospital.model.setup.Bed;
import com.misoft.hospital.model.setup.BedStatus;
import com.misoft.hospital.model.setup.Ward;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by kodero on 3/20/15.
 */
@Entity
@Table(name = "admission_beds")
@Filter(name = "filterByDeleted")
public class AdmissionBed extends ServiceRequestItem {

    @JoinColumn(name = "bed", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Bed bed;

    @JoinColumn(name = "ward", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Ward ward;

    @Column(name = "date_from")
    private Date dateFrom;

    @Column(name = "date_to")
    private Date dateTo;

    @JoinColumn(name = "admission_req", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private AdmissionRequest admissionRequest;

    @Column(name = "bed_status")
    private BedStatus bedStatus;

    public AdmissionBed(){

    }

    public AdmissionBed(Bed bed, Ward ward, Date dateFrom){
        setBed(bed);
        setWard(ward);
        setDateFrom(dateFrom);
    }

    public Bed getBed() {
        return bed;
    }

    public void setBed(Bed bed) {
        this.bed = bed;
    }

    public AdmissionRequest getAdmissionRequest() {
        return admissionRequest;
    }

    public void setAdmissionRequest(AdmissionRequest admissionRequest) {
        this.admissionRequest = admissionRequest;
    }

    public Ward getWard() {
        return ward;
    }

    public void setWard(Ward ward) {
        this.ward = ward;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public BedStatus getBedStatus() {
        return bedStatus;
    }

    public void setBedStatus(BedStatus bedStatus) {
        this.bedStatus = bedStatus;
    }
}
