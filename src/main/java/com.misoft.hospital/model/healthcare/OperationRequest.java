package com.misoft.hospital.model.healthcare;

import com.misoft.hospital.model.billing.BillItem;
import com.misoft.hospital.model.billing.BillItemType;
import com.misoft.hospital.model.billing.BilledFrom;
import com.misoft.hospital.model.billing.PatientBill;
import com.misoft.hospital.model.setup.Disease;
import com.misoft.hospital.model.setup.Product;
import com.misoft.hospital.model.setup.Staff;
import com.misoft.hospital.service.billing.BillableRequest;
import com.misoft.hospital.util.validation.ValidationMethod;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import javax.validation.ValidationException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Created by kodero on 3/20/15.
 */
@Entity
@DiscriminatorValue("OPERATION")
@Filter(name = "filterByDeleted")
public class OperationRequest extends ServiceRequest implements BillableRequest {

    @Column(name = "scheduled_date")
    private Date scheduledDate;

    @JoinColumn(name = "lead_surgeon", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Staff leadSurgeon;

    @Column(name = "start_time")
    private Date startTime;

    @Column(name = "end_time")
    private Date endTime;

    @OneToMany(mappedBy = "operationRequest", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @Filter(name = "filterByDeleted")
    private Collection<OperationStaff> operationStaffs;

    @OneToMany(mappedBy = "operationRequest", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @Filter(name = "filterByDeleted")
    private Collection<OperationRequestChecklist> operationRequestChecklists;

    @OneToMany(mappedBy = "operationRequest", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @Filter(name = "filterByDeleted")
    private Collection<RequestMedicalProcedure> requestMedicalProcedures = new ArrayList<>();

    @Column(name = "pre_op_notes", columnDefinition = "text")
    private String preOperativeNotes;

    @Column(name = "surgical_notes", columnDefinition = "text")
    private String surgicalNotes;

    @JoinColumn(name = "pre_op_diagnosis", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Disease preOperativeDiagnosis;

    @JoinColumn(name = "post_op_diagnosis", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Disease postOperativeDiagnosis;

    @Column(name = "intra_op_diagnosis")
    private String intraOperativeDiagnosis;

    @Column(name = "intra_op_notes")
    private String intraOperativeNotes;

    @Column(name = "operation_type")
    @Enumerated(EnumType.STRING)
    private OperationType operationType;

    @Column(name = "date_scheduled")
    private Date dateScheduled;

    @Column(name = "time_scheduled")
    private String timeScheduled;

    @Column(name = "duration")
    private Integer duration;

    @Column(name = "notes_on_schedule")
    private String notesOnSchedule;

    @JoinColumn(name = "surgical_room", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private SurgicalRoom surgicalRoom;

    //patient risk assesment

    @Column(name = "asaps")
    private String asaps;

    @Column(name = "rcri")
    private String rcri;

    @Column(name = "mallampati_score")
    private String mallampatiScore;

    public OperationRequest(){

    }

    public OperationRequest(Long requestNo, RequestSource requestSource, Patient patient, Date dateRequested, Staff staff, RequestStatus requestStatus, Appointment appointment){
        super(requestNo, requestSource, patient, dateRequested, staff, requestStatus, appointment);
    }

    public void extraValidations(){
        //perform any other validation
        if(getRequestMedicalProcedures() == null || getRequestMedicalProcedures().size() == 0){
            throw new ValidationException("Please add at least one procedure to be done!");
        }
    }

    @Override
    public PatientBill billRequest(PatientBill patientBill){
        Long startTime = System.currentTimeMillis();
        for(RequestMedicalProcedure item : getRequestMedicalProcedures()){
            Product product = item.getMedicalProcedure().getProduct();
            BillItem billItem = new BillItem();
            if(item.getBillItem() != null) billItem = item.getBillItem();//just revise the amount billed if it was already billed
            if(billItem.getBillNo() == null) billItem.setBillNo(startTime);
            item.setBillItem(billItem);
            billItem.setProduct(product);
            billItem.setQuantity(BigDecimal.ONE);
            billItem.setUnitPrice(product.getListPrice());
            billItem.setLineTotal(product.getListPrice().multiply(billItem.getQuantity()));
            billItem.setTax(BigDecimal.ZERO);
            billItem.setSrType(this.getServiceType());
            billItem.setSrFqn(this.getClass().getName());
            billItem.setSrId(this.getId());
            billItem.setBilledFrom(BilledFrom.SERVICE_REQUEST);
            //billItem.setServiceCenter(this.getServiceCenter());
            billItem.setBillItemType(BillItemType.Normal);
            patientBill.addBillItem(billItem);
            startTime++;
        }
        return patientBill;
    }

    @ValidationMethod
    public void validateEntries(){
        if (getRequestMedicalProcedures().size() == 0) throw new ValidationException("Please add at least 1 procedure to be done!");
        for (RequestMedicalProcedure lri : getRequestMedicalProcedures()) {
            if(lri.getMedicalProcedure() == null) throw new ValidationException("Please make sure all items have a procedure to be done!");
        }
    }

    public Date getScheduledDate() {
        return scheduledDate;
    }

    public void setScheduledDate(Date scheduledDate) {
        this.scheduledDate = scheduledDate;
    }

    public Staff getLeadSurgeon() {
        return leadSurgeon;
    }

    public void setLeadSurgeon(Staff leadSurgeon) {
        this.leadSurgeon = leadSurgeon;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Collection<OperationRequestChecklist> getOperationRequestChecklists() {
        return operationRequestChecklists;
    }

    public void setOperationRequestChecklists(Collection<OperationRequestChecklist> operationRequestChecklists) {
        this.operationRequestChecklists = operationRequestChecklists;
    }

    public String getSurgicalNotes() {
        return surgicalNotes;
    }

    public void setSurgicalNotes(String surgicalNotes) {
        this.surgicalNotes = surgicalNotes;
    }

    public Collection<OperationStaff> getOperationStaffs() {
        return operationStaffs;
    }

    public void setOperationStaffs(Collection<OperationStaff> operationStaffs) {
        this.operationStaffs = operationStaffs;
    }

    public Collection<RequestMedicalProcedure> getRequestMedicalProcedures() {
        return requestMedicalProcedures;
    }

    public void setRequestMedicalProcedures(Collection<RequestMedicalProcedure> requestMedicalProcedures) {
        this.requestMedicalProcedures = requestMedicalProcedures;
    }

    public String getPreOperativeNotes() {
        return preOperativeNotes;
    }

    public void setPreOperativeNotes(String preOperativeNotes) {
        this.preOperativeNotes = preOperativeNotes;
    }

    public Disease getPreOperativeDiagnosis() {
        return preOperativeDiagnosis;
    }

    public void setPreOperativeDiagnosis(Disease preOperativeDiagnosis) {
        this.preOperativeDiagnosis = preOperativeDiagnosis;
    }

    public String getIntraOperativeDiagnosis() {
        return intraOperativeDiagnosis;
    }

    public void setIntraOperativeDiagnosis(String intraOperativeDiagnosis) {
        this.intraOperativeDiagnosis = intraOperativeDiagnosis;
    }

    public Disease getPostOperativeDiagnosis() {
        return postOperativeDiagnosis;
    }

    public void setPostOperativeDiagnosis(Disease postOperativeDiagnosis) {
        this.postOperativeDiagnosis = postOperativeDiagnosis;
    }

    public OperationType getOperationType() {
        return operationType;
    }

    public void setOperationType(OperationType operationType) {
        this.operationType = operationType;
    }

    public Date getDateScheduled() {
        return dateScheduled;
    }

    public void setDateScheduled(Date dateScheduled) {
        this.dateScheduled = dateScheduled;
    }

    public String getNotesOnSchedule() {
        return notesOnSchedule;
    }

    public void setNotesOnSchedule(String notesOnSchedule) {
        this.notesOnSchedule = notesOnSchedule;
    }

    public String getIntraOperativeNotes() {
        return intraOperativeNotes;
    }

    public void setIntraOperativeNotes(String intraOperativeNotes) {
        this.intraOperativeNotes = intraOperativeNotes;
    }

    public SurgicalRoom getSurgicalRoom() {
        return surgicalRoom;
    }

    public void setSurgicalRoom(SurgicalRoom surgicalRoom) {
        this.surgicalRoom = surgicalRoom;
    }

    public String getAsaps() {
        return asaps;
    }

    public void setAsaps(String asaps) {
        this.asaps = asaps;
    }

    public String getRcri() {
        return rcri;
    }

    public void setRcri(String rcri) {
        this.rcri = rcri;
    }

    public String getMallampatiScore() {
        return mallampatiScore;
    }

    public void setMallampatiScore(String mallampatiScore) {
        this.mallampatiScore = mallampatiScore;
    }

    public String getTimeScheduled() {
        return timeScheduled;
    }

    public void setTimeScheduled(String timeScheduled) {
        this.timeScheduled = timeScheduled;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public void addOperationRequestItem(RequestMedicalProcedure requestMedicalProcedure) {
        if(requestMedicalProcedure != null){
            requestMedicalProcedure.setOperationRequest(this);
            getRequestMedicalProcedures().add(requestMedicalProcedure);
        }
    }

    public void addOperationRequestItems(List<RequestMedicalProcedure> requestMedicalProcedures) {
        for (RequestMedicalProcedure rmp : requestMedicalProcedures){
            if(rmp != null){
                rmp.setOperationRequest(this);
                getRequestMedicalProcedures().add(rmp);
            }
        }
    }
}
