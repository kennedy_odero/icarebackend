package com.misoft.hospital.model.healthcare;

import com.misoft.hospital.model.base.ModelBase;
import com.misoft.hospital.model.setup.Disease;
import com.misoft.hospital.service.callbacks.ConsultationDiagnosisCallback;
import com.misoft.hospital.util.callbacks.EntityCallbackClass;

import javax.persistence.*;

/**
 * Created by kodero on 5/25/15.
 */
@Entity
@Table(name = "consultation_diagnoses")
@EntityCallbackClass(ConsultationDiagnosisCallback.class)
public class ConsultationDiagnosis extends ModelBase{

    @Column(name = "comments")
    private String comments;

    @JoinColumn(name = "disease", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Disease disease;

    @JoinColumn(name = "conslt_request", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private ConsultationRequest consultationRequest;

    @Column(name = "age")
    private Integer age;

    @Column(name = "age_group")
    private String ageGroup;

    @Column(name = "new_patient", columnDefinition = "tinyint default '1'")
    private Boolean newPatient;

    @JoinColumn(name = "appointment", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Appointment appointment;

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Disease getDisease() {
        return disease;
    }

    public void setDisease(Disease disease) {
        this.disease = disease;
    }

    public ConsultationRequest getConsultationRequest() {
        return consultationRequest;
    }

    public void setConsultationRequest(ConsultationRequest consultationRequest) {
        this.consultationRequest = consultationRequest;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getAgeGroup() {
        return ageGroup;
    }

    public void setAgeGroup(String ageGroup) {
        this.ageGroup = ageGroup;
    }

    public Boolean getNewPatient() {
        return newPatient;
    }

    public void setNewPatient(Boolean newPatient) {
        this.newPatient = newPatient;
    }

    public Appointment getAppointment() {
        return appointment;
    }

    public void setAppointment(Appointment appointment) {
        this.appointment = appointment;
    }
}
