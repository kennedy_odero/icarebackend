package com.misoft.hospital.model.healthcare;

/**
 * Created by kodero on 1/25/16.
 */
public enum ReferralType {
    Internal,
    External
}
