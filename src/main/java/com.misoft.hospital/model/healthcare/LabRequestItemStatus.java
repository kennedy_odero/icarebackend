package com.misoft.hospital.model.healthcare;

/**
 * Created by kodero on 2/18/16.
 */
public enum LabRequestItemStatus {
    Sample_Received,
    Incomplete,
    Complete,
    Authorized,
    Released,
    Cancelled
}
