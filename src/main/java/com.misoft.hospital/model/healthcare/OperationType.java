package com.misoft.hospital.model.healthcare;

/**
 * Created by kodero on 9/4/15.
 */
public enum OperationType {
    Emergency,
    Elective
}
