package com.misoft.hospital.model.healthcare;

/**
 * Created by kodero on 10/27/15.
 */
public enum ProblemRecurrence {
    Frequently,
    Less_frequently,
    Just_once
}
