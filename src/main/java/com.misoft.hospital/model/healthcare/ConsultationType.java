package com.misoft.hospital.model.healthcare;

import com.misoft.hospital.model.base.ModelBase;
import com.misoft.hospital.model.setup.Product;
import com.misoft.hospital.model.setup.Unit;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by kodero on 9/14/15.
 */
@Entity
@Table(name = "consultation_types")
@Filter(name = "filterByDeleted")
public class ConsultationType extends ModelBase{

    @NotNull
    @Column(name = "name")
    private String name;

    @JoinColumn(name = "product", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Product product;

    @JoinColumn(name = "service_center", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private ServiceCenter serviceCenter;

    @Column(name = "visit_type")
    @Enumerated(EnumType.STRING)
    private VisitType visitType;

    @JoinColumn(name = "special_clinic", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private SpecialClinic specialClinic;

    @JoinColumn(name = "unit", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Unit unit;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ServiceCenter getServiceCenter() {
        return serviceCenter;
    }

    public void setServiceCenter(ServiceCenter serviceCenter) {
        this.serviceCenter = serviceCenter;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public VisitType getVisitType() {
        return visitType;
    }

    public void setVisitType(VisitType visitType) {
        this.visitType = visitType;
    }

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    public SpecialClinic getSpecialClinic() {
        return specialClinic;
    }

    public void setSpecialClinic(SpecialClinic specialClinic) {
        this.specialClinic = specialClinic;
    }
}
