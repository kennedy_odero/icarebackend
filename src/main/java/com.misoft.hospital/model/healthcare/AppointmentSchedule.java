package com.misoft.hospital.model.healthcare;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;

/**
 * Created by kodero on 9/11/15.
 */
@Entity
@Table(name = "appointment_schedules")
@Filter(name = "filterByDeleted")
public class AppointmentSchedule extends ModelBase{

    @JoinColumn(name = "clinic", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private SpecialClinic clinic;

    @Column(name = "effective_from")
    @Temporal(TemporalType.DATE)
    private Date effectiveFrom;

    @OneToMany(mappedBy = "appointmentSchedule", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @Filter(name = "filterByDeleted")
    private Collection<ScheduleDay> scheduleDays;

    public SpecialClinic getClinic() {
        return clinic;
    }

    public void setClinic(SpecialClinic clinic) {
        this.clinic = clinic;
    }

    public Date getEffectiveFrom() {
        return effectiveFrom;
    }

    public void setEffectiveFrom(Date effectiveFrom) {
        this.effectiveFrom = effectiveFrom;
    }

    public Collection<ScheduleDay> getScheduleDays() {
        return scheduleDays;
    }

    public void setScheduleDays(Collection<ScheduleDay> scheduleDays) {
        this.scheduleDays = scheduleDays;
    }
}
