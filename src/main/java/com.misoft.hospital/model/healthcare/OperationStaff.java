package com.misoft.hospital.model.healthcare;

import com.misoft.hospital.model.base.ModelBase;
import com.misoft.hospital.model.setup.Staff;
import org.hibernate.annotations.Filter;

import javax.persistence.*;

/**
 * Created by kodero on 3/26/15.
 */
@Entity
@Table(name = "operation_staff")
@Filter(name = "filterByDeleted")
public class OperationStaff extends ModelBase {

    @JoinColumn(name = "staff", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Staff staff;

    @JoinColumn(name = "role", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private OperationRole operationRole;

    @JoinColumn(name = "request", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private OperationRequest operationRequest;

    public Staff getStaff() {
        return staff;
    }

    public void setStaff(Staff staff) {
        this.staff = staff;
    }

    public OperationRole getOperationRole() {
        return operationRole;
    }

    public void setOperationRole(OperationRole operationRole) {
        this.operationRole = operationRole;
    }

    public OperationRequest getOperationRequest() {
        return operationRequest;
    }

    public void setOperationRequest(OperationRequest operationRequest) {
        this.operationRequest = operationRequest;
    }
}
