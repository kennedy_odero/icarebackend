package com.misoft.hospital.model.healthcare;

import com.misoft.hospital.model.base.ModelBase;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by kodero on 2/23/16.
 */
@Entity
@Table(name = "hpcs")
public class Hpc extends ModelBase{

    @NotNull
    @JoinColumn(name = "consultation_request", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private ConsultationRequest consultationRequest;

    @Column(name = "name")
    private String name;

    @Column(name = "site")
    private String site;//Where exactly is the pain?

    @Column(name = "onset")
    private String onset;//When did it start, was it constant/intermittent, gradual/ sudden?

    @Column(name = "hpc_character")
    private String hpcCharacter;//What is the pain like e.g. sharp, burning, tight?

    @Column(name = "radiation")
    private String radiation;//Does it radiate/move anywhere?

    @Column(name = "associations")
    private String associations;//Is there anything else associated with the pain e.g. sweating, vomiting

    @Column(name = "time_course")
    private String timeCourse;//Does it follow any time pattern, how long did it last?

    @Column(name = "ex_rel_factors")
    private String exacerbatingRelievingFactors;//Does anything make it better or worse?

    @Column(name = "intensity")
    private String intensity;//How severe is the pain, consider using the 1-10 scale?

    public ConsultationRequest getConsultationRequest() {
        return consultationRequest;
    }

    public void setConsultationRequest(ConsultationRequest consultationRequest) {
        this.consultationRequest = consultationRequest;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getOnset() {
        return onset;
    }

    public void setOnset(String onset) {
        this.onset = onset;
    }

    public String getHpcCharacter() {
        return hpcCharacter;
    }

    public void setHpcCharacter(String hpcCharacter) {
        this.hpcCharacter = hpcCharacter;
    }

    public String getRadiation() {
        return radiation;
    }

    public void setRadiation(String radiation) {
        this.radiation = radiation;
    }

    public String getAssociations() {
        return associations;
    }

    public void setAssociations(String associations) {
        this.associations = associations;
    }

    public String getTimeCourse() {
        return timeCourse;
    }

    public void setTimeCourse(String timeCourse) {
        this.timeCourse = timeCourse;
    }

    public String getExacerbatingRelievingFactors() {
        return exacerbatingRelievingFactors;
    }

    public void setExacerbatingRelievingFactors(String exacerbatingRelievingFactors) {
        this.exacerbatingRelievingFactors = exacerbatingRelievingFactors;
    }

    public String getIntensity() {
        return intensity;
    }

    public void setIntensity(String intensity) {
        this.intensity = intensity;
    }
}
