package com.misoft.hospital.model.healthcare;

import com.misoft.hospital.model.base.ModelBase;

import javax.persistence.*;

/**
 * Created by kodero on 2/17/16.
 */
@Entity
@Table(name = "patient_diets")
public class PatientDiet extends ModelBase{

    @JoinColumn(name = "admission_request", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private AdmissionRequest admissionRequest;

    @JoinColumn(name = "diet", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Diet diet;

    public AdmissionRequest getAdmissionRequest() {
        return admissionRequest;
    }

    public void setAdmissionRequest(AdmissionRequest admissionRequest) {
        this.admissionRequest = admissionRequest;
    }

    public Diet getDiet() {
        return diet;
    }

    public void setDiet(Diet diet) {
        this.diet = diet;
    }
}
