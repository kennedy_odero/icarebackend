package com.misoft.hospital.model.healthcare;

/**
 * Created by kodero on 4/27/16.
 */
public enum BillPaymentType {
    SELF,
    CORPORATE
}
