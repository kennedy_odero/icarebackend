package com.misoft.hospital.model.healthcare;

/**
 * Created by kodero on 12/12/15.
 */
public enum DischargeType {
    NORMAL,//patient has recovered
    DECEASED,//patient has died
    OWN_REFERRAL,//patient has requested referral
    MEDICAL_REFERRAL
}
