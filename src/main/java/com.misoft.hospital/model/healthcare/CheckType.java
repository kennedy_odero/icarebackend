package com.misoft.hospital.model.healthcare;

/**
 * Created by kodero on 11/17/15.
 */
public enum CheckType {
    ANC,
    PNC
}
