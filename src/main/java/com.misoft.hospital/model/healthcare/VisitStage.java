package com.misoft.hospital.model.healthcare;

/**
 * Created by kodero on 10/30/15.
 */
public enum VisitStage {
    First,
    Subsequent,
    Both
}
