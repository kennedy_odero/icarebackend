package com.misoft.hospital.model.healthcare;

import com.misoft.hospital.model.base.ModelBase;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by kodero on 2/23/16.
 */
@Entity
@Table(name = "system_problems")
public class SystemProblem extends ModelBase{

    @NotNull
    @Column(name = "problem")
    private String problem;

    @Column(name = "description")
    private String description;

    @JoinColumn(name = "review_system", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private ReviewSystem reviewSystem;

    public ReviewSystem getReviewSystem() {
        return reviewSystem;
    }

    public void setReviewSystem(ReviewSystem reviewSystem) {
        this.reviewSystem = reviewSystem;
    }

    public String getProblem() {
        return problem;
    }

    public void setProblem(String problem) {
        this.problem = problem;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
