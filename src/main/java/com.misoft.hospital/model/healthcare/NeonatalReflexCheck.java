package com.misoft.hospital.model.healthcare;

import com.misoft.hospital.model.base.ModelBase;

import javax.persistence.*;

/**
 * Created by kodero on 8/18/15.
 */
@Entity
@Table(name = "neonatal_reflex_check")
public class NeonatalReflexCheck extends ModelBase{

    @JoinColumn(name = "patient", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Patient patient;

    @JoinColumn(name = "neonatal_reflex", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private NeonatalReflex neonatalReflex;

    @Column(name = "checked")
    private Boolean checked;

    public NeonatalReflex getNeonatalReflex() {
        return neonatalReflex;
    }

    public void setNeonatalReflex(NeonatalReflex neonatalReflex) {
        this.neonatalReflex = neonatalReflex;
    }

    public Boolean getChecked() {
        return checked;
    }

    public void setChecked(Boolean checked) {
        this.checked = checked;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }
}
