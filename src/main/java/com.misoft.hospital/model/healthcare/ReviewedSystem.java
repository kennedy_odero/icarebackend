package com.misoft.hospital.model.healthcare;

import com.misoft.hospital.model.base.ModelBase;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by kodero on 2/23/16.
 */
@Entity
@Table(name = "reviewed_systems")
public class ReviewedSystem extends ModelBase{

    @JoinColumn(name = "consultation_request", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private ConsultationRequest consultationRequest;

    @JoinColumn(name = "system", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private ReviewSystem reviewSystem;

    @NotNull
    @Column(name = "reviewed")
    private Boolean reviewed;

    @Column(name = "summary")
    private String systemSummary;

    public ConsultationRequest getConsultationRequest() {
        return consultationRequest;
    }

    public void setConsultationRequest(ConsultationRequest consultationRequest) {
        this.consultationRequest = consultationRequest;
    }

    public ReviewSystem getReviewSystem() {
        return reviewSystem;
    }

    public void setReviewSystem(ReviewSystem reviewSystem) {
        this.reviewSystem = reviewSystem;
    }

    public Boolean getReviewed() {
        return reviewed;
    }

    public void setReviewed(Boolean reviewed) {
        this.reviewed = reviewed;
    }

    public String getSystemSummary() {
        return systemSummary;
    }

    public void setSystemSummary(String systemSummary) {
        this.systemSummary = systemSummary;
    }
}
