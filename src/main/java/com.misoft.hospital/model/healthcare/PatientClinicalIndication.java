package com.misoft.hospital.model.healthcare;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by kodero on 3/15/15.
 */
@Entity
@Table(name = "patient_clinical_indics")
@Filter(name = "filterByDeleted")
public class PatientClinicalIndication extends ModelBase {
    @JoinColumn(name = "patient", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Patient patient;

    @JoinColumn(name = "clinical_indication", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private ClinicalIndication clinicalIndication;

    @Column(name = "value")
    private BigDecimal value;

    @Column(name = "comments")
    private String comments;

    @JoinColumn(name = "triaging_req", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private TriagingRequest triagingRequest;

    @JoinColumn(name = "consultation_req", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private ConsultationRequest consultationRequest;

    @JoinColumn(name = "vital_monitoring", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private VitalMonitoring vitalMonitoring;

    public PatientClinicalIndication(){

    }

    public PatientClinicalIndication(ClinicalIndication clinicalIndication){
        setClinicalIndication(clinicalIndication);
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public ClinicalIndication getClinicalIndication() {
        return clinicalIndication;
    }

    public void setClinicalIndication(ClinicalIndication clinicalIndication) {
        this.clinicalIndication = clinicalIndication;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public TriagingRequest getTriagingRequest() {
        return triagingRequest;
    }

    public void setTriagingRequest(TriagingRequest triagingRequest) {
        this.triagingRequest = triagingRequest;
    }

    public VitalMonitoring getVitalMonitoring() {
        return vitalMonitoring;
    }

    public void setVitalMonitoring(VitalMonitoring vitalMonitoring) {
        this.vitalMonitoring = vitalMonitoring;
    }

    public ConsultationRequest getConsultationRequest() {
        return consultationRequest;
    }

    public void setConsultationRequest(ConsultationRequest consultationRequest) {
        this.consultationRequest = consultationRequest;
    }
}
