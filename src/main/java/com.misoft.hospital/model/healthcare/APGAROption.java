package com.misoft.hospital.model.healthcare;

import com.misoft.hospital.model.base.ModelBase;

import javax.persistence.*;

/**
 * Created by kodero on 8/18/15.
 */
@Entity
@Table(name = "apgar_options")
public class APGAROption extends ModelBase{

    @Column(name = "name")
    private String name;

    @Column(name = "score")
    private int score;

    @Column(name = "category")
    @Enumerated(EnumType.STRING)
    private APGARCategory category;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public APGARCategory getCategory() {
        return category;
    }

    public void setCategory(APGARCategory category) {
        this.category = category;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
