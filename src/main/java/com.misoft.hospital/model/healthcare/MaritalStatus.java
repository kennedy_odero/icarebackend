package com.misoft.hospital.model.healthcare;

/**
 * Created by kodero on 8/4/15.
 */
public enum MaritalStatus {
    SINGLE,
    MARRIED,
    SEPARATED,
    DIVORCED,
    WIDOWED,
    WIDOWER,
    NOT_KNOWN
}
