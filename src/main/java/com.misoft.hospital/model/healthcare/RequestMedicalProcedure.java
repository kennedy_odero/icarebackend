package com.misoft.hospital.model.healthcare;

import com.misoft.hospital.model.setup.MedicalProcedure;
import org.hibernate.annotations.Filter;

import javax.persistence.*;

/**
 * Created by kodero on 3/26/15.
 */
@Entity
@Table(name = "operation_req_procedures")
@Filter(name = "filterByDeleted")
public class RequestMedicalProcedure extends ServiceRequestItem {

    @JoinColumn(name = "operation_request", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private OperationRequest operationRequest;

    @JoinColumn(name = "medical_procedure", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private MedicalProcedure medicalProcedure;

    @Column(name = "comments")
    private String comments;

    public RequestMedicalProcedure(){

    }

    public RequestMedicalProcedure(MedicalProcedure medicalProcedure){
        setMedicalProcedure(medicalProcedure);
    }

    public OperationRequest getOperationRequest() {
        return operationRequest;
    }

    public void setOperationRequest(OperationRequest operationRequest) {
        this.operationRequest = operationRequest;
    }

    public MedicalProcedure getMedicalProcedure() {
        return medicalProcedure;
    }

    public void setMedicalProcedure(MedicalProcedure medicalProcedure) {
        this.medicalProcedure = medicalProcedure;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }
}
