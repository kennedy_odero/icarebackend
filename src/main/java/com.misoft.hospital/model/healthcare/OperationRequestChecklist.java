package com.misoft.hospital.model.healthcare;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.*;

/**
 * Created by kodero on 3/25/15.
 */
@Entity
@Table(name = "operation_req_checklist")
@Filter(name = "filterByDeleted")
public class OperationRequestChecklist extends ModelBase {

    @JoinColumn(name = "checklist", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private OperationChecklist operationChecklist;

    @Column(name = "passed")
    private Boolean passed;

    @Column(name = "comments")
    private String comments;

    @JoinColumn(name = "operation_request", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private OperationRequest operationRequest;

    public OperationChecklist getOperationChecklist() {
        return operationChecklist;
    }

    public void setOperationChecklist(OperationChecklist operationChecklist) {
        this.operationChecklist = operationChecklist;
    }

    public Boolean getPassed() {
        return passed;
    }

    public void setPassed(Boolean passed) {
        this.passed = passed;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public OperationRequest getOperationRequest() {
        return operationRequest;
    }

    public void setOperationRequest(OperationRequest operationRequest) {
        this.operationRequest = operationRequest;
    }
}
