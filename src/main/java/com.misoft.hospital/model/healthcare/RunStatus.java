package com.misoft.hospital.model.healthcare;

/**
 * Created by kodero on 12/13/15.
 */
public enum RunStatus {
    SCHEDULED,
    CANCELLED,
}
