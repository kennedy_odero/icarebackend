package com.misoft.hospital.model.healthcare;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by kodero on 4/1/15.
 */
@Entity
@Table(name = "nursing_care")
@Filter(name = "filterByDeleted")
public class NursingCare extends ModelBase{

    @JoinColumn(name = "patient", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Patient patient;

    @Column(name = "round_date")
    private Date date;

    @JoinColumn(name = "team", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private WardRoundTeam wardRoundTeam;

    @Column(name = "days_post_op")
    private Integer daysPostOp;
    //soap
    @Column(name = "subjective", columnDefinition = "text")
    private String subjective;

    @Column(name = "objective", columnDefinition = "text")
    private String objective;

    @Column(name = "assesment", columnDefinition = "text")
    private String assessment;

    @Column(name = "plan", columnDefinition = "text")
    private String plan;

    @Column(name = "intervention", columnDefinition = "text")
    private String intervention;

    @Column(name = "rationale", columnDefinition = "text")
    private String rationale;

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public WardRoundTeam getWardRoundTeam() {
        return wardRoundTeam;
    }

    public void setWardRoundTeam(WardRoundTeam wardRoundTeam) {
        this.wardRoundTeam = wardRoundTeam;
    }

    public Integer getDaysPostOp() {
        return daysPostOp;
    }

    public void setDaysPostOp(Integer daysPostOp) {
        this.daysPostOp = daysPostOp;
    }

    public String getSubjective() {
        return subjective;
    }

    public void setSubjective(String subjective) {
        this.subjective = subjective;
    }

    public String getObjective() {
        return objective;
    }

    public void setObjective(String objective) {
        this.objective = objective;
    }

    public String getPlan() {
        return plan;
    }

    public void setPlan(String plan) {
        this.plan = plan;
    }

    public String getAssessment() {
        return assessment;
    }

    public void setAssessment(String assessment) {
        this.assessment = assessment;
    }

    public String getIntervention() {
        return intervention;
    }

    public void setIntervention(String intervention) {
        this.intervention = intervention;
    }

    public String getRationale() {
        return rationale;
    }

    public void setRationale(String rationale) {
        this.rationale = rationale;
    }
}
