package com.misoft.hospital.model.healthcare;

/**
 * Created by kodero on 8/18/15.
 */
public enum Severity {
    Mild,
    Moderate,
    Severe
}
