package com.misoft.hospital.model.healthcare;

import com.misoft.hospital.model.base.ModelBase;

import javax.persistence.*;

/**
 * Created by kodero on 9/11/15.
 */
@Entity
@Table(name = "schedule_days")
public class ScheduleDay extends ModelBase{

    @JoinColumn(name = "appt_schedule", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private AppointmentSchedule appointmentSchedule;

    @Column(name = "day_of_week")
    @Enumerated(EnumType.STRING)
    private DayOfWeek dayOfWeek;

    @Column(name = "enabled")
    private Boolean enabled;

    @Column(name = "opening")
    private String opening;

    @Column(name = "closing")
    private String closing;

    public DayOfWeek getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(DayOfWeek dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public String getOpening() {
        return opening;
    }

    public void setOpening(String opening) {
        this.opening = opening;
    }

    public String getClosing() {
        return closing;
    }

    public void setClosing(String closing) {
        this.closing = closing;
    }

    public AppointmentSchedule getAppointmentSchedule() {
        return appointmentSchedule;
    }

    public void setAppointmentSchedule(AppointmentSchedule appointmentSchedule) {
        this.appointmentSchedule = appointmentSchedule;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }
}
