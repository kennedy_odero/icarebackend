package com.misoft.hospital.model.healthcare;

/**
 * Created by kodero on 9/11/15.
 */
public enum DayOfWeek {
    Monday,
    Tuesday,
    Wednesday,
    Thursday,
    Friday,
    Saturday,
    Sunday
}
