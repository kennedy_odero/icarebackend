package com.misoft.hospital.model.healthcare;

import com.misoft.hospital.model.base.ModelBase;
import com.misoft.hospital.model.shared.YesNo;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by kodero on 10/27/15.
 */
@Entity
@Table(name = "risk_factors")
public class RiskFactor extends ModelBase{

    @JoinColumn(name = "substance", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private RiskSubstance riskSubstance;

    @Enumerated(EnumType.STRING)
    @Column(name = "substance_used")
    private YesNo substanceUsed;

    @Column(name = "used_from")
    private Date usedFrom;

    @Column(name = "history", columnDefinition = "text")
    private String history;

    @JoinColumn(name = "patient", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Patient patient;

    public RiskSubstance getRiskSubstance() {
        return riskSubstance;
    }

    public void setRiskSubstance(RiskSubstance riskSubstance) {
        this.riskSubstance = riskSubstance;
    }

    public YesNo getSubstanceUsed() {
        return substanceUsed;
    }

    public void setSubstanceUsed(YesNo substanceUsed) {
        this.substanceUsed = substanceUsed;
    }

    public Date getUsedFrom() {
        return usedFrom;
    }

    public void setUsedFrom(Date usedFrom) {
        this.usedFrom = usedFrom;
    }

    public String getHistory() {
        return history;
    }

    public void setHistory(String history) {
        this.history = history;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }
}
