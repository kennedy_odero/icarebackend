package com.misoft.hospital.model.healthcare;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by kodero on 8/30/15.
 */
@Entity
@Table(name = "ward_rounds")
@Filter(name = "filterByDeleted")
public class WardRound extends ModelBase{

    @JoinColumn(name = "team", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private WardRoundTeam wardRoundTeam;

    @JoinColumn(name = "patient", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Patient patient;

    @Column(name = "_date")
    private Date date;

    @Column(name = "current_complaint")
    private String currentComplaint;

    @Column(name = "assessment")
    private String assessment;

    @Column(name = "recommendations")
    private String recommendations;

    public WardRoundTeam getWardRoundTeam() {
        return wardRoundTeam;
    }

    public void setWardRoundTeam(WardRoundTeam wardRoundTeam) {
        this.wardRoundTeam = wardRoundTeam;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getCurrentComplaint() {
        return currentComplaint;
    }

    public void setCurrentComplaint(String currentComplaint) {
        this.currentComplaint = currentComplaint;
    }

    public String getAssessment() {
        return assessment;
    }

    public void setAssessment(String assessment) {
        this.assessment = assessment;
    }

    public String getRecommendations() {
        return recommendations;
    }

    public void setRecommendations(String recommendations) {
        this.recommendations = recommendations;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }
}
