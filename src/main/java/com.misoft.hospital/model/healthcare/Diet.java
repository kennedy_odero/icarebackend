package com.misoft.hospital.model.healthcare;

import com.misoft.hospital.model.base.ModelBase;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * Created by kodero on 2/17/16.
 */
@Entity
@Table(name = "diets")
public class Diet extends ModelBase{

    @NotNull
    @Column(name = "diet_type")
    private String dietType;

    @NotNull
    @Column(name = "code")
    private String code;

    @Column(name = "description", columnDefinition = "text")
    private String description;

    public String getDietType() {
        return dietType;
    }

    public void setDietType(String dietType) {
        this.dietType = dietType;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
