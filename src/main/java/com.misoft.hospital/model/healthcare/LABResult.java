package com.misoft.hospital.model.healthcare;

/**
 * Created by kodero on 5/25/15.
 */
public enum LABResult {
    Positive,
    Negative,
    Indeterminate
}
