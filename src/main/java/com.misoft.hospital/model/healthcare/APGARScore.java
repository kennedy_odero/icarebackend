package com.misoft.hospital.model.healthcare;

import com.misoft.hospital.model.base.ModelBase;

import javax.persistence.*;

/**
 * Created by kodero on 8/18/15.
 */
@Entity
@Table(name = "appgar_scores")
public class APGARScore extends ModelBase{

    @JoinColumn(name = "patient", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Patient patient;

    @Column(name = "minute")
    private int minute;

    @Column(name = "score")
    private int aggregateScore;

    @JoinColumn(name = "appearance", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private APGAROption appearance;

    @JoinColumn(name = "grimace", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private APGAROption grimace;

    @JoinColumn(name = "pulse", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private APGAROption pulse;

    @JoinColumn(name = "activity", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private APGAROption activity;

    @JoinColumn(name = "respiration", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private APGAROption respiration;

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public int getAggregateScore() {
        return aggregateScore;
    }

    public void setAggregateScore(int aggregateScore) {
        this.aggregateScore = aggregateScore;
    }

    public APGAROption getAppearance() {
        return appearance;
    }

    public void setAppearance(APGAROption appearance) {
        this.appearance = appearance;
    }

    public APGAROption getGrimace() {
        return grimace;
    }

    public void setGrimace(APGAROption grimace) {
        this.grimace = grimace;
    }

    public APGAROption getPulse() {
        return pulse;
    }

    public void setPulse(APGAROption pulse) {
        this.pulse = pulse;
    }

    public APGAROption getActivity() {
        return activity;
    }

    public void setActivity(APGAROption activity) {
        this.activity = activity;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public APGAROption getRespiration() {
        return respiration;
    }

    public void setRespiration(APGAROption respiration) {
        this.respiration = respiration;
    }
}
