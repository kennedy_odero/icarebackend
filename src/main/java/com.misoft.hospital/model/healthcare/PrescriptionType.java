package com.misoft.hospital.model.healthcare;

/**
 * Created by kodero on 4/5/15.
 */
public enum PrescriptionType {
    IN_PATIENT,
    DISCHARGE
}