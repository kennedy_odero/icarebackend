package com.misoft.hospital.model.healthcare;

import org.hibernate.annotations.Filter;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by kodero on 4/12/15.
 */
@Entity
@Table(name = "request_consultations")
@Filter(name = "filterByDeleted")
public class RequestConsultation extends ServiceRequestItem {

    @JoinColumn(name = "consultation_type", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private ConsultationType consultationType;

    @JoinColumn(name = "conslt_request", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private ConsultationRequest consultationRequest;

    @Column(name = "line_total")
    private BigDecimal lineTotal;

    public RequestConsultation(){

    }

    public RequestConsultation(ConsultationRequest consultationRequest, ConsultationType consultationType){
        setConsultationRequest(consultationRequest);
        setConsultationType(consultationType);
    }

    public ConsultationRequest getConsultationRequest() {
        return consultationRequest;
    }

    public void setConsultationRequest(ConsultationRequest consultationRequest) {
        this.consultationRequest = consultationRequest;
    }

    public BigDecimal getLineTotal() {
        return lineTotal;
    }

    public void setLineTotal(BigDecimal lineTotal) {
        this.lineTotal = lineTotal;
    }

    public ConsultationType getConsultationType() {
        return consultationType;
    }

    public void setConsultationType(ConsultationType consultationType) {
        this.consultationType = consultationType;
    }
}
