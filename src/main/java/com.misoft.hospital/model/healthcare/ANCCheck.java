package com.misoft.hospital.model.healthcare;

import com.misoft.hospital.model.base.ModelBase;

import javax.persistence.*;

/**
 * Created by kodero on 10/28/15.
 */
@Entity
@Table(name = "anc_checks")
public class ANCCheck extends ModelBase{

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "stage")
    @Enumerated(EnumType.STRING)
    private VisitStage stage;

    @Column(name = "check_type")
    @Enumerated(EnumType.STRING)
    private CheckType checkType;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public VisitStage getStage() {
        return stage;
    }

    public void setStage(VisitStage stage) {
        this.stage = stage;
    }

    public CheckType getCheckType() {
        return checkType;
    }

    public void setCheckType(CheckType checkType) {
        this.checkType = checkType;
    }
}
