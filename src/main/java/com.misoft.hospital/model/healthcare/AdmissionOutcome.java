package com.misoft.hospital.model.healthcare;

/**
 * Created by kodero on 10/18/15.
 */
public enum AdmissionOutcome {
    Alive,
    Dead
}
