package com.misoft.hospital.model.healthcare;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 * Created by kodero on 4/22/15.
 */
@Entity
@Table(name = "vitals_monitoring")
@Filter(name = "filterByDeleted")
public class VitalMonitoring extends ModelBase {

    @Column(name = "date_taken")
    private Date dateTaken;

    @Column(name = "day_of_disease")
    private Integer dayOfDisease;

    @JoinColumn(name = "time", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REMOVE})
    private AdministrationTime time;

    @JoinColumn(name = "patient", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REMOVE})
    private Patient patient;

    @JoinColumn(name = "admission_request", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REMOVE})
    private AdmissionRequest admissionRequest;

    @OneToMany(mappedBy = "vitalMonitoring", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @Filter(name = "filterByDeleted")
    private Collection<PatientClinicalIndication> patientClinicalIndications = new ArrayList<>();

    public Date getDateTaken() {
        return dateTaken;
    }

    public void setDateTaken(Date dateTaken) {
        this.dateTaken = dateTaken;
    }

    public Integer getDayOfDisease() {
        return dayOfDisease;
    }

    public void setDayOfDisease(Integer dayOfDisease) {
        this.dayOfDisease = dayOfDisease;
    }

    public AdministrationTime getTime() {
        return time;
    }

    public void setTime(AdministrationTime time) {
        this.time = time;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public AdmissionRequest getAdmissionRequest() {
        return admissionRequest;
    }

    public void setAdmissionRequest(AdmissionRequest admissionRequest) {
        this.admissionRequest = admissionRequest;
    }

    public Collection<PatientClinicalIndication> getPatientClinicalIndications() {
        return patientClinicalIndications;
    }

    public void setPatientClinicalIndications(Collection<PatientClinicalIndication> patientClinicalIndications) {
        this.patientClinicalIndications = patientClinicalIndications;
    }
}
