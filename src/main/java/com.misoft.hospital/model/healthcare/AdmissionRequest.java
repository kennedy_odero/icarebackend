package com.misoft.hospital.model.healthcare;

import com.misoft.hospital.model.billing.BillItem;
import com.misoft.hospital.model.billing.BillItemType;
import com.misoft.hospital.model.billing.BilledFrom;
import com.misoft.hospital.model.billing.PatientBill;
import com.misoft.hospital.model.setup.Bed;
import com.misoft.hospital.model.setup.Product;
import com.misoft.hospital.model.setup.Ward;
import com.misoft.hospital.service.billing.BillableRequest;
import com.misoft.hospital.service.healthcare.AdmissionRequestCallback;
import com.misoft.hospital.util.callbacks.EntityCallbackClass;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.Formula;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Created by kodero on 3/11/15.
 */
@Entity
@DiscriminatorValue("ADMISSION")
@Filter(name = "filterByDeleted")
@EntityCallbackClass(AdmissionRequestCallback.class)
public class AdmissionRequest extends ServiceRequest implements BillableRequest {

    @OneToMany(mappedBy = "admissionRequest", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @Filter(name = "filterByDeleted")
    @OrderBy("created_at asc")
    private List<AdmissionBed> admissionBeds = new ArrayList<>();

    @OneToMany(mappedBy = "admissionRequest", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @Filter(name = "filterByDeleted")
    private Collection<PrescriptionRequestItem> prescriptionRequestItems = new ArrayList<>();

    @OneToMany(mappedBy = "admissionRequest", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @Filter(name = "filterByDeleted")
    private Collection<PrescriptionAdministration> prescriptionItemAdministrations;

    @Column(name = "pref_conditions", columnDefinition = "text")
    private String preferredConditions;

    @Column(name = "date_from")
    private Date dateFrom;

    @Column(name = "date_to")
    private Date dateTo;

    @Column(name = "next_visit")
    private Date nextVisit;

    @Column(name = "discharge_summary", columnDefinition = "text")
    private String dischargeSummary;

    @Column(name = "discharge_diet", columnDefinition = "text")
    private String dischargeDiet;

    @Column(name = "discharge_activity", columnDefinition = "text")
    private String dischargeActivity;

    @Column(name = "line_total")
    private BigDecimal lineTotal;

    @Enumerated(EnumType.STRING)
    @Column(name = "discharge_type")
    private DischargeType dischargeType;

    @Column(name = "date_deceased")
    private Date dateDeceased;

    @Column(name= "cause_of_death")
    private String causeOfDeath;

    @Column(name = "nature_of_death")
    private String natureOfDeath;

    @JoinColumn(name = "ward", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Ward ward;

    @JoinColumn(name = "current_bed", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Bed currentBed;

    //nutrition
    @JoinColumn(name = "belief", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Belief belief;

    @Column(name = "vegeterian")
    private String vegeterian;

    @Column(name = "other_nutr_notes", columnDefinition = "text")
    private String otherNutritionalNotes;

    @OneToMany(mappedBy = "admissionRequest", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @Filter(name = "filterByDeleted")
    private Collection<PatientDiet> patientDiets;

    @Formula("DATEDIFF(coalesce(date_to, date_deceased, now()), date_from)")
    private Integer durationInHospital;

    @Column(name = "hosp_status")
    @Enumerated(EnumType.STRING)
    private HospitalizationStatus hospitalizationStatus;

    @JoinColumn(name = "discharge_reason", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private DischargeReason dischargeReason;

    @Column(name = "create_mort_req", columnDefinition = "tinyint default '0'")
    private Boolean createMortuaryRequest = false;

    @Column(name = "cease_billing", columnDefinition = "tinyint default '0'")
    private Boolean ceaseBilling = true;

    @Transient
    private Boolean newPatient;

    @Transient
    private String identificationNo;

    @Transient
    private String nationalityString;

    @Transient
    private String employer;

    @Transient
    private String residentialAddress;

    @Transient
    private Long inpatientNo;

    public Collection<PrescriptionRequestItem> getPrescriptionRequestItems() {
        return prescriptionRequestItems;
    }

    public void setPrescriptionRequestItems(Collection<PrescriptionRequestItem> prescriptionRequestItems) {
        this.prescriptionRequestItems = prescriptionRequestItems;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public Collection<PrescriptionAdministration> getPrescriptionItemAdministrations() {
        return prescriptionItemAdministrations;
    }

    public void setPrescriptionItemAdministrations(Collection<PrescriptionAdministration> prescriptionItemAdministrations) {
        this.prescriptionItemAdministrations = prescriptionItemAdministrations;
    }

    public String getPreferredConditions() {
        return preferredConditions;
    }

    public void setPreferredConditions(String preferredConditions) {
        this.preferredConditions = preferredConditions;
    }

    public String getDischargeSummary() {
        return dischargeSummary;
    }

    public void setDischargeSummary(String dischargeSummary) {
        this.dischargeSummary = dischargeSummary;
    }

    public List<AdmissionBed> getAdmissionBeds() {
        return admissionBeds;
    }

    public void setAdmissionBeds(List<AdmissionBed> admissionBeds) {
        this.admissionBeds = admissionBeds;
    }

    public void addAdmissionBed(AdmissionBed admissionBed) {
        if (admissionBed == null) return;
        admissionBed.setAdmissionRequest(this);
        getAdmissionBeds().add(admissionBed);
    }

    public Date getNextVisit() {
        return nextVisit;
    }

    public void setNextVisit(Date nextVisit) {
        this.nextVisit = nextVisit;
    }

    public String getDischargeDiet() {
        return dischargeDiet;
    }

    public void setDischargeDiet(String dischargeDiet) {
        this.dischargeDiet = dischargeDiet;
    }

    public String getDischargeActivity() {
        return dischargeActivity;
    }

    public void setDischargeActivity(String dischargeActivity) {
        this.dischargeActivity = dischargeActivity;
    }

    public BigDecimal getLineTotal() {
        return lineTotal;
    }

    public void setLineTotal(BigDecimal lineTotal) {
        this.lineTotal = lineTotal;
    }

    public DischargeType getDischargeType() {
        return dischargeType;
    }

    public void setDischargeType(DischargeType dischargeType) {
        this.dischargeType = dischargeType;
    }

    public Date getDateDeceased() {
        return dateDeceased;
    }

    public void setDateDeceased(Date dateDeceased) {
        this.dateDeceased = dateDeceased;
    }

    public String getCauseOfDeath() {
        return causeOfDeath;
    }

    public void setCauseOfDeath(String causeOfDeath) {
        this.causeOfDeath = causeOfDeath;
    }

    public String getNatureOfDeath() {
        return natureOfDeath;
    }

    public void setNatureOfDeath(String natureOfDeath) {
        this.natureOfDeath = natureOfDeath;
    }

    public Ward getWard() {
        return ward;
    }

    public void setWard(Ward ward) {
        this.ward = ward;
    }

    public Bed getCurrentBed() {
        return currentBed;
    }

    public void setCurrentBed(Bed currentBed) {
        this.currentBed = currentBed;
    }

    public Belief getBelief() {
        return belief;
    }

    public void setBelief(Belief belief) {
        this.belief = belief;
    }

    public String getVegeterian() {
        return vegeterian;
    }

    public void setVegeterian(String vegeterian) {
        this.vegeterian = vegeterian;
    }

    public Collection<PatientDiet> getPatientDiets() {
        return patientDiets;
    }

    public void setPatientDiets(Collection<PatientDiet> patientDiets) {
        this.patientDiets = patientDiets;
    }

    public String getOtherNutritionalNotes() {
        return otherNutritionalNotes;
    }

    public void setOtherNutritionalNotes(String otherNutritionalNotes) {
        this.otherNutritionalNotes = otherNutritionalNotes;
    }

    public Integer getDurationInHospital() {
        return durationInHospital;
    }

    public void setDurationInHospital(Integer durationInHospital) {
        this.durationInHospital = durationInHospital;
    }

    public HospitalizationStatus getHospitalizationStatus() {
        return hospitalizationStatus;
    }

    public void setHospitalizationStatus(HospitalizationStatus hospitalizationStatus) {
        this.hospitalizationStatus = hospitalizationStatus;
    }

    public DischargeReason getDischargeReason() {
        return dischargeReason;
    }

    public void setDischargeReason(DischargeReason dischargeReason) {
        this.dischargeReason = dischargeReason;
    }

    @Override
    public PatientBill billRequest(PatientBill patientBill) {
        Long startTime = System.currentTimeMillis();
        for(PrescriptionRequestItem item : getPrescriptionRequestItems()){
            if(item.getDispensed()){
                //only bill if the item is available
                Product product = item.getMedicament().getProduct();
                BillItem billItem = new BillItem();
                if(item.getBillItem() != null) billItem = item.getBillItem();//just revise the amount billed if it was already billed
                if(billItem.getBillNo() == null) billItem.setBillNo(startTime);
                item.setBillItem(billItem);
                billItem.setProduct(product);
                billItem.setQuantity(item.getDispenseQty());
                billItem.setUnitPrice(product.getListPrice());
                billItem.setLineTotal(product.getListPrice().multiply(billItem.getQuantity()));
                billItem.setTax(BigDecimal.ZERO);
                billItem.setSrType(this.getServiceType());
                billItem.setSrFqn(this.getClass().getName());
                billItem.setSrId(this.getId());
                billItem.setBilledFrom(BilledFrom.SERVICE_REQUEST);
                //billItem.setServiceCenter(this.getServiceCenter());
                billItem.setBillItemType(BillItemType.Normal);
                patientBill.addBillItem(billItem);
                startTime++;
            }
        }
        return patientBill;
    }

    public Boolean isCreateMortuaryRequest() {
        return createMortuaryRequest;
    }

    public void setCreateMortuaryRequest(Boolean createMortuaryRequest) {
        this.createMortuaryRequest = createMortuaryRequest;
    }

    public Boolean isCeaseBilling() {
        return ceaseBilling;
    }

    public void setCeaseBilling(Boolean ceaseBilling) {
        this.ceaseBilling = ceaseBilling;
    }

    public Boolean getNewPatient() {
        return newPatient;
    }

    public void setNewPatient(Boolean newPatient) {
        this.newPatient = newPatient;
    }

    public Boolean getCreateMortuaryRequest() {
        return createMortuaryRequest;
    }

    public Boolean getCeaseBilling() {
        return ceaseBilling;
    }

    public String getIdentificationNo() {
        return identificationNo;
    }

    public void setIdentificationNo(String identificationNo) {
        this.identificationNo = identificationNo;
    }

    public String getNationalityString() {
        return nationalityString;
    }

    public void setNationalityString(String nationalityString) {
        this.nationalityString = nationalityString;
    }

    public String getEmployer() {
        return employer;
    }

    public void setEmployer(String employer) {
        this.employer = employer;
    }

    public String getResidentialAddress() {
        return residentialAddress;
    }

    public void setResidentialAddress(String residentialAddress) {
        this.residentialAddress = residentialAddress;
    }

    public Long getInpatientNo() {
        return inpatientNo;
    }

    public void setInpatientNo(Long inpatientNo) {
        this.inpatientNo = inpatientNo;
    }
}
