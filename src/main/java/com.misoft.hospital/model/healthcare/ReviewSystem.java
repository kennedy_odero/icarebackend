package com.misoft.hospital.model.healthcare;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by kodero on 2/23/16.
 */
@Entity
@Table(name = "review_system")
public class ReviewSystem extends ModelBase{

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @OneToMany(mappedBy = "reviewSystem", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @Filter(name = "filterByDeleted")
    private Collection<SystemProblem> systemProblems;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Collection<SystemProblem> getSystemProblems() {
        return systemProblems;
    }

    public void setSystemProblems(Collection<SystemProblem> systemProblems) {
        this.systemProblems = systemProblems;
    }
}
