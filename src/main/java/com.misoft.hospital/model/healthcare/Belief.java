package com.misoft.hospital.model.healthcare;

import com.misoft.hospital.model.base.ModelBase;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * Created by kodero on 2/17/16.
 */
@Entity
@Table(name = "beliefs")
public class Belief extends ModelBase{

    @NotNull
    @Column(name = "belief")
    private String belief;

    @NotNull
    @Column(name = "code")
    private String code;

    @Column(name = "description")
    private String description;

    public String getBelief() {
        return belief;
    }

    public void setBelief(String belief) {
        this.belief = belief;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
