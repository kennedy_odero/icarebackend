package com.misoft.hospital.model.healthcare;

import com.misoft.hospital.model.base.ModelBase;
import com.misoft.hospital.model.setup.Ward;
import org.hibernate.annotations.Filter;

import javax.persistence.*;

/**
 * Created by kodero on 4/1/15.
 */
@Entity
@Table(name = "ward_allocations")
@Filter(name = "filterByDeleted")
public class WardAllocation extends ModelBase {

    @JoinColumn(name = "ward", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Ward ward;

    @JoinColumn(name = "team", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private WardRoundTeam wardRoundTeam;

    public Ward getWard() {
        return ward;
    }

    public void setWard(Ward ward) {
        this.ward = ward;
    }

    public WardRoundTeam getWardRoundTeam() {
        return wardRoundTeam;
    }

    public void setWardRoundTeam(WardRoundTeam wardRoundTeam) {
        this.wardRoundTeam = wardRoundTeam;
    }
}
