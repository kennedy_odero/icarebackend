package com.misoft.hospital.model.healthcare;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by kodero on 12/13/15.
 */
@Entity
@Table(name = "schedule_runs")
@Filter(name = "filterByDeleted")
public class ScheduleRun extends ModelBase{

    @Column(name = "run_date")
    private Date runDate;

    @JoinColumn(name = "clinic", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private SpecialClinic specialClinic;

    @Column(name = "run_status")
    @Enumerated(EnumType.STRING)
    private RunStatus runStatus;

    public ScheduleRun(){

    }

    public ScheduleRun(SpecialClinic clinic, Date runDate){
        setSpecialClinic(clinic);
        setRunDate(runDate);
    }

    public Date getRunDate() {
        return runDate;
    }

    public void setRunDate(Date runDate) {
        this.runDate = runDate;
    }

    public SpecialClinic getSpecialClinic() {
        return specialClinic;
    }

    public void setSpecialClinic(SpecialClinic specialClinic) {
        this.specialClinic = specialClinic;
    }

    public RunStatus getRunStatus() {
        return runStatus;
    }

    public void setRunStatus(RunStatus runStatus) {
        this.runStatus = runStatus;
    }
}
