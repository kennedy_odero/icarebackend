package com.misoft.hospital.model.healthcare;

import com.misoft.hospital.model.setup.*;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.Formula;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * Created by kodero on 3/20/15.
 */
@Entity
@Table(name = "prescrip_req_items")
@Filter(name = "filterByDeleted")
public class PrescriptionRequestItem extends ServiceRequestItem {

    @JoinColumn(name = "medicament", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Medicament medicament;

    @JoinColumn(name = "drug_admin_route", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private DrugAdminRoute drugAdminRoute;

    @JoinColumn(name = "medicament_freq", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private MedicamentFrequency medicamentFrequency;

    @JoinColumn(name = "drug_dose_unit", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private DrugDoseUnit drugDoseUnit;

    @JoinColumn(name = "prescrip_request", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private PrescriptionRequest prescriptionRequest;

    @JoinColumn(name = "admission_request", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private AdmissionRequest admissionRequest;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private PrescriptionItemStatus status;

    @Column(name = "dispensed")
    private Boolean dispensed;

    @Column(name = "available", columnDefinition = "tinyint default '0'")
    private Boolean available = false;

    @Column(name = "prescr_type")
    @Enumerated(EnumType.STRING)
    private PrescriptionType prescriptionType;

    @JoinColumn(name = "uom", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private UnitOfMeasure uom;

    @NotNull
    @Column(name = "duration")
    private BigDecimal duration;

    @NotNull
    @Column(name = "duration_mode", columnDefinition = "mediumint default '7'")
    private Integer durationMode;

    //single dosage quantity
    @Column(name = "per_dose_qty")
    private BigDecimal perDoseQty;

    @Column(name = "dosage_qty")
    private BigDecimal fullDosageQty;

    @Column(name = "dispense_qty")
    private BigDecimal dispenseQty;

    @Formula("total_amount")
    private BigDecimal totalAmount;

    public PrescriptionRequestItem(){

    }

    public PrescriptionRequestItem(Medicament medicament, Integer quantity, Integer duration){
        setMedicament(medicament);
        setFullDosageQty(new BigDecimal(quantity));
        setDispenseQty(new BigDecimal(quantity));
        setDuration(new BigDecimal(duration));
        setDispensed(true);
    }

    public PrescriptionRequestItem(Medicament medicament, BigDecimal dosage, MedicamentFrequency medicamentFrequency, Integer quantity, Integer duration){
        setMedicament(medicament);
        setFullDosageQty(new BigDecimal(quantity));
        setMedicamentFrequency(medicamentFrequency);
        setPerDoseQty(dosage);
        setDispenseQty(new BigDecimal(quantity));
        setDuration(new BigDecimal(duration));
        setDispensed(true);
    }

    public Medicament getMedicament() {
        return medicament;
    }

    public void setMedicament(Medicament medicament) {
        this.medicament = medicament;
    }

    public DrugAdminRoute getDrugAdminRoute() {
        return drugAdminRoute;
    }

    public void setDrugAdminRoute(DrugAdminRoute drugAdminRoute) {
        this.drugAdminRoute = drugAdminRoute;
    }

    public MedicamentFrequency getMedicamentFrequency() {
        return medicamentFrequency;
    }

    public void setMedicamentFrequency(MedicamentFrequency medicamentFrequency) {
        this.medicamentFrequency = medicamentFrequency;
    }

    public DrugDoseUnit getDrugDoseUnit() {
        return drugDoseUnit;
    }

    public void setDrugDoseUnit(DrugDoseUnit drugDoseUnit) {
        this.drugDoseUnit = drugDoseUnit;
    }

    public PrescriptionRequest getPrescriptionRequest() {
        return prescriptionRequest;
    }

    public void setPrescriptionRequest(PrescriptionRequest prescriptionRequest) {
        this.prescriptionRequest = prescriptionRequest;
    }

    public PrescriptionItemStatus getStatus() {
        return status;
    }

    public void setStatus(PrescriptionItemStatus status) {
        this.status = status;
    }

    public Boolean getDispensed() {
        return dispensed;
    }

    public void setDispensed(Boolean dispensed) {
        this.dispensed = dispensed;
    }

    public AdmissionRequest getAdmissionRequest() {
        return admissionRequest;
    }

    public void setAdmissionRequest(AdmissionRequest admissionRequest) {
        this.admissionRequest = admissionRequest;
    }

    public PrescriptionType getPrescriptionType() {
        return prescriptionType;
    }

    public void setPrescriptionType(PrescriptionType prescriptionType) {
        this.prescriptionType = prescriptionType;
    }

    public UnitOfMeasure getUom() {
        return uom;
    }

    public void setUom(UnitOfMeasure uom) {
        this.uom = uom;
    }

    public BigDecimal getDuration() {
        return duration;
    }

    public void setDuration(BigDecimal duration) {
        this.duration = duration;
    }

    public BigDecimal getDispenseQty() {
        return dispenseQty;
    }

    public void setDispenseQty(BigDecimal dispenseQty) {
        this.dispenseQty = dispenseQty;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public BigDecimal getPerDoseQty() {
        return perDoseQty;
    }

    public void setPerDoseQty(BigDecimal perDoseQty) {
        this.perDoseQty = perDoseQty;
    }

    public BigDecimal getFullDosageQty() {
        return fullDosageQty;
    }

    public void setFullDosageQty(BigDecimal fullDosageQty) {
        this.fullDosageQty = fullDosageQty;
    }

    public Integer getDurationMode() {
        return durationMode;
    }

    public void setDurationMode(Integer durationMode) {
        this.durationMode = durationMode;
    }

    public Boolean isAvailable() {
        return available;
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }
}
