package com.misoft.hospital.model.healthcare;

import com.misoft.hospital.model.billing.BillItem;
import com.misoft.hospital.model.billing.BillItemType;
import com.misoft.hospital.model.billing.BilledFrom;
import com.misoft.hospital.model.billing.PatientBill;
import com.misoft.hospital.model.setup.Product;
import com.misoft.hospital.model.setup.Staff;
import com.misoft.hospital.service.billing.BillableRequest;
import com.misoft.hospital.util.validation.ValidationMethod;

import javax.persistence.*;
import javax.validation.ValidationException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 * Created by kodero on 3/20/15.
 */
@Entity
@DiscriminatorValue("IMAGING")
public class ImagingRequest extends ServiceRequest implements BillableRequest {

    @OneToMany(mappedBy = "imagingRequest", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @org.hibernate.annotations.Filter(name = "filterByDeleted")
    private Collection<ImagingRequestItem> imagingRequestItems = new ArrayList<>();

    public ImagingRequest(){

    }

    public ImagingRequest(Long requestNo, RequestSource requestSource, Patient patient, Date dateRequested, Staff staff, RequestStatus requestStatus, Appointment appointment){
        super(requestNo, requestSource, patient, dateRequested, staff, requestStatus, appointment);
    }

    @Override
    public PatientBill billRequest(PatientBill patientBill){
        Long startTime = System.currentTimeMillis();
        for(ImagingRequestItem item : getImagingRequestItems()){
            Product product = item.getImagingTest().getProduct();
            BillItem billItem = new BillItem();
            if(item.getBillItem() != null) billItem = item.getBillItem();//just revise the amount billed if it was already billed
            if(billItem.getBillNo() == null) billItem.setBillNo(startTime);
            item.setBillItem(billItem);
            billItem.setProduct(product);
            billItem.setQuantity(BigDecimal.ONE);
            billItem.setUnitPrice(product.getListPrice());
            billItem.setLineTotal(product.getListPrice().multiply(billItem.getQuantity()));
            billItem.setTax(BigDecimal.ZERO);
            billItem.setSrType(this.getServiceType());
            billItem.setSrFqn(this.getClass().getName());
            billItem.setSrId(this.getId());
            billItem.setBilledFrom(BilledFrom.SERVICE_REQUEST);
            //billItem.setServiceCenter(this.getServiceCenter());
            billItem.setBillItemType(BillItemType.Normal);
            patientBill.addBillItem(billItem);
            startTime++;
        }
        return patientBill;
    }

    @ValidationMethod
    public void validateEntries(){
        if (getImagingRequestItems().size() == 0) throw new ValidationException("Please add at least 1 test item!");
        for (ImagingRequestItem lri : getImagingRequestItems()) {
            if(lri.getImagingTest() == null) throw new ValidationException("Please make sure all items have a imaging test!");
            if(lri.getImagingTest().getBlocked()) throw new ValidationException("Imaging test {" + lri.getImagingTest().getName() +"} is blocked for requests!");
        }
    }

    public Collection<ImagingRequestItem> getImagingRequestItems() {
        return imagingRequestItems;
    }

    public void setImagingRequestItems(Collection<ImagingRequestItem> imagingRequestItems) {
        this.imagingRequestItems = imagingRequestItems;
    }

    public void addImagingRequestItem(ImagingRequestItem imagingRequestItem) {
        if(imagingRequestItem != null){
            imagingRequestItem.setImagingRequest(this);
            getImagingRequestItems().add(imagingRequestItem);
        }
    }

    public void addImagingRequestItems(Collection<ImagingRequestItem> imagingRequestItems) {
        for(ImagingRequestItem iri : imagingRequestItems){
            if(iri != null){
                iri.setImagingRequest(this);
                getImagingRequestItems().add(iri);
            }
        }
    }
}
