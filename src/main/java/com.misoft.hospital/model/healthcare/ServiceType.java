package com.misoft.hospital.model.healthcare;

/**
 * Created by kodero on 3/10/15.
 */
public enum ServiceType {
    CONSULTATION,
    LABORATORY,
    RADIOLOGY,
    SURGERY,
    PHARMACY,
    ADMISSION,
    PHYSIOTHERAPY,
    TRIAGING,
    STORES,
    MORTUARY,
    DENTAL
}
