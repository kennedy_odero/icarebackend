package com.misoft.hospital.model.healthcare;

import com.misoft.hospital.model.base.ModelBase;
import com.misoft.hospital.model.setup.Product;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created by kodero on 10/27/15.
 */
@Entity
@Table(name = "patient_allergies")
public class PatientAllergy extends ModelBase{

    @NotNull
    @Column(name = "description")
    private String description;

    @JoinColumn(name = "product", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Product product;

    @Column(name = "adverse_reactions")
    private String adverseReactions;

    @Column(name = "reaction_type")
    private String reactionType;

    @Column(name = "date_of_reaction")
    private Date dateOfReaction;

    @Enumerated(EnumType.STRING)
    @Column(name = "severity")
    private Severity severity;

    @JoinColumn(name = "patient", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Patient patient;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public String getAdverseReactions() {
        return adverseReactions;
    }

    public void setAdverseReactions(String adverseReactions) {
        this.adverseReactions = adverseReactions;
    }

    public String getReactionType() {
        return reactionType;
    }

    public void setReactionType(String reactionType) {
        this.reactionType = reactionType;
    }

    public Date getDateOfReaction() {
        return dateOfReaction;
    }

    public void setDateOfReaction(Date dateOfReaction) {
        this.dateOfReaction = dateOfReaction;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Severity getSeverity() {
        return severity;
    }

    public void setSeverity(Severity severity) {
        this.severity = severity;
    }
}
