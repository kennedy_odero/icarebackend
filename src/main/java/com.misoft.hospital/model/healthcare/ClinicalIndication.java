package com.misoft.hospital.model.healthcare;

import com.misoft.hospital.model.base.ModelBase;
import com.misoft.hospital.model.setup.UnitOfMeasure;
import org.hibernate.annotations.Filter;

import javax.persistence.*;

/**
 * Created by kodero on 3/15/15.
 */
@Entity
@Table(name = "clinical_indications")
@Filter(name = "filterByDeleted")
public class ClinicalIndication extends ModelBase {

    @Column(name = "name")
    private String name;

    @Column(name = "code")
    private String code;

    @JoinColumn(name = "unit_of_measure", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private UnitOfMeasure unitOfMeasure;

    @Column(name = "input_pattern", columnDefinition = "varchar(255) default ''")
    private String inputPattern;

    public ClinicalIndication(){

    }

    public ClinicalIndication(String id){
        setId(id);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public UnitOfMeasure getUnitOfMeasure() {
        return unitOfMeasure;
    }

    public void setUnitOfMeasure(UnitOfMeasure unitOfMeasure) {
        this.unitOfMeasure = unitOfMeasure;
    }

    public String getInputPattern() {
        return inputPattern;
    }

    public void setInputPattern(String inputPattern) {
        this.inputPattern = inputPattern;
    }
}
