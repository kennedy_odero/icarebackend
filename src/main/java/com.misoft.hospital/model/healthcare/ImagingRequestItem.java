package com.misoft.hospital.model.healthcare;

import com.misoft.hospital.model.setup.ImagingTest;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by kodero on 3/20/15.
 */
@Entity
@Table(name = "imaging_request_items")
@Filter(name = "filterByDeleted")
public class ImagingRequestItem extends ServiceRequestItem {

    @JoinColumn(name = "imaging_test", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private ImagingTest imagingTest;

    @JoinColumn(name = "imaging_request", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private ImagingRequest imagingRequest;

    @Column(name = "results", columnDefinition = "text")
    private String results;

    @Column(name = "comments", columnDefinition = "text")
    private String comments;

    @Column(name = "line_total")
    private BigDecimal lineTotal;

    public ImagingRequestItem(){

    }

    public ImagingRequestItem(ImagingTest imagingTest){
        setImagingTest(imagingTest);
    }

    public BigDecimal getLineTotal() {
        return lineTotal;
    }

    public void setLineTotal(BigDecimal lineTotal) {
        this.lineTotal = lineTotal;
    }

    public ImagingRequest getImagingRequest() {
        return imagingRequest;
    }

    public void setImagingRequest(ImagingRequest imagingRequest) {
        this.imagingRequest = imagingRequest;
    }

    public String getResults() {
        return results;
    }

    public void setResults(String results) {
        this.results = results;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public ImagingTest getImagingTest() {
        return imagingTest;
    }

    public void setImagingTest(ImagingTest imagingTest) {
        this.imagingTest = imagingTest;
    }
}
