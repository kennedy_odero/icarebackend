package com.misoft.hospital.model.healthcare;

import com.misoft.hospital.model.base.ModelBase;
import com.misoft.hospital.model.setup.DocumentType;
import com.misoft.hospital.model.setup.Location;
import com.misoft.hospital.model.setup.Staff;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by kodero on 8/30/15.
 */
@Entity
@Table(name = "burial_permits")
@Filter(name = "filterByDeleted")
public class BurialPermit extends ModelBase{

    @JoinColumn(name = "patient", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Patient patient;

    //next of kin details
    @Column(name = "nok_surname")
    private String nokSName;

    @Column(name = "nook_other_names")
    private String nokOtherNames;

    @JoinColumn(name = "nok_doc_type", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private DocumentType nokDocumentType;

    @Column(name = "nok_doc_no")
    private String nokIdentificationNo;

    @Column(name = "date_deceased")
    private Date dateDeceased;

    @Column(name = "cause_of_death")
    private String causeOfDeath;

    @JoinColumn(name = "certified_by", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Staff certifiedBy;

    @Column(name = "death_summary")
    private String deathSummary;

    @JoinColumn(name = "location", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Location location;

    @JoinColumn(name = "sub_location", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Location subLocation;

    @JoinColumn(name = "village", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Location village;

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public String getNokSName() {
        return nokSName;
    }

    public void setNokSName(String nokSName) {
        this.nokSName = nokSName;
    }

    public String getNokOtherNames() {
        return nokOtherNames;
    }

    public void setNokOtherNames(String nokOtherNames) {
        this.nokOtherNames = nokOtherNames;
    }

    public DocumentType getNokDocumentType() {
        return nokDocumentType;
    }

    public void setNokDocumentType(DocumentType nokDocumentType) {
        this.nokDocumentType = nokDocumentType;
    }

    public String getNokIdentificationNo() {
        return nokIdentificationNo;
    }

    public void setNokIdentificationNo(String nokIdentificationNo) {
        this.nokIdentificationNo = nokIdentificationNo;
    }

    public Staff getCertifiedBy() {
        return certifiedBy;
    }

    public void setCertifiedBy(Staff certifiedBy) {
        this.certifiedBy = certifiedBy;
    }

    public String getDeathSummary() {
        return deathSummary;
    }

    public void setDeathSummary(String deathSummary) {
        this.deathSummary = deathSummary;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Location getSubLocation() {
        return subLocation;
    }

    public void setSubLocation(Location subLocation) {
        this.subLocation = subLocation;
    }

    public Location getVillage() {
        return village;
    }

    public void setVillage(Location village) {
        this.village = village;
    }

    public Date getDateDeceased() {
        return dateDeceased;
    }

    public void setDateDeceased(Date dateDeceased) {
        this.dateDeceased = dateDeceased;
    }

    public String getCauseOfDeath() {
        return causeOfDeath;
    }

    public void setCauseOfDeath(String causeOfDeath) {
        this.causeOfDeath = causeOfDeath;
    }
}
