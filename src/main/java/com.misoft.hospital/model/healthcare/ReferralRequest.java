package com.misoft.hospital.model.healthcare;

import javax.persistence.*;

/**
 * Created by kodero on 4/18/15.
 */
@Entity
@DiscriminatorValue("REFERRAL")
public class ReferralRequest extends ServiceRequest{

    @JoinColumn(name = "reason", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private ReferralReason reason;

    @Column(name = "refered_to")
    private String referredTo;

    @Column(name = "referral_type")
    @Enumerated(EnumType.STRING)
    private ReferralType referralType;

    public ReferralReason getReason() {
        return reason;
    }

    public void setReason(ReferralReason reason) {
        this.reason = reason;
    }

    public String getReferredTo() {
        return referredTo;
    }

    public void setReferredTo(String referredTo) {
        this.referredTo = referredTo;
    }

    public ReferralType getReferralType() {
        return referralType;
    }

    public void setReferralType(ReferralType referralType) {
        this.referralType = referralType;
    }
}
