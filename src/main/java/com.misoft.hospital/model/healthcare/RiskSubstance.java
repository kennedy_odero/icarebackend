package com.misoft.hospital.model.healthcare;

import com.misoft.hospital.model.base.ModelBase;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by kodero on 10/27/15.
 */
@Entity
@Table(name = "risky_substances")
public class RiskSubstance extends ModelBase{

    @Column(name = "name")
    private String name;

    @Column(name = "risky_compounds")
    private String riskyCompounds;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRiskyCompounds() {
        return riskyCompounds;
    }

    public void setRiskyCompounds(String riskyCompounds) {
        this.riskyCompounds = riskyCompounds;
    }
}
