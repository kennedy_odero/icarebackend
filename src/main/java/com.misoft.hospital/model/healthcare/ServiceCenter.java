package com.misoft.hospital.model.healthcare;

import com.misoft.hospital.model.base.ModelBase;
import com.misoft.hospital.model.setup.Building;
import org.hibernate.annotations.Filter;

import javax.persistence.*;

/**
 * Created by kodero on 3/10/15.
 */
@Entity
@Table(name = "service_room")
@Filter(name = "filterByDeleted")
public class ServiceCenter extends ModelBase{

    @Column(name = "name")
    private String name;

    @Column(name = "code")
    private String code;

    @Column(name = "description")
    private String description;

    @Column(name = "room_type")
    private ServiceType serviceType;

    @Column(name = "can_hold_inventory")
    private Boolean canHoldInventory;

    @ManyToOne
    @JoinColumn(name = "building", referencedColumnName = "id")
    private Building building;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Building getBuilding() {
        return building;
    }

    public void setBuilding(Building building) {
        this.building = building;
    }

    public ServiceType getServiceType() {
        return serviceType;
    }

    public void setServiceType(ServiceType serviceType) {
        this.serviceType = serviceType;
    }

    public Boolean getCanHoldInventory() {
        return canHoldInventory;
    }

    public void setCanHoldInventory(Boolean canHoldInventory) {
        this.canHoldInventory = canHoldInventory;
    }
}
