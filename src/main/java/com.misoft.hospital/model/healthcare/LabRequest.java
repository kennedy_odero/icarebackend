package com.misoft.hospital.model.healthcare;

import com.misoft.hospital.model.billing.BillItem;
import com.misoft.hospital.model.billing.BillItemType;
import com.misoft.hospital.model.billing.BilledFrom;
import com.misoft.hospital.model.billing.PatientBill;
import com.misoft.hospital.model.setup.Product;
import com.misoft.hospital.model.setup.Staff;
import com.misoft.hospital.service.billing.BillableRequest;
import com.misoft.hospital.util.validation.ValidationMethod;

import javax.persistence.*;
import javax.validation.ValidationException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 * Created by kodero on 3/11/15.
 */
@Entity
@DiscriminatorValue("LABORATORY")
public class LabRequest extends ServiceRequest implements BillableRequest{

    @OneToMany(mappedBy = "labRequest", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @org.hibernate.annotations.Filter(name = "filterByDeleted")
    private Collection<LabRequestItem> labRequestItems = new ArrayList<>();

    @Override
    public PatientBill billRequest(PatientBill patientBill){
        Long startTime = System.currentTimeMillis();
        for(LabRequestItem item : getLabRequestItems()){
            Product product = item.getLabTestType().getProduct();
            BillItem billItem = new BillItem();
            if(billItem.getBillNo() == null) billItem.setBillNo(startTime);//only update if there was nothing
            System.out.print("----------------------===: " + item.getBillItem() + "===------------------");
            if(item.getBillItem() != null) billItem = item.getBillItem();//just revise the amount billed if it was already billed
            item.setBillItem(billItem);
            billItem.setProduct(product);
            billItem.setQuantity(BigDecimal.ONE);
            billItem.setUnitPrice(product.getListPrice());
            billItem.setLineTotal(product.getListPrice().multiply(billItem.getQuantity()));
            billItem.setTax(BigDecimal.ZERO);
            billItem.setSrType(this.getServiceType());
            billItem.setSrFqn(this.getClass().getName());
            billItem.setSrId(this.getId());
            billItem.setBilledFrom(BilledFrom.SERVICE_REQUEST);
            //billItem.setServiceCenter(this.getServiceCenter());
            billItem.setBillItemType(BillItemType.Normal);
            patientBill.addBillItem(billItem);
            startTime++;
        }
        setBilled(true);
        return patientBill;
    }

    public LabRequest(){

    }

    public LabRequest(Long requestNo, RequestSource requestSource, Patient patient, Date dateRequested, Staff staff, RequestStatus requestStatus, Appointment appointment){
        super(requestNo, requestSource, patient, dateRequested, staff, requestStatus, appointment);
    }

    @ValidationMethod
    public void validateEntries(){
        if (getLabRequestItems().size() == 0) throw new ValidationException("Please add at least 1 request item!");
        for (LabRequestItem lri : getLabRequestItems()) {
            if(lri.getLabTestType() == null) throw new ValidationException("Please make sure all items have a test type!");
            if(lri.getLabTestType().getBlocked()) throw new ValidationException("Lab test {" + lri.getLabTestType().getName() +"} is blocked for requests!");
        }
    }

    public Collection<LabRequestItem> getLabRequestItems() {
        return labRequestItems;
    }

    public void setLabRequestItems(Collection<LabRequestItem> labRequestItems) {
        this.labRequestItems = labRequestItems;
    }

    public void addLabRequestItem(LabRequestItem labRequestItem) {
        if(labRequestItem != null){
            getLabRequestItems().add(labRequestItem);
            labRequestItem.setLabRequest(this);
        }
    }

    public void addLabRequestItems(Collection<LabRequestItem> labRequestItems) {
        for (LabRequestItem lri : labRequestItems){
            if(lri != null){
                lri.setLabRequest(this);
                getLabRequestItems().add(lri);
            }
        }
    }
}
