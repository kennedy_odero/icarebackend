package com.misoft.hospital.model.healthcare;

import com.misoft.hospital.model.base.ModelBase;

import javax.persistence.*;

/**
 * Created by kodero on 8/18/15.
 */
@Entity
@Table(name = "neonatal_sign_checks")
public class NeonatalSignCheck extends ModelBase{

    @JoinColumn(name = "patient", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Patient patient;

    @JoinColumn(name = "sign", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private NeonatalSign sign;

    @Column(name = "checked")
    private Boolean checked;

    public NeonatalSign getSign() {
        return sign;
    }

    public void setSign(NeonatalSign sign) {
        this.sign = sign;
    }

    public Boolean getChecked() {
        return checked;
    }

    public void setChecked(Boolean checked) {
        this.checked = checked;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }
}
