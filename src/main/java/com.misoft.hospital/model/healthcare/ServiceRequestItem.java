package com.misoft.hospital.model.healthcare;

import com.misoft.hospital.model.base.ModelBase;
import com.misoft.hospital.model.billing.BillItem;

import javax.persistence.*;

/**
 * Created by kodero on 1/25/16.
 */
@MappedSuperclass
public abstract class ServiceRequestItem extends ModelBase{

    @JoinColumn(name = "service_request", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private ServiceRequest serviceRequest;

    @JoinColumn(name = "bill_item", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    private BillItem billItem;

    @Column(name = "bill_no")
    private Long billNo;

    public ServiceRequest getServiceRequest() {
        return serviceRequest;
    }

    public void setServiceRequest(ServiceRequest serviceRequest) {
        this.serviceRequest = serviceRequest;
    }

    public BillItem getBillItem() {
        return billItem;
    }

    public void setBillItem(BillItem billItem) {
        this.billItem = billItem;
    }

    public Long getBillNo() {
        return billNo;
    }

    public void setBillNo(Long billNo) {
        this.billNo = billNo;
    }
}
