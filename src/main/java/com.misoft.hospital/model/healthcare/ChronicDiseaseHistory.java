package com.misoft.hospital.model.healthcare;

import com.misoft.hospital.model.base.ModelBase;
import com.misoft.hospital.model.setup.Disease;
import com.misoft.hospital.model.shared.YesNo;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by kodero on 10/27/15.
 */
@Entity
@Table(name = "chronic_disease_history")
public class ChronicDiseaseHistory extends ModelBase{

    @Column(name = "relation")
    private String relation;

    @JoinColumn(name = "disease", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Disease disease;

    @Column(name = "date_of_diagnosis")
    private Date dateOfDiagnosis;

    @Column(name = "diseased")
    private YesNo diseased;

    @Column(name = "date_diseased")
    private Date dateDiseased;

    @Column(name = "cause_of_death")
    private String causeOfDeath;

    @JoinColumn(name = "patient", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Patient patient;

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    public Disease getDisease() {
        return disease;
    }

    public void setDisease(Disease disease) {
        this.disease = disease;
    }

    public Date getDateOfDiagnosis() {
        return dateOfDiagnosis;
    }

    public void setDateOfDiagnosis(Date dateOfDiagnosis) {
        this.dateOfDiagnosis = dateOfDiagnosis;
    }

    public YesNo getDiseased() {
        return diseased;
    }

    public void setDiseased(YesNo diseased) {
        this.diseased = diseased;
    }

    public Date getDateDiseased() {
        return dateDiseased;
    }

    public void setDateDiseased(Date dateDiseased) {
        this.dateDiseased = dateDiseased;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public String getCauseOfDeath() {
        return causeOfDeath;
    }

    public void setCauseOfDeath(String causeOfDeath) {
        this.causeOfDeath = causeOfDeath;
    }
}
