package com.misoft.hospital.model.healthcare;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by kodero on 9/30/15.
 */
@Entity
@Table(name = "clinic_bookings")
@Filter(name = "filterByDeleted")
public class ClinicBooking extends ModelBase{

    @Column(name = "booking_no")
    private Long bookingNo;

    @Column(name = "date_booked")
    private Date dateBooked;

    @JoinColumn(name = "patient", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Patient patient;

    @JoinColumn(name = "clinic", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private SpecialClinic specialClinic;

    @Column(name = "_start")
    private String opening;

    @Column(name = "_end")
    private String closing;

    public String getClosing() {
        return closing;
    }

    public void setClosing(String closing) {
        this.closing = closing;
    }

    public Date getDateBooked() {
        return dateBooked;
    }

    public void setDateBooked(Date dateBooked) {
        this.dateBooked = dateBooked;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public SpecialClinic getSpecialClinic() {
        return specialClinic;
    }

    public void setSpecialClinic(SpecialClinic specialClinic) {
        this.specialClinic = specialClinic;
    }

    public String getOpening() {
        return opening;
    }

    public void setOpening(String opening) {
        this.opening = opening;
    }

    public Long getBookingNo() {
        return bookingNo;
    }

    public void setBookingNo(Long bookingNo) {
        this.bookingNo = bookingNo;
    }
}
