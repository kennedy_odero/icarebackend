package com.misoft.hospital.model.healthcare;

/**
 * Created by kodero on 4/15/15.
 */
public enum PaymentType {
    CASH,
    INSURANCE
}
