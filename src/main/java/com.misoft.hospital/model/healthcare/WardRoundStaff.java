package com.misoft.hospital.model.healthcare;

import com.misoft.hospital.model.base.ModelBase;
import com.misoft.hospital.model.setup.Staff;
import org.hibernate.annotations.Filter;

import javax.persistence.*;

/**
 * Created by kodero on 4/1/15.
 */
@Entity
@Table(name = "ward_round_staff")
@Filter(name = "filterByDeleted")
public class WardRoundStaff extends ModelBase {

    @JoinColumn(name = "staff", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Staff staff;

    @JoinColumn(name = "team", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private WardRoundTeam wardRoundTeam;

    @JoinColumn(name = "role", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private WardRoundRole wardRoundRole;

    public Staff getStaff() {
        return staff;
    }

    public void setStaff(Staff staff) {
        this.staff = staff;
    }

    public WardRoundTeam getWardRoundTeam() {
        return wardRoundTeam;
    }

    public void setWardRoundTeam(WardRoundTeam wardRoundTeam) {
        this.wardRoundTeam = wardRoundTeam;
    }

    public WardRoundRole getWardRoundRole() {
        return wardRoundRole;
    }

    public void setWardRoundRole(WardRoundRole wardRoundRole) {
        this.wardRoundRole = wardRoundRole;
    }
}
