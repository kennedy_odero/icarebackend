package com.misoft.hospital.model.healthcare;

import com.misoft.hospital.model.base.ModelBase;

import javax.persistence.*;

/**
 * Created by kodero on 2/23/16.
 */
@Entity
@Table(name = "pat_sys_problems")
public class PatientSystemProblem extends ModelBase{

    @JoinColumn(name = "problem", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private SystemProblem systemProblem;

    @JoinColumn(name = "request", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private ConsultationRequest consultationRequest;

    @Column(name = "present")
    private Boolean present;

    @Column(name = "site")
    private String site;//Where exactly is the pain?

    @Column(name = "onset")
    private String onset;//When did it start, was it constant/intermittent, gradual/ sudden?

    @Column(name = "hpc_character")
    private String hpcCharacter;//What is the pain like e.g. sharp, burning, tight?

    @Column(name = "radiation")
    private String radiation;//Does it radiate/move anywhere?

    @Column(name = "associations")
    private String associations;//Is there anything else associated with the pain e.g. sweating, vomiting

    @Column(name = "time_course")
    private String timeCoure;//Does it follow any time pattern, how long did it last?

    @Column(name = "ex_rel_factors")
    private String exacerbatingRelievingFactors;//Does anything make it better or worse?

    @Column(name = "intensity")
    private String intensity;//How severe is the pain, consider using the 1-10 scale?

    @Column(name = "comments")
    private String comments;

    public SystemProblem getSystemProblem() {
        return systemProblem;
    }

    public void setSystemProblem(SystemProblem systemProblem) {
        this.systemProblem = systemProblem;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getOnset() {
        return onset;
    }

    public void setOnset(String onset) {
        this.onset = onset;
    }

    public String getHpcCharacter() {
        return hpcCharacter;
    }

    public void setHpcCharacter(String hpcCharacter) {
        this.hpcCharacter = hpcCharacter;
    }

    public String getRadiation() {
        return radiation;
    }

    public void setRadiation(String radiation) {
        this.radiation = radiation;
    }

    public String getAssociations() {
        return associations;
    }

    public void setAssociations(String associations) {
        this.associations = associations;
    }

    public String getTimeCoure() {
        return timeCoure;
    }

    public void setTimeCoure(String timeCoure) {
        this.timeCoure = timeCoure;
    }

    public String getExacerbatingRelievingFactors() {
        return exacerbatingRelievingFactors;
    }

    public void setExacerbatingRelievingFactors(String exacerbatingRelievingFactors) {
        this.exacerbatingRelievingFactors = exacerbatingRelievingFactors;
    }

    public String getIntensity() {
        return intensity;
    }

    public void setIntensity(String intensity) {
        this.intensity = intensity;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public ConsultationRequest getConsultationRequest() {
        return consultationRequest;
    }

    public void setConsultationRequest(ConsultationRequest consultationRequest) {
        this.consultationRequest = consultationRequest;
    }

    public Boolean getPresent() {
        return present;
    }

    public void setPresent(Boolean present) {
        this.present = present;
    }
}
