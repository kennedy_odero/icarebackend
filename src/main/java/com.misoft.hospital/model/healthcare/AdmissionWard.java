package com.misoft.hospital.model.healthcare;

import com.misoft.hospital.model.base.ModelBase;
import com.misoft.hospital.model.setup.Ward;
import org.hibernate.annotations.Filter;

import javax.persistence.*;

/**
 * Created by kodero on 3/20/15.
 */
@Entity
@Table(name = "admission_beds")
@Filter(name = "filterByDeleted")
public class AdmissionWard extends ModelBase{

    @JoinColumn(name = "ward", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Ward ward;

    @JoinColumn(name = "admission_request", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private AdmissionRequest admissionRequest;

    public Ward getWard() {
        return ward;
    }

    public void setWard(Ward ward) {
        this.ward = ward;
    }

    public AdmissionRequest getAdmissionRequest() {
        return admissionRequest;
    }

    public void setAdmissionRequest(AdmissionRequest admissionRequest) {
        this.admissionRequest = admissionRequest;
    }
}
