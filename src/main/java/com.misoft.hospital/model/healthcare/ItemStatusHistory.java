package com.misoft.hospital.model.healthcare;

import com.misoft.hospital.model.admin.User;
import com.misoft.hospital.model.base.ModelBase;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by kodero on 2/16/16.
 */
@Entity
@Table(name = "item_status_history")
public class ItemStatusHistory extends ModelBase{
    @Enumerated(EnumType.STRING)
    private LabRequestItemStatus status;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date_changed")
    private Date dateChanged;

    @JoinColumn(name = "user", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private User user;

    @JoinColumn(name = "service_request", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private LabRequestItem labRequestItem;

    public ItemStatusHistory(){

    }

    public ItemStatusHistory(LabRequestItemStatus status, Date dateChanged, User user){
        setStatus(status);
        setDateChanged(dateChanged);
        setUser(user);
    }

    public LabRequestItemStatus getStatus() {
        return status;
    }

    public void setStatus(LabRequestItemStatus status) {
        this.status = status;
    }

    public Date getDateChanged() {
        return dateChanged;
    }

    public void setDateChanged(Date dateChanged) {
        this.dateChanged = dateChanged;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public LabRequestItem getLabRequestItem() {
        return labRequestItem;
    }

    public void setLabRequestItem(LabRequestItem labRequestItem) {
        this.labRequestItem = labRequestItem;
    }
}
