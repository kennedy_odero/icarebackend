package com.misoft.hospital.model.healthcare;

/**
 * Created by kodero on 3/12/15.
 */
public enum Status {
    OPEN,
    CLOSED,
    CANCELLED
}
