package com.misoft.hospital.model.healthcare;

import com.misoft.hospital.model.base.ModelBase;

import javax.persistence.*;

/**
 * Created by kodero on 10/28/15.
 */
@Entity
@Table(name = "patient_anc_checks")
public class PatientANCCheck extends ModelBase{

    @JoinColumn(name = "anc_check", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private ANCCheck ancCheck;

    @Column(name = "checked")
    private Boolean checked;

    @Column(name = "notes")
    private String notes;

    @JoinColumn(name = "consultation", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private ConsultationRequest consultationRequest;

    public ANCCheck getAncCheck() {
        return ancCheck;
    }

    public void setAncCheck(ANCCheck ancCheck) {
        this.ancCheck = ancCheck;
    }

    public Boolean getChecked() {
        return checked;
    }

    public void setChecked(Boolean checked) {
        this.checked = checked;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public ConsultationRequest getConsultationRequest() {
        return consultationRequest;
    }

    public void setConsultationRequest(ConsultationRequest consultationRequest) {
        this.consultationRequest = consultationRequest;
    }
}
