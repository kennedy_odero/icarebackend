package com.misoft.hospital.model.healthcare;

import com.misoft.hospital.model.base.ModelBase;

import javax.persistence.*;

/**
 * Created by kodero on 8/18/15.
 */
@Entity
@Table(name = "psc_score")
public class PSCScore extends ModelBase{
    @JoinColumn(name = "patient", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Patient patient;

    @JoinColumn(name = "psc_question", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private PSCQuestion pscQuestion;

    @Column(name = "psc")
    private PSC psc;

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public PSC getPsc() {
        return psc;
    }

    public void setPsc(PSC psc) {
        this.psc = psc;
    }

    public PSCQuestion getPscQuestion() {
        return pscQuestion;
    }

    public void setPscQuestion(PSCQuestion pscQuestion) {
        this.pscQuestion = pscQuestion;
    }
}
