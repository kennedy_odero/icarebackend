package com.misoft.hospital.model.healthcare;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by kodero on 3/11/15.
 */
@Entity
@Table(name = "admissions")
@Filter(name = "filterByDeleted")
public class Admission extends ModelBase {

    @JoinColumn(name = "patient", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Patient patient;

    @Column(name = "date_admitted")
    private Date dateAdmitted;

    @Column(name = "date_discharged")
    private Date dateDischarged;

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Date getDateAdmitted() {
        return dateAdmitted;
    }

    public void setDateAdmitted(Date dateAdmitted) {
        this.dateAdmitted = dateAdmitted;
    }

    public Date getDateDischarged() {
        return dateDischarged;
    }

    public void setDateDischarged(Date dateDischarged) {
        this.dateDischarged = dateDischarged;
    }
}
