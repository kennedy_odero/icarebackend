package com.misoft.hospital.model.healthcare;

/**
 * Created by kodero on 12/10/15.
 */
public enum BillingStatus {
    UNBILLED,
    BILLED,
    PARTIALLY_PAID,
    FULLY_PAID,
    VOIDED,
    WAIVED
}
