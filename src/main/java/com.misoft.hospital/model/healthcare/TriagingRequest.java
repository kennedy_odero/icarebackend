package com.misoft.hospital.model.healthcare;

import com.misoft.hospital.model.setup.Staff;
import com.misoft.hospital.util.validation.ValidatorClass;
import com.misoft.hospital.validators.TriageValidator;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 * Created by kodero on 3/15/15.

 */
@Entity
@DiscriminatorValue("TRIAGING")
@ValidatorClass(TriageValidator.class)
public class TriagingRequest extends ServiceRequest{

    public TriagingRequest(){

    }

    public TriagingRequest(Patient patient, Date dateRequested, Staff requester, Long requestNo, RequestSource requestSource, Appointment appointment, RequestStatus requestStatus, Boolean autoSubmit){
        setPatient(patient);
        setDateRequested(dateRequested);
        setRequester(requester);
        setRequestNo(requestNo);
        setRequestSource(requestSource);
        setAppointment(appointment);
        setRequestStatus(requestStatus);
        setAutoSubmit(autoSubmit);
    }

    public TriagingRequest(String id){
        setId(id);
    }

    @OneToMany(mappedBy = "triagingRequest", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @org.hibernate.annotations.Filter(name = "filterByDeleted")
    private Collection<PatientClinicalIndication> patientClinicalIndications = new ArrayList<>();

    public Collection<PatientClinicalIndication> getPatientClinicalIndications() {
        return patientClinicalIndications;
    }

    public void setPatientClinicalIndications(Collection<PatientClinicalIndication> patientClinicalIndications) {
        this.patientClinicalIndications = patientClinicalIndications;
    }

    public void addPatientClinicalIndication(PatientClinicalIndication pci) {
        if(pci == null) return;
        pci.setTriagingRequest(this);
        getPatientClinicalIndications().add(pci);
    }
}
