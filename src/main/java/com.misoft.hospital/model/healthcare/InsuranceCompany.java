package com.misoft.hospital.model.healthcare;

import com.misoft.hospital.model.setup.Party;
import org.hibernate.annotations.Filter;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Created by kodero on 3/24/15.
 */
@Entity
@DiscriminatorValue("INSURANCE_COMPANY")
@Filter(name = "filterByDeleted")
public class InsuranceCompany extends Party {

    @Column(name = "contact_person")
    private String contactPerson;

    @Column(name = "contact_person_tel")
    private String contactPersonTelephone;

    @Column(name = "billing_address")
    private String billingAddress;

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getContactPersonTelephone() {
        return contactPersonTelephone;
    }

    public void setContactPersonTelephone(String contactPersonTelephone) {
        this.contactPersonTelephone = contactPersonTelephone;
    }

    public String getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(String billingAddress) {
        this.billingAddress = billingAddress;
    }
}
