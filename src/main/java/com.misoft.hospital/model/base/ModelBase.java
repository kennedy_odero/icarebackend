package com.misoft.hospital.model.base;

import com.misoft.hospital.model.admin.User;
import org.codehaus.jackson.annotate.JsonCreator;
import org.hibernate.annotations.Formula;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.Date;


/**
 * @author mokua
 *         Base class for all the entities
 */
@EntityListeners({ModelListener.class})
@MappedSuperclass
@XmlRootElement
@XmlAccessorType(XmlAccessType.PROPERTY)
public abstract class ModelBase {

    @Id
    @GeneratedValue(generator = "lemrUUIDGenerator")
    @Column(name = "id")
    protected String id;

    @Version
    @Column(name = "version")
    private int version = 0;


    public static enum EntityStatus {
        DRAFT("DRAFT"), FINAL("FINAL");

        private String name;


        EntityStatus(String name) {
            this.name = name;

        }

        @Override
        public String toString() {
            return name;
        }

        @JsonCreator
        public static EntityStatus create(String value) {
            if (value == null || value.isEmpty()) {
                return EntityStatus.FINAL;
            }
            for (EntityStatus v : values()) {
                if (value.equals(v.getName())) {
                    return v;
                }
            }
            throw new IllegalArgumentException();
        }

        public String getName() {
            return name;
        }

        public static EntityStatus fromString(String text) {
            return create(text);
        }


    }

    /**
     * DRAFT means only the author can view
     * FINAL means the public can view
     * subclass can change the default to DRAFT
     */
    @Column(name = "entity_status")
    @Enumerated(EnumType.STRING)
    /*@NotNull*/
    protected EntityStatus entityStatus = EntityStatus.FINAL;


    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;

    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;


    /*@XmlTransient
    @JoinColumn(name = "created_by_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private User createdByUser;

    @XmlTransient
    @JoinColumn(name = "updated_by_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private User updatedByUser;*/

    @Column(name = "created_by_id")
    private String createdByUserId;

    @Formula("(select u.email from users u where u.id = created_by_id)")
    private String createdByUser;

    @Column(name = "updated_by_id")
    private String updatedByUserId;

    @Formula("(select u.email from users u where u.id = updated_by_id)")
    private String updatedByUser;

    @Column(name = "deleted", columnDefinition = "tinyint(1) default '0'")
    private boolean deleted = false;

    @Column(name = "deleted_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date deletedAt;


    @Column(name = "deleted_by_id")
    private String deletedById;

    @Column(name = "locked_until")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lockedUntil;

    @XmlTransient
    @JoinColumn(name = "locked_by_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private User lockedById;

    public EntityStatus getEntityStatus() {
        return entityStatus;
    }

    public void setEntityStatus(EntityStatus status) {
        this.entityStatus = status;
    }

    /**
     * //_id
     *
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @return the version
     */
    public int getVersion() {
        return version;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCreatedByUser() {
        return createdByUser;
    }

    public void setCreatedByUser(String createdByUser) {
        this.createdByUser = createdByUser;
    }

    public String getUpdatedByUser() {
        return updatedByUser;
    }

    public void setUpdatedByUser(String updatedByUser) {
        this.updatedByUser = updatedByUser;
    }

    public Date getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Date deletedAt) {
        this.deletedAt = deletedAt;
    }

    public String getDeletedById() {
        return deletedById;
    }

    public void setDeletedById(String deletedById) {
        this.deletedById = deletedById;
    }

    public Date getLockedUntil() {
        return lockedUntil;
    }

    public void setLockedUntil(Date lockedUntil) {
        this.lockedUntil = lockedUntil;
    }

    public boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public User getLockedById() {
        return lockedById;
    }

    public void setLockedById(User lockedById) {
        this.lockedById = lockedById;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getCreatedByUserId() {
        return createdByUserId;
    }

    public void setCreatedByUserId(String createdByUserId) {
        this.createdByUserId = createdByUserId;
    }

    public String getUpdatedByUserId() {
        return updatedByUserId;
    }

    public void setUpdatedByUserId(String updatedByUserId) {
        this.updatedByUserId = updatedByUserId;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ModelBase other = (ModelBase) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return this.getClass().toString() + "{" +
                "id=" + id +
                '}';
    }
}
