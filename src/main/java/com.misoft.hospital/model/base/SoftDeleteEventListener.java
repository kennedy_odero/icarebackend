package com.misoft.hospital.model.base;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.EntityEntry;
import org.hibernate.event.internal.DefaultDeleteEventListener;
import org.hibernate.event.spi.DeleteEvent;
import org.hibernate.persister.entity.EntityPersister;

import java.util.Date;
import java.util.Set;
import java.util.logging.Logger;

public class SoftDeleteEventListener extends DefaultDeleteEventListener {
    private static Logger LOGGER = Logger.getLogger(SoftDeleteEventListener.class.getName());

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Override
    public void onDelete(DeleteEvent event, Set transientEntities) throws HibernateException {
        LOGGER.info("on delete ... ");
        Object o = event.getObject();
        if (o instanceof ModelBase) {
            LOGGER.info("Updating delete status .... ");
            ModelBase entity = (ModelBase) o;
            entity.setDeleted(true);
            entity.setDeletedAt(new Date());
            EntityPersister persister = event.getSession().getEntityPersister(event.getEntityName(), entity);
            EntityEntry entityEntry = event.getSession().getPersistenceContext().getEntry(entity);
            cascadeBeforeDelete(event.getSession(), persister, entity, entityEntry, transientEntities);
            cascadeAfterDelete(event.getSession(), persister, entity, transientEntities);
        } else {
            super.onDelete(event, transientEntities);
        }
    }
}