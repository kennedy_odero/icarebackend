package com.misoft.hospital.model.base;

import com.misoft.hospital.model.admin.User;
import com.misoft.hospital.service.admin.RequestContextUser;

import javax.enterprise.context.spi.CreationalContext;
import javax.enterprise.inject.spi.Bean;
import javax.enterprise.inject.spi.BeanManager;

import javax.inject.Inject;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.util.Date;


/**
 * @author mokua
 */

public class ModelListener {

    @Inject
    BeanManager beanManager; //Workaround WFLY-2387

    public RequestContextUser getCurrentUserService(){
        Bean<RequestContextUser> bean = (Bean<RequestContextUser>) beanManager.getBeans(RequestContextUser.class).iterator().next();
        RequestContextUser requestContextUser = beanManager.getContext(bean.getScope()).get(bean, beanManager.createCreationalContext(bean));
        return requestContextUser;
    }

    @PrePersist
    public void createEntity(Object entity) {
        User user = null;
        try{
            user = getCurrentUserService().getCurrentUser();
        }catch (Exception e){
            //injection did not work
            System.out.println("Injection did not work!!");
        }
        System.out.println("================current user in create is======================" + user);
        //set the create attributes
        ModelBase modelBase = (ModelBase) entity;
        modelBase.setCreatedAt(new Date());
        modelBase.setCreatedByUserId(user == null ? null : user.getId());
    }

    @PreUpdate
    public void updateEntity(Object entity) {
        //set the create attributes
        User user = null;
        try{
            user = getCurrentUserService().getCurrentUser();
        }catch (Exception e){
            //injection did not work
            System.out.println("Injection did not work!!");
        }
        System.out.println("================current user in update is======================" + user);
        ModelBase modelBase = (ModelBase) entity;
        modelBase.setUpdatedAt(new Date());
        modelBase.setUpdatedByUserId(user == null ? null : user.getId());
        if(modelBase.getCreatedAt() == null){
            //work around hibernate bug of not firing create event
            modelBase.setCreatedAt(new Date());
            modelBase.setCreatedByUserId(user == null ? null : user.getId());
        }
    }
}