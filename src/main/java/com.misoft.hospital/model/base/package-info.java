/*@GenericGenerator(
    name = "imSequenceGenerator",
    strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
    parameters = {
        @Parameter(name = "sequence_name", value = "im_sequence"),
        @Parameter(name = "initial_value", value = "1000"),
        @Parameter(name = "increment_size", value = "1")
    }
)



import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

*/
package com.misoft.hospital.model.base;