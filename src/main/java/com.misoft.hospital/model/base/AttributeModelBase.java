package com.misoft.hospital.model.base;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 4/7/14
 * Time: 9:41 AM
 * To change this template use File | Settings | File Templates.
 */

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class AttributeModelBase extends ModelBase {
    public AttributeModelBase() {
    }

    @OneToMany(mappedBy = "attributeModelBaseBase", fetch = FetchType.LAZY)
    @org.hibernate.annotations.Filter(name = "filterByDeleted")
    private Collection<ModelBaseGroup> attributes = new ArrayList<>();

    public Collection<ModelBaseGroup> getAttributes() {
        return attributes;
    }
}
