package com.misoft.hospital.model.base;

import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.event.internal.DefaultMergeEventListener;
import org.hibernate.event.spi.MergeEvent;
import org.hibernate.internal.util.collections.IdentitySet;
import org.hibernate.persister.entity.EntityPersister;

import java.io.Serializable;
import java.util.Map;

public class ModifiedIdTransferringMergeEventListener extends DefaultMergeEventListener {
private static final long serialVersionUID = 1L;
static ThreadLocal<IdentitySet> transientObjects = new ThreadLocal<>();
/**
Hibernate 3.1 implementation of ID transferral.
*/
    protected void entityIsTransient(MergeEvent event, Map copyCache) {
        boolean topLevelInvocation = transientObjects.get() == null;
        if (topLevelInvocation) { transientObjects.set(new IdentitySet()); }
        if (transientObjects.get().contains(event.getEntity())) return;

        transientObjects.get().add(event.getEntity());
        super.entityIsTransient(event, copyCache);
        //Start IdUpdating
        SessionImplementor session = event.getSession();
        EntityPersister persister = session.getEntityPersister(event.getEntityName(), event.getEntity());
        // Extract id from merged copy (which is currently registered with Session).
        Serializable id = persister.getIdentifier(event.getResult(), session);
        // Set id on original object (which remains detached).
        persister.setIdentifier(event.getOriginal(), id, session);
        //End IdUpdating
        { transientObjects.get().clear(); transientObjects.set(null); }
    }
}
