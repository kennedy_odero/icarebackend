/**
 * Copyright (c) Oct 13, 2010 mokua
 */
package com.misoft.hospital.model.base;

import com.misoft.hospital.model.util.MultiElementGroup;

import javax.persistence.*;
import java.io.Serializable;

/**
 * An associative entity
 * add model base stuff
 *
 * @author mokua
 */
@Entity
@Table(name = "model_base_group")
@org.hibernate.annotations.Filter(name = "filterByDeleted")
public class ModelBaseGroup implements Serializable {


    /**
     * Embedded composite identifier class that represents the
     * primary key columns of the many-to-many join table.
     */
    @SuppressWarnings({"serial", "JpaObjectClassSignatureInspection"})
    @Embeddable
    public static class Id implements Serializable {

        @Column(name = "model_base_id")
        private String modelBaseId;

        @Column(name = "multi_element_group_id")
        private String multiElementGroupId;

        public Id() {
        }

        public Id(String modelBaseId, String multiElementGroupId) {
            this.modelBaseId = modelBaseId;
            this.multiElementGroupId = multiElementGroupId;
        }

        @Override
        public boolean equals(Object o) {
            if (o instanceof Id) {
                Id that = (Id) o;
                return this.modelBaseId.equals(that.modelBaseId)
                        && this.multiElementGroupId.equals(that.multiElementGroupId);
            } else {
                return false;
            }
        }

        @Override
        public int hashCode() {
            return modelBaseId.hashCode() + multiElementGroupId.hashCode();
        }

        public String getMultiElementGroupId() {
            return multiElementGroupId;
        }

        public String getModelBaseId() {
            return modelBaseId;
        }

        @Override
        public String toString() {
            return "Id{" +
                    "modelBaseId=" + modelBaseId +
                    ", multiElementGroupId=" + multiElementGroupId +
                    '}';
        }
    }

    @EmbeddedId
    private Id id;


    private String value;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "model_base_id", insertable = false, updatable = false)
    private AttributeModelBase attributeModelBaseBase;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "multi_element_group_id", insertable = false, updatable = false)
    private MultiElementGroup multiElementGroup;


    public ModelBaseGroup() {
    }


    public ModelBaseGroup(String value, AttributeModelBase attributeModelBaseBase, MultiElementGroup multiElementGroup) {
        this.value = value;
        this.attributeModelBaseBase = attributeModelBaseBase;
        this.multiElementGroup = multiElementGroup;

        // Set primary key
        this.id = new Id(attributeModelBaseBase.getId(), multiElementGroup.getId());

        // Guarantee referential integrity
        attributeModelBaseBase.getAttributes().add(this);
        multiElementGroup.getAttributes().add(this);
    }


    /**
     * @param id the id to set
     */
    public void setId(Id id) {
        this.id = id;
    }

    public Id getId() {
        return id;
    }


    public AttributeModelBase getAttributeModelBaseBase() {
        return attributeModelBaseBase;
    }

    public void setAttributeModelBaseBase(AttributeModelBase attributeModelBaseBase) {
        this.attributeModelBaseBase = attributeModelBaseBase;
    }

    public MultiElementGroup getMultiElementGroup() {
        return multiElementGroup;
    }

    public void setMultiElementGroup(MultiElementGroup multiElementGroup) {
        this.multiElementGroup = multiElementGroup;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "ModelBaseGroup{" +
                "id=" + id +

                ", value='" + value + '\'' +
                '}';
    }


}
