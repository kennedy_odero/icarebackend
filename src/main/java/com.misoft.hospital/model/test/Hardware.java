package com.misoft.hospital.model.test;

import com.misoft.hospital.model.base.ModelBase;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by kodero on 7/1/15.
 */
@Entity
@Table(name= "hardwares")
public class Hardware extends ModelBase {

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
