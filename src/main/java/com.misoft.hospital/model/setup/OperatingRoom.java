package com.misoft.hospital.model.setup;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.*;

/**
 * Created by kodero on 3/1/15.
 */
@Entity
@Table(name = "operating_rooms")
@Filter(name = "filterByDeleted")
public class OperatingRoom extends ModelBase {

    @Column(name = "name")
    private String name;

    @Column(name = "code")
    private String code;

    @JoinColumn(name = "unit", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Unit unit;

    @JoinColumn(name = "building", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Building building;

    @Column(name = "info", columnDefinition = "text")
    private String info;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    public Building getBuilding() {
        return building;
    }

    public void setBuilding(Building building) {
        this.building = building;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
