package com.misoft.hospital.model.setup;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by kodero on 2/24/15.
 */
@Entity
@Table(name = "disease_genes")
@Filter(name = "filterByDeleted")
public class DiseaseGene extends ModelBase {

    @Column(name = "name")
    private String name;

    @Column(name = "info")
    private String info;

    @Column(name = "gene_id")
    private String geneId;

    @Column(name = "long_name")
    private String longName;

    @Column(name = "location")
    private String location;

    @Column(name = "dominance")
    private String dominance;

    @Column(name = "chromosome")
    private String chromosome;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getGeneId() {
        return geneId;
    }

    public void setGeneId(String geneId) {
        this.geneId = geneId;
    }

    public String getLongName() {
        return longName;
    }

    public void setLongName(String longName) {
        this.longName = longName;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDominance() {
        return dominance;
    }

    public void setDominance(String dominance) {
        this.dominance = dominance;
    }

    public String getChromosome() {
        return chromosome;
    }

    public void setChromosome(String chromosome) {
        this.chromosome = chromosome;
    }
}
