package com.misoft.hospital.model.setup;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by kodero on 3/1/15.
 */
@Entity
@Table(name = "identification_types")
@Filter(name = "filterByDeleted")
public class IdentificationType extends ModelBase {

    @Column(name = "name")
    private String name;

    @Column(name = "validation_regex")
    private String validationRegex;

    //private Country country;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValidationRegex() {
        return validationRegex;
    }

    public void setValidationRegex(String validationRegex) {
        this.validationRegex = validationRegex;
    }
}
