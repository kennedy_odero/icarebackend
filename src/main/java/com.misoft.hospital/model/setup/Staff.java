package com.misoft.hospital.model.setup;

import com.misoft.hospital.model.healthcare.ServiceRequest;
import org.hibernate.annotations.Formula;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 * Created by kodero on 3/1/15.
 */
@Entity
@DiscriminatorValue("STAFF")
public class Staff extends Person{

    @Column(name = "staff_no")
    private String staffNo;

    @Column(name = "health_prof")
    private boolean healthProfessional;

    @Column(name = "licence_no")
    private String licenceNo;

    @Column(name = "date_of_empl")
    private Date dateOfEmployment;

    @Column(name = "staff_status")
    @Enumerated(EnumType.STRING)
    private StaffStatus staffStatus;

    @JoinColumn(name = "medical_specialty", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private  MedicalSpecialty medicalSpecialty;

    @JoinColumn(name = "unit", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private  Unit unit;

    @Formula("unit")
    private String unitId;

    @OneToMany(mappedBy = "requester", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @org.hibernate.annotations.Filter(name = "filterByDeleted")
    private Collection<ServiceRequest> serviceRequests = new ArrayList<>();

    public Staff(){

    }

    public Staff(String id, String surname, String otherNames){
        setId(id);
        setSurname(surname);
        setOtherNames(otherNames);
    }

    public String getStaffNo() {
        return staffNo;
    }

    public void setStaffNo(String staffNo) {
        this.staffNo = staffNo;
    }

    public boolean isHealthProfessional() {
        return healthProfessional;
    }

    public void setHealthProfessional(boolean healthProfessional) {
        this.healthProfessional = healthProfessional;
    }

    public String getLicenceNo() {
        return licenceNo;
    }

    public void setLicenceNo(String licenceNo) {
        this.licenceNo = licenceNo;
    }

    public Date getDateOfEmployment() {
        return dateOfEmployment;
    }

    public void setDateOfEmployment(Date dateOfEmployment) {
        this.dateOfEmployment = dateOfEmployment;
    }

    public StaffStatus getStaffStatus() {
        return staffStatus;
    }

    public void setStaffStatus(StaffStatus staffStatus) {
        this.staffStatus = staffStatus;
    }

    public Collection<ServiceRequest> getServiceRequests() {
        return serviceRequests;
    }

    public void setServiceRequests(Collection<ServiceRequest> serviceRequests) {
        this.serviceRequests = serviceRequests;
    }

    public MedicalSpecialty getMedicalSpecialty() {
        return medicalSpecialty;
    }

    public void setMedicalSpecialty(MedicalSpecialty medicalSpecialty) {
        this.medicalSpecialty = medicalSpecialty;
    }

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }
}
