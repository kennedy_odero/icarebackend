package com.misoft.hospital.model.setup;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.*;

/**
 * Created by kodero on 2/23/15.
 */
@Entity
@Table(name = "diseases")
@Filter(name = "filterByDeleted")
public class Disease extends ModelBase {

    @Column(name = "name")
    private String name;

    @Column(name = "code")
    private String code;

    @Column(name = "info")
    private String info;

    @Column(name = "protein")
    private String protein;

    @Column(name = "gene")
    private String gene;

    @Column(name = "chromosome")
    private String chromosome;

    @JoinColumn(name = "disease_category", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private DiseaseCategory diseaseCategory;

    @JoinColumn(name = "pathology_group", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private PathologyGroup pathologyGroup;

    @JoinColumn(name = "disease_gene", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private DiseaseGene diseaseGene;

    @Column(name = "icd_version")
    private String icdVersion;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getProtein() {
        return protein;
    }

    public void setProtein(String protein) {
        this.protein = protein;
    }

    public String getGene() {
        return gene;
    }

    public void setGene(String gene) {
        this.gene = gene;
    }

    public String getChromosome() {
        return chromosome;
    }

    public void setChromosome(String chromosome) {
        this.chromosome = chromosome;
    }

    public DiseaseCategory getDiseaseCategory() {
        return diseaseCategory;
    }

    public void setDiseaseCategory(DiseaseCategory diseaseCategory) {
        this.diseaseCategory = diseaseCategory;
    }

    public PathologyGroup getPathologyGroup() {
        return pathologyGroup;
    }

    public void setPathologyGroup(PathologyGroup pathologyGroup) {
        this.pathologyGroup = pathologyGroup;
    }

    public DiseaseGene getDiseaseGene() {
        return diseaseGene;
    }

    public void setDiseaseGene(DiseaseGene diseaseGene) {
        this.diseaseGene = diseaseGene;
    }

    public String getIcdVersion() {
        return icdVersion;
    }

    public void setIcdVersion(String icdVersion) {
        this.icdVersion = icdVersion;
    }
}
