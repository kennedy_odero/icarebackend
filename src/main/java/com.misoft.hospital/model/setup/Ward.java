package com.misoft.hospital.model.setup;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Collection;

/**
 * Created by kodero on 3/1/15.
 */
@Entity
@Table(name = "wards")
@Filter(name = "filterByDeleted")
public class Ward extends ModelBase {
    private String name;

    @Column(name = "info", columnDefinition = "text")
    private String info;

    @Column(name = "gender")
    private Gender gender;

    @JoinColumn(name = "institution", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Building building;

    @Column(name = "ward_status")
    @Enumerated(EnumType.STRING)
    private WardStatus wardStatus;

    @Column(name = "_private")
    private boolean _private;

    @Column(name = "bio_hazard")
    private boolean bioHazard;

    @JoinColumn(name = "product", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Product product;

    @OneToMany(mappedBy = "ward", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @Filter(name = "filterByDeleted")
    private Collection<WardAccessoryList> wardAccessories;

    @OneToMany(mappedBy = "ward", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @Filter(name = "filterByDeleted")
    private Collection<Bed> beds;

    @NotNull
    @Column(name = "seq_name")
    private String seqName;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Building getBuilding() {
        return building;
    }

    public void setBuilding(Building building) {
        this.building = building;
    }

    public WardStatus getWardStatus() {
        return wardStatus;
    }

    public void setWardStatus(WardStatus wardStatus) {
        this.wardStatus = wardStatus;
    }

    public boolean is_private() {
        return _private;
    }

    public void set_private(boolean _private) {
        this._private = _private;
    }

    public boolean isBioHazard() {
        return bioHazard;
    }

    public void setBioHazard(boolean bioHazard) {
        this.bioHazard = bioHazard;
    }

    public Collection<WardAccessoryList> getWardAccessories() {
        return wardAccessories;
    }

    public void setWardAccessories(Collection<WardAccessoryList> wardAccessories) {
        this.wardAccessories = wardAccessories;
    }

    public Collection<Bed> getBeds() {
        return beds;
    }

    public void setBeds(Collection<Bed> beds) {
        this.beds = beds;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public String getSeqName() {
        return seqName;
    }

    public void setSeqName(String seqName) {
        this.seqName = seqName;
    }
}
