package com.misoft.hospital.model.setup;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by kodero on 3/1/15.
 */
@Entity
@Table(name = "ward_accessories")
@Filter(name = "filterByDeleted")
public class WardAccessory extends ModelBase {

    @Column(name = "name")
    private String name;

    @Column(name = "code")
    private String code;

    @OneToMany(mappedBy = "wardAccessory", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @Filter(name = "filterByDeleted")
    private Collection<WardAccessoryList> wardAccessoryLists = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Collection<WardAccessoryList> getWardAccessoryLists() {
        return wardAccessoryLists;
    }

    public void setWardAccessoryLists(Collection<WardAccessoryList> wardAccessoryLists) {
        this.wardAccessoryLists = wardAccessoryLists;
    }
}
