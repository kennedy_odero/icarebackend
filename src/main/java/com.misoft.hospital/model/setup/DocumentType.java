package com.misoft.hospital.model.setup;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.*;

/**
 * Created by kodero on 3/10/15.
 */
@Entity
@Table(name = "document_types")
@Filter(name = "filterByDeleted")
public class DocumentType extends ModelBase {

    @Column(name = "name")
    private String name;

    @ManyToOne
    @JoinColumn(name = "country", referencedColumnName = "id")
    private Country country;

    @Column(name = "id_regex")
    private String idRegex;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public String getIdRegex() {
        return idRegex;
    }

    public void setIdRegex(String idRegex) {
        this.idRegex = idRegex;
    }
}
