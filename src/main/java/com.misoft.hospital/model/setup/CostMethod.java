package com.misoft.hospital.model.setup;

/**
 * Created by kodero on 3/3/15.
 */
public enum CostMethod {
    FIXED,
    AVERAGE
}
