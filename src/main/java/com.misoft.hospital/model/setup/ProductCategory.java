package com.misoft.hospital.model.setup;

import com.misoft.hospital.model.accounts.Account;
import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by kodero on 3/3/15.
 */
@Entity
@Table(name = "product_categories")
@Filter(name = "filterByDeleted")
public class ProductCategory extends ModelBase {

    @Column(name = "name")
    private String name;

    @Column(name = "use_par_acct")
    private Boolean useParentsAccounts;

    @Column(name = "use_par_tax")
    private Boolean useParentsTax;

    @JoinColumn(name = "acct_rev", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Account accountRevenue;

    @JoinColumn(name = "acct_exp", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Account accountExpense;

    @OneToMany(mappedBy = "productCategory", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @Filter(name = "filterByDeleted")
    private Collection<Product> products = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getUseParentsAccounts() {
        return useParentsAccounts;
    }

    public void setUseParentsAccounts(Boolean useParentsAccounts) {
        this.useParentsAccounts = useParentsAccounts;
    }

    public Account getAccountRevenue() {
        return accountRevenue;
    }

    public void setAccountRevenue(Account accountRevenue) {
        this.accountRevenue = accountRevenue;
    }

    public Account getAccountExpense() {
        return accountExpense;
    }

    public void setAccountExpense(Account accountExpense) {
        this.accountExpense = accountExpense;
    }

    public Collection<Product> getProducts() {
        return products;
    }

    public void setProducts(Collection<Product> products) {
        this.products = products;
    }

    public Boolean getUseParentsTax() {
        return useParentsTax;
    }

    public void setUseParentsTax(Boolean useParentsTax) {
        this.useParentsTax = useParentsTax;
    }
}
