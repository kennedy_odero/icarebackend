package com.misoft.hospital.model.setup;

/**
 * Created by kodero on 3/4/15.
 */
public enum PregnancyCategory {
    A,
    B,
    C,
    D,
    N,
    X,
    UNKNOWN
}
