package com.misoft.hospital.model.setup;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * Created by kodero on 3/4/15.
 */
@Entity
@Table(name = "medicament_frequencies")
@Filter(name = "filterByDeleted")
public class MedicamentFrequency extends ModelBase{

    @Column(name ="name")
    private String name;

    @Column(name ="code")
    private String code;

    @Column(name ="abbr")
    private String abbreviation;

    @NotNull
    @Column(name = "dosage_formula")
    private String dosageFormula;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public String getDosageFormula() {
        return dosageFormula;
    }

    public void setDosageFormula(String dosageFormula) {
        this.dosageFormula = dosageFormula;
    }
}
