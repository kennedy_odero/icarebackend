package com.misoft.hospital.model.setup;

import com.misoft.hospital.model.accounts.Account;
import com.misoft.hospital.model.admin.Department;
import com.misoft.hospital.model.base.ModelBase;
import com.misoft.hospital.model.cssd.Period;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by kodero on 2/28/15.
 */
@Entity
@Table(name = "products")
@Filter(name = "filterByDeleted")
public class Product extends ModelBase {

    @Column(name = "product_code")
    private Long productCode;

    @Column(name = "reorder_level")
    private BigDecimal reorderLevel;

    @JoinColumn(name = "period", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Period period;

    @Column(name = "period_scalar")
    private BigDecimal periodScalar;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "ACTIVE")
    private Boolean active;

    @Column(name = "product_type")
    @Enumerated(EnumType.STRING)
    private ProductType productType;

    @JoinColumn(name = "department", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Department department;

    @JoinColumn(name = "unit", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Unit unit;

    @JoinColumn(name = "product_category", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private ProductCategory productCategory;

    @Column(name = "list_price")
    private BigDecimal listPrice;

    @Column(name = "minimum_sale_qty")
    private BigDecimal minimumSaleQty;

    @Column(name = "deposit")
    private BigDecimal deposit;

    @Column(name = "cost_price")
    private BigDecimal costPrice;

    @Column(name = "purchasable")
    private Boolean purchasable;

    @Column(name = "consumable")
    private Boolean consumable;

    @JoinColumn(name = "uom", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private UnitOfMeasure unitOfMeasure;

    @Column(name = "cost_method")
    @Enumerated(EnumType.STRING)
    private CostMethod costMethod;

    //Accounts settings
    @Column(name = "use_cat_accounts")
    private Boolean useCatAccounts;

    @JoinColumn(name = "acct_rev", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Account revenueAccount;

    @JoinColumn(name = "acct_exp", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Account expenseAccount;

    @OneToMany(mappedBy = "product", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @Filter(name = "filterByDeleted")
    private Collection<LabTestType> labTestTypes = new ArrayList<>();


    @OneToMany(mappedBy = "product", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @Filter(name = "filterByDeleted")
    private Collection<ProductPackaging> productPackagings = new ArrayList<>();

    @OneToMany(mappedBy = "product", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @Filter(name = "filterByDeleted")
    private Collection<Bed> beds = new ArrayList<>();

    @OneToMany(mappedBy = "product", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @Filter(name = "filterByDeleted")
    private Collection<Medicament> medicaments = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Collection<LabTestType> getLabTestTypes() {
        return labTestTypes;
    }

    public void setLabTestTypes(Collection<LabTestType> labTestTypes) {
        this.labTestTypes = labTestTypes;
    }

    public Collection<Bed> getBeds() {
        return beds;
    }

    public void setBeds(Collection<Bed> beds) {
        this.beds = beds;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public ProductType getProductType() {
        return productType;
    }

    public void setProductType(ProductType productType) {
        this.productType = productType;
    }

    public ProductCategory getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(ProductCategory productCategory) {
        this.productCategory = productCategory;
    }

    public BigDecimal getListPrice() {
        return listPrice;
    }

    public void setListPrice(BigDecimal listPrice) {
        this.listPrice = listPrice;
    }

    public BigDecimal getCostPrice() {
        return costPrice;
    }

    public void setCostPrice(BigDecimal costPrice) {
        this.costPrice = costPrice;
    }

    public Boolean getPurchasable() {
        return purchasable;
    }

    public void setPurchasable(Boolean purchasable) {
        this.purchasable = purchasable;
    }

    public Boolean getConsumable() {
        return consumable;
    }

    public void setConsumable(Boolean consumable) {
        this.consumable = consumable;
    }

    public UnitOfMeasure getUnitOfMeasure() {
        return unitOfMeasure;
    }

    public void setUnitOfMeasure(UnitOfMeasure unitOfMeasure) {
        this.unitOfMeasure = unitOfMeasure;
    }

    public CostMethod getCostMethod() {
        return costMethod;
    }

    public void setCostMethod(CostMethod costMethod) {
        this.costMethod = costMethod;
    }

    public Boolean getUseCatAccounts() {
        return useCatAccounts;
    }

    public void setUseCatAccounts(Boolean useCatAccounts) {
        this.useCatAccounts = useCatAccounts;
    }

    public Account getRevenueAccount() {
        return revenueAccount;
    }

    public void setRevenueAccount(Account revenueAccount) {
        this.revenueAccount = revenueAccount;
    }

    public Account getExpenseAccount() {
        return expenseAccount;
    }

    public void setExpenseAccount(Account expenseAccount) {
        this.expenseAccount = expenseAccount;
    }

    public Collection<Medicament> getMedicaments() {
        return medicaments;
    }

    public void setMedicaments(Collection<Medicament> medicaments) {
        this.medicaments = medicaments;
    }

    public Long getProductCode() {
        return productCode;
    }

    public void setProductCode(Long productCode) {
        this.productCode = productCode;
    }

    public BigDecimal getReorderLevel() {
        return reorderLevel;
    }

    public void setReorderLevel(BigDecimal reorderLevel) {
        this.reorderLevel = reorderLevel;
    }

    public Period getPeriod() {
        return period;
    }

    public void setPeriod(Period period) {
        this.period = period;
    }

    public BigDecimal getPeriodScalar() {
        return periodScalar;
    }

    public void setPeriodScalar(BigDecimal periodScalar) {
        this.periodScalar = periodScalar;
    }

    public BigDecimal getDeposit() {
        return deposit;
    }

    public void setDeposit(BigDecimal deposit) {
        this.deposit = deposit;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    public BigDecimal getMinimumSaleQty() {
        return minimumSaleQty;
    }

    public void setMinimumSaleQty(BigDecimal minimumSaleQty) {
        this.minimumSaleQty = minimumSaleQty;
    }

    public Collection<ProductPackaging> getProductPackagings() {
        return productPackagings;
    }

    public void setProductPackagings(Collection<ProductPackaging> productPackagings) {
        this.productPackagings = productPackagings;
    }
}
