package com.misoft.hospital.model.setup;

import com.misoft.hospital.model.base.ModelBase;
import com.misoft.hospital.model.stores.Packaging;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by kodero on 4/10/15.
 */
@Entity
@Table(name = "product_packaging")
@Filter(name = "filterByDeleted")
public class ProductPackaging extends ModelBase {

    @JoinColumn(name = "packaging", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Packaging packaging;

    @Column(name = "unit_qty")
    private Integer quantityInPackaging;

    @JoinColumn(name = "product", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Product product;

    public Integer getQuantityInPackaging() {
        return quantityInPackaging;
    }

    public void setQuantityInPackaging(Integer quantityInPackaging) {
        this.quantityInPackaging = quantityInPackaging;
    }

    public Packaging getPackaging() {
        return packaging;
    }

    public void setPackaging(Packaging packaging) {
        this.packaging = packaging;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
}
