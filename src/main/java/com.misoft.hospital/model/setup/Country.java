package com.misoft.hospital.model.setup;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by kodero on 3/10/15.
 */
@Entity
@Table(name = "countries")
@Filter(name = "filterByDeleted")
public class Country extends ModelBase{
    @Column(name = "name")
    private String name;

    @Column(name = "two_letter_iso")
    private String twoLetterIso;

    @Column(name = "three_letter_iso")
    private String threeLetterIso;

    @Column(name = "nationality")
    private String nationality;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTwoLetterIso() {
        return twoLetterIso;
    }

    public void setTwoLetterIso(String twoLetterIso) {
        this.twoLetterIso = twoLetterIso;
    }

    public String getThreeLetterIso() {
        return threeLetterIso;
    }

    public void setThreeLetterIso(String threeLetterIso) {
        this.threeLetterIso = threeLetterIso;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }
}
