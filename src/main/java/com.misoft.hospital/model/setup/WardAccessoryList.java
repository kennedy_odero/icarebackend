package com.misoft.hospital.model.setup;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.*;

/**
 * Created by kodero on 3/1/15.
 */
@Entity
@Table(name = "ward_accessory_list")
@Filter(name = "filterByDeleted")
public class WardAccessoryList extends ModelBase {

    @JoinColumn(name = "ward_accessory", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private WardAccessory wardAccessory;

    @JoinColumn(name = "ward", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Ward ward;

    public WardAccessory getWardAccessory() {
        return wardAccessory;
    }

    public void setWardAccessory(WardAccessory wardAccessory) {
        this.wardAccessory = wardAccessory;
    }

    public Ward getWard() {
        return ward;
    }

    public void setWard(Ward ward) {
        this.ward = ward;
    }
}
