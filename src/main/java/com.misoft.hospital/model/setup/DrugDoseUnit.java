package com.misoft.hospital.model.setup;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by kodero on 3/2/15.
 */
@Entity
@Table(name = "drug_dose_units")
@Filter(name = "filterByDeleted")
public class DrugDoseUnit extends ModelBase {

    @Column(name = "unit")
    private String unit;

    @Column(name = "description")
    private String description;

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
