package com.misoft.hospital.model.setup;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by kodero on 3/1/15.
 */
@Entity
@Table(name = "buildings")
@Filter(name = "filterByDeleted")
public class Building extends ModelBase {

    @Column(name = "name")
    private String name;

    @Column(name = "code")
    private String code;

    @JoinColumn(name = "institution", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Institution institution;

    @OneToMany(mappedBy = "unit", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @Filter(name = "filterByDeleted")
    private Collection<OperatingRoom> operatingRooms;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Institution getInstitution() {
        return institution;
    }

    public void setInstitution(Institution institution) {
        this.institution = institution;
    }

    public Collection<OperatingRoom> getOperatingRooms() {
        return operatingRooms;
    }

    public void setOperatingRooms(Collection<OperatingRoom> operatingRooms) {
        this.operatingRooms = operatingRooms;
    }
}
