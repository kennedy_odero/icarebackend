package com.misoft.hospital.model.setup;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by kodero on 3/1/15.
 */
@MappedSuperclass
public abstract class Person extends Party {

    @Column(name = "surname")
    private String surname;

    @Column(name = "other_names")
    private String otherNames;

    @Column(name = "dob")
    private Date dob;

    @Column(name = "gender")
    @Enumerated(EnumType.STRING)
    private Gender gender;

    @JoinColumn(name = "document_type", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private DocumentType documentType;

    @Column(name = "identification_no")
    private String identificationNo;

    //next of kin details
    @Column(name = "nok_surname")
    private String nokSName;

    @Column(name = "nook_other_names")
    private String nokOtherNames;

    @JoinColumn(name = "nok_doc_type", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private DocumentType nokDocumentType;

    @Column(name = "nok_doc_no")
    private String nokIdentificationNo;

    @JoinColumn(name = "nationality", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Country nationality;

    @Column(name = "nationality_str")
    private String nationalityString;

    @Column(name = "res_address")
    private String residentialAddress;

    @Column(name = "employer")
    private String employer;

    /*@JoinColumn(name = "occupation", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Occupation occupation;*/

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getIdentificationNo() {
        return identificationNo;
    }

    public void setIdentificationNo(String identificationNo) {
        this.identificationNo = identificationNo;
    }

    public DocumentType getDocumentType() {
        return documentType;
    }

    public void setDocumentType(DocumentType documentType) {
        this.documentType = documentType;
    }

    public String getNokSName() {
        return nokSName;
    }

    public void setNokSName(String nokSName) {
        this.nokSName = nokSName;
    }

    public String getNokOtherNames() {
        return nokOtherNames;
    }

    public void setNokOtherNames(String nokOtherNames) {
        this.nokOtherNames = nokOtherNames;
    }

    public DocumentType getNokDocumentType() {
        return nokDocumentType;
    }

    public void setNokDocumentType(DocumentType nokDocumentType) {
        this.nokDocumentType = nokDocumentType;
    }

    public String getNokIdentificationNo() {
        return nokIdentificationNo;
    }

    public void setNokIdentificationNo(String nokIdentificationNo) {
        this.nokIdentificationNo = nokIdentificationNo;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getOtherNames() {
        return otherNames;
    }

    public void setOtherNames(String otherNames) {
        this.otherNames = otherNames;
    }

    public Country getNationality() {
        return nationality;
    }

    public void setNationality(Country nationality) {
        this.nationality = nationality;
    }

    public String getNationalityString() {
        return nationalityString;
    }

    public void setNationalityString(String nationalityString) {
        this.nationalityString = nationalityString;
    }

    public String getResidentialAddress() {
        return residentialAddress;
    }

    public void setResidentialAddress(String residentialAddress) {
        this.residentialAddress = residentialAddress;
    }

    public String getEmployer() {
        return employer;
    }

    public void setEmployer(String employer) {
        this.employer = employer;
    }
}
