package com.misoft.hospital.model.setup;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by kodero on 3/3/15.
 */
@Entity
@Table(name = "units_of_measure")
@Filter(name = "filterByDeleted")
public class UnitOfMeasure extends ModelBase {

    @Column(name = "name")
    private String name;

    @Column(name = "symbol")
    private String symbol;

    @Column(name = "factor", precision = 5)
    private BigDecimal factor;

    @Column(name = "rounding_precision", precision = 5)
    private BigDecimal roundingPrecision;

    @JoinColumn(name = "uom_category", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private UOMCategory uomCategory;

    @Column(name = "active")
    private Boolean active;

    @Column(name = "rate", precision = 5)
    private BigDecimal rate;

    @Column(name = "display_digits")
    private Integer displayDigits;

    @OneToMany(mappedBy = "unitOfMeasure", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @Filter(name = "filterByDeleted")
    private Collection<Product> products = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public BigDecimal getFactor() {
        return factor;
    }

    public void setFactor(BigDecimal factor) {
        this.factor = factor;
    }

    public BigDecimal getRoundingPrecision() {
        return roundingPrecision;
    }

    public void setRoundingPrecision(BigDecimal roundingPrecision) {
        this.roundingPrecision = roundingPrecision;
    }

    public UOMCategory getUomCategory() {
        return uomCategory;
    }

    public void setUomCategory(UOMCategory uomCategory) {
        this.uomCategory = uomCategory;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public Integer getDisplayDigits() {
        return displayDigits;
    }

    public void setDisplayDigits(Integer displayDigits) {
        this.displayDigits = displayDigits;
    }

    public Collection<Product> getProducts() {
        return products;
    }

    public void setProducts(Collection<Product> products) {
        this.products = products;
    }
}
