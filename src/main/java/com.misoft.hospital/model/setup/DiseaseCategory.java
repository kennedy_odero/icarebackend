package com.misoft.hospital.model.setup;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by kodero on 2/24/15.
 */
@Entity
@Table(name = "disease_categories")
@Filter(name = "filterByDeleted")
public class DiseaseCategory extends ModelBase {

    @Column(name = "name")
    private String name;

    @ManyToOne
    private DiseaseCategory parent;

    /**
     * parent program
     */
    @JoinColumn(name = "parent", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private DiseaseCategory diseaseCategory;

    /**
     * sub-categories
     */
    @OneToMany(mappedBy = "diseaseCategory", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @Filter(name = "filterByDeleted")
    private Collection<DiseaseCategory> diseaseCategories;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DiseaseCategory getParent() {
        return parent;
    }

    public void setParent(DiseaseCategory parent) {
        this.parent = parent;
    }

    public DiseaseCategory getDiseaseCategory() {
        return diseaseCategory;
    }

    public void setDiseaseCategory(DiseaseCategory diseaseCategory) {
        this.diseaseCategory = diseaseCategory;
    }

    public Collection<DiseaseCategory> getDiseaseCategories() {
        return diseaseCategories;
    }

    public void setDiseaseCategories(Collection<DiseaseCategory> diseaseCategories) {
        this.diseaseCategories = diseaseCategories;
    }
}
