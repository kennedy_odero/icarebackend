package com.misoft.hospital.model.setup;

/**
 * Created by kodero on 3/1/15.
 */
public enum WardStatus {
    AVAILABLE,
    NOT_AVAILABLE
}
