package com.misoft.hospital.model.setup;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.*;

/**
 * Created by kodero on 2/24/15.
 */
@Entity
@Table(name = "imaging_test_type")
@Filter(name = "filterByDeleted")
public class ImagingTestType extends ModelBase {

    @Column(name = "code")
    private String code;

    @Column(name = "name")
    private String name;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
