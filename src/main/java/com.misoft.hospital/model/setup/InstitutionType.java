package com.misoft.hospital.model.setup;

/**
 * Created by kodero on 3/1/15.
 */
public enum InstitutionType {
    GENERAL_HOSPITAL,
    PRIMARY_CARE_CENTER,
    CLINIC,
    NURSING_HOME,
    HOSPICE,
    RURAL_FACILITY
}
