package com.misoft.hospital.model.setup;

/**
 * Created by kodero on 3/1/15.
 */
public enum BedType {
    GATCH_BED,
    ELECTRIC_BED,
    STRETCHER,
    LOW_BED,
    LOW_AIR_LOSS,
    CIRCO_ELECTRIC,
    CLINITRON
}
