package com.misoft.hospital.model.setup;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by kodero on 3/1/15.
 */
@Entity
@DiscriminatorValue(value = "INSTITUTION")
public class Institution extends Party {
    @Column(name = "code")
    private String code;

    @Column(name = "institution_type")
    @Enumerated(EnumType.STRING)
    private InstitutionType institutionType;

    @OneToMany(mappedBy = "institution", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @org.hibernate.annotations.Filter(name = "filterByDeleted")
    private Collection<InstitutionSpecialty> institutionSpecialties;

    @OneToMany(mappedBy = "institution", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @org.hibernate.annotations.Filter(name = "filterByDeleted")
    private Collection<Building> buildings = new ArrayList<>();

    @OneToMany(mappedBy = "institution", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @org.hibernate.annotations.Filter(name = "filterByDeleted")
    private Collection<Unit> units = new ArrayList<>();

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public InstitutionType getInstitutionType() {
        return institutionType;
    }

    public void setInstitutionType(InstitutionType institutionType) {
        this.institutionType = institutionType;
    }

    public Collection<InstitutionSpecialty> getInstitutionSpecialties() {
        return institutionSpecialties;
    }

    public void setInstitutionSpecialties(Collection<InstitutionSpecialty> institutionSpecialties) {
        this.institutionSpecialties = institutionSpecialties;
    }

    public Collection<Building> getBuildings() {
        return buildings;
    }

    public void setBuildings(Collection<Building> buildings) {
        this.buildings = buildings;
    }

    public Collection<Unit> getUnits() {
        return units;
    }

    public void setUnits(Collection<Unit> units) {
        this.units = units;
    }
}
