package com.misoft.hospital.model.setup;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.*;

/**
 * Created by kodero on 8/26/15.
 */
@Entity
@Table(name = "imaging_tests")
@Filter(name = "filterByDeleted")
public class ImagingTest extends ModelBase{

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @JoinColumn(name = "test_type", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private ImagingTestType testType;

    @JoinColumn(name = "product", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Product product;

    @Column(name = "blocked", columnDefinition = "tinyint default 0")
    private Boolean blocked = false;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public ImagingTestType getTestType() {
        return testType;
    }

    public void setTestType(ImagingTestType testType) {
        this.testType = testType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getBlocked() {
        return blocked;
    }

    public void setBlocked(Boolean blocked) {
        this.blocked = blocked;
    }
}
