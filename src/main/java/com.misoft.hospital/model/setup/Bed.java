package com.misoft.hospital.model.setup;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.Formula;

import javax.persistence.*;

/**
 * Created by kodero on 3/1/15.
 */
@Entity
@Table(name = "beds")
@Filter(name = "filterByDeleted")
public class Bed extends ModelBase {

    @Column(name = "bed_no")
    private String bedNo;

    @Column(name = "info")
    private String info;

    @JoinColumn(name = "product", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Product product;

    @Column(name = "tel")
    private String tel;

    @Column(name = "bed_type")
    @Enumerated(EnumType.STRING)
    private BedType bedType;

    @Column(name = "bed_status")
    @Enumerated(EnumType.STRING)
    private BedStatus bedStatus;

    @JoinColumn(name = "ward", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Ward ward;

    @Formula("(coalesce((select 'Occupied' from admission_beds ab where ab.bed = id and ab.date_to is null limit 1),'Vacant'))")
    private String occupancy;

    public String getBedNo() {
        return bedNo;
    }

    public void setBedNo(String bedNo) {
        this.bedNo = bedNo;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public BedType getBedType() {
        return bedType;
    }

    public void setBedType(BedType bedType) {
        this.bedType = bedType;
    }

    public BedStatus getBedStatus() {
        return bedStatus;
    }

    public void setBedStatus(BedStatus bedStatus) {
        this.bedStatus = bedStatus;
    }

    public Ward getWard() {
        return ward;
    }

    public void setWard(Ward ward) {
        this.ward = ward;
    }

    public String getOccupancy() {
        return occupancy;
    }

    public void setOccupancy(String occupancy) {
        this.occupancy = occupancy;
    }
}
