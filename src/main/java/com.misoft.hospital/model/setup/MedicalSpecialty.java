package com.misoft.hospital.model.setup;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by kodero on 3/1/15.
 */
@Entity
@Table(name = "medical_specialties")
@Filter(name = "filterByDeleted")
public class MedicalSpecialty extends ModelBase {

    @Column(name = "name")
    private String name;

    @Column(name = "code")
    private String code;

    @OneToMany(mappedBy = "medicalSpecialty", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @Filter(name = "filterByDeleted")
    private Collection<InstitutionSpecialty> institutionSpecialties = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Collection<InstitutionSpecialty> getInstitutionSpecialties() {
        return institutionSpecialties;
    }

    public void setInstitutionSpecialties(Collection<InstitutionSpecialty> institutionSpecialties) {
        this.institutionSpecialties = institutionSpecialties;
    }
}
