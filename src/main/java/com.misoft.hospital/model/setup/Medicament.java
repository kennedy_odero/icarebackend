package com.misoft.hospital.model.setup;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by kodero on 3/3/15.
 */
@Entity
@Table(name = "medicament")
@Filter(name = "filterByDeleted")
public class Medicament extends ModelBase {

    @Column(name = "active_comp")
    private String activeComponent;

    @Column(name = "applicable_age")
    private Integer applicableAge;

    @Column(name = "applicable_weight")
    private BigDecimal applicableWeight;

    @JoinColumn(name = "product", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Product product;

    @Column(name = "extra_info", columnDefinition = "text")
    private String extraInfo;

    @JoinColumn(name = "med_category", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private MedicamentCategory medicamentCategory;

    @JoinColumn(name = "drug_form", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private DrugForm drugForm;

    @JoinColumn(name = "drug_dose_unit", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private DrugDoseUnit drugDoseUnit;

    @Column(name = "unit_quantity")
    private BigDecimal unitQuantity;

    @Column(name = "presentation", columnDefinition = "text")
    private String presentation;

    @Column(name = "composition", columnDefinition = "text")
    private String composition;

    @Column(name = "dsg_intructions", columnDefinition = "text")
    private String dosageInstructions;

    @Column(name = "overdosage", columnDefinition = "text")
    private String overdosage;

    @Column(name = "preg_warning")
    private Boolean pregWarning;

    @Column(name = "pregnancy_category", columnDefinition="varchar(20) default 'UNKNOWN'")
    @Enumerated(EnumType.STRING)
    private PregnancyCategory pregnancyCategory;

    @Column(name = "adverse_reactions", columnDefinition = "text")
    private String adverseReactions;

    @Column(name = "storage", columnDefinition = "text")
    private String storage;

    @Column(name = "notes_on_preg", columnDefinition = "text")
    private String notesOnPreg;

    @Column(name = "blocked", columnDefinition = "tinyint default 0")
    private Boolean blocked = false;

    public String getActiveComponent() {
        return activeComponent;
    }

    public void setActiveComponent(String activeComponent) {
        this.activeComponent = activeComponent;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public String getExtraInfo() {
        return extraInfo;
    }

    public void setExtraInfo(String extraInfo) {
        this.extraInfo = extraInfo;
    }

    public MedicamentCategory getMedicamentCategory() {
        return medicamentCategory;
    }

    public void setMedicamentCategory(MedicamentCategory medicamentCategory) {
        this.medicamentCategory = medicamentCategory;
    }

    public String getPresentation() {
        return presentation;
    }

    public void setPresentation(String presentation) {
        this.presentation = presentation;
    }

    public String getComposition() {
        return composition;
    }

    public void setComposition(String composition) {
        this.composition = composition;
    }

    public String getDosageInstructions() {
        return dosageInstructions;
    }

    public void setDosageInstructions(String dosageInstructions) {
        this.dosageInstructions = dosageInstructions;
    }

    public String getOverdosage() {
        return overdosage;
    }

    public void setOverdosage(String overdosage) {
        this.overdosage = overdosage;
    }

    public Boolean getPregWarning() {
        return pregWarning;
    }

    public void setPregWarning(Boolean pregWarning) {
        this.pregWarning = pregWarning;
    }

    public PregnancyCategory getPregnancyCategory() {
        return pregnancyCategory;
    }

    public void setPregnancyCategory(PregnancyCategory pregnancyCategory) {
        this.pregnancyCategory = pregnancyCategory;
    }

    public String getNotesOnPreg() {
        return notesOnPreg;
    }

    public void setNotesOnPreg(String notesOnPreg) {
        this.notesOnPreg = notesOnPreg;
    }

    public String getAdverseReactions() {
        return adverseReactions;
    }

    public void setAdverseReactions(String adverseReactions) {
        this.adverseReactions = adverseReactions;
    }

    public String getStorage() {
        return storage;
    }

    public void setStorage(String storage) {
        this.storage = storage;
    }

    public Integer getApplicableAge() {
        return applicableAge;
    }

    public void setApplicableAge(Integer applicableAge) {
        this.applicableAge = applicableAge;
    }

    public BigDecimal getApplicableWeight() {
        return applicableWeight;
    }

    public void setApplicableWeight(BigDecimal applicableWeight) {
        this.applicableWeight = applicableWeight;
    }

    public DrugForm getDrugForm() {
        return drugForm;
    }

    public void setDrugForm(DrugForm drugForm) {
        this.drugForm = drugForm;
    }

    public DrugDoseUnit getDrugDoseUnit() {
        return drugDoseUnit;
    }

    public void setDrugDoseUnit(DrugDoseUnit drugDoseUnit) {
        this.drugDoseUnit = drugDoseUnit;
    }

    public BigDecimal getUnitQuantity() {
        return unitQuantity;
    }

    public void setUnitQuantity(BigDecimal unitQuantity) {
        this.unitQuantity = unitQuantity;
    }

    public Boolean getBlocked() {
        return blocked;
    }

    public void setBlocked(Boolean blocked) {
        this.blocked = blocked;
    }
}
