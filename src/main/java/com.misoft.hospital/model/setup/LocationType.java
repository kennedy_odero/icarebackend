package com.misoft.hospital.model.setup;

/**
 * Created by kodero on 8/25/15.
 */
public enum LocationType {
    LOCATION,
    SUBLOCATION,
    VILLAGE
}
