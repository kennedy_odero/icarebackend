package com.misoft.hospital.model.setup;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by kodero on 2/25/15.
 */
@Entity
@Table(name = "test_cases")
@Filter(name = "filterByDeleted")
public class TestCase extends ModelBase {

    @Column(name = "seq")
    private Integer seq;

    @Column(name = "analyte")
    private String analyte;

    @Column(name = "lower_limit")
    private String lowerLimit;

    @Column(name = "upper_limit")
    private String upperLimit;

    @Column(name = "reference", columnDefinition = "TEXT")
    private String reference;

    @JoinColumn(name = "test_unit", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private LabTestUnit labTestUnit;

    @JoinColumn(name = "test_type", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private LabTestType labTestType;

    public Integer getSeq() {
        return seq;
    }

    public void setSeq(Integer seq) {
        this.seq = seq;
    }

    public String getAnalyte() {
        return analyte;
    }

    public void setAnalyte(String analyte) {
        this.analyte = analyte;
    }

    public String getLowerLimit() {
        return lowerLimit;
    }

    public void setLowerLimit(String lowerLimit) {
        this.lowerLimit = lowerLimit;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public LabTestUnit getLabTestUnit() {
        return labTestUnit;
    }

    public void setLabTestUnit(LabTestUnit labTestUnit) {
        this.labTestUnit = labTestUnit;
    }

    public LabTestType getLabTestType() {
        return labTestType;
    }

    public void setLabTestType(LabTestType labTestType) {
        this.labTestType = labTestType;
    }

    public String getUpperLimit() {
        return upperLimit;
    }

    public void setUpperLimit(String upperLimit) {
        this.upperLimit = upperLimit;
    }
}
