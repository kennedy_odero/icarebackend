package com.misoft.hospital.model.setup;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by kodero on 3/3/15.
 */
@Entity
@Table(name = "uom_categories")
@Filter(name = "filterByDeleted")
public class UOMCategory extends ModelBase{

    @Column(name = "name")
    private String name;

    @Column(name = "descr")
    private String descr;

    @OneToMany(mappedBy = "uomCategory", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @Filter(name = "filterByDeleted")
    private Collection<UnitOfMeasure> unitOfMeasures = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Collection<UnitOfMeasure> getUnitOfMeasures() {
        return unitOfMeasures;
    }

    public void setUnitOfMeasures(Collection<UnitOfMeasure> unitOfMeasures) {
        this.unitOfMeasures = unitOfMeasures;
    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }
}
