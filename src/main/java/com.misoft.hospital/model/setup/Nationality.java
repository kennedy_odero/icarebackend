package com.misoft.hospital.model.setup;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.*;

/**
 * Created by kodero on 4/3/15.
 */
@Entity
@Table(name = "nationalities")
@Filter(name = "filterByDeleted")
public class Nationality extends ModelBase {

    @Column(name = "name")
    private String name;

    @JoinColumn(name = "country", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Country country;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }
}
