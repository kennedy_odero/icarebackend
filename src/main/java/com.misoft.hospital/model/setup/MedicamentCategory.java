package com.misoft.hospital.model.setup;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by kodero on 3/4/15.
 */
@Entity
@Table(name ="medicament_category")
@Filter(name = "filterByDeleted")
public class MedicamentCategory extends ModelBase {

    @Column(name ="name")
    private String name;

    @OneToMany(mappedBy = "medicamentCategory", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @Filter(name = "filterByDeleted")
    private Collection<Medicament> medicaments = new ArrayList<>();

    @OneToMany(mappedBy = "medicamentCategory", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @Filter(name = "filterByDeleted")
    private Collection<MedicamentCategory> medicamentCategories = new ArrayList<>();

    @JoinColumn(name = "parent", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private MedicamentCategory medicamentCategory;//parent category

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Collection<Medicament> getMedicaments() {
        return medicaments;
    }

    public void setMedicaments(Collection<Medicament> medicaments) {
        this.medicaments = medicaments;
    }

    public Collection<MedicamentCategory> getMedicamentCategories() {
        return medicamentCategories;
    }

    public void setMedicamentCategories(Collection<MedicamentCategory> medicamentCategories) {
        this.medicamentCategories = medicamentCategories;
    }

    public MedicamentCategory getMedicamentCategory() {
        return medicamentCategory;
    }

    public void setMedicamentCategory(MedicamentCategory medicamentCategory) {
        this.medicamentCategory = medicamentCategory;
    }
}
