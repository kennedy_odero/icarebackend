package com.misoft.hospital.model.setup;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.*;

/**
 * Created by kodero on 3/1/15.
 */
@Entity
@Table(name = "institution_specialty")
@Filter(name = "filterByDeleted")
public class InstitutionSpecialty extends ModelBase{

    @JoinColumn(name = "institution", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Institution institution;

    @JoinColumn(name = "medical_specialty", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private MedicalSpecialty medicalSpecialty;

    public Institution getInstitution() {
        return institution;
    }

    public void setInstitution(Institution institution) {
        this.institution = institution;
    }

    public MedicalSpecialty getMedicalSpecialty() {
        return medicalSpecialty;
    }

    public void setMedicalSpecialty(MedicalSpecialty medicalSpecialty) {
        this.medicalSpecialty = medicalSpecialty;
    }
}
