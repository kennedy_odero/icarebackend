package com.misoft.hospital.model.billing;

/**
 * Created by kodero on 9/14/15.
 */
public enum BillType {
    Cash,
    Insurance
}
