package com.misoft.hospital.model.billing;

/**
 * Created by kodero on 5/2/16.
 */
public enum BillItemType {
    Normal,
    Accumulation
}
