package com.misoft.hospital.model.billing;

import com.misoft.hospital.model.base.ModelBase;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by kodero on 12/17/15.
 */
@Entity
@Table(name = "bill_credits")
public class BillCredit extends ModelBase{

    @JoinColumn(name = "bill", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private PatientBill patientBill;

    @JoinColumn(name = "credit", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private PatientCredit patientCredit;

    @Column(name = "amount_applied")
    private BigDecimal amountApplied;

    public PatientBill getPatientBill() {
        return patientBill;
    }

    public void setPatientBill(PatientBill patientBill) {
        this.patientBill = patientBill;
    }

    public PatientCredit getPatientCredit() {
        return patientCredit;
    }

    public void setPatientCredit(PatientCredit patientCredit) {
        this.patientCredit = patientCredit;
    }

    public BigDecimal getAmountApplied() {
        return amountApplied;
    }

    public void setAmountApplied(BigDecimal amountApplied) {
        this.amountApplied = amountApplied;
    }
}
