package com.misoft.hospital.model.billing;

/**
 * Created by kodero on 10/3/17.
 */
public enum BillItemStatus {
    BILLED,
    CANCELLED,
    REFUNDED
}
