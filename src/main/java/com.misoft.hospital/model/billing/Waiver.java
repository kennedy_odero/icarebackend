package com.misoft.hospital.model.billing;

import com.misoft.hospital.model.base.ModelBase;
import com.misoft.hospital.model.healthcare.Patient;
import com.misoft.hospital.util.validation.ValidationMethod;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.Formula;

import javax.persistence.*;
import javax.validation.ValidationException;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 * Created by kodero on 9/27/15.
 */
@Entity
@Table(name = "waivers")
@Filter(name = "filterByDeleted")
public class Waiver extends ModelBase{

    @Column(name = "waiver_no")
    private Long waiverNo;

    @NotNull
    @JoinColumn(name = "patient", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Patient patient;

    @NotNull
    @Column(name = "waiver_date")
    private Date waiverDate;

    @Column(name = "form_number")
    private String formNumber;

    @NotNull
    @Column(name = "amount_waived")
    private BigDecimal amountWaived;

    @Formula("(select coalesce(sum(coalesce(bw.amount_waived, 0)),0) from bill_waivers bw where bw.waiver = id)")
    private BigDecimal allocated;

    //@Formula("(coalesce(amount_waived,0) - (select coalesce(sum(coalesce(bw.amount_waived, 0)),0) from bill_waivers bw where bw.waiver = id))")
    @Formula("0")
    private BigDecimal unallocated;

    @Column(name = "notes")
    private String notes;

    @OneToMany(mappedBy = "waiver", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @Filter(name = "filterByDeleted")
    private Collection<BillWaiver> billWaivers = new ArrayList<>();

    @OneToMany(mappedBy = "waiver", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @Filter(name = "filterByDeleted")
    private Collection<BillItemWaiver> billItemWaivers = new ArrayList<>();

    @Column(name = "waiver_status", columnDefinition = "varchar(20) default 'NEW'")
    @Enumerated(EnumType.STRING)
    private WaiverStatus waiverStatus = WaiverStatus.NEW;

    @ValidationMethod
    public void validateEmptyBills(){
        if(getBillItemWaivers() == null || getBillItemWaivers().size() == 0)
            throw new ValidationException("A waiver must be attached to at least one bill item!");
        BigDecimal totalWaived = BigDecimal.ZERO;
        for(BillItemWaiver bw : getBillItemWaivers()){
            totalWaived = totalWaived.add(bw.getAmountWaived());
        }
        if(getAmountWaived().compareTo(totalWaived) > 0)
            throw new ValidationException("The waived amount cannot be greater than the outstanding bill!");
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Date getWaiverDate() {
        return waiverDate;
    }

    public void setWaiverDate(Date waiverDate) {
        this.waiverDate = waiverDate;
    }

    public BigDecimal getAmountWaived() {
        return amountWaived;
    }

    public void setAmountWaived(BigDecimal amountWaived) {
        this.amountWaived = amountWaived;
    }

    public BigDecimal getAllocated() {
        return allocated;
    }

    public void setAllocated(BigDecimal allocated) {
        this.allocated = allocated;
    }

    public BigDecimal getUnallocated() {
        return unallocated;
    }

    public void setUnallocated(BigDecimal unallocated) {
        this.unallocated = unallocated;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Collection<BillWaiver> getBillWaivers() {
        return billWaivers;
    }

    public void setBillWaivers(Collection<BillWaiver> billWaivers) {
        this.billWaivers = billWaivers;
    }

    public Long getWaiverNo() {
        return waiverNo;
    }

    public void setWaiverNo(Long waiverNo) {
        this.waiverNo = waiverNo;
    }

    public WaiverStatus getWaiverStatus() {
        return waiverStatus;
    }

    public void setWaiverStatus(WaiverStatus waiverStatus) {
        this.waiverStatus = waiverStatus;
    }

    public String getFormNumber() {
        return formNumber;
    }

    public void setFormNumber(String formNumber) {
        this.formNumber = formNumber;
    }

    public Collection<BillItemWaiver> getBillItemWaivers() {
        return billItemWaivers;
    }

    public void setBillItemWaivers(Collection<BillItemWaiver> billItemWaivers) {
        this.billItemWaivers = billItemWaivers;
    }
}
