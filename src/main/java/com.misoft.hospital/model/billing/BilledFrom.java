package com.misoft.hospital.model.billing;

/**
 * Created by kodero on 2/7/16.
 */
public enum BilledFrom {
    SERVICE_REQUEST,
    PATIENT_CHARGE
}
