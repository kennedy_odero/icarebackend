package com.misoft.hospital.model.billing;

import com.misoft.hospital.model.base.ModelBase;
import com.misoft.hospital.model.healthcare.Appointment;
import com.misoft.hospital.model.healthcare.Patient;
import com.misoft.hospital.model.healthcare.ServiceRequest;
import com.misoft.hospital.model.setup.Product;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by kodero on 1/17/16.
 */
@Entity
@Table(name = "patient_charges")
@Filter(name = "filterByDeleted")
public class PatientCharge extends ModelBase{
    @NotNull
    @Column(name = "charge_no", nullable = false)
    private Long chargeNo;

    @NotNull
    @JoinColumn(name = "product", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Product product;

    @NotNull
    @Column(name = "quantity")
    private Integer quantity;

    @NotNull
    @Column(name = "total")
    private BigDecimal total;

    @NotNull
    @JoinColumn(name = "patient", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Patient patient;

    @JoinColumn(name = "service_request", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private ServiceRequest serviceRequest;

    @NotNull
    @JoinColumn(name = "appointment", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Appointment appointment;

    @NotNull
    @Column(name = "date_posted")
    private Date datePosted;

    @Column(name = "notes")
    private String notes;

    //billing details
    @JoinColumn(name = "billing_item", referencedColumnName = "id", unique = true)
    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    private BillItem billItem;

    public Long getChargeNo() {
        return chargeNo;
    }

    public void setChargeNo(Long chargeNo) {
        this.chargeNo = chargeNo;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public ServiceRequest getServiceRequest() {
        return serviceRequest;
    }

    public void setServiceRequest(ServiceRequest serviceRequest) {
        this.serviceRequest = serviceRequest;
    }

    public Appointment getAppointment() {
        return appointment;
    }

    public void setAppointment(Appointment appointment) {
        this.appointment = appointment;
    }

    public Date getDatePosted() {
        return datePosted;
    }

    public void setDatePosted(Date datePosted) {
        this.datePosted = datePosted;
    }

    public BillItem getBillItem() {
        return billItem;
    }

    public void setBillItem(BillItem billItem) {
        this.billItem = billItem;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }
}
