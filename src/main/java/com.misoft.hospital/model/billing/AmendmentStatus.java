package com.misoft.hospital.model.billing;

/**
 * Created by kodero on 6/7/16.
 */
public enum AmendmentStatus {
    NEW,
    SUBMITTED,
    CANCELLED,
    APPROVED,
    DECLINED
}
