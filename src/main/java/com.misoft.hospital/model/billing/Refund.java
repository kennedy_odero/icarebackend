package com.misoft.hospital.model.billing;

import com.misoft.hospital.model.accounts.CashierShift;
import com.misoft.hospital.model.accounts.PaymentMode;
import com.misoft.hospital.model.base.ModelBase;
import com.misoft.hospital.model.healthcare.Patient;
import com.misoft.hospital.util.validation.ValidateAt;
import com.misoft.hospital.util.validation.ValidationMethod;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import javax.validation.ValidationException;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 * Created by kodero on 12/16/15.
 */
@Entity
@Table(name = "refunds")
@Filter(name = "filterByDeleted")
public class Refund extends ModelBase{

    @NotNull
    @Column(name = "refund_no")
    private Long refundNo;

    @NotNull
    @JoinColumn(name = "patient", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Patient patient;

    @NotNull
    @Column(name = "date_created")
    private Date dateCreated = new Date();

    //@NotNull
    @Column(name = "date_paid")
    private Date datePaid;

    @NotNull
    @Column(name = "notes")
    private String notes;

    @NotNull
    @Column(name = "amount_paid")
    private BigDecimal amountPaid;

    @NotNull
    @Column(name = "payment_mode")
    @Enumerated(EnumType.STRING)
    private PaymentMode paymentMode;

    @OneToMany(mappedBy = "patientPayment", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @Filter(name = "filterByDeleted")
    private Collection<PaymentRefund> paymentRefunds = new ArrayList<>();

    @Column(name = "refund_status", columnDefinition = "varchar(20) default 'NEW'")
    @Enumerated(EnumType.STRING)
    private RefundStatus refundStatus;

    @Column(name = "issue_status", columnDefinition = "varchar(100) default 'NOT_ISSUED'")
    @Enumerated(EnumType.STRING)
    private IssueStatus issueStatus = IssueStatus.NOT_ISSUED;

    @Column(name = "issued_to_name")
    private String issuedToName;

    @Column(name = "issued_to_id")
    private String issuedToId;

    //@NotNull
    @JoinColumn(name = "shift", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private CashierShift cashierShift;

    @ValidationMethod
    public void validateZeroRefundAmount(){
        if(BigDecimal.ZERO.compareTo(getAmountPaid()) == 0 || getPaymentRefunds().size() == 0 )
            throw new ValidationException("Refund amount must be greater than '0'!");
    }

    @ValidationMethod(when = ValidateAt.UPDATE)
    public void validateIssuance(){
        if( getIssueStatus() == IssueStatus.ISSUED ){
            //now check stuff
            if(getIssuedToName() == null || getIssuedToId() == null)
                throw new ValidationException("You need to specify to whom the cash is being issued!");
            if(getCashierShift() == null)
                throw new ValidationException("You need to have started a shift in order to issue refunds!");
        }
    }

    public Long getRefundNo() {
        return refundNo;
    }

    public void setRefundNo(Long refundNo) {
        this.refundNo = refundNo;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Date getDatePaid() {
        return datePaid;
    }

    public void setDatePaid(Date datePaid) {
        this.datePaid = datePaid;
    }

    public BigDecimal getAmountPaid() {
        return amountPaid;
    }

    public void setAmountPaid(BigDecimal amountPaid) {
        this.amountPaid = amountPaid;
    }

    public PaymentMode getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(PaymentMode paymentMode) {
        this.paymentMode = paymentMode;
    }

    public Collection<PaymentRefund> getPaymentRefunds() {
        return paymentRefunds;
    }

    public void setPaymentRefunds(Collection<PaymentRefund> paymentRefunds) {
        this.paymentRefunds = paymentRefunds;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public RefundStatus getRefundStatus() {
        return refundStatus;
    }

    public void setRefundStatus(RefundStatus refundStatus) {
        this.refundStatus = refundStatus;
    }

    public void addPaymentRefund(PaymentRefund paymentRefund) {
        if(paymentRefund == null){
            paymentRefund.setRefund(this);
            getPaymentRefunds().add(paymentRefund);
        }
    }

    public IssueStatus getIssueStatus() {
        return issueStatus;
    }

    public void setIssueStatus(IssueStatus issueStatus) {
        this.issueStatus = issueStatus;
    }

    public String getIssuedToName() {
        return issuedToName;
    }

    public void setIssuedToName(String issuedToName) {
        this.issuedToName = issuedToName;
    }

    public String getIssuedToId() {
        return issuedToId;
    }

    public void setIssuedToId(String issuedToId) {
        this.issuedToId = issuedToId;
    }

    public CashierShift getCashierShift() {
        return cashierShift;
    }

    public void setCashierShift(CashierShift cashierShift) {
        this.cashierShift = cashierShift;
    }
}
