package com.misoft.hospital.model.billing;

/**
 * Created by kodero on 6/9/16.
 */
public class BillAmendedEvent {
    public PatientBill patientBill;

    public BillAmendedEvent(PatientBill patientBill){
        setPatientBill(patientBill);
    }

    public PatientBill getPatientBill() {
        return patientBill;
    }

    public void setPatientBill(PatientBill patientBill) {
        this.patientBill = patientBill;
    }
}
