package com.misoft.hospital.model.billing;

import com.misoft.hospital.model.base.ModelBase;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * Created by kodero on 9/16/16.
 */
@Entity
@Table(name = "payment_revisions")
public class PaymentRevision extends ModelBase{

    @NotNull
    @JoinColumn(name = "patient", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private PatientPayment patientPayment;

    @Column(name = "amount")
    private BigDecimal amount;

    public PaymentRevision(){

    }

    public PaymentRevision(PatientPayment patientPayment, BigDecimal amount){
        setPatientPayment(patientPayment);
        setAmount(amount);
    }

    public PatientPayment getPatientPayment() {
        return patientPayment;
    }

    public void setPatientPayment(PatientPayment patientPayment) {
        this.patientPayment = patientPayment;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
