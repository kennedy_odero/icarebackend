package com.misoft.hospital.model.billing;

import com.misoft.hospital.model.base.ModelBase;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by kodero on 9/27/15.
 */
@Entity
@Table(name = "bill_waivers")
public class BillWaiver extends ModelBase{
    @JoinColumn(name = "waiver", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Waiver waiver;

    @JoinColumn(name = "patient_bill", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private PatientBill patientBill;

    @Column(name = "amount_waived")
    private BigDecimal amountWaived;

    public Waiver getWaiver() {
        return waiver;
    }

    public void setWaiver(Waiver waiver) {
        this.waiver = waiver;
    }

    public PatientBill getPatientBill() {
        return patientBill;
    }

    public void setPatientBill(PatientBill patientBill) {
        this.patientBill = patientBill;
    }

    public BigDecimal getAmountWaived() {
        return amountWaived;
    }

    public void setAmountWaived(BigDecimal amountWaived) {
        this.amountWaived = amountWaived;
    }
}
