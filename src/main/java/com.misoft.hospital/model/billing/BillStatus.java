package com.misoft.hospital.model.billing;

/**
 * Created by kodero on 9/14/15.
 */
public enum BillStatus {
    APPROVED,
    AWAITING_PAYMENT,
    PARTIALLY_PAID,
    FULLY_PAID,
    VOIDED,
    CANCELLED,
    WAIVED
}
