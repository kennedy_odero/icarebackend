package com.misoft.hospital.model.billing;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * Created by kodero on 9/18/17.
 */
@Entity
@Table(name = "bill_item_payments")
@Filter(name = "filterByDeleted")
public class BillItemPayment extends ModelBase{

    @NotNull
    @JoinColumn(name = "bill_item", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private BillItem billItem;

    @NotNull
    @JoinColumn(name = "patient_payment", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private PatientPayment patientPayment;

    /*
     * Only here as a place holder, since a patient is required to pay for the bill items in full
     */
    @NotNull
    @Column(name = "amount_settled")
    private BigDecimal amountSettled;

    public BillItem getBillItem() {
        return billItem;
    }

    public void setBillItem(BillItem billItem) {
        this.billItem = billItem;
    }

    public PatientPayment getPatientPayment() {
        return patientPayment;
    }

    public void setPatientPayment(PatientPayment patientPayment) {
        this.patientPayment = patientPayment;
    }

    public BigDecimal getAmountSettled() {
        return amountSettled;
    }

    public void setAmountSettled(BigDecimal amountSettled) {
        this.amountSettled = amountSettled;
    }
}
