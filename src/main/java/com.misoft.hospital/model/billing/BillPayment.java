package com.misoft.hospital.model.billing;

import com.misoft.hospital.model.base.ModelBase;
import com.misoft.hospital.model.healthcare.ServiceRequest;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by kodero on 9/13/15.
 */
@Entity
@Table(name = "bill_payments")
@Filter(name = "filterByDeleted")
public class BillPayment extends ModelBase{

    @JoinColumn(name = "patient_payment", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private PatientPayment patientPayment;

    @JoinColumn(name = "patient_bill", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private PatientBill patientBill;

    @Column(name = "amount_settled")
    private BigDecimal amountSettled;

    public PatientPayment getPatientPayment() {
        return patientPayment;
    }

    public void setPatientPayment(PatientPayment patientPayment) {
        this.patientPayment = patientPayment;
    }

    public BigDecimal getAmountSettled() {
        return amountSettled;
    }

    public void setAmountSettled(BigDecimal amountSettled) {
        this.amountSettled = amountSettled;
    }

    public PatientBill getPatientBill() {
        return patientBill;
    }

    public void setPatientBill(PatientBill patientBill) {
        this.patientBill = patientBill;
    }
}
