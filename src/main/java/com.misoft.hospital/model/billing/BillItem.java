package com.misoft.hospital.model.billing;

import com.misoft.hospital.model.base.ModelBase;
import com.misoft.hospital.model.healthcare.ServiceCenter;
import com.misoft.hospital.model.setup.Product;
import com.misoft.hospital.util.callbacks.During;
import com.misoft.hospital.util.callbacks.EntityCallbackMethod;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.Formula;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by kodero on 9/14/15.
 */
@Entity
@Table(name = "bill_items")
@Filter(name = "filterByDeleted")
public class BillItem extends ModelBase{

    @NotNull
    @Column(name = "bill_no")
    private Long billNo;

    @JoinColumn(name = "product", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Product product;

    @JoinColumn(name = "patient_bill", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private PatientBill patientBill;

    @NotNull
    @Column(name = "quantity")
    private BigDecimal quantity;

    @Formula("(case bill_item_type when 'Normal' then quantity " +
            "   when 'Accumulation' then DATEDIFF(coalesce(date_to, now()), date_from) end)")
    private BigDecimal normalizedQty;

    @NotNull
    @Column(name = "unit_price")
    private BigDecimal unitPrice;

    @Column(name = "line_total")
    @Formula("(case bill_item_type when 'Normal' then (quantity * unit_price)" +
            "   when 'Accumulation' then DATEDIFF(coalesce(date_to, now()), date_from) * unit_price end)")
    private BigDecimal lineTotal;

    @Formula("((case bill_item_type when 'Normal' then (quantity * unit_price)" +
            "   when 'Accumulation' then DATEDIFF(coalesce(date_to, now()), date_from) * unit_price end) - " +
            "coalesce((select sum(bip.amount_settled) from bill_item_payments bip where bip.bill_item = id and bip.deleted <> 1),0) - " +
            "coalesce((select sum(biw.amount_waived) from bill_item_waivers biw where biw.bill_item = id and biw.deleted <> 1),0) )")
    private BigDecimal balance;

    @Column(name = "tax")
    private BigDecimal tax;

    @NotNull
    @Column(name = "billed_from")
    private BilledFrom billedFrom;

    @Column(name = "sr_fqn")
    private String srFqn;

    @Column(name = "sr_type")
    private String srType;

    @Column(name = "sr_id")
    private String srId;

    //@NotNull
    /*@JoinColumn(name = "svc_center", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private ServiceCenter serviceCenter;*/

    @Column(name = "bill_item_type", length = 20)
    @Enumerated(EnumType.STRING)
    private BillItemType billItemType = BillItemType.Normal;//default is normal

    @Column(name = "reference")
    private String reference;

    @Column(name = "date_from")
    private Date dateFrom;

    @Column(name = "date_to")
    private Date dateTo;

    @Column(name = "paid", columnDefinition = "tinyint default '0'")
    private Boolean paid;

    @Enumerated(EnumType.STRING)
    @Column(name = "bill_item_status")
    private BillItemStatus billItemStatus = BillItemStatus.BILLED;

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((billNo == null) ? 0 : billNo.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        BillItem other = (BillItem) obj;
        if (billNo == null) {
            if (other.billNo != null)
                return false;
        } else if (!billNo.equals(other.billNo))
            return false;
        return true;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    public BigDecimal getLineTotal() {
        return lineTotal;
    }

    public void setLineTotal(BigDecimal lineTotal) {
        this.lineTotal = lineTotal;
    }

    public BigDecimal getTax() {
        return tax;
    }

    public void setTax(BigDecimal tax) {
        this.tax = tax;
    }

    public PatientBill getPatientBill() {
        return patientBill;
    }

    public void setPatientBill(PatientBill patientBill) {
        this.patientBill = patientBill;
    }

    public String getSrFqn() {
        return srFqn;
    }

    public void setSrFqn(String srFqn) {
        this.srFqn = srFqn;
    }

    public String getSrId() {
        return srId;
    }

    public void setSrId(String srId) {
        this.srId = srId;
    }

    public BilledFrom getBilledFrom() {
        return billedFrom;
    }

    public void setBilledFrom(BilledFrom billedFrom) {
        this.billedFrom = billedFrom;
    }

    public String getSrType() {
        return srType;
    }

    public void setSrType(String srType) {
        this.srType = srType;
    }

    public BillItemType getBillItemType() {
        return billItemType;
    }

    public void setBillItemType(BillItemType billItemType) {
        this.billItemType = billItemType;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public BigDecimal getNormalizedQty() {
        return normalizedQty;
    }

    public void setNormalizedQty(BigDecimal normalizedQty) {
        this.normalizedQty = normalizedQty;
    }

    public Long getBillNo() {
        return billNo;
    }

    public void setBillNo(Long billNo) {
        this.billNo = billNo;
    }

    public Boolean isPaid() {
        return paid;
    }

    public void setPaid(Boolean paid) {
        this.paid = paid;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public BillItemStatus getBillItemStatus() {
        return billItemStatus;
    }

    public void setBillItemStatus(BillItemStatus billItemStatus) {
        this.billItemStatus = billItemStatus;
    }
}
