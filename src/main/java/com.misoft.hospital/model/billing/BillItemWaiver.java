package com.misoft.hospital.model.billing;

import com.misoft.hospital.model.base.ModelBase;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * Created by kodero on 9/26/17.
 */
@Entity
@Table(name = "bill_item_waivers")
public class BillItemWaiver extends ModelBase{
    @NotNull
    @JoinColumn(name = "bill_item", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private BillItem billItem;

    @NotNull
    @JoinColumn(name = "waiver", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Waiver waiver;

    /*
     * Only here as a place holder, since a patient is required to pay for the bill items in full
     */
    @NotNull
    @Column(name = "amount_waived")
    private BigDecimal amountWaived;

    public BillItem getBillItem() {
        return billItem;
    }

    public void setBillItem(BillItem billItem) {
        this.billItem = billItem;
    }

    public Waiver getWaiver() {
        return waiver;
    }

    public void setWaiver(Waiver waiver) {
        this.waiver = waiver;
    }

    public BigDecimal getAmountWaived() {
        return amountWaived;
    }

    public void setAmountWaived(BigDecimal amountWaived) {
        this.amountWaived = amountWaived;
    }
}
