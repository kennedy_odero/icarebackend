package com.misoft.hospital.model.billing;

import com.misoft.hospital.model.accounts.ShiftStatus;
import com.misoft.hospital.model.base.ModelBase;
import com.misoft.hospital.model.healthcare.*;
import com.misoft.hospital.service.billing.PatientBillCallback;
import com.misoft.hospital.util.callbacks.During;
import com.misoft.hospital.util.callbacks.EntityCallbackClass;
import com.misoft.hospital.util.callbacks.EntityCallbackMethod;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.Formula;

import javax.persistence.*;
import javax.validation.ValidationException;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.*;

/**
 * Created by kodero on 9/14/15.
 */
@Entity
@Table(name = "patient_bills")
@Filter(name = "filterByDeleted")
@EntityCallbackClass(PatientBillCallback.class)
public class PatientBill extends ModelBase{

    @NotNull
    @JoinColumn(name = "appointment", referencedColumnName = "id", unique = true)
    @ManyToOne(fetch = FetchType.LAZY)
    private Appointment appointment;

    @NotNull
    @Column(name = "bill_no", nullable = false)
    private Long billNo;

    @NotNull
    @JoinColumn(name = "patient", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Patient patient;

    @JoinColumn(name = "service_request", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private ServiceRequest serviceRequest;

    @Column(name = "date_generated")
    private Date dateGenerated;

    @Column(name = "due_date")
    private Date dueDate;

    @Column(name = "bill_type")
    @Enumerated(EnumType.STRING)
    private BillType billType;

    @Formula("(coalesce((select\n" +
            "(select\n" +
            "    sum(case bi.bill_item_type \n" +
            "        when 'Normal' then (bi.quantity * bi.unit_price) \n" +
            "        when 'Accumulation' then DATEDIFF(coalesce(bi.date_to, now()), bi.date_from) * bi.unit_price \n" +
            "    end )\n" +
            "from\n" +
            "    bill_items bi \n" +
            "where\n" +
            "    bi.patient_bill = id and bi.deleted <> 1)),0))")
    private BigDecimal amountBilled;

    @Formula("(coalesce((select sum(coalesce(bw.amount_waived, 0)) from bill_waivers bw inner join waivers w on bw.waiver = w.id where bw.patient_bill = id and w.waiver_status = 'APPROVED'),0) + " +
            "coalesce((select sum(coalesce(biw.amount_waived, 0)) from bill_item_waivers biw inner join waivers w on w.id = biw.waiver inner join bill_items bi on biw.bill_item = bi.id where bi.patient_bill = id and w.waiver_status = 'APPROVED'),0) )")
    private BigDecimal amountWaived;

    @Formula("coalesce(amount_billed, 0) - coalesce((select sum(coalesce(bw.amount_waived, 0)) from bill_waivers bw inner join waivers w on bw.waiver = w.id where bw.patient_bill = id and w.waiver_status = 'APPROVED'),0)")
    private BigDecimal aggregateAmount;

    @Formula("((select coalesce(sum(coalesce(bip.amount_settled, 0)),0) from bill_item_payments bip inner join bill_items bi on bip.bill_item = bi.id where bi.patient_bill = id and bi.deleted <> 1))")
    private BigDecimal amountPaid;

    @Formula("(coalesce((select\n" +
            "            (select \n" +
            "                sum(case bi.bill_item_type \n" +
            "                    when 'Normal' then (bi.quantity * bi.unit_price) \n" +
            "                    when 'Accumulation' then DATEDIFF(coalesce(bi.date_to, now()), bi.date_from) * bi.unit_price \n" +
            "                end )\n" +
            "            from \n" +
            "                bill_items bi  \n" +
            "            where \n" +
            "                bi.patient_bill = id and bi.deleted <> 1 and (bi.bill_item_status is null or bi.bill_item_status <> 'CANCELLED'))),0) - coalesce((select sum(coalesce(bw.amount_waived, 0)) from bill_waivers bw inner join waivers w on bw.waiver = w.id where bw.patient_bill = id and w.waiver_status = 'APPROVED'),0) - " +
            "coalesce((select coalesce(sum(coalesce(bp.amount_settled, 0)),0) from bill_payments bp where bp.patient_bill = id),0) - " +
            "coalesce((select coalesce(sum(coalesce(bip.amount_settled, 0)),0) from bill_item_payments bip inner join bill_items bi on bip.bill_item = bi.id where bi.patient_bill = id),0) - " +
            "coalesce((select sum(coalesce(biw.amount_waived, 0)) from bill_item_waivers biw inner join waivers w on w.id = biw.waiver inner join bill_items bi on biw.bill_item = bi.id where bi.patient_bill = id and w.waiver_status = 'APPROVED'),0) )")
    private BigDecimal balance;

    @Column(name = "aggregate_tax")
    private BigDecimal aggregateTax;

    @Column(name = "bill_status")
    @Enumerated(EnumType.STRING)
    private BillStatus billStatus;

    @OneToMany(mappedBy = "patientBill", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY, orphanRemoval = true)
    @Filter(name = "filterByDeleted")
    private Collection<BillItem> billItems = new ArrayList<>();

    @OneToMany(mappedBy = "patientBill", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY, orphanRemoval = true)
    @Filter(name = "filterByDeleted")
    private Collection<BillPayment> billPayments = new ArrayList<>();

    @NotNull
    @Column(name = "bill_pmt_type")
    @Enumerated(EnumType.STRING)
    private BillPaymentType billPaymentType;

    @JoinColumn(name = "insurance_company", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private InsuranceCompany insuranceCompany;

    @Column(name = "discharge", columnDefinition = "tinyint default '0'")
    private Boolean discharge;

    public PatientBill(){}

    public PatientBill(Appointment appointment, Patient patient, Date dateGenerated, Date dueDate, Long billNo, BillPaymentType billPaymentType){
        setAppointment(appointment);
        setPatient(patient);
        setDateGenerated(dateGenerated);
        setDueDate(dueDate);
        setBillNo(billNo);
        setAmountWaived(BigDecimal.ZERO);
        setBillStatus(BillStatus.APPROVED);
        setBillPaymentType(billPaymentType);
        if(billPaymentType == BillPaymentType.CORPORATE) setInsuranceCompany(appointment.getInsuranceCompany());
    }

    @EntityCallbackMethod(when = During.DELETE)
    public void updateClosure(){
        throw new ValidationException("Deletion of bills NOT allowed!");
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public ServiceRequest getServiceRequest() {
        return serviceRequest;
    }

    public void setServiceRequest(ServiceRequest serviceRequest) {
        this.serviceRequest = serviceRequest;
    }

    public Date getDateGenerated() {
        return dateGenerated;
    }

    public void setDateGenerated(Date dateGenerated) {
        this.dateGenerated = dateGenerated;
    }

    public BillType getBillType() {
        return billType;
    }

    public void setBillType(BillType billType) {
        this.billType = billType;
    }

    public BigDecimal getAmountBilled() {
        return amountBilled;
    }

    public void setAmountBilled(BigDecimal amountBilled) {
        this.amountBilled = amountBilled;
    }

    public BigDecimal getAmountWaived() {
        return amountWaived;
    }

    public void setAmountWaived(BigDecimal amountWaived) {
        this.amountWaived = amountWaived;
    }

    public BigDecimal getAggregateAmount() {
        return aggregateAmount;
    }

    public void setAggregateAmount(BigDecimal aggregateAmount) {
        this.aggregateAmount = aggregateAmount;
    }

    public BigDecimal getAggregateTax() {
        return aggregateTax;
    }

    public void setAggregateTax(BigDecimal aggregateTax) {
        this.aggregateTax = aggregateTax;
    }

    public BillStatus getBillStatus() {
        return billStatus;
    }

    public void setBillStatus(BillStatus billStatus) {
        this.billStatus = billStatus;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public Collection<BillItem> getBillItems() {
        return billItems;
    }

    public void addBillItem(BillItem billItem) {
        if(billItem != null){
            billItem.setPatientBill(this);
            getBillItems().add(billItem);
        }
    }

    public void addBillItems(Collection<BillItem> billItems) {
        for(BillItem billItem : billItems){
            addBillItem(billItem);
        }
        recomputeTotal();
    }

    public void setBillItems(Set<BillItem> billItems) {
        this.billItems = billItems;
    }

    public Long getBillNo() {
        return billNo;
    }

    public void setBillNo(Long billNo) {
        this.billNo = billNo;
    }

    public BigDecimal getAmountPaid() {
        return amountPaid;
    }

    public void setAmountPaid(BigDecimal amountPaid) {
        this.amountPaid = amountPaid;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public Appointment getAppointment() {
        return appointment;
    }

    public void setAppointment(Appointment appointment) {
        this.appointment = appointment;
    }

    public void recomputeTotal() {
        BigDecimal total = BigDecimal.ZERO;
        BigDecimal tax = BigDecimal.ZERO;
        for(BillItem item : getBillItems()){
            total = total.add(item.getLineTotal());
            tax = tax.add(item.getTax());
        }
        setAmountBilled(total);
        setAggregateTax(tax);
    }

    public BillPaymentType getBillPaymentType() {
        return billPaymentType;
    }

    public void setBillPaymentType(BillPaymentType billPaymentType) {
        this.billPaymentType = billPaymentType;
    }

    public InsuranceCompany getInsuranceCompany() {
        return insuranceCompany;
    }

    public void setInsuranceCompany(InsuranceCompany insuranceCompany) {
        this.insuranceCompany = insuranceCompany;
    }

    public Collection<BillPayment> getBillPayments() {
        return billPayments;
    }

    public void setBillPayments(Collection<BillPayment> billPayments) {
        this.billPayments = billPayments;
    }

    public Boolean getDischarge() {
        return discharge;
    }

    public void setDischarge(Boolean discharge) {
        this.discharge = discharge;
    }
}
