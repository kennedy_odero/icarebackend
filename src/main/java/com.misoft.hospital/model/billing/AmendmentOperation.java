package com.misoft.hospital.model.billing;

/**
 * Created by kodero on 6/7/16.
 */
public enum AmendmentOperation {
    REMOVE,
    UPDATE
}
