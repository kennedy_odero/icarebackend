package com.misoft.hospital.model.billing;

import com.misoft.hospital.model.accounts.CashierShift;
import com.misoft.hospital.model.accounts.PaymentMode;
import com.misoft.hospital.model.accounts.ShiftStatus;
import com.misoft.hospital.model.base.ModelBase;
import com.misoft.hospital.model.healthcare.Patient;
import com.misoft.hospital.model.setup.Staff;
import com.misoft.hospital.service.billing.PatientPaymentEntityCallback;
import com.misoft.hospital.util.callbacks.EntityCallbackClass;
import com.misoft.hospital.util.validation.ValidateAt;
import com.misoft.hospital.util.validation.ValidationMethod;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.Formula;

import javax.persistence.*;
import javax.validation.ValidationException;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 * Created by kodero on 9/13/15.
 */
@Entity
@Table(name = "patient_payments")
@Filter(name ="filterByDeleted")
@EntityCallbackClass(PatientPaymentEntityCallback.class)
public class PatientPayment extends ModelBase{

    @Column(name = "payment_no")
    private Long paymentNo;

    @NotNull
    @JoinColumn(name = "patient", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Patient patient;

    @NotNull
    @Column(name = "date_paid")
    private Date datePaid;

    @NotNull
    @Column(name = "amount_paid")
    private BigDecimal amountPaid = BigDecimal.ZERO;

    @Column(name = "amount_refunded")
    private BigDecimal refund = BigDecimal.ZERO;

    @Formula("(coalesce(amount_paid,0) - coalesce(amount_refunded,0))")
    private BigDecimal effectivePaid;

    @Formula("(select coalesce(sum(coalesce(bp.amount_settled, 0)),0) from bill_payments bp where bp.patient_payment = id and bp.deleted <> 1)")
    private BigDecimal allocated;

    @Formula("(coalesce(amount_paid,0) " +
            "   - (select coalesce(sum(coalesce(bp.amount_settled, 0)),0) from bill_payments bp where bp.patient_payment = id and bp.deleted <> 1) " +
            "   - (select coalesce(sum(coalesce(pr.amount_refunded, 0)),0) from payment_refunds pr inner join refunds r on pr.patient_refund = r.id " +
            "       where r.refund_status in ('NEW', 'SUBMITTED', 'APPROVED') and pr.patient_payment = id and pr.deleted <> 1))")
    private BigDecimal unallocated;

    @Column(name = "notes")
    private String notes;

    @Column(name = "payment_type")
    private PaymentType paymentType;

    @Column(name = "payment_mode")
    @Enumerated(EnumType.STRING)
    private PaymentMode paymentMode;

    @OneToMany(mappedBy = "patientPayment", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @Filter(name = "filterByDeleted")
    private Collection<BillPayment> billPayments;

    @OneToMany(mappedBy = "patientPayment", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @Filter(name = "filterByDeleted")
    private Collection<BillItemPayment> billItemPayments;

    @OneToMany(mappedBy = "patientPayment", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @Filter(name = "filterByDeleted")
    private Collection<PaymentRevision> paymentRevisions = new ArrayList<>();

    @NotNull
    @JoinColumn(name = "received_by", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Staff receivedBy;

    @NotNull
    @JoinColumn(name = "shift", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private CashierShift cashierShift;

    @Column(name = "tendered")
    private BigDecimal tendered;

    @Column(name = "_change")
    private BigDecimal change;

    @ValidationMethod(when = {ValidateAt.CREATE, ValidateAt.UPDATE})
    public void validateAmountTendered(){
        System.out.print("***************getAmountPaid() : " + getAmountPaid());
        if(BigDecimal.ZERO.compareTo(getTendered()) == 0)
            throw new ValidationException("Amount tendered cannot be zero!");

        if(this.cashierShift == null)
            throw new ValidationException("Please start a shift in order to receive money!");
    }

    @ValidationMethod(when = {ValidateAt.UPDATE})
    public void validateUpdatingPaymentWhenShiftIsClosed(){
        throw new ValidationException("Receipt update not allowed!");
        /*System.out.print("***************getAmountPaid() : " + getAmountPaid());
        if(getCashierShift().getStatus() == ShiftStatus.CLOSED)
            throw new ValidationException("Shift already closed!");*/
    }

    @ValidationMethod(when = {ValidateAt.DELETE})
    public void validatePaymentDeletion(){
        throw new ValidationException("Receipt deletion not allowed!");
    }

    public Collection<BillItemPayment> getBillItemPayments() {
        return billItemPayments;
    }

    public void setBillItemPayments(Collection<BillItemPayment> billItemPayments) {
        this.billItemPayments = billItemPayments;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Date getDatePaid() {
        return datePaid;
    }

    public void setDatePaid(Date datePaid) {
        this.datePaid = datePaid;
    }

    public BigDecimal getAmountPaid() {
        return amountPaid;
    }

    public void setAmountPaid(BigDecimal amountPaid) {
        this.amountPaid = amountPaid;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public PaymentType getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(PaymentType paymentType) {
        this.paymentType = paymentType;
    }

    public Collection<BillPayment> getBillPayments() {
        return billPayments;
    }

    public void setBillPayments(Collection<BillPayment> billPayments) {
        this.billPayments = billPayments;
    }

    public Staff getReceivedBy() {
        return receivedBy;
    }

    public void setReceivedBy(Staff receivedBy) {
        this.receivedBy = receivedBy;
    }

    public PaymentMode getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(PaymentMode paymentMode) {
        this.paymentMode = paymentMode;
    }

    public BigDecimal getAllocated() {
        return allocated;
    }

    public void setAllocated(BigDecimal allocated) {
        this.allocated = allocated;
    }

    public BigDecimal getUnallocated() {
        return unallocated;
    }

    public void setUnallocated(BigDecimal unallocated) {
        this.unallocated = unallocated;
    }

    public Long getPaymentNo() {
        return paymentNo;
    }

    public void setPaymentNo(Long paymentNo) {
        this.paymentNo = paymentNo;
    }

    public CashierShift getCashierShift() {
        return cashierShift;
    }

    public void setCashierShift(CashierShift cashierShift) {
        this.cashierShift = cashierShift;
    }

    public BigDecimal getChange() {
        return change;
    }

    public void setChange(BigDecimal change) {
        this.change = change;
    }

    public BigDecimal getTendered() {
        return tendered;
    }

    public void setTendered(BigDecimal tendered) {
        this.tendered = tendered;
    }

    public Collection<PaymentRevision> getPaymentRevisions() {
        return paymentRevisions;
    }

    public void setPaymentRevisions(Collection<PaymentRevision> paymentRevisions) {
        this.paymentRevisions = paymentRevisions;
    }

    public BigDecimal getRefund() {
        return refund;
    }

    public void setRefund(BigDecimal refund) {
        this.refund = refund;
    }

    public BigDecimal getEffectivePaid() {
        return effectivePaid;
    }

    public void setEffectivePaid(BigDecimal effectivePaid) {
        this.effectivePaid = effectivePaid;
    }
}
