package com.misoft.hospital.model.billing;

import com.misoft.hospital.model.admin.User;
import com.misoft.hospital.model.base.ModelBase;
import com.misoft.hospital.util.validation.ValidationMethod;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import javax.validation.ValidationException;
import javax.validation.constraints.NotNull;

/**
 * Created by kodero on 6/7/16.
 */
@Entity
@Table(name = "bill_item_amendments")
@Filter(name = "filterByDeleted")
public class BillItemAmendment extends ModelBase{

    @NotNull
    @JoinColumn(name = "bill_item", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private BillItem billItem;

    @NotNull
    @JoinColumn(name = "bill", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private PatientBill patientBill;

    @NotNull
    @Column(name = "operation")
    @Enumerated(EnumType.STRING)
    private AmendmentOperation amendmentOperation;

    @JoinColumn(name = "created_by", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private User createBy;

    @NotNull
    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private AmendmentStatus amendmentStatus;

    @NotNull
    @Column(name = "reason")
    private String reason;

    @Column(name = "new_qty")
    private Integer newQty;

    @ValidationMethod
    public void validateOperation(){
        System.out.println("New Quantity : " + getNewQty() + ", Old Qty : " + getBillItem().getQuantity().intValue());
        if (getAmendmentOperation() == AmendmentOperation.UPDATE){
            if(getNewQty() == null) throw new ValidationException("Please provide the new quantity for the update operation!");
            if(getNewQty() < 1 || getNewQty() >= getBillItem().getQuantity().intValue())
                throw new ValidationException("The new quantity must be between 1 and " + getBillItem().getQuantity().intValue() + "!");
        }
    }

    public BillItem getBillItem() {
        return billItem;
    }

    public void setBillItem(BillItem billItem) {
        this.billItem = billItem;
    }

    public AmendmentOperation getAmendmentOperation() {
        return amendmentOperation;
    }

    public void setAmendmentOperation(AmendmentOperation amendmentOperation) {
        this.amendmentOperation = amendmentOperation;
    }

    public User getCreateBy() {
        return createBy;
    }

    public void setCreateBy(User createBy) {
        this.createBy = createBy;
    }

    public AmendmentStatus getAmendmentStatus() {
        return amendmentStatus;
    }

    public void setAmendmentStatus(AmendmentStatus amendmentStatus) {
        this.amendmentStatus = amendmentStatus;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Integer getNewQty() {
        return newQty;
    }

    public void setNewQty(Integer newQty) {
        this.newQty = newQty;
    }

    public PatientBill getPatientBill() {
        return patientBill;
    }

    public void setPatientBill(PatientBill patientBill) {
        this.patientBill = patientBill;
    }
}
