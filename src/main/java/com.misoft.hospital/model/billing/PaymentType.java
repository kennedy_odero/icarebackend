package com.misoft.hospital.model.billing;

/**
 * Created by kodero on 9/13/15.
 */
public enum PaymentType {
    BILL,
    DEPOSIT
}
