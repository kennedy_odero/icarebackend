package com.misoft.hospital.model.billing;

/**
 * Created by kodero on 6/12/16.
 */
public enum IssueStatus {
    NOT_ISSUED,
    ISSUED
}
