package com.misoft.hospital.model.billing;

import com.misoft.hospital.model.base.ModelBase;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * Created by kodero on 12/16/15.
 */
@Entity
@Table(name = "payment_refunds")
public class PaymentRefund extends ModelBase{
    @JoinColumn(name = "patient_payment", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private PatientPayment patientPayment;

    @JoinColumn(name = "patient_refund", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Refund refund;

    @NotNull
    @Column(name = "amount_refunded")
    private BigDecimal amountRefunded;

    public PaymentRefund(){

    }

    public PaymentRefund(PatientPayment patientPayment, BigDecimal amountRefunded){
        setPatientPayment(patientPayment);
        setAmountRefunded(amountRefunded);
    }

    public PatientPayment getPatientPayment() {
        return patientPayment;
    }

    public void setPatientPayment(PatientPayment patientPayment) {
        this.patientPayment = patientPayment;
    }

    public Refund getRefund() {
        return refund;
    }

    public void setRefund(Refund refund) {
        this.refund = refund;
    }

    public BigDecimal getAmountRefunded() {
        return amountRefunded;
    }

    public void setAmountRefunded(BigDecimal amountRefunded) {
        this.amountRefunded = amountRefunded;
    }
}
