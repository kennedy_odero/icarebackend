package com.misoft.hospital.model.billing;

import com.misoft.hospital.model.base.ModelBase;
import com.misoft.hospital.model.healthcare.Patient;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

/**
 * Created by kodero on 12/16/15.
 */
@Entity
@Table(name = "patient_credits")
public class PatientCredit extends ModelBase{
    @Column(name = "credit_no")
    private Long creditNo;

    @NotNull
    @JoinColumn(name = "patient", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Patient patient;

    @Column(name = "date_created")
    private Date dateCreated;

    @Column(name = "notes")
    private String notes;

    @Column(name = "amount")
    private BigDecimal amount;

    @OneToMany(mappedBy = "patientCredit", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @Filter(name = "filterByDeleted")
    private Collection<BillCredit> billCredits;

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Collection<BillCredit> getBillCredits() {
        return billCredits;
    }

    public void setBillCredits(Collection<BillCredit> billCredits) {
        this.billCredits = billCredits;
    }

    public Long getCreditNo() {
        return creditNo;
    }

    public void setCreditNo(Long creditNo) {
        this.creditNo = creditNo;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
