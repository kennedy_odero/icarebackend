package com.misoft.hospital.model.billing;

/**
 * Created by kodero on 5/19/16.
 */
public enum RefundStatus {
    NEW,
    CANCELLED,
    SUBMITTED,
    APPROVED,
    REJECTED
}
