package com.misoft.hospital.model.preferences;

/**
 * Created by kodero on 10/27/15.
 */
public enum PreferenceEditor {
    Textfield,
    Textarea,
    Select,
    Numeric,
    Number,
    Multiselect,
    EntitySelect,
    Checkbox
}
