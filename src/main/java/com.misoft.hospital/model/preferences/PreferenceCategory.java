package com.misoft.hospital.model.preferences;

import com.misoft.hospital.model.base.ModelBase;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by kodero on 10/27/15.
 */
@Entity
@Table(name = "pref_categories")
public class PreferenceCategory extends ModelBase{

    @Column(name = "name")
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
