package com.misoft.hospital.model.preferences;

/**
 * Created by kodero on 10/27/15.
 */
public enum PreferenceVisibility {
    SYSTEM,
    USER
}
