package com.misoft.hospital.model.preferences;

import com.misoft.hospital.model.base.ModelBase;

import javax.persistence.*;

/**
 * Created by kodero on 10/27/15.
 */
@Entity
@Table(name = "preference_options")
public class PreferenceOption extends ModelBase{

    @Column(name = "label")
    private String label;

    @Column(name = "value")
    private String value;

    @JoinColumn(name = "definition", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private PreferenceDefinition preferenceDefinition;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public PreferenceDefinition getPreferenceDefinition() {
        return preferenceDefinition;
    }

    public void setPreferenceDefinition(PreferenceDefinition preferenceDefinition) {
        this.preferenceDefinition = preferenceDefinition;
    }
}
