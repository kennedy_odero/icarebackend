package com.misoft.hospital.model.preferences;

import com.misoft.hospital.data.gdto.JsonContent;
import com.misoft.hospital.data.gdto.types.AbstractType;
import com.misoft.hospital.data.gdto.types.StringType;
import com.misoft.hospital.model.admin.User;
import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Formula;

import javax.persistence.*;

/**
 * Created by kodero on 10/27/15.
 */
@Entity
@Table(name = "preference_values")
@NamedQuery(name = "getPreferenceByCode", query = "select pv from PreferenceValue pv where pv.preferenceDefinition.key =:prefCode")
public class PreferenceValue extends ModelBase{

    @JsonContent
    @Column(name = "_value")
    private String value;

    @JoinColumn(name = "definition", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private PreferenceDefinition preferenceDefinition;

    @JoinColumn(name = "user", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private User user;//to take of user preferences

    @Formula("(select pd._key from preference_defs pd where definition = pd.id)")
    private String key;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public PreferenceDefinition getPreferenceDefinition() {
        return preferenceDefinition;
    }

    public void setPreferenceDefinition(PreferenceDefinition preferenceDefinition) {
        this.preferenceDefinition = preferenceDefinition;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public <S, T extends AbstractType<S>> S getTypedValue(T t) {
        t.instantiateFromString(getValue());
        return t.getValue();
    }
}
