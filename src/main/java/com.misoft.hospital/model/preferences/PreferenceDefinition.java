package com.misoft.hospital.model.preferences;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by kodero on 10/27/15.
 */
@Entity
@Table(name = "preference_defs")
public class PreferenceDefinition extends ModelBase{

    @Column(name = "_key", unique = true)
    private String key;

    @Column(name = "visibility")
    @Enumerated(EnumType.STRING)
    private PreferenceVisibility preferenceVisibility;

    @Column(name = "editor")
    @Enumerated(EnumType.STRING)
    private PreferenceEditor preferenceEditor;

    @OneToMany(mappedBy = "preferenceDefinition", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @Filter(name = "filterByDeleted")
    private Collection<PreferenceOption> preferenceOptions;

    @Column(name = "option_values", columnDefinition = "text")
    private String optionValues;

    @Column(name = "input_pattern")
    private String inputPattern;

    @JoinColumn(name = "category", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private PreferenceCategory preferenceCategory;

    @Column(name = "entity")
    private String entity;

    @Column(name = "value_iterator")
    private String valueIterator;

    @Column(name = "_view")
    private String view;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public PreferenceVisibility getPreferenceVisibility() {
        return preferenceVisibility;
    }

    public void setPreferenceVisibility(PreferenceVisibility preferenceVisibility) {
        this.preferenceVisibility = preferenceVisibility;
    }

    public PreferenceEditor getPreferenceEditor() {
        return preferenceEditor;
    }

    public void setPreferenceEditor(PreferenceEditor preferenceEditor) {
        this.preferenceEditor = preferenceEditor;
    }

    public Collection<PreferenceOption> getPreferenceOptions() {
        return preferenceOptions;
    }

    public void setPreferenceOptions(Collection<PreferenceOption> preferenceOptions) {
        this.preferenceOptions = preferenceOptions;
    }

    public String getInputPattern() {
        return inputPattern;
    }

    public void setInputPattern(String inputPattern) {
        this.inputPattern = inputPattern;
    }

    public PreferenceCategory getPreferenceCategory() {
        return preferenceCategory;
    }

    public void setPreferenceCategory(PreferenceCategory preferenceCategory) {
        this.preferenceCategory = preferenceCategory;
    }

    public String getOptionValues() {
        return optionValues;
    }

    public void setOptionValues(String optionValues) {
        this.optionValues = optionValues;
    }

    public String getEntity() {
        return entity;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }

    public String getValueIterator() {
        return valueIterator;
    }

    public void setValueIterator(String valueIterator) {
        this.valueIterator = valueIterator;
    }

    public String getView() {
        return view;
    }

    public void setView(String view) {
        this.view = view;
    }
}
