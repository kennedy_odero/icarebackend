package com.misoft.hospital.model.cssd;

import com.misoft.hospital.model.base.ModelBase;
import com.misoft.hospital.model.setup.Product;
import org.hibernate.annotations.Filter;

import javax.persistence.*;

/**
 * Created by kodero on 3/29/15.
 */
@Entity
@Table(name = "sop_supplies")
@Filter(name = "filterByDeleted")
public class SOPSupplies extends ModelBase {

    @JoinColumn(name = "product", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Product product;

    @JoinColumn(name = "sop", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private SOP sop;

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public SOP getSop() {
        return sop;
    }

    public void setSop(SOP sop) {
        this.sop = sop;
    }
}
