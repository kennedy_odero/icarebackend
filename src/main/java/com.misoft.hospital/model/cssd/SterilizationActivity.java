package com.misoft.hospital.model.cssd;

import com.misoft.hospital.model.base.ModelBase;
import com.misoft.hospital.model.setup.Staff;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;

/**
 * Created by kodero on 3/29/15.
 */
@Entity
@Table(name = "sterilization_activity")
@Filter(name = "filterByDeleted")
public class SterilizationActivity extends ModelBase {

    @Column(name = "date_started")
    private Date dateStarted;

    @Column(name = "date_completed")
    private Date dateCompleted;

    @JoinColumn(name = "performed_by", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Staff performedBy;

    @Column(name = "type")
    @Enumerated(EnumType.STRING)
    private SterilizationType sterilizationType;//[Equipment, Article]

    @OneToMany(mappedBy = "sterilizationActivity", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @Filter(name = "filterByDeleted")
    private Collection<SterilizationEquipment> sterilizationEquipments;

    @OneToMany(mappedBy = "activity", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @Filter(name = "filterByDeleted")
    private Collection<SterilizationArticle> sterilizationArticles;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private SterilizationStatus status;

    public Date getDateStarted() {
        return dateStarted;
    }

    public void setDateStarted(Date dateStarted) {
        this.dateStarted = dateStarted;
    }

    public Date getDateCompleted() {
        return dateCompleted;
    }

    public void setDateCompleted(Date dateCompleted) {
        this.dateCompleted = dateCompleted;
    }

    public Staff getPerformedBy() {
        return performedBy;
    }

    public void setPerformedBy(Staff performedBy) {
        this.performedBy = performedBy;
    }

    public SterilizationType getSterilizationType() {
        return sterilizationType;
    }

    public void setSterilizationType(SterilizationType sterilizationType) {
        this.sterilizationType = sterilizationType;
    }

    public Collection<SterilizationEquipment> getSterilizationEquipments() {
        return sterilizationEquipments;
    }

    public void setSterilizationEquipments(Collection<SterilizationEquipment> sterilizationEquipments) {
        this.sterilizationEquipments = sterilizationEquipments;
    }

    public Collection<SterilizationArticle> getSterilizationArticles() {
        return sterilizationArticles;
    }

    public void setSterilizationArticles(Collection<SterilizationArticle> sterilizationArticles) {
        this.sterilizationArticles = sterilizationArticles;
    }

    public SterilizationStatus getStatus() {
        return status;
    }

    public void setStatus(SterilizationStatus status) {
        this.status = status;
    }
}
