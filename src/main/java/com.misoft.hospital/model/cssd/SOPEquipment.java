package com.misoft.hospital.model.cssd;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.*;

/**
 * Created by kodero on 3/29/15.
 */
@Entity
@Table(name = "sop_equipment")
@Filter(name = "filterByDeleted")
public class SOPEquipment extends ModelBase {

    @JoinColumn(name = "equipment", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Equipment equipment;

    @JoinColumn(name = "sop", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private SOP sop;

    public Equipment getEquipment() {
        return equipment;
    }

    public void setEquipment(Equipment equipment) {
        this.equipment = equipment;
    }

    public SOP getSop() {
        return sop;
    }

    public void setSop(SOP sop) {
        this.sop = sop;
    }
}
