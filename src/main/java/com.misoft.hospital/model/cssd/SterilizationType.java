package com.misoft.hospital.model.cssd;

/**
 * Created by kodero on 3/29/15.
 */
public enum SterilizationType {
    ARTICLE,
    EQUIPMENT
}
