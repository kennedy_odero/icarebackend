package com.misoft.hospital.model.cssd;

import com.misoft.hospital.model.base.ModelBase;
import com.misoft.hospital.model.setup.Product;
import org.hibernate.annotations.Filter;

import javax.persistence.*;

/**
 * Created by kodero on 3/27/15.
 */
@Entity
@Table(name = "cssd_equipment")
@Filter(name = "filterByDeleted")
public class Equipment extends ModelBase {

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @JoinColumn(name = "equipment_category", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private EquipmentCategory equipmentCategory;

    @JoinColumn(name = "product", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Product product;

    @JoinColumn(name = "maintenance_cycle", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Period maintenanceCycle;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private EquipmentStatus status;

    @Column(name = "retired")
    private Boolean retired;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public EquipmentCategory getEquipmentCategory() {
        return equipmentCategory;
    }

    public void setEquipmentCategory(EquipmentCategory equipmentCategory) {
        this.equipmentCategory = equipmentCategory;
    }

    public Period getMaintenanceCycle() {
        return maintenanceCycle;
    }

    public void setMaintenanceCycle(Period maintenanceCycle) {
        this.maintenanceCycle = maintenanceCycle;
    }

    public EquipmentStatus getStatus() {
        return status;
    }

    public void setStatus(EquipmentStatus status) {
        this.status = status;
    }

    public Boolean getRetired() {
        return retired;
    }

    public void setRetired(Boolean retired) {
        this.retired = retired;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
