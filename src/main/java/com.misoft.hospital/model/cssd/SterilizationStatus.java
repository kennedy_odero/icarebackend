package com.misoft.hospital.model.cssd;

/**
 * Created by kodero on 3/29/15.
 */
public enum SterilizationStatus {
    SCHEDULED,
    IN_PROGRESS,
    ON_HOLD,
    COMPLETED,
    CANCELLED
}
