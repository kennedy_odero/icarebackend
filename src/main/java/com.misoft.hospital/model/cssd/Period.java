package com.misoft.hospital.model.cssd;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by kodero on 3/27/15.
 */
@Entity
@Table(name = "periods")
@Filter(name = "filterByDeleted")
public class Period extends ModelBase {

    @Column(name = "name")
    private String name;

    @Column(name = "symbol")
    private String symbol;

    @Column(name = "multiplier")
    private BigDecimal multiplier;

    @JoinColumn(name = "base_period", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Period base;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public BigDecimal getMultiplier() {
        return multiplier;
    }

    public void setMultiplier(BigDecimal multiplier) {
        this.multiplier = multiplier;
    }

    public Period getBase() {
        return base;
    }

    public void setBase(Period base) {
        this.base = base;
    }
}
