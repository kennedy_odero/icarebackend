package com.misoft.hospital.model.cssd;

/**
 * Created by kodero on 3/27/15.
 */
public enum EquipmentStatus {
    OPERATIONAL,
    DEFECTIVE,
    RETIRED
}
