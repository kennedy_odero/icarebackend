package com.misoft.hospital.model.cssd;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.*;

/**
 * Created by kodero on 3/29/15.
 */
@Entity
@Table(name = "sterilization_equipment")
@Filter(name = "filterByDeleted")
public class SterilizationEquipment extends ModelBase {

    @JoinColumn(name = "equipment", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Equipment equipment;

    @JoinColumn(name = "activity", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private SterilizationActivity sterilizationActivity;

    public Equipment getEquipment() {
        return equipment;
    }

    public void setEquipment(Equipment equipment) {
        this.equipment = equipment;
    }

    public SterilizationActivity getSterilizationActivity() {
        return sterilizationActivity;
    }

    public void setSterilizationActivity(SterilizationActivity sterilizationActivity) {
        this.sterilizationActivity = sterilizationActivity;
    }
}
