package com.misoft.hospital.model.cssd;

import com.misoft.hospital.model.accounts.Creditor;
import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by kodero on 3/27/15.
 */
@Entity
@Table(name = "service_work_orders")
@Filter(name = "filterByDeleted")
public class ServiceWorkOrder extends ModelBase {

    @Column(name = "order_no")
    private Long orderNo;

    @Column(name = "date_requisitioned")
    private Date dateRequisitioned;
    //private Asset asset;

    @JoinColumn(name = "supplier", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Creditor creditor;

    @Column(name = "comments")
    private String comments;

    public Date getDateRequisitioned() {
        return dateRequisitioned;
    }

    public void setDateRequisitioned(Date dateRequisitioned) {
        this.dateRequisitioned = dateRequisitioned;
    }

    public Creditor getCreditor() {
        return creditor;
    }

    public void setCreditor(Creditor creditor) {
        this.creditor = creditor;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Long getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(Long orderNo) {
        this.orderNo = orderNo;
    }
}
