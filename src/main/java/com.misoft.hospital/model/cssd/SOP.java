package com.misoft.hospital.model.cssd;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;

/**
 * Created by kodero on 3/29/15.
 */
@Entity
@Table(name = "sops")
@Filter(name = "filterByDeleted")
public class SOP extends ModelBase {

    @Column(name = "sop_no")
    private Long sopNo;

    @Column(name = "title")
    private String title;

    @Column(name = "review_date")
    private Date reviewDate;

    @Column(name = "prepared_by")
    private String preparedBy;

    @Column(name = "area_of_appl")
    private String areasOfApplication;

    @Column(name = "staff_involved")
    private String staffInvolved;

    @Column(name = "objective")
    private String objective;

    @Column(name = "related_docs")
    private String relatedDocs;

    @OneToMany(mappedBy = "sop", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @Filter(name = "filterByDeleted")
    private Collection<SOPEquipment> sopEquipments;

    @OneToMany(mappedBy = "sop", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @Filter(name = "filterByDeleted")
    private Collection<SOPSupplies> sopSupplies;

    @Column(name = "_procedure")
    private String procedure;

    public String getProcedure() {
        return procedure;
    }

    public void setProcedure(String procedure) {
        this.procedure = procedure;
    }

    public Long getSopNo() {
        return sopNo;
    }

    public void setSopNo(Long sopNo) {
        this.sopNo = sopNo;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getReviewDate() {
        return reviewDate;
    }

    public void setReviewDate(Date reviewDate) {
        this.reviewDate = reviewDate;
    }

    public String getPreparedBy() {
        return preparedBy;
    }

    public void setPreparedBy(String preparedBy) {
        this.preparedBy = preparedBy;
    }

    public String getAreasOfApplication() {
        return areasOfApplication;
    }

    public void setAreasOfApplication(String areasOfApplication) {
        this.areasOfApplication = areasOfApplication;
    }

    public String getStaffInvolved() {
        return staffInvolved;
    }

    public void setStaffInvolved(String staffInvolved) {
        this.staffInvolved = staffInvolved;
    }

    public String getObjective() {
        return objective;
    }

    public void setObjective(String objective) {
        this.objective = objective;
    }

    public String getRelatedDocs() {
        return relatedDocs;
    }

    public void setRelatedDocs(String relatedDocs) {
        this.relatedDocs = relatedDocs;
    }

    public Collection<SOPEquipment> getSopEquipments() {
        return sopEquipments;
    }

    public void setSopEquipments(Collection<SOPEquipment> sopEquipments) {
        this.sopEquipments = sopEquipments;
    }

    public Collection<SOPSupplies> getSopSupplies() {
        return sopSupplies;
    }

    public void setSopSupplies(Collection<SOPSupplies> sopSupplies) {
        this.sopSupplies = sopSupplies;
    }
}
