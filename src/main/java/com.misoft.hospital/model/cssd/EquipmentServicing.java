package com.misoft.hospital.model.cssd;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by kodero on 3/27/15.
 */
@Entity
@Table(name = "equipment_servicing")
@Filter(name = "filterByDeleted")
public class EquipmentServicing extends ModelBase {

    @Column(name = "date_of_service")
    private Date dateOfService;

    @JoinColumn(name = "equipment", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Equipment equipment;

    @Column(name = "parts_serviced",columnDefinition = "text")
    private String partsServiced;

    @JoinColumn(name = "work_order", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private ServiceWorkOrder serviceWorkOrder;

    @Column(name = "service_type")
    @Enumerated(EnumType.STRING)
    private ServiceType serviceType;

    @Column(name = "next_service")
    private Date nextService;

    @Column(name = "service_comments", columnDefinition = "text")
    private String serviceComments;

    public Date getDateOfService() {
        return dateOfService;
    }

    public void setDateOfService(Date dateOfService) {
        this.dateOfService = dateOfService;
    }

    public Equipment getEquipment() {
        return equipment;
    }

    public void setEquipment(Equipment equipment) {
        this.equipment = equipment;
    }

    public String getPartsServiced() {
        return partsServiced;
    }

    public void setPartsServiced(String partsServiced) {
        this.partsServiced = partsServiced;
    }

    public ServiceWorkOrder getServiceWorkOrder() {
        return serviceWorkOrder;
    }

    public void setServiceWorkOrder(ServiceWorkOrder serviceWorkOrder) {
        this.serviceWorkOrder = serviceWorkOrder;
    }

    public ServiceType getServiceType() {
        return serviceType;
    }

    public void setServiceType(ServiceType serviceType) {
        this.serviceType = serviceType;
    }

    public Date getNextService() {
        return nextService;
    }

    public void setNextService(Date nextService) {
        this.nextService = nextService;
    }

    public String getServiceComments() {
        return serviceComments;
    }

    public void setServiceComments(String serviceComments) {
        this.serviceComments = serviceComments;
    }
}
