package com.misoft.hospital.model.cssd;

import com.misoft.hospital.model.base.ModelBase;
import com.misoft.hospital.model.setup.Product;
import org.hibernate.annotations.Filter;

import javax.persistence.*;

/**
 * Created by kodero on 3/29/15.
 */
@Entity
@Table(name = "sterilization_articles")
@Filter(name = "filterByDeleted")
public class SterilizationArticle extends ModelBase {

    @JoinColumn(name = "product", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Product product;

    @JoinColumn(name = "activity", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private SterilizationActivity activity;

    @Column(name = "qty")
    private Integer quantity;

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public SterilizationActivity getActivity() {
        return activity;
    }

    public void setActivity(SterilizationActivity activity) {
        this.activity = activity;
    }
}
