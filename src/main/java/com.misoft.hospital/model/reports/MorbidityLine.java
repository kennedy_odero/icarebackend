package com.misoft.hospital.model.reports;

import com.misoft.hospital.model.base.ModelBase;
import com.misoft.hospital.model.setup.Disease;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by kodero on 7/26/16.
 */
@Entity
@Table(name ="morbidity_lines")
@Filter(name = "filterByDeleted")
public class MorbidityLine extends ModelBase{

    @NotNull
    @JoinColumn(name = "disease", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Disease disease;

    @JoinColumn(name = "morbidity_header", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private MorbidityHeader morbidityHeader;

    public Disease getDisease() {
        return disease;
    }

    public void setDisease(Disease disease) {
        this.disease = disease;
    }

    public MorbidityHeader getMorbidityHeader() {
        return morbidityHeader;
    }

    public void setMorbidityHeader(MorbidityHeader morbidityHeader) {
        this.morbidityHeader = morbidityHeader;
    }
}
