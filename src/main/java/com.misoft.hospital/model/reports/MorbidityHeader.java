package com.misoft.hospital.model.reports;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.Formula;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by kodero on 7/26/16.
 */
@Entity
@Table(name = "morbidity_headers")
public class MorbidityHeader extends ModelBase{

    @NotNull
    @Column(name = "name")
    private String name;

    @NotNull
    @Column(name = "_index", columnDefinition = "mediumint default '0'")
    private int index;

    @Column(name = "description")
    private String description;

    @OneToMany(mappedBy = "morbidityHeader", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @Filter(name = "filterByDeleted")
    private Collection<MorbidityLine> morbidityLines = new ArrayList<>();

    @Formula("(select count(ml.id) from morbidity_lines ml where ml.morbidity_header = id)")
    private BigInteger lines;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Collection<MorbidityLine> getMorbidityLines() {
        return morbidityLines;
    }

    public void setMorbidityLines(Collection<MorbidityLine> morbidityLines) {
        this.morbidityLines = morbidityLines;
    }

    public BigInteger getLines() {
        return lines;
    }

    public void setLines(BigInteger lines) {
        this.lines = lines;
    }
}
