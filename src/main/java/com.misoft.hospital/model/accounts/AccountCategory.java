package com.misoft.hospital.model.accounts;

/**
 * Created by larry on 3/6/2015.
 */
public enum AccountCategory {
    ASSET,
    LIABILITY,
    REVENUE,
    EXPENSE,
    CAPITAL
}
