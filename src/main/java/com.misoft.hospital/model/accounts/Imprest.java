package com.misoft.hospital.model.accounts;

import com.misoft.hospital.model.base.ModelBase;
import com.misoft.hospital.model.shared.YesNo;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 * Created by larry on 3/30/2015.
 */
@Entity
@Table(name = "imprests")
@Filter(name = "filterByDeleted")
public class Imprest extends ModelBase {

    @Column(name = "imprest_date")
    @Temporal(TemporalType.DATE)
    private Date imprestDate;

    @Column(name = "amount")
    private BigDecimal amount;

    @Column(name = "imprest_bbf")
    private BigDecimal bbf;

    @Column(name = "posted")
    @Enumerated(EnumType.STRING)
    private YesNo posted;

    @Column(name = "imprest_number")
    private String imprestNumber;

    @ManyToOne
    private Account glaccount;

    @ManyToOne
    private Account bnkToDbt;

    @OneToMany(mappedBy = "imprest", cascade={CascadeType.ALL})
    private Collection<Payment> payments = new ArrayList<>();

    public Date getImprestDate() {
        return imprestDate;
    }

    public void setImprestDate(Date imprestDate) {
        this.imprestDate = imprestDate;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getBbf() {
        return bbf;
    }

    public void setBbf(BigDecimal bbf) {
        this.bbf = bbf;
    }

    public YesNo getPosted() {
        return posted;
    }

    public void setPosted(YesNo posted) {
        this.posted = posted;
    }

    public String getImprestNumber() {
        return imprestNumber;
    }

    public void setImprestNumber(String imprestNumber) {
        this.imprestNumber = imprestNumber;
    }

    public Account getGlaccount() {
        return glaccount;
    }

    public void setGlaccount(Account glaccount) {
        this.glaccount = glaccount;
    }

    public Account getBnkToDbt() {
        return bnkToDbt;
    }

    public void setBnkToDbt(Account bnkToDbt) {
        this.bnkToDbt = bnkToDbt;
    }

    public Collection<Payment> getPayments() {
        return payments;
    }

    public void setPayments(Collection<Payment> payments) {
        this.payments = payments;
    }
}
