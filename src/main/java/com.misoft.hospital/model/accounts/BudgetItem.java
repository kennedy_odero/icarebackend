package com.misoft.hospital.model.accounts;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.*;

/**
 * Created by larry on 3/22/2015.
 */
@Entity
@Table(name = "budget_items")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "budget_item_type")
@Filter(name = "filterByDeleted")
public class BudgetItem extends ModelBase {
    @Column(name = "code", length = 20, nullable = false)
    private String code;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @ManyToOne
    private BudgetHeader hdr;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BudgetHeader getHdr() {
        return hdr;
    }

    public void setHdr(BudgetHeader hdr) {
        this.hdr = hdr;
    }
}
