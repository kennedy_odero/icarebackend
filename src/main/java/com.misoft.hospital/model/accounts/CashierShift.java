package com.misoft.hospital.model.accounts;

import com.misoft.hospital.model.base.ModelBase;
import com.misoft.hospital.model.setup.Staff;
import com.misoft.hospital.util.callbacks.During;
import com.misoft.hospital.util.callbacks.EntityCallbackMethod;
import com.misoft.hospital.util.validation.ValidateAt;
import com.misoft.hospital.util.validation.ValidationMethod;
import com.misoft.hospital.util.validation.ValidatorClass;
import com.misoft.hospital.validators.CashierShiftValidator;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.Formula;

import javax.persistence.*;
import javax.validation.ValidationException;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

/**
 * Created by kodero on 9/13/15.
 */
@Entity
@Table(name = "cashier_shifts")
@Filter(name = "filterByDeleted")
@ValidatorClass(CashierShiftValidator.class)
public class CashierShift extends ModelBase{

    @Column(name = "shift_no")
    private Long shiftNo;

    @JoinColumn(name = "owner", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Staff owner;

    @Column(name = "_start")
    @Temporal(TemporalType.TIMESTAMP)
    private Date start;

    @Column(name = "_end")
    @Temporal(TemporalType.TIMESTAMP)
    private Date end;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private ShiftStatus status;

    @Column(name = "shift_closed", columnDefinition = "tinyint(1) default '0'")
    private boolean shiftClosed = false;

    @Column(name = "opening_value")
    private BigDecimal openingValue;

    @Column(name = "closing_value")
    private BigDecimal closingValue;

    @Formula("coalesce((select sum(pp.amount_paid) from patient_payments pp where pp.shift = id and pp.deleted <> 1),0)")
    //@Column(name = "total_receipts")
    private BigDecimal totalReceipts;

    @Formula("coalesce((select sum(pp.amount_paid) from patient_payments pp where pp.shift = id and pp.payment_mode = 'CASH' and pp.deleted <> 1),0)")
    //@Column(name = "total_receipts")
    private BigDecimal cashReceipts;

    @Formula("coalesce((select sum(pp.amount_refunded) from patient_payments pp where pp.shift = id and pp.payment_mode = 'CASH' and pp.deleted <> 1),0)")
    private BigDecimal refunds;

    @Column(name = "shorts_overs")
    private BigDecimal shortsAndOvers;

    @Filter(name = "filterByDeleted")
    @OneToMany(mappedBy = "cashierShift", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    private Collection<DrawerBalance> drawerBalances;

    @JoinColumn(name = "surrendered_to", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Staff surrenderedTo;

    @Formula("(select sum(bs.amount_banked) from banking_shifts bs where bs.shift = id)")
    private BigDecimal banked;

    @Formula("(closing_value - coalesce((select sum(bs.amount_banked) from banking_shifts bs where bs.shift = id), 0))")
    private BigDecimal unbanked;

    @Column(name = "obl")
    private BigDecimal obl;

    @Column(name = "obl_half")
    private BigDecimal oblHalf;

    public boolean isShiftClosed() {
        return shiftClosed;
    }

    public void setShiftClosed(boolean shiftClosed) {
        this.shiftClosed = shiftClosed;
    }

    public CashierShift(){

    }

    public CashierShift(String id, Long shiftNo){
        setId(id);
        setShiftNo(shiftNo);
    }

    @ValidationMethod(when = ValidateAt.UPDATE)
    public void validateCashSurrenderedTo(){
        //check if shift was already closed
        if(isShiftClosed()){
            throw new ValidationException("Shift already closed!");
        }
        if(getStatus() == ShiftStatus.CLOSED && surrenderedTo == null){
            throw new ValidationException("Please select the person to whom the cash has been surrendered!");
        }
        if(shiftNo == null){
            throw new ValidationException("Please start a shift first!");
        }
    }

    @EntityCallbackMethod(when = During.UPDATE)
    public void updateClosure(){
        if(getStatus() == ShiftStatus.CLOSED){
            setShiftClosed(true);
        }
    }

    public Staff getOwner() {
        return owner;
    }

    public void setOwner(Staff owner) {
        this.owner = owner;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public ShiftStatus getStatus() {
        return status;
    }

    public void setStatus(ShiftStatus status) {
        this.status = status;
    }

    public BigDecimal getOpeningValue() {
        return openingValue;
    }

    public void setOpeningValue(BigDecimal openingValue) {
        this.openingValue = openingValue;
    }

    public BigDecimal getClosingValue() {
        return closingValue;
    }

    public void setClosingValue(BigDecimal closingValue) {
        this.closingValue = closingValue;
    }

    public BigDecimal getShortsAndOvers() {
        return shortsAndOvers;
    }

    public void setShortsAndOvers(BigDecimal shortsAndOvers) {
        this.shortsAndOvers = shortsAndOvers;
    }

    public Collection<DrawerBalance> getDrawerBalances() {
        return drawerBalances;
    }

    public void setDrawerBalances(Collection<DrawerBalance> drawerBalances) {
        this.drawerBalances = drawerBalances;
    }

    public Long getShiftNo() {
        return shiftNo;
    }

    public void setShiftNo(Long shiftNo) {
        this.shiftNo = shiftNo;
    }

    public BigDecimal getTotalReceipts() {
        return totalReceipts;
    }

    public void setTotalReceipts(BigDecimal totalReceipts) {
        this.totalReceipts = totalReceipts;
    }

    public Staff getSurrenderedTo() {
        return surrenderedTo;
    }

    public void setSurrenderedTo(Staff surrenderedTo) {
        this.surrenderedTo = surrenderedTo;
    }

    public BigDecimal getBanked() {
        return banked;
    }

    public void setBanked(BigDecimal banked) {
        this.banked = banked;
    }

    public BigDecimal getUnbanked() {
        return unbanked;
    }

    public void setUnbanked(BigDecimal unbanked) {
        this.unbanked = unbanked;
    }

    public BigDecimal getCashReceipts() {
        return cashReceipts;
    }

    public void setCashReceipts(BigDecimal cashReceipts) {
        this.cashReceipts = cashReceipts;
    }

    public BigDecimal getRefunds() {
        return refunds;
    }

    public void setRefunds(BigDecimal refunds) {
        this.refunds = refunds;
    }

    public BigDecimal getObl() {
        return obl;
    }

    public void setObl(BigDecimal obl) {
        this.obl = obl;
    }

    public BigDecimal getOblHalf() {
        return oblHalf;
    }

    public void setOblHalf(BigDecimal oblHalf) {
        this.oblHalf = oblHalf;
    }
}
