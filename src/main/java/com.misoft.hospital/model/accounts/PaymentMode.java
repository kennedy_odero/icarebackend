package com.misoft.hospital.model.accounts;

/**
 * Created by larry on 3/30/2015.
 */
public enum PaymentMode {
    CHEQUE,
    CASH,
    EFT,
    MOBILE_MONEY,
    DIRECT_DEPOSIT
}
