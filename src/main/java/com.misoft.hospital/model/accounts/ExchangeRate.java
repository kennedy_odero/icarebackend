package com.misoft.hospital.model.accounts;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by larry on 3/28/2015.
 */
@Entity
@Table(name = "exchange_rates")
@Filter(name = "filterByDeleted")
public class ExchangeRate extends ModelBase {
    @ManyToOne(optional = false)
    private Currency base;

    @ManyToOne(optional = false)
    private Currency target;

    @Column(name = "rate_date")
    @Temporal(TemporalType.DATE)
    private Date date;

    @Column(name = "rate", length = 19, scale = 5)
    private BigDecimal rate;


    public Currency getBase() {
        return base;
    }

    public void setBase(Currency base) {
        this.base = base;
    }

    public Currency getTarget() {
        return target;
    }

    public void setTarget(Currency target) {
        this.target = target;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }
}
