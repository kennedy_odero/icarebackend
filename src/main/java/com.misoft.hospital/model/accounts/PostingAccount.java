package com.misoft.hospital.model.accounts;

import javax.persistence.*;

/**
 * Created by larry on 3/10/2015.
 */
@Entity
@DiscriminatorValue("POSTING")
public class PostingAccount extends Account {
    @JoinColumn(name = "headingAccount", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private HeadingAccount headingAccount;

    public HeadingAccount getHeadingAccount() {
        return headingAccount;
    }

    public void setHeadingAccount(HeadingAccount headingAccount) {
        this.headingAccount = headingAccount;
    }
}
