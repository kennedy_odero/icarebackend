package com.misoft.hospital.model.accounts;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Created by larry on 3/23/2015.
 */
@Entity
@Table(name = "accounting_periods")
@Filter(name = "filterByDeleted")
public class AccountingPeriod extends ModelBase {

    @Column(name = "name", length = 50)
    private String name;

    @Column(name = "seq")
    private int seq;

    @Column(name = "from_date")
    @Temporal(TemporalType.DATE)
    private Date fromDate;

    @Column(name = "to_date")
    @Temporal(TemporalType.DATE)
    private Date toDate;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "parent", referencedColumnName = "id")
    private AccountingPeriod parent;

    @OneToMany(mappedBy = "parent", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @Filter(name = "filterByDeleted")
    private Collection<AccountingPeriod> children = new ArrayList<>();

    @Column(name = "period_type")
    @Enumerated(EnumType.STRING)
    private PeriodType periodType;

    @Column(name = "period_status")
    @Enumerated(EnumType.STRING)
    private PeriodStatus periodStatus;


    public int getSeq() {
        return seq;
    }

    public void setSeq(int seq) {
        this.seq = seq;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public String getName() {
        DateFormat dfmt = new SimpleDateFormat("dd/MMM/yyyy");
        name = dfmt.format(this.fromDate) +" - " + dfmt.format(this.toDate);
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PeriodType getPeriodType() {
        return periodType;
    }

    public void setPeriodType(PeriodType periodType) {
        this.periodType = periodType;
    }

    public PeriodStatus getPeriodStatus() {
        return periodStatus;
    }

    public void setPeriodStatus(PeriodStatus periodStatus) {
        this.periodStatus = periodStatus;
    }

    public AccountingPeriod getParent() {
        return parent;
    }

    public void setParent(AccountingPeriod parent) {
        this.parent = parent;
    }

    public Collection<AccountingPeriod> getChildren() {
        return children;
    }

    public void addChild(AccountingPeriod ap) {
        ap.setParent(this);
        getChildren().add(ap);
        System.out.println("added : [" + ap.getFromDate() + " - " + ap.getToDate() + "] to parent : " + this.getPeriodType());
    }

    public void addChildren(List<AccountingPeriod> aps) {
        for(AccountingPeriod ap : aps){
            ap.setParent(this);
            getChildren().add(ap);
            System.out.println("added : [" + ap.getFromDate() + " - " + ap.getToDate() + "] to parent : " + this.getPeriodType());
        }
    }

    public void setChildren(Collection<AccountingPeriod> children) {
        this.children = children;
    }
}
