package com.misoft.hospital.model.accounts;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.*;

/**
 * Created by larry on 3/21/2015.
 */
@Entity
@Table(name = "cashflows_sub_headers")
@Filter(name = "filterByDeleted")
public class CashflowSubHeader extends ModelBase {

    @Column(name = "name")
    private String name;

    @Column(name = "descr")
    private String desc;

    @ManyToOne(fetch = FetchType.LAZY)
    private CashflowHeader cashflowHeader;

    @Column(name = "position")
    private int placing;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getPlacing() {
        return placing;
    }

    public void setPlacing(int placing) {
        this.placing = placing;
    }

    public CashflowHeader getCashflowHeader() {
        return cashflowHeader;
    }

    public void setCashflowHeader(CashflowHeader cashflowHeader) {
        this.cashflowHeader = cashflowHeader;
    }
}
