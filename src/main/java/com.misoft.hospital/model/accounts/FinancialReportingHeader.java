package com.misoft.hospital.model.accounts;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by larry on 3/19/2015.
 */
@Entity
@Table(name = "financial_reporting_headers")

@Filter(name = "filterByDeleted")
public class FinancialReportingHeader extends ModelBase {

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "header_type")
    private HeaderType headerType;

    @Column(name = "position")
    private int position;

    @Column(name = "category")
    @Enumerated(EnumType.STRING)
    private AccountCategory accountCategory;

    @OneToMany(mappedBy = "financialReportingHeader", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @Filter(name = "filterByDeleted")
    private Collection<FinancialReportingHeaderDetail> financialReportingHeaderDetails = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public HeaderType getHeaderType() {
        return headerType;
    }

    public void setHeaderType(HeaderType headerType) {
        this.headerType = headerType;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public AccountCategory getAccountCategory() {
        return accountCategory;
    }

    public void setAccountCategory(AccountCategory accountCategory) {
        this.accountCategory = accountCategory;
    }

    public Collection<FinancialReportingHeaderDetail> getFinancialReportingHeaderDetails() {
        return financialReportingHeaderDetails;
    }

    public void setFinancialReportingHeaderDetails(Collection<FinancialReportingHeaderDetail> financialReportingHeaderDetails) {
        this.financialReportingHeaderDetails = financialReportingHeaderDetails;
    }
}
