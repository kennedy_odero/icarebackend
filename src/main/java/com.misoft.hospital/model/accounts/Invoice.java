package com.misoft.hospital.model.accounts;

import com.misoft.hospital.model.base.ModelBase;
import com.misoft.hospital.model.setup.Party;
import com.misoft.hospital.model.shared.YesNo;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

@Entity
@Table(name = "invoices")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "invoice_type")
@Filter(name = "filterByDeleted")
public abstract class Invoice extends ModelBase {
    
    @Column(name = "ref_invoice_no")//the value captured from a hard-copy invoice
	private String refInvoiceNo;

    @Column(name = "invoice_no")
    private String invoiceNo;

    @Column(name = "invoice_date")
    private Date invoiceDate;

    @Column(name = "description")
    private String description;
    
    @Column(name = "due_date")
	@Temporal(TemporalType.DATE)
    private Date dueDate;
    
    @Column(name = "control_amnt")
	private BigDecimal controlAmnt;
	
	@Column(name = "balance")
	private BigDecimal balance;

	@Column(name = "vat")
	private BigDecimal vat;
	
	@Column(name = "with_tax")
	@Enumerated(EnumType.STRING)
	private YesNo withTax;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "currency", referencedColumnName = "id")
	private Currency currency;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "account", referencedColumnName = "id")
	private Account account;
	
	@Column(name = "spot_rate")
	private BigDecimal spotRate;
	
	@Column(name = "cleared")
	@Enumerated(EnumType.STRING)
	private YesNo cleared;
	
	@Column(name = "posted")
	@Enumerated(EnumType.STRING)
	private YesNo posted;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "period", referencedColumnName = "id")
	private AccountingPeriod period;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "approved_date")
	private Date approvedDate;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "approvedeBy", referencedColumnName = "id")
	private Party approvedBy;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "authorized_date")
	private Date authorizedDate;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "authorizedBy", referencedColumnName = "id")
	private Party authorizeBy;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "debtor", referencedColumnName = "id")
	private Debtor debtor;

    @OneToMany(mappedBy = "invoice", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @Filter(name = "filterByDeleted")
    private Collection<InvoiceLine> invoiceLines = new ArrayList<>();

	public String getRefInvoiceNo() {
		return refInvoiceNo;
	}

	public void setRefInvoiceNo(String refInvoiceNo) {
		this.refInvoiceNo = refInvoiceNo;
	}

	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public Date getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public BigDecimal getControlAmnt() {
		return controlAmnt;
	}

	public void setControlAmnt(BigDecimal controlAmnt) {
		this.controlAmnt = controlAmnt;
	}

	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	public BigDecimal getVat() {
		return vat;
	}

	public void setVat(BigDecimal vat) {
		this.vat = vat;
	}

	public YesNo getWithTax() {
		return withTax;
	}

	public void setWithTax(YesNo withTax) {
		this.withTax = withTax;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public BigDecimal getSpotRate() {
		return spotRate;
	}

	public void setSpotRate(BigDecimal spotRate) {
		this.spotRate = spotRate;
	}

	public YesNo getCleared() {
		return cleared;
	}

	public void setCleared(YesNo cleared) {
		this.cleared = cleared;
	}

	public YesNo getPosted() {
		return posted;
	}

	public void setPosted(YesNo posted) {
		this.posted = posted;
	}

	public AccountingPeriod getPeriod() {
		return period;
	}

	public void setPeriod(AccountingPeriod period) {
		this.period = period;
	}

	public Date getApprovedDate() {
		return approvedDate;
	}

	public void setApprovedDate(Date approvedDate) {
		this.approvedDate = approvedDate;
	}

	public Party getApprovedBy() {
		return approvedBy;
	}

	public void setApprovedBy(Party approvedBy) {
		this.approvedBy = approvedBy;
	}

	public Date getAuthorizedDate() {
		return authorizedDate;
	}

	public void setAuthorizedDate(Date authorizedDate) {
		this.authorizedDate = authorizedDate;
	}

	public Party getAuthorizeBy() {
		return authorizeBy;
	}

	public void setAuthorizeBy(Party authorizeBy) {
		this.authorizeBy = authorizeBy;
	}

	public Debtor getDebtor() {
		return debtor;
	}

	public void setDebtor(Debtor debtor) {
		this.debtor = debtor;
	}

	public Collection<InvoiceLine> getInvoiceLines() {
		return invoiceLines;
	}

	public void setInvoiceLines(Collection<InvoiceLine> invoiceLines) {
		this.invoiceLines = invoiceLines;
	}

    public void addInvoiceLine(InvoiceLine invoiceLine){
		if(invoiceLine != null) {
			getInvoiceLines().add(invoiceLine);
			invoiceLine.setInvoice(this);
		}
	}

	public void addInvoiceLines(Collection<InvoiceLine> invoiceLines){
		for(InvoiceLine invoiceLine : invoiceLines){
			if(invoiceLine != null) {
				getInvoiceLines().add(invoiceLine);
				invoiceLine.setInvoice(this);
			}
		}
	}
}