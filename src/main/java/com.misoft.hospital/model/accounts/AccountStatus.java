/**
 * 
 */
package com.misoft.hospital.model.accounts;

/**
 * @author larry
 *
 */
public enum AccountStatus {
	ACTIVE,
	DORMANT
}
