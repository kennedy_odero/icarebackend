package com.misoft.hospital.model.accounts;

/**
 * Created by larry on 3/21/2015.
 */
public enum CashflowHeaderType {
    OPERTNG_ACTIVITIES,
    INVESTING,
    NET_CASH,
    FINANCING
}
