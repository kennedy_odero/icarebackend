package com.misoft.hospital.model.accounts;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 * Created by larry on 3/21/2015.
 */
@Entity
@Table(name = "bank_reconciliation")
@Filter(name = "filterByDeleted")
public class BankReconciliation extends ModelBase {

    @Column(name = "statement_date")
    @Temporal(TemporalType.DATE)
    private Date bankStatementDate;

    @Column(name = "bank_bal")
    private BigDecimal bnkBal;

    @Column(name = "cb_bal")
    private BigDecimal cbBal;

    @Column(name = "movn_bal")
    private BigDecimal movnBal;

    @Column(name = "recon_date")
    private Date reconDate;

    @OneToMany(mappedBy = "bnkrecon",cascade={CascadeType.ALL})
    private Collection<BankReconcilliationItem> reconItems = new ArrayList<>();


    public Date getBankStatementDate() {
        return bankStatementDate;
    }

    public void setBankStatementDate(Date bankStatementDate) {
        this.bankStatementDate = bankStatementDate;
    }

    public BigDecimal getBnkBal() {
        return bnkBal;
    }

    public void setBnkBal(BigDecimal bnkBal) {
        this.bnkBal = bnkBal;
    }

    public BigDecimal getCbBal() {
        return cbBal;
    }

    public void setCbBal(BigDecimal cbBal) {
        this.cbBal = cbBal;
    }

    public BigDecimal getMovnBal() {
        return movnBal;
    }

    public void setMovnBal(BigDecimal movnBal) {
        this.movnBal = movnBal;
    }

    public Date getReconDate() {
        return reconDate;
    }

    public void setReconDate(Date reconDate) {
        this.reconDate = reconDate;
    }

    public Collection<BankReconcilliationItem> getReconItems() {
        return reconItems;
    }

    public void setReconItems(Collection<BankReconcilliationItem> reconItems) {
        this.reconItems = reconItems;
    }
}
