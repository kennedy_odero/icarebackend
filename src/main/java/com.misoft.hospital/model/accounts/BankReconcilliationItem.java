package com.misoft.hospital.model.accounts;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by larry on 3/21/2015.
 */
@Entity
@Table(name = "bank_recon_items")
@Filter(name = "filterByDeleted")
public class BankReconcilliationItem extends ModelBase {

    @Column(name = "txn_date")
    @Temporal(TemporalType.DATE)
    private Date date;

    @Column(name = "ref_no")
    private String refNo;

    @Column(name = "particular")
    private String particular;

    @Column(name = "dr_amount")
    private BigDecimal debitAmount;

    @Column(name = "cr_amount")
    private BigDecimal creditAmount;

    @ManyToOne
    private BankReconciliation bnkrecon;


    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getRefNo() {
        return refNo;
    }

    public void setRefNo(String refNo) {
        this.refNo = refNo;
    }

    public String getParticular() {
        return particular;
    }

    public void setParticular(String particular) {
        this.particular = particular;
    }

    public BigDecimal getDebitAmount() {
        return debitAmount;
    }

    public void setDebitAmount(BigDecimal debitAmount) {
        this.debitAmount = debitAmount;
    }

    public BigDecimal getCreditAmount() {
        return creditAmount;
    }

    public void setCreditAmount(BigDecimal creditAmount) {
        this.creditAmount = creditAmount;
    }

    public BankReconciliation getBnkrecon() {
        return bnkrecon;
    }

    public void setBnkrecon(BankReconciliation bnkrecon) {
        this.bnkrecon = bnkrecon;
    }
}
