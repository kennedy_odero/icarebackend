package com.misoft.hospital.model.accounts;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Created by larry on 3/2/2015.
 */
@Entity
@DiscriminatorValue("PAYABLE")
public class Payable extends Invoice {
}
