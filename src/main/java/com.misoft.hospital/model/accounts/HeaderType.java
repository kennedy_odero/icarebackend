package com.misoft.hospital.model.accounts;

/**
 * Created by larry on 3/19/2015.
 */
public enum HeaderType {
    BALANCE_SHEET,
    INCOME_STATEMENT
}
