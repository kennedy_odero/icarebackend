package com.misoft.hospital.model.accounts;

import com.misoft.hospital.model.base.ModelBase;
import com.misoft.hospital.model.shared.Status;
import org.hibernate.annotations.Filter;

import javax.persistence.*;

/**
 * Created by larry on 3/21/2015.
 */
@Entity
@Table(name = "banks")
@Filter(name = "filterByDeleted")
public class Bank extends ModelBase {
    @Column(name = "name", unique = true)
    private String name;

    @Column(name = "code", unique = true)
    private String code;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private Status status;

    @Column(name = "paypoint_type")
    @Enumerated(EnumType.STRING)
    private PayPointType payPointType;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public PayPointType getPayPointType() {
        return payPointType;
    }

    public void setPayPointType(PayPointType payPointType) {
        this.payPointType = payPointType;
    }
}
