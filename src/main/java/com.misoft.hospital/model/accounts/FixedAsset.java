package com.misoft.hospital.model.accounts;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by larry on 3/15/2015.
 */
@Entity
@Table(name = "fixed_assets")
@Filter(name = "filterByDeleted")
public class FixedAsset extends ModelBase {

    @Column(name="name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "date_acquired")
    @Temporal(TemporalType.DATE)
    private Date dateAcquired;

    @Column(name = "original_cost")
    private BigDecimal originalCost;

    @Column(name = "initial_book_value")
    private BigDecimal initialBookValue;

    @Column(name = "current_value")
    private BigDecimal currentValue;

    @Column(name = "date_recorded")
    @Temporal(TemporalType.DATE)
    private Date dateRecorded;

    @Column(name = "prev_dep_date")
    @Temporal(TemporalType.DATE)
    private Date prevDepreciationDate;

    @Column(name = "caretaker")
    private String caretaker;

    @Column(name = "asset_code")
    private String assetCode;

    @Column(name = "location")
    private String location;

    @ManyToOne(fetch=FetchType.LAZY)
    private AssetClass assetClass;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDateAcquired() {
        return dateAcquired;
    }

    public void setDateAcquired(Date dateAcquired) {
        this.dateAcquired = dateAcquired;
    }

    public BigDecimal getOriginalCost() {
        return originalCost;
    }

    public void setOriginalCost(BigDecimal originalCost) {
        this.originalCost = originalCost;
    }

    public BigDecimal getInitialBookValue() {
        return initialBookValue;
    }

    public void setInitialBookValue(BigDecimal initialBookValue) {
        this.initialBookValue = initialBookValue;
    }

    public BigDecimal getCurrentValue() {
        return currentValue;
    }

    public void setCurrentValue(BigDecimal currentValue) {
        this.currentValue = currentValue;
    }

    public Date getDateRecorded() {
        return dateRecorded;
    }

    public void setDateRecorded(Date dateRecorded) {
        this.dateRecorded = dateRecorded;
    }

    public Date getPrevDepreciationDate() {
        return prevDepreciationDate;
    }

    public void setPrevDepreciationDate(Date prevDepreciationDate) {
        this.prevDepreciationDate = prevDepreciationDate;
    }

    public String getCaretaker() {
        return caretaker;
    }

    public void setCaretaker(String caretaker) {
        this.caretaker = caretaker;
    }

    public String getAssetCode() {
        return assetCode;
    }

    public void setAssetCode(String assetCode) {
        this.assetCode = assetCode;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public AssetClass getAssetClass() {
        return assetClass;
    }

    public void setAssetClass(AssetClass assetClass) {
        this.assetClass = assetClass;
    }
}
