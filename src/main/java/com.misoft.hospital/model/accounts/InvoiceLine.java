package com.misoft.hospital.model.accounts;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * @author larry
 *
 */
@Entity
@Table(name = "invoice_lines")
@Filter(name = "filterByDeleted")
public class InvoiceLine extends ModelBase{
	
	@Column(name="description")
	private String itemDescription;
	
	@Column(name="amount")
	private BigDecimal amount;
	
    @Column(name="tax")
    private BigDecimal tax;
    
    @Column(name = "seq")
	private Long seq;

	@Column(name = "quantity")
	private BigDecimal quantity;

	@Column(name = "line_total")
	private BigDecimal lineTotal;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "currency", referencedColumnName = "id")
	private Currency currency;
	
	@Column(name = "spot_rate")
	private BigDecimal spotRate;
	
	@Column(name = "bc_value")
	private BigDecimal bcValue;//base currency value=(price*spotrate)*quantity
	
	@Column(name = "sc_value")
	private BigDecimal scValue;//source currency value=(price) * quantity
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "account", referencedColumnName = "id")
	private Account account;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "budgetLine", referencedColumnName = "id")
	private BudgetHeader budgetLine;
	
	@Column(name = "dr_cr", length = 5)
	@Enumerated(EnumType.STRING)
	private CrDr drcr;
	
	@Column(name = "vat_amount")
	private BigDecimal vatAmount;
	
	@Column(name = "vat_rateapplied")
	private BigDecimal vatRateApplied;

    @JoinColumn(name = "invoice", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
	private Invoice invoice;
    
	public BigDecimal getTax() {
		return tax;
	}
	public void setTax(BigDecimal tax) {
		this.tax = tax;
	}
	public Long getSeq() {
		return seq;
	}
	public void setSeq(Long seq) {
		this.seq = seq;
	}
	public BigDecimal getQuantity() {
		return quantity;
	}
	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}
	public Currency getCurrency() {
		return currency;
	}
	public void setCurrency(Currency currency) {
		this.currency = currency;
	}
	public BigDecimal getSpotRate() {
		return spotRate;
	}
	public void setSpotRate(BigDecimal spotRate) {
		this.spotRate = spotRate;
	}
	public BigDecimal getBcValue() {
		return bcValue;
	}
	public void setBcValue(BigDecimal bcValue) {
		this.bcValue = bcValue;
	}
	public BigDecimal getScValue() {
		return scValue;
	}
	public void setScValue(BigDecimal scValue) {
		this.scValue = scValue;
	}
	public Account getAccount() {
		return account;
	}
	public void setAccount(Account account) {
		this.account = account;
	}
	public BudgetHeader getBudgetLine() {
		return budgetLine;
	}
	public void setBudgetLine(BudgetHeader budgetLine) {
		this.budgetLine = budgetLine;
	}
	public CrDr getDrcr() {
		return drcr;
	}
	public void setDrcr(CrDr drcr) {
		this.drcr = drcr;
	}
	public BigDecimal getVatAmount() {
		return vatAmount;
	}
	public void setVatAmount(BigDecimal vatAmount) {
		this.vatAmount = vatAmount;
	}
	public BigDecimal getVatRateApplied() {
		return vatRateApplied;
	}
	public void setVatRateApplied(BigDecimal vatRateApplied) {
		this.vatRateApplied = vatRateApplied;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public Invoice getInvoice() {
        return invoice;
    }

    public void setInvoice(Invoice invoice) {
        this.invoice = invoice;
    }

    public Invoice getPayable() {
        return invoice;
    }

    public void setPayable(Invoice invoice) {
        this.invoice = invoice;
    }

	public BigDecimal getLineTotal() {
		return lineTotal;
	}

	public void setLineTotal(BigDecimal lineTotal) {
		this.lineTotal = lineTotal;
	}
}
