package com.misoft.hospital.model.accounts;

/**
 * Created by larry on 3/21/2015.
 */
public enum BankReconciliationItemType {
    UNPRESENTED_CHEQUES,
    RECONCILIATION_ITEMS,
    UNCREDITED_RECEIPT,
    GL_TRANSACTION
}
