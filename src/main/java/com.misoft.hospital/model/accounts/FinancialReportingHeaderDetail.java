package com.misoft.hospital.model.accounts;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.*;

/**
 * Created by larry on 3/19/2015.
 */
@Entity
@Table(name = "fin_reporting_header_dets")
@Filter(name = "filterByDeleted")
public class FinancialReportingHeaderDetail extends ModelBase {

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;


    @ManyToOne(fetch = FetchType.LAZY)
    private FinancialReportingHeader financialReportingHeader;

    @Column(name = "position")
    private int position;

    @ManyToOne(fetch = FetchType.LAZY)
    private IncomeStatementGroup incomeStatementGroup;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public FinancialReportingHeader getFinancialReportingHeader() {
        return financialReportingHeader;
    }

    public void setFinancialReportingHeader(FinancialReportingHeader financialReportingHeader) {
        this.financialReportingHeader = financialReportingHeader;
    }


    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public IncomeStatementGroup getIncomeStatementGroup() {
        return incomeStatementGroup;
    }

    public void setIncomeStatementGroup(IncomeStatementGroup incomeStatementGroup) {
        this.incomeStatementGroup = incomeStatementGroup;
    }
}
