package com.misoft.hospital.model.accounts;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Created by kodero on 9/30/15.
 */
@Entity
@Table(name = "drawer_balances")
@Filter(name = "filterByDeleted")
public class DrawerBalance extends ModelBase{

    @JoinColumn(name = "shift", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private CashierShift cashierShift;

    @JoinColumn(name = "denomination", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Denomination denomination;

    @Column(name = "opening")
    private BigInteger opening;

    @Column(name = "closing")
    private BigInteger closing;

    @Column(name = "opening_value")
    private BigDecimal openingValue;

    @Column(name = "closing_value")
    private BigDecimal closingValue;

    public CashierShift getCashierShift() {
        return cashierShift;
    }

    public void setCashierShift(CashierShift cashierShift) {
        this.cashierShift = cashierShift;
    }

    public Denomination getDenomination() {
        return denomination;
    }

    public void setDenomination(Denomination denomination) {
        this.denomination = denomination;
    }

    public BigInteger getOpening() {
        return opening;
    }

    public void setOpening(BigInteger opening) {
        this.opening = opening;
    }

    public BigInteger getClosing() {
        return closing;
    }

    public void setClosing(BigInteger closing) {
        this.closing = closing;
    }

    public BigDecimal getOpeningValue() {
        return openingValue;
    }

    public void setOpeningValue(BigDecimal openingValue) {
        this.openingValue = openingValue;
    }

    public BigDecimal getClosingValue() {
        return closingValue;
    }

    public void setClosingValue(BigDecimal closingValue) {
        this.closingValue = closingValue;
    }
}
