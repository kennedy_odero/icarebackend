package com.misoft.hospital.model.accounts;

/**
 * Created by larry on 3/15/2015.
 */
public enum DepreciationMethod {
    STRAIGHT_LINE,
    REDUCING_BALANCE
}
