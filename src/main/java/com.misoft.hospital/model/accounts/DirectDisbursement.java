package com.misoft.hospital.model.accounts;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Created by larry on 3/19/2015.
 */
@Entity
@DiscriminatorValue("DIRECT_DISBURSEMENT")
public class DirectDisbursement extends Disbursement {
}
