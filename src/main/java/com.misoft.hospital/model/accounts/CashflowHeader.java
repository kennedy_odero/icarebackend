package com.misoft.hospital.model.accounts;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by larry on 3/21/2015.
 */
@Entity
@Table(name = "cashflows_headers")
@Filter(name = "filterByDeleted")
public class CashflowHeader extends ModelBase {

    @Column(name = "name")
    private String name;

    @Column(name = "descr")
    private String desc;

    @Column(name = "position")
    private int position;

    @Column(name = "category")
    @Enumerated(EnumType.STRING)
    private CashflowHeaderType category;

    @OneToMany(mappedBy = "cashflowHeader",cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @Filter(name = "filterByDeleted")
    private Collection<CashflowSubHeader> cashflowSubHeaders = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public CashflowHeaderType getCategory() {
        return category;
    }

    public void setCategory(CashflowHeaderType category) {
        this.category = category;
    }

    public Collection<CashflowSubHeader> getCashflowSubHeaders() {
        return cashflowSubHeaders;
    }

    public void setCashflowSubHeaders(Collection<CashflowSubHeader> cashflowSubHeaders) {
        this.cashflowSubHeaders = cashflowSubHeaders;
    }
}
