package com.misoft.hospital.model.accounts;

/**
 * Created by larry on 3/6/2015.
 */
public enum AccountType {
    HEADING,
    POSTING
}
