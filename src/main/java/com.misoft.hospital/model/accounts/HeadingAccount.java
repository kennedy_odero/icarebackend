package com.misoft.hospital.model.accounts;

import org.hibernate.annotations.Filter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by larry on 3/10/2015.
 */
@Entity
@DiscriminatorValue("HEADING")
public class HeadingAccount extends Account {
    @OneToMany(mappedBy = "headingAccount", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @Filter(name = "filterByDeleted")
    private Collection<PostingAccount> childAccounts = new ArrayList<>();
}
