package com.misoft.hospital.model.accounts;

/**
 * Created by larry on 3/28/2015.
 */
public enum PeriodType {
    FISCAL_YEAR,
    FISCAL_HALF,
    FISCAL_QUARTER,
    FISCAL_MONTH
}
