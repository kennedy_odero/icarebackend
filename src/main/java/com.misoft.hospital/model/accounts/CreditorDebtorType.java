package com.misoft.hospital.model.accounts;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.*;

/**
 * Created by larry on 3/27/2015.
 */
@Entity
@Table(name = "creditor_debtor_types")
@Filter(name = "filterByDeleted")
public class CreditorDebtorType extends ModelBase {

    @Column(name = "name")
    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    private Account account;

    @Column(name = "cr_dr")
    @Enumerated(EnumType.STRING)
    private CrDr crdr;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public CrDr getCrdr() {
        return crdr;
    }

    public void setCrdr(CrDr crdr) {
        this.crdr = crdr;
    }
}
