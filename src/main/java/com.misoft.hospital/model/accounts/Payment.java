package com.misoft.hospital.model.accounts;

import com.misoft.hospital.model.base.ModelBase;
import com.misoft.hospital.model.shared.YesNo;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

@Entity
@Table(name = "payments")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "payment_type")
@Filter(name = "filterByDeleted")
public abstract class Payment extends ModelBase {

    public Payment(){

    }

    @Column(name = "payment_no")
    private String paymentNo;

    @Column(name = "payment_date")
    private Date paymentDate;

    @Column(name = "description")
    private String description;

    @OneToMany(mappedBy = "payment", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @Filter(name = "filterByDeleted")
    private Collection<PaymentLine> paymentLines = new ArrayList<PaymentLine>();

    @ManyToOne(fetch = FetchType.LAZY)
    private Imprest imprest;

    @Column(name = "pmt_date")
    @Temporal(TemporalType.DATE)
    private Date date;

    @Column(name = "voucher_date")
    @Temporal(TemporalType.DATE)
    private Date voucherDate;

    @Column(name = "payment_mode")
    @Enumerated(EnumType.STRING)
    private PaymentMode paymentMode;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Bank cashbook;

    @ManyToOne(fetch = FetchType.LAZY)
    private BudgetItem budgetItem;

    @ManyToOne(fetch = FetchType.LAZY)
    private Currency currency;

    @Column(name = "cheque_no")
    private String chequeNo;

    @Column(name = "imprest_no")
    private String imprestNo;

    @Column(name = "bank_txn_no")
    private String bankTxnNo;///transaction number

    @Column(name = "posted")
    @Enumerated(EnumType.STRING)
    private YesNo posted;

    @Column(name = "cleared")
    @Enumerated(EnumType.STRING)
    private YesNo cleared;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private AccountingPeriod period;

    @Column(name = "prepared_date")
    @Temporal(TemporalType.DATE)
    private Date preparedDate;

    @Column(name = "approved_date")
    @Temporal(TemporalType.DATE)
    private Date approvedDate;

    @Column(name = "authorized_date")
    @Temporal(TemporalType.DATE)
    private Date authorizedDate;

    @Column(name = "whtax_amount")
    private BigDecimal withHoldingTaxAmount;

    @Column(name = "vat_amount")
    private BigDecimal vatTaxAmount;

    @Column(name = "control_amt")
    private BigDecimal controlAmount;

    @Column(name = "whtaxrate_id")
    private Long withholdingTaxRateId;

    @Column(name = "whtax_rate")
    private BigDecimal withHoldingTaxRate;

    public String getPaymentNo() {
        return paymentNo;
    }

    public void setPaymentNo(String paymentNo) {
        this.paymentNo = paymentNo;
    }

    public Date getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Collection<PaymentLine> getPaymentLines() {
        return paymentLines;
    }

    public void addPaymentLine(PaymentLine paymentLine) {
        if(paymentLine != null){
            paymentLine.setPayment(this);
            getPaymentLines().add(paymentLine);
        }
    }

    public void setPaymentLines(Collection<PaymentLine> paymentLines) {
        this.paymentLines = paymentLines;
    }

    public Imprest getImprest() {
        return imprest;
    }

    public void setImprest(Imprest imprest) {
        this.imprest = imprest;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getVoucherDate() {
        return voucherDate;
    }

    public void setVoucherDate(Date voucherDate) {
        this.voucherDate = voucherDate;
    }

    public PaymentMode getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(PaymentMode paymentMode) {
        this.paymentMode = paymentMode;
    }

    public Bank getCashbook() {
        return cashbook;
    }

    public void setCashbook(Bank cashbook) {
        this.cashbook = cashbook;
    }

    public BudgetItem getBudgetItem() {
        return budgetItem;
    }

    public void setBudgetItem(BudgetItem budgetItem) {
        this.budgetItem = budgetItem;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public String getChequeNo() {
        return chequeNo;
    }

    public void setChequeNo(String chequeNo) {
        this.chequeNo = chequeNo;
    }

    public String getImprestNo() {
        return imprestNo;
    }

    public void setImprestNo(String imprestNo) {
        this.imprestNo = imprestNo;
    }

    public String getBankTxnNo() {
        return bankTxnNo;
    }

    public void setBankTxnNo(String bankTxnNo) {
        this.bankTxnNo = bankTxnNo;
    }

    public YesNo getPosted() {
        return posted;
    }

    public void setPosted(YesNo posted) {
        this.posted = posted;
    }

    public YesNo getCleared() {
        return cleared;
    }

    public void setCleared(YesNo cleared) {
        this.cleared = cleared;
    }

    public AccountingPeriod getPeriod() {
        return period;
    }

    public void setPeriod(AccountingPeriod period) {
        this.period = period;
    }

    public Date getPreparedDate() {
        return preparedDate;
    }

    public void setPreparedDate(Date preparedDate) {
        this.preparedDate = preparedDate;
    }

    public Date getApprovedDate() {
        return approvedDate;
    }

    public void setApprovedDate(Date approvedDate) {
        this.approvedDate = approvedDate;
    }

    public Date getAuthorizedDate() {
        return authorizedDate;
    }

    public void setAuthorizedDate(Date authorizedDate) {
        this.authorizedDate = authorizedDate;
    }

    public BigDecimal getWithHoldingTaxAmount() {
        return withHoldingTaxAmount;
    }

    public void setWithHoldingTaxAmount(BigDecimal withHoldingTaxAmount) {
        this.withHoldingTaxAmount = withHoldingTaxAmount;
    }

    public BigDecimal getVatTaxAmount() {
        return vatTaxAmount;
    }

    public void setVatTaxAmount(BigDecimal vatTaxAmount) {
        this.vatTaxAmount = vatTaxAmount;
    }

    public Long getWithholdingTaxRateId() {
        return withholdingTaxRateId;
    }

    public void setWithholdingTaxRateId(Long withholdingTaxRateId) {
        this.withholdingTaxRateId = withholdingTaxRateId;
    }

    public BigDecimal getWithHoldingTaxRate() {
        return withHoldingTaxRate;
    }

    public void setWithHoldingTaxRate(BigDecimal withHoldingTaxRate) {
        this.withHoldingTaxRate = withHoldingTaxRate;
    }

    public BigDecimal getControlAmount() {
        return controlAmount;
    }

    public void setControlAmount(BigDecimal controlAmount) {
        this.controlAmount = controlAmount;
    }
}