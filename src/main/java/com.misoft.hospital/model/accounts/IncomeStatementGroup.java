package com.misoft.hospital.model.accounts;

import com.misoft.hospital.model.base.ModelBase;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by larry on 3/19/2015.
 */
@Entity
@Table(name = "income_stmt_groups")
public class IncomeStatementGroup extends ModelBase {
    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
