package com.misoft.hospital.model.accounts;

import com.misoft.hospital.model.base.ModelBase;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by kodero on 4/25/16.
 */
@Entity
@Table(name = "banking_shifts")
public class BankingShift extends ModelBase{

    @JoinColumn(name = "banking", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Banking banking;

    @JoinColumn(name = "shift", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private CashierShift cashierShift;

    @Column(name = "amount_banked")
    private BigDecimal amountBanked;

    public Banking getBanking() {
        return banking;
    }

    public void setBanking(Banking banking) {
        this.banking = banking;
    }

    public CashierShift getCashierShift() {
        return cashierShift;
    }

    public void setCashierShift(CashierShift cashierShift) {
        this.cashierShift = cashierShift;
    }

    public BigDecimal getAmountBanked() {
        return amountBanked;
    }

    public void setAmountBanked(BigDecimal amountBanked) {
        this.amountBanked = amountBanked;
    }
}
