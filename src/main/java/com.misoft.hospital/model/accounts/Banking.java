package com.misoft.hospital.model.accounts;

import com.misoft.hospital.model.base.ModelBase;
import com.misoft.hospital.model.setup.Staff;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.Formula;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

/**
 * Created by kodero on 4/25/16.
 */
@Entity
@Table(name = "bankings")
@Filter(name = "filterByDeleted")
public class Banking extends ModelBase{

    @NotNull
    @Column(name = "date_banked")
    private Date dateBanked;

    @NotNull
    @JoinColumn(name = "banked_by", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Staff bankedBy;

    @Formula("(select sum(bs.amount_banked) from banking_shifts bs where bs.banking = id)")
    private BigDecimal totalBanked;

    @Formula("(select sum(cs.closing_value) from cashier_shifts cs inner join banking_shifts bs on cs.id = bs.shift where bs.banking = id)")
    private BigDecimal shiftTotal;

    @JoinColumn(name = "cashbook", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Account cashbook;

    @OneToMany(mappedBy = "banking", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @Filter(name = "filterByDeleted")
    private Collection<BankingShift> bankingShifts;

    @NotNull
    @Column(name = "reference_no")
    private String referenceNumber;

    public Date getDateBanked() {
        return dateBanked;
    }

    public void setDateBanked(Date dateBanked) {
        this.dateBanked = dateBanked;
    }

    public Staff getBankedBy() {
        return bankedBy;
    }

    public void setBankedBy(Staff bankedBy) {
        this.bankedBy = bankedBy;
    }

    public BigDecimal getTotalBanked() {
        return totalBanked;
    }

    public void setTotalBanked(BigDecimal totalBanked) {
        this.totalBanked = totalBanked;
    }

    public Account getCashbook() {
        return cashbook;
    }

    public void setCashbook(Account cashbook) {
        this.cashbook = cashbook;
    }

    public Collection<BankingShift> getBankingShifts() {
        return bankingShifts;
    }

    public void setBankingShifts(Collection<BankingShift> bankingShifts) {
        this.bankingShifts = bankingShifts;
    }

    public BigDecimal getShiftTotal() {
        return shiftTotal;
    }

    public void setShiftTotal(BigDecimal shiftTotal) {
        this.shiftTotal = shiftTotal;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }
}
