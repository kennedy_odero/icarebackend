package com.misoft.hospital.model.accounts;

import com.misoft.hospital.model.setup.Party;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 * Created by larry on 3/4/2015.
 */
@Entity
@DiscriminatorValue("DEBTOR")
public class Debtor extends Party {

    @ManyToOne
    private CreditorDebtorType creditorDebtorType;

    @Column(name = "code")
    private String code;

    @Column(name = "vat_no")
    private String vatNo;

    @Column(name = "tax_pin")
    private String taxPin;

    public CreditorDebtorType getCreditorDebtorType() {
        return creditorDebtorType;
    }

    public void setCreditorDebtorType(CreditorDebtorType creditorDebtorType) {
        this.creditorDebtorType = creditorDebtorType;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getVatNo() {
        return vatNo;
    }

    public void setVatNo(String vatNo) {
        this.vatNo = vatNo;
    }

    public String getTaxPin() {
        return taxPin;
    }

    public void setTaxPin(String taxPin) {
        this.taxPin = taxPin;
    }
}
