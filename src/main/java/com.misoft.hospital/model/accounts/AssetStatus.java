/**
 * 
 */
package com.misoft.hospital.model.accounts;

/**
 * @author larry
 * @date-created April 9, 2015
 */
public enum AssetStatus {
	IN_USE,
	RETIRED,
	SOLD
}
