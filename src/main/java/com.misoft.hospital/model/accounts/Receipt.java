package com.misoft.hospital.model.accounts;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Created by larry on 3/4/2015.
 */
@Entity
@DiscriminatorValue("RECEIPT")
@DiscriminatorColumn(name = "transaction_type")
public class Receipt extends Payment {
}
