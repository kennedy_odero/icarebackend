package com.misoft.hospital.model.accounts;

import com.misoft.hospital.model.base.ModelBase;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.math.BigDecimal;

/**
 * Created by larry on 3/23/2015.
 */
@Entity
@Table(name = "budget_allocations")
public class BudgetAllocation extends ModelBase {

    @Column(name = "amount")
    private BigDecimal amount;

    @ManyToOne
    private BudgetItem item;

    @ManyToOne
    private AccountingPeriod ap;

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BudgetItem getItem() {
        return item;
    }

    public void setItem(BudgetItem item) {
        this.item = item;
    }

    public AccountingPeriod getAp() {
        return ap;
    }

    public void setAp(AccountingPeriod ap) {
        this.ap = ap;
    }
}
