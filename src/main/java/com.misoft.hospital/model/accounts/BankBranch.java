package com.misoft.hospital.model.accounts;

import com.misoft.hospital.model.base.ModelBase;
import com.misoft.hospital.model.shared.Status;
import org.hibernate.annotations.Filter;

import javax.persistence.*;

/**
 * Created by larry on 3/21/2015.
 */
@Entity
@Table(name = "bank_branches")
@Filter(name = "filterByDeleted")
public class BankBranch extends ModelBase {
    @Column(name = "name")
    private String name;

    @Column(name = "code",unique = true)
    private String code;

    @Column(name = "physical_location")
    private String location;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private Status status;

    @ManyToOne
    private Bank bank;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Bank getBank() {
        return bank;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }
}
