package com.misoft.hospital.model.accounts;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * @author larry
 *
 */
@Entity
@Table(name = "payment_lines")
@Filter(name = "filterByDeleted")
public class PaymentLine extends ModelBase {
	
	@Column(name="description")
	private String itemDescription;

    @Column(name="amount")
	private BigDecimal amount;

    @Column(name="tax")
    private BigDecimal tax;

    @JoinColumn(name = "payment", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
	private Payment payment;

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public Payment getPayment() {
        return payment;
    }

    public void setPayment(Payment invoice) {
        this.payment = payment;
    }

    public BigDecimal getTax() {
        return tax;
    }

    public void setTax(BigDecimal tax) {
        this.tax = tax;
    }
}
