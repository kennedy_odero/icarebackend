package com.misoft.hospital.model.accounts;

import com.misoft.hospital.data.gdto.GenericDTO;
import com.misoft.hospital.model.base.ModelBase;
import com.misoft.hospital.model.shared.YesNo;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by kodero on 3/3/15.
 */
@Entity
@Table(name = "accounts")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "account_type")
@Filter(name = "filterByDeleted")
public class Account extends ModelBase {

    @Column(name = "name")
    private String name;

    @Column(name = "code")
    private String code;

    @Column(name = "account_category")
    @Enumerated(EnumType.STRING)
    private AccountCategory accountCategory;
    
    @Column(name = "lft")
	private int left;
	
	@Column(name = "rgt")
	private int right;
	
	@ManyToOne
	private FinancialReportingHeaderDetail frhd;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "status")
	private AccountStatus status;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "cf_header")
	private CashflowHeaderType cashflowHeaderType;
	
	@Column(name = "revalue_acct")
	@Enumerated(EnumType.STRING)
	private YesNo revalueAccnt;
	
	@JoinColumn(name = "srcCurrency_id")
	@ManyToOne
	private Currency srcCurrency;
	
	@JoinColumn(name = "revalAcct_id")
	@ManyToOne
	private Account revalAcct;
	
	@Column(name = "active")
	private int active;
	
	@Column(name = "taxable")
	private YesNo isTaxable;
	
	@Column(name = "cashbook")
	private YesNo isCashbook;
	
	@JoinColumn(name = "cashflowSubHeader_id")
	@ManyToOne
	private CashflowSubHeader cashflowSubHeader;
	
	@JoinColumn(name = "cashflowHeader_id")
	@ManyToOne
	private CashflowHeader cashflowHeader;
	
	private int lvl;

    public int getLvl() {
		return lvl;
	}



	public void setLvl(int lvl) {
		this.lvl = lvl;
	}



	@Column(name = "from_date")
    private Date fromDate;

    @Column(name = "thru_date")
    private Date thruDate;
	


	public GenericDTO genericDTO() {
        GenericDTO dto = new GenericDTO(Account.class.getName());
        dto.addString("id", getId());
        dto.addString("name", getName());
        dto.addString("code", getCode());
        dto.addString("accountCategory", getAccountCategory()  == null ? "" : getAccountCategory().name());
        dto.addInt("left", getLeft());
        dto.addInt("right", getRight());
        dto.addString("frhd", getFrhd() == null ? "" :  getFrhd().getName());
        dto.addString("status", getStatus().name());
        return dto;
    }



	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public String getCode() {
		return code;
	}



	public void setCode(String code) {
		this.code = code;
	}



	public AccountCategory getAccountCategory() {
		return accountCategory;
	}



	public void setAccountCategory(AccountCategory accountCategory) {
		this.accountCategory = accountCategory;
	}



	public int getLeft() {
		return left;
	}



	public void setLeft(int left) {
		this.left = left;
	}



	public int getRight() {
		return right;
	}



	public void setRight(int right) {
		this.right = right;
	}



	public FinancialReportingHeaderDetail getFrhd() {
		return frhd;
	}



	public void setFrhd(FinancialReportingHeaderDetail frhd) {
		this.frhd = frhd;
	}



	public AccountStatus getStatus() {
		return status;
	}



	public void setStatus(AccountStatus status) {
		this.status = status;
	}



	public CashflowHeaderType getCashflowHeaderType() {
		return cashflowHeaderType;
	}



	public void setCashflowHeaderType(CashflowHeaderType cashflowHeaderType) {
		this.cashflowHeaderType = cashflowHeaderType;
	}



	public YesNo getRevalueAccnt() {
		return revalueAccnt;
	}



	public void setRevalueAccnt(YesNo revalueAccnt) {
		this.revalueAccnt = revalueAccnt;
	}



	public Currency getSrcCurrency() {
		return srcCurrency;
	}



	public void setSrcCurrency(Currency srcCurrency) {
		this.srcCurrency = srcCurrency;
	}



	public Account getRevalAcct() {
		return revalAcct;
	}



	public void setRevalAcct(Account revalAcct) {
		this.revalAcct = revalAcct;
	}



	public int getActive() {
		return active;
	}



	public void setActive(int active) {
		this.active = active;
	}



	public YesNo getIsTaxable() {
		return isTaxable;
	}



	public void setIsTaxable(YesNo isTaxable) {
		this.isTaxable = isTaxable;
	}



	public CashflowSubHeader getCashflowSubHeader() {
		return cashflowSubHeader;
	}



	public void setCashflowSubHeader(CashflowSubHeader cashflowSubHeader) {
		this.cashflowSubHeader = cashflowSubHeader;
	}



	public CashflowHeader getCashflowHeader() {
		return cashflowHeader;
	}



	public void setCashflowHeader(CashflowHeader cashflowHeader) {
		this.cashflowHeader = cashflowHeader;
	}



	public Date getFromDate() {
		return fromDate;
	}



	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}



	public Date getThruDate() {
		return thruDate;
	}



	public void setThruDate(Date thruDate) {
		this.thruDate = thruDate;
	}



	public YesNo getIsCashbook() {
		return isCashbook;
	}



	public void setIsCashbook(YesNo isCashbook) {
		this.isCashbook = isCashbook;
	}

	
}
