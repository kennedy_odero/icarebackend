package com.misoft.hospital.model.accounts;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by larry on 3/15/2015.
 */
@Entity
@Table(name = "asset_classes")
@Filter(name = "filterByDeleted")
public class AssetClass extends ModelBase {
    @Column(name="name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "depreciation_factor")
    private BigDecimal depreciationFactor;

    @JoinColumn(name = "dr_acq_account")
    @ManyToOne(fetch = FetchType.LAZY)
    private Account drAcqAccount;

    @JoinColumn(name = "cr_acq_account")
    @ManyToOne(fetch = FetchType.LAZY)
    private Account crAcqAccount;

    @JoinColumn(name = "dr_account")
    @ManyToOne(fetch = FetchType.LAZY)
    private Account drAccount;

    @JoinColumn(name = "cr_account")
    @ManyToOne(fetch = FetchType.LAZY)
    private Account crAccount;

    @Column(name = "depreciation_method")
    private DepreciationMethod depreciationMethod;

    @Column(name = "months_to_depreciate")
    private int monthsToDepreciate;

    @OneToMany(mappedBy = "assetClass",cascade = {CascadeType.ALL})
    @Filter(name = "filterByDeleted")
    private Collection<FixedAsset> fixedAssets = new ArrayList<>();


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getDepreciationFactor() {
        return depreciationFactor;
    }

    public void setDepreciationFactor(BigDecimal depreciationFactor) {
        this.depreciationFactor = depreciationFactor;
    }

    public Account getDrAcqAccount() {
        return drAcqAccount;
    }

    public void setDrAcqAccount(Account drAcqAccount) {
        this.drAcqAccount = drAcqAccount;
    }

    public Account getCrAcqAccount() {
        return crAcqAccount;
    }

    public void setCrAcqAccount(Account crAcqAccount) {
        this.crAcqAccount = crAcqAccount;
    }

    public Account getDrAccount() {
        return drAccount;
    }

    public void setDrAccount(Account drAccount) {
        this.drAccount = drAccount;
    }

    public Account getCrAccount() {
        return crAccount;
    }

    public void setCrAccount(Account crAccount) {
        this.crAccount = crAccount;
    }

    public DepreciationMethod getDepreciationMethod() {
        return depreciationMethod;
    }

    public void setDepreciationMethod(DepreciationMethod depreciationMethod) {
        this.depreciationMethod = depreciationMethod;
    }

    public int getMonthsToDepreciate() {
        return monthsToDepreciate;
    }

    public void setMonthsToDepreciate(int monthsToDepreciate) {
        this.monthsToDepreciate = monthsToDepreciate;
    }

    public Collection<FixedAsset> getFixedAssets() {
        return fixedAssets;
    }

    public void setFixedAssets(Collection<FixedAsset> fixedAssets) {
        this.fixedAssets = fixedAssets;
    }
}
