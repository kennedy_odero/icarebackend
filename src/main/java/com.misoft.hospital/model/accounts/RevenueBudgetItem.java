package com.misoft.hospital.model.accounts;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Created by larry on 3/22/2015.
 */
@Entity
@DiscriminatorValue("REVENUE")
public class RevenueBudgetItem extends BudgetItem {
}
