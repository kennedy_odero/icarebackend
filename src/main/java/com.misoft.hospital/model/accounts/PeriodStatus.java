package com.misoft.hospital.model.accounts;

/**
 * Created by larry on 3/23/2015.
 */
public enum PeriodStatus {
    OPEN,
    SOFT_CLOSED,
    CLOSED,
    PERMANENTLY_CLOSED
}
