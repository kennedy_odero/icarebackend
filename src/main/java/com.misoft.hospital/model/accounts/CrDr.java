package com.misoft.hospital.model.accounts;

/**
 * Created by larry on 3/27/2015.
 */
public enum CrDr {
    Cr,
    Dr
}
