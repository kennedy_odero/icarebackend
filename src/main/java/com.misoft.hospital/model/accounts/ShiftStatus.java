package com.misoft.hospital.model.accounts;

/**
 * Created by kodero on 9/13/15.
 */
public enum ShiftStatus {
    OPEN,
    CLOSED,
    CANCELLED
}
