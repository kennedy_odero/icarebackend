package com.misoft.hospital.model.mortuary;

import com.misoft.hospital.model.base.ModelBase;
import com.misoft.hospital.model.healthcare.ServiceCenter;

import javax.persistence.*;

/**
 * Created by kodero on 6/10/15.
 */
@Entity
@Table(name = "storage_rooms")
public class StorageRoom extends ModelBase {

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @JoinColumn(name = "room", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private ServiceCenter serviceCenter;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ServiceCenter getServiceCenter() {
        return serviceCenter;
    }

    public void setServiceCenter(ServiceCenter serviceCenter) {
        this.serviceCenter = serviceCenter;
    }
}
