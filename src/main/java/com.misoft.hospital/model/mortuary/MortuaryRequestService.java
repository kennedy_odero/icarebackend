package com.misoft.hospital.model.mortuary;

import com.misoft.hospital.model.base.ModelBase;

import javax.persistence.*;

/**
 * Created by kodero on 12/12/15.
 */
@Entity
@Table(name = "mortuary_request_services")
public class MortuaryRequestService extends ModelBase{

    @JoinColumn(name = "setup", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private MortuaryService mortuaryService;

    @Column(name = "qty")
    private Integer quantity;

    @JoinColumn(name = "mortuary_request", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private MortuaryRequest mortuaryRequest;

    public MortuaryService getMortuaryService() {
        return mortuaryService;
    }

    public void setMortuaryService(MortuaryService mortuaryService) {
        this.mortuaryService = mortuaryService;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public MortuaryRequest getMortuaryRequest() {
        return mortuaryRequest;
    }

    public void setMortuaryRequest(MortuaryRequest mortuaryRequest) {
        this.mortuaryRequest = mortuaryRequest;
    }
}
