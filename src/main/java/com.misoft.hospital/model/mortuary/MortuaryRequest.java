package com.misoft.hospital.model.mortuary;

import com.misoft.hospital.model.healthcare.ServiceRequest;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Created by kodero on 11/17/15.
 */
@Entity
@DiscriminatorValue("MORTUARY")
public class MortuaryRequest extends ServiceRequest{

    @Column(name = "deceased_on")
    private Date deceasedOn;

    @Column(name = "date_from")
    private Date dateFrom;

    @Column(name = "date_to")
    private Date dateTo;

    @OneToMany(mappedBy = "mortuaryRequest", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @org.hibernate.annotations.Filter(name = "filterByDeleted")
    private Collection<MortuaryRequestService> mortuaryServices;

    @Column(name = "date_released")
    private Date dateReleased;

    @Column(name = "release_comments")
    private String releaseComments;

    @JoinColumn(name = "storage_room", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private StorageRoom storageRoom;

    @JoinColumn(name = "storage_unit", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private StorageUnit storageUnit;

    @OneToMany(mappedBy = "mortuaryRequest", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @Filter(name = "filterByDeleted")
    @OrderBy("created_at asc")
    private List<MortuaryStorageUnit> mortuaryStorageRooms = new ArrayList<>();

    public Date getDeceasedOn() {
        return deceasedOn;
    }

    public void setDeceasedOn(Date deceasedOn) {
        this.deceasedOn = deceasedOn;
    }

    public Collection<MortuaryRequestService> getMortuaryServices() {
        return mortuaryServices;
    }

    public void setMortuaryServices(Collection<MortuaryRequestService> mortuaryServices) {
        this.mortuaryServices = mortuaryServices;
    }

    public Date getDateReleased() {
        return dateReleased;
    }

    public void setDateReleased(Date dateReleased) {
        this.dateReleased = dateReleased;
    }

    public String getReleaseComments() {
        return releaseComments;
    }

    public void setReleaseComments(String releaseComments) {
        this.releaseComments = releaseComments;
    }

    public StorageRoom getStorageRoom() {
        return storageRoom;
    }

    public void setStorageRoom(StorageRoom storageRoom) {
        this.storageRoom = storageRoom;
    }

    public StorageUnit getStorageUnit() {
        return storageUnit;
    }

    public void setStorageUnit(StorageUnit storageUnit) {
        this.storageUnit = storageUnit;
    }

    public List<MortuaryStorageUnit> getMortuaryStorageRooms() {
        return mortuaryStorageRooms;
    }

    public void setMortuaryStorageRooms(List<MortuaryStorageUnit> mortuaryStorageRooms) {
        this.mortuaryStorageRooms = mortuaryStorageRooms;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public void addMortuaryStorageRoom(MortuaryStorageUnit mortuaryStorageRoom) {
        if(mortuaryStorageRoom != null){
            mortuaryStorageRoom.setMortuaryRequest(this);
            getMortuaryStorageRooms().add(mortuaryStorageRoom);
        }
    }
}
