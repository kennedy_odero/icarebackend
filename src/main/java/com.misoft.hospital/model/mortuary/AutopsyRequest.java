package com.misoft.hospital.model.mortuary;

import com.misoft.hospital.model.healthcare.ServiceRequest;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.Date;

/**
 * Created by kodero on 6/10/15.
 */
@Entity
@DiscriminatorValue("AUTOPSY")
public class AutopsyRequest extends ServiceRequest {

    @Column(name = "date_of_death")
    private Date dateOfDeath;

    @Column(name = "party_name")
    private String interestedPartyName;

    @Column(name = "party_address")
    private String interestedPartyAddress;

    @Column(name = "relationship")
    private String relationToDeceased;

    //report details
    @Column(name = "diagnoses", columnDefinition = "text")
    private String diagnoses;

    @Column(name = "toxicology", columnDefinition = "text")
    private String toxicology;

    @Column(name = "circums_of_death", columnDefinition = "text")
    private String circumstancesOfDeath;

    @Column(name = "identification", columnDefinition = "text")
    private String identification;

    @Column(name = "descr_clothing", columnDefinition = "text")
    private String descriptionAndClothing;

    @Column(name = "autopsy_findings", columnDefinition = "text")
    private String grossAutopsyFindings;

    @Column(name = "microscopy", columnDefinition = "text")
    private String microscopy;

    @Column(name = "pathologist_opinion", columnDefinition = "text")
    private String opinionOfPathologist;

    public String getDiagnoses() {
        return diagnoses;
    }

    public void setDiagnoses(String diagnoses) {
        this.diagnoses = diagnoses;
    }

    public String getToxicology() {
        return toxicology;
    }

    public void setToxicology(String toxicology) {
        this.toxicology = toxicology;
    }

    public String getCircumstancesOfDeath() {
        return circumstancesOfDeath;
    }

    public void setCircumstancesOfDeath(String circumstancesOfDeath) {
        this.circumstancesOfDeath = circumstancesOfDeath;
    }

    public String getIdentification() {
        return identification;
    }

    public void setIdentification(String identification) {
        this.identification = identification;
    }

    public String getDescriptionAndClothing() {
        return descriptionAndClothing;
    }

    public void setDescriptionAndClothing(String descriptionAndClothing) {
        this.descriptionAndClothing = descriptionAndClothing;
    }

    public String getGrossAutopsyFindings() {
        return grossAutopsyFindings;
    }

    public void setGrossAutopsyFindings(String grossAutopsyFindings) {
        this.grossAutopsyFindings = grossAutopsyFindings;
    }

    public String getMicroscopy() {
        return microscopy;
    }

    public void setMicroscopy(String microscopy) {
        this.microscopy = microscopy;
    }

    public String getOpinionOfPathologist() {
        return opinionOfPathologist;
    }

    public void setOpinionOfPathologist(String opinionOfPathologist) {
        this.opinionOfPathologist = opinionOfPathologist;
    }

    public Date getDateOfDeath() {
        return dateOfDeath;
    }

    public void setDateOfDeath(Date dateOfDeath) {
        this.dateOfDeath = dateOfDeath;
    }

    public String getInterestedPartyName() {
        return interestedPartyName;
    }

    public void setInterestedPartyName(String interestedPartyName) {
        this.interestedPartyName = interestedPartyName;
    }

    public String getInterestedPartyAddress() {
        return interestedPartyAddress;
    }

    public void setInterestedPartyAddress(String interestedPartyAddress) {
        this.interestedPartyAddress = interestedPartyAddress;
    }

    public String getRelationToDeceased() {
        return relationToDeceased;
    }

    public void setRelationToDeceased(String relationToDeceased) {
        this.relationToDeceased = relationToDeceased;
    }
}
