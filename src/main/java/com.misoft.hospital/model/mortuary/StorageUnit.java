package com.misoft.hospital.model.mortuary;

import com.misoft.hospital.model.base.ModelBase;
import com.misoft.hospital.model.setup.Product;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by kodero on 6/10/15.
 */
@Entity
@Table(name = "storage_units")
public class StorageUnit extends ModelBase {

    @Column(name = "unit_code")
    private String unitCode;

    @Column(name = "info")
    private String info;

    @JoinColumn(name = "room", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private StorageRoom storageRoom;

    //@NotNull
    @JoinColumn(name = "product", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    Product product;

    public String getUnitCode() {
        return unitCode;
    }

    public void setUnitCode(String unitCode) {
        this.unitCode = unitCode;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public StorageRoom getStorageRoom() {
        return storageRoom;
    }

    public void setStorageRoom(StorageRoom storageRoom) {
        this.storageRoom = storageRoom;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
}
