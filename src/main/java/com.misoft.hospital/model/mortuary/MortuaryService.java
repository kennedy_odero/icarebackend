package com.misoft.hospital.model.mortuary;

import com.misoft.hospital.model.base.ModelBase;
import com.misoft.hospital.model.setup.Product;

import javax.persistence.*;

/**
 * Created by kodero on 12/10/15.
 */
@Entity
@Table(name = "mortuary_services")
public class MortuaryService extends ModelBase{

    @Column(name = "name")
    private String name;

    @JoinColumn(name = "product", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Product product;

    @JoinColumn(name = "request", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private MortuaryRequest mortuaryRequest;

    @Column(name = "service_type")
    @Enumerated(EnumType.STRING)
    private ServiceType serviceType;

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MortuaryRequest getMortuaryRequest() {
        return mortuaryRequest;
    }

    public void setMortuaryRequest(MortuaryRequest mortuaryRequest) {
        this.mortuaryRequest = mortuaryRequest;
    }

    public ServiceType getServiceType() {
        return serviceType;
    }

    public void setServiceType(ServiceType serviceType) {
        this.serviceType = serviceType;
    }
}
