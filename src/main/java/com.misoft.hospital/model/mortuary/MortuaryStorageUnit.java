package com.misoft.hospital.model.mortuary;

import com.misoft.hospital.model.base.ModelBase;
import com.misoft.hospital.model.healthcare.ServiceRequestItem;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by kodero on 5/25/16.
 */
@Entity
@Table(name = "mort_storage_rooms")
public class MortuaryStorageUnit extends ServiceRequestItem{
    @Column(name = "date_from")
    private Date dateFrom;

    @Column(name = "date_to")
    private Date dateTo;

    @JoinColumn(name = "storage_room", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private StorageRoom storageRoom;

    @JoinColumn(name = "storage_unit", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private StorageUnit storageUnit;

    @JoinColumn(name = "mortuary_request", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private MortuaryRequest mortuaryRequest;

    public MortuaryStorageUnit() {

    }

    public MortuaryStorageUnit(StorageUnit storageUnit, StorageRoom storageRoom, Date dateFrom) {
        setStorageUnit(storageUnit);
        setStorageRoom(storageRoom);
        setDateFrom(dateFrom);
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public StorageRoom getStorageRoom() {
        return storageRoom;
    }

    public void setStorageRoom(StorageRoom storageRoom) {
        this.storageRoom = storageRoom;
    }

    public StorageUnit getStorageUnit() {
        return storageUnit;
    }

    public void setStorageUnit(StorageUnit storageUnit) {
        this.storageUnit = storageUnit;
    }

    public MortuaryRequest getMortuaryRequest() {
        return mortuaryRequest;
    }

    public void setMortuaryRequest(MortuaryRequest mortuaryRequest) {
        this.mortuaryRequest = mortuaryRequest;
    }
}
