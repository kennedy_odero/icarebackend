package com.misoft.hospital.model.mortuary;

/**
 * Created by kodero on 6/10/15.
 */
public enum MortuaryLevel {
    LEVEL_1,
    LEVEL_2,
    LEVEL_3
}
