package com.misoft.hospital.model.stats;

/**
 * Created by kodero on 12/28/15.
 */
public enum Interval {
    DAY{
        public String getSqlInterval(){
           return "0 DAY";
        }
    },
    WEEK{
        public String getSqlInterval(){
            return "7 DAY";
        }
    },
    MONTH{
        public String getSqlInterval(){
            return "1 MONTH";
        }
    },
    THREE_MONTH{
        public String getSqlInterval(){
            return "3 MONTH";
        }
    },
    SIX_MONTH{
        public String getSqlInterval(){
            return "6 MONTH";
        }
    },
    TWELVE_MONTH{
        public String getSqlInterval(){
            return "12 MONTH";
        }
    };

    public abstract String getSqlInterval();
}
