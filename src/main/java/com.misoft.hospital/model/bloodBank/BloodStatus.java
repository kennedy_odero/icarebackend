package com.misoft.hospital.model.bloodBank;

/**
 * Created by kodero on 4/12/15.
 */
public enum BloodStatus {
    USABLE,
    UNUSABLE,
    USED
}
