package com.misoft.hospital.model.bloodBank;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.*;

/**
 * Created by kodero on 9/9/15.

 */
@Entity
@Table(name = "blood_mvt_items")
@Filter(name = "filterByDeleted")
public class BloodMovementItem extends ModelBase{

    @JoinColumn(name = "blood_mvt", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private BloodProductRequest bloodProductRequest;

    @JoinColumn(name = "blood_product", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private BloodProduct product;

    @Column(name = "qty")
    private Integer quantity;

    public BloodProduct getProduct() {
        return product;
    }

    public void setProduct(BloodProduct product) {
        this.product = product;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public BloodProductRequest getBloodProductRequest() {
        return bloodProductRequest;
    }

    public void setBloodProductRequest(BloodProductRequest bloodProductRequest) {
        this.bloodProductRequest = bloodProductRequest;
    }
}
