package com.misoft.hospital.model.bloodBank;

import com.drew.lang.annotations.NotNull;
import com.misoft.hospital.model.base.ModelBase;
import com.misoft.hospital.model.stores.OrderLine;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;

/**
 * Created by kodero on 9/8/15.
 */
@Entity
@Table(name = "collection_batches")
public class CollectionBatch extends ModelBase{

    @NotNull
    @Column(name = "batch_no", nullable = false)
    private Long batchNo;

    @NotNull
    @Column(name = "date_of_coll", nullable = false)
    private Date dateOfCollection;

    @Column(name = "source")
    @Enumerated(EnumType.STRING)
    private BloodSource source;

    @NotNull
    @Column(name = "quantity", nullable = false)
    private Integer quantity;

    @JoinColumn(name = "blood_group", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private BloodGroup bloodGroup;

    @JoinColumn(name = "rhesus", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Rhesus rhesus;

    @OneToMany(mappedBy = "collectionBatch", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @Filter(name = "filterByDeleted")
    private Collection<OrderLine> orderLines;

    @OneToMany(mappedBy = "collectionBatch", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @Filter(name = "filterByDeleted")
    private Collection<SerialBatch> serialBatches;

    @OneToMany(mappedBy = "collectionBatch", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @Filter(name = "filterByDeleted")
    private Collection<BloodUnit> units;

    @JoinColumn(name = "blood_bank", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private BloodBank bloodBank;

    public Long getBatchNo() {
        return batchNo;
    }

    public void setBatchNo(Long batchNo) {
        this.batchNo = batchNo;
    }

    public Date getDateOfCollection() {
        return dateOfCollection;
    }

    public void setDateOfCollection(Date dateOfCollection) {
        this.dateOfCollection = dateOfCollection;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public BloodGroup getBloodGroup() {
        return bloodGroup;
    }

    public void setBloodGroup(BloodGroup bloodGroup) {
        this.bloodGroup = bloodGroup;
    }

    public Rhesus getRhesus() {
        return rhesus;
    }

    public void setRhesus(Rhesus rhesus) {
        this.rhesus = rhesus;
    }

    public Collection<BloodUnit> getUnits() {
        return units;
    }

    public void setUnits(Collection<BloodUnit> units) {
        this.units = units;
    }

    public BloodBank getBloodBank() {
        return bloodBank;
    }

    public void setBloodBank(BloodBank bloodBank) {
        this.bloodBank = bloodBank;
    }

    public Collection<OrderLine> getOrderLines() {
        return orderLines;
    }

    public void setOrderLines(Collection<OrderLine> orderLines) {
        this.orderLines = orderLines;
    }

    public Collection<SerialBatch> getSerialBatches() {
        return serialBatches;
    }

    public void setSerialBatches(Collection<SerialBatch> serialBatches) {
        this.serialBatches = serialBatches;
    }

    public BloodSource getSource() {
        return source;
    }

    public void setSource(BloodSource source) {
        this.source = source;
    }
}
