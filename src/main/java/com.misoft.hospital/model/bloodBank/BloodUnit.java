package com.misoft.hospital.model.bloodBank;

import com.misoft.hospital.model.base.ModelBase;
import com.misoft.hospital.model.setup.UnitOfMeasure;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

/**
 * Created by kodero on 4/12/15.
 */
@Entity
@Table(name = "blood_units")
@Filter(name = "filterByDeleted")
public class BloodUnit extends ModelBase {

    @JoinColumn(name = "batch", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private CollectionBatch collectionBatch;

    @Column(name = "collection_no")
    private Long collectionNo;

    @Column(name = "collection_date")
    private Date collectionDate;

    @Column(name = "expiry")
    private Date expiry;

    @Column(name = "shelf_life")
    private Integer shelfLife;

    @JoinColumn(name = "phenotype", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Phenotype phenotype;

    @JoinColumn(name = "genotype", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Genotype genotype;

    @JoinColumn(name = "rhesus", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Rhesus rhesus;

    @JoinColumn(name = "blood_product", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private BloodProduct product;

    @JoinColumn(name = "blood_group", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private BloodGroup bloodGroup;

    @Column(name = "status")
    private BloodStatus status;

    @Column(name = "quantity")
    private BigDecimal quantity;

    @JoinColumn(name = "uom", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private UnitOfMeasure unitOfMeasure;

    @OneToMany(mappedBy = "bloodUnit", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @Filter(name = "filterByDeleted")
    private Collection<UnitScreeningTest> tests;

    public Long getCollectionNo() {
        return collectionNo;
    }

    public void setCollectionNo(Long collectionNo) {
        this.collectionNo = collectionNo;
    }

    public Date getCollectionDate() {
        return collectionDate;
    }

    public void setCollectionDate(Date collectionDate) {
        this.collectionDate = collectionDate;
    }

    public Date getExpiry() {
        return expiry;
    }

    public void setExpiry(Date expiry) {
        this.expiry = expiry;
    }

    public Phenotype getPhenotype() {
        return phenotype;
    }

    public void setPhenotype(Phenotype phenotype) {
        this.phenotype = phenotype;
    }

    public Genotype getGenotype() {
        return genotype;
    }

    public void setGenotype(Genotype genotype) {
        this.genotype = genotype;
    }

    public Rhesus getRhesus() {
        return rhesus;
    }

    public void setRhesus(Rhesus rhesus) {
        this.rhesus = rhesus;
    }

    public BloodProduct getProduct() {
        return product;
    }

    public void setProduct(BloodProduct product) {
        this.product = product;
    }

    public BloodStatus getStatus() {
        return status;
    }

    public void setStatus(BloodStatus status) {
        this.status = status;
    }

    public Integer getShelfLife() {
        return shelfLife;
    }

    public void setShelfLife(Integer shelfLife) {
        this.shelfLife = shelfLife;
    }

    public BloodGroup getBloodGroup() {
        return bloodGroup;
    }

    public void setBloodGroup(BloodGroup bloodGroup) {
        this.bloodGroup = bloodGroup;
    }

    public Collection<UnitScreeningTest> getTests() {
        return tests;
    }

    public void setTests(Collection<UnitScreeningTest> tests) {
        this.tests = tests;
    }

    public CollectionBatch getCollectionBatch() {
        return collectionBatch;
    }

    public void setCollectionBatch(CollectionBatch collectionBatch) {
        this.collectionBatch = collectionBatch;
    }

    public UnitOfMeasure getUnitOfMeasure() {
        return unitOfMeasure;
    }

    public void setUnitOfMeasure(UnitOfMeasure unitOfMeasure) {
        this.unitOfMeasure = unitOfMeasure;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }
}
