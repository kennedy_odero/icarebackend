package com.misoft.hospital.model.bloodBank;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.*;

/**
 * Created by kodero on 9/8/15.
 */
@Entity
@Table(name = "unit_screening")
@Filter(name = "filterByDeleted")
public class UnitScreeningTest extends ModelBase{

    @JoinColumn(name = "test", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private ScreeningTest screeningTest;

    @JoinColumn(name = "unit", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private BloodUnit bloodUnit;

    @Column(name = "test_result")
    @Enumerated(EnumType.STRING)
    private ScreeningResult testResult;

    public ScreeningTest getScreeningTest() {
        return screeningTest;
    }

    public void setScreeningTest(ScreeningTest screeningTest) {
        this.screeningTest = screeningTest;
    }

    public BloodUnit getBloodUnit() {
        return bloodUnit;
    }

    public void setBloodUnit(BloodUnit bloodUnit) {
        this.bloodUnit = bloodUnit;
    }

    public ScreeningResult getTestResult() {
        return testResult;
    }

    public void setTestResult(ScreeningResult testResult) {
        this.testResult = testResult;
    }
}
