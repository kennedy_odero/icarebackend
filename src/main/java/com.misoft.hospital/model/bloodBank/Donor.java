package com.misoft.hospital.model.bloodBank;

import com.misoft.hospital.model.setup.Person;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Created by kodero on 4/12/15.
 */
@Entity
@DiscriminatorValue("DONOR")
public class Donor extends Person{

    @Column(name = "occupation")
    private String occupation;

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }
}
