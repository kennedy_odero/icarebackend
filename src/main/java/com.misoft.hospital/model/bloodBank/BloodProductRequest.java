package com.misoft.hospital.model.bloodBank;

import com.misoft.hospital.model.base.ModelBase;
import com.misoft.hospital.model.healthcare.ServiceCenter;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;

/**
 * Created by kodero on 9/9/15.
 */
@Entity
@Table(name = "blood_mvt_requests")
@Filter(name = "filterByDeleted")
public class BloodProductRequest extends ModelBase{

    @Column(name = "mvt_no")
    private Long mvtNo;

    @Column(name = "date_ordered")
    private Date dateRequested;

    @JoinColumn(name = "blood_bank", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private BloodBank bloodBank;

    @JoinColumn(name = "service_center", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private ServiceCenter serviceCenter;

    @OneToMany(mappedBy = "bloodProductRequest", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @Filter(name = "filterByDeleted")
    private Collection<BloodMovementItem> movementItems;

    public Long getMvtNo() {
        return mvtNo;
    }

    public void setMvtNo(Long mvtNo) {
        this.mvtNo = mvtNo;
    }

    public Date getDateRequested() {
        return dateRequested;
    }

    public void setDateRequested(Date dateRequested) {
        this.dateRequested = dateRequested;
    }

    public Collection<BloodMovementItem> getMovementItems() {
        return movementItems;
    }

    public void setMovementItems(Collection<BloodMovementItem> movementItems) {
        this.movementItems = movementItems;
    }

    public BloodBank getBloodBank() {
        return bloodBank;
    }

    public void setBloodBank(BloodBank bloodBank) {
        this.bloodBank = bloodBank;
    }

    public ServiceCenter getServiceCenter() {
        return serviceCenter;
    }

    public void setServiceCenter(ServiceCenter serviceCenter) {
        this.serviceCenter = serviceCenter;
    }
}
