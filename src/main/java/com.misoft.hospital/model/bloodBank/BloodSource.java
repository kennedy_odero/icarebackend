package com.misoft.hospital.model.bloodBank;

/**
 * Created by kodero on 9/10/15.
 */
public enum BloodSource {
    DONATION_DRIVE,
    BLOOD_BANK
}
