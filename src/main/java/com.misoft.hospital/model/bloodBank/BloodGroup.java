package com.misoft.hospital.model.bloodBank;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.*;

/**
 * Created by kodero on 9/8/15.
 */
@Entity
@Table(name = "blood_groups")
@Filter(name = "filterByDeleted")
public class BloodGroup extends ModelBase{
    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @JoinColumn(name = "rhesus", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Rhesus rhesus;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Rhesus getRhesus() {
        return rhesus;
    }

    public void setRhesus(Rhesus rhesus) {
        this.rhesus = rhesus;
    }
}
