package com.misoft.hospital.model.bloodBank;

/**
 * Created by kodero on 9/8/15.
 */
public enum ScreeningResult {
    Positive,
    Negative,
    Not_done
}
