package com.misoft.hospital.model.bloodBank;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by kodero on 9/9/15.
 */
@Entity
@Table(name = "blood_banks")
@Filter(name = "filterByDeleted")
public class BloodBank extends ModelBase{

    @Column(name = "name")
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
