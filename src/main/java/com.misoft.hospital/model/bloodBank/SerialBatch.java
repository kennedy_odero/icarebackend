package com.misoft.hospital.model.bloodBank;

import com.misoft.hospital.model.base.ModelBase;
import org.hibernate.annotations.Filter;

import javax.persistence.*;

/**
 * Created by kodero on 9/10/15.
 */
@Entity
@Table(name = "serial_batches")
@Filter(name = "filterByDeleted")
public class SerialBatch extends ModelBase{

    @Column(name = "serial_start")
    private Long serialStart;

    @Column(name = "serial_end")
    private Long serialEnd;

    @JoinColumn(name = "collection_batch", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private CollectionBatch collectionBatch;

    public Long getSerialStart() {
        return serialStart;
    }

    public void setSerialStart(Long serialStart) {
        this.serialStart = serialStart;
    }

    public Long getSerialEnd() {
        return serialEnd;
    }

    public void setSerialEnd(Long serialEnd) {
        this.serialEnd = serialEnd;
    }

    public CollectionBatch getCollectionBatch() {
        return collectionBatch;
    }

    public void setCollectionBatch(CollectionBatch collectionBatch) {
        this.collectionBatch = collectionBatch;
    }
}
