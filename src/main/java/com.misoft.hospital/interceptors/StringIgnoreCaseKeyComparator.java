package com.misoft.hospital.interceptors;

public class StringIgnoreCaseKeyComparator implements KeyComparator<String> {

    private static final long serialVersionUID = 9106900325469360723L;

    public static final StringIgnoreCaseKeyComparator SINGLETON = new StringIgnoreCaseKeyComparator();

    public int hash(String k) {
        return k.toLowerCase().hashCode();
    }

    public boolean equals(String x, String y) {
        return x.equalsIgnoreCase(y);
    }

    public int compare(String o1, String o2) {
        return o1.compareToIgnoreCase(o2);
    }

}