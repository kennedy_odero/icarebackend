/**
 *
 */
package com.misoft.hospital.interceptors;

import com.misoft.hospital.service.admin.UserService;
import com.misoft.hospital.util.annotations.AuthenticationNotRequired;
import org.jboss.resteasy.core.Headers;
import org.jboss.resteasy.core.ResourceMethodInvoker;
import org.jboss.resteasy.core.ServerResponse;

import javax.annotation.Priority;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Context;
import javax.ws.rs.ext.Provider;
import java.lang.reflect.Method;
import java.util.logging.Logger;

/**
 * @author kodero
 */
@Provider
@Priority(11)// run before anyone
public class SecurityInterceptor implements ContainerRequestFilter {

    public static final String ORG_JBOSS_RESTEASY_CORE_RESOURCE_METHOD_INVOKER = "org.jboss.resteasy.core.ResourceMethodInvoker";

    public static final String LOGIN_METHOD = "authenticateUser";

    public static final String LOGOUT_METHOD = "logout";

    public static final String TOKEN = "token";

    public static final String PROGRAM_RESOURCE = "ProgramResource";

    public static final String GET_ALL = "getAll";

    public static final String USER_RESOURCE = "UserResource";

    public static final String CREATE_USER = "createState";

    public static final String ACTIVATE_USER = "activate";

    public static final String INITIATE_PASSWORD_RESET = "sendResetPasswordEmail";

    public static final String CONFIRM_PASSWORD_RESET_USER = "confirmPasswordResetUser";

    public static final String COMPLETE_PASSWORD_RESET = "completePasswordReset";

    public static final String MODEL_DOCUMENT_RESOURCE = "ModelDocumentResource";

    public static final String UPLOAD_FILE = "uploadFile";

    @Context
    private HttpServletRequest servletRequest;

    @Inject
    private transient Logger log;

    @Inject
    private UserService userService;

    @Override
    public void filter(ContainerRequestContext requestContext) {

        String token = servletRequest.getHeader(TOKEN);
        // user not logged-in?
        if (!checkLoggedIn(token, requestContext)) {
            log.info("Authentication Failed!!");
            requestContext.abortWith(new ServerResponse("You need to login first", 401, new Headers<>()));
        }

        //we have a valid user
        /*  if (checkPermission(token, requestContext)) {
            //check persmission bit map
        }*/
    }

    /**
     * Check if the user with the given token has logged in
     *
     * @param token
     * @param requestContext
     * @return true if user is logged in and false otherwise
     */
    private boolean checkLoggedIn(String token, ContainerRequestContext requestContext) {
        ResourceMethodInvoker methodInvoker = (ResourceMethodInvoker) requestContext.getProperty(ORG_JBOSS_RESTEASY_CORE_RESOURCE_METHOD_INVOKER);

        Method method = methodInvoker.getMethod();
        if(method.isAnnotationPresent(AuthenticationNotRequired.class)) return true; //proceed with invocation
        if (token == null) return false;
        try {
            userService.currentUser();
            return userService.isLoggedIn(token);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
