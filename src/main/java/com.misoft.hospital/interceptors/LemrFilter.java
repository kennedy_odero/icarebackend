package com.misoft.hospital.interceptors;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import java.io.IOException;
import java.util.logging.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 4/8/14
 * Time: 4:50 PM
 * To change this template use File | Settings | File Templates.
 */
/*@Provider
@Priority(5)//later, after authentication*/
public class LemrFilter implements ContainerRequestFilter, ContainerResponseFilter {
    private static Logger logger = Logger.getLogger(LemrFilter.class.getName());

    @javax.ws.rs.core.Context
    private HttpServletRequest request;


    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        Context context = Context.newInstance(request);
        final String sessionId = request.getSession().getId();
        requestContext.setProperty(sessionId, context);
        logger.info("stuffed the context into the request, sessionid " + sessionId);

    }

    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) throws IOException {
        //get the session id from the responseContext
        final String sessionId = request.getSession().getId();
        Context context = (Context) requestContext.getProperty(sessionId);
        logger.info(" retrieved the context from the request, session id  " + sessionId + " context " + context);
        context.release();

    }
}
