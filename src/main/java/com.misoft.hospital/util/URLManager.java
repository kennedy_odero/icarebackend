package com.misoft.hospital.util;


public class URLManager {


    public final static int URL_FILE = 4;


    private static final String applicationURL = "http://127.0.0.1:8080/Lemr/resources/modelDocuments";


    /**
     * Returns The Application web context.
     *
     * @return The Application web context.
     */
    public static String getApplicationURL() {
        return applicationURL;
    }


    public static String getSimpleURL(int type, String id) {
        String url = getApplicationURL();

        if (url.endsWith("/")) {
            url = url.substring(0, url.length() - 1);
        }
        switch (type) {

            case URL_FILE:
                url += "/files/download/" + id;
                break;

        }
        return url;
    }


}