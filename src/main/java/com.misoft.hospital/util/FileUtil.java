package com.misoft.hospital.util;


import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOCase;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.filefilter.FalseFileFilter;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.commons.io.filefilter.SuffixFileFilter;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.apache.tika.Tika;

import javax.activation.MimetypesFileTypeMap;
import java.io.*;
import java.math.BigDecimal;
import java.util.*;
import java.util.logging.Logger;

public class FileUtil implements MimeTypes {
    private static Logger log = Logger.getLogger(FileUtil.class.getName());


    public static final String CONTEXT_TOKEN = ",";

    public static final String BASE_CONTEXT = "Attachment";

    private static final MimetypesFileTypeMap MIME_TYPES = new MimetypesFileTypeMap();

    private static Map<String, String> MIME_TYPES_EXTENSIONS = new LinkedHashMap<>();

    static {
        MIME_TYPES_EXTENSIONS.put("bz2", "application/x-bzip");
        MIME_TYPES_EXTENSIONS.put("bz2", "application/x-bzip");
        MIME_TYPES_EXTENSIONS.put("tar.bz2", "application/x-bzip");
        MIME_TYPES_EXTENSIONS.put("tbz2", "application/x-bzip");
        MIME_TYPES_EXTENSIONS.put("tb2", "application/x-bzip");
        MIME_TYPES_EXTENSIONS.put("tar.gz", "application/x-gzip");
        MIME_TYPES_EXTENSIONS.put("tgz", "application/x-gzip");
        MIME_TYPES_EXTENSIONS.put("hqx", "application/mac-binhex40");
        MIME_TYPES_EXTENSIONS.put("cpt", "application/mac-compactpro");
        MIME_TYPES_EXTENSIONS.put("doc", "application/msword");
        MIME_TYPES_EXTENSIONS.put("class", "application/octet-stream");
        MIME_TYPES_EXTENSIONS.put("bin", "application/octet-stream");
        MIME_TYPES_EXTENSIONS.put("dms", "application/octet-stream");
        MIME_TYPES_EXTENSIONS.put("lha", "application/octet-stream");
        MIME_TYPES_EXTENSIONS.put("lzh", "application/octet-stream");
        MIME_TYPES_EXTENSIONS.put("exe", "application/octet-stream");
        MIME_TYPES_EXTENSIONS.put("jsp", "application/jsp");
        MIME_TYPES_EXTENSIONS.put("oda", "application/oda");
        MIME_TYPES_EXTENSIONS.put("pdf", "application/pdf");
        MIME_TYPES_EXTENSIONS.put("ai", "application/postscript");
        MIME_TYPES_EXTENSIONS.put("eps", "application/postscript");
        MIME_TYPES_EXTENSIONS.put("ps", "application/postscript");
        MIME_TYPES_EXTENSIONS.put("ppt", "application/powerpoint");
        MIME_TYPES_EXTENSIONS.put("bcpio", "application/x-bcpio");
        MIME_TYPES_EXTENSIONS.put("vcd", "application/x-cdlink");
        MIME_TYPES_EXTENSIONS.put("Z", "application/x-compress");
        MIME_TYPES_EXTENSIONS.put("cpio", "application/x-cpio");
        MIME_TYPES_EXTENSIONS.put("csh", "application/x-csh");
        MIME_TYPES_EXTENSIONS.put("dcr", "application/x-director");
        MIME_TYPES_EXTENSIONS.put("dir", "application/x-director");
        MIME_TYPES_EXTENSIONS.put("dxr", "application/x-director");
        MIME_TYPES_EXTENSIONS.put("dvi", "application/x-dvi");
        MIME_TYPES_EXTENSIONS.put("gtar", "application/x-gtar");
        MIME_TYPES_EXTENSIONS.put("gz", "application/x-gzip");
        MIME_TYPES_EXTENSIONS.put("hdf", "application/x-hdf");
        MIME_TYPES_EXTENSIONS.put("cgi", "application/x-httpd-cgi");
        MIME_TYPES_EXTENSIONS.put("skp", "application/x-koan");
        MIME_TYPES_EXTENSIONS.put("skd", "application/x-koan");
        MIME_TYPES_EXTENSIONS.put("skt", "application/x-koan");
        MIME_TYPES_EXTENSIONS.put("skm", "application/x-koan");
        MIME_TYPES_EXTENSIONS.put("latex", "application/x-latex");
        MIME_TYPES_EXTENSIONS.put("mif", "application/x-mif");
        MIME_TYPES_EXTENSIONS.put("nc", "application/x-netcdf");
        MIME_TYPES_EXTENSIONS.put("cdf", "application/x-netcdf");
        MIME_TYPES_EXTENSIONS.put("sh", "application/x-sh");
        MIME_TYPES_EXTENSIONS.put("shar", "application/x-shar");
        MIME_TYPES_EXTENSIONS.put("sit", "application/x-stuffit");
        MIME_TYPES_EXTENSIONS.put("sv4cpio", "application/x-sv4cpio");
        MIME_TYPES_EXTENSIONS.put("sv4crc", "application/x-sv4crc");
        MIME_TYPES_EXTENSIONS.put("tar", "application/x-tar");
        MIME_TYPES_EXTENSIONS.put("tcl", "application/x-tcl");
        MIME_TYPES_EXTENSIONS.put("tex", "application/x-tex");
        MIME_TYPES_EXTENSIONS.put("texinfo", "application/x-texinfo");
        MIME_TYPES_EXTENSIONS.put("texi", "application/x-texinfo");
        MIME_TYPES_EXTENSIONS.put("t", "application/x-troff");
        MIME_TYPES_EXTENSIONS.put("tr", "application/x-troff");
        MIME_TYPES_EXTENSIONS.put("roff", "application/x-troff");
        MIME_TYPES_EXTENSIONS.put("man", "application/x-troff-man");
        MIME_TYPES_EXTENSIONS.put("me", "application/x-troff-me");
        MIME_TYPES_EXTENSIONS.put("ms", "application/x-troff-ms");
        MIME_TYPES_EXTENSIONS.put("ustar", "application/x-ustar");
        MIME_TYPES_EXTENSIONS.put("src", "application/x-wais-source");
        MIME_TYPES_EXTENSIONS.put("xml", "text/xml");
        MIME_TYPES_EXTENSIONS.put("ent", "text/xml");
        MIME_TYPES_EXTENSIONS.put("cat", "text/xml");
        MIME_TYPES_EXTENSIONS.put("sty", "text/xml");
        MIME_TYPES_EXTENSIONS.put("dtd", "text/dtd");
        MIME_TYPES_EXTENSIONS.put("xsl", "text/xsl");
        MIME_TYPES_EXTENSIONS.put("jsp", "application/jsp");
        MIME_TYPES_EXTENSIONS.put("zip", "application/zip");
        MIME_TYPES_EXTENSIONS.put("au", "audio/basic");
        MIME_TYPES_EXTENSIONS.put("snd", "audio/basic");
        MIME_TYPES_EXTENSIONS.put("mpga", "audio/mpeg");
        MIME_TYPES_EXTENSIONS.put("mp2", "audio/mpeg");
        MIME_TYPES_EXTENSIONS.put("aif", "audio/x-aiff");
        MIME_TYPES_EXTENSIONS.put("aiff", "audio/x-aiff");
        MIME_TYPES_EXTENSIONS.put("aifc", "audio/x-aiff");
        MIME_TYPES_EXTENSIONS.put("ram", "audio/x-pn-realaudio");
        MIME_TYPES_EXTENSIONS.put("rpm", "audio/x-pn-realaudio-plugin");
        MIME_TYPES_EXTENSIONS.put("ra", "audio/x-realaudio");
        MIME_TYPES_EXTENSIONS.put("wav", "audio/x-wav");
        MIME_TYPES_EXTENSIONS.put("pdb", "chemical/x-pdb");
        MIME_TYPES_EXTENSIONS.put("xyz", "chemical/x-pdb");
        MIME_TYPES_EXTENSIONS.put("gif", "image/gif");
        MIME_TYPES_EXTENSIONS.put("ief", "image/ief");
        MIME_TYPES_EXTENSIONS.put("jpeg", "image/jpeg");
        MIME_TYPES_EXTENSIONS.put("jpg", "image/jpeg");
        MIME_TYPES_EXTENSIONS.put("jpe", "image/jpeg");
        MIME_TYPES_EXTENSIONS.put("png", "image/png");
        MIME_TYPES_EXTENSIONS.put("tiff", "image/tiff");
        MIME_TYPES_EXTENSIONS.put("tif", "image/tiff");
        MIME_TYPES_EXTENSIONS.put("bmp", "image/bmp");
        MIME_TYPES_EXTENSIONS.put("ras", "image/x-cmu-raster");
        MIME_TYPES_EXTENSIONS.put("pnm", "image/x-portable-anymap");
        MIME_TYPES_EXTENSIONS.put("pbm", "image/x-portable-bitmap");
        MIME_TYPES_EXTENSIONS.put("pgm", "image/x-portable-graymap");
        MIME_TYPES_EXTENSIONS.put("ppm", "image/x-portable-pixmap");
        MIME_TYPES_EXTENSIONS.put("rgb", "image/x-rgb");
        MIME_TYPES_EXTENSIONS.put("xbm", "image/x-xbitmap");
        MIME_TYPES_EXTENSIONS.put("xpm", "image/x-xpixmap");
        MIME_TYPES_EXTENSIONS.put("xwd", "image/x-xwindowdump");
        MIME_TYPES_EXTENSIONS.put("html", "text/html");
        MIME_TYPES_EXTENSIONS.put("htm", "text/html");
        MIME_TYPES_EXTENSIONS.put("txt", "text/plain");
        MIME_TYPES_EXTENSIONS.put("rtx", "text/richtext");
        MIME_TYPES_EXTENSIONS.put("tsv", "text/tab-separated-values");
        MIME_TYPES_EXTENSIONS.put("etx", "text/x-setext");
        MIME_TYPES_EXTENSIONS.put("sgml", "text/x-sgml");
        MIME_TYPES_EXTENSIONS.put("sgm", "text/x-sgml");
        MIME_TYPES_EXTENSIONS.put("mpeg", "video/mpeg");
        MIME_TYPES_EXTENSIONS.put("mpg", "video/mpeg");
        MIME_TYPES_EXTENSIONS.put("mpe", "video/mpeg");
        MIME_TYPES_EXTENSIONS.put("qt", "video/quicktime");
        MIME_TYPES_EXTENSIONS.put("mov", "video/quicktime");
        MIME_TYPES_EXTENSIONS.put("avi", "video/x-msvideo");
        MIME_TYPES_EXTENSIONS.put("movie", "video/x-sgi-movie");
        MIME_TYPES_EXTENSIONS.put("ice", "x-conference/x-cooltalk");
        MIME_TYPES_EXTENSIONS.put("wrl", "x-world/x-vrml");
        MIME_TYPES_EXTENSIONS.put("vrml", "x-world/x-vrml");
        MIME_TYPES_EXTENSIONS.put("wml", "text/vnd.wap.wml");
        MIME_TYPES_EXTENSIONS.put("wmlc", "application/vnd.wap.wmlc");
        MIME_TYPES_EXTENSIONS.put("wmls", "text/vnd.wap.wmlscript");
        MIME_TYPES_EXTENSIONS.put("wmlsc", "application/vnd.wap.wmlscriptc");
        MIME_TYPES_EXTENSIONS.put("wbmp", "image/vnd.wap.wbmp");
        MIME_TYPES_EXTENSIONS.put("xls", "application/x-msexcel");
        MIME_TYPES_EXTENSIONS.put("abs", "audio/x-mpeg");
        MIME_TYPES_EXTENSIONS.put("aim", "application/x-aim");
        MIME_TYPES_EXTENSIONS.put("art", "image/x-jg");
        MIME_TYPES_EXTENSIONS.put("asf", "video/x-ms-asf");
        MIME_TYPES_EXTENSIONS.put("asx", "video/x-ms-asf");
        MIME_TYPES_EXTENSIONS.put("avx", "video/x-rad-screenplay");
        MIME_TYPES_EXTENSIONS.put("cer", "application/x-x509-ca-cert");
        MIME_TYPES_EXTENSIONS.put("css", "text/css");
        MIME_TYPES_EXTENSIONS.put("jar", "application/java-archive");
        MIME_TYPES_EXTENSIONS.put("java", "text/plain");
        MIME_TYPES_EXTENSIONS.put("jnlp", "application/x-java-jnlp-file");
        MIME_TYPES_EXTENSIONS.put("js", "text/javascript");
        MIME_TYPES_EXTENSIONS.put("mac", "image/x-macpaint");
        MIME_TYPES_EXTENSIONS.put("mp1", "audio/x-mpeg");
        MIME_TYPES_EXTENSIONS.put("mp2", "audio/x-mpeg");
        MIME_TYPES_EXTENSIONS.put("mp3", "audio/x-mpeg");
        MIME_TYPES_EXTENSIONS.put("mp4", "video/mp4");
        MIME_TYPES_EXTENSIONS.put("odb", "application/vnd.oasis.opendocument.database");
        MIME_TYPES_EXTENSIONS.put("odc", "application/vnd.oasis.opendocument.chart");
        MIME_TYPES_EXTENSIONS.put("odf", "application/vnd.oasis.opendocument.formula");
        MIME_TYPES_EXTENSIONS.put("odg", "application/vnd.oasis.opendocument.graphics");
        MIME_TYPES_EXTENSIONS.put("odi", "application/vnd.oasis.opendocument.image");
        MIME_TYPES_EXTENSIONS.put("odm", "application/vnd.oasis.opendocument.text-master");
        MIME_TYPES_EXTENSIONS.put("odp", "application/vnd.oasis.opendocument.presentation");
        MIME_TYPES_EXTENSIONS.put("ods", "application/vnd.oasis.opendocument.spreadsheet");
        MIME_TYPES_EXTENSIONS.put("odt", "application/vnd.oasis.opendocument.text");
        MIME_TYPES_EXTENSIONS.put("ogg", "application/ogg");
        MIME_TYPES_EXTENSIONS.put("otg", "application/vnd.oasis.opendocument.graphics-template");
        MIME_TYPES_EXTENSIONS.put("oth", "application/vnd.oasis.opendocument.text-web");
        MIME_TYPES_EXTENSIONS.put("otp", "application/vnd.oasis.opendocument.presentation-template");
        MIME_TYPES_EXTENSIONS.put("ots", "application/vnd.oasis.opendocument.spreadsheet-template");
        MIME_TYPES_EXTENSIONS.put("ott", "application/vnd.oasis.opendocument.text-template");
        MIME_TYPES_EXTENSIONS.put("pic", "image/pict");
        MIME_TYPES_EXTENSIONS.put("pict", "image/pict");
        MIME_TYPES_EXTENSIONS.put("pls", "audio/x-scpls");
        MIME_TYPES_EXTENSIONS.put("psd", "image/x-photoshop");
        MIME_TYPES_EXTENSIONS.put("rdf", "application/rdf+xml");
        MIME_TYPES_EXTENSIONS.put("rtf", "application/rtf");
        MIME_TYPES_EXTENSIONS.put("swf", "application/x-shockwave-flash");
        MIME_TYPES_EXTENSIONS.put("xht", "application/xhtml+xml");
        MIME_TYPES_EXTENSIONS.put("xhtml", "application/xhtml+xml");
        MIME_TYPES_EXTENSIONS.put("xslt", "application/xslt+xml");
        MIME_TYPES_EXTENSIONS.put("xul", "application/vnd.mozilla.xul+xml");
        MIME_TYPES_EXTENSIONS.put("svg", "image/svg+xml");
        MIME_TYPES_EXTENSIONS.put("svgz", "image/svg+xml");
        MIME_TYPES_EXTENSIONS.put("vsd", "application/x-visio");
        MIME_TYPES_EXTENSIONS.put("docm", "application/vnd.ms-word.document.macroEnabled.12");
        MIME_TYPES_EXTENSIONS.put("docx", "application/vnd.openxmlformats-officedocument.wordprocessingml.document");
        MIME_TYPES_EXTENSIONS.put("dotm", "application/vnd.ms-word.template.macroEnabled.12");
        MIME_TYPES_EXTENSIONS.put("dotx", "application/vnd.openxmlformats-officedocument.wordprocessingml.template");
        MIME_TYPES_EXTENSIONS.put("potm", "application/vnd.ms-powerpoint.template.macroEnabled.12");
        MIME_TYPES_EXTENSIONS.put("potx", "application/vnd.openxmlformats-officedocument.presentationml.template");
        MIME_TYPES_EXTENSIONS.put("ppam", "application/vnd.ms-powerpoint.addin.macroEnabled.12");
        MIME_TYPES_EXTENSIONS.put("ppsm", "application/vnd.ms-powerpoint.slideshow.macroEnabled.12");
        MIME_TYPES_EXTENSIONS.put("ppsx", "application/vnd.openxmlformats-officedocument.presentationml.slideshow");
        MIME_TYPES_EXTENSIONS.put("pptm", "application/vnd.ms-powerpoint.presentation.macroEnabled.12");
        MIME_TYPES_EXTENSIONS.put("pptx", "application/vnd.openxmlformats-officedocument.presentationml.presentation");
        MIME_TYPES_EXTENSIONS.put("xlam", "application/vnd.ms-excel.addin.macroEnabled.12");
        MIME_TYPES_EXTENSIONS.put("xlsb", "application/vnd.ms-excel.sheet.binary.macroEnabled.12");
        MIME_TYPES_EXTENSIONS.put("xlsm", "application/vnd.ms-excel.sheet.macroEnabled.12");
        MIME_TYPES_EXTENSIONS.put("xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        MIME_TYPES_EXTENSIONS.put("xltm", "application/vnd.ms-excel.template.macroEnabled.12");
        MIME_TYPES_EXTENSIONS.put("xltx", "application/vnd.openxmlformats-officedocument.spreadsheetml.template");
    }


    /**
     * Detects the mime-type of the specified file.
     * <p/>
     * The mime-type is first extracted from its content. If the detection fails or if the file cannot
     * be located by its specified name, then the mime-type is detected from the file extension.
     *
     * @param fileName the name of the file with its path.
     * @return the mime-type of the specified file.
     */
    public static String getMimeType(final String fileName) {
        String mimeType = null;
        final String fileExtension = FileRepositoryManager.getFileExtension(fileName).toLowerCase();
        File file = new File(fileName);
        if (file.exists()) {
            try {
                Tika tika = new Tika();
                mimeType = tika.detect(file);
            } catch (Exception ex) {
                ex.printStackTrace();
                log.info("attachment,FileUtil" +
                        "attachment.MSG_MISSING_MIME_TYPES_PROPERTIES");
            }
        }
        if (!StringUtil.isDefined(mimeType)) {
            if (MIME_TYPES_EXTENSIONS != null) {
                mimeType = MIME_TYPES_EXTENSIONS.get(fileExtension);
            }
        }
        if (!StringUtil.isDefined(mimeType)) {
            mimeType = MIME_TYPES.getContentType(fileName);
        }

        if (mimeType == null) {
            mimeType = MimeTypes.DEFAULT_MIME_TYPE;
        }
        return mimeType;
    }

    /**
     * Create the array of strings this array represents the repertories where the files must be
     * stored.
     *
     * @param context
     * @return
     */
    public static String[] getAttachmentContext(final String context) {
        if (!StringUtil.isDefined(context)) {
            return new String[]{BASE_CONTEXT};
        }
        final StringTokenizer strToken = new StringTokenizer(context, CONTEXT_TOKEN);
        final List<String> folders = new ArrayList<>(10);
        folders.add(BASE_CONTEXT);
        while (strToken.hasMoreElements()) {
            folders.add(strToken.nextToken().trim());
        }
        return folders.toArray(new String[folders.size()]);
    }

    /**
     * Read the content of a file in a byte array.
     *
     * @param file the file to be read.
     * @return the bytes array containing the content of the file.
     * @throws java.io.IOException
     */
    public static byte[] readFile(final File file) throws IOException {
        return FileUtils.readFileToByteArray(file);
    }

    /**
     * Read the content of a file as text (the text is supposed to be in the UTF-8 charset).
     *
     * @param file the file to read.
     * @return the file content as a String.
     * @throws java.io.IOException if an error occurs while reading the file.
     */
    public static String readFileToString(final File file) throws IOException {
        return FileUtils.readFileToString(file);
    }

    /**
     * Write a stream into a file.
     *
     * @param file the file to be written.
     * @param data the data to be written.
     * @throws java.io.IOException
     */
    public static void writeFile(final File file, final InputStream data) throws IOException {
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(file);
            //IOUtils.copy(data, out);
        } finally {
            if (out != null) {
                out.close();
            }
        }
    }

    /**
     * Write a stream into a file.
     *
     * @param file the file to be written.
     * @param data the data to be written.
     * @throws java.io.IOException
     */
    public static void writeFile(final File file, final Reader data) throws IOException {
        FileWriter out = new FileWriter(file);
        try {
            //IOUtils.copy(data, out);
        } finally {
            IOUtils.closeQuietly(out);
        }
    }


    /**
     * Indicates if the OS is from the Microsoft Windows familly
     *
     * @return true if the OS is from the Microsoft Windows familly - false otherwise.
     */
    public static boolean isWindows() {
        return OsEnum.getOS().isWindows();
    }

    /**
     * If 3D document.
     *
     * @param filename the name of the file.
     * @return true or false
     */
    public static boolean isSpinfireDocument(String filename) {
        return MimeTypes.SPINFIRE_MIME_TYPE.equals(getMimeType(filename));
    }

    /**
     * Indicates if the current file is of type archive.
     *
     * @param filename the name of the file.
     * @return true is the file s of type archive - false otherwise.
     */
    public static boolean isArchive(final String filename) {
        return MimeTypes.ARCHIVE_MIME_TYPES.contains(getMimeType(filename));
    }

    /**
     * Indicates if the current file is of type image.
     *
     * @param filename the name of the file.
     * @return true is the file is of type image - false otherwise.
     */
    public static boolean isImage(final String filename) {
        String mimeType = getMimeType(filename);
        if (MimeTypes.DEFAULT_MIME_TYPE.equals(mimeType)) {
            return FilenameUtils.isExtension(filename.toLowerCase(), ImageUtil.IMAGE_EXTENTIONS);
        } else {
            return mimeType.startsWith("image");
        }
    }

    /**
     * Indicates if the current file is of type mail.
     *
     * @param filename the name of the file.
     * @return true is the file is of type mail - false otherwise.
     */
    public static boolean isMail(final String filename) {
        //return FilenameUtils.isExtension(filename, Mail.MAIL_EXTENTIONS);
        return false;
    }

    /**
     * Indicates if the current file is of type PDF.
     *
     * @param filename the name of the file.
     * @return true is the file s of type archive - false otherwise.
     */
    public static boolean isPdf(final String filename) {
        final String mimeType = getMimeType(filename);
        return MimeTypes.PDF_MIME_TYPE.equals(mimeType);
    }

    public static boolean isOpenOfficeCompatible(final String filename) {
        final String mimeType = getMimeType(filename);
        return MimeTypes.OPEN_OFFICE_MIME_TYPES.contains(mimeType) || isMsOfficeExtension(mimeType);
    }

    static boolean isMsOfficeExtension(final String mimeType) {
        return mimeType.startsWith(MimeTypes.WORD_2007_EXTENSION) || mimeType.startsWith(MimeTypes.EXCEL_2007_EXTENSION)
                || mimeType.startsWith(MimeTypes.POWERPOINT_2007_EXTENSION);
    }

    /**
     * Checking that the path doesn't contain relative navigation between paths.
     *//*
  public static void checkPathNotRelative(String path) throws RelativeFileAccessException {
    String unixPath = FilenameUtils.separatorsToUnix(path);
    if (unixPath != null && (unixPath.contains("../") || unixPath.contains("/.."))) {
      throw new RelativeFileAccessException();
    }
  }*/
    public static Collection<File> listFiles(File directory, String[] extensions, boolean recursive) {
        return FileUtils.listFiles(directory, extensions, recursive);
    }

    public static Collection<File> listFiles(File directory, String[] extensions,
                                             boolean caseSensitive, boolean recursive) {
        if (caseSensitive) {
            return listFiles(directory, extensions, recursive);
        }
        IOFileFilter filter;
        if (extensions == null) {
            filter = TrueFileFilter.INSTANCE;
        } else {
            String[] suffixes = new String[extensions.length];
            for (int i = 0; i < extensions.length; i++) {
                suffixes[i] = "." + extensions[i];
            }
            filter = new SuffixFileFilter(suffixes, IOCase.INSENSITIVE);
        }
        return FileUtils.listFiles(directory, filter, (recursive ? TrueFileFilter.INSTANCE
                : FalseFileFilter.INSTANCE));
    }

    /**
     * Forces the deletion of the specified file. If the write property of the file to delete isn't
     * set, this property is then set before deleting.
     *
     * @param fileToDelete file to delete.
     * @throws java.io.IOException if the deletion failed or if the file doesn't exist.
     */
    public static void forceDeletion(File fileToDelete) throws IOException {
        if (fileToDelete.exists() && !fileToDelete.canWrite()) {
            fileToDelete.setWritable(true);
        }
        FileUtils.forceDelete(fileToDelete);
    }

    /**
     * Moves the specified source file to the specified destination. If the destination exists, it is
     * then replaced by the source; if the destination is a directory, then it is deleted with all of
     * its contain.
     *
     * @param source      the file to move.
     * @param destination the destination file of the move.
     * @throws java.io.IOException if the source or the destination is invalid or if an error occurs while
     *                     moving the file.
     */
    public static void moveFile(File source, File destination) throws IOException {
        if (destination.exists()) {
            FileUtils.forceDelete(destination);
        }
        FileUtils.moveFile(source, destination);
    }

    /**
     * Copies the specified source file to the specified destination. If the destination exists, it is
     * then replaced by the source. If the destination can be overwritten, its write property is set
     * before the copy.
     *
     * @param source      the file to copy.
     * @param destination the destination file of the move.
     * @throws java.io.IOException if the source or the destination is invalid or if an error occurs while
     *                     copying the file.
     */
    public static void copyFile(File source, File destination) throws IOException {
        if (destination.exists() && !destination.canWrite()) {
            destination.setWritable(true);
        }
        FileUtils.copyFile(source, destination);
    }

    /*
     * Remove any \ or / from the filename thus avoiding conflicts on the server.
     *
     * @param fileName
     * @return
     */
    public static String getFilename(String fileName) {
        if (!StringUtil.isDefined(fileName)) {
            return "";
        }
        String logicalName = convertPathToServerOS(fileName);
        return logicalName.substring(logicalName.lastIndexOf(File.separator) + 1, logicalName.length());
    }

    private FileUtil() {
    }

    /**
     * Convert a path to the current OS path format.
     *
     * @param undeterminedOsPath
     * @return server OS pah.
     */
    public static String convertPathToServerOS(String undeterminedOsPath) {
        if (undeterminedOsPath == null || !StringUtil.isDefined(undeterminedOsPath)) {
            return "";
        }
        String localPath = undeterminedOsPath;
        localPath = localPath.replace('\\', File.separatorChar).replace('/', File.separatorChar);
        return localPath;
    }

    public static String convertFilePath(File file) {
        if (OsEnum.getOS().isWindows()) {
            return StringUtil.quoteArgument(file.getAbsolutePath());
        }
        String path = file.getAbsolutePath();
        path = path.replaceAll("\\\\", "\\\\\\\\");
        path = path.replaceAll("\\s", "\\\\ ");
        path = path.replaceAll("<", "\\\\<");
        path = path.replaceAll(">", "\\\\>");
        path = path.replaceAll("'", "\\\\'");
        path = path.replaceAll("\"", "\\\\\"");
        path = path.replaceAll("\\{", "\\\\{");
        path = path.replaceAll("}", "\\\\}");
        path = path.replaceAll("\\(", "\\\\(");
        path = path.replaceAll("\\)", "\\\\)");
        path = path.replaceAll("\\[", "\\\\[");
        path = path.replaceAll("\\]", "\\\\]");
        path = path.replaceAll("\\&", "\\\\&");
        path = path.replaceAll("\\|", "\\\\|");
        return path;
    }

    public static boolean deleteEmptyDir(File directory) {
        if (directory.exists() && directory.isDirectory() && directory.list() != null && directory.
                list().length == 0) {
            return directory.delete();
        }
        return false;
    }

    public static String displayFilesize(long fileSizeInBytes) {
        // TODO: Yeah, that could be done smarter..
        //TODO i18n
        if (fileSizeInBytes >= 1073741824) {
            return new BigDecimal(fileSizeInBytes / 1024 / 1024 / 1024) + " GiB";
        } else if (fileSizeInBytes >= 1048576) {
            return new BigDecimal(fileSizeInBytes / 1024 / 1024) + " MiB";
        } else if (fileSizeInBytes >= 1024) {
            return new BigDecimal(fileSizeInBytes / 1024) + " KiB";
        } else {
            return new BigDecimal(fileSizeInBytes) + " Bytes";
        }
    }
}