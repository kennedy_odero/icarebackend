package com.misoft.hospital.util;


import org.apache.commons.io.FilenameUtils;

import java.text.DecimalFormat;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Logger;

/**
 * @author mokua
 */

public class FileRepositoryManager {

    private static Logger log = Logger.getLogger(FileRepositoryManager.class.getName());

    /**
     * path where file icons are located
     */
    public static final String FILE_ICONS_PATH = "C:\\Mokua\\JCR\\docs\\icons";

    static String upLoadPath = "C:\\Mokua\\JCR\\docs";

    static String indexUpLoadPath = "";

    static String unknownFileIcon = null;

    public static final String CONTEXT_TOKEN = ",";


    static public String getFileIcon(boolean small, String extension) {
        return getFileIcon(small, extension, false);
    }


    //TODO
    public static String getFileIcon(boolean small, String filename, boolean isReadOnly) {
        String extension = FilenameUtils.getExtension(filename);
        if (!StringUtil.isDefined(extension)) {
            extension = filename;
        }
        if (extension == null) {
            extension = "";
        }
        String fileIcon = getFileIconFromExtension(extension.toLowerCase(Locale.getDefault()));
        if (fileIcon == null) {
            fileIcon = unknownFileIcon;
        } else {
            if (isReadOnly) {
                fileIcon = fileIcon.substring(0, fileIcon.lastIndexOf(".gif")) + "Lock.gif";
            }
        }
        if (small && fileIcon != null) {
            String newFileIcon = fileIcon.substring(0, fileIcon.lastIndexOf(".gif")) + "Small.gif";
            fileIcon = newFileIcon;
        }

        return fileIcon;
    }

    //TODO put in settings
    private static String getFileIconFromExtension(String extension) {
        Map<String, String> exts = new LinkedHashMap<>();
        exts.put("doc", "word.gif");
        exts.put("rtf", "word.gif");
        exts.put("dot", "wordModel.gif");
        exts.put("xls", "excel.gif");
        exts.put("xlt", "excel.gif");
        exts.put("mdb", "access.gif");
        exts.put("ppt", "powerpoint.gif");
        exts.put("pps", "powerpoint.gif");
        exts.put("pot", "powerpoint.gif");
        exts.put("ppa", "powerpoint.gif");

        exts.put("docx", "word2007.gif");
        exts.put("dotx", "word2007.gif");
        exts.put("docm", "word2007.gif");
        exts.put("dotm", "word2007.gif");

        exts.put("xlsx", "excel2007.gif");
        exts.put("xla", "excel2007.gif");
        exts.put("xltx", "excel2007.gif");
        exts.put("xlsm", "excel2007.gif");
        exts.put("xltm", "excel2007.gif");
        exts.put("xlam", "excel2007.gif");
        exts.put("xlsb", "excel2007.gif");

        exts.put("pptx", "powerPoint2007.gif");
        exts.put("potx", "powerPoint2007.gif");
        exts.put("ppsx", "powerPoint2007.gif");
        exts.put("ppam", "powerPoint2007.gif");
        exts.put("pptm", "powerPoint2007.gif");
        exts.put("potm", "powerPoint2007.gif");
        exts.put("ppsm", "powerPoint2007.gif");

        exts.put("pubx", "publisher2007.gif");

        exts.put("3d ", "3d.gif");
        exts.put("3D ", "3d.gif");

        exts.put("avi", "video.gif");
        exts.put("mp3", "sound.gif");
        exts.put("wav", "sound.gif");
        exts.put("cda", "sound.gif");
        exts.put("ogg", "sound.gif");

        exts.put("mpg", "video.gif");
        exts.put("asf", "video.gif");
        exts.put("asx", "video.gif");
        exts.put("mov", "video.gif");
        exts.put("flv", "video.gif");
        exts.put("mp4", "video.gif");

        exts.put("gif", "image.gif");
        exts.put("jpg", "image.gif");
        exts.put("jpeg", "image.gif");
        exts.put("bmp", "image.gif");
        exts.put("tif", "image.gif");
        exts.put("tiff", "image.gif");
        exts.put("png", "image.gif");

        exts.put("swf", "flash.gif");
        exts.put("fla", "flash.gif");
        exts.put("exe", "exe.gif");

        exts.put("htm", "html.gif");
        exts.put("html", "html.gif");

        exts.put("pdf", "pdf.gif");
        exts.put("txt", "texte.gif");

        exts.put("unknown", "unknown.gif");

        exts.put("zip", "zip.gif");
        exts.put("rar", "zip.gif");
        exts.put("jar", "zip.gif");

        exts.put("odt", "oo_text.gif");
        exts.put("odp", "oo_presentation.gif");
        exts.put("ods", "oo_spreadsheet.gif");
        exts.put("odb", "oo_base.gif");
        exts.put("odg", "oo_draw.gif");

        exts.put("csv", "doc_excel_csv.gif");
        exts.put("psd", "doc_photoshop.gif");
        exts.put("pdd", "doc_photoshop.gif");

        exts.put("eps", "doc_illustrator.gif");
        exts.put("ai", "doc_illustrator.gif");

        exts.put("cpp", "page_white_cplusplus.gif");
        exts.put("cs", "page_white_csharp.gif");
        exts.put("php", "page_white_php.gif");
        exts.put("rb", "page_white_ruby.gif");
        exts.put("vb", "page_white_visualstudio.gif");
        exts.put("vbe", "page_white_visualstudio.gif");
        exts.put("vbs", "page_white_visualstudio.gif");

        exts.put("java", "page_white_cup.gif");
        exts.put("jsp", "page_white_cup.gif");
        exts.put("js", "js.gif");

        exts.put("iso", "page_white_dvd.gif");
        exts.put("eml", "envelope.png");
        exts.put("msg", "envelope.png");

        return exts.get(extension);

    }

    public static String getFileExtension(String fileName) {
        return FilenameUtils.getExtension(fileName);
    }


    /**
     * Get the file size with the suitable unit
     *
     * @param lSize
     * @return String
     */
    static public String formatFileSize(long lSize) {
        String Mo = "MiB";
        String Ko = "KiB";
        String o = "Bytes";

        float size = new Long(lSize).floatValue();

        if (size < 1024) {
            return Float.toString(size).concat(" ").concat(o);
        } else if (size < 1024 * 1024) {
            return Integer.toString(Math.round(size / 1024)).concat(" ").concat(Ko);
        } else {
            DecimalFormat format = new DecimalFormat();
            format.setMaximumFractionDigits(2);
            return format.format(size / (1024 * 1024)).concat(" ").concat(Mo);
        }
    }


}