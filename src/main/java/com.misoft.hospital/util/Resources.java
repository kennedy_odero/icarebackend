
package com.misoft.hospital.util;

import org.hibernate.Session;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.logging.Logger;

/**
 * This class uses CDI to alias Java EE resources, such as the persistence context, to CDI beans
 * <p/>
 * <p>
 * Example injection on a managed bean field:
 * </p>
 * <p/>
 * <pre>
 * &#064;Inject
 * private EntityManager em;
 * </pre>
 */
public class Resources {

    @SuppressWarnings("unused")
    @PersistenceContext
    private EntityManager em;

    @Produces
    public EntityManager getEntityManager() {
        System.out.println("***** GETTING EM *********");
        final Session session = em.unwrap(Session.class);
        org.hibernate.Filter filter = session.enableFilter("filterByDeleted");
        System.out.println(" Filter enabled " + filter.getName());
        return em;
    }


    @Produces
    @RequestScoped
    public Session createHibernateSession() {
        return (Session) em.getDelegate();
    }

    @Produces
    public Logger produceLog(InjectionPoint injectionPoint) {
        return Logger.getLogger(injectionPoint.getMember().getDeclaringClass().getName());
    }

}
