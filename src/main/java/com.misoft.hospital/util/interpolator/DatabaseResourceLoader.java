package com.misoft.hospital.util.interpolator;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Logger;

public class DatabaseResourceLoader extends ResourceLoader {
    private static final Logger log = Logger.getLogger(ResourceLoader.class.getName());


    // Redefine how we load a resource bundle
    @Override
    public ResourceBundle loadBundle(final String bundleName) {
        log.info("loading  resource bundle : " + bundleName + " from the database");

        final ResourceBundle resourceBundle = new ResourceBundle() {

            public Enumeration<String> getKeys() {
                java.util.Locale locale = Locale.instance();
                EntityManager entityManager = ProgrammaticBeanLookup.lookup(EntityManager.class);
                List resources = entityManager.createNamedQuery("keys")
                        .setParameter("bundleName", bundleName)
                        .setParameter("language", locale.getLanguage())
                        .getResultList();
                log.info(" total keys " + resources.size());
                return Collections.enumeration(resources);
            }

            protected Object handleGetObject(String key) {
                java.util.Locale locale = Locale.instance();
                EntityManager entityManager = ProgrammaticBeanLookup.lookup(EntityManager.class);
                try {
                    return entityManager.createNamedQuery("value")
                            .setParameter("bundleName", bundleName)
                            .setParameter("language", locale.getLanguage())
                            .setParameter("key", key)
                            .getSingleResult();
                } catch (NoResultException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

        log.info("finished loading  resource bundle : " + bundleName + " from the database");
        log.info(" KEYS ");
        Enumeration<String> en = resourceBundle.getKeys();
        while (en.hasMoreElements()) {
            log.info(" KEY " + en.nextElement());
        }
        return resourceBundle;
    }

    public static ResourceLoader instance() {
        return new DatabaseResourceLoader();
    }

    private static class LocalisedMessage {
        private final String message;

        private final java.util.Locale locale;

        LocalisedMessage(String message, java.util.Locale locale) {
            this.message = message;
            this.locale = locale;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            LocalisedMessage that = (LocalisedMessage) o;

            if (locale != null ? !locale.equals(that.locale) : that.locale != null) {
                return false;
            }
            if (message != null ? !message.equals(that.message) : that.message != null) {
                return false;
            }

            return true;
        }

        @Override
        public int hashCode() {
            int result = message != null ? message.hashCode() : 0;
            result = 31 * result + (locale != null ? locale.hashCode() : 0);
            return result;
        }
    }
}