package com.misoft.hospital.util.interpolator;

/**
 * Manager component for the current locale. This base
 * implementation simply returns the server default
 * locale.
 *
 * @author Gavin King
 */

public class Locale {

    // @Unwrap
    public java.util.Locale getLocale() {
        return java.util.Locale.getDefault();
    }

    public static java.util.Locale instance() {
        return java.util.Locale.getDefault(); //for unit tests
    }
}