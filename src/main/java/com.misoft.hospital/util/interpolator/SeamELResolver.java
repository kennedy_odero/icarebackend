package com.misoft.hospital.util.interpolator;


import javax.el.ELContext;
import javax.el.ELResolver;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.ResourceBundle;


/**
 * Resolves Seam components and namespaces. Also
 * allows the use of #{dataModel.size}, #{dataModel.empty},
 * #{collection.size}, #{map.size}, #{map.values}, #{map.keySet},
 * and #{map.entrySet}. Also allows #{sessionContext['name']}.
 *
 * @author Gavin King
 */
public class SeamELResolver extends ELResolver {

    @Override
    public Class getCommonPropertyType(ELContext context, Object base) {
        return null;
    }

    @Override
    public Iterator getFeatureDescriptors(ELContext context, Object base) {
        return null;
    }

    @Override
    public Class getType(ELContext context, Object base, Object property) {
        return null;
    }

    @Override
    public boolean isReadOnly(ELContext context, Object base, Object property) {
        return base != null
                && ((base instanceof Collection) || (base instanceof Map));
    }

    @Override
    public void setValue(ELContext context, Object base, Object property, Object value) {
    }


    @Override
    public Object getValue(ELContext context, Object base, Object property) {
        System.out.println("******************************");

        System.out.println(" GET VALUE context " + context + "  base " + base + "  property " + property);
        if (base == null) {
            return resolveBase(context, property);

        } else {
            return null;
        }
    }


    private Object resolveBase(ELContext context, Object property) {


        String key = (String) property;
        context.setPropertyResolved(true);
        ResourceBundle res = DatabaseResourceLoader.instance().loadBundle(key);
        if (res != null) {
            System.out.println(" the db resource bundle is not null  ");
            return res;
        }
        return res;
        /*System.out.println(" the db resource bundle IS null  ");
        return ResourceLoader.instance().loadBundle(key);*/

    }


}
