package com.misoft.hospital.util.interpolator;

import com.misoft.hospital.model.i18n.ResourceBundle;
import com.misoft.hospital.util.Strings;

import java.io.InputStream;
import java.util.logging.Logger;

/**
 * Access to application resources and resource bundles.
 *
 * @author Gavin King
 */

public abstract class ResourceLoader {
    private static final Logger log = Logger.getLogger(ResourceLoader.class.getName());

    private String[] bundleNames = {ResourceBundle.DEFAULT_BUNDLE_NAME};

    /**
     * The configurable list of delegate resource bundle names
     *
     * @return an array of resource bundle names
     */
    public String[] getBundleNames() {
        return bundleNames;
    }

    public void setBundleNames(String[] bundleNames) {
        this.bundleNames = bundleNames;
    }

    public InputStream getResourceAsStream(String resource) {
        return Resources.getResourceAsStreamWithCtxt(resource, null);
    }


    /**
     * Load a resource bundle by name (may be overridden by subclasses
     * who want to use non-standard resource bundle types).
     *
     * @param bundleName the name of the resource bundle
     * @return an instance of java.util.ResourceBundle
     */
    public abstract java.util.ResourceBundle loadBundle(String bundleName);/*
   {
      try
      {
         java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle( 
               bundleName,
                 com.lemr.util.temp.Locale.instance(),
               Thread.currentThread().getContextClassLoader() 
         );
         
         // for getting bundle from page level message properties
       *//*  if (bundle == null){
            bundle = java.util.ResourceBundle.getBundle( 
                  bundleName,
                  com.lemr.util.temp.Locale.instance(),
                  ServletLifecycle.getCurrentServletContext().getClass().getClassLoader() 
            );
         }*//*
         log.info("loaded resource bundle: " + bundleName);
         return bundle;
      }
      catch (MissingResourceException mre)
      {
         log.info("resource bundle missing: " + bundleName);
         return null;
      }
   }*/

    @Override
    public String toString() {
        String concat = bundleNames == null ? "" : Strings.toString(", ", (Object[]) bundleNames);
        return "ResourceBundle(" + concat + ")";
    }
   
   /*public static ResourceLoader instance()
   {
       return new ResourceLoader();
   }*/

}
