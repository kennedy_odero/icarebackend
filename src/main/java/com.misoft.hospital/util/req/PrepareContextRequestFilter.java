package com.misoft.hospital.util.req;

import javax.annotation.Priority;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.ext.Provider;
import java.io.IOException;

@Provider
@PreMatching
@RequestScoped
@Priority(10) // Run before other filters
public class PrepareContextRequestFilter implements ContainerRequestFilter {

    @Context
    protected UriInfo uriInfo;

    @Context
    protected HttpServletRequest request;

    @Produces
    @com.misoft.hospital.util.req.UriInfo
    public UriInfo produceUriInfo() {
        return uriInfo;
    }

    @Produces
    @RequestHost
    public String produceRequestHost() {
        return uriInfo.getBaseUri().getHost();
    }

    @Produces
    @RequestPort
    public Integer produceRequestPort() {
        return uriInfo.getBaseUri().getPort() != -1
                ? uriInfo.getBaseUri().getPort()
                : request.getServerPort();
    }

    @Produces
    @RequestPath
    public String produceRequestPath() {
        return uriInfo.getPath(false);
    }

    @Produces
    @RequestQuery
    public String produceRequestQuery() {
        return request.getQueryString();
    }

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        // Nothing to do here, this filter only produces (named) context variables
    }
}