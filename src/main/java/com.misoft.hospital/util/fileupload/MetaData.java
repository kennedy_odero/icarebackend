package com.misoft.hospital.util.fileupload;

import com.misoft.hospital.util.StringUtil;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.metadata.Property;
import org.apache.tika.metadata.TikaCoreProperties;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * @author mokua
 */
public class MetaData {

    private final Metadata metadata;

    MetaData(Metadata metadata) {
        this.metadata = metadata;
    }

    public String getValue(String name) {
        return metadata.get(name);
    }

    public List<String> getAvailablePropertyNames() {
        return Arrays.asList(metadata.names());
    }

    /**
     * Return Title of an Office document
     *
     * @return String
     */
    public String getTitle() {
        return cleanString(metadata.get(TikaCoreProperties.TITLE));
    }

    /**
     * Return Subject of an Office document
     *
     * @return String
     */
    public String getSubject() {
        return cleanString(metadata.get(Metadata.SUBJECT));
    }

    /**
     * Return Author of an Office document
     *
     * @return String
     */
    public String getAuthor() {
        String author = metadata.get(Metadata.AUTHOR);
        if (author == null) {
            author = metadata.get(Metadata.CREATOR);
        }
        return cleanString(author);
    }

    //Content-Type

    public String getContentType() {
        String contentType = metadata.get(Metadata.CONTENT_TYPE);

        return cleanString(contentType);
    }

    /**
     * Return Comments of an Office document
     *
     * @return String
     */
    public String getComments() {
        String comments = metadata.get(Metadata.COMMENTS);
        if (!StringUtil.isDefined(comments)) {
            comments = metadata.get(Metadata.DESCRIPTION);
        }
        return cleanString(comments);
    }

    /**
     * Return Security of an Office document
     *
     * @return String
     */
    public int getSecurity() {
        return metadata.getInt(Property.internalInteger(Metadata.SECURITY));
    }

    /**
     * Return Keywords of an Office document
     *
     * @return String
     */
    public String[] getKeywords() {
        return cleanString(metadata.getValues(Metadata.KEYWORDS));
    }


    public Date getLastSaveDateTime() {
        Date date = parseDate(Metadata.LAST_SAVED);
        if (date == null) {
            return parseDate(Metadata.LAST_MODIFIED);
        }
        return date;
    }

    /**
     * Return CreateDateTime of an Office document
     */
    public Date getCreationDate() {
        Date result = getDate(TikaCoreProperties.CREATED);
        if (result == null) {
            result = metadata.getDate(TikaCoreProperties.CREATED);
        }
        if (result == null) {
            result = metadata.getDate(Metadata.DATE);
        }
        return result;
    }

    protected Date getDate(Property property) {
        Date result = metadata.getDate(property);
        if (result == null) {
            return parseDate(property);
        }
        return result;
    }

    protected Date parseDate(Property property) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        String date = metadata.get(property);
        if (date != null) {
            try {

                return sdf.parse(date);
            } catch (ParseException ex) {
                return null;
            }
        }
        return null;
    }

    private String[] cleanString(String[] values) {
        String[] result = new String[values.length];
        for (int i = 0; i < values.length; i++) {
            result[i] = cleanString(values[i]);
        }
        return result;
    }

    private String cleanString(String value) {
        if (StringUtil.isDefined(value)) {
            return value.replace("\u0000", "").replace("ï¿½ï¿½", "").trim();
        }
        return value;
    }

    @Override
    public String toString() {
        return "MetaData{" +
                "metadata=" + metadata +
                '}';
    }
}