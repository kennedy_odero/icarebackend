package com.misoft.hospital.util.fileupload;

import org.apache.commons.io.IOUtils;
import org.apache.tika.Tika;
import org.apache.tika.metadata.Metadata;

import java.io.*;

/**
 * @author mokua
 */

public class MetadataExtractor {

    public MetadataExtractor() {
    }

    /**
     * Return Metadata of a document.
     *
     * @param fileName
     * @return Metadata
     */
    public MetaData extractMetadata(String fileName) {
        InputStream inputStream = null;
        try {
            inputStream = new FileInputStream(fileName);
            return getMetadata(inputStream);
        } catch (IOException ex) {
            ex.printStackTrace();
            return new MetaData(new Metadata());
        } finally {
            IOUtils.closeQuietly(inputStream);
        }
    }

    private MetaData getMetadata(InputStream inputStream) throws IOException {
        Metadata metadata = new Metadata();
        Tika tika = new Tika();
        Reader reader = tika.parse(inputStream, metadata);
        reader.close();
        return new MetaData(metadata);
    }

    public MetaData extractMetadata(File file) {
        InputStream inputStream = null;
        try {
            inputStream = new FileInputStream(file);
            return getMetadata(inputStream);
        } catch (IOException ex) {
            ex.printStackTrace();
            return new MetaData(new Metadata());
        } finally {
            IOUtils.closeQuietly(inputStream);
        }
    }
}