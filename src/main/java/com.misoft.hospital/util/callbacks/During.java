package com.misoft.hospital.util.callbacks;

/**
 * Created by kodero on 5/29/16.
 */
public enum During {
    CREATE,
    UPDATE,
    DELETE,
    ALWAYS
}
