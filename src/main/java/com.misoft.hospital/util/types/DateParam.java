package com.misoft.hospital.util.types;

import com.misoft.hospital.util.Strings;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateParam {
  private final Date date;

  public DateParam(String dateStr) throws WebApplicationException {
    if (Strings.isEmpty(dateStr)) {
      this.date = null;
      return;
    }
    final DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    if(dateStr.equalsIgnoreCase("today")) dateStr = dateFormat.format(new Date());
      try {
      this.date = dateFormat.parse(dateStr);
    } catch (ParseException e) {
      throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
        .entity("Couldn't parse date string: " + e.getMessage())
        .build());
    }
  }

  public Date getDate() {
    return date;
  }

  public String toString(){
    if(date != null) return new SimpleDateFormat("dd-MM-yyyy").format(date);
    return null;
  }
}