package com.misoft.hospital.util;

import java.io.*;

public class ICD10Loader {
    public static void main(String[] args){
        BufferedReader br = null;
        FileReader fr = null;

        try {

            //br = new BufferedReader(new FileReader(FILENAME));
            fr = new FileReader("/Users/kodero/git/icd10/icd10.csv");
            br = new BufferedReader(fr);

            BufferedWriter writer = new BufferedWriter(new FileWriter("/Users/kodero/git/icd10/icd10.sql"));

            String sCurrentLine;
            while ((sCurrentLine = br.readLine()) != null) {
                String[] parts = sCurrentLine.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
                StringBuffer sb = new StringBuffer("insert into diseases(id, created_at, updated_at, version, code, name, info, icd_version) ");
                String code = parts[0].replaceAll("...(?!$)", "$0.");
                String name = parts[1].replaceAll("\"", "");
                name = name.replaceAll("'", "");
                String info = parts[2].replaceAll("\"", "");
                info = info.replaceAll("'", "");
                sb.append("values(uuid(), now(), now(), 0, '" + code + "', '" + name + "', '" + info + "', '10');\n");
                System.out.println(sb.toString());
                //writer
                writer.write(sb.toString());
            }
            writer.close();

        } catch (IOException e) {
            e.printStackTrace();
        } finally {

            try {
                if (br != null)
                    br.close();
                if (fr != null)
                    fr.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }

        }
    }
}
