package com.misoft.hospital.util.mirror;

public interface TypeExtractor {

    Class<?>[] extract(Mirror<?> mirror);

}