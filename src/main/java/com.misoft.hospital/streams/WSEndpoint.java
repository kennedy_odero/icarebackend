package com.misoft.hospital.streams;

import com.misoft.hospital.model.billing.PatientBill;
import com.misoft.hospital.rest.BaseEntityService;
import org.jboss.logging.Logger;

import javax.annotation.Resource;
import javax.enterprise.concurrent.ManagedExecutorService;
import javax.enterprise.event.Observes;
import javax.enterprise.event.TransactionPhase;
import javax.inject.Named;
import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Named
@ServerEndpoint(value = "/streams")
public class WSEndpoint {
	Logger log = Logger.getLogger(this.getClass());

	@Resource
	ManagedExecutorService mes;

	private static final ConcurrentHashMap<String, Session> activeSessions = new ConcurrentHashMap<>();

	@OnMessage
	public String receiveMessage(String message, Session session) {
		log.info("Received : "+ message + ", session:" + session.getId());
		return "Response from the server";
	}

	@OnOpen
	public void open(Session session) {
		log.info("Open session:" + session.getId());
		activeSessions.put(session.getId(), session);
		log.info("Active sessions" + activeSessions);
	}

	public void onPatientBillCreated(@Observes(during = TransactionPhase.AFTER_SUCCESS) BaseEntityService.EntityCreatedEvent<PatientBill> event){
		log.info("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%Patient bill created!" + event.getEntity().getPatient().getPatientNo() + ", event being sent to websocket %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
		log.info("Active sessions" + activeSessions);
		notifyClients(event.getEntity());
	}

	public void onPatientBillUpdated(@Observes(during = TransactionPhase.AFTER_SUCCESS) BaseEntityService.EntityUpdatedEvent<PatientBill> event){
		//send the completion message to all ws clients
		log.info("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%Patient bill created!" + event.getEntity().getPatient().getPatientNo() + ", event being sent to websocket %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
		log.info("Active sessions" + activeSessions);
		notifyClients(event.getEntity());
	}

	private void notifyClients(final PatientBill patientBill) {
		mes.execute(new Runnable() {
			@Override
			public void run() {
				try {
					for (Map.Entry<String, Session> entry : activeSessions.entrySet()) {
						log.info("Sending completion message to : " + entry.getKey());
						entry.getValue().getBasicRemote().sendText("{\"event\": \"patientBillCreated\", \"msg\": \"A bill for patient " +
								patientBill.getPatient().getPatientNo() + " - " + patientBill.getPatient().getPatientName() +  " has been created!\", billId : \"" + patientBill.getId() + "\"}");
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});
	}

	@OnClose
	public void close(Session session, CloseReason c) {
		log.info("Closing:" + session.getId() + ", reason : " + c.getReasonPhrase());
		activeSessions.remove(session.getId());
	}
}