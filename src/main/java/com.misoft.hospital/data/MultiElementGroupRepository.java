
package com.misoft.hospital.data;

import com.misoft.hospital.model.util.MultiElementGroup;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;
import java.util.logging.Logger;

@ApplicationScoped
public class MultiElementGroupRepository extends AbstractFacade<MultiElementGroup> {

    @Inject
    private EntityManager em;

    @Inject
    private Logger log;

    public MultiElementGroupRepository() {

    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public MultiElementGroup findByTargetClassAndName(final String targetClass, final String groupName) {
        final String sql = "select DISTINCT g from MultiElementGroup g join fetch g.multiElementValues " +
                " where g.name =:groupName and g.targetClassName =:targetClassName";
        List<MultiElementGroup> res = em.createQuery(sql, MultiElementGroup.class)
                .setParameter("groupName", groupName)
                .setParameter("targetClassName", targetClass)
                .getResultList();


        if (res.size() == 0) throw new RuntimeException("No group element for  group " + groupName
                + " target class " + targetClass);

        if (res.size() > 1) throw new RuntimeException("More than one group element for  group " + groupName
                + " target class " + targetClass);

        return res.get(0);


    }


}
