
package com.misoft.hospital.data.admin;

import com.misoft.hospital.model.admin.User;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

@Stateless
public class UserRepository implements Serializable {

    @Inject
    private EntityManager em;

    @Inject
    protected Logger log;

    public UserRepository() {

    }


    public User findUser(String userId) {
        return em.find(User.class, userId);
    }

    public User updateAfterSuccessfulLogin(User user) {
        Query q = em.createQuery("UPDATE User u SET u.lastLoggedInAt = :lastLoggedInAt," +
                "u.lastLoginAt =:lastLoginAt," +
                "u.currentLoginAt =:currentLoginAt," +
                "u.lastLoginIp =:lastLoginIp," +
                "u.currentLoginIp =:currentLoginIp," +
                "u.loginCount =:loginCount, " +
                "u.persistenceToken =:persistenceToken " +
                "WHERE u.id =:id");
        q.setParameter("lastLoggedInAt", user.getLastLoggedInAt());
        q.setParameter("lastLoginAt", user.getLastLoginAt());
        q.setParameter("currentLoginAt", user.getCurrentLoginAt());
        q.setParameter("lastLoginIp", user.getLastLoginIp());
        q.setParameter("currentLoginIp", user.getCurrentLoginIp());
        q.setParameter("loginCount", user.getLoginCount());
        q.setParameter("persistenceToken", user.getPersistenceToken());
        q.setParameter("id", user.getId());
        int updated = q.executeUpdate();
        return findUser(user.getId());
    }

    public User updateUser(User user) {
        //em.getEntityGraph()
        User res = em.merge(user);
        return res;
    }


    public List<User> usersFull() {
        String query = "select u from User u \n" +
                " left join fetch u.userProfile profile \n";
        return em.createQuery(query.toString(), User.class).getResultList();


    }

    /**
     * @param username      the fellows email
     * @param onlyActivated
     * @param caseSensitive
     * @return
     */
    public User findUser(String username, boolean onlyActivated, boolean caseSensitive) {
        StringBuilder query = new StringBuilder("select u from User u where");
        if (caseSensitive)
            query.append(" u.email = :username");
        else
            query.append(" lower(u.email) = :username");
        if (onlyActivated) query.append(" and u.approved = true");

        try {
            return (User) em
                    .createQuery(query.toString())
                    .setParameter("username", caseSensitive ? username : username.toLowerCase())
                    .getSingleResult();
        } catch (EntityNotFoundException | NoResultException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public User findUserByEmail(String email) {
        StringBuilder query = new StringBuilder("select u from User u where u.email = :email");

        try {
            return (User) em
                    .createQuery(query.toString())
                    .setParameter("email", email)
                    .getSingleResult();
        } catch (EntityNotFoundException | NoResultException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public User findUserByToken(String token) {
        String query = "select u from User u LEFT JOIN FETCH u.userProfile p where u.persistenceToken = :token";
        log.info("Said token : " + token);
        return (User) em
                .createQuery(query.toString())
                .setParameter("token", token)
                .getSingleResult();
    }

    public User findFullUserByToken(String token) {
        String query = "select u from User u where u.persistenceToken = :token";
        log.info("Said token : " + token);
        return (User) em
                .createQuery(query.toString())
                .setParameter("token", token)
                .getSingleResult();
    }

    /**
     * also checks if the activation code has expired
     *
     * @param activationCode
     * @return
     */
    public User findUserWithActivationCode(String activationCode) {
        StringBuilder query = new StringBuilder("select u from User u where u.activationCode = :activationCode " +
                "and u.emailVerificationExpiryAt >= :currentTimestamp ");
        try {
            return (User) em
                    .createQuery(query.toString())
                    .setParameter("activationCode", activationCode)
                    .setParameter("currentTimestamp", new Date())
                    .getSingleResult();
        } catch (EntityNotFoundException | NoResultException ex) {
            ex.printStackTrace();
        }
        return null;
    }


}
