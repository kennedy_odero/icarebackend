
package com.misoft.hospital.data.admin;

import com.misoft.hospital.model.admin.Permission;
import com.misoft.hospital.model.admin.User;
import com.misoft.hospital.model.admin.UserPermission;
import com.misoft.hospital.rest.admin.UserPermissionRequest;
import com.misoft.hospital.service.grant.GService;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.AliasToBeanResultTransformer;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.validation.ValidationException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

@ApplicationScoped
public class PermissionRepository {


    @Inject
    private java.util.logging.Logger log;

    @Inject
    private EntityManager em;

    @Inject
    GService gService;

    public PermissionRepository() {
    }


    public List<UserPermissionRequest> findAll() {
        //String query = "select b from UserPermission b  ";
        Session session = (Session) em.getDelegate();
        SQLQuery q = session.createSQLQuery("select\n" +
                "     b.status as status,\n" +
                "     u.id as userId,\n " +
                "     u.login as userName,\n" +
                "     p.id as permissionId,\n" +
                "     p.permission_name as permissionName, \n" +
                "     p.description as permissionDescription,\n " +
                "     p.model_type as modelType,\n " +
                "     p.namespace as namespace " +
                "\n" +
                " from\n" +
                "     user_permissions b\n" +
                " inner join users u on b.user_id = u.id\n" +
                " inner join permissions p on b.permission_id = p.id");

        q.setResultTransformer(new AliasToBeanResultTransformer(UserPermissionRequest.class));
        List<UserPermissionRequest> res = q.list();
        return res;
    }

    public List<UserPermissionRequest> register(Collection<UserPermissionRequest> requests) throws Exception {
        List<UserPermissionRequest> createdPermissions = new LinkedList<>();
        for (UserPermissionRequest request : requests) {
            createdPermissions.add(register(request));
        }
        return createdPermissions;
    }

    public UserPermissionRequest register(UserPermissionRequest request) {
        User existingUser = em.find(User.class, request.getUserId().longValue());
        Permission existingPermission = em.find(Permission.class, request.getPermissionId().longValue());
        if (existingUser == null) {
            throw new ValidationException(" Use id " + request.getUserId() + " does not exist");
        }
        if (existingPermission == null) {
            throw new ValidationException(" Permission id " + request.getPermissionId() + " does not exist");
        }

        //UserPermission newPermission = new UserPermission(request.getStatus(),existingUser,existingPermission);
        //TODO hack
        final String sql = "insert into user_permissions(permission_id,user_id,status) values(?,?,?)";
        gService.executeQuery(request, sql);
        //gService.makePersistent(newPermission)

        return request;

    }

    public List<UserPermissionRequest> findAllForUser(String userId) {
        Session session = (Session) em.getDelegate();
        SQLQuery q = session.createSQLQuery(" select\n" +
                "     b.status as status,\n" +
                "     u.id as userId,\n" +
                "     u.login as userName,\n" +
                "     p.id as permissionId, \n" +
                "     p.permission_name as permissionName, \n" +
                "     p.description as permissionDescription, " +
                "     p.model_type as modelType,\n " +
                "     p.namespace as namespace " +
                "\n" +
                " from\n" +
                "     user_permissions b\n" +
                "\n" +
                " inner join users u on b.user_id = u.id\n" +
                " inner join permissions p on b.permission_id = p.id\n" +
                "      where u.login =:userId");
        q.setString("userId", userId);
        q.setResultTransformer(new AliasToBeanResultTransformer(UserPermissionRequest.class));
        List<UserPermissionRequest> res = q.list();
        return res;

    }

    public List<UserPermissionRequest> findAllForUserAndNamespace(String userId, String namespace) {
        Session session = (Session) em.getDelegate();
        SQLQuery q = session.createSQLQuery(" select\n" +
                "     b.status as status,\n" +
                "     u.id as userId,\n" +
                "     u.login as userName,\n" +
                "     p.id as permissionId ,\n" +
                "     p.permission_name as permissionName, \n" +
                "     p.description as permissionDescription, " +
                "     p.model_type as modelType,\n " +
                "     p.namespace as namespace " +
                "\n" +
                " from\n" +
                "     user_permissions b\n" +
                "\n" +
                " inner join users u on b.user_id = u.id\n" +
                " inner join permissions p on b.permission_id = p.id\n" +
                "      where u.login =:userId and p.namespace =:namespace");
        q.setString("userId", userId);
        q.setString("namespace", namespace);
        q.setResultTransformer(new AliasToBeanResultTransformer(UserPermissionRequest.class));
        List<UserPermissionRequest> res = q.list();
        return res;
    }

    public List<UserPermissionRequest> findAllForUserAndNamespaceAndModelType(String userId,
                                                                              String namespace,
                                                                              String modelType) {

        Session session = (Session) em.getDelegate();
        SQLQuery q = session.createSQLQuery(" select\n" +
                "     b.status as status,\n" +
                "     u.id as userId,\n" +
                "     u.login as userName,\n" +
                "     p.id as permissionId, \n" +
                "     p.permission_name as permissionName, \n" +
                "     p.description as permissionDescription, " +
                "     p.model_type as modelType,\n " +
                "     p.namespace as namespace " +
                "\n" +
                " from\n" +
                "     user_permissions b\n" +
                "\n" +
                " inner join users u on b.user_id = u.id\n" +
                " inner join permissions p on b.permission_id = p.id\n" +
                "      where u.login =:userId and p.namespace =:namespace and p.model_type =:modelType");
        q.setString("userId", userId);
        q.setString("namespace", namespace);
        q.setString("modelType", modelType);
        q.setResultTransformer(new AliasToBeanResultTransformer(UserPermissionRequest.class));
        List<UserPermissionRequest> res = q.list();
        return res;


    }


    public int updateUserPermissionStatus(UserPermission userPermission, Boolean status) {
        return gService.updateUserPermissionStatus(userPermission, status);

    }

    public UserPermission findUserPermissionById(long userPermissionId) {
        return em.find(UserPermission.class, userPermissionId);
    }
}
