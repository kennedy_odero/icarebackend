
package com.misoft.hospital.data.admin;

import com.misoft.hospital.model.admin.Role;
import com.misoft.hospital.model.admin.User;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.io.Serializable;
import java.util.List;

@Stateless
public class RoleRepository implements Serializable {

    @Inject
    private EntityManager em;

    public RoleRepository() {

    }


    /**
     * TODO krap code
     *
     * @param role
     * @return
     */
    public List<User> findUser(Role role) {
        Query q = em.createQuery("select u from User u WHERE u.role.name =:role", User.class);
        try {
            List<User> res = q.setParameter("role", role.getName())
                    .getResultList();
            return res;
        } catch (EntityNotFoundException | NoResultException ex) {
            ex.printStackTrace();
        }
        return null;
    }


}
