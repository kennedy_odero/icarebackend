
package com.misoft.hospital.data.admin;

import com.misoft.hospital.data.AbstractFacade;
import com.misoft.hospital.model.admin.UserProfile;
import com.misoft.hospital.model.admin.UserProfile_;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.io.Serializable;

@Stateless
public class UserProfileRepository extends AbstractFacade<UserProfile> implements Serializable {

    @Inject
    private EntityManager em;

    public UserProfileRepository() {

    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }


    /**
     * can throw EntityNotFoundException &   NoResultException
     *
     * @param profileName
     * @return
     */
    public UserProfile findUserProfile(String profileName) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<UserProfile> criteria = cb.createQuery(UserProfile.class);
        Root<UserProfile> organization = criteria.from(UserProfile.class);
        criteria.select(organization).where(cb.equal(organization.get(UserProfile_.name), profileName));
        return em.createQuery(criteria).getSingleResult();
    }


}
