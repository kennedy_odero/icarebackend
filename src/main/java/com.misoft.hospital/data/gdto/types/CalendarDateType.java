package com.misoft.hospital.data.gdto.types;


import com.misoft.hospital.model.shared.time.CalendarDate;
import com.misoft.hospital.util.dates.DateConverter;

import java.text.ParseException;
import java.util.Date;
import java.util.logging.Logger;

public class CalendarDateType extends AbstractType<CalendarDate> {
    private static Logger log = Logger.getLogger(CalendarDateType.class.getName());

    public CalendarDateType(String regExp) {
        super(regExp);
    }

    public CalendarDateType(String regExp, String contentAsString) {
        super(regExp);
        this.instantiateFromString(contentAsString);
    }

    public CalendarDateType() {
        this(null);
    }

    protected CalendarDate construct(String content) {
        log.info(" construct calendar date instance from string ' " + content + " '");
        if (content == null) return null;
        DateConverter dc = new DateConverter(true);
        try {
            Date date = dc.parse(content);
            return CalendarDate.fromDate(date);
        } catch (ParseException e) {
            e.printStackTrace();
            throw new IllegalStateException(e);
        }
    }

    @Override
    public String toString() {
        return "{" +
                "MoneyType ='" + getValue() + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CalendarDateType)) return false;
        CalendarDateType that = (CalendarDateType) o;
        if (this.t.compareTo(that.t) != 0) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return t.hashCode();
    }
}