package com.misoft.hospital.data.gdto.types;


import com.misoft.hospital.model.shared.Money;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.logging.Logger;

public class MoneyType extends AbstractType<Money> {
    private static Logger log = Logger.getLogger(MoneyType.class.getName());

    public MoneyType(String regExp) {
        super(regExp);
    }

    public MoneyType(String regExp, String contentAsString) {
        super(regExp);
        this.instantiateFromString(contentAsString);
    }

    public MoneyType() {
        this(null);
    }

    protected Money construct(String content) {
        log.info(" construct money instance from string ' " + content + " '");
        if (content == null) return null;
        String[] contentArray = content.split(" ");
        assert contentArray.length == 2;
        final String code = contentArray[0];
        final String qty = contentArray[1];
        final BigDecimal value = new BigDecimal(qty);
        return Money.valueOf(value, Currency.getInstance(code));
    }

    @Override
    public String toString() {
        return "{" +
                "MoneyType ='" + getValue() + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MoneyType)) return false;
        MoneyType that = (MoneyType) o;
        if (this.t.compareTo(that.t) != 0) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return t.hashCode();
    }
}