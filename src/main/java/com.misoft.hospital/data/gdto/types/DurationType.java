package com.misoft.hospital.data.gdto.types;


import com.misoft.hospital.model.shared.time.Duration;

import java.util.logging.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: mokua
 * Date: 4/17/14
 * Time: 9:15 AM
 */
public class DurationType extends AbstractType<Duration> {
    private static Logger log = Logger.getLogger(DurationType.class.getName());

    public DurationType(String regExp) {
        super(regExp);
    }

    public DurationType(String regExp, String contentAsString) {
        super(regExp);
        this.instantiateFromString(contentAsString);
    }

    public DurationType() {
        this(null);
    }

    /**
     * 10 days
     *
     * @param content
     * @return
     */
    protected Duration construct(String content) {
        log.info(" construct duration instance from string ' " + content + " '");
        if (content == null) return null;
        String[] contentArray = content.split(" ");
        assert contentArray.length == 2;
        final String qtyString = contentArray[0];
        final String units = contentArray[1];
        final long value = Long.valueOf(qtyString);
        String ss = units.endsWith("s") ? units.substring(0, units.length() - 1) : units;
        return Duration.of(value, ss);
    }

    @Override
    public String toString() {
        return "{" +
                "DurationType ='" + getValue() + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DurationType)) return false;
        DurationType that = (DurationType) o;
        if (this.t.compareTo(that.t) != 0) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return t.hashCode();
    }

}