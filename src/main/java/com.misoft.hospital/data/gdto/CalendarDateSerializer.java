package com.misoft.hospital.data.gdto;

import com.misoft.hospital.model.shared.time.CalendarDate;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;

/**
 * @author mokua
 *         {
 *         <p/>
 *         yyy-MM-dd
 *         }
 */
public class CalendarDateSerializer extends JsonSerializer<CalendarDate> {

    @Override
    public void serialize(CalendarDate value, JsonGenerator jsonGenerator, SerializerProvider provider) throws IOException {
        jsonGenerator.writeString(value.toString());
    }
}
