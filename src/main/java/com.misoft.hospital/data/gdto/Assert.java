
package com.misoft.hospital.data.gdto;

/**
 * @author Adam Bien, www.adam-bien.com
 */

public class Assert {

    public static void notNull(Object actual, String message) {
        if (actual == null)
            throw new IllegalStateException(message);
    }
}
