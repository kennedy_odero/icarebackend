package com.misoft.hospital.data.gdto;

import com.misoft.hospital.model.shared.Money;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Currency;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Logger;

/**
 * @author mokua
 *         {
 *         <p/>
 *         "currency": "KES",
 *         "value": 4564
 *         }
 */
public class MoneyDeserializer extends JsonDeserializer<Money> {
    private static Logger log = Logger.getLogger(MoneyDeserializer.class.getName());

    @Override
    public Money deserialize(JsonParser jsonParser, DeserializationContext ctxt) throws IOException {
        ObjectCodec oc = jsonParser.getCodec();
        JsonNode node = oc.readTree(jsonParser);
        Iterator<Map.Entry<String, JsonNode>> fields = node.fields();
        log.info(" All fields for money " + fields);
        Money returnValue = null;
        Map.Entry<String, JsonNode> next = fields.next();
        String currencyCode = next.getValue().textValue();
        final Currency currency = Currency.getInstance(currencyCode);
        //get next
        next = fields.next();
        final BigDecimal value = next.getValue().decimalValue();
        log.info(" currency code ' " + currencyCode + " ' , value ' " + value + "' ");

        returnValue = Money.valueOf(value, currency);
        return returnValue;
    }
}
