package com.misoft.hospital.data.gdto;

import com.misoft.hospital.model.shared.time.CalendarDate;
import com.misoft.hospital.util.dates.DateConverter;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.util.logging.Logger;

/**
 * @author mokua
 *         {
 *         <p/>
 *         yyyy-MM-dd
 *         }
 */
public class CalendarDateDeserializer extends JsonDeserializer<CalendarDate> {
    private static Logger log = Logger.getLogger(CalendarDateDeserializer.class.getName());

    @Override
    public CalendarDate deserialize(JsonParser jsonParser, DeserializationContext ctxt) throws IOException {
        JsonToken curr = jsonParser.getCurrentToken();
        // Usually should just get string value:
        CalendarDate returnValue = null;
        if (curr == JsonToken.VALUE_STRING) {
            // BEGIN NULL_STRING fix
            if (jsonParser.getText() == null) {
                return null;
            }
            // END NULL_STRING fix

            String dateString = jsonParser.getText();
            DateConverter dc = new DateConverter(true);
            try {
                Date date = dc.parse(dateString);
                returnValue = CalendarDate.fromDate(date);
            } catch (ParseException e) {
                e.printStackTrace();
                throw new IllegalStateException(e);
            }
        }

        return returnValue;
    }
}
