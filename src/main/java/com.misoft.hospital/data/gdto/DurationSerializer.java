package com.misoft.hospital.data.gdto;

import com.misoft.hospital.model.shared.time.Duration;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;

/**
 * @author mokua
 *         {
 *         "unit": "day",
 *         "quantity": 10
 *         }
 */
public class DurationSerializer extends JsonSerializer<Duration> {

    @Override
    public void serialize(Duration value, JsonGenerator jsonGenerator, SerializerProvider provider) throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeStringField("unit", value.getForPersistentMapping_Unit().toString());
        jsonGenerator.writeNumberField("quantity", value.getForPersistentMapping_Quantity());
        jsonGenerator.writeEndObject();
    }
}
