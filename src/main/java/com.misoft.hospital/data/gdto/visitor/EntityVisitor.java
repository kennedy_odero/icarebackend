package com.misoft.hospital.data.gdto.visitor;

import com.misoft.hospital.data.gdto.types.AbstractType;
import com.misoft.hospital.data.gdto.types.BigDecimalType;
import com.misoft.hospital.data.gdto.types.BooleanType;

public interface EntityVisitor<T, K> {
    T visit(AbstractType unknown, K args);

    T visit(BigDecimalType unknown, K args);

    T visit(BooleanType source, K args);

}