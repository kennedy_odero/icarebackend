package com.misoft.hospital.data.gdto;

import com.misoft.hospital.model.shared.Money;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;

/**
 * @author mokua
 *         {
 *         <p/>
 *         "currency": "KES",
 *         "value": 4564
 *         }
 */
public class MoneySerializer extends JsonSerializer<Money> {

    @Override
    public void serialize(Money value, JsonGenerator jsonGenerator, SerializerProvider provider) throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeStringField("currency", value.breachEncapsulationOfCurrency().getCurrencyCode());
        jsonGenerator.writeNumberField("value", value.breachEncapsulationOfAmount());
        jsonGenerator.writeEndObject();
    }
}
