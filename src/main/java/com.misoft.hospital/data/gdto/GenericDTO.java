package com.misoft.hospital.data.gdto;


import com.misoft.hospital.data.gdto.types.*;
import com.misoft.hospital.model.shared.Money;
import com.misoft.hospital.model.shared.time.CalendarDate;
import com.misoft.hospital.model.shared.time.Duration;
import com.misoft.hospital.util.dates.DateConverter;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.*;

import static com.misoft.hospital.data.gdto.Assert.notNull;


@JsonDeserialize(using = GenericDTODeserializer.class)
@JsonSerialize(using = GenericDTOSerializer.class)
public class GenericDTO implements Serializable {
    public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

    public static final String DATE_FORMAT_2 = "yyyy-MM-dd";
    // public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

    private static final long serialVersionUID = 1L;


    private Map<String, Attribute> attributes = null;

    private Map<String, Set<GenericDTO>> relations = null;

    private Map<String, GenericDTO> relations2 = null;


    private String name = null;

    public GenericDTO() {
        this.attributes = new HashMap<>();
        this.relations = new HashMap<>();
        this.relations2 = new HashMap<>();
    }

    public GenericDTO(String name) {
        notNull(name, "The name of the DTO cannot be null...");
        this.attributes = new HashMap<>();
        this.relations = new HashMap<>();
        this.relations2 = new HashMap<>();
        this.name = name;
    }

    public GenericDTO add(String name, Attribute attribute) {
        notNull(name, "Attribute name cannot be null");
        //notNull(attribute, "Attribute with name: " + name + " is null!");
        this.attributes.put(name, attribute);
        return this;
    }

    public GenericDTO addString(String name, String value) {
        notNull(name, "Attribute name cannot be null");
        //notNull(value, "Attribute with name: " + name + " is null!");
        this.attributes.put(name, new StringType(null, value));
        return this;
    }

    public GenericDTO addChar(String name, Character value) {
        notNull(name, "Attribute name cannot be null");
        //notNull(value, "Attribute with name: " + name + " is null!");
        this.attributes.put(name, new CharacterType(null, String.valueOf(value)));
        return this;
    }


    public GenericDTO addDate(String name, Date value) {
        notNull(name, "Attribute name cannot be null");
        //notNull(value, "Attribute with name: " + name + " is null!");
        //SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
        DateConverter sdf = new DateConverter(true);
        this.attributes.put(name, value == null ? null : new DateType(null, sdf.format(value)));
        return this;
    }

    public GenericDTO addInt(String name, Integer value) {
        notNull(name, "Attribute name cannot be null");
        //notNull(value, "Attribute with name: " + name + " is null!");
        this.attributes.put(name, new IntType(null, value == null ? null : String.valueOf(value)));
        return this;
    }

    public GenericDTO addBigDecimal(String name, BigDecimal value) {
        notNull(name, "Attribute name cannot be null");
        //notNull(value, "Attribute with name: " + name + " is null!");
        this.attributes.put(name, new BigDecimalType(null, value == null ? null : value.toString()));
        return this;
    }

    public GenericDTO addLong(String name, Long value) {
        notNull(name, "Attribute name cannot be null");
        //notNull(value, "Attribute with name: " + name + " is null!");
        this.attributes.put(name, new LongType(null, value == null ? null : value.toString()));
        return this;
    }


    public GenericDTO addFloat(String name, Float value) {
        notNull(name, "Attribute name cannot be null");
        //notNull(value, "Attribute with name: " + name + " is null!");
        this.attributes.put(name, new FloatType(null, value == null ? null : String.valueOf(value)));
        return this;
    }

    public GenericDTO addDouble(String name, Double value) {
        notNull(name, "Attribute name cannot be null");
        //notNull(value, "Attribute with name: " + name + " is null!");
        this.attributes.put(name, new DoubleType(null, value == null ? null : String.valueOf(value)));
        return this;
    }

    public GenericDTO addBoolean(String fieldName, Boolean value) {
        notNull(name, "Attribute name cannot be null");
        //notNull(value, "Attribute with name: " + name + " is null!");
        this.attributes.put(fieldName, new BooleanType(null, value == null ? null : String.valueOf(value)));
        return this;
    }


    public GenericDTO addBigInteger(String name, BigInteger value) {
        notNull(name, "Attribute name cannot be null");
        //notNull(value, "Attribute with name: " + name + " is null!");
        this.attributes.put(name, new BigIntegerType(null, value == null ? null : value.toString()));
        return this;
    }

    public GenericDTO addMoney(String name, Money value) {
        notNull(name, "Attribute name cannot be null");
        //notNull(value, "Attribute with name: " + name + " is null!");
        this.attributes.put(name, new MoneyType(null, value == null ? Money.shillings(0.0).toString() : value.toString()));
        return this;
    }


    public GenericDTO addCalendarDate(String name, CalendarDate value) {
        notNull(name, "Attribute name cannot be null");
        //notNull(value, "Attribute with name: " + name + " is null!");
        this.attributes.put(name, new CalendarDateType(null, value == null ? null : value.toString()));
        return this;
    }


    public GenericDTO addDuration(String name, Duration value) {
        notNull(name, "Attribute name cannot be null");
        //notNull(value, "Attribute with name: " + name + " is null!");
        this.attributes.put(name, new DurationType(null, value == null ? null : value.toString()));
        return this;
    }


    public GenericDTO remove(String name) {
        notNull(name, "Attribute name cannot be null");
        this.attributes.remove(name);
        return this;
    }

    public GenericDTO addRelation(String relationName, GenericDTO genericDTO) {
        notNull(relationName, "The name of the relation cannot be null !");
        notNull(genericDTO, "The target cannot for the relation with name " + relationName + " be null");
        addTarget(relationName, genericDTO);
        return this;
    }

    public GenericDTO addRelation2(String relationName, GenericDTO genericDTO) {
        Assert.notNull(relationName, "The name of the relation cannot be null !");
        notNull(genericDTO, "The target cannot for the relation with name " + relationName + " be null");
        //TODO check that it does not exist
        this.relations2.put(relationName, genericDTO);
        return this;
    }

    private GenericDTO addTarget(String relationName, GenericDTO target) {
        Set<GenericDTO> targets = this.relations.get(relationName);
        if (targets == null) {
            targets = new LinkedHashSet<>();
            this.relations.put(relationName, targets);
        }
        targets.add(target);
        return this;
    }

    public <T> Attribute<T> get(String name) {
        notNull(name, "Attribute name cannot be null");
        return this.attributes.get(name);
    }

    public Set<GenericDTO> getTargets(String name) {
        notNull(name, "The name of the relation cannot be null !");
        return this.relations.get(name);
    }

    public GenericDTO getTarget(String name) {
        notNull(name, "The name of the relation cannot be null !");
        return this.relations.get(name).iterator().next();
    }

    public GenericDTO getTarget2(String name) {
        notNull(name, "The name of the relation cannot be null !");
        return this.relations2.get(name);
    }

    public Iterator<String> getAttributeNames() {
        return this.attributes.keySet().iterator();
    }

    public Iterator<String> getRelationNames() {
        return this.relations.keySet().iterator();
    }

    public Iterator<Attribute> getAttributes() {
        return this.attributes.values().iterator();
    }

    public Iterator<Set<GenericDTO>> getTargets() {
        return this.relations.values().iterator();
    }

    public GenericDTO validate() throws CompositeValidationException {
        Set<Map.Entry<String, Attribute>> attributeEntries = this.attributes.entrySet();
        Iterator<Map.Entry<String, Attribute>> attributeIterator = attributeEntries.iterator();
        CompositeValidationException compositeValidationException = new CompositeValidationException(this.name);
        Map.Entry<String, Attribute> entry = null;
        while (attributeIterator.hasNext()) {
            try {
                entry = attributeIterator.next();
                Attribute attributeEntry = entry.getValue();
                attributeEntry.validate();
            } catch (ValidationException ex) {
                compositeValidationException.add(entry.getKey(), ex);
            }
            //some validation errors occurred
            if (!compositeValidationException.isEmpty()) {
                throw compositeValidationException;
            }

        }
        return this;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append("GDTO Name: ").append(this.name);
        result.append("\n").append("--- attributes ---").append("\n");
        Set<Map.Entry<String, Attribute>> attributeEntries = this.attributes.entrySet();
        Iterator<Map.Entry<String, Attribute>> attributeIterator = attributeEntries.iterator();
        while (attributeIterator.hasNext()) {
            Map.Entry<String, Attribute> entry = attributeIterator.next();
            result.append(" Name: ").append(entry.getKey()).append(", Content: ").append(entry.getValue() == null ? "" : entry.getValue().getValue());
        }
        return result.toString();
    }

    public Attribute getId() {
        Iterator<Attribute> iterator = this.getAttributes();
        while (iterator.hasNext()) {
            Attribute attribute = iterator.next();
            if (attribute.isId()) {
                return attribute;
            }
        }
        return null;
    }

    public int getNumberOfAttributes() {
        return this.attributes.size();
    }

    public boolean isEmpty() {
        return (this.attributes.isEmpty() && this.relations.isEmpty());
    }


    //TODO get the default field names in the format a, or a:b (for relationship)
    public static Collection<String> fieldNames(GenericDTO dto) {
        Set<String> attributeFields = new LinkedHashSet<>();
        final Iterator<String> attributeNames = dto.getAttributeNames();
        while (attributeNames.hasNext()) {
            String next = attributeNames.next();
            attributeFields.add(next);
        }
        final Map<String, GenericDTO> entityAttributes = dto.getRelations2();
        if(entityAttributes != null){
            Iterator<Map.Entry<String, GenericDTO>> it = entityAttributes.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry<String, GenericDTO> pair = it.next();
                final Iterator<String> attrNames = pair.getValue().getAttributeNames();
                attributeFields.add(pair.getKey() + ":id");//just in case the id attribute is not there
                while (attrNames.hasNext()) {
                    String next = attrNames.next();
                    attributeFields.add(pair.getKey() + ":" + next);
                }
            }
        }
        dto.getRelations2();
        //now the relations
        /*Iterator<String> relationsIt = dto.getRelationNames();
        while (relationsIt.hasNext()) {
            String next = relationsIt.next();
            //hahahaha!, how to get the children
            //for each relationship, get the attributes and relationships of the targets
            GenericDTO target = dto.getTarget(next);
            Collection<String> childFields = fieldNames(target);
            //add the prefix
            for (String child : childFields) {
                attributeFields.add(next + ":" + child);
            }
        }*/
        //attributeFields.add("id");
        return attributeFields;
    }

    /**
     * ******************************************
     */
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public Map<String, Attribute> attributeMap() {
        return attributes;
    }

    public Map<String, Set<GenericDTO>> getRelations() {
        return relations;
    }

    public void setRelations(Map<String, Set<GenericDTO>> relations) {
        this.relations = relations;
    }

    public void setAttributes(Map<String, Attribute> attributes) {
        this.attributes = attributes;
    }

    public Map<String, GenericDTO> getRelations2() {
        return relations2;
    }
}
