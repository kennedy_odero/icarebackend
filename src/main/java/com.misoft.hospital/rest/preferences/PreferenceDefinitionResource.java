package com.misoft.hospital.rest.preferences;

import com.misoft.hospital.model.preferences.PreferenceDefinition;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 10/27/15.
 */
@Stateless
@Path("/preferenceDefinitions")
public class PreferenceDefinitionResource extends BaseEntityService<PreferenceDefinition>{
    public PreferenceDefinitionResource(){
        super(PreferenceDefinition.class);
    }
}
