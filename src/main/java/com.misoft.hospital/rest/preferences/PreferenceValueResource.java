package com.misoft.hospital.rest.preferences;

import com.misoft.hospital.data.gdto.GenericDTO;
import com.misoft.hospital.model.admin.UserProfile;
import com.misoft.hospital.model.preferences.PreferenceDefinition;
import com.misoft.hospital.model.preferences.PreferenceValue;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import java.util.List;

/**
 * Created by kodero on 10/27/15.
 */
@Stateless
@Path("/preferenceValues")
public class PreferenceValueResource extends BaseEntityService<PreferenceValue>{
    public PreferenceValueResource(){
        super(PreferenceValue.class);
    }

    @GET
    @Override
    @Produces(MediaType.APPLICATION_JSON)
    public List<GenericDTO> getAll(@Context UriInfo uriInfo,
                                   @QueryParam("pageSize") @DefaultValue("25") int pageSize,
                                   @QueryParam("page") @DefaultValue("1") int pageNum,
                                   @QueryParam("orderBy") List<String> orderBy,
                                   @QueryParam("fields") String fieldList) {

        //check if all permissions for this profile have been added
        //TODO, put this code in a setup class
        /*final String profileId = uriInfo.getQueryParameters().getFirst("profileId");
        if (profileId == null || profileId.trim().isEmpty()) {
            throw new WebApplicationException("No profile id specified", Response.Status.BAD_REQUEST);
        }
        UserProfile profile = getEntityManager().find(UserProfile.class, profileId);*/
        List<PreferenceDefinition> preferenceDefinitions = getEntityManager()
                .createQuery("select pd from PreferenceDefinition pd where pd.id not in (select pv.preferenceDefinition.id from PreferenceValue pv) ")
                .getResultList();
        for(PreferenceDefinition p : preferenceDefinitions){
            //create Preference value and save it
            PreferenceValue pv = new PreferenceValue();
            pv.setPreferenceDefinition(p);
            getEntityManager().merge(pv);
        }
        return super.getAll(uriInfo, pageSize, pageNum, orderBy, fieldList);
    }
}
