package com.misoft.hospital.rest.preferences;

import com.misoft.hospital.model.preferences.PreferenceCategory;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 10/27/15.
 */
@Stateless
@Path("/preferenceCategories")
public class PreferenceCategoryResource extends BaseEntityService<PreferenceCategory>{
    public PreferenceCategoryResource(){
        super(PreferenceCategory.class);
    }
}
