package com.misoft.hospital.rest.billing;

import com.misoft.hospital.data.gdto.GenericDTO;
import com.misoft.hospital.model.billing.PatientBill;
import com.misoft.hospital.rest.BaseEntityService;
import com.misoft.hospital.util.types.DateParam;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by kodero on 9/14/15.
 */
@Stateless
@Path("/patientBills")
public class PatientBillResource extends BaseEntityService<PatientBill>{
    @Inject
    EntityManager em;

    public PatientBillResource(){
        super(PatientBill.class);
    }

    @GET
    @Path("/revenueByDepartment")
    public Response revenueByDepartment(@QueryParam("dateFrom") @DefaultValue("today")DateParam dateFrom, @QueryParam("dateTo") @DefaultValue("today")DateParam dateTo){
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String sql = "select u.name,\n" +
                "sum(case bi.bill_item_type \n" +
                " when 'Normal' then (bi.quantity * bi.unit_price)\n" +
                " when 'Accumulation' then DATEDIFF(coalesce(bi.date_to, now()), bi.date_from) * bi.unit_price \n" +
                " end ) total\n" +
                "from bill_items bi \n" +
                "\tinner join products p \n" +
                "\t\ton bi.product = p.id \n" +
                "\tinner join units u \n" +
                "\t\ton p.unit = u.id \n" +
                "\tinner join patient_bills pb\n" +
                "\t\ton bi.patient_bill = pb.id\n" +
                "where bi.deleted <> 1\n" +
                "\tand pb.date_generated >= '" + df.format(dateFrom.getDate()) + "' and pb.date_generated <= '" + df.format(dateTo.getDate()) + "'\n" +
                "group by u.name";
        List<Object> results = em.createNativeQuery(sql)
                .getResultList();
        List<GenericDTO> balances = new ArrayList<>();
        for(Object o : results){
            Object[] row = (Object[]) o;
            GenericDTO genericDTO = new GenericDTO("com.misoft.reports.RevenueByDepartment");
            genericDTO.addString("department", (String) row[0]);
            genericDTO.addBigDecimal("total", (BigDecimal) row[1]);
            balances.add(genericDTO);
        }
        return Response.ok(balances).build();
    }

    @GET
    @Path("/dailyRevenueByDepartment")
    public Response dailyRevenueByDepartment(@QueryParam("departmentId") @DefaultValue("xdsdsd") String departmentId, @QueryParam("dateFrom") @DefaultValue("today")DateParam dateFrom, @QueryParam("dateTo") @DefaultValue("today")DateParam dateTo){
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String sql = "select DATE(pb.date_generated), u.name,\n" +
                "sum(case bi.bill_item_type \n" +
                " when 'Normal' then (bi.quantity * bi.unit_price)\n" +
                " when 'Accumulation' then DATEDIFF(coalesce(bi.date_to, now()), bi.date_from) * bi.unit_price \n" +
                " end ) total\n" +
                "from bill_items bi \n" +
                "\tinner join products p \n" +
                "\t\ton bi.product = p.id \n" +
                "\tinner join units u \n" +
                "\t\ton p.unit = u.id \n" +
                "\tinner join patient_bills pb\n" +
                "\t\ton bi.patient_bill = pb.id\n" +
                "where bi.deleted <> 1\n" +
                "   and u.id ='" + departmentId + "' " +
                "\tand pb.date_generated >= '" + df.format(dateFrom.getDate()) + "' and pb.date_generated <= '" + df.format(dateTo.getDate()) + "'\n" +
                "group by DATE(pb.date_generated) " +
                "order by pb.date_generated asc";
        List<Object> results = em.createNativeQuery(sql)
                .getResultList();
        List<GenericDTO> balances = new ArrayList<>();
        for(Object o : results){
            Object[] row = (Object[]) o;
            GenericDTO genericDTO = new GenericDTO("com.misoft.reports.DailyRevenueByDepartment");
            genericDTO.addDate("date", (Date) row[0]);
            genericDTO.addString("department", (String) row[1]);
            genericDTO.addBigDecimal("total", (BigDecimal) row[2]);
            balances.add(genericDTO);
        }
        return Response.ok(balances).build();
    }
}
