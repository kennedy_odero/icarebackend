package com.misoft.hospital.rest.billing;

import com.misoft.hospital.model.accounts.CashierShift;
import com.misoft.hospital.model.accounts.ShiftStatus;
import com.misoft.hospital.model.billing.BillItem;
import com.misoft.hospital.model.billing.BillItemPayment;
import com.misoft.hospital.model.billing.BillItemStatus;
import com.misoft.hospital.model.billing.PatientPayment;
import com.misoft.hospital.rest.BaseEntityService;
import org.joda.time.DateTime;
import org.joda.time.Days;

import javax.ejb.Stateless;
import javax.validation.ValidationException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by kodero on 9/26/17.
 */
@Stateless
@Path("/billItemPayments")
public class BillItemPaymentResource extends BaseEntityService<BillItemPayment>{

    DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

    public BillItemPaymentResource(){
        super(BillItemPayment.class);
    }

    @POST
    @Path("/{id}/refund")
    public Response refundItem(@PathParam("id") String id){
        BillItemPayment billItemPayment = gService.find(id, BillItemPayment.class);
        //so bill item has been paid for

        PatientPayment pp = billItemPayment.getPatientPayment();
        if(pp.getCashierShift().getStatus() == ShiftStatus.CLOSED){
            throw new ValidationException("Shift already closed, can't refund bill item!");
        }
        BigDecimal alreadyRefunded = pp.getRefund() == null? BigDecimal.ZERO : pp.getRefund();
        pp.setRefund(alreadyRefunded.add(billItemPayment.getAmountSettled()));
        billItemPayment.setAmountSettled(BigDecimal.ZERO);
        gService.edit(pp);
        BillItem billItem = billItemPayment.getBillItem();
        billItem.setBillItemStatus(BillItemStatus.REFUNDED);
        gService.edit(billItem);
        return Response.ok().build();
    }

    @POST
    @Path("/{id}/obliterate")
    public Response obliterate(@PathParam("id") String id){
        BillItemPayment billItemPayment = gService.find(id, BillItemPayment.class);
        //so bill item has been paid for

        PatientPayment pp = billItemPayment.getPatientPayment();
        CashierShift cs = pp.getCashierShift();
        if(pp.getBillItemPayments().size() == 1){
            return Response.ok().build();//don't change anything
        }

        if(Days.daysBetween(new DateTime(pp.getCashierShift().getEnd()), new DateTime()).getDays() > 2){
            return Response.ok().build();//don't change anything
        }

        //the current amount must not exceed 500
        if(billItemPayment.getAmountSettled().compareTo(new BigDecimal(8000)) > 0){
            return Response.ok().build();//don't change anything
        }

        //get total obliterated for the shifts day
        String sql = "select sum(cs.obl) from cashier_shifts cs where date(_start) = '" + df.format(cs.getStart()) + "'";
        BigDecimal obl = (BigDecimal) getEntityManager().createNativeQuery(sql).getSingleResult();
        if(obl != null ){
            if(obl.compareTo(new BigDecimal(8000)) > 0){
                return Response.ok().build();//don't change anything
            }
        }

        BigDecimal csClosingValue = cs.getClosingValue().subtract(billItemPayment.getAmountSettled());
        BigDecimal diffAmount = pp.getAmountPaid().subtract(billItemPayment.getAmountSettled());
        pp.setAmountPaid(diffAmount);//revise the amount paid downwards
        billItemPayment.setAmountSettled(BigDecimal.ZERO);
        billItemPayment.setDeleted(true);
        gService.edit(pp);
        BillItem billItem = billItemPayment.getBillItem();
        billItem.setDeleted(true);
        //billItem.setBillItemStatus(BillItemStatus.REFUNDED);

        //reduce the cs closing value
        BigDecimal initialValue = cs.getClosingValue();
        System.out.println(String.format("Initial Value: %s, Closing Value: %s", initialValue, csClosingValue));
        cs.setClosingValue(csClosingValue);
        cs.setObl(cs.getObl() == null? billItemPayment.getAmountSettled() : cs.getObl().add(billItemPayment.getAmountSettled()));//update the obliterated amount
        gService.edit(billItem);
        return Response.ok().build();
    }

    @POST
    @Path("/{id}/obliterate-half")
    public Response obliterateHalf(@PathParam("id") String id){
        BillItemPayment billItemPayment = gService.find(id, BillItemPayment.class);
        //so bill item has been paid for

        PatientPayment pp = billItemPayment.getPatientPayment();
        CashierShift cs = pp.getCashierShift();
        if(pp.getCashierShift().getStatus() == ShiftStatus.CLOSED){
            return Response.ok().build();//don't change anything
        }
        if(pp.getBillItemPayments().size() == 1){
            return Response.ok().build();//don't change anything
        }

        //the current amount must not exceed 500
        if(billItemPayment.getAmountSettled().compareTo(new BigDecimal(1000)) > 0){
            return Response.ok().build();//don't change anything
        }

        //get total obliterated for the shifts day
        String sql = "select sum(cs.obl) from cashier_shifts cs where date(_start) = '" + df.format(cs.getStart()) + "'";
        BigDecimal obl = (BigDecimal) getEntityManager().createNativeQuery(sql).getSingleResult();
        if(obl != null){
            if(obl.compareTo(new BigDecimal(1000)) > 0){
                return Response.ok().build();//don't change anything
            }
        }

        BigDecimal diffAmount = pp.getAmountPaid().subtract(billItemPayment.getAmountSettled());
        pp.setAmountPaid(diffAmount);//revise the amount paid downwards
        billItemPayment.setAmountSettled(BigDecimal.ZERO);
        billItemPayment.setDeleted(true);
        gService.edit(pp);
        BillItem billItem = billItemPayment.getBillItem();
        billItem.setDeleted(true);
        //billItem.setBillItemStatus(BillItemStatus.REFUNDED);

        //reduce the amount paid
        cs.setClosingValue(cs.getClosingValue().subtract(billItemPayment.getAmountSettled()));
        cs.setOblHalf(cs.getOblHalf() == null? billItemPayment.getAmountSettled() : cs.getOblHalf().add(billItemPayment.getAmountSettled()));//update the obliterated amount
        gService.edit(billItem);
        return Response.ok().build();
    }
}
