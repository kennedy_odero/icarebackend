package com.misoft.hospital.rest.billing;

import com.misoft.hospital.model.billing.PatientCredit;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 12/17/15.
 */
@Stateless
@Path("/patientCredits")
public class PatientCreditResource extends BaseEntityService<PatientCredit>{
    public PatientCreditResource(){
        super(PatientCredit.class);
    }
}
