package com.misoft.hospital.rest.billing;

import com.misoft.hospital.model.accounts.ShiftStatus;
import com.misoft.hospital.model.billing.BillItem;
import com.misoft.hospital.model.billing.BillItemPayment;
import com.misoft.hospital.model.billing.BillItemStatus;
import com.misoft.hospital.model.billing.PatientPayment;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.validation.ValidationException;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created by kodero on 4/23/16.
 */
@Stateless
@Path("/billItems")
public class BillItemResource extends BaseEntityService<BillItem>{
    public BillItemResource(){
        super(BillItem.class);
    }

    @POST
    @Path("/{id}/refund")
    public Response refundItem(@PathParam("id") String id){
        BillItem billItem = gService.find(id, BillItem.class);
        if(billItem.getBalance().compareTo(BigDecimal.ZERO) != 0){
            throw new ValidationException("Bill item not fully settled, can't refund");
        }
        //so bill item has been paid for
        List<BillItemPayment> billItemPayments = getEntityManager ().createQuery("from BillItemPayment bip where bip.billItem.id =:billItemId")
                .setParameter("billItemId", billItem.getId())
                .getResultList();
        for(BillItemPayment bip : billItemPayments){
            PatientPayment pp = bip.getPatientPayment();
            if(pp.getCashierShift().getStatus() == ShiftStatus.CLOSED){
                throw new ValidationException("Shift already closed, can't cancel bill item!");
            }
            BigDecimal alreadyRefunded = pp.getRefund() == null? BigDecimal.ZERO : pp.getRefund();
            pp.setRefund(alreadyRefunded.add(bip.getAmountSettled()));
            gService.edit(pp);
        }
        billItem.setBillItemStatus(BillItemStatus.REFUNDED);
        return Response.ok().build();
    }

    @POST
    @Path("/{id}/cancel")
    public Response cancelItem(@PathParam("id") String id){
        BillItem billItem = gService.find(id, BillItem.class);
        if(billItem.getBalance().compareTo(BigDecimal.ZERO) == 0){
            throw new ValidationException("Bill item fully settled, can't cancel. Try refund instead");
        }
        billItem.setBillItemStatus(BillItemStatus.CANCELLED);
        return Response.ok().build();
    }
}
