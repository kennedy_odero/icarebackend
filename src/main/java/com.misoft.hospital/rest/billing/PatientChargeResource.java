package com.misoft.hospital.rest.billing;

import com.misoft.hospital.model.billing.PatientCharge;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 1/18/16.
 */
@Stateless
@Path("/patientCharges")
public class PatientChargeResource extends BaseEntityService<PatientCharge>{
    public PatientChargeResource(){
        super(PatientCharge.class);
    }
}
