package com.misoft.hospital.rest.billing;

import com.misoft.hospital.model.billing.*;
import com.misoft.hospital.model.healthcare.PatientStatus;
import com.misoft.hospital.rest.BaseEntityService;
import com.misoft.hospital.rest.util.SequenceManager;

import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.validation.ValidationException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

/**
 * Created by kodero on 6/7/16.
 */
@Stateless
@Path("/billItemAmendments")
public class BillItemAmendmentResource extends BaseEntityService<BillItemAmendment>{

    @Inject
    SequenceManager sequenceManager;

    @Inject
    Event<BillAmendedEvent> billAmendedEventEvent;

    public BillItemAmendmentResource(){
        super(BillItemAmendment.class);
    }

    @POST
    @Path("/{id:[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}}/submit")
    public Response submit(@PathParam("id") String id){
        BillItemAmendment billItemAmendment = gService.find(id, entityClass);
        if(billItemAmendment == null) {
            log.log(Level.SEVERE, "No bill item amendment with id {" + id + "} given!");
            return Response.status(404).build();
        }
        if(billItemAmendment.getAmendmentStatus() != AmendmentStatus.NEW){
            return createValidationResponse(new ValidationException("Can only submit an amendment in 'New' status")).build();
        }
        billItemAmendment.setAmendmentStatus(AmendmentStatus.SUBMITTED);
        entityUpdatedEventSrc.fire(new EntityUpdatedEvent<>(billItemAmendment));
        return Response.ok().build();
    }

    @POST
    @Path("/{id:[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}}/cancel")
    public Response cancel(@PathParam("id") String id){
        BillItemAmendment billItemAmendment = gService.find(id, entityClass);
        if(billItemAmendment == null) {
            log.log(Level.SEVERE, "No bill item amendment with id {" + id + "} given!");
            return Response.status(404).build();
        }
        if(billItemAmendment.getAmendmentStatus()!= AmendmentStatus.NEW){
            return createValidationResponse(new ValidationException("Can only cancel a waiver in 'New' status")).build();
        }
        billItemAmendment.setAmendmentStatus(AmendmentStatus.CANCELLED);
        entityUpdatedEventSrc.fire(new EntityUpdatedEvent<>(billItemAmendment));
        return Response.ok().build();
    }

    @POST
    @Path("/{id:[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}}/approve")
    public Response approve(@PathParam("id") String id){
        BillItemAmendment billItemAmendment = gService.find(id, entityClass);
        if(billItemAmendment == null) {
            log.log(Level.SEVERE, "No bill item amendment with id {" + id + "} given!");
            return Response.status(404).build();
        }
        if(billItemAmendment.getAmendmentStatus() != AmendmentStatus.SUBMITTED){
            return createValidationResponse(new ValidationException("Can only approve an amendment in 'Submitted' status")).build();
        }
        PatientBill patientBill = billItemAmendment.getPatientBill();
        //BigDecimal balance = patientBill.getBalance();
        BigDecimal amountToRefund = BigDecimal.ZERO;
        if(billItemAmendment.getAmendmentOperation() == AmendmentOperation.REMOVE){
            billItemAmendment.getBillItem().setDeleted(true);
            billItemAmendment.getBillItem().setDeletedAt(new Date());
            amountToRefund = billItemAmendment.getBillItem().getQuantity().multiply(billItemAmendment.getBillItem().getProduct().getListPrice());
        }else if(billItemAmendment.getAmendmentOperation() == AmendmentOperation.UPDATE){
            BigDecimal diffQuantity = billItemAmendment.getBillItem().getQuantity().subtract(new BigDecimal(billItemAmendment.getNewQty()));
            billItemAmendment.getBillItem().setQuantity(new BigDecimal(billItemAmendment.getNewQty()));
            amountToRefund = diffQuantity.multiply(billItemAmendment.getBillItem().getProduct().getListPrice());
        }
        gService.edit(billItemAmendment.getBillItem());
        BigDecimal currentAmountToRefund = amountToRefund;
        log.info("currentAmountToRefund 1 : " + currentAmountToRefund);
        List<BillPayment> billPayments = new ArrayList<>();
        //update the bill payments to match what should have been paid
        for(BillPayment bp : patientBill.getBillPayments()){
            log.info("currentAmountToRefund 2 : " + currentAmountToRefund);
            if(bp.getPatientBill().getId().equalsIgnoreCase(patientBill.getId()) && currentAmountToRefund.compareTo(BigDecimal.ZERO) > 0){
                log.info("currentAmountToRefund 3 : " + currentAmountToRefund);
                //only modify if we are in this same bill, and the currentAmountToRefund is greater than zero
                if(bp.getAmountSettled().compareTo(currentAmountToRefund) > 0){//amount paid is less or
                    log.info("currentAmountToRefund 4.1 : " + currentAmountToRefund);
                    bp.setAmountSettled(bp.getAmountSettled().subtract(currentAmountToRefund));
                    currentAmountToRefund = currentAmountToRefund.subtract(bp.getAmountSettled());
                    log.info("currentAmountToRefund 4.2 : " + currentAmountToRefund);
                }else if(bp.getAmountSettled().compareTo(currentAmountToRefund) <= 0){
                    log.info("currentAmountToRefund 5.1 : " + currentAmountToRefund);
                    BigDecimal amountRemoved = bp.getAmountSettled();
                    BigDecimal amountToSettle = amountRemoved.subtract(currentAmountToRefund);
                    if(amountToSettle.compareTo(BigDecimal.ZERO) < 0) bp.setAmountSettled(BigDecimal.ZERO);
                    else bp.setAmountSettled(amountToSettle);
                    currentAmountToRefund = currentAmountToRefund.subtract(amountRemoved);
                    log.info("currentAmountToRefund 5.2 : " + currentAmountToRefund);
                }
                gService.edit(bp);
            }
        }

        if(patientBill.getBalance().compareTo(BigDecimal.ZERO) < 0 && patientBill.getPatient().getPatientStatus() != PatientStatus.ADMITTED){
            //balance after modifying the bill is -ve, so refund it
            BigDecimal toRefund = patientBill.getBalance();
            Refund refund = new Refund();
            refund.setRefundNo(sequenceManager.nextSequence("PATIENT_REFUND"));
            refund.setPatient(patientBill.getPatient());
            refund.setDateCreated(new Date());
            refund.setNotes(billItemAmendment.getReason());
            refund.setAmountPaid(patientBill.getBalance().abs());
            refund.setRefundStatus(RefundStatus.APPROVED);
            PatientPayment patientPayment = new ArrayList<>(patientBill.getBillPayments()).get(0).getPatientPayment();
            refund.addPaymentRefund(new PaymentRefund(patientPayment, toRefund));
        }
        billItemAmendment.setAmendmentStatus(AmendmentStatus.APPROVED);
        entityUpdatedEventSrc.fire(new EntityUpdatedEvent<>(billItemAmendment));
        billAmendedEventEvent.fire(new BillAmendedEvent(patientBill));
        return Response.ok().build();
    }

    @POST
    @Path("/{id:[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}}/decline")
    public Response reject(@PathParam("id") String id){
        BillItemAmendment billItemAmendment = gService.find(id, entityClass);
        if(billItemAmendment == null) {
            log.log(Level.SEVERE, "No bill amendment with id {" + id + "} given!");
            return Response.status(404).build();
        }
        if(billItemAmendment.getAmendmentStatus() != AmendmentStatus.SUBMITTED){
            return createValidationResponse(new ValidationException("Can only reject a bill amendment in 'Submitted' status")).build();
        }
        billItemAmendment.setAmendmentStatus(AmendmentStatus.DECLINED);
        entityUpdatedEventSrc.fire(new EntityUpdatedEvent<>(billItemAmendment));
        return Response.ok().build();
    }
}
