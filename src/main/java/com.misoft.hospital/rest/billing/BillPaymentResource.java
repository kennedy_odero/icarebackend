package com.misoft.hospital.rest.billing;

import com.misoft.hospital.model.billing.BillPayment;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 9/13/15.
 */
@Stateless
@Path("/billPayments")
public class BillPaymentResource extends BaseEntityService<BillPayment>{
    public BillPaymentResource(){
        super(BillPayment.class);
    }
}
