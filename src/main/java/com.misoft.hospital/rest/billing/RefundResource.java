package com.misoft.hospital.rest.billing;

import com.misoft.hospital.model.billing.Refund;
import com.misoft.hospital.model.billing.RefundStatus;
import com.misoft.hospital.model.billing.Waiver;
import com.misoft.hospital.model.billing.WaiverStatus;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.validation.ValidationException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import java.util.logging.Level;

/**
 * Created by kodero on 12/17/15.
 */
@Stateless
@Path("/refunds")
public class RefundResource extends BaseEntityService<Refund>{
    public RefundResource() {
        super(Refund.class);
    }

    @POST
    @Path("/{id:[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}}/submit")
    public Response submit(@PathParam("id") String id){
        Refund refund = gService.find(id, entityClass);
        if(refund == null) {
            log.log(Level.SEVERE, "No refund with id {" + id + "} given!");
            return Response.status(404).build();
        }
        if(refund.getRefundStatus() != RefundStatus.NEW){
            return createValidationResponse(new ValidationException("Can only submit a waiver in 'New' status")).build();
        }
        refund.setRefundStatus(RefundStatus.SUBMITTED);
        entityUpdatedEventSrc.fire(new EntityUpdatedEvent<>(refund));
        return Response.ok().build();
    }

    @POST
    @Path("/{id:[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}}/cancel")
    public Response cancel(@PathParam("id") String id){
        Refund refund = gService.find(id, entityClass);
        if(refund == null) {
            log.log(Level.SEVERE, "No refund with id {" + id + "} given!");
            return Response.status(404).build();
        }
        if(refund.getRefundStatus() != RefundStatus.NEW){
            return createValidationResponse(new ValidationException("Can only cancel a waiver in 'New' status")).build();
        }
        refund.setRefundStatus(RefundStatus.CANCELLED);
        entityUpdatedEventSrc.fire(new EntityUpdatedEvent<>(refund));
        return Response.ok().build();
    }

    @POST
    @Path("/{id:[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}}/approve")
    public Response approve(@PathParam("id") String id){
        Refund refund = gService.find(id, entityClass);
        if(refund == null) {
            log.log(Level.SEVERE, "No refund with id {" + id + "} given!");
            return Response.status(404).build();
        }
        if(refund.getRefundStatus() != RefundStatus.SUBMITTED){
            return createValidationResponse(new ValidationException("Can only approve a waiver in 'Submitted' status")).build();
        }
        refund.setRefundStatus(RefundStatus.APPROVED);
        entityUpdatedEventSrc.fire(new EntityUpdatedEvent<>(refund));
        return Response.ok().build();
    }

    @POST
    @Path("/{id:[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}}/reject")
    public Response reject(@PathParam("id") String id){
        Refund refund = gService.find(id, entityClass);
        if(refund == null) {
            log.log(Level.SEVERE, "No refund with id {" + id + "} given!");
            return Response.status(404).build();
        }
        if(refund.getRefundStatus() != RefundStatus.SUBMITTED){
            return createValidationResponse(new ValidationException("Can only reject a waiver in 'Submitted' status")).build();
        }
        refund.setRefundStatus(RefundStatus.REJECTED);
        entityUpdatedEventSrc.fire(new EntityUpdatedEvent<>(refund));
        return Response.ok().build();
    }
}
