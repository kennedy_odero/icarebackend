package com.misoft.hospital.rest.billing;

import com.misoft.hospital.data.gdto.GenericDTO;
import com.misoft.hospital.model.billing.PatientPayment;
import com.misoft.hospital.rest.BaseEntityService;
import com.misoft.hospital.util.types.DateParam;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by kodero on 9/13/15.
 */
@Stateless
@Path("/patientPayments")
public class PatientPaymentResource extends BaseEntityService<PatientPayment>{
    @Inject
    EntityManager em;

    DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

    public PatientPaymentResource(){
        super(PatientPayment.class);
    }

    @GET
    @Path("/collectionsByDate")
    public Response getCollectionsByDate(@QueryParam("dateFrom")@DefaultValue("today")DateParam dateFrom, @QueryParam("dateTo")@DefaultValue("today") DateParam dateTo){
        String sql = "select \n" +
                "\tp.date_paid,\n" +
                "    sum(p.amount_paid)\n" +
                "from patient_payments p\n" +
                "where p.deleted <> 1 and p.date_paid between '" + df.format(dateFrom.getDate()) + "' and '" + df.format(dateTo.getDate()) + "' \n" +
                "group by p.date_paid";
        List<Object> results = em.createNativeQuery(sql).getResultList();
        List<GenericDTO> balances = new ArrayList<>();
        for(Object o : results){
            Object[] row = (Object[]) o;
            GenericDTO genericDTO = new GenericDTO("com.misoft.reports.CollectionsByDate");
            genericDTO.addDate("date", (Date) row[0]);
            genericDTO.addBigDecimal("total", (BigDecimal) row[1]);
            balances.add(genericDTO);
        }
        return Response.ok(balances).build();
    }

    @GET
    @Path("/collectionsByDept")
    public Response getCollectionsByDept(@QueryParam("dateFrom")@DefaultValue("today")DateParam dateFrom, @QueryParam("dateTo")@DefaultValue("today") DateParam dateTo){
        String sql = "select \n" +
                "\tu.name, \n" +
                "    coalesce((select sum(bip.amount_settled) \n" +
                "    from bill_item_payments bip \n" +
                "\t\tinner join bill_items bi on bip.bill_item = bi.id \n" +
                "\t\tinner join products p on bi.product = p.id \n" +
                "\twhere bip.deleted <> 1 and p.unit = u.id\n" +
                "\t\tand date(bip.created_at) between '" + df.format(dateFrom.getDate()) + "' and '" + df.format(dateTo.getDate()) + "'), 0) collected\n" +
                "from units u \n" +
                "where u.deleted <> 1\n" +
                "group by u.name\n" +
                "order by u.name asc";
        //and date(bip.created_at) between '" + df.format(dateFrom.getDate()) + "' and '" + df.format(dateTo.getDate()) + "'
        List<Object> results = em.createNativeQuery(sql).getResultList();
        List<GenericDTO> balances = new ArrayList<>();
        for(Object o : results){
            Object[] row = (Object[]) o;
            GenericDTO genericDTO = new GenericDTO("com.misoft.reports.CollectionsByDate");
            genericDTO.addString("dept", (String) row[0]);
            genericDTO.addBigDecimal("total", (BigDecimal) row[1]);
            balances.add(genericDTO);
        }
        return Response.ok(balances).build();
    }

    @GET
    @Path("/collectionsByShift")
    public Response getCollectionsByShift(@QueryParam("dateFrom")@DefaultValue("today")DateParam dateFrom, @QueryParam("dateTo")@DefaultValue("today") DateParam dateTo){
        String sql = "select\n" +
                "   date(cs.created_at) _date,\n" +
                "   concat(staff.surname, ' ', staff.other_names) cashier," +
                "   cs.shift_no, \n" +
                "   sum(pp.amount_paid - coalesce(pp.amount_refunded, 0)) collected \n" +
                "from patient_payments pp \n" +
                "   inner join cashier_shifts cs on cs.id = pp.shift\n" +
                "   inner join parties staff on cs.owner = staff.id " +
                "where pp.deleted <> 1 and date(cs.created_at) between '" + df.format(dateFrom.getDate()) + "' and '" + df.format(dateTo.getDate()) + "'\n" +
                "group by cs.shift_no\n" +
                "order by cs.created_at";
        List<Object> results = em.createNativeQuery(sql).getResultList();
        List<GenericDTO> balances = new ArrayList<>();
        for(Object o : results){
            Object[] row = (Object[]) o;
            GenericDTO genericDTO = new GenericDTO("com.misoft.reports.CollectionsByDate");
            genericDTO.addDate("date", (Date) row[0]);
            genericDTO.addString("cashier", (String) row[1]);
            genericDTO.addBigInteger("shiftNo", (BigInteger) row[2]);
            genericDTO.addBigDecimal("total", (BigDecimal) row[3]);
            balances.add(genericDTO);
        }
        return Response.ok(balances).build();
    }

    @GET
    @Path("/refunds")
    public Response getRefunds(@QueryParam("dateFrom") @DefaultValue("today") DateParam dateFrom, @QueryParam("dateTo") @DefaultValue("today")DateParam dateTo){
        String sql = "select  patient.patient_no,\n" +
                "\tconcat(upper(patient.surname), ' ', upper(patient.other_names)) as patient, \n" +
                "\tproduct.name as product,\n" +
                "\t(case bill_item_type \n" +
                "\t\twhen 'Normal' then (quantity * unit_price) \n" +
                "\t\twhen 'Accumulation' then DATEDIFF(coalesce(date_to, now()), date_from) * unit_price end) as total,\n" +
                "\tbi.updated_at as refunded_at,\n" +
                "\tconcat(staff.surname, ' ', staff.other_names) as refunded_by\n" +
                "\n" +
                "from bill_items bi inner join patient_bills pb on bi.patient_bill = pb.id \n" +
                "\tinner join parties patient on pb.patient = patient.id\n" +
                "\tinner join products product on bi.product = product.id\n" +
                "\tinner join users u on bi.updated_by_id = u.id inner join parties staff on u.staff = staff.id\n" +
                "\n" +
                "where bi.bill_item_status = 'REFUNDED' " +
                "and date(bi.updated_at) between :dateFrom and :dateTo " +
                "order by date(bi.updated_at) asc";

        List<Object> results = em.createNativeQuery(sql)
                .setParameter("dateFrom", df.format(dateFrom.getDate()))
                .setParameter("dateTo", df.format(dateTo.getDate()))
                .getResultList();
        List<GenericDTO> balances = new ArrayList<>();
        for(Object o : results){
            Object[] row = (Object[]) o;
            GenericDTO genericDTO = new GenericDTO("com.misoft.reports.RefundReport");
            genericDTO.addString("patientNo", (String) row[0]);
            genericDTO.addString("patientName", (String) row[1]);
            genericDTO.addString("product", (String) row[2]);
            genericDTO.addBigDecimal("total", (BigDecimal) row[3]);
            genericDTO.addDate("refundedAt", (Date) row[4]);
            genericDTO.addString("refundedBy", (String) row[5]);
            balances.add(genericDTO);
        }
        return Response.ok(balances).build();
    }

    @GET
    @Path("/waivers")
    public Response getWaivers(@QueryParam("dateFrom") @DefaultValue("today") DateParam dateFrom, @QueryParam("dateTo") @DefaultValue("today")DateParam dateTo){
        String sql = "select  patient.patient_no,\n" +
                "\tconcat(upper(patient.surname), ' ', upper(patient.other_names)) as patient, \n" +
                "\tproduct.name as product,\n" +
                "\tbiw.amount_waived,\n" +
                "\tw.created_at as waived_at,\n" +
                "\tconcat(staff.surname, ' ', staff.other_names) as waived_by\n" +
                "\t\n" +
                "from bill_item_waivers biw\n" +
                "\tinner join bill_items bi on biw.bill_item = bi.id\n" +
                "\tinner join products product on bi.product = product.id\n" +
                "\tinner join waivers w on biw.waiver = w.id\n" +
                "\tinner join parties patient on w.patient = patient.id\n" +
                "\tinner join users u on w.created_by_id = u.id inner join parties staff on u.staff = staff.id\n" +
                "\n" +
                "\twhere w.waiver_status = 'APPROVED' " +
                "and date(w.created_at) between :dateFrom and :dateTo";

        List<Object> results = em.createNativeQuery(sql)
                .setParameter("dateFrom", df.format(dateFrom.getDate()))
                .setParameter("dateTo", df.format(dateTo.getDate()))
                .getResultList();
        List<GenericDTO> balances = new ArrayList<>();
        for(Object o : results){
            Object[] row = (Object[]) o;
            GenericDTO genericDTO = new GenericDTO("com.misoft.reports.RefundReport");
            genericDTO.addString("patientNo", (String) row[0]);
            genericDTO.addString("patientName", (String) row[1]);
            genericDTO.addString("product", (String) row[2]);
            genericDTO.addBigDecimal("total", (BigDecimal) row[3]);
            genericDTO.addDate("waivedAt", (Date) row[4]);
            genericDTO.addString("waivedBy", (String) row[5]);
            balances.add(genericDTO);
        }
        return Response.ok(balances).build();
    }
}
