package com.misoft.hospital.rest.approval;

import com.misoft.hospital.model.approval.ApprovalTemplate;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 12/16/15.
 */
@Stateless
@Path("/approvalTemplates")
public class ApprovalTemplateResource extends BaseEntityService<ApprovalTemplate>{
    public ApprovalTemplateResource(){
        super(ApprovalTemplate.class);
    }
}
