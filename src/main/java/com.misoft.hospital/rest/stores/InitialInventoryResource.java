package com.misoft.hospital.rest.stores;

import com.misoft.hospital.model.stores.InitialInventory;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 8/5/16.
 */
@Stateless
@Path("initialInventories")
public class InitialInventoryResource extends BaseEntityService<InitialInventory>{
    public InitialInventoryResource(){
        super(InitialInventory.class);
    }
}
