package com.misoft.hospital.rest.stores;

import com.misoft.hospital.model.stores.Packaging;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 8/5/16.
 */
@Stateless
@Path("/packaging")
public class PackagingResource extends BaseEntityService<Packaging>{

    public PackagingResource(){
        super(Packaging.class);
    }
}
