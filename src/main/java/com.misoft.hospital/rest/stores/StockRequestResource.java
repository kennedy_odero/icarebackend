package com.misoft.hospital.rest.stores;

import com.misoft.hospital.model.admin.User;
import com.misoft.hospital.model.stores.MovementStatusHistory;
import com.misoft.hospital.model.stores.StockRequest;
import com.misoft.hospital.rest.BaseEntityService;
import com.misoft.hospital.service.admin.UserService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Path;

import java.util.Date;

/**
 * Created by kodero on 4/15/15.
 */
@Stateless
@Path("/stockRequests")
public class StockRequestResource extends BaseEntityService<StockRequest> {

    @Inject
    UserService userService;

    public StockRequestResource(){
        super(StockRequest.class);
    }
    
    @Override
    public void beforeSaveOrUpdate(StockRequest request){
        //update status history
        User currentUser = userService.currentUser();
        if(request.getMovementStatusHistories().size() > 0){
            //get the last status and see if its different from the current one
            if(request.getMovementStatusHistories().get(request.getMovementStatusHistories().size() - 1).getStatus() != request.getStatus()){
                request.getMovementStatusHistories().add(new MovementStatusHistory(request.getStatus(), request, new Date(), currentUser));
            }
        }
    }
}