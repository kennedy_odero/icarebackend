package com.misoft.hospital.rest.stores;

import com.misoft.hospital.model.stores.PurchaseIssue;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 8/6/16.
 */
@Stateless
@Path("/purchaseIssues")
public class PurchaseIssueResource extends BaseEntityService<PurchaseIssue>{
    public PurchaseIssueResource(){
        super(PurchaseIssue.class);
    }
}
