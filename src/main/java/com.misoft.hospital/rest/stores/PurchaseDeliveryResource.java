package com.misoft.hospital.rest.stores;

import com.misoft.hospital.model.healthcare.ServiceCenter;
import com.misoft.hospital.model.setup.Product;
import com.misoft.hospital.model.stores.InventoryUpdateEvent;
import com.misoft.hospital.model.stores.OrderLine;
import com.misoft.hospital.model.stores.PurchaseDelivery;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.ws.rs.Path;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by kodero on 4/10/15.
 */
@Stateless
@Path("/purchaseDeliveries")
public class PurchaseDeliveryResource extends BaseEntityService<PurchaseDelivery> {
    @Inject
    private Event<InventoryUpdateEvent> inventoryUpdateEventEvent;

    public PurchaseDeliveryResource(){
        super(PurchaseDelivery.class);
    }

    @Override
    public void beforeSaveOrUpdate(PurchaseDelivery purchaseDelivery){
        //construct event and fire
        InventoryUpdateEvent event = new InventoryUpdateEvent();
        event.setUpdateDate(purchaseDelivery.getDeliveredOn());
        Collection<ServiceCenter> serviceCenters = new ArrayList<>();
        serviceCenters.add(purchaseDelivery.getServiceCenter());
        Collection<Product> products = new ArrayList<>();
        for(OrderLine ol : purchaseDelivery.getOrderLines()){
            products.add(ol.getProduct());
        }
        event.setProducts(products);
        event.setServiceCenters(serviceCenters);
        inventoryUpdateEventEvent.fire(event);
    }
}
