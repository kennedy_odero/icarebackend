package com.misoft.hospital.rest.stores;

import com.misoft.hospital.model.stores.PurchaseOrder;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 4/9/15.
 */
@Stateless
@Path("/purchaseOrders")
public class PurchaseOrderResource extends BaseEntityService<PurchaseOrder> {
    public PurchaseOrderResource(){
        super(PurchaseOrder.class);
    }
}
