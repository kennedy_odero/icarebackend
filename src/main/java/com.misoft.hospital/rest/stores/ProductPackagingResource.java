package com.misoft.hospital.rest.stores;

import com.misoft.hospital.model.setup.ProductPackaging;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 8/6/16.
 */
@Stateless
@Path("/productPackaging")
public class ProductPackagingResource extends BaseEntityService<ProductPackaging>{
    public ProductPackagingResource(){
        super(ProductPackaging.class);
    }
}
