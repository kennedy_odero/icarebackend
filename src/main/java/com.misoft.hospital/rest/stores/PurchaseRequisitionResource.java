package com.misoft.hospital.rest.stores;

import com.misoft.hospital.model.stores.PurchaseRequisition;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 8/17/15.
 */
@Stateless
@Path("/purchaseRequisitions")
public class PurchaseRequisitionResource extends BaseEntityService<PurchaseRequisition>{
    public PurchaseRequisitionResource(){
        super(PurchaseRequisition.class);
    }
}
