package com.misoft.hospital.rest.stores;

import com.misoft.hospital.model.stores.StockAdjustment;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 8/20/16.
 */
@Stateless
@Path("/stockAdjustments")
public class StockAdjustmentResource extends BaseEntityService<StockAdjustment>{
    public StockAdjustmentResource(){
        super(StockAdjustment.class);
    }
}
