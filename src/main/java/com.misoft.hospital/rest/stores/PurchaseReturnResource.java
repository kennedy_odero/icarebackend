package com.misoft.hospital.rest.stores;

import com.misoft.hospital.model.healthcare.ServiceCenter;
import com.misoft.hospital.model.setup.Product;
import com.misoft.hospital.model.stores.InventoryUpdateEvent;
import com.misoft.hospital.model.stores.OrderLine;
import com.misoft.hospital.model.stores.PurchaseReturn;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.ws.rs.Path;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by kodero on 4/10/15.
 */
@Stateless
@Path("/purchaseReturns")
public class PurchaseReturnResource extends BaseEntityService<PurchaseReturn> {
    @Inject
    private Event<InventoryUpdateEvent> inventoryUpdateEventEvent;

    public PurchaseReturnResource(){
        super(PurchaseReturn.class);
    }

    @Override
    public void beforeSaveOrUpdate(PurchaseReturn purchaseReturn){
        //construct event and fire
        InventoryUpdateEvent event = new InventoryUpdateEvent();
        event.setUpdateDate(purchaseReturn.getDateReceived());
        Collection<ServiceCenter> serviceCenters = new ArrayList<>();
        serviceCenters.add(purchaseReturn.getServiceCenter());
        Collection<Product> products = new ArrayList<>();
        for(OrderLine ol : purchaseReturn.getOrderLines()){
            products.add(ol.getProduct());
        }
        event.setProducts(products);
        event.setServiceCenters(serviceCenters);
        inventoryUpdateEventEvent.fire(event);
    }
}
