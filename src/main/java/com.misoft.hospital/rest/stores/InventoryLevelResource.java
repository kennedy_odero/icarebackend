package com.misoft.hospital.rest.stores;

/**
 * Created by kodero on 5/3/15.
 */

import com.misoft.hospital.data.gdto.GenericDTO;
import com.misoft.hospital.model.stores.InventoryLevel;
import com.misoft.hospital.rest.BaseEntityService;
import com.misoft.hospital.util.types.DateParam;
import org.hibernate.SQLQuery;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Stateless
@Path("/inventoryLevels")
public class InventoryLevelResource extends BaseEntityService<InventoryLevel> {
    @Inject
    EntityManager em;

    public InventoryLevelResource(){
        super(InventoryLevel.class);
    }

    @GET
    @Path("/liveInventory")
    public Response getInventoryLevels(@QueryParam("productId") String productId, @QueryParam("productName") @DefaultValue("%") String productName, @QueryParam("dateFrom") @DefaultValue("today")DateParam dateFrom, @QueryParam("dateTo") @DefaultValue("today")DateParam dateTo){
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        //get the levels from sql
        String sql = "select\n" +
                "\tp.id,\n" +
                "\tp.name,\n" +
                "\tcoalesce((select convert(m.blocked, DECIMAL(1,0)) from medicament m where m.product = p.id and m.deleted <> 1), 0) as blocked,\n" +
                "    sum(case when ol.order_type in ('PURCHASE_DELIVERY', 'INITIAL_INVENTORY', 'ADJUSTMENT') then (ol.quantity * ol.multiplier) else 0 end) -\n" +
                "    sum(case ol.order_type when 'STOCK_ISSUE' then (ol.quantity * ol.multiplier) else 0 end) opening,\n" +
                "    sum(case ol2.order_type when 'PURCHASE_DELIVERY' then (ol2.quantity * ol2.multiplier) else 0 end) received,\n" +
                "    sum(case ol2.order_type when 'STOCK_ISSUE' then (ol2.quantity * ol2.multiplier) else 0 end) issued,\n" +
                "    sum(coalesce(bi.quantity, 0)) sold,\n" +
                "    sum(case when ol.order_type in ('PURCHASE_DELIVERY', 'INITIAL_INVENTORY', 'ADJUSTMENT') then (ol.quantity * ol.multiplier) else 0 end) -\n" +
                "    sum(case ol.order_type when 'STOCK_ISSUE' then (ol.quantity * ol.multiplier) else 0 end) +\n" +
                "    sum(case ol2.order_type when 'PURCHASE_DELIVERY' then (ol2.quantity * ol2.multiplier) else 0 end) -\n" +
                "    sum(case ol2.order_type when 'STOCK_ISSUE' then (ol2.quantity * ol2.multiplier) else 0 end) -\n" +
                "    sum(coalesce(bi.quantity, 0)) closing\n" +
                "from products p\n" +
                "\tleft outer join order_lines ol \n" +
                "\t\ton p.id = ol.product and ol.deleted <> 1 and ol.order_date < '" + df.format(dateFrom.getDate()) + "'\n" +
                "\tleft outer join order_lines ol2 \n" +
                "\t\ton p.id = ol2.product and ol2.deleted <> 1 and date(ol2.order_date) between '" + df.format(dateFrom.getDate()) + "' and '" + df.format(dateTo.getDate()) + "'\n" +
                "\tleft outer join bill_items bi\n" +
                "\t\ton p.id = bi.product and bi.deleted <> 1 and date(bi.created_at) between '" + df.format(dateFrom.getDate()) + "' and '" + df.format(dateTo.getDate()) + "'\n" +
                "where p.product_type='GOODS' and p.deleted <> 1 and p.name like '%" + productName + "' " + (productId == null? "" : " and p.id = '" + productId + "' ") + " \n" +
                "group by p.name\n";
        List<Object> results = em.createNativeQuery(sql).getResultList();
        List<GenericDTO> balances = new ArrayList<>();
        for(Object o : results){
            Object[] row = (Object[]) o;
            GenericDTO genericDTO = new GenericDTO("com.misoft.reports.InventoryLevel");
            genericDTO.addString("id", (String) row[0]);
            genericDTO.addString("productName", (String) row[1]);
            genericDTO.addInt("blocked", ((BigDecimal) row[2]).toBigInteger().intValue());
            genericDTO.addDouble("opening", (Double) row[3]);
            genericDTO.addDouble("received", (Double) row[4]);
            genericDTO.addDouble("issued", (Double) row[5]);
            genericDTO.addBigDecimal("sold", (BigDecimal) row[6]);
            genericDTO.addDouble("closing", (Double) row[7]);
            balances.add(genericDTO);
        }
        return Response.ok(balances).build();
    }

    @GET
    @Path("/stockCard")
    public Response getStockCard(@QueryParam("productId") @DefaultValue("-Not-Provided-") String productId, @QueryParam("dateFrom") @DefaultValue("today")DateParam dateFrom, @QueryParam("dateTo") @DefaultValue("today")DateParam dateTo){
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String sql = "select\n" +
                "\t'2016-06-30' as txn_date, \n" +
                "    '-' as rec_iss,\n" +
                "\t'Opening Balance' as txn_type,\n" +
                "    coalesce((sum(case when ol.order_type in ('PURCHASE_DELIVERY', 'INITIAL_INVENTORY') then (ol.quantity * ol.multiplier) else 0 end) - \n" +
                "    sum(case ol.order_type when 'STOCK_ISSUE' then ol.quantity else 0 end) + \n" +
                "    sum(coalesce(bi.quantity, 0)) ), 0) as quantity\n" +
                "from\n" +
                "    products p  \n" +
                "left outer join\n" +
                "    order_lines ol    \n" +
                "        on p.id = ol.product \n" +
                "        and ol.deleted <> 1 \n" +
                "        and ol.order_date < '" + df.format(dateFrom.getDate()) + "'  \n" +
                "left outer join\n" +
                "    bill_items bi   \n" +
                "        on p.id = bi.product \n" +
                "        and bi.deleted <> 1 \n" +
                "        and date(bi.created_at) < '" + df.format(dateFrom.getDate()) + "'\n" +
                "where p.id='" + productId + "'\n" +
                "\n" +
                "union all\n" +
                "\n" +
                "select \n" +
                "\tdate(ol.order_date),\n" +
                "    coalesce(p.name, svc.name),\n" +
                "    ol.order_type,\n" +
                "    (case when ol.order_type in ('PURCHASE_DELIVERY', 'INITIAL_INVENTORY') then (ol.quantity * ol.multiplier) else (ol.quantity * ol.multiplier * -1 ) end) as quantity\n" +
                "from order_lines ol\n" +
                "\tinner join orders o\n" +
                "\t\ton ol.purchase_order = o.id\n" +
                "\tleft outer join parties p\n" +
                "\t\ton o.supplier = p.id\n" +
                "\tleft outer join service_room svc\n" +
                "\t\ton o.service_center = svc.id\n" +
                "where ol.product = '" + productId + "'\n" +
                "\tand ol.order_date between '" + df.format(dateFrom.getDate()) + "' and '" + df.format(dateTo.getDate()) + "'\n" +
                "\n" +
                "union all \n" +
                "\n" +
                "select \n" +
                "\tdate(bi.created_at),\n" +
                "    'OPD',\n" +
                "    'SALE',\n" +
                "    (bi.quantity * -1) as quantity\n" +
                "from bill_items bi \n" +
                "\tinner join products p\n" +
                "\t\ton bi.product = p.id\n" +
                "where bi.created_at between '" + df.format(dateFrom.getDate()) + "' and '" + df.format(dateTo.getDate()) + "'\n" +
                "\tand p.id = '" + productId + "'\n" +
                "order by 1 asc\n" +
                "    \n" +
                "    \n" +
                "\t";
        List<Object> results = em.createNativeQuery(sql).getResultList();
        List<GenericDTO> balances = new ArrayList<>();
        BigDecimal currentBalance = results.size() > 0? new BigDecimal((Double)((Object[])results.get(0))[3]) : BigDecimal.ZERO;
        boolean firstRow = true;
        for(Object o : results){
            Object[] row = (Object[]) o;
            GenericDTO genericDTO = new GenericDTO("com.misoft.reports.StockCard");
            genericDTO.addString("txnDate", (String) row[0]);
            genericDTO.addString("receivedOrIssued", (String) row[1]);
            genericDTO.addString("txnType", (String) row[2]);
            genericDTO.addDouble("quantity", (Double) row[3]);
            if(firstRow) genericDTO.addBigDecimal("balance", currentBalance);
            else {
                currentBalance = currentBalance.add(new BigDecimal((Double) row[3]));
                genericDTO.addBigDecimal("balance", currentBalance);
            }
            balances.add(genericDTO);
            firstRow = false;
        }
        return Response.ok(balances).build();
    }
}
