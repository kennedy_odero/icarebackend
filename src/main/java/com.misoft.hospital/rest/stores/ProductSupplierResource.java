package com.misoft.hospital.rest.stores;

import com.misoft.hospital.model.stores.ProductSupplier;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 8/14/15.
 */
@Stateless
@Path("/productSuppliers")
public class ProductSupplierResource extends BaseEntityService<ProductSupplier>{
    public ProductSupplierResource(){
        super(ProductSupplier.class);
    }
}
