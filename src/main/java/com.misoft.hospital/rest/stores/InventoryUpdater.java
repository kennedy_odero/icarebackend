package com.misoft.hospital.rest.stores;

import com.misoft.hospital.model.accounts.AccountingPeriod;
import com.misoft.hospital.model.accounts.PeriodType;
import com.misoft.hospital.model.healthcare.ServiceCenter;
import com.misoft.hospital.model.setup.Product;
import com.misoft.hospital.model.stores.InventoryLevel;
import com.misoft.hospital.model.stores.InventoryUpdateEvent;
import com.misoft.hospital.service.accounts.AccountingPeriodService;

import javax.ejb.Asynchronous;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.enterprise.event.Observes;
import javax.enterprise.event.TransactionPhase;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by kodero on 5/3/15.
 */
@Singleton
public class InventoryUpdater {

    @Inject
    private EntityManager em;

    @Inject
    private AccountingPeriodService apService;

    @Inject
    protected Logger log;

    @Asynchronous
    @Lock(LockType.READ)
    public void updateInventoryLevels(@Observes(during = TransactionPhase.AFTER_COMPLETION) InventoryUpdateEvent event) throws InterruptedException {
        log.info("-------------------------------Updating inventory levels------------------------------------");
        Collection<InventoryLevel> levels = new ArrayList<>();
        DateFormat df = new SimpleDateFormat("YYYY-MM-dd");
        //collect product ids
        List pIds = new ArrayList();
        for(Product p : event.getProducts()){
            pIds.add(p.getId());
        }
        //get the ap associated with this
        AccountingPeriod ap = apService.getPeriod(event.getUpdateDate(), PeriodType.FISCAL_MONTH);
        if(ap == null){
            throw new IllegalStateException("----No accounting period found for " + event.getUpdateDate());
        }

        for (ServiceCenter svc : event.getServiceCenters()){

            //softRemove what is existing
            String deleteSql = "delete from inventory_levels where service_center =:svcCenter and accounting_period =:ap and product in (:pIds)";
            Query deleteQuery = em.createNativeQuery(deleteSql);
            deleteQuery.setParameter("svcCenter", svc.getId());
            deleteQuery.setParameter("ap", ap.getId());
            deleteQuery.setParameter("pIds", pIds);
            log.info("deleted : -------" + deleteQuery.executeUpdate());

            String sql = "select " +
                    "p.id, \n" +
                    "p.name, \n" +
                    "COALESCE((select sum(ol.quantity) from order_lines ol inner join orders o on ol.purchase_order = o.id where o.delivered_on between '" + df.format(ap.getFromDate()) + "' and '" + df.format(event.getUpdateDate()) + "' and o.order_type='PURCHASE_DELIVERY' and ol.product = p.id and \t\to.service_center='" + svc.getId() + "'),0.0) delivered,\n" +
                    "COALESCE((select sum(ol.quantity) from order_lines ol inner join orders o on ol.purchase_order = o.id where o.date_received between '" + df.format(ap.getFromDate()) + "' and '" + df.format(event.getUpdateDate()) + "' and o.order_type='PURCHASE_RETURN' and ol.product = p.id and \t\to.service_center='" + svc.getId() + "'),0.0) returned,\n" +
                    "COALESCE((select sum(ml.qty_issued) from mvt_line_items ml inner join stock_movements sm on ml.stock_mvt = sm.id where sm.date_issued between '" + df.format(ap.getFromDate()) + "' and '" + df.format(event.getUpdateDate()) + "' and sm.mvt_type='ISSUE' and ml.product = p.id and \t\tsm.svc_center_to='" + svc.getId() + "'),0.0) moved_in,\n" +
                    "COALESCE((select sum(ml.qty_issued) from mvt_line_items ml inner join stock_movements sm on ml.stock_mvt = sm.id where sm.date_issued between '" + df.format(ap.getFromDate()) + "' and '" + df.format(event.getUpdateDate()) + "' and sm.mvt_type='ISSUE' and ml.product = p.id and \t\tsm.svc_center_from='" + svc.getId() + "'),0.0) moved_out,\n" +
                    "0.0 dispensed " +
                    "from products p \n" +
                    "where p.id in (:pIds)";
            Query levelQuery = em.createNativeQuery(sql);
            levelQuery.setParameter("pIds", pIds);
            List<Object[]> res = levelQuery.getResultList();
            for (Object[] obj : res){
                InventoryLevel level = new InventoryLevel();
                level.setProduct(em.find(Product.class, obj[0]));
                level.setDelivered(new BigDecimal((Double)obj[2]));
                level.setReturned(new BigDecimal((Double)obj[3]));
                level.setMovedIn((BigDecimal)obj[4]);
                level.setMovedOut((BigDecimal)obj[5]);
                level.setDispensed((BigDecimal)obj[6]);
                level.setCalcQuantity(level.getDelivered().subtract(level.getReturned()).add(level.getMovedIn()).subtract(level.getMovedOut()).subtract(level.getDispensed()));
                level.setPhysicalQuantity(level.getCalcQuantity());
                level.setVariance(BigDecimal.ZERO);
                level.setServiceCenter(svc);
                level.setAccountingPeriod(ap);
                levels.add(level);
            }
        }
        for(InventoryLevel l : levels){
            try{
                em.persist(l);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }
}
