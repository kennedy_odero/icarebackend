package com.misoft.hospital.rest.stores;

import com.misoft.hospital.data.gdto.GenericDTO;
import com.misoft.hospital.model.healthcare.ServiceCenter;
import com.misoft.hospital.model.setup.Product;
import com.misoft.hospital.model.stores.InventoryUpdateEvent;
import com.misoft.hospital.model.stores.MovementLineItem;
import com.misoft.hospital.model.stores.StockIssue;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by kodero on 5/6/15.
 */
@Stateless
@Path("/stockIssues")
public class StockIssueResource extends BaseEntityService<StockIssue>{
    @Inject
    private Event<InventoryUpdateEvent> inventoryUpdateEventEvent;

    public StockIssueResource(){
        super(StockIssue.class);
    }

    @Override
    public void beforeSaveOrUpdate(StockIssue stockIssue){
        //construct event and fire
        InventoryUpdateEvent event = new InventoryUpdateEvent();
        event.setUpdateDate(stockIssue.getDateOfMovement());
        Collection<ServiceCenter> serviceCenters = new ArrayList<>();
        serviceCenters.add(stockIssue.getFrom());
        serviceCenters.add(stockIssue.getTo());
        Collection<Product> products = new ArrayList<>();
        for(MovementLineItem ol : stockIssue.getMovementLineItems()){
            products.add(ol.getProduct());
        }
        event.setProducts(products);
        event.setServiceCenters(serviceCenters);
        inventoryUpdateEventEvent.fire(event);
    }

    @GET
    @Path("/issuedQuantities")
    public Response getRequestQuantities(@QueryParam("requestNumbers") String requestNumbers){
        //get the sum of quantities ordered for the requests specified
        List<GenericDTO> returns = new ArrayList<>();
        if(requestNumbers == null) return Response.ok(returns).build();
        String sql = "select mli.product, " +
                "   sum(mli.qty) " +
                "from mvt_line_items mli inner join stock_movements sm on mli.stock_mvt = sm.id " +
                "where sm.stock_request =:requestNumber " +
                "group by mli.product";
        List<Object[]> results = getEntityManager().createNativeQuery(sql).setParameter("requestNumber", requestNumbers).getResultList();
        for(Object[] o : results){
            GenericDTO g = new GenericDTO("com.misoft.hospital.model.stores.MovementLineItem");
            g.addRelation2("product", new GenericDTO("com.misoft.hospital.model.setup.Product").addString("id", (String)o[0]));
            g.addBigDecimal("issued", (BigDecimal)o[1]);
            returns.add(g);
        }
        return Response.ok(returns).build();
    }
}
