package com.misoft.hospital.rest.admin;

import com.misoft.hospital.model.admin.Permission;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ws.rs.Path;

/**
 * @author mokua
 */
@Path("/permissions")
@javax.ejb.Stateless
public class PermissionResource extends BaseEntityService<Permission> {
    public PermissionResource() {
        super(Permission.class);
    }

    /*@Override
    protected Predicate[] extractPredicates(
            MultivaluedMap<String, String> queryParameters,
            CriteriaBuilder criteriaBuilder,
            Root<Permission> root, MultivaluedMap<String, String> pathParameters) {
        List<Predicate> predicates = new ArrayList<>();

        if (queryParameters.containsKey("modelType")) {
            String taskableType = queryParameters.getFirst("modelType");
            predicates.add(criteriaBuilder.equal(root.get("modelType"), taskableType));
        }


        return predicates.toArray(new Predicate[predicates.size()]);
    }*/
}
