

package com.misoft.hospital.rest.admin;

import com.misoft.hospital.model.admin.Role;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ws.rs.Path;

/**
 * @author mokua
 */
@Path("/roles")
@javax.ejb.Stateless
public class RoleResource extends BaseEntityService<Role> {

    public RoleResource() {
        super(Role.class);
    }
}
