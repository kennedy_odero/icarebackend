package com.misoft.hospital.rest.admin;

import com.misoft.hospital.model.admin.Department;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 7/24/15.
 */
@Stateless
@Path("/departments")
public class DepartmentResource extends BaseEntityService<Department>{
    public DepartmentResource(){
        super(Department.class);
    }
}
