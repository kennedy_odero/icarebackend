package com.misoft.hospital.rest.admin;

import com.misoft.hospital.data.gdto.GenericDTO;
import com.misoft.hospital.model.admin.Quote;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by kodero on 12/30/14.
 */
@Stateless
@Path("/quotes")
public class QuoteResource{

    @Inject
    private EntityManager entityManager;

    @GET
    @Path("/random")
    @Produces("application/json")
    public Response getRandomQuote(){
        Quote q = (Quote) entityManager.createNativeQuery("select * from quotes ORDER BY RAND() LIMIT 1", Quote.class).getResultList().get(0);
        GenericDTO g = new GenericDTO(q.getClass().getCanonicalName());
        g.addString("id", q.getId());
        g.addString("quote", q.getQuote());
        g.addString("author", q.getAuthor());
        g.addString("genre", q.getGenre());
        return Response.ok(g).build();
    }

    @POST
    @Path("/loadQuotes")
    public Response loadQuotes(){
        String csvFile = "/home/kodero/Downloads/quotes_all.csv";
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ";";

        try {

            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {

                // use ; as separator
                String[] q = line.split(cvsSplitBy);
                System.out.println("q [quote= " + q[0] + " , author=" + q[1] + ", genre=" + q[2] + "]");
                Quote quote = new Quote(q[0], q[1], q[2]);
                entityManager.merge(quote);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        System.out.println("Done");
        return Response.ok().build();
    }
}
