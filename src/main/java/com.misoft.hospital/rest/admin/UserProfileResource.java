package com.misoft.hospital.rest.admin;

import com.misoft.hospital.model.admin.UserProfile;
import com.misoft.hospital.rest.BaseEntityService;

import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Path;

/**
 * Created by kodero on 4/5/14.
 */
@Path("/profiles")
@RequestScoped
public class UserProfileResource extends BaseEntityService<UserProfile> {

    public UserProfileResource() {
        super(UserProfile.class);
    }
}
