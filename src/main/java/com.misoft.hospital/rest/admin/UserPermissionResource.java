package com.misoft.hospital.rest.admin;

import com.misoft.hospital.data.admin.PermissionRepository;
import com.misoft.hospital.data.gdto.GenericDTO;
import com.misoft.hospital.model.admin.Permission;
import com.misoft.hospital.model.admin.UserPermission;
import com.misoft.hospital.rest.BaseEntityService;
import com.misoft.hospital.util.StackTraceUtil;

import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.MappedSuperclass;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.ValidationException;
import javax.validation.Validator;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.*;

/**
 * @author mokua
 */
@Path("/user_permissions")
@javax.ejb.Stateless
public class UserPermissionResource extends BaseEntityService<UserPermission> {

    private static final String COM_LEMR_MODEL_GRANT = "com.misoft.hospital.model";

    public UserPermissionResource() {
        super(UserPermission.class);
    }

    @Inject
    private java.util.logging.Logger log;

    @Inject
    private Validator validator;

    @Inject
    private EntityManager em;

    @Inject
    private PermissionRepository repository;

    @Context
    protected UriInfo uriInfo;

    @Inject
    private Event<UserPermission> entityEventSrc;

    @GET
    @Path("/{user_name}/{names_space}/{model_type}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response listAllForUser(@PathParam("user_name") final String userId,
                                   @PathParam("names_space") final String namespace,
                                   @PathParam("model_type") final String modelType) throws IOException {
        List<UserPermissionRequest> resources = repository.findAllForUserAndNamespaceAndModelType(userId, namespace, modelType);
        return Response.ok(toDTO(resources), MediaType.APPLICATION_JSON_TYPE).build();
    }

    @GET
    @Path("/{user_name}/{names_space}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response listAllForUser(@PathParam("user_name") final String userName,
                                   @PathParam("names_space") final String namespace) throws IOException {
        List<UserPermissionRequest> resources = repository.findAllForUserAndNamespace(userName, namespace);
        return Response.ok(toDTO(resources), MediaType.APPLICATION_JSON_TYPE).build();
    }

    @GET
    @Path("/{user_name}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response listAllForUser(@PathParam("user_name") final String userId) throws IOException {
        List<UserPermissionRequest> resources = repository.findAllForUser(userId);
        return Response.ok(toDTO(resources), MediaType.APPLICATION_JSON_TYPE).build();
    }

    @Override
    protected Predicate[] extractPredicates(MultivaluedMap<String, String> queryParameters, CriteriaBuilder criteriaBuilder, Root<UserPermission> root, MultivaluedMap<String, String> pathParameters) {
        List<Predicate> predicates = new ArrayList<>();
        if (queryParameters.containsKey("permission.namespace")) {
            String namespace = queryParameters.getFirst("permission.namespace");
            predicates.add(criteriaBuilder.equal(root.get("permission.namespace"), namespace));
        }
        return predicates.toArray(new Predicate[predicates.size()]);
    }

    @POST
    @Path("/{id:[0-9][0-9]*}/status")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateUserPermissionStatus(@PathParam("id") long userPermissionId, Boolean status) {
        Response.ResponseBuilder builder = null;

        try {
            //get the loi
            UserPermission userPermission = repository.findUserPermissionById(userPermissionId);
            if (userPermission == null) {
                Map<String, String> responseObj = new HashMap<>();
                responseObj.put("error", "User Permission with id ' " + userPermissionId + "' not found in database");
                builder = Response.status(Response.Status.NOT_FOUND).entity(responseObj);
                return builder.build();
            }
            int updatedStatus = repository.updateUserPermissionStatus(userPermission, status);
            // Create an "ok" response
            builder = Response.ok(updatedStatus);
        } catch (Exception e) {
            e.printStackTrace();
            // Handle generic exceptions
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("error", StackTraceUtil.getStackTrace(e));
            builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
        }

        return builder.build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createResource(Collection<UserPermissionRequest> userPermissionRequests) {

        Response.ResponseBuilder builder = null;

        try {
            validateResource(userPermissionRequests);
            List<UserPermissionRequest> created = repository.register(userPermissionRequests);
            builder = Response.ok(toDTO(created), MediaType.APPLICATION_JSON_TYPE);

        } catch (ConstraintViolationException ce) {
            // Handle bean validation issues
            ce.printStackTrace();
            builder = createViolationResponse(ce.getConstraintViolations());
        } catch (ValidationException e) {
            // Handle the unique constrain violation
            e.printStackTrace();
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put(e.getMessage(), StackTraceUtil.getStackTrace(e));
            builder = Response.status(Response.Status.CONFLICT).entity(responseObj);
        } catch (Exception e) {
            e.printStackTrace();
            // Handle generic exceptions
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("error", e.getMessage());
            builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
        }

        return builder.build();
    }

    private List<GenericDTO> toDTO(List<UserPermissionRequest> list) {
        List<GenericDTO> genericDTOList = new ArrayList<>(list.size());
        for (UserPermissionRequest request : list) {
            genericDTOList.add(request.genericDTO());
        }
        return genericDTOList;
    }

    private void validateResource(Collection<UserPermissionRequest> requests) throws ConstraintViolationException, ValidationException {
        // Create a bean validator and check for issues.
        for (UserPermissionRequest request : requests) {
            Set<ConstraintViolation<UserPermissionRequest>> violations = validator.validate(request);
            if (!violations.isEmpty()) {
                throw new ConstraintViolationException(new HashSet<ConstraintViolation<?>>(violations));
            }
        }
    }

    protected Response.ResponseBuilder createViolationResponse(Set<ConstraintViolation<?>> violations) {
        log.fine("Validation completed. violations found: " + violations.size());

        Map<String, String> responseObj = new HashMap<>();

        for (ConstraintViolation<?> violation : violations) {
            responseObj.put(violation.getPropertyPath().toString(), violation.getMessage());
        }
        return Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
    }

    @POST
    @Path("/createPermissions")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Permission> createPermissions() throws IOException {
        org.reflections.Reflections reflections = new org.reflections.Reflections(COM_LEMR_MODEL_GRANT);
        Set<Class<?>> entityClasses = reflections.getTypesAnnotatedWith(Entity.class);
        List<Permission> permissions = new ArrayList<>();
        for (Class<?> s : entityClasses) {
            Set<Annotation> classAnnotations = new HashSet<>(Arrays.asList(s.getAnnotations()));
            if(!classAnnotations.contains(MappedSuperclass.class)){
                permissions.add(new Permission(s.getName() + ".save", "Save/edit " + s.getSimpleName()));
                permissions.add(new Permission(s.getName() + ".view", "View " + s.getSimpleName()));
                permissions.add(new Permission(s.getName() + ".delete", "Delete " + s.getSimpleName()));
                permissions.add(new Permission(s.getName() + ".list", "List " + s.getSimpleName()));
            }
        }
        //now save the shit
        for(Permission p : permissions){
            try {
                gService.makePersistent(p);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return permissions;
    }
}
