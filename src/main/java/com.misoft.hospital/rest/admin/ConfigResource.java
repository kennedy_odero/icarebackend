package com.misoft.hospital.rest.admin;

import com.misoft.hospital.data.gdto.GenericDTO;
import com.misoft.hospital.model.admin.Config;
import com.misoft.hospital.rest.BaseEntityService;
import com.misoft.hospital.service.admin.ConfigService;

import javax.ejb.Stateful;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

/**
 * Created by kodero on 12/30/14.
 */
@Stateful
@Path("/config")
public class ConfigResource extends BaseEntityService<Config> {
    @Inject
    private EntityManager entityManager;

    @Inject
    private ConfigService configService;

    public ConfigResource(){
        super(Config.class);
    }

    @GET
    @Path("/getCurrentConfig")
    public Response getConfig(@QueryParam("fields") String fieldList) throws Exception {
        String[] fields = fieldList == null ? makeDefaultSelectionFields(entityClass) : fieldList.split(",");
        Config c = configService.getCurrentConfig();
        if(c != null) return Response.ok(toDTO(c, fields, Config.class)).build();
        return Response.ok(new GenericDTO(entityClass.getCanonicalName())).build();
    }
}
