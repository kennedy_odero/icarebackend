package com.misoft.hospital.rest.cssd;

import com.misoft.hospital.model.cssd.EquipmentServicing;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 3/30/15.
 */
@Stateless
@Path("/equipmentServicing")
public class EquipmentServicingResource extends BaseEntityService<EquipmentServicing> {
    public EquipmentServicingResource(){
        super(EquipmentServicing.class);
    }
}
