package com.misoft.hospital.rest.cssd;

import com.misoft.hospital.model.cssd.Period;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 3/30/15.
 */
@Stateless
@Path("/periods")
public class PeriodResource extends BaseEntityService<Period> {
    public PeriodResource(){
        super(Period.class);
    }
}
