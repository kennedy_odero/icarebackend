package com.misoft.hospital.rest.cssd;

import com.misoft.hospital.model.cssd.EquipmentCategory;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 3/29/15.
 */
@Stateless
@Path("/equipmentCategories")
public class EquipmentCategoryResource extends BaseEntityService<EquipmentCategory>{
    public EquipmentCategoryResource(){
        super(EquipmentCategory.class);
    }
}
