package com.misoft.hospital.rest.cssd;

import com.misoft.hospital.model.cssd.Equipment;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 3/30/15.
 */
@Stateless
@Path("/equipment")
public class EquipmentResource extends BaseEntityService<Equipment> {
    public EquipmentResource(){
        super(Equipment.class);
    }
}
