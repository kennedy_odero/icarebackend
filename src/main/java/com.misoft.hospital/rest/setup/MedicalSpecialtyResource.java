package com.misoft.hospital.rest.setup;

import com.misoft.hospital.model.setup.MedicalSpecialty;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 3/1/15.
 */
@Stateless
@Path("/medicalSpecialties")
public class MedicalSpecialtyResource extends BaseEntityService<MedicalSpecialty> {
    public MedicalSpecialtyResource(){
        super(MedicalSpecialty.class);
    }
}
