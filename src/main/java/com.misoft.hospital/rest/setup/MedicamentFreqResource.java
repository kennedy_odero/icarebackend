package com.misoft.hospital.rest.setup;

import com.misoft.hospital.model.setup.MedicamentFrequency;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 3/4/15.
 */
@Stateless
@Path("/medicamentFrequencies")
public class MedicamentFreqResource extends BaseEntityService<MedicamentFrequency> {
    public MedicamentFreqResource(){
        super(MedicamentFrequency.class);
    }
}
