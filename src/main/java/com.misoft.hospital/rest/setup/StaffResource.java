package com.misoft.hospital.rest.setup;

import com.misoft.hospital.model.setup.Staff;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 4/3/15.
 */
@Stateless
@Path("/staff")
public class StaffResource extends BaseEntityService<Staff> {
    public StaffResource(){
        super(Staff.class);
    }
}
