package com.misoft.hospital.rest.setup;

import com.misoft.hospital.model.setup.Disease;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 2/23/15.
 */
@Stateless
@Path("/diseases")
public class DiseaseResource extends BaseEntityService<Disease> {
    public DiseaseResource(){
        super(Disease.class);
    }
}
