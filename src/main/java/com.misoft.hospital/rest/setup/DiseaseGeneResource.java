package com.misoft.hospital.rest.setup;

import com.misoft.hospital.model.setup.DiseaseGene;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 2/24/15.
 */
@Stateless
@Path("/diseaseGenes")
public class DiseaseGeneResource extends BaseEntityService<DiseaseGene>{
    public DiseaseGeneResource(){
        super(DiseaseGene.class);
    }
}
