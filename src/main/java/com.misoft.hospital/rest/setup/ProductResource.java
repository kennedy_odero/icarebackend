package com.misoft.hospital.rest.setup;

import com.misoft.hospital.model.setup.Product;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 3/4/15.
 */
@Stateless
@Path("/products")
public class ProductResource extends BaseEntityService<Product> {
    public ProductResource(){
        super(Product.class);
    }
}
