package com.misoft.hospital.rest.setup;

import com.misoft.hospital.model.setup.Location;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 8/24/15.
 */
@Stateless
@Path("/locations")
public class LocationResource extends BaseEntityService<Location>{
    public LocationResource(){
        super(Location.class);
    }
}
