package com.misoft.hospital.rest.setup;

import com.misoft.hospital.model.setup.Building;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 3/1/15.
 */
@Stateless
@Path("/buildings")
public class BuildingResource extends BaseEntityService<Building>{

    public BuildingResource(){
        super(Building.class);
    }
}
