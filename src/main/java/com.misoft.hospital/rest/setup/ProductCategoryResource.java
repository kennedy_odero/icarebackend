package com.misoft.hospital.rest.setup;

import com.misoft.hospital.model.setup.ProductCategory;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 3/4/15.
 */
@Stateless
@Path("/productCategories")
public class ProductCategoryResource extends BaseEntityService<ProductCategory>{
    public ProductCategoryResource(){
        super(ProductCategory.class);
    }
}
