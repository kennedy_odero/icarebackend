package com.misoft.hospital.rest.setup;

import com.misoft.hospital.model.setup.MedicalProcedure;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 2/25/15.
 */
@Stateless
@Path("/medicalProcedures")
public class MedicalProcedureResource extends BaseEntityService<MedicalProcedure> {

    public MedicalProcedureResource(){
        super(MedicalProcedure.class);
    }
}
