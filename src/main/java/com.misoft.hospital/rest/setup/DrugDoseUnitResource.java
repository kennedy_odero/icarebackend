package com.misoft.hospital.rest.setup;

import com.misoft.hospital.model.setup.DrugDoseUnit;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 3/2/15.
 */
@Stateless
@Path("/drugDoseUnits")
public class DrugDoseUnitResource extends BaseEntityService<DrugDoseUnit> {
    public DrugDoseUnitResource(){
        super(DrugDoseUnit.class);
    }
}
