package com.misoft.hospital.rest.setup;

import com.misoft.hospital.model.setup.UnitOfMeasure;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 3/4/15.
 */
@Stateless
@Path("/unitsOfMeasure")
public class UnitOfMeasureResource extends BaseEntityService<UnitOfMeasure> {
    public UnitOfMeasureResource(){
        super(UnitOfMeasure.class);
    }
}
