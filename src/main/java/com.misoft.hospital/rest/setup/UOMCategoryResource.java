package com.misoft.hospital.rest.setup;

import com.misoft.hospital.model.setup.UOMCategory;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 3/4/15.
 */
@Stateless
@Path("/uomCategories")
public class UOMCategoryResource extends BaseEntityService<UOMCategory> {
    public UOMCategoryResource(){
        super(UOMCategory.class);
    }
}
