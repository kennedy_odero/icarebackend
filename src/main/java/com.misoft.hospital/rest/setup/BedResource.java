package com.misoft.hospital.rest.setup;

import com.misoft.hospital.model.setup.Bed;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 3/1/15.
 */
@Stateless
@Path("/beds")
public class BedResource extends BaseEntityService<Bed>{
    public BedResource(){
        super(Bed.class);
    }
}
