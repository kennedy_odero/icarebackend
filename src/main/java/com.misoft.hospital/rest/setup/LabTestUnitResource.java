package com.misoft.hospital.rest.setup;

import com.misoft.hospital.model.setup.LabTestUnit;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 2/25/15.
 */
@Stateless
@Path("/labTestUnits")
public class LabTestUnitResource extends BaseEntityService<LabTestUnit> {
    public LabTestUnitResource(){
        super(LabTestUnit.class);
    }
}
