package com.misoft.hospital.rest.setup;

import com.misoft.hospital.model.setup.ImagingTestType;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 2/24/15.
 */
@Stateless
@Path("/imagingTestTypes")
public class ImagingTestTypeResource extends BaseEntityService<ImagingTestType>{
    public ImagingTestTypeResource(){
        super(ImagingTestType.class);
    }
}
