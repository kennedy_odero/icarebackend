package com.misoft.hospital.rest.setup;

import com.misoft.hospital.model.setup.MedicamentCategory;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 7/24/15.
 */
@Stateless
@Path("/medicamentCategories")
public class MedicamentCategoryResource extends BaseEntityService<MedicamentCategory>{
    public MedicamentCategoryResource(){
        super(MedicamentCategory.class);
    }
}
