package com.misoft.hospital.rest.setup;

import com.misoft.hospital.model.setup.DocumentType;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 3/13/15.
 */
@Stateless
@Path("/documentTypes")
public class DocumentTypeResource extends BaseEntityService<DocumentType> {
    public DocumentTypeResource(){
        super(DocumentType.class);
    }
}
