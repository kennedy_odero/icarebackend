package com.misoft.hospital.rest.setup;

import com.misoft.hospital.model.setup.ImagingTest;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

/**
 * Created by kodero on 8/26/15.
 */
@Stateless
@Path("/imagingTests")
public class ImagingTestResource extends BaseEntityService<ImagingTest>{
    public ImagingTestResource(){
        super(ImagingTest.class);
    }

    @POST
    @Path("/{id}/block")
    public Response block(@PathParam("id") String id){
        ImagingTest imagingTest = getEntityManager().find(ImagingTest.class, id);
        if(imagingTest == null) return Response.status(404).build();
        imagingTest.setBlocked(!imagingTest.getBlocked());
        try {
            return Response.ok(toDTO(imagingTest, new String[]{"id", "blocked"}, ImagingTest.class)).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(500).build();
        }
    }
}
