package com.misoft.hospital.rest.setup;

import com.misoft.hospital.model.setup.Country;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 8/24/15.
 */
@Stateless
@Path("/countries")
public class CountryResource extends BaseEntityService<Country>{
    public CountryResource(){
        super(Country.class);
    }
}
