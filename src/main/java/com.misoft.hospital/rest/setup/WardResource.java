package com.misoft.hospital.rest.setup;

import com.misoft.hospital.model.setup.Ward;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 3/1/15.
 */
@Stateless
@Path("/wards")
public class WardResource extends BaseEntityService<Ward>{
    public WardResource(){
        super(Ward.class);
    }
}
