package com.misoft.hospital.rest.setup;

import com.misoft.hospital.model.setup.DiseaseCategory;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 2/24/15.
 */
@Stateless
@Path("/diseaseCategories")
public class DiseaseCategoryResource extends BaseEntityService<DiseaseCategory> {
    public DiseaseCategoryResource(){
        super(DiseaseCategory.class);
    }
}
