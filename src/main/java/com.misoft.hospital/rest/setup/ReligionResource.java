package com.misoft.hospital.rest.setup;

import com.misoft.hospital.model.setup.Religion;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 8/24/15.
 */
@Stateless
@Path("/religions")
public class ReligionResource extends BaseEntityService<Religion>{
    public ReligionResource(){
        super(Religion.class);
    }
}
