package com.misoft.hospital.rest.setup;

import com.misoft.hospital.model.setup.OperatingRoom;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 3/1/15.
 */
@Stateless
@Path("/operatingRooms")
public class OperatingRoomResource extends BaseEntityService<OperatingRoom> {
    public OperatingRoomResource(){
        super(OperatingRoom.class);
    }
}
