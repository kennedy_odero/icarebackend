package com.misoft.hospital.rest.setup;

import com.misoft.hospital.model.setup.TestCase;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 2/8/16.
 */
@Stateless
@Path("/testCases")
public class TestCaseResource extends BaseEntityService<TestCase>{
    public TestCaseResource(){
        super(TestCase.class);
    }
}
