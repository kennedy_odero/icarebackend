package com.misoft.hospital.rest.setup;

import com.misoft.hospital.model.setup.DrugForm;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 3/2/15.
 */
@Stateless
@Path("/drugForms")
public class DrugFormResource extends BaseEntityService<DrugForm> {
    public DrugFormResource(){
        super(DrugForm.class);
    }
}
