package com.misoft.hospital.rest.setup;

import com.misoft.hospital.model.setup.Unit;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 3/1/15.
 */
@Stateless
@Path("/units")
public class UnitResource extends BaseEntityService<Unit> {
    public UnitResource(){
        super(Unit.class);
    }
}
