package com.misoft.hospital.rest.setup;

import com.misoft.hospital.model.setup.PathologyGroup;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 2/18/15.
 */
@Stateless
@Path("/pathologyGroups")
public class PathologyGroupResource extends BaseEntityService<PathologyGroup>{
    public PathologyGroupResource(){
        super(PathologyGroup.class);
    }
}
