package com.misoft.hospital.rest.setup;

import com.misoft.hospital.model.setup.Medicament;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

/**
 * Created by kodero on 3/4/15.
 */
@Stateless
@Path("/medicaments")
public class MedicamentResource extends BaseEntityService<Medicament> {
    public MedicamentResource(){
        super(Medicament.class);
    }

    @POST
    @Path("/{id}/block")
    public Response block(@PathParam("id") String id){
        Medicament medicament = getEntityManager().find(Medicament.class, id);
        if(medicament == null) return Response.status(404).build();
        medicament.setBlocked(!medicament.getBlocked());
        try {
            return Response.ok(toDTO(medicament, new String[]{"id", "blocked"}, Medicament.class)).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(500).build();
        }
    }
}
