package com.misoft.hospital.rest.setup;

import com.misoft.hospital.model.setup.Module;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 7/24/15.
 */
@Stateless
@Path("/modules")
public class ModuleResource extends BaseEntityService<Module>{
    public ModuleResource(){
        super(Module.class);
    }
}
