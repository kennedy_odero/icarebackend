package com.misoft.hospital.rest.setup;

import com.misoft.hospital.model.setup.Institution;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 3/1/15.
 */
@Stateless
@Path("/institutions")
public class InstitutionResource extends BaseEntityService<Institution>{

    public InstitutionResource(){
        super(Institution.class);
    }
}
