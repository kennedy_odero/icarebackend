package com.misoft.hospital.rest.setup;

import com.misoft.hospital.model.admin.FrontendResource;
import com.misoft.hospital.model.admin.ResourceType;
import com.misoft.hospital.rest.BaseEntityService;
import com.misoft.hospital.util.annotations.AuthenticationNotRequired;

import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import java.util.*;

/**
 * Created by kodero on 7/1/15.
 */
@Stateless
@Path("/frontendResources")
public class ResourceResource extends BaseEntityService<FrontendResource> {
    public ResourceResource(){
        super(FrontendResource.class);
    }

    @GET
    @AuthenticationNotRequired
    @Path("/{namespace}/{url}")
    public Response streamResource(@PathParam("namespace") String namespace, @PathParam("url") String url){
        log.info("Namespace : {" + namespace + "}, url : {" + url + "}");
        if(url != null){
            String content = (String) getEntityManager().createNamedQuery("findByUrlAndNamespace")
                    .setParameter("url", url).setParameter("namespace", namespace).getSingleResult();
            if (url.indexOf(".html") > 0) response.getOutputHeaders().putSingle("Content-Type", "text/html");
            return Response.ok(content).build();
        }else{
            log.info("No {url} parameter provided!");
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    @GET
    @AuthenticationNotRequired
    @Path("/resourceTree/{resourceType}")
    public Response streamResources(@PathParam("resourceType") ResourceType resourceType){
        log.info("resourceType : {" + resourceType + "}");
        Query q = getEntityManager().createQuery("select NEW com.misoft.hospital.model.admin.FrontendResource(r.id, r.name, r.folder, r.resourceUrl, r.resourceType) from FrontendResource r where r.deleted <> 1 order by r.folder", FrontendResource.class);
        //q.setParameter("resourceType", resourceType);
        List<FrontendResource> resources = q.getResultList();
        //create the result tree at scripts
        Map<String, Object> root = new HashMap<>();
        root.put("name", "Js resources");
        for(FrontendResource fer : resources){
            String[] folderParts = fer.getFolder().split("\\.");
            log.info("folderParts : " + fer.getFolder());
            String folder1 = folderParts[0];
            Map<String, Object> folder1Map = addChildIfNotExists(root, folder1);
            if(folderParts.length > 1){
                String folder2 = folderParts[1];
                Map<String, Object> folder2Map = addChildIfNotExists(folder1Map, folder2);
                addResource(folder2Map, fer);
            }else{
                //add it to the outer folder
                addResource(folder1Map, fer);
            }
        }
        List ret = new ArrayList();
        ret.add(root);
        return Response.ok(ret).build();
    }

    private Map<String, Object> addChildIfNotExists(Map<String, Object> node, String nodeName) {
        //construct a child map
        Map<String, Object> childNode = new HashMap<>();
        childNode.put("name", nodeName);
        childNode.put("type", "folder");
        if(!node.containsKey("children")) {
            node.put("children", new ArrayList<>());
        }else{
            //check if the child already exists in the collection
            List<Map<String, Object>> existingChildren = (List)node.get("children");
            //check if the stuff exists
            Iterator<Map<String, Object>> it = existingChildren.iterator();
            boolean found = false;
            while(it.hasNext()){
                Map<String, Object> currentMap = it.next();
                if(((String)childNode.get("name")).equalsIgnoreCase((String)currentMap.get("name"))){
                    found = true;
                    childNode = currentMap;
                }
            }
            //if found, return it, otherwise, add it (the last code block)
            if(found) return childNode;
        }

        ((List)node.get("children")).add(childNode);
        return childNode;
    }

    private Map addResource(Map<String, Object> map, FrontendResource fer) {
        Map<String, String> leafContent = new HashMap<>();
        leafContent.put("id", fer.getId());
        leafContent.put("name", fer.getResourceUrl());
        leafContent.put("canonicalName", fer.getName());
        leafContent.put("type", fer.getResourceType().toString().toLowerCase());
        if(!map.containsKey("children")) map.put("children", new ArrayList<>());
        ((List)map.get("children")).add(leafContent);
        return map;
    }
}
