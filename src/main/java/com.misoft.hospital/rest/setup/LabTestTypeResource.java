package com.misoft.hospital.rest.setup;

import com.misoft.hospital.model.setup.LabTestType;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

/**
 * Created by kodero on 2/25/15.
 */
@Stateless
@Path("/labTestTypes")
public class LabTestTypeResource extends BaseEntityService<LabTestType> {
    public LabTestTypeResource(){
        super(LabTestType.class);
    }

    @POST
    @Path("/{id}/block")
    public Response block(@PathParam("id") String id){
        LabTestType labTestType = getEntityManager().find(LabTestType.class, id);
        if(labTestType == null) return Response.status(404).build();
        labTestType.setBlocked(!labTestType.getBlocked());
        try {
            return Response.ok(toDTO(labTestType, new String[]{"id", "blocked"}, LabTestType.class)).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(500).build();
        }
    }
}
