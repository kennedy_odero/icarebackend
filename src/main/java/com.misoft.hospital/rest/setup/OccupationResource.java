package com.misoft.hospital.rest.setup;

import com.misoft.hospital.model.setup.Occupation;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 8/25/15.
 */
@Stateless
@Path("/occupations")
public class OccupationResource extends BaseEntityService<Occupation>{
    public OccupationResource(){
        super(Occupation.class);
    }
}
