package com.misoft.hospital.rest.util;

import com.misoft.hospital.model.util.MappedEntity;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ws.rs.Path;

/**
 * Created by kodero on 10/19/14.
 */
@Path("/mappedEntities")
@javax.ejb.Stateless
public class MappedEntityResource extends BaseEntityService<MappedEntity> {

    public MappedEntityResource(){
        super(MappedEntity.class);
    }
}
