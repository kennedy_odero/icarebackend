package com.misoft.hospital.rest.util;

import com.misoft.hospital.model.util.MultiElementGroup;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ws.rs.Path;

/**
 * @author mokua
 */
@Path("/multiElementGroups")
@javax.ejb.Stateless
public class MultiElementGroupResource extends BaseEntityService<MultiElementGroup> {

    public MultiElementGroupResource() {
        super(MultiElementGroup.class);
    }

}
