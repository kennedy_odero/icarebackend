package com.misoft.hospital.rest.util;

import com.misoft.hospital.model.util.GroupMember;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ws.rs.Path;

/**
 * @author mokua
 */
@Path("/groupMembers")
@javax.ejb.Stateless
public class GroupMemberResource extends BaseEntityService<GroupMember> {

    public GroupMemberResource() {
        super(GroupMember.class);
    }

}
