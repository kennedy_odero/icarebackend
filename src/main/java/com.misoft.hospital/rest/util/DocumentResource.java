

package com.misoft.hospital.rest.util;

import com.misoft.hospital.model.util.Document;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ws.rs.Path;

/**
 * @author mokua
 */
@Path("/documents")
@javax.ejb.Stateless
public class DocumentResource extends BaseEntityService<Document> {

    public DocumentResource() {
        super(Document.class);
    }
}
