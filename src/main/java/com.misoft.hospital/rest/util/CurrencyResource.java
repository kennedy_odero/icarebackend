

package com.misoft.hospital.rest.util;

import com.misoft.hospital.data.gdto.GenericDTO;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;
import java.util.Set;

/**
 * @author mokua
 *         <p/>
 *         TODO cache..
 */
@Path("/currency")
@javax.ejb.Stateless
public class CurrencyResource {
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<GenericDTO> getAll() {
        Set<Currency> all = Currency.getAvailableCurrencies();
        List<GenericDTO> dtos = new ArrayList<>(all.size());
        for (Currency currency : all) {
            GenericDTO genericDTO = new GenericDTO(currency.getClass().getName());
            genericDTO.addString("currencyCode", currency.getCurrencyCode());
            genericDTO.addString("displayName", currency.getDisplayName());
            genericDTO.addString("symbol", currency.getSymbol());
            genericDTO.addInt("numericCode", currency.getNumericCode());
            dtos.add(genericDTO);
        }
        return dtos;
    }
}
