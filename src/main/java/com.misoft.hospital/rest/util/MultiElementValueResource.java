

package com.misoft.hospital.rest.util;

import com.misoft.hospital.model.util.MultiElementValue;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ws.rs.Path;

/**
 * @author mokua
 */
@Path("/multiElementValues")
@javax.ejb.Stateless
public class MultiElementValueResource extends BaseEntityService<MultiElementValue> {

    public MultiElementValueResource() {
        super(MultiElementValue.class);
    }

}
