package com.misoft.hospital.rest.util;

import com.misoft.hospital.data.gdto.GenericDTO;
import com.misoft.hospital.model.util.SeqGen;
import org.joda.time.DateTime;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import java.io.Serializable;
import java.util.Calendar;
import java.util.logging.Logger;

/**
 * @author mokua
 */
@javax.ejb.Stateless
@Path("/sequences")
public class SequenceManager implements Serializable {

    public static final long DEFAULT_SEQ_START_VALUE = 1000000;

    @Inject
    private EntityManager entityManager;

    @Inject
    private transient Logger log;

    @GET
    @Path("/nextSequence/{seqName}")
    public Response getNextNextSequence(@PathParam("seqName") String seqName){
        Long nextSeqence = nextSequence(seqName);
        return Response.ok(new GenericDTO("com.misoft.hospital.model.util.SeqGen").addString("sequenceName", seqName).addLong("nextVal", nextSeqence)).build();
    }

    @GET
    @Path("/nextSequence/{currentYear}/{currentMonth}")
    public Response getNextNextSequence(){
        DateTime dt = new DateTime();
        Calendar cal = Calendar.getInstance();
        String seqName = cal.get(Calendar.YEAR) + "/" + dt.getMonthOfYear();
        Long nextSeqence = nextSequence(seqName);
        return Response.ok(new GenericDTO("com.misoft.hospital.model.util.SeqGen").addString("sequenceName", seqName).addLong("nextVal", nextSeqence)).build();
    }

    public synchronized Long nextSequence(String sequenceName) {
        SeqGen seqGen = null;
        Long nextSequence = null;
        try {
            log.info("Seq Name" + sequenceName);
            seqGen = (SeqGen) entityManager
                    .createQuery("select i from SeqGen i where i.sequenceName=:seqName ")
                    .setParameter("seqName", sequenceName)
                    .getSingleResult();
            log.info("The seqGen" + seqGen);

            nextSequence = seqGen.getNextVal();
            //update
            seqGen.setNextVal(nextSequence + 1);
            entityManager.merge(seqGen);
            return nextSequence;
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            if (seqGen == null) {
                //create it and insert then return default 0
                seqGen = new SeqGen(sequenceName, DEFAULT_SEQ_START_VALUE);
                seqGen = entityManager.merge(seqGen);
                nextSequence = seqGen.getNextVal();
                //update
                seqGen.setNextVal(nextSequence + 1);
                entityManager.merge(seqGen);
                entityManager.flush();
                return nextSequence;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
