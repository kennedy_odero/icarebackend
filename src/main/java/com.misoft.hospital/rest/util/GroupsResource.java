

package com.misoft.hospital.rest.util;

import com.misoft.hospital.model.util.Groups;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ws.rs.Path;

/**
 * @author mokua
 */
@Path("/groups")
@javax.ejb.Stateless
public class GroupsResource extends BaseEntityService<Groups> {

    public GroupsResource() {
        super(Groups.class);
    }

}
