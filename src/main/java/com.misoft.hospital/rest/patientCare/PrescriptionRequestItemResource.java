package com.misoft.hospital.rest.patientCare;

import com.misoft.hospital.model.healthcare.PrescriptionRequestItem;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 12/12/15.
 */
@Stateless
@Path("/prescriptionRequestItems")
public class PrescriptionRequestItemResource extends BaseEntityService<PrescriptionRequestItem>{
    public PrescriptionRequestItemResource(){
        super(PrescriptionRequestItem.class);
    }
}
