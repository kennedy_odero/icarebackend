package com.misoft.hospital.rest.patientCare;

import com.misoft.hospital.model.healthcare.ScheduleRun;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 12/13/15.
 */
@Stateless
@Path("/scheduleRuns")
public class ScheduleRunResource extends BaseEntityService<ScheduleRun>{
    public ScheduleRunResource(){
        super(ScheduleRun.class);
    }
}
