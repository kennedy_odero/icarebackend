package com.misoft.hospital.rest.patientCare;

import com.misoft.hospital.model.healthcare.LabRequestItem;
import com.misoft.hospital.model.healthcare.LabRequestItemStatus;
import com.misoft.hospital.rest.BaseEntityService;
import com.misoft.hospital.service.grant.GService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.validation.ValidationException;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;

/**
 * Created by kodero on 11/17/15.
 */
@Stateless
@Path("/labRequestItems")
public class LabRequestItemResource extends BaseEntityService<LabRequestItem>{
    @Inject
    GService gService;

    public LabRequestItemResource(){
        super(LabRequestItem.class);
    }

    @POST
    @Path("/{id:[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}}/authorize")
    public Response authorize(@PathParam("id") String id){
        LabRequestItem labRequestItem = gService.find(id, LabRequestItem.class);
        if(labRequestItem == null) return createValidationResponse(new ValidationException("No test with the given id[" + id + "]")).build();
        if(labRequestItem.getRequestItemStatus() != LabRequestItemStatus.Complete) return createValidationResponse(new ValidationException("Cannot authorize a test that is not complete!")).build();
        labRequestItem.setRequestItemStatus(LabRequestItemStatus.Authorized);
        entityUpdatedEventSrc.fire(new EntityUpdatedEvent<>(labRequestItem));
        return Response.ok().build();
    }

    @POST
    @Path("/{id:[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}}/release")
    public Response release(@PathParam("id") String id){
        LabRequestItem labRequestItem = gService.find(id, LabRequestItem.class);
        if(labRequestItem == null) return createValidationResponse(new ValidationException("No test with the given id[" + id + "]")).build();
        if(labRequestItem.getRequestItemStatus() != LabRequestItemStatus.Authorized) return createValidationResponse(new ValidationException("Cannot authorize a test that is not complete!")).build();
        labRequestItem.setRequestItemStatus(LabRequestItemStatus.Released);
        entityUpdatedEventSrc.fire(new EntityUpdatedEvent<>(labRequestItem));
        return Response.ok().build();
    }

    @POST
    @Path("/{id:[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}}/cancel")
    public Response cancel(@PathParam("id") String id){
        LabRequestItem labRequestItem = gService.find(id, LabRequestItem.class);
        if(labRequestItem == null) return createValidationResponse(new ValidationException("No test with the given id[" + id + "]")).build();
        labRequestItem.setRequestItemStatus(LabRequestItemStatus.Cancelled);//TODO, only cancel if its not been paid for
        entityUpdatedEventSrc.fire(new EntityUpdatedEvent<>(labRequestItem));
        return Response.ok().build();
    }
}
