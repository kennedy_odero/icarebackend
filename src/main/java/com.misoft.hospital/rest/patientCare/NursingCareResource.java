package com.misoft.hospital.rest.patientCare;

import com.misoft.hospital.model.healthcare.NursingCare;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 4/3/15.
 */
@Stateless
@Path("/nursingCare")
public class NursingCareResource extends BaseEntityService<NursingCare> {
    public NursingCareResource(){
        super(NursingCare.class);
    }
}
