package com.misoft.hospital.rest.patientCare;

import com.misoft.hospital.model.healthcare.ClinicBooking;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 9/30/15.
 */
@Stateless
@Path("/clinicBookings")
public class ClinicBookingResource extends BaseEntityService<ClinicBooking>{
    public ClinicBookingResource(){
        super(ClinicBooking.class);
    }
}
