package com.misoft.hospital.rest.patientCare;

import com.misoft.hospital.model.healthcare.ReviewSystem;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 3/13/16.
 */
@Stateless
@Path("/reviewSystems")
public class ReviewSystemResource extends BaseEntityService<ReviewSystem>{
    public ReviewSystemResource(){
        super(ReviewSystem.class);
    }
}
