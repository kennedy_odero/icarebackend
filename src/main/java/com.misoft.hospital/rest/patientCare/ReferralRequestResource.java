package com.misoft.hospital.rest.patientCare;

/**
 * Created by kodero on 4/19/15.
 */

import com.misoft.hospital.model.healthcare.ReferralRequest;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

@Stateless
@Path("/referralRequests")
public class ReferralRequestResource extends ServiceRequestResource<ReferralRequest> {

    public void extraValidations(ReferralRequest referralRequest){
        //call super class validation first
        super.extraValidations(referralRequest);
        //perform any other validation
    }

    public ReferralRequestResource(){
        setEntityClass(ReferralRequest.class);
    }
}
