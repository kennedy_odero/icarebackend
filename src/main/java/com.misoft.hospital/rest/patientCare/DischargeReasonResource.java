package com.misoft.hospital.rest.patientCare;

import com.misoft.hospital.model.healthcare.DischargeReason;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 5/23/16.
 */
@Stateless
@Path("/dischargeReasons")
public class DischargeReasonResource extends BaseEntityService<DischargeReason>{
    public DischargeReasonResource(){
        super(DischargeReason.class);
    }
}
