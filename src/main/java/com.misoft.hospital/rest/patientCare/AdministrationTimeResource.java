package com.misoft.hospital.rest.patientCare;

import com.misoft.hospital.model.healthcare.AdministrationTime;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 4/19/15.
 */
@Stateless
@Path("/administrationTimes")
public class AdministrationTimeResource extends BaseEntityService<AdministrationTime> {
    public AdministrationTimeResource(){
        super(AdministrationTime.class);
    }
}
