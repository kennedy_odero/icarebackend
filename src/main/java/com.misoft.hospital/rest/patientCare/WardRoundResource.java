package com.misoft.hospital.rest.patientCare;

import com.misoft.hospital.model.healthcare.WardRound;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 8/30/15.
 */
@Stateless
@Path("/wardRounds")
public class WardRoundResource extends BaseEntityService<WardRound>{
    public WardRoundResource(){
        super(WardRound.class);
    }
}
