package com.misoft.hospital.rest.patientCare;

import com.misoft.hospital.model.healthcare.WardRoundRole;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 4/1/15.
 */
@Stateless
@Path("/roundingRoles")
public class WardRoundRoleResource extends BaseEntityService<WardRoundRole> {
    public WardRoundRoleResource(){
        super(WardRoundRole.class);
    }
}
