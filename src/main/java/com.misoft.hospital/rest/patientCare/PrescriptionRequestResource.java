package com.misoft.hospital.rest.patientCare;

import com.misoft.hospital.data.gdto.GenericDTO;
import com.misoft.hospital.model.billing.BillItem;
import com.misoft.hospital.model.billing.BillStatus;
import com.misoft.hospital.model.billing.BillType;
import com.misoft.hospital.model.billing.PatientBill;
import com.misoft.hospital.model.healthcare.*;
import com.misoft.hospital.model.setup.Medicament;
import com.misoft.hospital.model.setup.MedicamentFrequency;
import com.misoft.hospital.model.setup.Product;
import com.misoft.hospital.rest.util.SequenceManager;
import com.misoft.hospital.util.types.DateParam;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.validation.ValidationException;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by kodero on 3/20/15.
 */
@Stateless
@Path("/prescriptionRequests")
public class PrescriptionRequestResource extends ServiceRequestResource<PrescriptionRequest>{
    @Inject
    private SequenceManager seq;

    public PrescriptionRequestResource(){
        setEntityClass(PrescriptionRequest.class);
    }

    public void extraValidations(PrescriptionRequest prescriptionRequest){
        //call super class validation first
        super.extraValidations(prescriptionRequest);
        //perform any other validation
        if(prescriptionRequest.getPrescriptionRequestItems().size() == 0){
            throw new ValidationException("Please add at least one prescription medicine!");
        }
    }

    @POST
    @Path("/{id}/generateBill")
    public Response generateBill(@PathParam("id") String id){
        PrescriptionRequest req = gService.find(id, PrescriptionRequest.class);
        if(req.getBilled()){
            return Response.status(Response.Status.BAD_REQUEST).entity("Service request (#" + req.getRequestNo() + ") already billed!").build();
        }
        List<PrescriptionRequestItem> items = new ArrayList<>(req.getPrescriptionRequestItems());
        //now generate bill
        PatientBill patientBill = new PatientBill();
        patientBill.setPatient(req.getPatient());
        patientBill.setBillNo(seq.nextSequence("PATIENT_BILL"));
        patientBill.setServiceRequest(req);
        patientBill.setDateGenerated(new Date());
        patientBill.setDueDate(new Date());
        patientBill.setBillType(BillType.Cash);
        patientBill.setAmountWaived(BigDecimal.ZERO);
        patientBill.setBillStatus(BillStatus.APPROVED);

        BigDecimal total = BigDecimal.ZERO;
        BigDecimal tax = BigDecimal.ZERO;
        for(PrescriptionRequestItem item : items){
            Product product = item.getMedicament().getProduct();
            BillItem billItem = new BillItem();
            billItem.setProduct(product);
            billItem.setQuantity(item.getPerDoseQty());
            billItem.setUnitPrice(product.getListPrice());
            billItem.setLineTotal(product.getListPrice().multiply(billItem.getQuantity()));
            billItem.setTax(BigDecimal.ZERO);
            total = total.add(billItem.getLineTotal());
            tax = tax.add(billItem.getTax());
            patientBill.addBillItem(billItem);
        }
        patientBill.setAmountBilled(total);
        patientBill.setAggregateTax(tax);
        try{
            patientBill = gService.makePersistent(patientBill);
            req.setBilled(true);
            gService.edit(req);
            String[] receivableFields = {"id", "amountBilled"};
            return Response.ok().entity(toDTO(patientBill, receivableFields, PatientBill.class)).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();
        }
    }

    @GET
    @Path("/calculateDosage")
    public Response calculateDosage(@QueryParam("perDoseQty") BigDecimal perDoseQty, @QueryParam("drugFreqId") String drugFreqId,
                                    @QueryParam("duration") String duration, @QueryParam("medicamentId") String medicamentId){
        ScriptEngineManager mgr = new ScriptEngineManager();
        ScriptEngine engine = mgr.getEngineByName("JavaScript");
        Medicament med = gService.find(medicamentId, Medicament.class);
        MedicamentFrequency freq = gService.find(drugFreqId, MedicamentFrequency.class);
        if(freq == null) return createValidationResponse(new ValidationException("No medicament frequency found with the provided id {" + drugFreqId + "}")).build();
        try{
            //lets execute the expression
            log.info("Per dosage quantity : " + perDoseQty);
            log.info("Duration : " + duration);
            String toEval = freq.getDosageFormula().replaceAll("p", perDoseQty + "").replaceAll("d", duration);
            Object ret = engine.eval(toEval);
            log.info("The return object : " + ret);
            BigDecimal dosageQuantity = BigDecimal.ZERO;
            if(ret instanceof Double) dosageQuantity = new BigDecimal((Double) engine.eval(toEval));
            else if(ret instanceof Integer) dosageQuantity = new BigDecimal((Integer) engine.eval(toEval));
            BigDecimal dispenseQty = new BigDecimal(dosageQuantity.divide(med.getUnitQuantity()).doubleValue());
            log.info("Dispense Quantity : " + dispenseQty);
            dispenseQty = dispenseQty.setScale(0, RoundingMode.UP);
            log.info("Dispense Quantity (after rounding): " + dispenseQty);
            Map<String, BigDecimal> res = new HashMap<>();
            res.put("fullDosageQty", dosageQuantity.setScale(0, RoundingMode.UP));
            res.put("dispenseQty", dispenseQty);
            return Response.ok(res).build();
        }catch (Exception e){
            e.printStackTrace();
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("error", e.getMessage());
            return Response.status(Response.Status.BAD_REQUEST).entity(responseObj).build();
        }
    }

    @POST
    @Path("/{id}/markAsDispensed")
    public Response markAsDispensed(@PathParam("id") String id){
        PrescriptionRequest req = gService.find(id, PrescriptionRequest.class);
        //mark as dispensed
        log.info("Dispensed : " + req.getDispensed());
        if(req.getDispensed()) throw new ValidationException("Prescription already dispensed");
        req.setDispensed(true);
        req.setRequestStatus(RequestStatus.COMPLETE);
        gService.edit(req);
        return Response.ok().build();
    }

    @POST
    @Path("/{id}/unConfirm")
    public Response unConfirm(@PathParam("id") String id){
        PrescriptionRequest req = gService.find(id, PrescriptionRequest.class);
        //mark as dispensed
        log.info("Confirmed : " + req.isConfirmed());
        if(!req.isConfirmed()) throw new ValidationException("Prescription has not been confirmed!");
        if(req.isDispensed()) throw new ValidationException("Sorry, prescription already dispensed!");
        req.setConfirmed(false);
        req.setRequestStatus(RequestStatus.PROCESSING);
        gService.edit(req);
        return Response.ok().build();
    }

    @GET
    @Path("/drugConsumptionDetailed")
    public Response getDrugConsumptionDetailed(@QueryParam("dateFrom") @DefaultValue("today") DateParam dateFrom,
                                               @QueryParam("dateTo") @DefaultValue("today") DateParam dateTo){
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String sql = "select \n" +
                "\tsr.date_requested,\n" +
                "\tp.name,\n" +
                "    concat(patient.patient_no, '-', patient.surname, ' ', patient.other_names) patient,\n" +
                "    FLOOR(DATEDIFF(curdate(), patient.dob)/365) as age\n" +
                "from prescrip_req_items pri \n" +
                "\tinner join service_requests sr\n" +
                "\t\ton pri.service_request = sr.id\n" +
                "\tinner join medicament m\n" +
                "\t\ton pri.medicament = m.id\n" +
                "\tinner join products p \n" +
                "\t\ton m.product = p.id\n" +
                "\tinner join parties patient \n" +
                "\t\ton sr.patient = patient.id\n" +
                "where pri.deleted <> 1 \n" +
                "\tand sr.deleted <> 1 " +
                "\tand sr.date_requested between '" + df.format(dateFrom.getDate()) + "' and '" + df.format(dateTo.getDate()) + "' " +
                "order by sr.date_requested, patient.surname, patient.other_names";
        List<Object> results = getEntityManager().createNativeQuery(sql).getResultList();
        List<GenericDTO> balances = new ArrayList<>();
        for(Object o : results){
            Object[] row = (Object[]) o;
            GenericDTO genericDTO = new GenericDTO("com.misoft.reports.DrugConsumption");
            genericDTO.addDate("date", (Date) row[0]);
            genericDTO.addString("medicament", (String) row[1]);
            genericDTO.addString("patient", (String) row[2]);
            genericDTO.addInt("age", (Integer) row[3]);
            balances.add(genericDTO);
        }
        return Response.ok(balances).build();
    }

    @GET
    @Path("/drugConsumptionSummary")
    public Response getDrugConsumptionSummary(@QueryParam("dateFrom") @DefaultValue("today") DateParam dateFrom,
                                              @QueryParam("dateTo") @DefaultValue("today") DateParam dateTo){
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String sql = "select \n" +
                "\tdc.drug_name,\n" +
                "\tsum(case when dc.age < 5 and dc.gender = 'MALE' then 1 else 0 end) under_5_m,\n" +
                "    sum(case when dc.age < 5 and dc.gender = 'FEMALE' then 1 else 0 end) under_5_f,\n" +
                "    sum(case when dc.age >= 5 and dc.gender = 'MALE' then 1 else 0 end) over_5_m,\n" +
                "    sum(case when dc.age >= 5 and dc.gender = 'FEMALE' then 1 else 0 end) over_5_f,\n" +
                "    count(dc.drug_name) as _count\n" +
                "from \n" +
                "    (select\n" +
                "\t\tsr.date_requested,\n" +
                "\t\tp.name as drug_name,\n" +
                "\t\tconcat(patient.patient_no, '-', patient.surname, ' ', patient.other_names) patient,\n" +
                "\t\tFLOOR(DATEDIFF(curdate(), patient.dob)/365) as age,\n" +
                "        patient.gender\n" +
                "\tfrom prescrip_req_items pri \n" +
                "\t\tinner join service_requests sr\n" +
                "\t\t\ton pri.service_request = sr.id\n" +
                "\t\tinner join medicament m\n" +
                "\t\t\ton pri.medicament = m.id\n" +
                "\t\tinner join products p \n" +
                "\t\t\ton m.product = p.id\n" +
                "\t\tinner join parties patient \n" +
                "\t\t\ton sr.patient = patient.id\n" +
                "\twhere pri.deleted <> 1 \n" +
                "\t\tand sr.deleted <> 1\n" +
                "        and sr.date_requested between '" + df.format(dateFrom.getDate()) + "' and '" + df.format(dateTo.getDate()) + "') as dc\n" +
                "group by dc.drug_name\n" +
                "order by dc.drug_name";
        List<Object> results = getEntityManager().createNativeQuery(sql).getResultList();
        List<GenericDTO> balances = new ArrayList<>();
        for(Object o : results){
            Object[] row = (Object[]) o;
            GenericDTO genericDTO = new GenericDTO("com.misoft.reports.DrugConsumption");
            genericDTO.addString("medicament", (String) row[0]);
            genericDTO.addBigDecimal("under5m", (BigDecimal) row[1]);
            genericDTO.addBigDecimal("under5f", (BigDecimal) row[2]);
            genericDTO.addBigDecimal("over5m", (BigDecimal) row[3]);
            genericDTO.addBigDecimal("over5f", (BigDecimal) row[4]);
            genericDTO.addBigInteger("total", (BigInteger) row[5]);
            balances.add(genericDTO);
        }
        return Response.ok(balances).build();
    }

    @GET
    @Path("/drugConsumptionByCategory")
    public Response getDrugConsumptionByCategory(@QueryParam("dateFrom") @DefaultValue("today") DateParam dateFrom,
                                                 @QueryParam("dateTo") @DefaultValue("today") DateParam dateTo){
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String sql = "select \n" +
                "\tdc.med_category,\n" +
                "\tsum(case when dc.age < 5 and dc.gender = 'MALE' then 1 else 0 end) under_5_m,\n" +
                "    \tsum(case when dc.age < 5 and dc.gender = 'FEMALE' then 1 else 0 end) under_5_f,\n" +
                "    \tsum(case when dc.age >= 5 and dc.gender = 'MALE' then 1 else 0 end) over_5_m,\n" +
                "    \tsum(case when dc.age >= 5 and dc.gender = 'FEMALE' then 1 else 0 end) over_5_f,\n" +
                "    \tcount(dc.med_category) as _count\n" +
                "from \n" +
                "    (select\n" +
                "\tsr.date_requested,\n" +
                "\tFLOOR(DATEDIFF(curdate(), patient.dob)/365) as age,\n" +
                "\tpatient.gender,\n" +
                "\tcoalesce(mc.name, '-Not Categorized-') as med_category\n" +
                "    from prescrip_req_items pri \n" +
                "\tinner join service_requests sr\n" +
                "\t\ton pri.service_request = sr.id\n" +
                "\tinner join medicament m\n" +
                "\t\ton pri.medicament = m.id\n" +
                "\tinner join products p \n" +
                "\t\ton m.product = p.id\n" +
                "\tinner join parties patient \n" +
                "\t\ton sr.patient = patient.id\n" +
                "\tleft outer join medicament_category mc \n" +
                "\t\ton m.med_category = mc.id\n" +
                "\twhere pri.deleted <> 1 \n" +
                "\t\tand sr.deleted <> 1\n" +
                "        \tand sr.date_requested between  '" + df.format(dateFrom.getDate()) + "' and  '" + df.format(dateTo.getDate()) + "') as dc\n" +
                "    group by dc.med_category\n" +
                "    order by dc.med_category;";
        List<Object> results = getEntityManager().createNativeQuery(sql).getResultList();
        List<GenericDTO> balances = new ArrayList<>();
        for(Object o : results){
            Object[] row = (Object[]) o;
            GenericDTO genericDTO = new GenericDTO("com.misoft.reports.DrugConsumption");
            genericDTO.addString("medicamentCategory", (String) row[0]);
            genericDTO.addBigDecimal("under5m", (BigDecimal) row[1]);
            genericDTO.addBigDecimal("under5f", (BigDecimal) row[2]);
            genericDTO.addBigDecimal("over5m", (BigDecimal) row[3]);
            genericDTO.addBigDecimal("over5f", (BigDecimal) row[4]);
            genericDTO.addBigInteger("total", (BigInteger) row[5]);
            balances.add(genericDTO);
        }
        return Response.ok(balances).build();
    }
}
