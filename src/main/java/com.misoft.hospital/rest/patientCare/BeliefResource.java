package com.misoft.hospital.rest.patientCare;

import com.misoft.hospital.model.healthcare.Belief;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 2/19/16.
 */
@Stateless
@Path("/beliefs")
public class BeliefResource extends BaseEntityService<Belief>{
    public BeliefResource(){
        super(Belief.class);
    }
}
