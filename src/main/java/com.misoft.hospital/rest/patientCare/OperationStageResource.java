package com.misoft.hospital.rest.patientCare;

import com.misoft.hospital.model.healthcare.OperationStage;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 4/26/15.
 */
@Stateless
@Path("/operationStages")
public class OperationStageResource extends BaseEntityService<OperationStage> {
    public OperationStageResource(){
        super(OperationStage.class);
    }
}
