package com.misoft.hospital.rest.patientCare;

import com.misoft.hospital.data.gdto.GenericDTO;
import com.misoft.hospital.data.gdto.ValidationException;
import com.misoft.hospital.model.healthcare.Patient;
import com.misoft.hospital.rest.BaseEntityService;
import com.misoft.hospital.util.annotations.AuthenticationNotRequired;
import org.joda.time.DateTime;

import javax.ejb.Stateless;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Created by kodero on 3/12/15.
 */
@Stateless
@Path(("/patients"))
public class PatientResource extends BaseEntityService<Patient> {
    public PatientResource(){
        super(Patient.class);
    }

    @GET
    @AuthenticationNotRequired
    @Produces("image/png")
    @Path("/{id}/barcode")
    public Response getBarcode(@PathParam("id") String id) {
        List<Patient> list = getEntityManager().createQuery("select new Patient(p.id, p.patientNoBarcode) from Patient p where p.id =:patientId")
                .setParameter("patientId", id)
                .getResultList();
        if(list.size() == 1){
            return Response.ok(list.get(0).getPatientNoBarcode()).build();
        }
        return Response.status(404).build();
    }

    @GET
    @Path("/{ageInString}/getDob")
    public Response getAge(@PathParam("ageInString") String ageInString, @QueryParam("dobType") String dobType){
        DateTime dob = new DateTime();
        String [] ageParts = ageInString.split("-");
        if(dobType.equalsIgnoreCase("age")){
            if(ageParts.length == 1) dob = dob.minusYears(Integer.parseInt(ageParts[0]));
            else if(ageParts.length == 2) dob = dob.minusYears(Integer.parseInt(ageParts[0])).minusMonths(Integer.parseInt(ageParts[1]));
            else if(ageParts.length == 3) dob = dob.minusYears(Integer.parseInt(ageParts[0])).minusMonths(Integer.parseInt(ageParts[1])).minusDays(Integer.parseInt(ageParts[2]));
            else{
                createValidationResponse(new javax.validation.ValidationException("Invalid age format. Try years/months/days!"));
            }
        }else{
            //dobType = dob
            dob = dob.withYear(Integer.parseInt(ageParts[0]));
        }
        return Response.ok(new GenericDTO("patientAge").addDate("dob", dob.toDate())).build();
    }
}
