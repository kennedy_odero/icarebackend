package com.misoft.hospital.rest.patientCare;

import com.misoft.hospital.model.healthcare.AdmissionBed;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 4/5/15.
 */
@Stateless
@Path("/admissionBeds")
public class AdmissionBedResource extends BaseEntityService<AdmissionBed> {
    public AdmissionBedResource(){
        super(AdmissionBed.class);
    }
}
