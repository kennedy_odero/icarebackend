package com.misoft.hospital.rest.patientCare;

import com.misoft.hospital.model.healthcare.LabTestResult;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 5/20/16.
 */
@Stateless
@Path("/labTestResults")
public class LabTestResultResource extends BaseEntityService<LabTestResult>{
    public LabTestResultResource(){
        super(LabTestResult.class);
    }
}
