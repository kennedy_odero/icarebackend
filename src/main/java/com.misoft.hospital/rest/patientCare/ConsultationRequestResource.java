package com.misoft.hospital.rest.patientCare;

import com.misoft.hospital.data.gdto.GenericDTO;
import com.misoft.hospital.model.billing.BillItem;
import com.misoft.hospital.model.billing.BillStatus;
import com.misoft.hospital.model.billing.BillType;
import com.misoft.hospital.model.billing.PatientBill;
import com.misoft.hospital.model.healthcare.ConsultationRequest;
import com.misoft.hospital.model.healthcare.RequestConsultation;
import com.misoft.hospital.model.healthcare.TriagingRequest;
import com.misoft.hospital.model.setup.Product;
import com.misoft.hospital.rest.util.SequenceManager;
import com.misoft.hospital.service.healthcare.ServiceRequestService;
import com.misoft.hospital.util.types.DateParam;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.validation.Validation;
import javax.validation.ValidationException;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by kodero on 3/18/15.
 */
@Stateless
@Path("/consultationRequests")
public class ConsultationRequestResource extends ServiceRequestResource<ConsultationRequest> {

    @Inject
    private SequenceManager seq;

    public ConsultationRequestResource(){
        setEntityClass(ConsultationRequest.class);
    }

    @Override
    public void beforeSaveOrUpdate(ConsultationRequest consultationRequest){
        /*if(consultationRequest.getRequestConsultations().size() == 0){
            //create request consultations
            consultationRequest.getRequestConsultations().add(new RequestConsultation(consultationRequest, consultationRequest.getConsultationType()));
        }*/
    }

    public void extraValidations(ConsultationRequest consultationRequest){
        //call super class validation first
        super.extraValidations(consultationRequest);
        //perform any other validation
        if(consultationRequest.getRequestConsultations().size() == 0){
            //throw new ValidationException("Please add at least one consultation request");
        }
    }

    @POST
    @Path("/{id}/generateBill")
    public Response generateBill(@PathParam("id") String id){
        ConsultationRequest req = gService.find(id, ConsultationRequest.class);
        log.info("The req : " + req.getBilled() + ", the id : " + id);
        if(req.getBilled()){
            return Response.status(Response.Status.BAD_REQUEST).entity("Service request (#" + req.getRequestNo() + ") already billed!").build();
        }
        List<RequestConsultation> items = new ArrayList<>(req.getRequestConsultations());
        //now generate bill
        PatientBill patientBill = new PatientBill();
        patientBill.setPatient(req.getPatient());
        patientBill.setBillNo(seq.nextSequence("PATIENT_BILL"));
        patientBill.setServiceRequest(req);
        patientBill.setDateGenerated(new Date());
        patientBill.setDueDate(new Date());
        patientBill.setBillType(BillType.Cash);
        patientBill.setAmountWaived(BigDecimal.ZERO);
        patientBill.setBillStatus(BillStatus.APPROVED);

        BigDecimal total = BigDecimal.ZERO;
        BigDecimal tax = BigDecimal.ZERO;
        for(RequestConsultation item : items){
            Product product = item.getConsultationType().getProduct();
            BillItem billItem = new BillItem();
            billItem.setProduct(product);
            billItem.setQuantity(BigDecimal.ONE);
            billItem.setUnitPrice(product.getListPrice());
            billItem.setLineTotal(product.getListPrice().multiply(billItem.getQuantity()));
            billItem.setTax(BigDecimal.ZERO);
            total = total.add(billItem.getLineTotal());
            tax = tax.add(billItem.getTax());
            patientBill.addBillItem(billItem);
        }
        patientBill.setAmountBilled(total);
        patientBill.setAggregateTax(tax);
        try{
            patientBill = gService.makePersistent(patientBill);
            req.setBilled(true);
            gService.edit(req);
            entityUpdatedEventSrc.fire(new EntityUpdatedEvent<>(req));
            //entityCreatedEventSrc.fire(new EntityCreatedEvent<>(patientBill));
            String[] receivableFields = {"id", "amountBilled"};
            return Response.ok().entity(toDTO(patientBill, receivableFields, PatientBill.class)).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();
        }
    }

    @GET
    @Path("/workloadDetailed")
    public Response getWorkloadDetailedReport(@QueryParam("dateFrom") @DefaultValue("today") DateParam dateFrom,
                                               @QueryParam("dateTo") @DefaultValue("today") DateParam dateTo, @QueryParam("userId") String userId){
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String sql = "select\n" +
                "\trsh.date_changed,\n" +
                "\tp.patient_no,\n" +
                "\tconcat(p.surname, ' ', p.other_names) patient_name,\n" +
                "    concat(staff.surname, ' ', staff.other_names) seen_by,\n" +
                "    1 as count\n" +
                "from request_status_history rsh \n" +
                "\tinner join service_requests sr \n" +
                "\t\ton rsh.service_request = sr.id\n" +
                "\tinner join parties p \n" +
                "\t\ton sr.patient = p.id\n" +
                "\tinner join users u \n" +
                "\t\ton rsh.created_by_id = u.id\n" +
                "inner join parties staff " +
                "    on u.staff = staff.id " +
                "where rsh.status = 'RECEIVED' and date(rsh.date_changed) between '" + df.format(dateFrom.getDate()) + "' and '" + df.format(dateTo.getDate()) + "'\n" +
                "" + (userId == null? "" : "and staff.id = '" + userId + "' ") +
                "\tand p.deleted <> 1";
        List<Object> results = getEntityManager().createNativeQuery(sql)
                .getResultList();
        List<GenericDTO> balances = new ArrayList<>();
        for(Object o : results){
            Object[] row = (Object[]) o;
            GenericDTO genericDTO = new GenericDTO("com.misoft.reports.DoctorWorkload");
            genericDTO.addDate("date", (Date) row[0]);
            genericDTO.addString("patientNo", (String) row[1]);
            genericDTO.addString("patientName", (String) row[2]);
            genericDTO.addString("seenBy", (String) row[3]);
            genericDTO.addBigInteger("count", (BigInteger) row[4]);
            balances.add(genericDTO);
        }
        return Response.ok(balances).build();
    }

    @GET
    @Path("/workloadSummary")
    public Response getWorkloadSummaryReport(@QueryParam("dateFrom") @DefaultValue("today") DateParam dateFrom,
                                              @QueryParam("dateTo") @DefaultValue("today") DateParam dateTo, @QueryParam("userId") String userId){
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String sql = "select\n" +
                "\tconcat(staff.surname, ' ', staff.other_names) as seenBy,\n" +
                "\tcount(p.patient_no)\n" +
                "from request_status_history rsh \n" +
                "\tinner join service_requests sr \n" +
                "\t\ton rsh.service_request = sr.id\n" +
                "\tinner join parties p \n" +
                "\t\ton sr.patient = p.id\n" +
                "\tinner join users u \n" +
                "\t\ton rsh.created_by_id = u.id\n" +
                "\tinner join parties staff \n" +
                "\t\ton u.staff = staff.id\n" +
                "where rsh.status = 'RECEIVED' and date(rsh.date_changed) between '" + df.format(dateFrom.getDate()) + "' and '" + df.format(dateTo.getDate()) + "'\n" +
                "\tand p.deleted <> 1\n" +
                "group by staff.id\n" +
                "order by staff.surname, staff.other_names\n" +
                "\t";
        List<Object> results = getEntityManager().createNativeQuery(sql)
                .getResultList();
        List<GenericDTO> balances = new ArrayList<>();
        for(Object o : results){
            Object[] row = (Object[]) o;
            GenericDTO genericDTO = new GenericDTO("com.misoft.reports.DoctorWorkload");
            genericDTO.addString("seenBy", (String) row[0]);
            genericDTO.addBigInteger("total", (BigInteger) row[1]);
            balances.add(genericDTO);
        }
        return Response.ok(balances).build();
    }
}
