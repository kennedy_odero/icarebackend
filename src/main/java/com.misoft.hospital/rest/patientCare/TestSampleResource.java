package com.misoft.hospital.rest.patientCare;

import com.misoft.hospital.model.healthcare.TestSample;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 8/31/15.
 */
@Stateless
@Path("/testSamples")
public class TestSampleResource extends BaseEntityService<TestSample>{
    public TestSampleResource(){
        super(TestSample.class);
    }
}
