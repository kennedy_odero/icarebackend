package com.misoft.hospital.rest.patientCare;

import com.misoft.hospital.model.healthcare.*;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import java.util.Date;

/**
 * Created by kodero on 3/15/15.
 */
@Stateless
@Path("/triagingRequests")
public class TriagingResource extends ServiceRequestResource<TriagingRequest> {
    public TriagingResource(){
        setEntityClass(TriagingRequest.class);
    }

    public void extraValidations(TriagingRequest triagingRequest){
        //call super class validation first
        super.extraValidations(triagingRequest);
        //perform any other validation
    }

    @POST
    @Path("/{triagingReqId}/createConsultationRequest")
    public Response sendToConsultation(@PathParam("triagingReqId") String triagReqId){
        TriagingRequest triag = gService.find(triagReqId, TriagingRequest.class);
        Appointment appointment = triag.getAppointment();
        //create triaging
        ConsultationRequest con = new ConsultationRequest();
        con.setAppointment(appointment);
        con.setPatient(appointment.getPatient());
        con.setDateRequested(new Date());
        con.setServiceType("CONSULTATION");
        con.setRequestStatus(RequestStatus.NEW);
        try{
            gService.makePersistent(con);
            return Response.ok().build();
        }catch (Exception e){
            e.printStackTrace();
            return Response.status(500).build();
        }
    }

    public void raiseCreateOrUpdatedEvent(TriagingRequest triagingRequest){
        if(triagingRequest.getId() == null){
            entityCreatedEventSrc.fire(new EntityCreatedEvent<>(triagingRequest));
        }else{
            entityUpdatedEventSrc.fire(new EntityUpdatedEvent<>(triagingRequest));
        }
    }
}
