package com.misoft.hospital.rest.patientCare;

import com.misoft.hospital.model.healthcare.BurialPermit;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 8/30/15.
 */
@Stateless
@Path("/burialPermits")
public class BurialPermitResource extends BaseEntityService<BurialPermit>{
    public BurialPermitResource(){
        super(BurialPermit.class);
    }
}
