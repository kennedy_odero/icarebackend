package com.misoft.hospital.rest.patientCare;

import com.misoft.hospital.model.healthcare.ConsultationType;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 9/15/15.
 */
@Stateless
@Path("/consultationTypes")
public class ConsultationTypeResource extends BaseEntityService<ConsultationType>{
    public ConsultationTypeResource(){
        super(ConsultationType.class);
    }
}
