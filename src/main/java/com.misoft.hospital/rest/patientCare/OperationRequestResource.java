package com.misoft.hospital.rest.patientCare;

/**
 * Created by kodero on 3/20/15.
 */

import com.misoft.hospital.model.billing.BillItem;
import com.misoft.hospital.model.billing.BillStatus;
import com.misoft.hospital.model.billing.BillType;
import com.misoft.hospital.model.billing.PatientBill;
import com.misoft.hospital.model.healthcare.LabRequest;
import com.misoft.hospital.model.healthcare.OperationRequest;
import com.misoft.hospital.model.healthcare.RequestMedicalProcedure;
import com.misoft.hospital.model.setup.Product;
import com.misoft.hospital.rest.util.SequenceManager;
import com.misoft.hospital.service.healthcare.ServiceRequestService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.validation.Validation;
import javax.validation.ValidationException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Stateless
@Path("/operationRequests")
public class OperationRequestResource extends ServiceRequestResource<OperationRequest>{
    @Inject
    private SequenceManager seq;

    public OperationRequestResource(){
        setEntityClass(OperationRequest.class);
    }

    @POST
    @Path("/{id}/generateBill")
    public Response generateBill(@PathParam("id") String id){
        OperationRequest req = gService.find(id, OperationRequest.class);
        if(req.getBilled()){
            return Response.status(Response.Status.BAD_REQUEST).entity("Service request (#" + req.getRequestNo() + ") already billed!").build();
        }
        List<RequestMedicalProcedure> items = new ArrayList<>(req.getRequestMedicalProcedures());
        //now generate bill
        PatientBill patientBill = new PatientBill();
        patientBill.setPatient(req.getPatient());
        patientBill.setBillNo(seq.nextSequence("PATIENT_BILL"));
        patientBill.setServiceRequest(req);
        patientBill.setDateGenerated(new Date());
        patientBill.setDueDate(new Date());
        patientBill.setBillType(BillType.Cash);
        patientBill.setAmountWaived(BigDecimal.ZERO);
        patientBill.setBillStatus(BillStatus.APPROVED);

        BigDecimal total = BigDecimal.ZERO;
        BigDecimal tax = BigDecimal.ZERO;
        for(RequestMedicalProcedure item : items){
            Product product = item.getMedicalProcedure().getProduct();
            BillItem billItem = new BillItem();
            billItem.setProduct(product);
            billItem.setQuantity(BigDecimal.ONE);
            billItem.setUnitPrice(product.getListPrice());
            billItem.setLineTotal(product.getListPrice().multiply(billItem.getQuantity()));
            billItem.setTax(BigDecimal.ZERO);
            total = total.add(billItem.getLineTotal());
            tax = tax.add(billItem.getTax());
            patientBill.addBillItem(billItem);
        }
        patientBill.setAmountBilled(total);
        patientBill.setAggregateTax(tax);
        try{
            patientBill = gService.makePersistent(patientBill);
            req.setBilled(true);
            gService.edit(req);
            String[] receivableFields = {"id", "amountBilled"};
            return Response.ok().entity(toDTO(patientBill, receivableFields, PatientBill.class)).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();
        }
    }
}
