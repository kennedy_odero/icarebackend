package com.misoft.hospital.rest.patientCare;

import com.misoft.hospital.data.gdto.GenericDTO;
import com.misoft.hospital.model.healthcare.*;
import com.misoft.hospital.rest.BaseEntityService;
import com.misoft.hospital.rest.util.SequenceManager;
import com.misoft.hospital.service.grant.GService;
import com.misoft.hospital.util.types.DateParam;
import com.misoft.hospital.validators.AppointmentValidator;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.validation.Valid;
import javax.validation.ValidationException;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by kodero on 3/15/15.
 */
@Stateless
@Path("/appointments")
public class AppointmentResource extends BaseEntityService<Appointment> {

    @Inject
    private SequenceManager seq;

    @Inject
    EntityManager em;

    /*@Inject
    private GService*/

    @Inject
    private AppointmentValidator appointmentValidator;

    public AppointmentResource(){
        super(Appointment.class);
    }

    @POST
    @Path("/cancel/{id}")
    public Response cancelAppointment(@PathParam("id") String id) {
        Appointment appointment = gService.find(id, Appointment.class);
        if (appointment == null) return Response.status(404).build();
        try {
            appointmentValidator.validateAppointmentCancellation(appointment);
            return Response.ok().build();
        } catch (ValidationException ve) {
            return createValidationResponse(ve).build();
        }
    }

    @POST
    @Path("/close/{id}")
    public Response closeAppointment(@PathParam("id") String id) {
        Appointment appointment = gService.find(id, Appointment.class);
        if (appointment == null) return Response.status(404).build();
        try {
            appointmentValidator.validateAppointmentClose(appointment);
            appointmentValidator.validateAppointmentClose(appointment);
            return Response.ok().build();
        } catch (ValidationException ve) {
            return createValidationResponse(ve).build();
        }
    }

    @POST
    @Path("/{appointmentId}/createTriagingRequest")
    public Response createTriagingRequest(@PathParam("appointmentId") String appointmentId){
        Appointment appointment = gService.find(appointmentId, Appointment.class);
        //create triaging
        TriagingRequest tr = new TriagingRequest();
        tr.setRequestNo(seq.nextSequence("SERVICE_REQUEST"));
        tr.setAppointment(appointment);
        tr.setPatient(appointment.getPatient());
        tr.setDateRequested(new Date());
        tr.setServiceType("TRIAGING");
        tr.setRequestStatus(RequestStatus.NEW);
        tr.setRequester(userService.currentUser().getStaff());
        //associate all the clinical indications with the triage
        List<ClinicalIndication> indicators = getEntityManager().createQuery("select new com.misoft.hospital.model.healthcare.ClinicalIndication(ci.id) from ClinicalIndication ci ").getResultList();
        for(ClinicalIndication ci : indicators){
            PatientClinicalIndication pci = new PatientClinicalIndication(ci);
            tr.addPatientClinicalIndication(pci);
        }
        try{
            //validateEntity(tr);
            gService.makePersistent(tr);
            return Response.ok().build();
        }catch (Exception e){
            e.printStackTrace();
            return Response.status(500).build();
        }
    }

    @GET
    @Path("/dailyWorkload")
    public Response getDailyWorload(@QueryParam("from") @DefaultValue("today") DateParam dateFrom,
                                       @QueryParam("to") @DefaultValue("today") DateParam dateTo){
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String sql = "select a.appt_date, " +
                "   sum(case p.gender when 'MALE' then 1 else 0 end) m, " +
                "   sum(case p.gender when 'FEMALE' then 1 else 0 end) f," +
                "   count(a.id) total " +
                "from appointments a " +
                "   inner join parties p " +
                "       on a.patient = p.id " +
                "where a.deleted <> 1 and a.appt_date group by a.appt_date";
        List<Object> results = em.createNativeQuery(sql)
                .getResultList();
        List<GenericDTO> balances = new ArrayList<>();
        for(Object o : results){
            Object[] row = (Object[]) o;
            GenericDTO genericDTO = new GenericDTO("com.misoft.reports.DailyWorkload");
            genericDTO.addDate("date", (Date) row[0]);
            genericDTO.addBigDecimal("male", (BigDecimal) row[1]);
            genericDTO.addBigDecimal("female", (BigDecimal) row[2]);
            genericDTO.addBigInteger("total", (BigInteger) row[3]);
            balances.add(genericDTO);
        }
        return Response.ok(balances).build();
    }
}
