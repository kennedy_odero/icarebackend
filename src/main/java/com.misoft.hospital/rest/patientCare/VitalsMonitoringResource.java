package com.misoft.hospital.rest.patientCare;

import com.misoft.hospital.model.healthcare.VitalMonitoring;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 4/24/15.
 */
@Stateless
@Path("/vitalsMonitoring")
public class VitalsMonitoringResource extends BaseEntityService<VitalMonitoring>{
    public VitalsMonitoringResource(){
        super(VitalMonitoring.class);
    }
}
