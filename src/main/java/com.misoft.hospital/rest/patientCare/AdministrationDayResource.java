package com.misoft.hospital.rest.patientCare;

import com.misoft.hospital.model.healthcare.AdministrationDay;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 4/19/15.
 */
@Stateless
@Path("/administrationDays")
public class AdministrationDayResource extends BaseEntityService<AdministrationDay>{
    public AdministrationDayResource(){
        super(AdministrationDay.class);
    }
}
