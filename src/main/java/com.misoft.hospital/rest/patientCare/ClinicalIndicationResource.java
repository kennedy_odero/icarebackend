package com.misoft.hospital.rest.patientCare;

import com.misoft.hospital.model.healthcare.ClinicalIndication;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 3/16/15.
 */
@Stateless
@Path("/clinicalIndications")
public class ClinicalIndicationResource extends BaseEntityService<ClinicalIndication> {
    public ClinicalIndicationResource(){
        super(ClinicalIndication.class);
    }
}
