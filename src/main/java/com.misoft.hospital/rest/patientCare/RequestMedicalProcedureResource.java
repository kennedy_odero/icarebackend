package com.misoft.hospital.rest.patientCare;

import com.misoft.hospital.model.healthcare.RequestMedicalProcedure;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 6/7/16.
 */
@Stateless
@Path("/requestMedicalProcedures")
public class RequestMedicalProcedureResource extends BaseEntityService<RequestMedicalProcedure>{
    public RequestMedicalProcedureResource(){
        super(RequestMedicalProcedure.class);
    }
}
