package com.misoft.hospital.rest.patientCare;

import com.misoft.hospital.model.healthcare.PrescriptionAdministration;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 4/19/15.
 */
@Stateless
@Path("/prescriptionAdministrations")
public class PrescriptionAdministrationResource extends BaseEntityService<PrescriptionAdministration>{
    public PrescriptionAdministrationResource(){
        super(PrescriptionAdministration.class);
    }
}
