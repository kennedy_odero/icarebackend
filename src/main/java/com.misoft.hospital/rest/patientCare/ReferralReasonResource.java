package com.misoft.hospital.rest.patientCare;

import com.misoft.hospital.model.healthcare.ReferralReason;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 4/19/15.
 */
@Stateless
@Path("/referralReasons")
public class ReferralReasonResource extends BaseEntityService<ReferralReason> {
    public ReferralReasonResource(){
        super(ReferralReason.class);
    }
}
