package com.misoft.hospital.rest.patientCare;

import com.misoft.hospital.model.healthcare.WardRoundTeam;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 4/2/15.
 */
@Stateless
@Path("/roundingTeams")
public class WardRoundTeamResource extends BaseEntityService<WardRoundTeam> {
    public WardRoundTeamResource(){
        super(WardRoundTeam.class);
    }
}
