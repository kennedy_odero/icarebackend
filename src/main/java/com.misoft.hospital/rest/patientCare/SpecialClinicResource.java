package com.misoft.hospital.rest.patientCare;

import com.misoft.hospital.model.healthcare.SpecialClinic;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 9/11/15.
 */
@Stateless
@Path("/specialClinics")
public class SpecialClinicResource extends BaseEntityService<SpecialClinic>{
    public SpecialClinicResource(){
        super(SpecialClinic.class);
    }
}
