package com.misoft.hospital.rest.patientCare;

import com.misoft.hospital.model.healthcare.APGAROption;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 8/18/15.
 */
@Stateless
@Path("/apgarOptions")
public class APGAROptionResource extends BaseEntityService<APGAROption>{
    public APGAROptionResource(){
        super(APGAROption.class);
    }
}
