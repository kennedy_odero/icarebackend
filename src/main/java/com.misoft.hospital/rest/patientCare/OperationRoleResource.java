package com.misoft.hospital.rest.patientCare;

import com.misoft.hospital.model.healthcare.OperationRole;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 4/26/15.
 */
@Stateless
@Path("/operationRoles")
public class OperationRoleResource extends BaseEntityService<OperationRole>{
    public OperationRoleResource(){
        super(OperationRole.class);
    }
}
