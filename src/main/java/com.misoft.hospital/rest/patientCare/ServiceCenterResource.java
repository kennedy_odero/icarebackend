package com.misoft.hospital.rest.patientCare;

import com.misoft.hospital.model.healthcare.ServiceCenter;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 3/15/15.
 */
@Stateless
@Path("/serviceCenters")
public class ServiceCenterResource extends BaseEntityService<ServiceCenter> {
    public ServiceCenterResource(){
        super(ServiceCenter.class);
    }
}
