package com.misoft.hospital.rest.patientCare;

import com.misoft.hospital.model.healthcare.SystemProblem;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 3/14/16.
 */
@Stateless
@Path("/systemProblems")
public class SystemProblemResource extends BaseEntityService<SystemProblem>{
    public SystemProblemResource(){
        super(SystemProblem.class);
    }
}
