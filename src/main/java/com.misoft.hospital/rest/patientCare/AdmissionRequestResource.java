package com.misoft.hospital.rest.patientCare;

import com.misoft.hospital.model.billing.BillItem;
import com.misoft.hospital.model.billing.BillStatus;
import com.misoft.hospital.model.billing.BillType;
import com.misoft.hospital.model.billing.PatientBill;
import com.misoft.hospital.model.events.AdmissionBedStart;
import com.misoft.hospital.model.events.AdmissionBedStop;
import com.misoft.hospital.model.healthcare.AdmissionBed;
import com.misoft.hospital.model.healthcare.AdmissionRequest;
import com.misoft.hospital.model.healthcare.DischargeType;
import com.misoft.hospital.model.healthcare.RequestStatus;
import com.misoft.hospital.model.setup.Product;
import com.misoft.hospital.rest.util.SequenceManager;
import com.misoft.hospital.util.types.DateParam;
import org.joda.time.DateTime;
import org.joda.time.Days;

import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.validation.ValidationException;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by kodero on 3/20/15.
 */
@Stateless
@Path("/admissionRequests")
public class AdmissionRequestResource extends ServiceRequestResource<AdmissionRequest>{
    @Inject
    private SequenceManager seq;

    @Inject
    Event<AdmissionBedStart> admissionBedStartEvent;

    @Inject
    Event<AdmissionBedStop> admissionBedStopEvent;

    public AdmissionRequestResource(){
        setEntityClass(AdmissionRequest.class);
    }

    @Override
    public void beforeSaveOrUpdate(AdmissionRequest admissionRequest){
        List<AdmissionBed> admissionBeds = admissionRequest.getAdmissionBeds();

        if((admissionRequest.getRequestStatus() == RequestStatus.PROCESSING)){
            //check if a bed has been assigned
            if(admissionRequest.getDateFrom() == null) throw new ValidationException("Please select the date of admission!");
            if(admissionRequest.getCurrentBed() == null) throw new ValidationException("Please assign a bed to the patient!");
        }
        if((admissionRequest.getRequestStatus() == RequestStatus.COMPLETE)){
            //check if a bed has been assigned
            if(admissionRequest.getDateTo() == null) throw new ValidationException("Please select the date of discharge!");
        }
        /*Bed currentBed = admissionRequest.getCurrentBed();
        if(admissionBeds.size() == 0 ) {
            if(currentBed != null){
                AdmissionBed admissionBed = new AdmissionBed(currentBed, currentBed.getWard(), admissionRequest.getDateFrom());
                admissionRequest.addAdmissionBed(admissionBed);
                admissionBedStartEvent.fire(new AdmissionBedStart(admissionBed));
            }
        }else{
            //check if already added
            log.info("============Current Bed : " + currentBed.getBedNo());
            for(AdmissionBed b : admissionBeds){
                log.info("============Bed : " + b.getBed().getBedNo());
            }
            if(admissionBeds.get(admissionBeds.size() - 1).getBed().getId() != currentBed.getId()){
                //first update previous beds to end their tenancy
                for(AdmissionBed bed : admissionRequest.getAdmissionBeds()){
                    //update dateTo
                    if(bed.getDateTo() == null) {
                        bed.setDateTo(new DateTime().withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0).toDate());//only update the beds that have not been updated
                        admissionBedStopEvent.fire(new AdmissionBedStop(bed));
                    }
                }
                AdmissionBed admissionBed = new AdmissionBed(currentBed, currentBed.getWard(), new Date());
                admissionRequest.addAdmissionBed(admissionBed);
                admissionBedStartEvent.fire(new AdmissionBedStart(admissionBed));
            }
        }
        //update the bed status
        if(currentBed != null){
            currentBed.setBedStatus(BedStatus.OCCUPIED);
            gService.edit(currentBed);
        }*/
    }

    @POST
    @Path("/{id}/discharge")
    public Response discharge(@PathParam("id") String id, @QueryParam("dateTo") @DefaultValue("today") DateParam dateParam){
        log.info("Date to : " + dateParam.getDate());
        AdmissionRequest admissionRequest = gService.find(id, AdmissionRequest.class);
        admissionRequest.setDischargeType(DischargeType.NORMAL);
        admissionRequest.setDateTo(dateParam.getDate());
        //set date for discharge
        for(AdmissionBed bed : admissionRequest.getAdmissionBeds()){
            //update dateTo
            if(bed.getDateTo() == null) {
                bed.setDateTo(new DateTime(dateParam.getDate()).withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0).toDate());//only update the beds that have not been updated
                admissionBedStopEvent.fire(new AdmissionBedStop(bed));
            }
        }
        return Response.ok().build();
    }

    @POST
    @Path("/{id}/generateBill")
    public Response generateBill(@PathParam("id") String id){
        AdmissionRequest req = gService.find(id, AdmissionRequest.class);
        if(req.getBilled()){
            return Response.status(Response.Status.BAD_REQUEST).entity("Service request (#" + req.getRequestNo() + ") already billed!").build();
        }
        List<AdmissionBed> items = new ArrayList<>(req.getAdmissionBeds());
        //now generate bill
        PatientBill patientBill = new PatientBill();
        patientBill.setPatient(req.getPatient());
        patientBill.setBillNo(seq.nextSequence("PATIENT_BILL"));
        patientBill.setServiceRequest(req);
        patientBill.setDateGenerated(new Date());
        patientBill.setDueDate(new Date());
        patientBill.setBillType(BillType.Cash);
        patientBill.setAmountWaived(BigDecimal.ZERO);
        patientBill.setBillStatus(BillStatus.APPROVED);

        BigDecimal total = BigDecimal.ZERO;
        BigDecimal tax = BigDecimal.ZERO;
        for(AdmissionBed item : items){
            Product product = item.getBed().getProduct();
            BillItem billItem = new BillItem();
            billItem.setProduct(product);
            DateTime from = new DateTime(item.getDateFrom());
            DateTime to = new DateTime(item.getDateTo());
            int duration = Days.daysBetween(from, to).getDays();
            billItem.setQuantity(new BigDecimal(duration));
            billItem.setUnitPrice(product.getListPrice());
            billItem.setLineTotal(product.getListPrice().multiply(billItem.getQuantity()));
            billItem.setTax(BigDecimal.ZERO);
            total = total.add(billItem.getLineTotal());
            tax = tax.add(billItem.getTax());
            patientBill.addBillItem(billItem);
        }
        patientBill.setAmountBilled(total);
        patientBill.setAggregateTax(tax);
        try{
            patientBill = gService.makePersistent(patientBill);
            req.setBilled(true);
            gService.edit(req);
            String[] receivableFields = {"id", "amountBilled"};
            return Response.ok().entity(toDTO(patientBill, receivableFields, PatientBill.class)).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();
        }
    }
}
