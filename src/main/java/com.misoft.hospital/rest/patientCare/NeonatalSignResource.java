package com.misoft.hospital.rest.patientCare;

import com.misoft.hospital.model.healthcare.NeonatalSign;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 8/18/15.
 */
@Stateless
@Path("/neonatalSigns")
public class NeonatalSignResource extends BaseEntityService<NeonatalSign>{
    public NeonatalSignResource(){
        super(NeonatalSign.class);
    }
}
