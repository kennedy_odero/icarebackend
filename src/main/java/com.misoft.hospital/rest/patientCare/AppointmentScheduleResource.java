package com.misoft.hospital.rest.patientCare;

import com.misoft.hospital.model.healthcare.AppointmentSchedule;
import com.misoft.hospital.model.healthcare.ScheduleDay;
import com.misoft.hospital.model.healthcare.ScheduleRun;
import com.misoft.hospital.rest.BaseEntityService;
import org.joda.time.DateTime;

import javax.ejb.Stateless;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by kodero on 9/11/15.
 */
@Stateless
@Path("/appointmentSchedules")
public class AppointmentScheduleResource extends BaseEntityService<AppointmentSchedule>{
    public AppointmentScheduleResource(){
        super(AppointmentSchedule.class);
    }

    @POST
    @Path("/{id}/createScheduleRuns")
    public Response createScheduleRuns(@PathParam("id") String id){
        AppointmentSchedule schedule = gService.find(id, AppointmentSchedule.class);
        if(schedule == null) return Response.status(404).build();
        //get end of year
        DateTime start = new DateTime("2015-01-01");
        DateTime end = new DateTime("2015-12-31");
        List<DateTime> runDates = new ArrayList<>();
        for(ScheduleDay sd : schedule.getScheduleDays()){
            if(sd.getEnabled()){
                //for each schedule day, start some where un till you reach end of year
                DateTime dt1 = start.withDayOfWeek(sd.getDayOfWeek().ordinal() + 1);
                ///DateTime runDate = dt1;
                runDates.add(dt1);
                while (!dt1.isAfter(end)){
                    DateTime runDate = dt1.plusDays(7);
                    runDates.add(runDate);
                    dt1 = runDate;
                }
                log.info("Run-dates : " + runDates);
            }
        }
        for (DateTime dt : runDates){
            log.info("Datetime : " + dt);
            ScheduleRun sr = new ScheduleRun(schedule.getClinic(), dt.toDate());
            try{
                gService.makePersistent(sr);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return Response.ok().build();
    }
}
