package com.misoft.hospital.rest.patientCare;

import com.misoft.hospital.model.healthcare.OperationChecklist;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 4/26/15.
 */
@Stateless
@Path("/operationChecklists")
public class OperationChecklistResource extends BaseEntityService<OperationChecklist> {
    public OperationChecklistResource(){
        super(OperationChecklist.class);
    }
}
