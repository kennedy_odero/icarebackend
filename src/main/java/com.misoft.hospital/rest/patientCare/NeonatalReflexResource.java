package com.misoft.hospital.rest.patientCare;

import com.misoft.hospital.model.healthcare.NeonatalReflex;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 8/18/15.
 */
@Stateless
@Path("/neonatalReflexes")
public class NeonatalReflexResource extends BaseEntityService<NeonatalReflex>{
    public NeonatalReflexResource(){
        super(NeonatalReflex.class);
    }
}
