package com.misoft.hospital.rest.patientCare;

import com.misoft.hospital.model.billing.BillItem;
import com.misoft.hospital.model.billing.BillStatus;
import com.misoft.hospital.model.billing.BillType;
import com.misoft.hospital.model.billing.PatientBill;
import com.misoft.hospital.model.healthcare.*;
import com.misoft.hospital.model.setup.Product;
import com.misoft.hospital.rest.util.SequenceManager;
import com.misoft.hospital.service.grant.GService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.validation.ValidationException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by kodero on 3/20/15.
 */
@Stateless
@Path("/labRequests")
public class LabRequestResource extends ServiceRequestResource<LabRequest> {
    @Inject
    private SequenceManager seq;

    @Inject
    GService gService;

    public LabRequestResource(){
        setEntityClass(LabRequest.class);
    }

    public void extraValidations(LabRequest labRequest){
        //call super class validation first
        super.extraValidations(labRequest);
        //perform any other validation
        if(labRequest.getLabRequestItems().size() == 0){
            throw new ValidationException("Please add lab request items!");
        }
    }

    @POST
    @Path("/{id}/generateBill")
    public Response generateBill(@PathParam("id") String id){
        LabRequest req = gService.find(id, LabRequest.class);
        if(req.getBilled()){
            return Response.status(Response.Status.BAD_REQUEST).entity("Service request (#" + req.getRequestNo() + ") already billed!").build();
        }
        List<LabRequestItem> items = new ArrayList<>(req.getLabRequestItems());
        //now generate bill
        PatientBill patientBill = new PatientBill();
        patientBill.setPatient(req.getPatient());
        patientBill.setBillNo(seq.nextSequence("PATIENT_BILL"));
        patientBill.setServiceRequest(req);
        patientBill.setDateGenerated(new Date());
        patientBill.setDueDate(new Date());
        patientBill.setBillType(BillType.Cash);
        patientBill.setAmountWaived(BigDecimal.ZERO);
        patientBill.setBillStatus(BillStatus.APPROVED);

        BigDecimal total = BigDecimal.ZERO;
        BigDecimal tax = BigDecimal.ZERO;
        for(LabRequestItem item : items){
            Product product = item.getLabTestType().getProduct();
            BillItem billItem = new BillItem();
            billItem.setProduct(product);
            billItem.setQuantity(BigDecimal.ONE);
            billItem.setUnitPrice(product.getListPrice());
            billItem.setLineTotal(product.getListPrice().multiply(billItem.getQuantity()));
            billItem.setTax(BigDecimal.ZERO);
            total = total.add(billItem.getLineTotal());
            tax = tax.add(billItem.getTax());
            patientBill.addBillItem(billItem);
        }
        patientBill.setAmountBilled(total);
        patientBill.setAggregateTax(tax);
        try{
            patientBill = gService.makePersistent(patientBill);
            gService.edit(req);
            String[] receivableFields = {"id", "amountBilled"};
            return Response.ok().entity(toDTO(patientBill, receivableFields, PatientBill.class)).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();
        }
    }
}
