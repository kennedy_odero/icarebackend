package com.misoft.hospital.rest.patientCare;

import com.misoft.hospital.model.healthcare.LabUnit;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 8/31/15.
 */
@Stateless
@Path("/labUnits")
public class LabUnitResource extends BaseEntityService<LabUnit>{
    public LabUnitResource(){
        super(LabUnit.class);
    }
}
