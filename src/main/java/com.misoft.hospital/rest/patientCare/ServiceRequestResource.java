
package com.misoft.hospital.rest.patientCare;

import com.misoft.hospital.model.healthcare.Appointment;
import com.misoft.hospital.model.healthcare.ServiceRequest;
import com.misoft.hospital.model.healthcare.Status;
import com.misoft.hospital.rest.BaseEntityService;
import com.misoft.hospital.service.healthcare.ServiceRequestService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.validation.ValidationException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import java.util.logging.Level;

/**
 * Created by kodero on 12/10/15.
 */

@Stateless
public class ServiceRequestResource<T extends ServiceRequest> extends BaseEntityService<T>{

    @Inject
    private ServiceRequestService requestService;

    @Override
    public void beforeSaveOrUpdate(T entity){
        //implemented in child classes
    }

    @Override
    public void extraValidations(T entity){
        /*// do not create any request, where the appointment is closed
        Appointment app = entity.getAppointment();
        if(app.getStatus() != null && app.getStatus() == Status.CLOSED){
            throw new ValidationException("Appointment is already closed!");
        }*/
    }

    @POST
    @Path("/{id:[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}}/submit")
    public Response submit(@PathParam("id") String id){
        T sr = gService.find(id, entityClass);
        if(sr == null) {
            log.log(Level.SEVERE, "No ServiceRequest with id {" + id + "} given!");
            return Response.status(404).build();
        }
        try{
            requestService.submitRequest(sr);
        }catch (ValidationException vex){
            vex.printStackTrace();
            return createValidationResponse(vex).build();
        }
        entityUpdatedEventSrc.fire(new EntityUpdatedEvent<>(sr));
        return Response.ok().build();
    }

    @POST
    @Path("/{id:[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}}/receive")
    public Response receive(@PathParam("id") String id){
        T sr = gService.find(id, entityClass);
        if(sr == null) {
            log.log(Level.SEVERE, "No ServiceRequest with id {" + id + "} given!");
            return Response.status(404).build();
        }
        try{
            requestService.receiveRequest(sr);
        }catch (ValidationException vex){
            vex.printStackTrace();
            return createValidationResponse(vex).build();
        }
        entityUpdatedEventSrc.fire(new EntityUpdatedEvent<>(sr));
        return Response.ok().build();
    }

    @POST
    @Path("/{id:[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}}/cancel")
    public Response cancel(@PathParam("id") String id){
        T sr = gService.find(id, entityClass);
        if(sr == null) {
            log.log(Level.SEVERE, "No ServiceRequest with id {" + id + "} given!");
            return Response.status(404).build();
        }
        try{
            requestService.cancelRequest(sr);
        }catch (ValidationException vex){
            vex.printStackTrace();
            return createValidationResponse(vex).build();
        }
        entityUpdatedEventSrc.fire(new EntityUpdatedEvent<>(sr));
        return Response.ok().build();
    }

    @POST
    @Path("/{id:[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}}/hold")
    public Response hold(@PathParam("id") String id){
        T sr = gService.find(id, entityClass);
        if(sr == null) {
            log.log(Level.SEVERE, "No ServiceRequest with id {" + id + "} given!");
            return Response.status(404).build();
        }
        try{
            requestService.holdRequest(sr);
        }catch (ValidationException vex){
            vex.printStackTrace();
            return createValidationResponse(vex).build();
        }
        entityUpdatedEventSrc.fire(new EntityUpdatedEvent<>(sr));
        return Response.ok().build();
    }

    @POST
    @Path("/{id:[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}}/recall")
    public Response recall(@PathParam("id") String id){
        T sr = gService.find(id, entityClass);
        if(sr == null) {
            log.log(Level.SEVERE, "No ServiceRequest with id {" + id + "} given!");
            return Response.status(404).build();
        }
        try{
            requestService.recallRequest(sr);
        }catch (ValidationException vex){
            vex.printStackTrace();
            return createValidationResponse(vex).build();
        }
        entityUpdatedEventSrc.fire(new EntityUpdatedEvent<>(sr));
        return Response.ok().build();
    }

    @POST
    @Path("/{id:[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}}/_return")
    public Response _return(@PathParam("id") String id){
        T sr = gService.find(id, entityClass);
        if(sr == null) {
            log.log(Level.SEVERE, "No ServiceRequest with id {" + id + "} given!");
            return Response.status(404).build();
        }
        try{
            requestService.returnRequest(sr);
        }catch (ValidationException vex){
            vex.printStackTrace();
            return createValidationResponse(vex).build();
        }
        entityUpdatedEventSrc.fire(new EntityUpdatedEvent<>(sr));
        return Response.ok().build();
    }
}

