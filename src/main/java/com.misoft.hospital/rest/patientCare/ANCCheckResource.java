package com.misoft.hospital.rest.patientCare;

import com.misoft.hospital.model.healthcare.ANCCheck;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 10/28/15.
 */
@Stateless
@Path("/ancChecks")
public class ANCCheckResource extends BaseEntityService<ANCCheck>{
    public ANCCheckResource(){
        super(ANCCheck.class);
    }
}