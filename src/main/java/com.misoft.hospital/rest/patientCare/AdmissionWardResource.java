package com.misoft.hospital.rest.patientCare;

import com.misoft.hospital.model.healthcare.AdmissionWard;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 4/5/15.
 */
@Stateless
@Path("/admissionWards")
public class AdmissionWardResource extends BaseEntityService<AdmissionWard> {
    public AdmissionWardResource(){
        super(AdmissionWard.class);
    }
}
