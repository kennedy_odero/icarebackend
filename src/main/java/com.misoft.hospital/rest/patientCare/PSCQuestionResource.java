package com.misoft.hospital.rest.patientCare;

import com.misoft.hospital.model.healthcare.PSCQuestion;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 8/18/15.
 */
@Stateless
@Path("/pscQuestions")
public class PSCQuestionResource extends BaseEntityService<PSCQuestion>{
    public PSCQuestionResource(){
        super(PSCQuestion.class);
    }
}
