package com.misoft.hospital.rest.patientCare;

import com.misoft.hospital.model.healthcare.ImagingRequestItem;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 5/21/16.
 */
@Stateless
@Path("/imagingRequestItems")
public class ImagingRequestItemResource extends BaseEntityService<ImagingRequestItem>{
    public ImagingRequestItemResource(){
        super(ImagingRequestItem.class);
    }
}
