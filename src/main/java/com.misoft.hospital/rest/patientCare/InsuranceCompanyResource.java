package com.misoft.hospital.rest.patientCare;

import com.misoft.hospital.model.healthcare.InsuranceCompany;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 12/13/15.
 */
@Stateless
@Path("/insuranceCompanies")
public class InsuranceCompanyResource extends BaseEntityService<InsuranceCompany>{
    public InsuranceCompanyResource(){
        super(InsuranceCompany.class);
    }
}
