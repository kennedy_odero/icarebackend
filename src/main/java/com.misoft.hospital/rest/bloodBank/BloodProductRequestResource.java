package com.misoft.hospital.rest.bloodBank;

import com.misoft.hospital.model.bloodBank.BloodProductRequest;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 9/9/15.
 */
@Stateless
@Path("/bloodRequests")
public class BloodProductRequestResource extends BaseEntityService<BloodProductRequest>{
    public BloodProductRequestResource(){
        super(BloodProductRequest.class);
    }
}
