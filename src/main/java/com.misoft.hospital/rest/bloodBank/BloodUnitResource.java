package com.misoft.hospital.rest.bloodBank;

import com.misoft.hospital.model.bloodBank.BloodUnit;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 4/12/15.
 */
@Stateless
@Path("/bloodUnits")
public class BloodUnitResource extends BaseEntityService<BloodUnit> {
    public BloodUnitResource(){
        super(BloodUnit.class);
    }
}
