package com.misoft.hospital.rest.bloodBank;

import com.misoft.hospital.model.bloodBank.Rhesus;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 4/12/15.
 */
@Stateless
@Path("/rhesus")
public class RhesusResource extends BaseEntityService<Rhesus> {
    public RhesusResource(){
        super(Rhesus.class);
    }
}
