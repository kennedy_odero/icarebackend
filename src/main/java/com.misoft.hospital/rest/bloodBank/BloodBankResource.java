package com.misoft.hospital.rest.bloodBank;

import com.misoft.hospital.model.bloodBank.BloodBank;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 9/9/15.
 */
@Stateless
@Path("/bloodBanks")
public class BloodBankResource extends BaseEntityService<BloodBank>{
    public BloodBankResource(){
        super(BloodBank.class);
    }
}
