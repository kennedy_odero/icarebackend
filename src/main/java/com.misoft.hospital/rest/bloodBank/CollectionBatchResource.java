package com.misoft.hospital.rest.bloodBank;

import com.misoft.hospital.model.bloodBank.CollectionBatch;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 9/8/15.
 */
@Stateless
@Path("/collectionBatches")
public class CollectionBatchResource extends BaseEntityService<CollectionBatch>{
    public CollectionBatchResource(){
        super(CollectionBatch.class);
    }
}
