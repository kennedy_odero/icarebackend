package com.misoft.hospital.rest.bloodBank;

import com.misoft.hospital.model.bloodBank.BloodProduct;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 4/12/15.
 */
@Stateless
@Path("/bloodProducts")
public class BloodProductResource extends BaseEntityService<BloodProduct> {
    public BloodProductResource(){
        super(BloodProduct.class);
    }
}
