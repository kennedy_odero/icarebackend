package com.misoft.hospital.rest.bloodBank;

import com.misoft.hospital.model.bloodBank.ScreeningTest;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 9/9/15.
 */
@Stateless
@Path("/screeningTests")
public class ScreeningTestResource extends BaseEntityService<ScreeningTest>{
    public ScreeningTestResource(){
        super(ScreeningTest.class);
    }
}
