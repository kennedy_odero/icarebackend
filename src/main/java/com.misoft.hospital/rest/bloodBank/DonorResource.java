package com.misoft.hospital.rest.bloodBank;

import com.misoft.hospital.model.bloodBank.Donor;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 4/12/15.
 */
@Stateless
@Path("/donors")
public class DonorResource extends BaseEntityService<Donor> {
    public DonorResource(){
        super(Donor.class);
    }
}
