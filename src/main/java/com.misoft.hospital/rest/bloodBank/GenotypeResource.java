package com.misoft.hospital.rest.bloodBank;

import com.misoft.hospital.model.bloodBank.Genotype;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 4/12/15.
 */
@Stateless
@Path("/genotypes")
public class GenotypeResource extends BaseEntityService<Genotype> {
    public GenotypeResource(){
        super(Genotype.class);
    }
}
