package com.misoft.hospital.rest.bloodBank;

import com.misoft.hospital.model.bloodBank.Phenotype;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 4/12/15.
 */
@Stateless
@Path("/phenotypes")
public class PhenotypeResource extends BaseEntityService<Phenotype> {
    public PhenotypeResource(){
        super(Phenotype.class);
    }
}
