package com.misoft.hospital.rest.bloodBank;

import com.misoft.hospital.model.bloodBank.BloodGroup;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 9/8/15.
 */
@Stateless
@Path("/bloodGroups")
public class BloodGroupResource extends BaseEntityService<BloodGroup>{
    public BloodGroupResource(){
        super(BloodGroup.class);
    }
}
