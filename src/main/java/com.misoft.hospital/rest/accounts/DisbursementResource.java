package com.misoft.hospital.rest.accounts;

import com.misoft.hospital.model.accounts.Disbursement;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by larry on 3/4/2015.
 */
@Path("/disbursements")
@Stateless
public class DisbursementResource extends BaseEntityService<Disbursement> {
    public DisbursementResource(){ super(Disbursement.class); }
}
