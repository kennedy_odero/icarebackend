package com.misoft.hospital.rest.accounts;

import com.misoft.hospital.model.accounts.CashierShift;
import com.misoft.hospital.model.accounts.ShiftStatus;
import com.misoft.hospital.rest.BaseEntityService;
import com.misoft.hospital.service.admin.CurrentUserService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.Query;
import javax.validation.ValidationException;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by kodero on 9/30/15.
 */
@Stateless
@Path("/cashierShifts")
public class CashierShiftResource extends BaseEntityService<CashierShift>{

    @Inject
    private CurrentUserService currentUserService;

    public CashierShiftResource(){
        super(CashierShift.class);
    }

    @Override
    public void beforeSaveOrUpdate(CashierShift shift){
        if(shift.getId() == null && shift.getStart() == null){
            shift.setStart(new Date());//opening shift
        }
        if(shift.getStatus() == ShiftStatus.CLOSED){
            shift.setEnd(new Date());
        }
    }

    @GET
    @Path("/currentShift")
    public Response getCurrentShift(){
        log.info("currentUserService.currentUser().getStaff().getId() : " + currentUserService.currentUser().getStaff().getId());
        Query q = getEntityManager().createQuery("select new CashierShift(cs.id, cs.shiftNo) from CashierShift cs where cs.owner.id =:ownerId and cs.status =:status")
                .setParameter("ownerId", currentUserService.currentUser().getStaff().getId())
                .setParameter("status", ShiftStatus.OPEN);
        List<CashierShift> openShifts = q.getResultList();
        if(openShifts.size() == 1){
            try {
                return Response.ok(toDTO(openShifts.get(0), "id,shiftNo".split(","), CashierShift.class)).build();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else {
            log.info("---------------No open shift for this user ------------------");
        }
        return Response.ok().build();
    }

    @POST
    @Path("/{id}/cancel")
    public Response cancel(@PathParam("id") String id){
        CashierShift cs = gService.find(id, CashierShift.class);
        log.info("Shift Value : " + cs.getTotalReceipts());
        if(cs.getStatus() != ShiftStatus.OPEN) return createValidationResponse(new ValidationException("A shift must be in {OPEN} status to be cancelled!")).build();
        if(BigDecimal.ZERO.compareTo(cs.getOpeningValue()) != 0 || BigDecimal.ZERO.compareTo(cs.getTotalReceipts()) != 0) return createValidationResponse(new ValidationException("Error! Shift has a non-zero start value or there are receipts already. Try close instead.")).build();
        cs.setStatus(ShiftStatus.CANCELLED);
        return Response.ok().build();
    }
}
