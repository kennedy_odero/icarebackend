package com.misoft.hospital.rest.accounts;

import com.misoft.hospital.model.accounts.Payable;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by larry on 3/2/2015.
 */
@Path("/payables")
@Stateless
public class PayableResource extends BaseEntityService<Payable> {
    public PayableResource(){
        super(Payable.class);
    }
}
