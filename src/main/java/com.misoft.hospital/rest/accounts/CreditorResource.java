package com.misoft.hospital.rest.accounts;

import com.misoft.hospital.model.accounts.Creditor;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by larry on 3/4/2015.
 */
@Path("/creditors")
@Stateless
public class CreditorResource extends BaseEntityService<Creditor> {
    public CreditorResource(){
        super(Creditor.class);
    }
}
