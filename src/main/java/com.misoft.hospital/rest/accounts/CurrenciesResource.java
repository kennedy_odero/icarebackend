package com.misoft.hospital.rest.accounts;

import com.misoft.hospital.model.accounts.Currency;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by larry on 3/6/2015.
 */
@Path("/currencies")
@Stateless
public class CurrenciesResource extends BaseEntityService<Currency> {
    public CurrenciesResource(){super(Currency.class);}
}
