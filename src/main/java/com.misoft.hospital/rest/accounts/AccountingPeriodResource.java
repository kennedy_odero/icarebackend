package com.misoft.hospital.rest.accounts;

import com.misoft.hospital.model.accounts.AccountingPeriod;
import com.misoft.hospital.model.accounts.PeriodStatus;
import com.misoft.hospital.model.accounts.PeriodType;
import com.misoft.hospital.rest.BaseEntityService;
import com.misoft.hospital.service.accounts.AccountingPeriodService;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Created by larry on 3/23/2015.
 */
@Stateless
@Path("/accountingPeriods")
public class AccountingPeriodResource extends BaseEntityService<AccountingPeriod> {
    private int monthSeq = 1;
    private int quarterSeq = 1;

    @Inject
    private AccountingPeriodService apService;

    @Inject
    EntityManager em;

    public AccountingPeriodResource(){super(AccountingPeriod.class);}

    @POST
    @Path("/createInitialPeriods/{startDate}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createInitialAccountingPeriod(@PathParam("startDate") Date startDate) throws Exception{
        int seq = 1;
        Calendar c = Calendar.getInstance();
        AccountingPeriod ap0 = new AccountingPeriod();
        ap0.setPeriodType(PeriodType.FISCAL_YEAR);
        ap0.setSeq(seq);
        ap0.setFromDate(startDate);
        c.setTime(startDate);
        c.add(Calendar.YEAR, 1);
        c.add(Calendar.DATE, -1);
        ap0.setToDate(c.getTime());
        ap0.setPeriodStatus(PeriodStatus.OPEN);
        //create quarterly
        createHalfYearPeriods(ap0);
        ap0 = gService.persist(ap0);
        monthSeq = 1; quarterSeq = 1; //reset the values
        return Response.ok().build();
    }

    @POST
    @Path("/closePeriod/{currentApId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response closePeriod(@PathParam("currentApId") String currentApId) throws Exception{
        AccountingPeriod currentAp = gService.find(currentApId, AccountingPeriod.class);
        int seq = currentAp.getSeq() + 1;
        Calendar c = Calendar.getInstance();
        c.setTime(currentAp.getToDate());
        c.add(Calendar.DATE, 1);//forward the date to the following year.
        //check if the next period already exists
        if(apService.getPeriod(c.getTime(), currentAp.getPeriodType()) != null){
            //throw new WebApplicationException("The next period already created.");
        }else{
            AccountingPeriod ap0 = new AccountingPeriod();
            ap0.setPeriodType(PeriodType.FISCAL_YEAR);
            ap0.setSeq(seq);
            ap0.setFromDate(c.getTime());
            c.add(Calendar.YEAR, 1);
            c.add(Calendar.DATE, -1);
            ap0.setToDate(c.getTime());
            ap0.setPeriodStatus(PeriodStatus.OPEN);
            //create quarterly
            createHalfYearPeriods(ap0);
            ap0 = gService.persist(ap0);
        }
        currentAp.setPeriodStatus(PeriodStatus.CLOSED);
        monthSeq = 1; quarterSeq = 1; //reset the values
        return Response.ok().build();
    }

    private AccountingPeriod createHalfYearPeriods(AccountingPeriod yearAp) {
        System.out.println("Creating half year periods for : " + yearAp.getToDate() + ", PeriodType : " + yearAp.getPeriodType());
        int seq = 1;
        Date currentStartDate = yearAp.getFromDate();
        Calendar c = Calendar.getInstance(); c.setTime(currentStartDate);
        c.add(Calendar.MONTH, 5);//advance the date by 6 months
        c.set(Calendar.DATE, c.getActualMaximum(Calendar.DATE));
        Date currentEndDate = c.getTime();
        System.out.println("Current start-h: " + currentStartDate + ", end :" + currentEndDate + ", PeriodType : " + PeriodType.FISCAL_HALF);
        while(!currentEndDate.after(yearAp.getToDate())) {
            //System.out.println("Current start: " + currentStartDate + ", end :" + currentEndDate);
            AccountingPeriod acc = new AccountingPeriod();
            acc.setFromDate(currentStartDate);
            acc.setToDate(currentEndDate);
            acc.setPeriodType(PeriodType.FISCAL_HALF);
            acc.setSeq(seq);
            acc.setPeriodStatus(PeriodStatus.OPEN);
            //List<AccountingPeriod> children = getChildrenPeriods(acc, APType.QUARTER);
            createQuarterlyPeriods(acc);
            yearAp.addChild(acc);
            //advance the counter
            seq = seq + 1;
            //advance the dates
            Calendar startCal = GregorianCalendar.getInstance(); startCal.setTime(currentEndDate);
            startCal.add(Calendar.DATE, 1);//advance the calendar by one day
            currentStartDate = startCal.getTime();
            Calendar endCal = GregorianCalendar.getInstance(); endCal.setTime(currentStartDate);
            endCal.add(Calendar.MONTH, 5);//advance the date by 3 months
            endCal.set(Calendar.DATE, endCal.getActualMaximum(Calendar.DATE));
            currentEndDate = endCal.getTime();
        }
        return yearAp;
    }

    private AccountingPeriod createQuarterlyPeriods(AccountingPeriod halfYearAp) {
        System.out.println("Creating quarter year periods starting : " + halfYearAp.getFromDate() + ", to " + halfYearAp.getToDate() + ", PeriodType : " + halfYearAp.getPeriodType());
        int seq1 = 1;
        Date currentStartDate = halfYearAp.getFromDate();
        Calendar c = Calendar.getInstance(); c.clear();
        c.setTime(currentStartDate);
        c.add(Calendar.MONTH, 2);//advance the date by 3 months
        c.set(Calendar.DATE, c.getActualMaximum(Calendar.DATE));
        Date currentEndDate = c.getTime();
        System.out.println("Current start-q: " + currentStartDate + ", end :" + currentEndDate + ", PeriodType : " + PeriodType.FISCAL_QUARTER);
        while(!currentEndDate.after(halfYearAp.getToDate())) {
            //System.out.println("Current start: " + currentStartDate + ", end :" + currentEndDate);
            AccountingPeriod acc = new AccountingPeriod();
            acc.setFromDate(currentStartDate);
            acc.setToDate(currentEndDate);
            acc.setPeriodType(PeriodType.FISCAL_QUARTER);
            acc.setSeq(quarterSeq);
            acc.setPeriodStatus(PeriodStatus.OPEN);
            createMonthlyPeriods(acc);
            halfYearAp.addChild(acc);
            //advance the counter
            quarterSeq = quarterSeq + 1;
            //advance the dates
            Calendar startCal = GregorianCalendar.getInstance(); startCal.setTime(currentEndDate);
            startCal.add(Calendar.DATE, 1);//advance the calendar by one day
            currentStartDate = startCal.getTime();
            Calendar endCal = GregorianCalendar.getInstance(); endCal.setTime(currentStartDate);
            endCal.add(Calendar.MONTH, 2);//advance the date by 3 months
            endCal.set(Calendar.DATE, endCal.getActualMaximum(Calendar.DATE));
            currentEndDate = endCal.getTime();
        }
        return halfYearAp;
    }

    private void createMonthlyPeriods(AccountingPeriod ap1) {
        //int monthSeq1 = 1;
        AccountingPeriod firstMonth = new AccountingPeriod();
        firstMonth.setFromDate(ap1.getFromDate());
        Calendar c = GregorianCalendar.getInstance();
        c.setTime(ap1.getFromDate());
        int lastDate = c.getActualMaximum(Calendar.DATE);
        c.set(Calendar.DATE, lastDate);
        firstMonth.setToDate(c.getTime());
        firstMonth.setPeriodType(PeriodType.FISCAL_MONTH);
        firstMonth.setPeriodStatus(PeriodStatus.OPEN);
        firstMonth.setSeq(monthSeq);
        c.add(Calendar.MONTH, 1);//advance the date by one month
        ap1.addChild(firstMonth);
        //iterate over the next months
        Date lastdate = firstMonth.getToDate();
        while(true) {
            if(lastdate.compareTo(ap1.getToDate()) > 0) {
                break;//just go away
            }
            AccountingPeriod acc = new AccountingPeriod();
            monthSeq = monthSeq + 1;
            Calendar cal = GregorianCalendar.getInstance();
            cal.clear();
            //System.out.println("Cal after clear :" + cal.getTime());
            cal.setTime(lastdate);
            cal.add(Calendar.DATE, 1);//advance to the firstday of the next month
            acc.setFromDate(cal.getTime());
            int lastday = cal.getActualMaximum(Calendar.DATE);
            cal.set(Calendar.DATE, lastday);
            acc.setToDate(cal.getTime());
            acc.setPeriodType(PeriodType.FISCAL_MONTH);
            acc.setSeq(monthSeq);
            acc.setPeriodStatus(PeriodStatus.OPEN);
            lastdate = acc.getToDate();
            if (!acc.getFromDate().after(ap1.getToDate())) {
                System.out.println("Added Month " + acc.getSeq() + "[ " + acc.getFromDate() + ", " + acc.getToDate() + "]");
                ap1.addChild(acc);
            }
            if(acc.getToDate().compareTo(ap1.getToDate()) > 0) {
                acc.setToDate(ap1.getToDate());
                break;
            }
            System.out.println("Month " + acc.getSeq() + "[ " + acc.getFromDate() + ", " + acc.getFromDate() + "]");
        }
    }

    public AccountingPeriod createNextPeriod(AccountingPeriod currentYear) {
        Calendar c = Calendar.getInstance();
        //check if we already have the next year
        Criteria crit = ((Session) em.getDelegate()).createCriteria(AccountingPeriod.class);
        crit.add(Restrictions.eq("APType", PeriodType.FISCAL_YEAR));
        crit.add(Restrictions.eq("seq", currentYear.getSeq() + 1));
        List<AccountingPeriod> existingAp = crit.list();
        if(existingAp.size() > 0) {
            //the next ap already exists
            System.out.println("The ap already exists!!!!");
            return null;
        }
        AccountingPeriod newAp = new AccountingPeriod();
        c.setTime(currentYear.getToDate());
        //add one day to the current date
        c.add(Calendar.DATE, 1);
        newAp.setPeriodType(PeriodType.FISCAL_YEAR);
        newAp.setPeriodStatus(PeriodStatus.OPEN);
        newAp.setFromDate(c.getTime());
        newAp.setSeq(currentYear.getSeq() + 1);
        c.add(Calendar.YEAR, 1);
        c.add(Calendar.DATE, -1);//reduce by one day  to get the end of the year
        newAp.setToDate(c.getTime());
        System.out.println("Year starts on " + newAp.getFromDate() + ", ends on " + newAp.getToDate());
        //now create monthly periods
        createMonthlyPeriods(newAp);
        return newAp;
    }

    public List<AccountingPeriod> getChildrenPeriods(AccountingPeriod quarter, PeriodType type){
        //get the accounting periods between these dates
        Criteria crit = ((Session)em.getDelegate()) .createCriteria(AccountingPeriod.class);
        crit.add(Restrictions.between("fromDate", quarter.getFromDate(), quarter.getToDate()));
        crit.add(Restrictions.eq("type", type));
        return crit.list();
    }
}
