package com.misoft.hospital.rest.accounts;

import com.misoft.hospital.model.accounts.ExpenditureBudgetItem;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by larry on 3/22/2015.
 */
@Path("/expenditureBudgetItem")
@Stateless
public class ExpenditureBudgetItemResource extends BaseEntityService<ExpenditureBudgetItem> {
    public ExpenditureBudgetItemResource(){super(ExpenditureBudgetItem.class);}
}
