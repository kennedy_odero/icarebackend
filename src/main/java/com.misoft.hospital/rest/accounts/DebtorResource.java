package com.misoft.hospital.rest.accounts;

import com.misoft.hospital.model.accounts.Debtor;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by larry on 3/4/2015.
 */
@Path("/debtors")
@Stateless
public class DebtorResource extends BaseEntityService<Debtor> {
    public DebtorResource(){
        super(Debtor.class);
    }
}
