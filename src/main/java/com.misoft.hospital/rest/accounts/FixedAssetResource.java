package com.misoft.hospital.rest.accounts;

import com.misoft.hospital.model.accounts.FixedAsset;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by larry on 3/15/2015.
 */
@Path("/fixedAssets")
@Stateless
public class FixedAssetResource extends BaseEntityService<FixedAsset> {
    public FixedAssetResource(){super(FixedAsset.class);}
}
