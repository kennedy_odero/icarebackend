package com.misoft.hospital.rest.accounts;

import com.misoft.hospital.model.accounts.Imprest;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by larry on 3/30/2015.
 */
@Path("/imprests")
@Stateless
public class ImprestResource extends BaseEntityService<Imprest> {
    public ImprestResource(){super(Imprest.class);}
}
