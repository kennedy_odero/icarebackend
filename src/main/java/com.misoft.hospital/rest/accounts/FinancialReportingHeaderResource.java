package com.misoft.hospital.rest.accounts;

import com.misoft.hospital.model.accounts.FinancialReportingHeader;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by larry on 3/19/2015.
 */
@Path("/financialReportingHeader")
@Stateless
public class FinancialReportingHeaderResource extends BaseEntityService<FinancialReportingHeader> {
    public FinancialReportingHeaderResource(){super(FinancialReportingHeader.class);}
}
