package com.misoft.hospital.rest.accounts;

import com.misoft.hospital.model.accounts.PostingAccount;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by larry on 3/10/2015.
 */
@Path("/postingAccounts")
@Stateless
public class PostingAccountResource extends BaseEntityService<PostingAccount> {
    public PostingAccountResource(){super(PostingAccount.class);}
}
