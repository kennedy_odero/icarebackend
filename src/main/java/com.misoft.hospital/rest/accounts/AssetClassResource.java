package com.misoft.hospital.rest.accounts;

import com.misoft.hospital.model.accounts.AssetClass;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by larry on 3/15/2015.
 */
@Path("/assetClasses")
@Stateless
public class AssetClassResource extends BaseEntityService<AssetClass> {
    public AssetClassResource(){super(AssetClass.class);}
}
