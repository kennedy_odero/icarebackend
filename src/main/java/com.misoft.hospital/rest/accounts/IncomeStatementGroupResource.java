package com.misoft.hospital.rest.accounts;

import com.misoft.hospital.model.accounts.IncomeStatementGroup;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by larry on 3/19/2015.
 */
@Path("/incomeStatementGroup")
@Stateless
public class IncomeStatementGroupResource extends BaseEntityService<IncomeStatementGroup> {
    public IncomeStatementGroupResource(){super(IncomeStatementGroup.class);}
}
