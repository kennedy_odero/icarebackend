package com.misoft.hospital.rest.accounts;

import com.misoft.hospital.model.accounts.BudgetAllocation;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by larry on 3/23/2015.
 */
@Path("/budgetAllocations")
@Stateless
public class BudgetAllocationResource extends BaseEntityService<BudgetAllocation> {
    public BudgetAllocationResource(){super(BudgetAllocation.class);}
}
