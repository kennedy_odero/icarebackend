package com.misoft.hospital.rest.accounts;

import com.misoft.hospital.model.accounts.Banking;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 4/25/16.
 */
@Stateless
@Path("/bankings")
public class BankingResource extends BaseEntityService<Banking>{
    public BankingResource(){
        super(Banking.class);
    }
}
