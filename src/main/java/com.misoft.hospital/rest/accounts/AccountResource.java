package com.misoft.hospital.rest.accounts;

import com.misoft.hospital.data.gdto.GenericDTO;
import com.misoft.hospital.model.accounts.Account;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by larry on 3/8/2015.
 */
@Path("/chartAccounts")
@Stateless
public class AccountResource extends BaseEntityService<Account> {
	public AccountResource() {
		super(Account.class);
	}

	@SuppressWarnings("unchecked")
	@GET
	@Override
	@Produces(MediaType.APPLICATION_JSON)
	public List<GenericDTO> getAll(@Context UriInfo uriInfo,
			@QueryParam("pageSize") @DefaultValue("25") int pageSize,
			@QueryParam("page") @DefaultValue("1") int pageNum,
			@QueryParam("orderBy") List<String> orderBy,
			@QueryParam("fields") String fieldList) {
		List<Account> accounts = getEntityManager()
				.createNativeQuery(
						"SELECT "
						+ "node.account_type,"
						+ "node.id, "
						+ "node.created_at,"
						+ "node.deleted,"
						+ "node.deleted_at,"
						+ "node.entity_status,"
						+ "node.locked_until,"
						+ "node.updated_at,"
						+ "node.version,"
						+ "node.created_by_id,"
						+ "node.deleted_by_id,"
						+ "node.locked_by_id,"
						+ "node.updated_by_id,"
						+ "node.taxable, "
						+ "((COUNT(parent.name) - 1) * 2 ) as lvl, "
						+ "node.name AS name,"
						+ "node.lft, "
						+ "node.cf_header,"
						+ "node.frhd_id,"
						+ "node.from_date,"
						+ "node.thru_date,"
						+ "node.rgt,"
						+ "node.account_type,"
						+ "node.code,"
						+ "node.account_category, "
						+ "node.frhd_id as frhd,"
						+ "node.cashflowSubHeader_id,"
						+ "node.cashflowHeader_id, "
						+ "node.active, "
						+ "node.status,"
						+ "node.REVALACCT_ID,"
						+ "node.revalue_acct, "
						+ "node.SRCCURRENCY_ID"
						+ " FROM accounts node, accounts parent "
						+ "WHERE node.lft BETWEEN parent.lft AND parent.rgt and node.lft > 1 "
						+ "and node.active=1 and parent.active=1 "
						+ "GROUP BY node.id,"
						+ "node.taxable,"
						+ "node.name,"
						+ "node.lft,"
						+ "node.cf_header,"
						+ "node.frhd_id,"
						+ "node.revalue_acct, "
						+ "node.rgt, "
						+ "node.account_type,"
						+ "node.code,"
						+ "node.account_category, "
						+ "node.frhd_id, "
						+ "node.from_date,"
						+ "node.thru_date,"
						+ "node.cashflowSubHeader_id, "
						+ "node.cashflowHeader_id, "
						+ "node.active, "
						+ "node.status,"
						+ "node.REVALACCT_ID, "
						+ "node.account_type,"
						+ "node.created_at,"
						+ "node.deleted,"
						+ "node.deleted_at,"
						+ "node.entity_status,"
						+ "node.locked_until,"
						+ "node.updated_at,"
						+ "node.version,"
						+ "node.account_category,"
						+ "node.created_by_id,"
						+ "node.deleted_by_id,"
						+ "node.locked_by_id,"
						+ "node.updated_by_id,"
						+ "node.SRCCURRENCY_ID ORDER BY node.lft",
						Account.class).getResultList();
		return toDTO(accounts);
	}
	
	@POST
    @Path("/createDefaultChart")
    public Response createDefaultChart(){
		String chartSql = "insert into accounts(account_type,id,account_category,code,name,active, lft, rgt, status, lvl) values('HEADING','1','','ACC0001','Chart of Accounts',1,1,10,'ACTIVE',0)";
		String assetsSql = "insert into accounts(account_type,id,account_category,code,name,active, lft, rgt, status, lvl) values('HEADING','2','','ACC0002','Assets',1,2,3,'ACTIVE',0)";
		String liabSql = "insert into accounts(account_type,id,account_category,code,name,active, lft, rgt, status, lvl) values('HEADING','3','','ACC0003','Liabilities',1,4,5,'ACTIVE',0)";
		String revSql = "insert into accounts(account_type,id,account_category,code,name,active, lft, rgt, status, lvl) values('HEADING','4','','ACC0004','Revenue',1,6,7,'ACTIVE',0)";
		String expSql = "insert into accounts(account_type,id,account_category,code,name,active, lft, rgt, status, lvl) values('HEADING','5','','ACC0005','Expenses',1,8,9,'ACTIVE',0)";
		getEntityManager().createNativeQuery(chartSql).executeUpdate();
		getEntityManager().createNativeQuery(assetsSql).executeUpdate();
		getEntityManager().createNativeQuery(liabSql).executeUpdate();
		getEntityManager().createNativeQuery(revSql).executeUpdate();
		getEntityManager().createNativeQuery(expSql).executeUpdate();
        return Response.ok().build();
    }

	private List<GenericDTO> toDTO(List<Account> list) {
		List<GenericDTO> genericDTOList = new ArrayList<>(list.size());
		for (Account request : list) {
			genericDTOList.add(request.genericDTO());
		}
		return genericDTOList;
	}

}
