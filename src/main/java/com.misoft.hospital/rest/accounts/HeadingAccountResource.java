package com.misoft.hospital.rest.accounts;

import com.misoft.hospital.model.accounts.HeadingAccount;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by larry on 3/10/2015.
 */
@Path("/headingAccounts")
@Stateless
public class HeadingAccountResource extends BaseEntityService<HeadingAccount> {
    public HeadingAccountResource(){super(HeadingAccount.class);}
}
