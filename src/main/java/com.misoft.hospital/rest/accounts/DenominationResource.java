package com.misoft.hospital.rest.accounts;

import com.misoft.hospital.model.accounts.Denomination;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 9/30/15.
 */
@Stateless
@Path("/denominations")
public class DenominationResource extends BaseEntityService<Denomination>{
    public DenominationResource(){
        super(Denomination.class);
    }
}
