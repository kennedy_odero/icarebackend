package com.misoft.hospital.rest.accounts;

import com.misoft.hospital.model.accounts.DirectDisbursement;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by larry on 3/19/2015.
 */
@Path("/directDisbursements")
@Stateless
public class DirectDisbursementResource extends BaseEntityService<DirectDisbursement> {
    public DirectDisbursementResource(){super(DirectDisbursement.class);}
}
