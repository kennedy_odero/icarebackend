package com.misoft.hospital.rest.accounts;


import com.misoft.hospital.model.accounts.Invoice;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

@Path("/invoices")
@Stateless
public class InvoiceResource extends BaseEntityService<Invoice> {
	public InvoiceResource(){
		super(Invoice.class);
	}
}