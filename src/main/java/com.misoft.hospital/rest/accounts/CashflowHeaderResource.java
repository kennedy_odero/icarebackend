package com.misoft.hospital.rest.accounts;

import com.misoft.hospital.model.accounts.CashflowHeader;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by larry on 3/21/2015.
 */
@Path("/cashflowHeader")
@Stateless
public class CashflowHeaderResource extends BaseEntityService<CashflowHeader> {
    public CashflowHeaderResource(){super(CashflowHeader.class);}
}
