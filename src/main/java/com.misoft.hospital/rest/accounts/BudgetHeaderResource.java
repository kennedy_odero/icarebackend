package com.misoft.hospital.rest.accounts;

import com.misoft.hospital.model.accounts.BudgetHeader;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by larry on 3/22/2015.
 */
@Path("/budgetHeader")
@Stateless
public class BudgetHeaderResource extends BaseEntityService<BudgetHeader> {
    public BudgetHeaderResource(){super(BudgetHeader.class);}
}
