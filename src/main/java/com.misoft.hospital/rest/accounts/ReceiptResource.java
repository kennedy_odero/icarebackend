package com.misoft.hospital.rest.accounts;

import com.misoft.hospital.model.accounts.Receipt;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by larry on 3/4/2015.
 */
@Path("/receipts")
@Stateless
public class ReceiptResource extends BaseEntityService<Receipt> {
    public ReceiptResource(){
        super(Receipt.class);
    }
}
