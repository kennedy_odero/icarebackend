package com.misoft.hospital.rest.accounts;

import com.misoft.hospital.model.accounts.CreditorDebtorType;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by larry on 3/27/2015.
 */
@Path("/creditorDebtorTypes")
@Stateless
public class CreditorDebtorTypeResource extends BaseEntityService<CreditorDebtorType> {
    public CreditorDebtorTypeResource(){super(CreditorDebtorType.class);}
}
