package com.misoft.hospital.rest.accounts;

import com.misoft.hospital.model.accounts.Receivable;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by larry on 3/2/2015.
 */
@Path("/receivables")
@Stateless
public class ReceivableResource extends BaseEntityService<Receivable> {
    public ReceivableResource(){
        super(Receivable.class);
    }
}
