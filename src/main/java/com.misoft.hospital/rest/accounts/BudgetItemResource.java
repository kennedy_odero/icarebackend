package com.misoft.hospital.rest.accounts;

import com.misoft.hospital.model.accounts.BudgetItem;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by larry on 3/22/2015.
 */
@Path("/budgetItem")
@Stateless
public class BudgetItemResource extends BaseEntityService<BudgetItem> {
    public BudgetItemResource(){super(BudgetItem.class);}
}
