package com.misoft.hospital.rest.accounts;

import com.misoft.hospital.model.accounts.FinancialReportingHeaderDetail;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by larry on 3/19/2015.
 */
@Path("/financialReportingHeaderDetail")
@Stateless
public class FinancialReportingHeaderDetailResource extends BaseEntityService<FinancialReportingHeaderDetail> {
    public FinancialReportingHeaderDetailResource(){super(FinancialReportingHeaderDetail.class);}
}
