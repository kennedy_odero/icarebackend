package com.misoft.hospital.rest.accounts;

import com.misoft.hospital.model.accounts.ExchangeRate;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by larry on 3/28/2015.
 */
@Path("/exchangeRates")
@Stateless
public class ExchangeRateResource extends BaseEntityService<ExchangeRate> {
    public ExchangeRateResource(){super(ExchangeRate.class);}
}
