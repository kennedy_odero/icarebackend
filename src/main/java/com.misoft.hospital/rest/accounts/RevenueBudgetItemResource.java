package com.misoft.hospital.rest.accounts;

import com.misoft.hospital.model.accounts.RevenueBudgetItem;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by larry on 3/22/2015.
 */
@Path("/revenueBudgetItem")
@Stateless
public class RevenueBudgetItemResource extends BaseEntityService<RevenueBudgetItem> {
    public RevenueBudgetItemResource(){super(RevenueBudgetItem.class);}
}
