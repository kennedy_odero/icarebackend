package com.misoft.hospital.rest.accounts;

import com.misoft.hospital.model.accounts.CashflowSubHeader;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by larry on 3/21/2015.
 */
@Path("/cashflowSubHeader")
@Stateless
public class CashflowSubHeaderResource extends BaseEntityService<CashflowSubHeader> {
    public CashflowSubHeaderResource(){super(CashflowSubHeader.class);}
}
