package com.misoft.hospital.rest;

import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
public class LoginObject {
    private String username;

    private String password;

    public LoginObject() {
    }

    /**
     * @param username
     * @param password
     */
    public LoginObject(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
