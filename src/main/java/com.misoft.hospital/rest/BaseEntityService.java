package com.misoft.hospital.rest;

import com.misoft.hospital.data.gdto.GenericDTO;
import com.misoft.hospital.data.gdto.types.*;
import com.misoft.hospital.model.base.ModelBase;
import com.misoft.hospital.service.admin.UserService;
import com.misoft.hospital.service.grant.GService;
import com.misoft.hospital.util.*;
import com.misoft.hospital.util.annotations.Permission;
import com.misoft.hospital.util.callbacks.During;
import com.misoft.hospital.util.callbacks.EntityCallbackClass;
import com.misoft.hospital.util.callbacks.EntityCallbackMethod;
import com.misoft.hospital.util.types.BasicTypeHelperImpl;
import com.misoft.hospital.util.types.ConversionException;
import com.misoft.hospital.util.types.ConversionManager;
import com.misoft.hospital.util.validation.ValidateAt;
import com.misoft.hospital.util.validation.ValidationMethod;
import com.misoft.hospital.util.validation.ValidatorClass;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.jboss.resteasy.spi.HttpResponse;

import javax.annotation.PostConstruct;
import javax.enterprise.context.spi.CreationalContext;
import javax.enterprise.event.Event;
import javax.enterprise.inject.spi.Bean;
import javax.enterprise.inject.spi.BeanManager;
import javax.enterprise.inject.spi.CDI;
import javax.inject.Inject;
import javax.persistence.*;
import javax.persistence.criteria.*;
import javax.persistence.criteria.Path;
import javax.validation.*;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.lang.reflect.*;
import java.net.URI;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Logger;

/**
 * <p>
 * A number of RESTful services implement GET operations on a particular type of entity. For
 * observing the DRY principle, the generic operations are implemented in the <code>BaseEntityService</code>
 * class, and the other services can inherit from here.
 * </p>
 * <p/>
 * <p>
 * Subclasses will declare a base path using the JAX-RS {@link javax.ws.rs.Path} annotation, for example:
 * </p>
 * <p/>
 * <pre>
 * <code>
 * &#064;Path("/widgets")
 * public class WidgetService extends BaseEntityService<Widget> {
 * ...
 * }
 * </code>
 * </pre>
 * <p/>
 * <p>
 * will support the following methods:
 * </p>
 * <p/>
 * <pre>
 * <code>
 *   GET /widgets
 *   GET /widgets/:id
 *   GET /widgets/count
 *   GET /widgets/:id/:collection_name
 * </code>
 * </pre>
 * <p/>
 * <p>
 * Subclasses may specify various criteria for filtering entities when retrieving a list of them, by supporting
 * custom query parameters. Pagination is supported by default through the query parameters <code>first</code>
 * and <code>maxResults</code>.
 * </p>
 * <p/>
 * <p>
 * The class is abstract because it is not intended to be used directly, but subclassed by actual JAX-RS
 * endpoints.
 * </p>
 */
@SuppressWarnings("CdiManagedBeanInconsistencyInspection")
@javax.ejb.Stateless
public abstract class BaseEntityService<T extends ModelBase> {

    //@PersistenceContext(type=PersistenceContextType.EXTENDED)
    @Inject
    private EntityManager entityManager;

    @Inject
    private BeanManager beanManager;

    @Context
    protected HttpResponse response;

    @Context
    protected UriInfo uriInfo;

    /*@Inject*/
    protected Validator validator;

    /*@Inject
    protected Event<T> entityEventSrc;*/

    @Inject
    protected Event<EntityCreatedEvent<T>> entityCreatedEventSrc;

    @Inject
    protected Event<EntityUpdatedEvent<T>> entityUpdatedEventSrc;

    @Inject
    protected UserService userService;

    @Inject
    protected Logger log;

    @Inject
    protected GService gService;

    protected Class<T> entityClass;

    protected LocaleAwareMessageInterpolator interpolator;

    protected BaseEntityService() {
    }

    public BaseEntityService(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    //an alternative to the constructor
    protected void setEntityClass(Class<T> entityClass){
        this.entityClass = entityClass;
    }

    public EntityManager getEntityManager() {
        final Session session = entityManager.unwrap(Session.class);

        org.hibernate.Filter filter = session.enableFilter("filterByDeleted");
     /*   AuditLogInterceptor interceptor =
                (AuditLogInterceptor) ((SessionImplementor) session).getInterceptor();
        interceptor.setCurrentSession(session);
        interceptor.setCurrentUserId(10010L);//TODO*/
        return entityManager;
    }

    @PostConstruct
    public void init() {
        log.info("*********** init ****************");
        // Create a bean validator and check for issues.
        interpolator = new LocaleAwareMessageInterpolator();
        //set the headers

        javax.validation.Configuration<?> configuration = Validation.byDefaultProvider().configure();
        this.validator = configuration.messageInterpolator(interpolator).buildValidatorFactory().getValidator();
    }

    @DELETE
    @javax.ws.rs.Path("/{id:[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("id") String id) {
        Response.ResponseBuilder builder = null;
        T entity = gService.find(id, entityClass);
        try {
            //perform validator validation
            validatorValidation(entity, ValidateAt.DELETE);
            //entity callbacks
            invokeEntityCallbacks(entity, During.DELETE);
            //now perform remove
            entity.setDeletedById(userService.currentUser().getId());
            gService.softRemove(entity);
            builder = Response.ok();
        } catch (ConstraintViolationException ce) {
            ce.printStackTrace();
            // Handle bean validation issues
            builder = createViolationResponse(ce.getConstraintViolations());
        } catch (ValidationException e) {
            e.printStackTrace();
            // Handle the unique constrain violation
            builder = createValidationResponse(e);
        } catch (InvocationTargetException ite){
            ite.printStackTrace();
            if(ite.getCause() instanceof ValidationException){
                builder = createValidationResponse(ite);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("error", StackTraceUtil.getStackTrace(e));
            builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
        }
        return builder.build();
    }

    @DELETE
    @javax.ws.rs.Path("/{id:[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}}/collections/{collection_name}/softRemove/{itemId:[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteChild(@PathParam("id") String id, @PathParam("collection_name") String collectionName, @PathParam("itemId") String itemId) {
        Field collectionField = Reflections.getField(entityClass, collectionName);
        if (collectionField == null) {
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("error", "The collection field ' " + collectionName + " ' does not exist on the entity ' " + entityClass.getCanonicalName() + "'");
            return Response.status(Response.Status.BAD_REQUEST).entity(responseObj).build();
        }
        Response.ResponseBuilder builder = null;
        Type type = collectionField.getGenericType();
        Class<ModelBase> clzz = null;
        clzz = getGenericParams(type, clzz);
        //now perform remove
        gService.softRemove(itemId, clzz);
        return Response.ok().build();
    }

    @GET
    @javax.ws.rs.Path("/{id:[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}}/collections/{collection_name}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllChildrens(@Context UriInfo uriInfo,
                                    @QueryParam("pageSize") @DefaultValue("10") int pageSize,
                                    @QueryParam("page") @DefaultValue("1") int pageNum,
                                    @QueryParam("orderBy") List<String> orderBy,
                                    @QueryParam("fields") String fieldList,
                                    @PathParam("id") String id,
                                    @PathParam("collection_name") String collectionName
    ) throws Exception {
        log.info("the id " + id);
        log.info(" order by " + orderBy);
        log.info(" fields " + fieldList);
        log.info("collection name " + collectionName);

        Field collectionField = Reflections.getField(entityClass, collectionName);
        if (collectionField == null) {
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("error", "The collection field ' " + collectionName + " ' does not exist on the entity ' " + entityClass.getName() + "'");
            return Response.status(Response.Status.BAD_REQUEST).entity(responseObj).build();
        }
        String sql = "select e." + collectionName + " from " + entityClass.getSimpleName() + " as e where e.id =:id";


        Type type = collectionField.getGenericType();
        Class<ModelBase> clzz = null;
        clzz = getGenericParams(type, clzz);
        String[] fields = fieldList == null ? makeDefaultSelectionFields(clzz) : fieldList.split(",");
        List<? extends ModelBase> res = getEntityManager().createQuery(sql)
                .setParameter("id", id)
                .getResultList();
        Collection<GenericDTO> dtos = new ArrayList<>(res.size());
        for (ModelBase o : res) {
            dtos.add(toDTO(o, fields, clzz));
        }

        int numOfRecords = count2(uriInfo, clzz);
        final Pager pager = new Pager(pageSize, pageNum, numOfRecords);
        pager.setNumOfRecords(numOfRecords);
        log.info(" pager " + pager);

        response.getOutputHeaders().putSingle(Pager.TOTAL_COUNT, pager.getNumOfRecords());
        response.getOutputHeaders().putSingle(Pager.CURRENT_PAGE, pager.getPage());
        response.getOutputHeaders().putSingle(Pager.START, pager.getFirstRecord());
        response.getOutputHeaders().putSingle(Pager.END, pager.getIndexEnd());
        return Response.ok().entity(dtos).build();
    }

    private Class<ModelBase> getGenericParams(Type type, Class<ModelBase> clzz) {
        if (type instanceof ParameterizedType) {
            ParameterizedType pType = (ParameterizedType) type;
            Type arr = pType.getActualTypeArguments()[0];
            clzz = (Class<ModelBase>) arr;
        }
        return clzz;
    }


    /**
     * <p>
     * A method for retrieving all entities of a given type.
     * Supports the query parameters
     * <li>
     * <li><code>pageSize</code> , <code>page</code> for pagination.
     * The defaults are <code>pageSize=10</code> , <code>page=1</code>
     * </li>
     * <li> <code>orderBy</code> for sorting, e.g orderBy=-organizationName&orderBy=-createdAt
     * use a leading <code>-</code> to indicate desc, otherwise default it asc.
     * The default orders by createdAt desc.
     * </li>
     * <li><code>fields</code> e.g invoices?fields=name,createdAt,state, party:name, party:state
     * Restricts the fields returned. To traverse relationships, use party:name syntax
     * </li>
     * </li>
     * <p/>
     * <p/>
     * <p/>
     * </p>
     *
     * @return
     */
    @GET
    @Permission(suffix = "entityCLass.list")
    @Produces(MediaType.APPLICATION_JSON)
    public List<GenericDTO> getAll(@Context UriInfo uriInfo,
                                   @QueryParam("pageSize") @DefaultValue("25") int pageSize,
                                   @QueryParam("page") @DefaultValue("1") int pageNum,
                                   @QueryParam("orderBy") List<String> orderBy,
                                   @QueryParam("fields") String fieldList) {
        int numOfRecords = count(uriInfo);
        final Pager pager = new Pager(pageSize, pageNum, numOfRecords);
        pager.setNumOfRecords(numOfRecords);
        log.info(" pager " + pager);
        log.info(" order by " + orderBy);
        log.info(" fields " + fieldList);
        String[] fields = fieldList == null ? makeDefaultSelectionFields(entityClass) : fieldList.split(",");

        return getAll(pager, orderBy, fields, uriInfo.getQueryParameters(), uriInfo.getPathParameters());

    }

    public List<GenericDTO> getAll(final Pager pager,
                                   List<String> orderBy,
                                   String[] fields,
                                   MultivaluedMap<String, String> queryParameters,
                                   MultivaluedMap<String, String> pathParameters) {

        final CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        final CriteriaQuery<Tuple> cq = cb.createTupleQuery();
        Root<T> root = cq.from(entityClass);
        Predicate[] predicates = extractPredicates(queryParameters, cb, root, pathParameters);
        List<Selection<?>> selections = getSelections(fields, root);

        log.info("created selections size " + selections.size());
        cq.multiselect(selections).where(predicates);
        List<Order> od = getOrders(orderBy, cb, root);
        cq.orderBy(od);
        //execute query
        Query q = getEntityManager().createQuery(cq);
        log.info(" max results " + pager.getIndexEnd() + ", first result " + pager.getIndexBegin());
        //q.setMaxResults(pager.getPageSize() + 1);
        q.setMaxResults(pager.getPageSize());
        q.setFirstResult(pager.getIndexBegin());
        List<Tuple> tupleResult = q.getResultList();
        log.info("results obtained , size " + tupleResult.size());
        //format the output
        List<GenericDTO> dtos = Collections.EMPTY_LIST;
        try {
            dtos = getGenericDTOs(fields, tupleResult, entityClass);
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File |//TODO| File Templates.
        }

        response.getOutputHeaders().putSingle(Pager.TOTAL_COUNT, pager.getNumOfRecords());
        response.getOutputHeaders().putSingle(Pager.CURRENT_PAGE, pager.getPage());
        response.getOutputHeaders().putSingle(Pager.START, pager.getIndexBegin() + 1);
        response.getOutputHeaders().putSingle(Pager.END, pager.getIndexEnd() + 1);
        response.getOutputHeaders().putSingle(Pager.PAGE_SIZE, pager.getPageSize());
        return dtos;
    }

    private List<GenericDTO> getGenericDTOs2(String[] fields, List<Object[]> tupleResult) throws NoSuchFieldException {
        List<GenericDTO> dtos = new ArrayList<>();
        for (Object[] row : tupleResult) {
            //dumpRow(row);
            GenericDTO dto = getGenericDTO(entityClass, fields, row);
            dtos.add(dto);
        }
        return dtos;
    }

    protected List<GenericDTO> getGenericDTOs(String[] fields, List<Tuple> tupleResult, Class<T> entityClass1) throws NoSuchFieldException {
        List<GenericDTO> dtos = new ArrayList<>();
        for (Tuple t : tupleResult) {
            Object[] row = t.toArray();
            dumpRow(fields, row);
            GenericDTO dto = getGenericDTO(entityClass1, fields, row);
            dtos.add(dto);
        }
        return dtos;
    }

    public <T extends ModelBase> GenericDTO getGenericDTO(Class<T> entityClass, String[] fields, Object[] row) {
        log.info(" getGenericDTO , entityClass " + entityClass.getSimpleName() + " , fields " + Arrays.asList(fields) + " the " +
                "values " + Arrays.asList(row));
        GenericDTO dto = new GenericDTO(entityClass.getName());
        int i = 0;
        Map<String, GenericDTO> relationships = new HashMap<>();
        for (Object r : row) {
            final String fieldName = fields[i++];
            log.info("current field name " + fieldName);
            Map<String, GenericDTO> relationships2 = new HashMap<>();
            if (fieldName.contains(":")) {
                //relationship
                String[] rels = fieldName.split(":");
                log.info(" the length of rels " + rels.length);
                String relationName = rels[0].trim();
                Field attrClass = ReflectionUtils.getField(relationName, entityClass);
                //check if the relationship exists
                String relationAttributeName = rels[1].trim();
                boolean relationExists = relationships.containsKey(relationName);
                if (relationExists) {
                    //add to the relationship
                    GenericDTO relateddto = relationships.get(relationName);
                    Class<?> fieldType = attrClass.getType();
                    Field ff = ReflectionUtils.getField(rels[1], fieldType);
                    if (!rels[1].contains("<")) {
                        ReflectionUtils.extractValueFromFieldToDTO(relateddto, r, relationAttributeName, ff);
                    } else if (rels[1].contains("<")) {
                        //wfTask:owner<firstName;lastName>
                        String[] childFields = null;
                        String collectionField = rels[1].trim();   //owner<firstName;lastName>
                        log.info("collectionField2 " + collectionField);
                        String ownerRelationName;
                        if (collectionField.contains("<")) {
                            int startIndex = collectionField.indexOf("<");
                            int endIndex = collectionField.lastIndexOf(">");
                            ownerRelationName = collectionField.substring(0, startIndex);
                            log.info("RELATION NAME2 " + ownerRelationName);
                            childFields = collectionField.substring(startIndex + 1, endIndex).split(";");
                        } else {
                            //wfTask:owner
                            ownerRelationName = collectionField;
                            //childFields = makeDefaultSelectionFields(<<owner class>>);
                        }
                        Field ownerField = Reflections.getField(fieldType, ownerRelationName);
                        Class<?> ownerClass = ownerField.getType();
                        log.info("owner class2 " + ownerClass.getName());
                        if (childFields == null) {
                            childFields = makeDefaultSelectionFields(ownerClass);
                        }
                        log.info("RELATION FIELDS2 " + Arrays.asList(childFields));
                        GenericDTO ownerDTO = null;
                        if(r != null){
                            for (String attributeName : childFields) {
                                String firstNameRelationAttributeName = attributeName.trim();
                                log.info("owner relation name " + ownerRelationName + " fist name RAN " + firstNameRelationAttributeName + ", r : " + r);
                                Field firstNameField = Reflections.getField(ownerClass, firstNameRelationAttributeName);
                                //get the value of the field from the class
                                Method fistNameGetterMethod = Reflections.getGetterMethod(ownerClass, firstNameRelationAttributeName);
                                Object firstNameValue = Reflections.invokeAndWrap(fistNameGetterMethod, r);
                                log.info("the field value " + firstNameValue);
                                if (relationships2.containsKey(ownerRelationName)) {
                                    ownerDTO = relationships2.get(ownerRelationName);
                                    ReflectionUtils.extractValueFromFieldToDTO(ownerDTO, firstNameValue, firstNameRelationAttributeName, firstNameField);
                                } else {
                                    ownerDTO = new GenericDTO(ownerClass.getName());
                                    ReflectionUtils.extractValueFromFieldToDTO(ownerDTO, firstNameValue, firstNameRelationAttributeName, firstNameField);
                                    relationships2.put(ownerRelationName, ownerDTO);
                                }
                            }
                            log.info("the owner dto " + ownerDTO);
                            relateddto.addRelation2(ownerRelationName, ownerDTO);
                        }
                    }
                } else /*if (!relationExists) */ {
                    //create the relation
                    //get the FQN of the related entity
                    GenericDTO relateddto = new GenericDTO(attrClass.getType().getName());
                    Class<?> fieldType = attrClass.getType();
                    Field ff = ReflectionUtils.getField(rels[1], fieldType);
                    if (!rels[1].contains("<")) {
                        ReflectionUtils.extractValueFromFieldToDTO(relateddto, r, relationAttributeName, ff);
                    } else if (rels[1].contains("<")) {
                        //wfTask:owner<firstName;lastName>
                        String[] childFields = null;
                        String collectionField = rels[1].trim();   //owner<firstName;lastName>
                        log.info("collectionField2 " + collectionField);
                        String ownerRelationName;
                        if (collectionField.contains("<")) {
                            int startIndex = collectionField.indexOf("<");
                            int endIndex = collectionField.lastIndexOf(">");
                            ownerRelationName = collectionField.substring(0, startIndex);
                            log.info("RELATION NAME2 " + ownerRelationName);
                            childFields = collectionField.substring(startIndex + 1, endIndex).split(";");
                        } else {
                            //wfTask:owner
                            ownerRelationName = collectionField;
                            //childFields = makeDefaultSelectionFields(<<owner class>>);
                        }
                        Field ownerField = Reflections.getField(fieldType, ownerRelationName);
                        Class<?> ownerClass = ownerField.getType();
                        log.info("owner class " + ownerClass.getName());
                        if (childFields == null) {
                            childFields = makeDefaultSelectionFields(ownerClass);
                        }
                        log.info("RELATION FIELDS2 " + Arrays.asList(childFields));
                        GenericDTO ownerDTO = null;
                        for (String attributeName : childFields) {
                            String firstNameRelationAttributeName = attributeName.trim();
                            log.info("owner relation name " + ownerRelationName + " fist name RAN " + firstNameRelationAttributeName);
                            Field firstNameField = Reflections.getField(ownerClass, firstNameRelationAttributeName);
                            //get the value of the field from the class
                            Method fistNameGetterMethod = Reflections.getGetterMethod(ownerClass, firstNameRelationAttributeName);
                            log.info("Relation : " + r);
                            Object firstNameValue = r == null? null : Reflections.invokeAndWrap(fistNameGetterMethod, r);
                            log.info("the field value " + firstNameValue);
                            if (relationships2.containsKey(ownerRelationName)) {
                                ownerDTO = relationships2.get(ownerRelationName);
                                ReflectionUtils.extractValueFromFieldToDTO(ownerDTO, firstNameValue, firstNameRelationAttributeName, firstNameField);
                            } else {
                                ownerDTO = new GenericDTO(ownerClass.getName());
                                ReflectionUtils.extractValueFromFieldToDTO(ownerDTO, firstNameValue, firstNameRelationAttributeName, firstNameField);
                                relationships2.put(ownerRelationName, ownerDTO);
                            }
                        }
                        log.info("the owner dto " + ownerDTO);
                        relateddto.addRelation2(ownerRelationName, ownerDTO);
                    }
                    relationships.put(relationName, relateddto);
                }
            } else {
                Field f = ReflectionUtils.getField(fieldName, entityClass);
                ReflectionUtils.extractValueFromFieldToDTO(dto, r, fieldName, f);
            }
            //add the relationships
            for (Map.Entry<String, GenericDTO> entry : relationships.entrySet()) {
                //dto.addRelation(entry.getKey(), entry.getValue());
                dto.addRelation2(entry.getKey(), entry.getValue());
            }
        }
        return dto;
    }


    /**
     * Dotting added to orderBy e.g  orderBy=user:login
     * NOTE : its one level deep
     *
     * @param orderBy
     * @param cb
     * @param root
     * @return
     */
    private List<Order> getOrders(List<String> orderBy, CriteriaBuilder cb, Root<T> root) {
        log.info("Received order by : " + orderBy);
        if (orderBy.size() == 0) {
            //add default order by createdAt desc
            orderBy.add("-createdAt");
        }
        log.info("The order fields : " + orderBy);
        List<Order> od = new ArrayList<>(orderBy.size());
        for (String orderDesc : orderBy) {
            if (orderDesc.contains(":")) {
                String[] rels = orderDesc.split(":");
                //root.get(rels[0]).get(rels[1]);
                od.add(rels[0].startsWith("-") ? cb.desc(root.get(rels[0].substring(1)).get(rels[1])) :
                        cb.asc(root.get(rels[0]).get(rels[1])));
            } else {
                od.add(orderDesc.startsWith("-") ? cb.desc(root.get(orderDesc.substring(1))) : cb.asc(root.get(orderDesc)));
            }
        }
        return od;
    }


    protected List<Selection<?>> getSelections(String[] fields, Root<T> root) {
        log.info(" the fields in selection " + Arrays.asList(fields) + " number of fields " + fields.length);
        List<Selection<?>> selections = new ArrayList<>(fields.length);
        for (String field : fields) {
            log.info("field name " + field);
            if (field.contains(":")) {
                //joins
                //wfTask:owner<firstName;lastName>
                String[] rels = field.split(":");
                Join<Object, Object> join = root.join(rels[0], JoinType.LEFT);
                String relationRest = rels[1].trim();
                if (relationRest.contains("<")) {
                    //owner<firstName;lastName>
                    final int startIndex = relationRest.indexOf("<");
                    final int endIndex = relationRest.indexOf(">");
                    String[] attributes = relationRest.substring(startIndex + 1, endIndex).split(";");
                    String relationName = relationRest.substring(0, startIndex);
                    //TODO attributes in the join query
                    log.info(" relation name " + relationName + " attributes " + Arrays.asList(attributes));
                    Path<Object> p = join.join(relationName, JoinType.LEFT);
                    /*if (attributes.length > 0) {
                        for (String attributeName : attributes) {
                            p = p.get(attributeName);
                            selections.add(p);
                        }
                    } else {
                        selections.add(p);
                    }*/
                    selections.add(p);

                } else {
                    Path<Object> p = join.get(relationRest);
                    selections.add(p);
                }
            } else {
                selections.add(root.get(field));
            }
        }
        return selections;
    }

    public int count2(UriInfo uriInfo, Class entityClass) {
        CriteriaBuilder criteriaBuilder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root root = criteriaQuery.from(entityClass);
        criteriaQuery.select(criteriaBuilder.count(root));
        Predicate[] predicates = extractPredicates(uriInfo.getQueryParameters(), criteriaBuilder, root, uriInfo.getPathParameters());
        criteriaQuery.where(predicates);
        int count = getEntityManager().createQuery(criteriaQuery).getSingleResult().intValue();
        return count;
    }

    public int count(UriInfo uriInfo) {
        CriteriaBuilder criteriaBuilder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<T> root = criteriaQuery.from(entityClass);
        criteriaQuery.select(criteriaBuilder.count(root));
        Predicate[] predicates = extractPredicates(uriInfo.getQueryParameters(), criteriaBuilder, root, uriInfo.getPathParameters());
        criteriaQuery.where(predicates);
        int count = getEntityManager().createQuery(criteriaQuery).getSingleResult().intValue();
        return count;
    }

    /**
     * <p>
     * A method for counting all entities of a given type
     * </p>
     *
     * @param uriInfo application and request context information (see {@see UriInfo} class information for more details)
     * @return
     */
    @GET
    @javax.ws.rs.Path("/count")
    @Produces(MediaType.APPLICATION_JSON)
    public Map<String, Long> getCount(@Context UriInfo uriInfo) {

        CriteriaBuilder criteriaBuilder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<T> root = criteriaQuery.from(entityClass);
        criteriaQuery.select(criteriaBuilder.count(root));
        Predicate[] predicates = extractPredicates(uriInfo.getQueryParameters(), criteriaBuilder, root, uriInfo.getPathParameters());
        criteriaQuery.where(predicates);
        Map<String, Long> result = new HashMap<>();
        result.put("count", getEntityManager().createQuery(criteriaQuery).getSingleResult());
        return result;
    }

    /**
     * <p>
     * Subclasses may choose to expand the set of supported query parameters (for adding more filtering
     * criteria on search and count) by overriding this method.
     * </p>
     *
     * @param queryParameters - the HTTP query parameters received by the endpoint
     * @param criteriaBuilder - @{link CriteriaBuilder} used by the invoker
     * @param root            @{link Root} used by the invoker
     * @param pathParameters
     * @return a list of {@link javax.persistence.criteria.Predicate}s that will added as query parameters
     */
    protected Predicate[] extractPredicates(MultivaluedMap<String, String> queryParameters, CriteriaBuilder criteriaBuilder, Root<T> root, MultivaluedMap<String, String> pathParameters) {

        List<String> whereList = queryParameters.get("where");
        if (whereList == null || whereList.isEmpty()) {
            log.info(" no where clause ...");
            return new Predicate[]{};
        }

        log.info(" processing where list ' " + whereList + "'");
        List<Predicate> predicates = new ArrayList<>();
        //?[where=<field>,<comparator>,<value>]*
        //?[where=<field>,<comparator>,<value>,|,<field>,<comparator>,<value>]
        for (String whereParam : whereList) {
            log.info(" the current where clause " + whereParam);
            String[] p = whereParam.split(",");
            log.info(" the fields ' " + Arrays.asList(p) + "' , size of p " + p.length);
            if (p.length != 3 && p.length != 7) {
                throw new WebApplicationException("Invalid number of params for where request, ' " + whereList + "'", Response.Status.BAD_REQUEST);
            }
            String fieldPath = p[0];
            if (p.length == 3) {
                convertClausePredicate(criteriaBuilder, root, predicates, whereParam, p, fieldPath);
            } else {
                //[where=<field>,<comparator>,<value>,|,<field>,<comparator>,<value>]
                String[] clauses = whereParam.split("\\|");
                log.info("we have or clause ; " + whereParam + " clauses are " + clauses[0] + " " + clauses[1]);
                List<Predicate> leftPredicates = new ArrayList<>();
                List<Predicate> rightPredicates = new ArrayList<>();
                log.info("converting the left..");
                convertClausePredicate(criteriaBuilder, root, leftPredicates, clauses[0], clauses[0].split(","), p[0]);
                log.info("after converting, predicates are " + leftPredicates);
                log.info("converting the right..");
                final String[] split = clauses[1].split(",");
                String[] ff = new String[split.length - 1];
                System.arraycopy(split, 1, ff, 0, ff.length);
                convertClausePredicate(criteriaBuilder, root, rightPredicates, clauses[1], ff, p[4]);
                log.info("after converting, predicates are " + rightPredicates);
                leftPredicates.addAll(rightPredicates);
                log.info("all predicates are " + leftPredicates);
                Predicate[] pred = new Predicate[leftPredicates.size()];
                leftPredicates.toArray(pred);
                predicates.add(criteriaBuilder.or(pred));
            }
        }

        log.info(" collected predicates " + predicates);
        return predicates.toArray(new Predicate[predicates.size()]);
    }

    private void convertClausePredicate(CriteriaBuilder criteriaBuilder, Root<T> root, List<Predicate> predicates, String whereParam, String[] p, String fieldPath) {
        if (fieldPath.isEmpty()) {
            throw new WebApplicationException("Invalid path for where request, ' " + fieldPath + "'", Response.Status.BAD_REQUEST);

        }
        //validate path expression
        Path<?> path = getPath(root, fieldPath);
        Class<?> pathClass = path.getJavaType();
        log.info(" the path " + path + " path java type " + path.getJavaType().getName());
        String comparator = p[1];
        ConversionManager conversionManager = ConversionManager.getDefaultManager();
        BasicTypeHelperImpl typeHelper = BasicTypeHelperImpl.getInstance();
        switch (comparator) {
            case "=":
            case "!=":
            case ">":
            case ">=":
            case "<":
            case "<=": {
                //the value must be numeric and field also needs to be numeric
                if (p.length != 3) {
                    throw new WebApplicationException("No value specified for where request, ' " + whereParam + "'", Response.Status.BAD_REQUEST);
                }
                String value = p[2];
                if("loggedInUser".equalsIgnoreCase(value)) value = userService.currentUser().getId() + ""; //get the real value of the psuedo value loggedInUser
                Object typedValue = null;
                //this also checks compatibility between the path and the input query parameter
                try {
                    typedValue = conversionManager.convertObject(value, pathClass);
                } catch (ConversionException e) {
                    e.printStackTrace();
                    throw new WebApplicationException("The specified value '" + value + "'" +
                            "is incompatible with the target type '" + pathClass.getName() + "', in the request  " + whereParam + "'", Response.Status.BAD_REQUEST);
                }
                log.info(" the typed value parameter " + typedValue);
                //pathClass || typedValue must be numeric (since they r the same , we don't need to check both)
                final Class parameterClass = typeHelper.getJavaClass(typedValue);
                log.info(" parameter class " + parameterClass);
                if (typeHelper.isNumericType(parameterClass) || typeHelper.isDateClass(parameterClass)) {
                    switch (comparator) {
                        case "=":
                            predicates.add(criteriaBuilder.equal(path, typedValue));
                            break;
                        case "!=":
                            predicates.add(criteriaBuilder.notEqual(path, typedValue));
                            break;
                        case ">":
                            if (typeHelper.isNumericType(parameterClass)) {
                                predicates.add(criteriaBuilder.gt((Expression<? extends Number>) path, (Number) typedValue));
                            } else if (typeHelper.isDateClass(parameterClass)) {
                                predicates.add(criteriaBuilder.greaterThan((Expression<? extends Comparable>) path, (Comparable) typedValue));
                            }
                            break;
                        case ">=":
                            if (typeHelper.isNumericType(parameterClass)) {
                                predicates.add(criteriaBuilder.ge((Expression<? extends Number>) path, (Number) typedValue));
                            } else if (typeHelper.isDateClass(parameterClass)) {
                                System.out.print("get ======" + new SimpleDateFormat("yyyy-MM-dd").format(typedValue));
                                predicates.add(criteriaBuilder.greaterThanOrEqualTo((Expression<? extends Comparable>) path, (Comparable) typedValue));
                            }
                            break;
                        case "<":
                            if (typeHelper.isNumericType(parameterClass)) {
                                predicates.add(criteriaBuilder.lt((Expression<? extends Number>) path, (Number) typedValue));
                            } else if (typeHelper.isDateClass(parameterClass)) {
                                predicates.add(criteriaBuilder.lessThan((Expression<? extends Comparable>) path, (Comparable) typedValue));
                            }
                            break;
                        case "<=":
                            if (typeHelper.isNumericType(parameterClass)) {
                                predicates.add(criteriaBuilder.le((Expression<? extends Number>) path, (Number) typedValue));
                            } else if (typeHelper.isDateClass(parameterClass)) {
                                System.out.print("get =<=====" + new SimpleDateFormat("yyyy-MM-dd").format(typedValue));
                                predicates.add(criteriaBuilder.lessThanOrEqualTo((Expression<? extends Comparable>) path, (Comparable) typedValue));
                            }
                            break;
                    }
                } else if (comparator.compareTo("=") == 0 && typeHelper.isEnumType(parameterClass)) {
                    log.info("we have an enum value, class ' " + parameterClass + "'");
                    predicates.add(criteriaBuilder.equal(path, typedValue));
                } else if (comparator.compareTo("!=") == 0 && typeHelper.isEnumType(parameterClass)) {
                    log.info("we have an enum value, class ' " + parameterClass + "'");
                    predicates.add(criteriaBuilder.notEqual(path, typedValue));
                } else if (comparator.compareTo("=") == 0 && typeHelper.isStringType(parameterClass)) {
                    log.info("we have a string value, class ' " + parameterClass + "'");
                    predicates.add(criteriaBuilder.equal(path, typedValue));
                } else if (comparator.compareTo("=") == 0 && typeHelper.isBooleanType(parameterClass)) {
                    log.info("we have a boolean value, class ' " + parameterClass + "'");
                    predicates.add(criteriaBuilder.equal(path, typedValue));
                } else if (comparator.compareTo("!=") == 0 && typeHelper.isBooleanType(parameterClass)) {
                    log.info("we have a boolean value, class ' " + parameterClass + "'");
                    predicates.add(criteriaBuilder.notEqual(path, typedValue));
                } else {
                    throw new WebApplicationException("The specified value '" + value + "' , of type '" + parameterClass + ", " +
                            " type is not Numeric, in the request  " + whereParam + "'", Response.Status.BAD_REQUEST);
                }
                break;
            }

            case "LIKE":
            case "NOT_LIKE": {
                log.info(" LIKE comparator , p array => " + Arrays.asList(p));
                if (p.length != 3) {
                    throw new WebApplicationException("No value specified for where request, ' " + whereParam + "'", Response.Status.BAD_REQUEST);
                }
                String value = p[2];
                //path must be of  type Path<String>
                // LIKE & NOT_LIKE  are supported for Strings only
                if (typeHelper.isStringType(pathClass)) {
                    switch (comparator) {
                        case "LIKE":
                            predicates.add(criteriaBuilder.like((Expression<String>) path, "%" + value + "%"));
                            break;
                        case "NOT_LIKE":
                            predicates.add(criteriaBuilder.notLike((Expression<String>) path, value));
                            break;
                    }
                } else {
                    throw new WebApplicationException("LIKE & NOT_LIKE comparator are supported only for String " +
                            "attributes , the path specified,' " + fieldPath + "'  has a type ' " + pathClass + "' ", Response.Status.BAD_REQUEST);
                }
                break;
            }

            //name in ( 'A'; 'B'; 'C'; 'D')
            case "IN":
            case "NOT_IN": {
                log.info(" IN/NOT_IN comparator , p array => " + Arrays.asList(p));
                if (p.length != 3) {
                    throw new WebApplicationException("No value specified for where request, ' " + whereParam + "'", Response.Status.BAD_REQUEST);
                }
                //check the path
                if (typeHelper.isNumericType(pathClass) || typeHelper.isDateClass(pathClass) || typeHelper.isEnumType(pathClass) || typeHelper.isStringType(pathClass)) {
                    String value = p[2];
                    log.info(" IN comparator, value ' " + value + " ' ");
                    int startIndex = value.indexOf("(");
                    int endIndex = value.indexOf(")");
                    String[] values = value.substring(startIndex + 1, endIndex).split(";");
                    log.info("Substring : " + value.substring(startIndex + 1, endIndex) + ", Values: " + values);
                    if (values.length <= 0) {
                        throw new WebApplicationException("No IN parameter specified for where request, ' " + whereParam + "'", Response.Status.BAD_REQUEST);
                    }
                    List typedValues = new ArrayList();
                    //this also checks compatibility between the path and the input query parameters
                    for (String v : values) {
                        try {
                            typedValues.add(conversionManager.convertObject(v, pathClass));
                        } catch (ConversionException e) {
                            e.printStackTrace();
                            throw new WebApplicationException("The specified value '" + value + "'" +
                                    "is incompatible with the target type '" + pathClass.getName() + "', in the request  " + whereParam + "'", Response.Status.BAD_REQUEST);
                        }
                    }
                    log.info(" the typed value parameters " + typedValues);
                    switch (comparator) {
                        case "IN":
                            predicates.add(path.in(typedValues));
                            break;
                        case "NOT_IN":
                            predicates.add(path.in(typedValues).not());
                            break;
                    }


                } else {
                    throw new WebApplicationException("IN & NOT_IN comparator are supported only for Numeric & Date " +
                            "attributes , the path specified,' " + fieldPath + "'  has a type ' " + pathClass + "' ", Response.Status.BAD_REQUEST);
                }
                break;
            }
            case "IS":
            case "IS_NOT": {
                log.info(" IS/IS_NOT comparator , p array => " + Arrays.asList(p));
                if (p.length != 3) {
                    throw new WebApplicationException("No value specified for where request, ' " + whereParam + "'", Response.Status.BAD_REQUEST);
                }
                String value = p[2];
                if (value.trim().isEmpty()) {
                    throw new WebApplicationException("No parameter specified for where request, ' " + whereParam + "'", Response.Status.BAD_REQUEST);
                }
                log.info(" the value parameter " + value);
                //we support NULL,NOT_NULL,
                switch (value.toUpperCase().trim()) {
                    case "NULL":
                        //in this case, the path/LHS can be any expression
                        predicates.add(criteriaBuilder.isNull(path));
                        break;
                    case "NOT_NULL":
                        predicates.add(criteriaBuilder.isNotNull(path));
                        break;
                    default:
                        //now we have a value ... so proceed as usual ..
                        //note that this works only for Number & Date fields
                        //and its exactly the same as "=" and "!="
                        break;
                }
                break;
            }
            default:
                throw new IllegalArgumentException("Invalid comparator: " + comparator);

        }
    }

    public static Path<?> getPath(Path<?> path, String propertyPath) {
        if (StringUtils.isEmpty(propertyPath))
            return path;

        String name = StringUtils.substringBefore(propertyPath, PropertyUtils.PROPERTY_SEPARATOR);
        Path<?> p = path.get(name);

        return getPath(p, StringUtils.substringAfter(propertyPath, PropertyUtils.PROPERTY_SEPARATOR));

    }

    /**
     * <p>
     * A method for retrieving individual entity instances.
     * </p>
     *
     * @param id entity id
     * @return
     */
    @GET
    @javax.ws.rs.Path("/{id:[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSingleInstance(@Context UriInfo uriInfo,
                                      @PathParam("id") String id,
                                      @QueryParam("fields") String fieldList,
                                      @QueryParam("collections") String collectionsList) {
        String[] fields = fieldList == null ? makeDefaultSelectionFields(entityClass) : fieldList.split(",");
        List<String> listWithId = new ArrayList<>(fields.length + 1);
        listWithId.addAll(Arrays.asList(fields));
        listWithId.add("id"); // NOTE : this is a must .... coz of the collections thingy
        fields = listWithId.toArray(new String[]{});
        final CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        final CriteriaQuery<Tuple> cq = cb.createTupleQuery();
        Root<T> root = cq.from(entityClass);
        Predicate condition = cb.equal(root.get("id"), id);
        List<Selection<?>> selections = getSelections(fields, root);
        cq.multiselect(selections).where(condition);
        final TypedQuery<Tuple> query = getEntityManager().createQuery(cq);
        final List<Tuple> tuples = query.getResultList();

        if (tuples.size() == 0) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        GenericDTO dto = getGenericDTO(entityClass, fields, tuples.get(0).toArray());
        T entity = gService.find(id, entityClass);
        log.info("collections param " + collectionsList);
        if (collectionsList != null) {
            //@OneToMany
            //private Collection<GeoCity> geoCities;
            //collections=invoiceLines(x,y,z),foobarz(m,n,o)
            int length = collectionsList.length();
            List<String> colls = new ArrayList<>();
            int start = 0;
            int firstClosingBrace = 0;
            if (collectionsList.contains("(")) {
                for (; firstClosingBrace < length; ) {
                    firstClosingBrace = collectionsList.indexOf(")", start);
                    if (firstClosingBrace < 0) break;
                    firstClosingBrace += 1;
                    System.out.println("closing " + firstClosingBrace);
                    String substring = collectionsList.substring(start, firstClosingBrace);
                    System.out.println(substring);
                    colls.add(substring);
                    start = firstClosingBrace + 1; //the ,
                }
            }
            System.out.println("collection string " + colls + " colls size " + colls.size());
            //try see if we have collectionsOrderBy, something that looks like below
            //collectionsOrderBy=items1|-date,product.name&collectionsOrderBy=items2|name,age
            Map<String, String[]> collectionOrders = new HashMap<>();
            List<String> collectionOrderList = uriInfo.getQueryParameters().get("collectionsOrderBy");
            if (collectionOrderList != null) {
                if(!collectionOrderList.isEmpty()){
                    //now process the order by
                    for(String collectionOrder : collectionOrderList){
                        String[] orderArgs = collectionOrder.split("\\|");
                        log.info("orderArgs : " + Arrays.asList(orderArgs));
                        String collName = orderArgs[0];
                        String[] orderFields = orderArgs[1].split(",");
                        collectionOrders.put(collName, orderFields);
                    }
                }
            }
            log.info("The collection order map : " + collectionOrders);

            String[] collectionFields = colls.toArray(new String[]{});
            log.info("FIELDS " + Arrays.asList(collectionFields) + " fields size " + collectionFields.length);
            for (String collectionField : collectionFields) {
                //invoiceLines(x,y,z)
                String[] childFields = null;
                String relationName = null;
                if (collectionField.contains("(")) {
                    log.info("collectionField " + collectionField);
                    int startIndex = collectionField.indexOf("(");
                    int endIndex = collectionField.lastIndexOf(")");
                    relationName = collectionField.substring(0, startIndex);
                    log.info("RELATION NAME " + relationName);
                    log.info(" START INDEX " + startIndex + " END " + endIndex);
                    final String substring = collectionField.substring(startIndex + 1, endIndex);
                    System.out.println("subs " + substring);
                    //id,index,name,wfTask:owner<firstName;lastName>
                    childFields = substring.substring(0).split(",");
                }
                log.info("RELATION FIELDS " + Arrays.asList(childFields) + " size " + childFields.length);
                //get the relation class
                Field relationField = Reflections.getField(entityClass, relationName);
                //ha, note that fieldType is a generic collection, we need the contained element
                //Collection<GeoCity> ww....
                Type type = relationField.getGenericType();
                if (type instanceof ParameterizedType) {
                    ParameterizedType pType = (ParameterizedType) type;
                    Type arr = pType.getActualTypeArguments()[0];
                    Class<?> childClazz = (Class<?>) arr;
                    if (childFields == null) {
                        log.info(" no child collection fields specified, we manufacture default list");
                        childFields = makeDefaultSelectionFields(childClazz);
                        log.info(" manufactured fields " + Arrays.asList(childFields));
                    }
                    //get the name of the corresponding @ManyToOne field on the child
                    //we now proceed to create the query for said child
                    final String parentClassName = entityClass.getSimpleName();
                    final String parentIdPath = parentClassName.substring(0, 1).toLowerCase() + parentClassName.substring(1);
                    String parentId = (String) dto.get("id").getValue();
                    log.info(" Child Class " + childClazz.getName() + " , Parent ID Path " + parentIdPath + " parent Id " + parentId + " Child Fields " + Arrays.asList(childFields) + " " +
                            "child fields size " + childFields.length);
                    //check if order fields is present new LinkedList<String>(Arrays.asList(split))
                    String[] orderFields = (collectionOrders.containsKey(relationName)? collectionOrders.get(relationName) : new String[0]);
                    List<GenericDTO> childDTOs = getChild((Class<T>) childClazz, childFields, parentIdPath, entity, orderFields);
                    for (GenericDTO child : childDTOs) {
                        dto.addRelation(relationName, child);
                    }

                } else {
                    //we are cooked...
                    throw new IllegalStateException(" type of collection is wrong ' " + type + "' ");
                }
            }
        }
        return Response.ok(dto).build();
    }

    private List<GenericDTO> getChild(Class<T> childClazz, String[] fields, String parentIdPath, T parent, String[] orderFields) {
        log.info("getting the children ,  childClazz " + childClazz.getSimpleName() + " fields " + Arrays.asList(fields) + " " +
                " parentIdPath " + parentIdPath + " parent Id " + parent);
        final CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        final CriteriaQuery<Tuple> cq = cb.createTupleQuery();
        Root<T> root = cq.from(childClazz);
        List<Selection<?>> selections = getSelections(fields, root);
        log.info("created selections size " + selections.size());
        Predicate condition = null;
        try{
            condition = cb.equal(root.get(parentIdPath), parent);
        }catch (IllegalArgumentException ex){
            //lets try find the referenced property in the parent
            String e = entityClass.getSuperclass().getSimpleName();
            parentIdPath = e.substring(0, 1).toLowerCase() + e.substring(1);
            condition = cb.equal(root.get(parentIdPath), parent);
        }
        cq.multiselect(selections).where(condition);
        //List<String> orderBy = new ArrayList<>();
        List<Order> od = getOrders(new LinkedList<>(Arrays.asList(orderFields)), cb, root);
        cq.orderBy(od);
        //execute query
        Query q = getEntityManager().createQuery(cq);
        log.info(" max results " + 100);
        q.setMaxResults(100);
        List<Tuple> tupleResult = q.getResultList();
        log.info("results obtained , size " + tupleResult.size());
        //format the output
        List<GenericDTO> dtos = Collections.EMPTY_LIST;
        try {
            dtos = getGenericDTOs(fields, tupleResult, childClazz);
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File |//TODO| File Templates.
        }
        return dtos;
    }

    @POST
    @javax.ws.rs.Path("/{id:[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateState(@Context HttpHeaders headers,
                                @PathParam("id") String id,
                                GenericDTO entityDTO) {
        log.info("<<<<<<<<<<<< entityDTo : " + entityDTO + " >>>>>>>>>>>>>>>>>>>>");
        List<Locale> acceptedLanguages = headers.getAcceptableLanguages();
        log.info("==> Accepted languages " + acceptedLanguages);
        if ((acceptedLanguages != null) && (!acceptedLanguages.isEmpty())) {
            interpolator.setDefaultLocale(acceptedLanguages.get(0));
        }

        Response.ResponseBuilder builder = null;
        //check if it exists
        log.info("check if the entity exist ' " + entityClass + "'");
        T dbEntity = getEntityManager().find(entityClass, id);
        if (dbEntity == null) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }

        log.info("Found the entity with id = ' " + dbEntity.getId() + "'");

        try {

            T entity = fromDTO(entityDTO, entityClass);
            log.info(" The  DTO ' " + entityDTO + " ' , entity from DTO ' " + entity + " ' ");
            validateEntity(entity);
            extraValidations(entity);

            //perform business validation
            validatorValidation(entity, ValidateAt.UPDATE);

            //entity callbacks
            invokeEntityCallbacks(entity, During.UPDATE);

            //u need to set clone the other attributes that are not in entity but
            //are in the database
            log.info("******** DB STATE ***********");
            dumpEntity(dbEntity);
            log.info("******************************");

            log.info("******** DTO STATE ***********");
            dumpEntity(entity);
            log.info("******************************");
            entity = (T) ReflectionUtils.diff(entity, dbEntity);

            log.info("******** AFTER MERGING STATE ***********");
            dumpEntity(entity);
            log.info("******************************");

            //log.info(" the full entity for update ' " + toDTO(entity, new String[]{}, entityClass));
            entity.setVersion(dbEntity.getVersion());    //set version
            //update the children
            entity = updateChildren(entity);
            log.info("******** AFTER UPDATING CHILDREN ***********");
            dumpEntity(entity);
            log.info("******************************");
            beforeSaveOrUpdate(entity);
            T updatedEntity = merge(entity);
            Collection<String> fieldNames = GenericDTO.fieldNames(entityDTO);
            fieldNames.add("id");       //add the id field
            builder = Response.created(URI.create(uriInfo.getAbsolutePath() + "/" + updatedEntity.getId().toString()))
                    .entity(toDTO(updatedEntity, fieldNames.toArray(new String[fieldNames.size()]), entityClass));

            //we raise event
            //entityEventSrc.fire(updatedEntity);
            entityUpdatedEventSrc.fire(new EntityUpdatedEvent<>(updatedEntity));
        } catch (ConstraintViolationException ce) {
            ce.printStackTrace();
            // Handle bean validation issues
            builder = createViolationResponse(ce.getConstraintViolations());
        } catch (ValidationException e) {
            e.printStackTrace();
            // Handle the unique constrain violation
            builder = createValidationResponse(e);
        } catch (InvocationTargetException ite){
            ite.printStackTrace();
            if(ite.getCause() instanceof ValidationException){
                builder = createValidationResponse(ite);
            }
        } catch (Exception e) {
            e.printStackTrace();
            // Handle generic exceptions
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("error", e.getMessage());
            builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
        }
        return builder.build();
    }

    /**
     * update the children nodes to recent version
     *
     * @param entity
     * @return
     */
    private T updateChildren(T entity) {
        List<Field> relationFields = Reflections.getFields(entity.getClass(), OneToMany.class);
        log.info("OneToMany fields " + relationFields);
        if (relationFields.size() == 0) return entity;
        for (Field field : relationFields) {
            Collection targets = new ArrayList(relationFields.size());
            Type type = field.getGenericType();
            if (type instanceof ParameterizedType) {
                ParameterizedType pType = (ParameterizedType) type;
                Type arr = pType.getActualTypeArguments()[0];
                Class<?> childClazz = (Class<?>) arr;
                Collection children = (Collection) Reflections.getAndWrap(field, entity);
                for (Object child : children) {
                    if (child instanceof ModelBase) {
                        log.info("-----------------------===== child instanceof ModelBase =====---------------");
                        ModelBase childEntity = (ModelBase) child;
                        String childId = childEntity.getId();
                        if (childId != null) {
                            ModelBase dbEntity = (ModelBase) getEntityManager().find(childClazz, childId);
                            if (dbEntity != null) {
                                child = ReflectionUtils.diff(child, dbEntity);
                                ((ModelBase) child).setVersion(dbEntity.getVersion());
                            }
                        }
                        targets.add(child);
                    }
                }
            }
            if (targets.size() > 0) {
                Reflections.setAndWrap(field, entity, targets);
            }
        }
        return entity;
    }


    /**
     * Creates a new state from the values provided. Performs validation, and will return a JAX-RS response with either 200 ok,
     * or with a map of fields, and related errors.
     */
    @POST
    @Permission(suffix = ".save")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createState(@Context HttpHeaders headers, GenericDTO entityDTO) {
        Response.ResponseBuilder builder = null;

        try {
            //make the entity from the dto
            T entity = fromDTO(entityDTO, entityClass);
            dumpEntity(entity);

            // Validates entity using bean validation
            List<Locale> acceptedLanguages = headers.getAcceptableLanguages();
            log.info("==> Accepted languages " + acceptedLanguages);
            if ((acceptedLanguages != null) && (!acceptedLanguages.isEmpty())) {
                interpolator.setDefaultLocale(acceptedLanguages.get(0));
            }
            validateEntity(entity);
            extraValidations(entity);

            //perform validator validation
            validatorValidation(entity, ValidateAt.CREATE);

            //entity callbacks
            invokeEntityCallbacks(entity, During.CREATE);

            beforeSaveOrUpdate(entity);
            T createdState = persist(entity);

            log.info(" the PERSISTED entity " + createdState);

            Collection<String> fieldNames = GenericDTO.fieldNames(entityDTO);
            fieldNames.add("id");       //add the id field
            builder = Response.created(URI.create(uriInfo.getAbsolutePath() + "/" + createdState.getId().toString()))
                    .entity(toDTO(createdState, fieldNames.toArray(new String[fieldNames.size()]), entityClass));
            entityCreatedEventSrc.fire(new EntityCreatedEvent<>(createdState));
        } catch (ConstraintViolationException ce) {
            ce.printStackTrace();
            // Handle bean validation issues
            builder = createViolationResponse(ce.getConstraintViolations());
        } catch (ValidationException e) {
            e.printStackTrace();
            // Handle the unique constrain violation
            builder = createValidationResponse(e);
        } catch (InvocationTargetException ite){
            ite.printStackTrace();
            if(ite.getCause() instanceof ValidationException){
                builder = createValidationResponse(ite);
            }
        } catch (Exception e) {
            e.printStackTrace();
            // Handle generic exceptions
            e.printStackTrace();
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("error", StackTraceUtil.getStackTrace(e));
            builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
        }

        return builder.build();
    }

    private void invokeEntityCallbacks(T entity, During when) throws InvocationTargetException, IllegalAccessException {
        //entity callbacks
        List<Method> validationMethods = Reflections.getMethodsAnnotatedWith(entityClass, EntityCallbackMethod.class);
        log.info("Invoking callback methods on the entity '" + entityClass + "', when : " + when);
        for(Method m : validationMethods){
            //invoke them methods
            System.out.println("Method " + m.getName());
            Set<During> validateAts = new HashSet<>(Arrays.asList(m.getAnnotation(EntityCallbackMethod.class).when()));
            if(m.getAnnotation(EntityCallbackMethod.class).enabled()
                    && (validateAts.contains(when) || validateAts.contains(During.ALWAYS))) m.invoke(entity);
        }
        //validator class
        if(entityClass.isAnnotationPresent(EntityCallbackClass.class)){
            Class validatorClass = (entityClass.getDeclaredAnnotation(EntityCallbackClass.class)).value();
            log.info("Running callbacks on the callback class : " + validatorClass);
            Object validator = CDI.current().select(validatorClass).get();
            //Object validator = getBean(validatorClass);
            List<Method> validatorValidationMethods = Reflections.getMethodsAnnotatedWith(validatorClass, EntityCallbackMethod.class);
            for(Method m : validatorValidationMethods){
                //invoke them methods
                Set<During> validateAts = new HashSet<>(Arrays.asList(m.getAnnotation(EntityCallbackMethod.class).when()));
                if(m.getAnnotation(EntityCallbackMethod.class).enabled()
                        && (validateAts.contains(when) || validateAts.contains(ValidateAt.ALWAYS))) m.invoke(validator, entity);
            }
        }
    }

    protected Response.ResponseBuilder createValidationResponse(InvocationTargetException ite) {
        Response.ResponseBuilder builder;Map<String, String> responseObj = new HashMap<>();
        responseObj.put("error", ite.getCause().getMessage());
        builder = Response.status(Response.Status.CONFLICT).entity(responseObj);
        return builder;
    }

    private void validatorValidation(T entity, ValidateAt validateAt) throws IllegalAccessException, InvocationTargetException {
        //business logic validation
        List<Method> validationMethods = Reflections.getMethodsAnnotatedWith(entityClass, ValidationMethod.class);
        log.info("Invoking validation methods on the entity '" + entityClass + "', validateAt : " + validateAt);
        for(Method m : validationMethods){
            //invoke them methods
            System.out.println("Method " + m.getName());
            Set<ValidateAt> validateAts = new HashSet<>(Arrays.asList(m.getAnnotation(ValidationMethod.class).when()));
            log.info("validateAts : " + validateAts);
            if(m.getAnnotation(ValidationMethod.class).enabled()
                    && (validateAts.contains(validateAt) || validateAts.contains(ValidateAt.ALWAYS))) m.invoke(entity);
        }
        //validator class
        if(entityClass.isAnnotationPresent(ValidatorClass.class)){
            Class validatorClass = (entityClass.getDeclaredAnnotation(ValidatorClass.class)).value();
            log.info("Running validations on the validator class : " + validatorClass);
            Object validator = CDI.current().select(validatorClass).get();
            //Object validator = getBean(validatorClass);
            List<Method> validatorValidationMethods = Reflections.getMethodsAnnotatedWith(validatorClass, ValidationMethod.class);
            for(Method m : validatorValidationMethods){
                //invoke them methods
                Set<ValidateAt> validateAts = new HashSet<>(Arrays.asList(m.getAnnotation(ValidationMethod.class).when()));
                if(m.getAnnotation(ValidationMethod.class).enabled()
                        && (validateAts.contains(validateAt) || validateAts.contains(ValidateAt.ALWAYS))) m.invoke(validator, entity);
            }
        }
    }

    public <T> T getBean(Class<T> clazz) {
        Bean<T> bean = (Bean<T>) beanManager.getBeans(clazz).iterator().next();
        CreationalContext<T> ctx = beanManager.createCreationalContext(bean);
        T object = (T) beanManager.getReference(bean, clazz, ctx);
        return object;
    }

    protected Response.ResponseBuilder createValidationResponse(ValidationException e) {
        Response.ResponseBuilder builder;Map<String, String> responseObj = new HashMap<>();
        responseObj.put("Validation error", e.getMessage());
        builder = Response.status(Response.Status.CONFLICT).entity(responseObj);
        return builder;
    }

    @Deprecated
    public void beforeSaveOrUpdate(T entity){
        //just a place holder, return
        return;
    }

    protected void dumpEntity(T entity) {
      /*  XStream xstream = new XStream(new Sun14ReflectionProvider(
                new FieldDictionary(new ImmutableFieldKeySorter())),
                new DomDriver("utf-8"));
        log.info(xstream.toXML(entity));*/

       /* XStream xstream = new XStream(new JsonHierarchicalStreamDriver() {
            public HierarchicalStreamWriter createWriter(Writer writer) {
                return new JsonWriter(writer, JsonWriter.DROP_ROOT_MODE);
            }
        });

        log.info(xstream.toXML(entity));*/
    }

    /**
     * TODO
     * when u post an entity with @manyToOne relationship,
     * and said children don't exists in the db (i.e don't hav id in the json)
     * ...i need to detect that and persist the children and then the parent
     * and set the ids appropriately.
     *
     * @param entity
     * @return
     * @throws Exception
     */
    protected T persist(T entity) throws Exception {
        log.info(" persist the entity " + entity);
        return gService.persist(entity);
    }

    /**
     * specifically call this method to update and entity
     * that already exists in the database, otherwise
     * use the above persist for new entities
     * @param entity
     * @return
     * @throws Exception
     */
    protected T merge(T entity) throws Exception {
        log.info(" merging the entity " + entity);
        return gService.edit(entity);
    }


    /**
     * <p>
     * Validates the given Member variable and throws validation exceptions based on the type of error. If the error is standard
     * bean validation errors then it will throw a ConstraintValidationException with the set of the constraints violated.
     * </p>
     * <p>
     * If the error is caused because an existing member with the same email is registered it throws a regular validation
     * exception so that it can be interpreted separately.
     * </p>
     *
     * @param state to be validated
     * @throws javax.validation.ConstraintViolationException
     *          If Bean Validation errors exist
     * @throws javax.validation.ValidationException
     *          If member with the same email already exists
     */
    protected void validateEntity(T state) throws ValidationException {

        Set<ConstraintViolation<T>> violations = validator.validate(state);

        if (!violations.isEmpty()) {
            throw new ConstraintViolationException(new HashSet<ConstraintViolation<?>>(violations));
        }

    }


    /**
     * <p>
     * Subclasses may choose to add extra validations by overriding this method.
     * </p>
     *
     * @param entity the entity for further validations
     */
    @Deprecated
    protected void extraValidations(T entity) {
        //see subclass
    }

    /**
     * Creates a JAX-RS "Bad Request" response including a map of all violation fields, and their message. This can then be used
     * by clients to show violations.
     *
     * @param violations A set of violations that needs to be reported
     * @return JAX-RS response containing all violations
     */
    protected Response.ResponseBuilder createViolationResponse(Set<ConstraintViolation<?>> violations) {
        log.info("Validation completed. violations found: " + violations.size());

        Map<String, String> responseObj = new HashMap<>();

        for (ConstraintViolation<?> violation : violations) {
            responseObj.put(violation.getPropertyPath().toString(), violation.getMessage());
        }

        return Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
    }


    protected String[] makeDefaultSelectionFields(Class<?> entityType) {
        List<String> defaults = ReflectionUtils.primitiveFieldNames(entityType);
        //defaults.add("id");
        log.info("the defaults, in makeDefaultSelectionFields " + defaults);
        String[] fieldNames = new String[defaults.size()];
        defaults.toArray(fieldNames);
        return fieldNames;
    }

    /**
     * Make a  class T from dto
     *
     * @param genericDTO  from which to make the class
     * @param entityClass the type
     * @param <T>         returned class type
     * @return
     * @throws Exception
     */
    public <T> T fromDTO(GenericDTO genericDTO, Class<T> entityClass) throws Exception {
        //validate the name
        log.info("genericDTO.getName() : " + genericDTO.getName());
        log.info("entityClass.getCanonicalName() : " + entityClass.getCanonicalName());
        if (genericDTO.getName().compareTo(entityClass.getCanonicalName()) != 0) {
            throw new IllegalStateException("DTO Name = " + genericDTO.getName() + " , class Name  " + entityClass.getCanonicalName());
        }
        //create the fully formed entity T
        T entity = entityClass.newInstance();

        //attributes
        Set<Map.Entry<String, com.misoft.hospital.data.gdto.Attribute>> allAttributes = genericDTO.attributeMap().entrySet();
        log.info(" the attributes " + allAttributes);
        for (Map.Entry<String, com.misoft.hospital.data.gdto.Attribute> entry : allAttributes) {
            log.info("current field " + entry.getKey());
            final String fieldName = entry.getKey().trim();
            final com.misoft.hospital.data.gdto.Attribute attribute = entry.getValue();
            Field targetField = Reflections.getField(entityClass, fieldName);
            Class<?> targetFieldType = targetField.getType();

            if (attribute instanceof DurationType) {
                DurationType v = (DurationType) attribute;
                Reflections.setAndWrap(targetField, entity, v.getValue());
            } else if (attribute instanceof CalendarDateType) {
                CalendarDateType v = (CalendarDateType) attribute;
                Reflections.setAndWrap(targetField, entity, v.getValue());
            } else if (attribute instanceof MoneyType) {
                MoneyType v = (MoneyType) attribute;
                Reflections.setAndWrap(targetField, entity, v.getValue());
            } else if (attribute instanceof BigDecimalType) {
                BigDecimalType v = (BigDecimalType) attribute;
                Reflections.setAndWrap(targetField, entity, v.getValue());
            } else if (attribute instanceof BigIntegerType) {
                BigIntegerType v = (BigIntegerType) attribute;
                Reflections.setAndWrap(targetField, entity, v.getValue());
            } else if (attribute instanceof BooleanType) {
                BooleanType v = (BooleanType) attribute;
                Reflections.setAndWrap(targetField, entity, v.getValue());
            } else if (attribute instanceof DateType) {
                DateType v = (DateType) attribute;
                if (Calendar.class.isAssignableFrom(targetFieldType)) {
                    Calendar calendar = ReflectionUtils.dateToCalendar(v.getValue());
                    Reflections.setAndWrap(targetField, entity, calendar);
                } else if (Date.class.isAssignableFrom(targetFieldType)) {
                    Reflections.setAndWrap(targetField, entity, v.getValue());
                } else if (java.sql.Date.class.isAssignableFrom(targetFieldType)) {
                    java.sql.Date sqlDate = new java.sql.Date(v.getValue().getTime());
                    Reflections.setAndWrap(targetField, entity, sqlDate);
                } else if (Time.class.isAssignableFrom(targetFieldType)) {
                    Time sqlTime = new Time(v.getValue().getTime());
                    Reflections.setAndWrap(targetField, entity, sqlTime);
                } else {
                    throw new IllegalStateException("unknown date field name " + fieldName + "  target type  " + targetFieldType + " value " + attribute.getValue());
                }
            } else if (attribute instanceof DoubleType) {
                DoubleType v = (DoubleType) attribute;
                Reflections.setAndWrap(targetField, entity, v.getValue());
            } else if (attribute instanceof FloatType) {
                FloatType v = (FloatType) attribute;
                Reflections.setAndWrap(targetField, entity, v.getValue());
            } else if (attribute instanceof IntType) {
                IntType v = (IntType) attribute;
                Reflections.setAndWrap(targetField, entity, v.getValue());
            } else if (attribute instanceof LongType) {
                LongType v = (LongType) attribute;
                Reflections.setAndWrap(targetField, entity, v.getValue());
            } else if (attribute instanceof StringType) {
                StringType v = (StringType) attribute;
                String value = v.getValue();
                //could be an enum field
                if (targetFieldType.isEnum() && value != null) {
                    //create the said enum , with the given string ..
                    Enum value1 = Enum.valueOf((Class<Enum>) targetFieldType, value);
                    Reflections.setAndWrap(targetField, entity, value1);
                } else {
                    Reflections.setAndWrap(targetField, entity, value);
                }
            } else if (attribute == null) {
                //use m to check for the types as fall back plan :-)
                log.info("<<<<<<<<<<<<<<<<<<<<<<<<<<< target field value [" + targetField + "] was null!");
                Reflections.setAndWrap(targetField, entity, null);

            } else {
                throw new IllegalStateException("unknown field type " + targetFieldType + " value is " + entry);
            }
        }

        //ManyToOne
        Set<Map.Entry<String, GenericDTO>> r2 = genericDTO.getRelations2().entrySet();
        log.info(" the @ManyToOne attributes " + r2);
        for (Map.Entry<String, GenericDTO> entry : r2) {
            final String key = entry.getKey();
            final GenericDTO val = entry.getValue();
            final com.misoft.hospital.data.gdto.Attribute<Object> idAttribute = val.get("id");
            if (idAttribute != null) {
                log.info("we have an id attribute " + idAttribute);
                Object idValue = idAttribute.getValue();
                if(idValue == null){
                    log.info("the specified \'id\' for the @ManyToOne attribute is null " + idAttribute);
                    continue;
                }
                log.info("checking if the entity ' " + val.getName() + "' exists");
                Class<? extends ModelBase> childEntityClass = (Class<? extends ModelBase>) Class.forName(val.getName());
                ModelBase childDbEntity = getEntityManager().find(childEntityClass, idValue);
                if (childDbEntity == null) {
                    log.info(" the entity ' " + childEntityClass + "' with id ' " + idValue + "' does not exist in the db ");
                    throw new WebApplicationException(Response.Status.NOT_FOUND);
                }
                log.info("Found the entity with id = ' " + childDbEntity + "'");
                //TODO add child updates here ...
                //ModelBase childEntity = fromDTO(val, childEntityClass);
                //log.info(" The  Child DTO ' " + val + " ' , entity from DTO ' " + childEntity + " ' ");
                //childEntity = (ModelBase) ReflectionUtils.diff(childEntity, childDbEntity);
                //log.info(" the full entity for update ' " + toDTO(childEntity, new String[]{}, childEntityClass));
                //childEntity.setVersion(childDbEntity.getVersion());    //set version
                Field field = Reflections.getField(entityClass, key);
                //Class<?> fieldType = field.getType();
                log.info(" Field " + field.getName() + ", entity " + entity + " , child db entity " + childDbEntity);
                Reflections.setAndWrap(field, entity, childDbEntity);
                //log.info("Clazz " + fieldType.getName()+", methods " + Arrays.asList(fieldType.getMethods()));
                //Method setter = Reflections.getSetterMethod(fieldType,field.getName());
                //Reflections.invokeAndWrap(setter,entity,childDbEntity);
                System.out.println("here.....");
            } else {
                Field field = Reflections.getField(entityClass, key);
                Class<?> fieldType = field.getType();
                Reflections.setAndWrap(field, entity, fromDTO(val, fieldType));
            }

        }
        //relations
        Set<Map.Entry<String, Set<GenericDTO>>> allRelations = genericDTO.getRelations().entrySet();
        for (Map.Entry<String, Set<GenericDTO>> entry : allRelations) {
            Field field = Reflections.getField(entityClass, entry.getKey());
            Class<?> fieldType = field.getType();
            Collection targets = new ArrayList();
            //check if fieldType is a collection
            if (ReflectionUtils.isCollectionField(field)) {
                //get the collection type and then create it and set it
                for (GenericDTO dto : entry.getValue()) {
                    //ha, note that fieldType is a generic collection, we need the contained element
                    //Collection<GeoCity> ww....
                    Type type = field.getGenericType();
                    if (type instanceof ParameterizedType) {
                        ParameterizedType pType = (ParameterizedType) type;
                        Type arr = pType.getActualTypeArguments()[0];
                        Class<?> clzz = (Class<?>) arr;
                        Object child = fromDTO(dto, clzz);
                        //set the child's parent to be entity
                        child = setChildParent(child, entity);
                        //call the add<ChildEntityName> -- method
                        String methodName = "add" + child.getClass().getSimpleName();
                        Method csMethod = null;
                        try {
                            csMethod = entityClass.getDeclaredMethod(methodName, clzz);
                        } catch (NoSuchMethodException | SecurityException e) {
                            e.printStackTrace();
                        }
                        if (csMethod != null) {
                            Reflections.invoke(csMethod, entity, child);
                        }
                        //--end the  add<ChildEntityName>  logic
                        targets.add(child);
                    } else {
                        //we are cooked...
                        throw new IllegalStateException(" type of collection is wrong ' " + type + "' ");
                    }

                }
                Reflections.setAndWrap(field, entity, targets);


            } else {
                //a single instance
                //TODO check for errors in the iterator
                GenericDTO val = entry.getValue().iterator().next();
                Reflections.setAndWrap(field, entity, fromDTO(val, fieldType));
            }

        }
        return entity;
    }

    /**
     * set the child's parent
     *
     * @param child
     * @param parentEntity
     * @param <T>
     * @return
     */
    private <T> Object setChildParent(Object child, T parentEntity) {
        String parentClassName = parentEntity.getClass().getSimpleName();
        String fieldName = parentClassName.substring(0, 1).toLowerCase() + parentClassName.substring(1);
        Field field = null;
        try{
            field = Reflections.getField(child.getClass(), fieldName);
        }catch (Exception nsfe){
            nsfe.printStackTrace();
            log.info(" The field named " + fieldName + " can not be found on " +
                    "class " + child.getClass().getName() + ", trying to find it from the parent class...");
            //try the field of the parent of parent
            parentClassName = parentEntity.getClass().getSuperclass().getSimpleName();
            fieldName = parentClassName.substring(0, 1).toLowerCase() + parentClassName.substring(1);
            field = Reflections.getField(child.getClass(), fieldName);
            if (field == null) {
                throw new IllegalArgumentException(" The field named " + fieldName + " can not be found on " +
                        "class " + child.getClass().getName() + " (even from the parent class!!)");
            }
        }

        ManyToOne manyToOne = field.getAnnotation(ManyToOne.class);
        if (manyToOne == null) {
            throw new IllegalArgumentException(" The field named " + fieldName + " on class " + child.getClass() + " does " +
                    "not have ManyToOne annotation");
        }
        Reflections.setAndWrap(field, child, parentEntity);
        return child;
    }


    /**
     * Covert the entity into a dto
     * Optionally, you can specify the fields u need, else pass an empty array.
     * Indicate relations using <code>fieldName:attributeName</code>
     *
     * @param entity
     * @param fieldNames
     * @param entityClass
     * @param <T>
     * @return
     * @throws Exception
     */
    public <T extends ModelBase> GenericDTO toDTO(T entity, String[] fieldNames, Class<T> entityClass) throws Exception {
        //create the fully formed dto from T
        log.info("Entity class " + entity);
        //all the field names for entity
        String[] fields = fieldNames.length == 0 ? makeDefaultSelectionFields(entity.getClass()) : fieldNames;
        fields = cleanUpFields(fields);
        log.info("The fields " + Arrays.asList(fields));
        Object[] row = collectFieldValues(fields, entity);
        dumpRow(fields, row);

        GenericDTO dto = getGenericDTO(entityClass, fields, row);
        return dto;
    }

    /**
     * TODO softRemove spaces, and also check for any weired character
     *
     * @param fields
     * @return
     */
    private String[] cleanUpFields(String[] fields) {
        List<String> cleaned = new ArrayList<>(fields.length);
        for (String field : fields) {
            cleaned.add(field.trim());
        }
        return cleaned.toArray(new String[fields.length]);
    }

    private void dumpRow(String[] fields, Object[] row) {
        System.out.println("********** DUMPING ROW **************");
        assert fields.length == row.length;
        for (int i = 0; i < fields.length; i++) {
            String ss = " Field name => " + fields[i];
            final Object o = row[i];
            final String x = o == null ? "  <NULL>  " : "  :  " + o + " : type " + o.getClass().getCanonicalName();
            System.out.println(ss + x);
        }

        System.out.println("********** END DUMPING ROW **************");
    }

    public Object[] collectFieldValues(String[] fieldsNames, Object entity) {
        List<Object> values = new ArrayList<>(fieldsNames.length);
        for (String fieldName : fieldsNames) {
            Object val = collectFieldValue(entity, fieldName);
            values.add(val);
        }
        return values.toArray(new Object[values.size()]);
    }

    private Object collectFieldValue(Object entity, String fieldName) {
        log.info(" Entity " + entity + " field name " + fieldName);
        Object val;
        if(entity == null) return null;
        else if (fieldName.contains(":")) {
            String[] attrs = fieldName.split(":");
            String fn = attrs[0];
            Field relationObjectField = Reflections.getField(entity.getClass(), fn);
            Object relationObjectFieldValue = Reflections.getAndWrap(relationObjectField, entity);
            val = collectFieldValue(relationObjectFieldValue, attrs[1]);
        } else {
            log.info("entity class " + entity);
            final Method getterMethod = Reflections.getGetterMethod(entity.getClass(), fieldName);
            log.info("getter  " + getterMethod);
            val = Reflections.invokeAndWrap(getterMethod, entity);
            log.info(" the val " + val);
        }
        return val;
    }

    public static class EntityCreatedEvent<T extends ModelBase> {
        private final T entity;

        public EntityCreatedEvent(T entity) {
            this.entity = entity;
        }

        public T getEntity() {
            return entity;
        }
    }

    public static class EntityUpdatedEvent<T extends ModelBase> {
        private final T entity;

        public EntityUpdatedEvent(T entity) {
            this.entity = entity;
        }

        public T getEntity() {
            return entity;
        }
    }
}
