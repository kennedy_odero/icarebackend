package com.misoft.hospital.rest.stats;

import com.misoft.hospital.model.stats.Interval;
import com.misoft.hospital.util.types.DateParam;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Logger;

/**
 * Created by kodero on 12/29/15.
 */
@Stateless
@Path("/revenueStats")
public class RevenueStatsResource {
    @Inject
    EntityManager em;

    @Inject
    protected Logger log;

    @GET
    @Path("/overall/{asAt}")
    public Response getOverallRevenue(@PathParam("asAt") DateParam asAt,
                                      @QueryParam("interval") @DefaultValue("DAY") Interval interval,
                                      @QueryParam("groupBy") String groupBy){
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String sql = "select \n" +
                "\tsc.name,\n" +
                "\tsum(coalesce(pb.amount_billed, 0) - coalesce((select coalesce(bw.amount_waived, 0) from bill_waivers bw where bw.patient_bill = pb.id),0)) billed,\n" +
                "    \tsum(bp.amount_settled) collected\n" +
                "from patient_bills pb \n" +
                "\tjoin service_requests sr \n" +
                "\t\ton pb.service_request = sr.id\n" +
                "\tjoin service_room sc \n" +
                "\t\ton sr.svc_center = sc.id\n" +
                "\tjoin bill_payments bp \n" +
                "    \t\ton bp.patient_bill = pb.id\n" +
                "where pb.deleted <> 1 \n" +
                "\tand pb.date_generated between '" + df.format(asAt.getDate()) + "' and '" + df.format(asAt.getDate()) + "'";
        if(groupBy != null) sql = sql + "group by " + groupBy;
        Query q = em.createNativeQuery(sql);
        List<Object[]> res = q.getResultList();

        Iterator<Object[]> it = res.iterator();
        List statLines = new ArrayList();
        List<Map> res2 = new ArrayList<>();
        while(it.hasNext()){
            Map<String,Object> stats = new HashMap<>();
            Object[] row = it.next();
            List<Map<String, Object>> statLine = new ArrayList<>();
            Map billedMap = new HashMap(); billedMap.put("label", "Billed"); billedMap.put("value", row[1]); statLine.add(billedMap);
            Map collectedMap = new HashMap(); collectedMap.put("label", "Collected"); collectedMap.put("value", row[2]); statLine.add(collectedMap);
            statLines.add(statLine);
            stats.put("key", row[0]);
            stats.put("values", statLine);
            res2.add(stats);
        }
        //stats.put("values", statLines);
        return Response.ok(res2).build();
    }
}
