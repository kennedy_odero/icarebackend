package com.misoft.hospital.rest.stats;

import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by kodero on 1/24/16.
 */
@Stateless
@Path("/osStats")
public class OperatingSystemStatsResource {

    @GET
    @Path("/memory")
    public Response getMemoryStats(){
        Runtime runtime = Runtime.getRuntime();

        long maxMemory = runtime.maxMemory();
        long allocatedMemory = runtime.totalMemory();
        long freeMemory = runtime.freeMemory();
        //construct response
        List statLines = new ArrayList();
        List<Map> res2 = new ArrayList<>();
        Map<String,Object> stats = new HashMap<>();
        List<Map<String, Object>> statLine = new ArrayList<>();
        Map freeMap = new HashMap(); freeMap.put("label", "Free"); freeMap.put("value", (freeMemory / 1024)); statLine.add(freeMap);
        Map allocatedMap = new HashMap(); allocatedMap.put("label", "Allocated"); allocatedMap.put("value", (allocatedMemory / 1024)); statLine.add(allocatedMap);
        Map maxMap = new HashMap(); maxMap.put("label", "Maximum"); maxMap.put("value", (maxMemory / 1024)); statLine.add(maxMap);
        Map totalFree = new HashMap(); totalFree.put("label", "TotalFree"); totalFree.put("value", ((freeMemory + (maxMemory - allocatedMemory)) / 1024)); statLine.add(totalFree);
        statLines.add(statLine);
        stats.put("key", "Memory Usage");
        stats.put("values", statLine);
        res2.add(stats);
        return Response.ok(res2).build();
    }
}
