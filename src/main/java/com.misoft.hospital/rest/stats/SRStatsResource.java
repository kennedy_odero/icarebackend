package com.misoft.hospital.rest.stats;

import com.misoft.hospital.data.gdto.GenericDTO;
import com.misoft.hospital.model.stats.Interval;
import com.misoft.hospital.util.types.DateParam;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Logger;

/**
 * Created by kodero on 12/28/15.
 */
@Stateless
@Path("/srStats")
public class SRStatsResource {

    @Inject
    EntityManager em;

    @Inject
    protected Logger log;

    @GET
    @Path("/overall/{asAt}")
    public Response overallSRStats(@PathParam("asAt") @DefaultValue("today") DateParam asAt,
                                   @QueryParam("interval") @DefaultValue("DAY") Interval interval,
                                   @QueryParam("groupBy") String groupBy,
                                   @QueryParam("requestStatus") String requestStatus){
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String sql = "select\n" +
                "    sr.request_status,\n" +
                "    count(case sr.request_tye when 'ADMISSION' then 1 else null end) admissions,\n" +
                "    count(case sr.request_tye when 'AUTOPSY' then 1 else null end) autopsies,\n" +
                "    count(case sr.request_tye when 'CONSULTATION' then 1 else null end) consultations,\n" +
                "    count(case sr.request_tye when 'IMAGING' then 1 else null end) imaging,\n" +
                "    count(case sr.request_tye when 'LABORATORY' then 1 else null end) labs,\n" +
                "    count(case sr.request_tye when 'MORTUARY' then 1 else null end) mortuary,\n" +
                "    count(case sr.request_tye when 'OPERATION' then 1 else null end) operations,\n" +
                "    count(case sr.request_tye when 'PRESCRIPTION' then 1 else null end) prescriptions,\n" +
                //"    count(case sr.request_tye when 'REFERRAL' then 1 else null end) referrals,\n" +
                "    count(case sr.request_tye when 'TRIAGING' then 1 else null end) triages\n" +
                "from service_requests sr\n" +
                "where DATE(sr.date_requested) between DATE_SUB('" + df.format(asAt.getDate()) + "', INTERVAL " + interval.getSqlInterval() + ") and '" + df.format(asAt.getDate()) + "'";
        if(requestStatus != null) sql = sql + "and sr.request_status = '" + requestStatus + "'";
        if(groupBy != null) sql = sql + "group by " + groupBy;
        Query q = em.createNativeQuery(sql);
        List<Object[]> res = q.getResultList();

        Iterator<Object[]> it = res.iterator();
        List statLines = new ArrayList();
        List<Map> res2 = new ArrayList<>();
        while(it.hasNext()){
            Map<String,Object> stats = new HashMap<>();
            Object[] row = it.next();
            List<Map<String, Object>> statLine = new ArrayList<>();
            Map admissionsMap = new HashMap(); admissionsMap.put("label", "ADM"); admissionsMap.put("value", row[1]); statLine.add(admissionsMap);
            Map autopsiesMap = new HashMap(); autopsiesMap.put("label", "ATP"); autopsiesMap.put("value", row[2]); statLine.add(autopsiesMap);
            Map consultationsMap = new HashMap(); consultationsMap.put("label", "CON"); consultationsMap.put("value", row[3]); statLine.add(consultationsMap);
            Map imagingMap = new HashMap(); imagingMap.put("label", "RAD"); imagingMap.put("value", row[4]); statLine.add(imagingMap);
            Map labsMap = new HashMap(); labsMap.put("label", "LAB"); labsMap.put("value", row[5]); statLine.add(labsMap);
            Map mortuaryMap = new HashMap(); mortuaryMap.put("label", "MOR"); mortuaryMap.put("value", row[6]); statLine.add(mortuaryMap);
            Map operationsMap = new HashMap(); operationsMap.put("label", "OPE"); operationsMap.put("value", row[7]); statLine.add(operationsMap);
            Map prescriptionsMap = new HashMap(); prescriptionsMap.put("label", "PRE"); prescriptionsMap.put("value", row[8]); statLine.add(prescriptionsMap);
            //Map referralsMap = new HashMap(); referralsMap.put("label", "REF"); referralsMap.put("value", row[9]); statLine.add(referralsMap);
            Map triagesMap = new HashMap(); triagesMap.put("label", "TRI"); triagesMap.put("value", row[9]); statLine.add(triagesMap);
            statLines.add(statLine);
            stats.put("key", row[0]);
            stats.put("values", statLine);
            res2.add(stats);
        }
        //stats.put("values", statLines);
        return Response.ok(res2).build();
    }

    @GET
    @Path("/byRequestType/{asAt}")
    public Response getSRStats(@PathParam("asAt") @DefaultValue("today") DateParam asAt,
                               @QueryParam("interval") @DefaultValue("DAY") Interval interval,
                               @QueryParam("requestType") @DefaultValue("CONSULTATION") String requestType){
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String sql = "select request_status, count(request_status) from service_requests " +
                "where request_tye='" + requestType + "' " +
                "and DATE(date_requested) between DATE_SUB('" + df.format(asAt.getDate()) + "', INTERVAL " + interval.getSqlInterval() + ") and '" + df.format(asAt.getDate()) + "' " +
                "group by request_status " +
                "order by request_status desc";
        Query q = em.createNativeQuery(sql);
        List<Object[]> res = q.getResultList();

        List<Map> res2 = new ArrayList<>();
        Iterator<Object[]> it = res.iterator();
        Map<String,Object> stats = new HashMap<>();
        List<Map<String, Object>> statLine = new ArrayList<>();
        while(it.hasNext()){
            Object[] row = it.next();
            Map rowMap = new HashMap(); rowMap.put("label", row[0]); rowMap.put("value", row[1]); statLine.add(rowMap);
        }
        stats.put("key", "Consultation Stats");
        stats.put("values", statLine);
        res2.add(stats);
        return Response.ok(res2).build();
    }

    @GET
    @Path("/waitingTime")
    public Response getWaitingTime(@PathParam("asAt") @DefaultValue("today") DateParam asAt,
                                   @QueryParam("interval") @DefaultValue("DAY") Interval interval){
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String sql = "select \n" +
                "\tp.patient_no,\n" +
                "\tconcat(p.surname, ' ', p.other_names), \n" +
                "\t(select date_changed \n" +
                "\t\tfrom request_status_history rsh \n" +
                "\t\twhere rsh.service_request = sr.id and rsh.status = 'SUBMITTED'\n" +
                "        order by date_changed desc limit 1) submitted_at,\n" +
                "\tTIMESTAMPDIFF(SECOND,(select date_changed \n" +
                "\t\tfrom request_status_history rsh \n" +
                "\t\twhere rsh.service_request = sr.id and rsh.status = 'SUBMITTED'\n" +
                "        order by date_changed desc limit 1),NOW()) waiting_time\n" +
                "from service_requests sr \n" +
                "\tinner join parties p \n" +
                "\t\ton sr.patient = p.id\n" +
                "\tinner join consultation_types ct \n" +
                "\t\ton sr.consultation_type = ct.id \n" +
                "where sr.request_status='SUBMITTED' and request_tye='CONSULTATION' and ct.unit in ('ab83c74d-06e8-4585-87a8-9b203f1a400c', '790d7b6e-8042-403d-923c-6500c9ab2e8f', '18daf5aa-0faa-409b-a608-4fb22661b9d9')\n" +
                "\tand DATE(sr.date_requested) between DATE_SUB(now(), INTERVAL 3 DAY) and now()" +
                "order by waiting_time desc";
        List<Object> results = em.createNativeQuery(sql).getResultList();
        List<GenericDTO> balances = new ArrayList<>();
        for(Object o : results){
            Object[] row = (Object[]) o;
            GenericDTO genericDTO = new GenericDTO("com.misoft.reports.RequestQueueDetails");
            genericDTO.addString("patientNo", (String) row[0]);
            genericDTO.addString("patientName", (String) row[1]);
            genericDTO.addDate("submittedAt", (Date) row[2]);
            genericDTO.addBigInteger("waitingTime", (BigInteger) row[3]);
            balances.add(genericDTO);
        }
        return Response.ok(balances).build();
    }

    @GET
    @Path("/doctorWorkload")
    public Response getDoctorsWorkload(@PathParam("dateFrom") @DefaultValue("today") DateParam dateFrom,
                                       @PathParam("dateTo") @DefaultValue("today") DateParam dateTo,
                                       @QueryParam("staffId") String staffId){
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String sql = "select\n" +
                "\trsh.date_changed,\n" +
                "\trsh.service_request,\n" +
                "\tp.patient_no,\n" +
                "\tp.surname,\n" +
                "    p.other_names,\n" +
                "    u.email seen_by\n" +
                "from request_status_history rsh \n" +
                "\tinner join service_requests sr \n" +
                "\t\ton rsh.service_request = sr.id\n" +
                "\tinner join parties p \n" +
                "\t\ton sr.patient = p.id\n" +
                "\tinner join users u \n" +
                "\t\ton rsh.created_by_id = u.id\n" +
                "where rsh.status = 'RECEIVED' and date(rsh.date_changed) between '" + df.format(dateFrom.getDate()) + "' and ''" + df.format(dateTo.getDate()) + "";
        List<Object> results = em.createNativeQuery(sql).getResultList();
        List<GenericDTO> balances = new ArrayList<>();
        for(Object o : results){
            Object[] row = (Object[]) o;
            GenericDTO genericDTO = new GenericDTO("com.misoft.reports.RequestQueueDetails");
            genericDTO.addString("patientNo", (String) row[0]);
            genericDTO.addString("patientName", (String) row[1]);
            genericDTO.addDate("submittedAt", (Date) row[2]);
            genericDTO.addBigInteger("waitingTime", (BigInteger) row[3]);
            balances.add(genericDTO);
        }
        return Response.ok(balances).build();
    }
}
