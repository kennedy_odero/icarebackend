package com.misoft.hospital.rest;

import com.misoft.hospital.model.setup.DrugAdminRoute;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 3/2/15.
 */
@Stateless
@Path("/drugAdminRoutes")
public class DrugAdminRouteResource extends BaseEntityService<DrugAdminRoute>{
    public DrugAdminRouteResource(){
        super(DrugAdminRoute.class);
    }
}
