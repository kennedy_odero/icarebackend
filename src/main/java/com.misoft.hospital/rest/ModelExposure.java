package com.misoft.hospital.rest;

import com.misoft.hospital.util.ReflectionUtils;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.*;

@Path("/models")
public class ModelExposure {

    public static final String COM_LEMR_MODEL_GRANT = "com.lemr.model";

    @GET
    @Path("/fqns")
    @Produces(MediaType.APPLICATION_JSON)
    public Set<String> entitiesFQNS() throws IOException {
        org.reflections.Reflections reflections = new org.reflections.Reflections(COM_LEMR_MODEL_GRANT);
        Set<Class<?>> entityClasses = reflections.getTypesAnnotatedWith(Entity.class);
        Set<String> fqns = new LinkedHashSet<>(entityClasses.size());
        for (Class<?> s : entityClasses) {
            fqns.add(s.getName());
        }
        return Collections.unmodifiableSet(fqns);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Map<String, List<FF>>> modelMap() throws IOException {
        org.reflections.Reflections reflections = new org.reflections.Reflections(COM_LEMR_MODEL_GRANT);
        Set<Class<?>> entityClasses = reflections.getTypesAnnotatedWith(Entity.class);
        List<Map<String, List<FF>>> res = new ArrayList<>();
        for (Class<?> s : entityClasses) {
            Map<String, List<FF>> ent = new HashMap<>();
            ent.put(s.getName(), makeFFS(ReflectionUtils.primitiveFields(s)));
            res.add(ent);
        }
        return res;
    }

    private List<FF> makeFFS(List<Field> declaredFields) {
        List<FF> ffs = new ArrayList<>(declaredFields.size());
        for (Field f : declaredFields) {
            ffs.add(makeFF(f));
        }
        return ffs;
    }

    private FF makeFF(Field f) {
        return new FF(f.getName(), f.getType().getName(), checkRequired(f));
    }

    private boolean checkRequired(Field f) {
        NotNull notNull = f.getAnnotation(NotNull.class);
        return notNull != null;
    }

    @XmlRootElement
    public static class FF {
        private String name;

        private String type;

        private boolean required = false;


        public FF() {
        }

        public FF(String name, String type, boolean required) {
            this.name = name;
            this.type = type;
            this.required = required;
        }

        public boolean isRequired() {
            return required;
        }

        public void setRequired(boolean required) {
            this.required = required;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        @Override
        public String toString() {
            return "FF{" +
                    "name='" + name + '\'' +
                    ", type='" + type + '\'' +
                    '}';
        }
    }
}