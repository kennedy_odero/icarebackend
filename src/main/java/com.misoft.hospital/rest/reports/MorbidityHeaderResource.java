package com.misoft.hospital.rest.reports;

import com.misoft.hospital.model.reports.MorbidityHeader;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 7/26/16.
 */
@Stateless
@Path("/morbidityHeaders")
public class MorbidityHeaderResource extends BaseEntityService<MorbidityHeader>{
    public MorbidityHeaderResource(){
        super(MorbidityHeader.class);
    }
}
