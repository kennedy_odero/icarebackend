package com.misoft.hospital.rest.reports;

import com.misoft.hospital.data.gdto.GenericDTO;
import com.misoft.hospital.util.types.DateParam;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

@Path("/refunds")
public class RefundReport {

    @Inject
    EntityManager em;

    @Inject
    Logger log;

    DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
}
