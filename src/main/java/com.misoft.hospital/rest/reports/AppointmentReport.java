package com.misoft.hospital.rest.reports;

import com.misoft.hospital.data.gdto.GenericDTO;
import com.misoft.hospital.util.types.DateParam;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kodero on 7/2/16.
 */
@Stateless
@Path("/reports/appointments")
public class AppointmentReport {

    @Inject
    EntityManager em;

    /*@GET
    @Path("/dailyWorkload")
    public Response getSalesByCustomer(@QueryParam("from") @DefaultValue("today") DateParam dateFrom,
                                       @QueryParam("to") @DefaultValue("today") DateParam dateTo){
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String sql = "select a.appt_date, " +
                "   sum(case p.gender when 'MALE' then 1 else 0 end) m, " +
                "   sum(case p.gender when 'FEMALE' then 1 else 0 end) f " +
                "from appointments a " +
                "   inner join parties p " +
                "       on a.patient = p.id " +
                "where a.deleted <> 1 and a.appt_date group by a.appt_date";
        List<Object> results = em.createNativeQuery(sql)
                .getResultList();
        List<GenericDTO> balances = new ArrayList<>();
        for(Object o : results){
            Object[] row = (Object[]) o;
            GenericDTO genericDTO = new GenericDTO("com.misoft.reports.DailyWorkload");
            genericDTO.addString("date", (String) row[0]);
            genericDTO.addString("male", (String) row[1]);
            genericDTO.addBigInteger("female", (BigInteger) row[2]);
            genericDTO.addBigDecimal("total", (BigDecimal) row[3]);
            balances.add(genericDTO);
        }
        return Response.ok(balances).build();
    }*/
}
