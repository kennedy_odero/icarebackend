package com.misoft.hospital.rest.reports;

import com.misoft.hospital.data.gdto.GenericDTO;
import com.misoft.hospital.util.types.DateParam;

import javax.ejb.Stateless;
import javax.enterprise.inject.Default;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by kodero on 7/28/16.
 */
@Stateless
@Path("/morbidityReport")
public class MorbidityReport {

    @Inject
    EntityManager em;

    @Inject
    Logger log;

    DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

    @GET
    @Path("/list")
    public Response getMorbidityReport(@QueryParam("dateFrom") DateParam dateFrom, @QueryParam("dateTo") DateParam dateTo, @QueryParam("ageGroup") @DefaultValue("OVER5") String ageGroup){
        log.info("Date from : " + dateFrom + ", dateTo : " + dateTo);
        if(dateFrom == null || dateTo == null) return Response.ok().build();

        String sql = "select \n" +
                "\tmh.name, \n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 1 then 1 else 0 end) first,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 2 then 1 else 0 end) second,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 3 then 1 else 0 end) third,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 4 then 1 else 0 end) fourth,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 5 then 1 else 0 end) fifth,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 6 then 1 else 0 end) sixth,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 7 then 1 else 0 end) seventh,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 8 then 1 else 0 end) eighth,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 9 then 1 else 0 end) ninth,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 10 then 1 else 0 end) tenth,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 11 then 1 else 0 end) eleventh,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 12 then 1 else 0 end) twelfth,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 13 then 1 else 0 end) thirteenth,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 14 then 1 else 0 end) fourteenth,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 15 then 1 else 0 end) fifteenth,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 16 then 1 else 0 end) sixteenth,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 17 then 1 else 0 end) seventeenth,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 18 then 1 else 0 end) eighteenth,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 19 then 1 else 0 end) nineteenth,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 20 then 1 else 0 end) twentieth,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 21 then 1 else 0 end) twentyFirst,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 22 then 1 else 0 end) twentySecond,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 23 then 1 else 0 end) twentyThird,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 24 then 1 else 0 end) twentyFourth,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 25 then 1 else 0 end) twentyFifth,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 26 then 1 else 0 end) twentySixth,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 27 then 1 else 0 end) twentySeventh,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 28 then 1 else 0 end) twentyEighth,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 29 then 1 else 0 end) twentyNinth,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 30 then 1 else 0 end) thirtieth,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 31 then 1 else 0 end) thirtyFirst,\n" +
                "    count(cd.disease) total\n" +
                "from morbidity_headers mh \n" +
                "\tleft outer join morbidity_lines ml \n" +
                "\t\ton mh.id = ml.morbidity_header \n" +
                "\tleft outer join consultation_diagnoses cd \n" +
                "\t\ton ml.disease = cd.disease and date(cd.created_at) between '" + df.format(dateFrom.getDate()) + "' and '" + df.format(dateTo.getDate()) + "' and cd.age_group ='" + ageGroup + "' " +
                "group by mh.name \n" +
                "order by mh._index";
        Query q = em.createNativeQuery(sql);
        List<Object> results = em.createNativeQuery(sql).getResultList();
        log.info("Result length");
        List<GenericDTO> balances = new ArrayList<>();
        for(Object o : results){
            balances.add(getGenericDTO((Object[]) o));
        }

        balances.add(getAllOtherDiseasesCases(dateFrom.getDate(), dateTo.getDate(), ageGroup));
        balances.add(getTotalNewCases("Total New Cases", dateFrom.getDate(), dateTo.getDate(), ageGroup));
        balances.add(getTotalNewCases("No. of First Attendances", dateFrom.getDate(), dateTo.getDate(), ageGroup));
        balances.add(getReattendances("RE-ATTENDANCES", dateFrom.getDate(), dateTo.getDate(), ageGroup));

        //referrals in
        GenericDTO referralsIn = new GenericDTO("com.misoft.reports.MorbiditySummary");
        referralsIn.addString("diseaseCategory", "REFERRALS IN");
        referralsIn.addBigInteger("total", BigInteger.ZERO);
        balances.add(referralsIn);

        //referrals in
        GenericDTO referralsOut = new GenericDTO("com.misoft.reports.MorbiditySummary");
        referralsOut.addString("diseaseCategory", "REFERRALS OUT");
        referralsOut.addBigInteger("total", BigInteger.ZERO);
        balances.add(referralsOut);
        return Response.ok(balances).build();
    }

    private GenericDTO getTotalNewCases(String label, Date date, Date date1, String ageGroup) {
        String sql = "select \n" +
                "\t'" + label + "', \n" +
                "\tsum(case when DAYOFMONTH(cd.created_at) = 1 then 1 else 0 end) _1,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 2 then 1 else 0 end) _2,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 3 then 1 else 0 end) _3,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 4 then 1 else 0 end) _4,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 5 then 1 else 0 end) _5,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 6 then 1 else 0 end) _6,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 7 then 1 else 0 end) _7,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 8 then 1 else 0 end) _8,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 9 then 1 else 0 end) _9,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 10 then 1 else 0 end) _10,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 11 then 1 else 0 end) _11,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 12 then 1 else 0 end) _12,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 13 then 1 else 0 end) _13,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 14 then 1 else 0 end) _14,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 15 then 1 else 0 end) _15,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 16 then 1 else 0 end) _16,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 17 then 1 else 0 end) _17,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 18 then 1 else 0 end) _18,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 19 then 1 else 0 end) _19,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 20 then 1 else 0 end) _20,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 21 then 1 else 0 end) _21,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 22 then 1 else 0 end) _22,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 23 then 1 else 0 end) _23,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 24 then 1 else 0 end) _24,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 25 then 1 else 0 end) _25,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 26 then 1 else 0 end) _26,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 27 then 1 else 0 end) _27,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 28 then 1 else 0 end) _28,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 29 then 1 else 0 end) _29,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 30 then 1 else 0 end) _30,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 31 then 1 else 0 end) _31,\n" +
                "    count(cd.disease) total\n" +
                "from consultation_diagnoses cd \n" +
                "where date(cd.created_at) between '" + df.format(date) + "' and '" + df.format(date1) + "' and cd.age_group ='" + ageGroup + "' and cd.new_patient = 1 " +
                "";
        Query q = em.createNativeQuery(sql);
        List<Object> results = em.createNativeQuery(sql).getResultList();
        log.info("Result length");
        GenericDTO genericDTO = new GenericDTO("com.misoft.reports.MorbiditySummary");
        List<GenericDTO> balances2 = new ArrayList<>();
        for(Object o : results){
            genericDTO = getGenericDTO((Object[]) o);
        }
        return genericDTO;
    }

    private GenericDTO getReattendances(String label, Date date, Date date1, String ageGroup) {
        String sql = "select \n" +
                "\t'" + label + "', \n" +
                "\t  coalesce(sum(case when DAYOFMONTH(cd.created_at) = 1 then 1 else 0 end),0) _1,\n" +
                "    coalesce(sum(case when DAYOFMONTH(cd.created_at) = 2 then 1 else 0 end),0) _2,\n" +
                "    coalesce(sum(case when DAYOFMONTH(cd.created_at) = 3 then 1 else 0 end),0) _3,\n" +
                "    coalesce(sum(case when DAYOFMONTH(cd.created_at) = 4 then 1 else 0 end),0) _4,\n" +
                "    coalesce(sum(case when DAYOFMONTH(cd.created_at) = 5 then 1 else 0 end),0) _5,\n" +
                "    coalesce(sum(case when DAYOFMONTH(cd.created_at) = 6 then 1 else 0 end),0) _6,\n" +
                "    coalesce(sum(case when DAYOFMONTH(cd.created_at) = 7 then 1 else 0 end),0) _7,\n" +
                "    coalesce(sum(case when DAYOFMONTH(cd.created_at) = 8 then 1 else 0 end),0) _8,\n" +
                "    coalesce(sum(case when DAYOFMONTH(cd.created_at) = 9 then 1 else 0 end),0) _9,\n" +
                "    coalesce(sum(case when DAYOFMONTH(cd.created_at) = 10 then 1 else 0 end),0) _10,\n" +
                "    coalesce(sum(case when DAYOFMONTH(cd.created_at) = 11 then 1 else 0 end),0) _11,\n" +
                "    coalesce(sum(case when DAYOFMONTH(cd.created_at) = 12 then 1 else 0 end),0) _12,\n" +
                "    coalesce(sum(case when DAYOFMONTH(cd.created_at) = 13 then 1 else 0 end),0) _13,\n" +
                "    coalesce(sum(case when DAYOFMONTH(cd.created_at) = 14 then 1 else 0 end),0) _14,\n" +
                "    coalesce(sum(case when DAYOFMONTH(cd.created_at) = 15 then 1 else 0 end),0) _15,\n" +
                "    coalesce(sum(case when DAYOFMONTH(cd.created_at) = 16 then 1 else 0 end),0) _16,\n" +
                "    coalesce(sum(case when DAYOFMONTH(cd.created_at) = 17 then 1 else 0 end),0) _17,\n" +
                "    coalesce(sum(case when DAYOFMONTH(cd.created_at) = 18 then 1 else 0 end),0) _18,\n" +
                "    coalesce(sum(case when DAYOFMONTH(cd.created_at) = 19 then 1 else 0 end),0) _19,\n" +
                "    coalesce(sum(case when DAYOFMONTH(cd.created_at) = 20 then 1 else 0 end),0) _20,\n" +
                "    coalesce(sum(case when DAYOFMONTH(cd.created_at) = 21 then 1 else 0 end),0) _21,\n" +
                "    coalesce(sum(case when DAYOFMONTH(cd.created_at) = 22 then 1 else 0 end),0) _22,\n" +
                "    coalesce(sum(case when DAYOFMONTH(cd.created_at) = 23 then 1 else 0 end),0) _23,\n" +
                "    coalesce(sum(case when DAYOFMONTH(cd.created_at) = 24 then 1 else 0 end),0) _24,\n" +
                "    coalesce(sum(case when DAYOFMONTH(cd.created_at) = 25 then 1 else 0 end),0) _25,\n" +
                "    coalesce(sum(case when DAYOFMONTH(cd.created_at) = 26 then 1 else 0 end),0) _26,\n" +
                "    coalesce(sum(case when DAYOFMONTH(cd.created_at) = 27 then 1 else 0 end),0) _27,\n" +
                "    coalesce(sum(case when DAYOFMONTH(cd.created_at) = 28 then 1 else 0 end),0) _28,\n" +
                "    coalesce(sum(case when DAYOFMONTH(cd.created_at) = 29 then 1 else 0 end),0) _29,\n" +
                "    coalesce(sum(case when DAYOFMONTH(cd.created_at) = 30 then 1 else 0 end),0) _30,\n" +
                "    coalesce(sum(case when DAYOFMONTH(cd.created_at) = 31 then 1 else 0 end),0) _31,\n" +
                "    coalesce(count(cd.disease),0) total\n" +
                "from consultation_diagnoses cd \n" +
                "where date(cd.created_at) between '" + df.format(date) + "' and '" + df.format(date1) + "' and cd.age_group ='" + ageGroup + "' and cd.new_patient = 0 " +
                "";
        Query q = em.createNativeQuery(sql);
        List<Object> results = em.createNativeQuery(sql).getResultList();
        log.info("Result length");
        GenericDTO genericDTO = new GenericDTO("com.misoft.reports.MorbiditySummary");
        List<GenericDTO> balances2 = new ArrayList<>();
        for(Object o : results){
            genericDTO = getGenericDTO((Object[]) o);
        }
        return genericDTO;
    }

    private GenericDTO getAllOtherDiseasesCases(Date date, Date date1, String ageGroup) {
        String sql = "select \n" +
                "\t'All Other Diseases', \n" +
                "\tsum(case when DAYOFMONTH(cd.created_at) = 1 then 1 else 0 end) _1,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 2 then 1 else 0 end) _2,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 3 then 1 else 0 end) _3,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 4 then 1 else 0 end) _4,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 5 then 1 else 0 end) _5,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 6 then 1 else 0 end) _6,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 7 then 1 else 0 end) _7,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 8 then 1 else 0 end) _8,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 9 then 1 else 0 end) _9,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 10 then 1 else 0 end) _10,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 11 then 1 else 0 end) _11,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 12 then 1 else 0 end) _12,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 13 then 1 else 0 end) _13,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 14 then 1 else 0 end) _14,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 15 then 1 else 0 end) _15,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 16 then 1 else 0 end) _16,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 17 then 1 else 0 end) _17,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 18 then 1 else 0 end) _18,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 19 then 1 else 0 end) _19,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 20 then 1 else 0 end) _20,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 21 then 1 else 0 end) _21,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 22 then 1 else 0 end) _22,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 23 then 1 else 0 end) _23,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 24 then 1 else 0 end) _24,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 25 then 1 else 0 end) _25,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 26 then 1 else 0 end) _26,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 27 then 1 else 0 end) _27,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 28 then 1 else 0 end) _28,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 29 then 1 else 0 end) _29,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 30 then 1 else 0 end) _30,\n" +
                "    sum(case when DAYOFMONTH(cd.created_at) = 31 then 1 else 0 end) _31,\n" +
                "    count(cd.disease) total\n" +
                "from consultation_diagnoses cd \n" +
                "where date(cd.created_at) between '" + df.format(date) + "' and '" + df.format(date1) + "' and cd.age_group ='" + ageGroup + "' " +
                "\tand cd.disease not in(select ml.disease from morbidity_lines ml where ml.deleted <> 1)";
        Query q = em.createNativeQuery(sql);
        List<Object> results = em.createNativeQuery(sql).getResultList();
        log.info("Result length");
        List<GenericDTO> balances2 = new ArrayList<>();
        GenericDTO genericDTO = new GenericDTO("com.misoft.reports.MorbiditySummary");
        for(Object o : results){
            genericDTO = getGenericDTO((Object[]) o);
        }
        return genericDTO;
    }

    private GenericDTO getGenericDTO(Object[] o) {
        Object[] row = o;
        GenericDTO genericDTO = new GenericDTO("com.misoft.reports.MorbiditySummary");
        genericDTO.addString("diseaseCategory", (String) row[0]);
        genericDTO.addBigDecimal("_1", (BigDecimal) row[1]);
        genericDTO.addBigDecimal("_2", (BigDecimal) row[2]);
        genericDTO.addBigDecimal("_3", (BigDecimal) row[3]);
        genericDTO.addBigDecimal("_4", (BigDecimal) row[4]);
        genericDTO.addBigDecimal("_5", (BigDecimal) row[5]);
        genericDTO.addBigDecimal("_6", (BigDecimal) row[6]);
        genericDTO.addBigDecimal("_7", (BigDecimal) row[7]);
        genericDTO.addBigDecimal("_8", (BigDecimal) row[8]);
        genericDTO.addBigDecimal("_9", (BigDecimal) row[9]);
        genericDTO.addBigDecimal("_10", (BigDecimal) row[10]);
        genericDTO.addBigDecimal("_11", (BigDecimal) row[11]);
        genericDTO.addBigDecimal("_12", (BigDecimal) row[12]);
        genericDTO.addBigDecimal("_13", (BigDecimal) row[13]);
        genericDTO.addBigDecimal("_14", (BigDecimal) row[14]);
        genericDTO.addBigDecimal("_15", (BigDecimal) row[15]);
        genericDTO.addBigDecimal("_16", (BigDecimal) row[16]);
        genericDTO.addBigDecimal("_17", (BigDecimal) row[17]);
        genericDTO.addBigDecimal("_18", (BigDecimal) row[18]);
        genericDTO.addBigDecimal("_19", (BigDecimal) row[19]);
        genericDTO.addBigDecimal("_20", (BigDecimal) row[20]);
        genericDTO.addBigDecimal("_21", (BigDecimal) row[21]);
        genericDTO.addBigDecimal("_22", (BigDecimal) row[22]);
        genericDTO.addBigDecimal("_23", (BigDecimal) row[23]);
        genericDTO.addBigDecimal("_24", (BigDecimal) row[24]);
        genericDTO.addBigDecimal("_25", (BigDecimal) row[25]);
        genericDTO.addBigDecimal("_26", (BigDecimal) row[26]);
        genericDTO.addBigDecimal("_27", (BigDecimal) row[27]);
        genericDTO.addBigDecimal("_28", (BigDecimal) row[28]);
        genericDTO.addBigDecimal("_29", (BigDecimal) row[29]);
        genericDTO.addBigDecimal("_30", (BigDecimal) row[30]);
        genericDTO.addBigDecimal("_31", (BigDecimal) row[31]);
        genericDTO.addBigInteger("total", (BigInteger) row[32]);
        return genericDTO;
    }

    @GET
    @Path("/diagnosisDetailed")
    public Response getDiagnosisDetailed(@QueryParam("dateFrom") @DefaultValue("today") DateParam dateFrom,
                                        @QueryParam("dateTo") @DefaultValue("today") DateParam dateTo){
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String sql = "select " +
                "   date(cd.created_at), " +
                "   d.name, " +
                "   d.code, " +
                "   p.surname, " +
                "   p.other_names, " +
                "   cd.age, " +
                "   p.gender, " +
                "   u.email " +
                "from consultation_diagnoses cd " +
                "   inner join appointments ap " +
                "       on cd.appointment = ap.id " +
                "   inner join parties p " +
                "       on ap.patient = p.id " +
                "   inner join users u " +
                "       on cd.created_by_id = u.id " +
                "   inner join diseases d " +
                "       on cd.disease = d.id " +
                "where date(cd.created_at) between '" + df.format(dateFrom.getDate()) + "' and '" + df.format(dateTo.getDate()) + "' " +
                "order by cd.created_at desc";
        List<Object> results = em.createNativeQuery(sql).getResultList();
        List<GenericDTO> balances = new ArrayList<>();
        for(Object o : results){
            Object[] row = (Object[]) o;
            GenericDTO genericDTO = new GenericDTO("com.misoft.reports.DrugConsumption");
            genericDTO.addDate("date", (Date) row[0]);
            genericDTO.addString("disease", (String) row[1]);
            genericDTO.addString("code", (String) row[2]);
            genericDTO.addString("surname", (String) row[3]);
            genericDTO.addString("otherNames", (String) row[4]);
            genericDTO.addInt("age", (Integer) row[5]);
            genericDTO.addString("gender", (String) row[6]);
            genericDTO.addString("diagnosedBy", (String) row[7]);
            balances.add(genericDTO);
        }
        return Response.ok(balances).build();
    }

    @GET
    @Path("/diagnosisSummary")
    public Response getDiagnosisSummary(@QueryParam("dateFrom") @DefaultValue("today") DateParam dateFrom,
                                        @QueryParam("dateTo") @DefaultValue("today") DateParam dateTo){
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String sql = "select \n" +
                "\tcoalesce(d.name, '_Undiagnosed'), \n" +
                "    coalesce(d.code, '-'), \n" +
                "\tcount(coalesce(d.name, '_Undiagnosed')) _count\n" +
                "from appointments ap \n" +
                "\tinner join parties p \n" +
                "\t\ton ap.patient = p.id \n" +
                "\tleft outer join consultation_diagnoses cd \n" +
                "\t\ton cd.appointment = ap.id \n" +
                "\tleft outer join diseases d \n" +
                "\t\ton cd.disease = d.id \n" +
                "where date(ap.created_at) between '" + df.format(dateFrom.getDate()) + "' and '" + df.format(dateTo.getDate()) + "'\n" +
                "group by 1";
        List<Object> results = em.createNativeQuery(sql).getResultList();
        List<GenericDTO> balances = new ArrayList<>();
        for(Object o : results){
            Object[] row = (Object[]) o;
            GenericDTO genericDTO = new GenericDTO("com.misoft.reports.DiagnosisDetailed");
            genericDTO.addString("diseaseName", (String) row[0]);
            genericDTO.addString("diseaseCode", (String) row[1]);
            genericDTO.addInt("count", (Integer) row[2]);
            balances.add(genericDTO);
        }
        return Response.ok(balances).build();
    }
}
