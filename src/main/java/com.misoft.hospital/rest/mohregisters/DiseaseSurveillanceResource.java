package com.misoft.hospital.rest.mohregisters;

import com.misoft.hospital.model.mohregisters.DiseaseSurveillance;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 10/20/15.
 */
@Stateless
@Path("/diseaseSurveillances")
public class DiseaseSurveillanceResource extends BaseEntityService<DiseaseSurveillance>{
    public DiseaseSurveillanceResource(){
        super(DiseaseSurveillance.class);
    }
}
