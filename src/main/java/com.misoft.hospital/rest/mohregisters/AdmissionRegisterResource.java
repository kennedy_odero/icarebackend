package com.misoft.hospital.rest.mohregisters;


import com.misoft.hospital.model.mohregisters.AdmissionRegister;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 10/18/15.
 */
@Stateless
@Path("/admissionRegisters")
public class AdmissionRegisterResource extends BaseEntityService{
    public AdmissionRegisterResource(){
        super(AdmissionRegister.class);
    }
}
