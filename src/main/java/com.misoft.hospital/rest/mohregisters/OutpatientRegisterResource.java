package com.misoft.hospital.rest.mohregisters;

import com.misoft.hospital.model.mohregisters.OutpatientRegister;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 10/18/15.
 */
@Stateless
@Path("/outpatientRegisters")
public class OutpatientRegisterResource extends BaseEntityService<OutpatientRegister>{
    public OutpatientRegisterResource(){
        super(OutpatientRegister.class);
    }
}
