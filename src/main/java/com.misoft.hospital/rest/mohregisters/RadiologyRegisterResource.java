package com.misoft.hospital.rest.mohregisters;

import com.misoft.hospital.model.mohregisters.RadiologyRegister;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 10/18/15.
 */
@Stateless
@Path("/radiologyRegisters")
public class RadiologyRegisterResource extends BaseEntityService<RadiologyRegister>{
    public RadiologyRegisterResource(){
        super(RadiologyRegister.class);
    }
}
