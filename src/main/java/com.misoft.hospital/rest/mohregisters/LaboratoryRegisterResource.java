package com.misoft.hospital.rest.mohregisters;

import com.misoft.hospital.model.mohregisters.LaboratoryRegister;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 10/18/15.
 */
@Stateless
@Path("/laboratoryRegisters")
public class LaboratoryRegisterResource extends BaseEntityService<LaboratoryRegister>{
    public LaboratoryRegisterResource(){
        super(LaboratoryRegister.class);
    }
}
