package com.misoft.hospital.rest.monitoring;

import java.util.Map;

public interface MonitoringResourceMXBean {
    Map getDiagnostics();
}