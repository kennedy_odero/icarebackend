/**
 *
 */
package com.misoft.hospital.rest;

import com.misoft.hospital.data.admin.UserRepository;
import com.misoft.hospital.data.gdto.GenericDTO;
import com.misoft.hospital.model.admin.LoginAttemptResult;
import com.misoft.hospital.model.admin.User;
import com.misoft.hospital.rest.admin.UserResource;
import com.misoft.hospital.service.admin.UserService;
import com.misoft.hospital.util.BCrypt;
import com.misoft.hospital.util.annotations.AuthenticationNotRequired;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.logging.Logger;

/**
 * @author kodero
 */
@Path("/authentication")
@javax.ejb.Stateless
public class AuthenticationResource {

    @Inject
    private transient Logger log;

    @Inject
    private UserRepository userDAO;

    @Context
    private HttpServletRequest req;

    @Inject
    private UserService userService;

    @Inject
    private UserResource userResource;

    /**
     * This must be accessed behind https
     * Check t
     *
     * @param loginObject@return
     */
    @POST
    @Path("/login")
    @AuthenticationNotRequired
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response authenticateUser(@Context HttpHeaders headers, LoginObject loginObject) {
        User user = getUserWithCredentials(loginObject.getUsername(), loginObject.getPassword());
        if (user == null) {
            return Response.status(Response.Status.UNAUTHORIZED)
                    .entity(new LoginResult(null, null, "NOK"))
                    .type(MediaType.APPLICATION_JSON_TYPE)
                    .build();
        }
        final String ip = getIp();
        user = userService.updateUserLogin(user, ip, req.getHeaders("User-Agent").nextElement(), req.getHeaders("Referer").nextElement(), LoginAttemptResult.SUCCESS);

        req.getSession().setAttribute(User.TOKEN, user.getPersistenceToken());
        req.getSession().setAttribute(User.CURRENT_USER_OBJECT_SESSION_ATTRIBUTE, user);
        return Response.status(Response.Status.OK)
                .entity(new LoginResult(user.getPersistenceToken(), user.getId().toString(), "OK"))
                .type(MediaType.APPLICATION_JSON_TYPE)
                .build();

    }

    private String getIp() {
        String remoteHost = req.getRemoteHost();
        String remoteAddr = req.getRemoteAddr();
        int remotePort = req.getRemotePort();
        final String ip = remoteHost + " (" + remoteAddr + ":" + remotePort + ")";
        return ip;
    }

    private User getUserWithCredentials(String username, String password) {
        if (username == null || password == null) {
            log.info("Incomplete authentication credentials for username '" + username + "'");
            return null;
        }
        User user = userDAO.findUser(username, true, false);
        if(user != null && !BCrypt.checkpw(password, user.getCryptedPassword())){
            //invalid login, make a note
            userService.logFailedLoginAttempt(user, getIp(), req.getHeaders("User-Agent").nextElement(), req.getHeaders("Referer").nextElement(), LoginAttemptResult.FAILURE);
            return null;
        }
        log.info("Successfully authenticated user: " + user.getEmail());
        return user;
    }

    @GET
    @Path("/logout")
    public Response logout(@HeaderParam("token") String token) {
        //final String token = (String) req.getSession().getAttribute(User.TOKEN);
        User currentUser = userDAO.findUserByToken(token);
        //TODO set some values
        userDAO.updateUser(currentUser);
        req.getSession().setAttribute(User.TOKEN, null);
        req.getSession().invalidate();
        return Response.ok().build();
    }

    @GET
    @Path("/loggedinUser")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getLoggedInUser() throws Exception {
        User u = userService.currentUser();
        GenericDTO d = userResource.toDTO(u, User.USER_SESSION_ATTRIBUTES, User.class);
        return Response.ok(d).build();
    }
}
