package com.misoft.hospital.rest;

import com.misoft.hospital.rest.util.JaxRsResource;
import org.jboss.resteasy.core.Dispatcher;
import org.jboss.resteasy.core.ResourceInvoker;
import org.jboss.resteasy.core.ResourceMethodInvoker;
import org.jboss.resteasy.core.ResourceMethodRegistry;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Path("/meta")
public class MetaService {
    @Context
    Dispatcher dispatcher;

    @GET
    @Path("/resources")
    @Produces(MediaType.APPLICATION_JSON)
    public Set<JaxRsResource> getAllResources() {
        Set<JaxRsResource> resources = new HashSet<>();

        ResourceMethodRegistry registry = (ResourceMethodRegistry) dispatcher.getRegistry();


        for (Map.Entry<String, List<ResourceInvoker>> entry : registry.getBounded().entrySet()) {
            for (ResourceInvoker invoker : entry.getValue()) {
                ResourceMethodInvoker method = (ResourceMethodInvoker) invoker;
                if (method.getMethod().getDeclaringClass() == getClass()) {
                    continue;
                }
                for (String verb : method.getHttpMethods()) {
                    String uri = entry.getKey();
                    resources.add(new JaxRsResource(method.getMethod().getDeclaringClass().getName(),
                            method.getMethod().getName(), verb, uri));

                }
            }
        }

        return resources;
    }

}