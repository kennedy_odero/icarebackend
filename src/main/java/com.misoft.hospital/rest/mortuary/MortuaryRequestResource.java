package com.misoft.hospital.rest.mortuary;

import com.misoft.hospital.model.healthcare.RequestStatus;
import com.misoft.hospital.model.mortuary.MortuaryRequest;
import com.misoft.hospital.model.mortuary.MortuaryStorageUnit;
import com.misoft.hospital.model.mortuary.StorageUnit;
import com.misoft.hospital.rest.patientCare.ServiceRequestResource;
import org.joda.time.DateTime;

import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.validation.ValidationException;
import javax.ws.rs.Path;
import java.util.Date;
import java.util.List;

/**
 * Created by kodero on 11/17/15.
 */
@Stateless
@Path("/mortuaryRequests")
public class MortuaryRequestResource extends ServiceRequestResource<MortuaryRequest> {

    @Inject
    Event<MortuaryStorageUnitStart> mortuaryStorageRoomStartEvent;

    @Inject
    Event<MortuaryStorageUnitStop> mortuaryStorageRoomStopEvent;

    public MortuaryRequestResource(){
        setEntityClass(MortuaryRequest.class);
    }

    @Override
    public void beforeSaveOrUpdate(MortuaryRequest mortuaryRequest){
        List<MortuaryStorageUnit> mortuaryStorageUnits = mortuaryRequest.getMortuaryStorageRooms();

        if((mortuaryRequest.getRequestStatus() == RequestStatus.PROCESSING)){
            //check if a bed has been assigned
            if(mortuaryRequest.getDateFrom() == null) throw new ValidationException("Please select the date of assignment!");
            if(mortuaryRequest.getStorageUnit() == null) throw new ValidationException("Please assign a unit to the patient!");
        }
        if((mortuaryRequest.getRequestStatus() == RequestStatus.COMPLETE)){
            //check if a bed has been assigned
            if(mortuaryRequest.getDateTo() == null) throw new ValidationException("Please select the date of release!");
        }
        StorageUnit currentUnit = mortuaryRequest.getStorageUnit();
        if(mortuaryStorageUnits.size() == 0 ) {
            if(currentUnit != null){
                MortuaryStorageUnit mortuaryStorageUnit = new MortuaryStorageUnit(currentUnit, currentUnit.getStorageRoom(), mortuaryRequest.getDateFrom());
                mortuaryRequest.addMortuaryStorageRoom(mortuaryStorageUnit);
                mortuaryStorageRoomStartEvent.fire(new MortuaryStorageUnitStart(mortuaryStorageUnit));
            }
        }else{
            //check if already added
            log.info("============Current Unit : " + currentUnit.getUnitCode());
            for(MortuaryStorageUnit unit : mortuaryStorageUnits){
                log.info("============Unit : " + unit.getStorageUnit().getUnitCode());
            }
            if(mortuaryStorageUnits.get(mortuaryStorageUnits.size() - 1).getStorageUnit().getId() != currentUnit.getId()){
                //first update previous beds to end their tenancy
                for(MortuaryStorageUnit storageUnit : mortuaryRequest.getMortuaryStorageRooms()){
                    //update dateTo
                    if(storageUnit.getDateTo() == null) {
                        storageUnit.setDateTo(new DateTime().withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0).toDate());//only update the beds that have not been updated
                        mortuaryStorageRoomStopEvent.fire(new MortuaryStorageUnitStop(storageUnit));
                    }
                }
                MortuaryStorageUnit mortuaryStorageUnit = new MortuaryStorageUnit(currentUnit, currentUnit.getStorageRoom(), new Date());
                mortuaryRequest.addMortuaryStorageRoom(mortuaryStorageUnit);
                mortuaryStorageRoomStopEvent.fire(new MortuaryStorageUnitStop(mortuaryStorageUnit));
            }
        }
    }
}
