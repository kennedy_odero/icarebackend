package com.misoft.hospital.rest.mortuary;

import com.misoft.hospital.model.mortuary.MortuaryStorageUnit;

/**
 * Created by kodero on 5/25/16.
 */
public class MortuaryStorageUnitStart {
    private MortuaryStorageUnit mortuaryStorageUnit;

    public MortuaryStorageUnit getMortuaryStorageUnit() {
        return mortuaryStorageUnit;
    }

    public void setMortuaryStorageUnit(MortuaryStorageUnit mortuaryStorageUnit) {
        this.mortuaryStorageUnit = mortuaryStorageUnit;
    }

    public MortuaryStorageUnitStart(){

    }

    public MortuaryStorageUnitStart(MortuaryStorageUnit mortuaryStorageRoom){
        setMortuaryStorageUnit(mortuaryStorageRoom);
    }
}
