package com.misoft.hospital.rest.mortuary;

import com.misoft.hospital.model.mortuary.MortuaryService;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 12/12/15.
 */
@Stateless
@Path("/mortuaryServices")
public class MortuaryServiceResource extends BaseEntityService<MortuaryService>{
    public MortuaryServiceResource(){
        super(MortuaryService.class);
    }
}
