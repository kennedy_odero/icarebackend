package com.misoft.hospital.rest.mortuary;

import com.misoft.hospital.model.mortuary.AutopsyType;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 6/14/15.
 */
@Stateless
@Path("/autopsyTypes")
public class AutopsyTypeResource extends BaseEntityService<AutopsyType> {
    public AutopsyTypeResource(){
        super(AutopsyType.class);
    }
}
