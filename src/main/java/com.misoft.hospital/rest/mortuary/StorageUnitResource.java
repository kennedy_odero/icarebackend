package com.misoft.hospital.rest.mortuary;

import com.misoft.hospital.model.mortuary.StorageUnit;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 6/10/15.
 */
@Stateless
@Path("/storageUnits")
public class StorageUnitResource extends BaseEntityService<StorageUnit> {
    public StorageUnitResource(){
        super(StorageUnit.class);
    }
}
