package com.misoft.hospital.rest.mortuary;

import com.misoft.hospital.model.mortuary.AutopsyRequest;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 6/14/15.
 */
@Stateless
@Path("/autopsyRequests")
public class AutopsyRequestResource extends BaseEntityService<AutopsyRequest>{
    public AutopsyRequestResource (){
        super(AutopsyRequest.class);
    }
}
