package com.misoft.hospital.rest.mortuary;

import com.misoft.hospital.model.mortuary.StorageRoom;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 6/10/15.
 */
@Path("/storageRooms")
@Stateless
public class StorageRoomResource extends BaseEntityService<StorageRoom>{
    public StorageRoomResource(){
        super(StorageRoom.class);
    }
}
