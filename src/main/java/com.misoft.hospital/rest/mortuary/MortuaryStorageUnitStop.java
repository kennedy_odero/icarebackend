package com.misoft.hospital.rest.mortuary;

import com.misoft.hospital.model.mortuary.MortuaryStorageUnit;

/**
 * Created by kodero on 5/25/16.
 */
public class MortuaryStorageUnitStop {
    private MortuaryStorageUnit mortuaryStorageUnit;

    public MortuaryStorageUnitStop(){

    }

    public MortuaryStorageUnitStop(MortuaryStorageUnit mortuaryStorageRoom){
        setMortuaryStorageUnit(mortuaryStorageRoom);
    }

    public MortuaryStorageUnit getMortuaryStorageUnit() {
        return mortuaryStorageUnit;
    }

    public void setMortuaryStorageUnit(MortuaryStorageUnit mortuaryStorageRoom) {
        this.mortuaryStorageUnit = mortuaryStorageRoom;
    }
}
