package com.misoft.hospital.rest.test;

import com.misoft.hospital.model.test.Hardware;
import com.misoft.hospital.rest.BaseEntityService;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 * Created by kodero on 7/1/15.
 */
@Stateless
@Path("/hardwares")
public class HardwareResource extends BaseEntityService<Hardware> {
    public HardwareResource(){
        super(Hardware.class);
    }
}
