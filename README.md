# README #

Steps necessary to get LemrModule up and running.

### What is this repository for? ###

* Quick summary
Lemr Module for quick REST app development
* Version


### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
  			
```
#!xml

		      <connection-url>jdbc:mysql://localhost/lemr_module</connection-url>
  		      <jndi-name>java:jboss/LemrDatasource</jndi-name>
                     <enabled>true</enabled>
                     <enable>true</enable>
                     <user-name>root</user-name>
                     <password></password>
  					 <driver-name>mysql.jar</driver-name>
```

* How to run tests
 1.  uses wildfly-8.2.0.Final
 2.   Set JBOSS_HOME 
 3.   start the wildfly 
 4.   then on mvn console :
    
```
#!mvn

mvn clean test -Parq-wildfly-remote
```


* Deployment instructions
  1. uses wildfly-8.2.0.Final
  2. set JBOSS_HOME 
  3.  start the wildfly 
  4.  then on mvn console :
    
```
#!mvn

mvn clean wildfly:deploy
```

    
### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact